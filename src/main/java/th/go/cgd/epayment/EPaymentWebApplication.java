package th.go.cgd.epayment;

import com.captcha.botdetect.web.servlet.SimpleCaptchaServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.Arrays;

@SpringBootApplication
@EnableTransactionManagement
@EnableAutoConfiguration
@ComponentScan(basePackages = "th.go.cgd.epayment")
public class EPaymentWebApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(EPaymentWebApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(EPaymentWebApplication.class);
	}

	@Bean
	public ServletRegistrationBean dispatcherServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(new SimpleCaptchaServlet());
		registration.setUrlMappings(Arrays.asList("/botdetectcaptcha"));
		registration.setLoadOnStartup(3);
		return registration;
	}


}
