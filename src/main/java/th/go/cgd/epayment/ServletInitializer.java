package th.go.cgd.epayment;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.ws.transport.http.MessageDispatcherServlet;

import th.go.cgd.epayment.config.WebMvcConfig;

public class ServletInitializer implements WebApplicationInitializer {


	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
		ctx.register(WebMvcConfig.class);
		ctx.setServletContext(servletContext);

		MessageDispatcherServlet servlet = new MessageDispatcherServlet();
		servlet.setContextClass(AnnotationConfigWebApplicationContext.class);
		servlet.setApplicationContext(ctx);
		servlet.setTransformWsdlLocations(true);

		Dynamic dynamic = servletContext.addServlet("dispatcher-servlet", new DispatcherServlet(ctx));
		dynamic.addMapping("/e-payment-web");
		dynamic.setLoadOnStartup(1);

	}

}
