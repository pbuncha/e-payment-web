package th.go.cgd.epayment.component;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import th.go.cgd.epayment.entity.LogAccess;
import th.go.cgd.epayment.entity.MLogAction;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.util.ClientMachineUtil;
import th.go.cgd.epayment.util.IPAddressUtil;

@Component
public class EpaymentUserLogin implements UserDetailsService, AuthenticationFailureHandler, LogoutHandler, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger logger = Logger.getLogger(EpaymentUserLogin.class);

	private static final String LOG_IN = "IN";
	
	private static final String LOGIN = "/login";
	private static final String LOGIN_G = "/login_goverment";
	
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	@Autowired
	private UserService userService;
	
//	@Autowired
//	private LogAccessService logAccessService;
	
	@Override
	public UserDetails loadUserByUsername(String userName)
			throws UsernameNotFoundException {
		
		User user = userService.findByUserName(userName);
		
		if(user == null) {
			throw new UsernameNotFoundException("Username Not Found");
		}
		return new SecurityUser(user);
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex)
			throws IOException, ServletException {
		
		String username = request.getParameter("username");
		String status = "";
		System.out.println("In onAuthenticationFailure");
		System.out.println(ex);
		Boolean bFromLoginG =  request.getHeader("Referer").contains("goverment");
		String redirectPage = (bFromLoginG ? LOGIN_G : LOGIN);
		if(ex instanceof BadCredentialsException) { System.out.println("In ex BadCredentialsException");
		 	redirectStrategy.sendRedirect(request, response, redirectPage + "?error=1");
			//response.sendRedirect("login?error=1"); // BAD_CREDENTIAL
			status = "BAD_CREDENTIAL";
		} else if(ex instanceof DisabledException) { System.out.println("In ex DisabledException");
			redirectStrategy.sendRedirect(request, response, redirectPage + "?disabled=1");
			//response.sendRedirect("login?disabled=1"); // USER_DISABLED
			status = "USER_DISABLED";
		} else { System.out.println("In OTHER_EXCEPTION");
			redirectStrategy.sendRedirect(request, response, redirectPage + "?error=1");
			//response.sendRedirect("login?error=1"); // OTHER_EXCEPTION
			status = "OTHER_EXCEPTION";
		}
		
		String computerName = ClientMachineUtil.getClientMachineName(request);
		MLogAction mLogAction = new MLogAction();
		mLogAction.setActionId(LOG_IN);
		LogAccess logAccess = new LogAccess();
//		logAccess.setUserName(username);
		logAccess.setAccessDate(Calendar.getInstance().getTime());
		logAccess.setExecutionTime((short) 0);
		logAccess.setClientIp(IPAddressUtil.getClientAddress(request));
//		logAccess.setMLogAction(mLogAction);
		logAccess.setUrl(request.getRequestURL().toString());
		logAccess.setClientName(computerName);
		logAccess.setStatus(status);
//		logAccessService.save(logAccess);
		
		
	}

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authen) {
		
		request.getSession().invalidate();
	}
	
	public class SecurityUser extends User implements UserDetails{

		private static final long serialVersionUID = 1L;
		public SecurityUser(User user) {
		    System.out.println("in SecurityUser");
		    
			if(user != null){
			    List<String> roleUser = new ArrayList<String>();
			    roleUser.add("ADMIN");
			    
			   
//				user.setUserRole(roleUser);
//				user.setAuthorities(listRole);
//				
				
				this.setUserId(user.getUserId());
				this.setUserName(user.getUserName());
				this.setPassword(user.getPassword());
				this.setStatus(user.getStatus());
				System.out.println(user.getPassword());
				//System.out.println(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
//				this.setUserRoles(listRole);
//				this.setMUserStatus(user.getMUserStatus());


			}		
		}
		
		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			
			Collection<GrantedAuthority> authorities = new ArrayList<>();
//			List<Role> userRoles = this.getRoles();
//			
//			if(userRoles != null)
//			{
//				for (Role role : userRoles) {
					SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ADMIN");
					authorities.add(authority);
//				}
//			}
			return authorities;
		}
		
		@Override
		public String getUsername() {
			return super.getUserName();
		}

		@Override
		public String getPassword() {
			return super.getPassword();
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			//if(super.getMUserStatus() == null || !super.getMUserStatus().getStatus().equals("A")){
			if(super.getStatus() != '1'){
				return false;
			}
			return true;
		}
		
	}

	
}
