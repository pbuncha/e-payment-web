//package th.go.cgd.epayment.component;
//
//import java.sql.Connection;
//import java.sql.SQLException;
//import javax.annotation.PostConstruct;
//import javax.sql.DataSource;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.core.Appender;
//import org.apache.logging.log4j.core.LoggerContext;
//import org.apache.logging.log4j.core.appender.db.jdbc.ColumnConfig;
//import org.apache.logging.log4j.core.appender.db.jdbc.ConnectionSource;
//import org.apache.logging.log4j.core.appender.db.jdbc.JdbcAppender;
//import org.apache.logging.log4j.core.config.Configuration;
//import org.apache.logging.log4j.core.config.LoggerConfig;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
////@Component
//public class LogUtils {
//
//	@Autowired
//	private DataSource dataSource;
//
//	// inner class
//	class Connect implements ConnectionSource {
//		private DataSource dsource;
//
//		public Connect(DataSource dsource) {
//			this.dsource = dsource;
//		}
//
//		@Override
//		public Connection getConnection() throws SQLException {
//			return this.dsource.getConnection();
//		}
//	}
//
//	public LogUtils() {
//		System.err.println("logutils");
//	}
//
//	@PostConstruct
//	private void init() throws SQLException {
//		System.err.println("init logutils: " + dataSource.getConnection().isClosed());
//		final LoggerContext ctx = (LoggerContext) LogManager.getContext(false);
//		final Configuration config = ctx.getConfiguration();
//		ColumnConfig[] cc = {
//				// ColumnConfig.createColumnConfig(config, "LOGGING", null,
//				// null, null, null, null),
//				ColumnConfig.createColumnConfig(config, "LOG_LEVEL", "%level", null, null, "false", null),
//				// ColumnConfig.createColumnConfig(config, "USER_ACEES_NAME",
//				// "%logger", null, null, null, null),
//				ColumnConfig.createColumnConfig(config, "LOG_MESSAGE", "%message", null, null, "false", null) };
//		// ColumnConfig.createColumnConfig(config, "CREATE_DATE", null, null,
//		// "true", null, null) };
//		Appender appender = JdbcAppender.createAppender("Database_Appender", "true", null, new Connect(dataSource), "0",
//				"PAYDB.LOGGING", cc);
//		appender.start();
//		config.addAppender(appender);
//		LoggerConfig loggerConfig = config.getLoggerConfig("Database_Appender");
//		loggerConfig.addAppender(appender, null, null);
//		ctx.updateLoggers();
//	}
//
//	public DataSource getDataSource() {
//		return dataSource;
//	}
//
//	public void setDataSource(DataSource dataSource) {
//		this.dataSource = dataSource;
//	}
//}
