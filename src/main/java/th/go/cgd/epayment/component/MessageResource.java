package th.go.cgd.epayment.component;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

@Component
public class MessageResource {
	
	@Resource(name = "messageResourceProperties")
	private Map<String, String> messageResource;

	public String getMessage(String key) {
		return messageResource.containsKey(key) ? messageResource.get(key) : null;
	}
	
	public Map<String, String> getMessageResource(){
		return messageResource;
	}
	
	
	/**
	 * All Key from file MessageResource.properties
	 */
	public static final String REPORT_PICK_ALL_DESC = "report.criteria.pick.all";

}
