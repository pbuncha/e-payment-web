package th.go.cgd.epayment.component;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PasswordConfigModel {
	
	@Value("${password.maxLength}")
	public Integer maxLength;
	@Value("${password.minLength}")
	public Integer minLength;
	@Value("${password.pattern.charCase}")
	public String charCase;
	@Value("${password.pattern.number}")
	public String number;
	@Value("${password.pattern.matchChar}")
	public String matchChar;
	@Value("${password.allowChar}")
	public String allowChar;
		
	
	
}
