package th.go.cgd.epayment.component;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.model.UserSession;

@Component
public class SessionUtil {
	
	public SessionUtil() {
	}

	public static UserSession getUserSession() {
		ServletRequestAttributes req=(ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();
		HttpServletRequest request = req.getRequest();
		UserSession userSession = (UserSession) request.getSession()
				.getAttribute(EPaymentConstant.USER_SESSION);
		return userSession;
	}

	public static UserSession getUserSession(HttpServletRequest request) {
		UserSession userSession = (UserSession) request.getSession()
				.getAttribute(EPaymentConstant.USER_SESSION);
		return userSession;
	}

}
