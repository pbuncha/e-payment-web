package th.go.cgd.epayment.config;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import th.go.cgd.epayment.interceptor.LogAccessInterceptor;

@Configuration
public class ComponentConfig {

	private Logger log = Logger.getLogger(ComponentConfig.class);
	private static final String PROPERTIES_SOUCRE = "MessageResource.properties";
	@Value("${temporary.path.pdf}")
	private String temporaryPathPdf; 
	
	@PostConstruct
	public void init() {
		createTemporaryDirectory();
	}
	
	private void createTemporaryDirectory(){
		log.info("Initial temporary directory.");
		Path path = Paths.get(temporaryPathPdf);
		// if directory exists?
		if (!Files.exists(path)) {
			log.info("create path: " + temporaryPathPdf);
			new File(temporaryPathPdf).mkdirs();
		} else {
			log.info(temporaryPathPdf + "is exists.");
		}
		log.info("Initial temporary directory success");
	}
	
	@Bean(name = "messageResourceProperties")
	public PropertiesFactoryBean messageResourceProperties() throws IOException {
		PropertiesFactoryBean bean = new PropertiesFactoryBean();
		bean.setLocation(new ClassPathResource(PROPERTIES_SOUCRE));
		return bean;
	}
	
	
	@Bean
    public LocaleChangeInterceptor localeChangeInterceptor() 
	{
        LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
        lci.setParamName("locale");
        return lci;
    }
 
    @Bean
	public LocaleResolver localeResolver()
	{
	    SessionLocaleResolver localeResolver = new SessionLocaleResolver();
	    localeResolver.setDefaultLocale(new Locale("th","TH"));
	    return localeResolver;
	}
    

    
    @Bean
    public LogAccessInterceptor logAccessInterceptor() {
    	return new LogAccessInterceptor();
    }
}
