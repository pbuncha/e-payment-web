package th.go.cgd.epayment.config;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.service.jdbc.connections.spi.ConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jndi.JndiObjectFactoryBean;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class DatabaseConfigStub {
    
    @Value("${spring.epay.datasource.jndi-name}")
    private String jndiName;
    @Value("${spring.epay.datasource.hibernate.show_sql}")
    private String showSql;
    @Value("${spring.epay.datasource.hibernate.hbm2ddl.auto}")
    private String hbm2ddlAuto;
    @Value("${spring.epay.datasource.hibernate.dialect}")
    private String hibernateDialect;
    @Value("${spring.epay.datasource.enable.lazy.load.no.trans}")
    private String enableLazyLoadNoTrans;
    @Value("${spring.epay.datasource.hibernate.service.jta.platform}")
    private String hibernateTransactionJtaPlatform;
    @Value("${spring.epay.datasource.schema}")
    private String dataSourceSchema;
    @Value("${spring.epay.datasource.driverClassName}")
    private String driverClassName;
    @Value("${spring.epay.datasource.url}")
    private String url;
    @Value("${spring.epay.datasource.username}")
    private String username;
    @Value("${spring.epay.datasource.password}")
    private String password;
//    @Value("${spring.epay.datasource.jndi-name.tomcat}")
//    private String jndiNameServer;

    
   
    @Profile("prod")
    @Bean(name = "dataSource")
    @ConfigurationProperties(prefix = "spring.epay.datasource")
    public DataSource epayDataSource() throws IllegalArgumentException, NamingException {
	JndiObjectFactoryBean bean = new JndiObjectFactoryBean();
	bean.setJndiName(jndiName);
	bean.setProxyInterface(DataSource.class);
	bean.setLookupOnStartup(false);
	bean.afterPropertiesSet();
	return (DataSource)bean.getObject();
    }
    
    @Profile("dev")
	@Bean(name = "dataSource")
	public DataSource dataSourceDev() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName( driverClassName );
		dataSource.setUrl(url);
		dataSource.setUsername(username);
		dataSource.setPassword(password);
		dataSource.setInitialSize(1);
		dataSource.setMaxIdle(-1);
		dataSource.setMaxTotal(-1);
		return dataSource;
	}
    
    
    @Bean 
    public EntityManagerFactory entityManagerFactory( @Autowired DataSource datasource) throws IllegalArgumentException, NamingException {
      HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
      vendorAdapter.setGenerateDdl(true);
      LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
      factory.setJpaVendorAdapter(vendorAdapter);
      factory.setPackagesToScan("th.go.cgd.epayment");
      factory.setDataSource(datasource);
      factory.afterPropertiesSet();
      return factory.getObject();
    }
    
    
    @Bean
    public PlatformTransactionManager transactionManager(  EntityManagerFactory entityManagerFactory) {
    	return new JpaTransactionManager(entityManagerFactory);
    }
    
    @Bean
    public SessionFactory sessionFactory() throws IOException, IllegalArgumentException, NamingException{
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setPackagesToScan("th.go.cgd.epayment");
        sessionFactoryBean.setHibernateProperties(getHibernateProperties());
        sessionFactoryBean.setDataSource(epayDataSource());
        sessionFactoryBean.afterPropertiesSet();
        return sessionFactoryBean.getObject();
    }

	@Bean
	public java.sql.Connection  jdbcConnection( SessionFactory sessionFactory) throws SQLException {
		SessionFactoryImplementor sfi = (SessionFactoryImplementor) sessionFactory;
		ConnectionProvider connectionProvider = sfi.getConnectionProvider();
		return connectionProvider.getConnection();
	}
	
	
    private Properties getHibernateProperties() {
      Properties properties = new Properties();
      	properties.put("hibernate.show_sql", showSql);
      	properties.put("hibernate.hbm2ddl.auto", hbm2ddlAuto);
      	properties.put("hibernate.dialect", hibernateDialect);
  	properties.put("hibernate.enable_lazy_load_no_trans", enableLazyLoadNoTrans);
  	properties.put("hibernate.transaction.jta.platform", hibernateTransactionJtaPlatform);
  	properties.put("hibernate.default_schema", dataSourceSchema);

      return properties;
  }
    
    
    
//    @Bean
//	public TomcatEmbeddedServletContainerFactory tomcatFactory() {
//		return new TomcatEmbeddedServletContainerFactory() {
//			@Override
//			protected TomcatEmbeddedServletContainer getTomcatEmbeddedServletContainer(
//					Tomcat tomcat) {
//				tomcat.enableNaming();
//				return super.getTomcatEmbeddedServletContainer(tomcat);
//			}
//			@Override
//			protected void postProcessContext(Context context) {
//				ContextResource resource = new ContextResource();
//				resource.setName(jndiNameServer);
//				resource.setType(DataSource.class.getName());
//				resource.setProperty("driverClassName", driverClassName);
//				resource.setProperty("url", url);
//				resource.setProperty("username", username);
//				resource.setProperty("password", password);
//				resource.setProperty("schema", dataSourceSchema);
//				context.getNamingResources().addResource(resource);
//			}
//		};
//	}



}
