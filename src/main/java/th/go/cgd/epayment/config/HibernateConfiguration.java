//package th.go.cgd.epayment.config;
//
//import java.io.IOException;
//import java.util.Properties;
//
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.env.Environment;
//import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
//import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
//import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
//import org.springframework.transaction.annotation.EnableTransactionManagement;
//
//@Configuration
//@PropertySource({"classpath:/hibernate.properties"})
//@EnableTransactionManagement
//public class HibernateConfiguration {
//	
//	private static final Logger logger = LogManager.getLogger(HibernateConfiguration.class);
//	
//	@Autowired
//    private javax.sql.DataSource dataSource;
//	
//	@Autowired
//	private Environment env;
//	
//	@Bean
//    LocalSessionFactoryBean getSessionFactory() throws IOException {
//    	logger.info("HibernateConfig.getSessionFactory() starting");
//    	
//        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
//        factoryBean.setHibernateProperties(getHibernateProperties());
//        factoryBean.setDataSource(dataSource);
//        factoryBean.setPackagesToScan("th.go.cgd.epayment.entity");
//        return factoryBean;
//    }
//
//	
//	@Bean
//    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
//        return new PersistenceExceptionTranslationPostProcessor();
//    }
//	
//	private Properties getHibernateProperties() {
//        Properties properties = new Properties();
//        
//        properties.put("hibernate.show_sql", env.getRequiredProperty("hibernate.show_sql"));
//        properties.put("hibernate.hbm2ddl.auto", env.getRequiredProperty("hibernate.hbm2ddl.auto"));
//        properties.put("hibernate.dialect", env.getRequiredProperty("hibernate.dialect"));
//    	properties.put("hibernate.enable_lazy_load_no_trans", env.getRequiredProperty("hibernate.enable_lazy_load_no_trans"));
//    	properties.put("hibernate.transaction.jta.platform", env.getRequiredProperty("hibernate.transaction.jta.platform"));
//    	properties.put("hibernate.default_schema", env.getRequiredProperty("hibernate.default_schema"));
//    	
//        return properties;
//    }
//
//}
