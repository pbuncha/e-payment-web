package th.go.cgd.epayment.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

@Service
public class MenuPermissionConfig {
    
	@PersistenceContext
	private EntityManager entityManager;
	private static Map<String, String[]> permissionMapping;
	
	private static final String SQL =
			"SELECT "+
			"    PROGRAM_CODE "+
			"    , LISTAGG(M_PERMISSION_GROUP_CODE, ',') WITHIN GROUP( ORDER BY PROGRAM_CODE) AS ROLES "+
			"FROM (  SELECT T3.PROGRAM_CODE, "+
			"            T1.M_PERMISSION_GROUP_CODE "+
			"        FROM PAYDB.M_PERMISSION_GROUP T1 "+
			"            INNER JOIN PAYDB.M_PERMISSIONGROUP_PROGRAM T2 "+
			"            ON T1.M_PERMISSION_GROUP_ID = T2.M_PERMISSION_GROUP_ID"+
			"            INNER JOIN PAYDB.M_PROGRAM T3 "+
			"            ON T2.PROGRAM_ID = T3.PROGRAM_ID "+
			"		WHERE T1.STATUS = '1' AND T3.STATUS = '1' AND T2.STATUS = '1' "+
			"    ) T0 "+
			" GROUP BY PROGRAM_CODE ";
	
	
	@Bean(name = "menuRoleService")
    public MenuRoleService MenuRoleService() {
        return (String menuId) -> permissionMapping.get(menuId);
    }

    public interface MenuRoleService {
        String[] getPermission( String menuId);
    }
    
    
    @PostConstruct
    @SuppressWarnings("unchecked")
    public void initialPermission(){
    	System.out.println("Initial Permission");
    	
    	permissionMapping = new HashMap<>();
    	Query query = entityManager.createNativeQuery(SQL);
		List<Object[]> result = query.getResultList();
    	for(Object[] row : result){
    		Object[] cols = (Object[]) row;
    		String key = cols[0].toString();
    		String[] value = (cols[1].toString()).split(",");
    		System.out.println( "key=" +cols[0].toString() + ", value="+cols[1].toString() );
    		permissionMapping.put(key, value );
    	}
    }
    
}

