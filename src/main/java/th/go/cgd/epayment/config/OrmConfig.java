package th.go.cgd.epayment.config;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.autoconfigure.transaction.TransactionManagerCustomizers;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.jta.JtaTransactionManager;

import th.go.cgd.epayment.util.OrmXmlManager;

//@Configuration
//@EnableTransactionManagement
public class OrmConfig {
	
	private static final Logger logger = Logger.getLogger(OrmConfig.class);
	
	
	@Configuration
    public static class HibernateConfiguration extends HibernateJpaAutoConfiguration 
    {
        /**
	     * @param dataSource
	     * @param jpaProperties
	     * @param jtaTransactionManager
	     * @param transactionManagerCustomizers
	     */
	    public HibernateConfiguration(DataSource dataSource, JpaProperties jpaProperties,
		    ObjectProvider<JtaTransactionManager> jtaTransactionManager,
		    ObjectProvider<TransactionManagerCustomizers> transactionManagerCustomizers) {
		super(dataSource, jpaProperties, jtaTransactionManager, transactionManagerCustomizers);
		// TODO Auto-generated constructor stub
	    }

	@Bean
        @Override
        public LocalContainerEntityManagerFactoryBean entityManagerFactory(EntityManagerFactoryBuilder factoryBuilder)
        {
            final LocalContainerEntityManagerFactoryBean l = super.entityManagerFactory(factoryBuilder);
            l.setMappingResources("orm.xml");
            return l;
        }
	}
    
    @PostConstruct
	public void appInit() {
		OrmXmlManager.init(OrmConfig.class.getResource("/orm.xml").getPath());
	}

}
