package th.go.cgd.epayment.config;
 
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.filter.CharacterEncodingFilter;


import th.go.cgd.epayment.component.EpaymentUserLogin;
import th.go.cgd.epayment.controller.common.LoginController;
import th.go.cgd.epayment.security.AuthenFailureHandler;
import th.go.cgd.epayment.security.CustomAuthenticationProvider;
import th.go.cgd.epayment.security.filter.ExtraParamsUsernamePasswordAuthenticationFilter;

import java.util.ArrayList;
import java.util.List;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled  = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter
{

	private final Logger log = Logger.getLogger(this.getClass());

	@Autowired
	private UserDetailsService userDS;

	@Autowired
	private AuthenFailureHandler failureHandler;

	@Autowired
	private CustomAuthenticationProvider authenticationProvider;
	
	@Override
    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/css/**"
//        	,"/js/**"
//        	,"/lib/**"
//        	, "/images/**"
//        	, "/img/**"
//        	, "/resources/**"
////        	, "/assets/**"
//        	,"/fonts/**");

	    web.ignoring().antMatchers("/css/**");
	    web.ignoring().antMatchers("/js/**");
	    web.ignoring().antMatchers("/lib/**");
	    web.ignoring().antMatchers("/images/**");
	    web.ignoring().antMatchers("/img/**");
	    web.ignoring().antMatchers("/resources/**");
	    web.ignoring().antMatchers("/assets/**");
	    web.ignoring().antMatchers("/fonts**");
//	    web.ignoring().antMatchers("/favicon.ico/**");
//	    web.ignoring().antMatchers("/static/js/**");
//	    web.ignoring().antMatchers("/static/assets/**");
	       
	    
    }
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
		CharacterEncodingFilter filter = new CharacterEncodingFilter();
		filter.setEncoding("UTF-8");
		filter.setForceEncoding(true);
		
//		http.csrf().disable();
		
		http.authorizeRequests().antMatchers(
			"/login_citizen"
			,"/login1"
			,"/login2"
			,"/login**"
			, "/login_goverment"
			,"/login/submit"
			,"/logout"
			,"/publish-news/**"
			,"/reset-password/forgot"
			,"/reset-password/token/**"
			,"/static/**"
			,"/bower_components/**"
			,"/reset-password/reset"
			,"/reset-password/complete"
			,"/reset-password/expire"
			,"/reset-password/check-password"
			,"/registerController/**"
			,"/botdetectcaptcha**"
//			,"/paymentController/**"
			,"/login/**"
//			,"/manageInvoice/**"
			,"/static/assets/**"
			,"/bower_components/**"
//			,"/templates/views/payment/**"
			,"/service/**"
			,"/epaymentApiService/**"
			,"/templates/views/forgot/**"
			).anonymous();
		
		http.authorizeRequests().antMatchers(
//			"/paymentController/**"
			"/registerController/**"
//			,"/manageInvoice/**"
			,"/static/assets/**"
			,"/bower_components/**"
//			,"/templates/views/payment/**"
			,"/epaymentApiService/**"
			,"/templates/views/forgot/**"
			,"/botdetectcaptcha**"
			).permitAll().anyRequest().authenticated();
		

		
		  http.csrf().disable().authorizeRequests().antMatchers("/login","/service/**").anonymous()
		        .antMatchers("/static/logout").permitAll()
		        .antMatchers("/static/js/**").permitAll()
		        .antMatchers("/static/css/**").permitAll()
		        .antMatchers("/static/images/**").permitAll();
//		        .antMatchers("/static/assets/**").permitAll();


		
		http.headers().frameOptions().sameOrigin().and()
		.authorizeRequests()
//			.antMatchers(
//					"/login_citizen"//"/login_citizen/submit",
//					,"/login1"
//					,"/login2"
//					,"/login**"
//					, "/login_goverment"
//					,"/login/submit"
//					,"/logout"
//					,"/publish-news/**"
//					,"/reset-password/forgot"
//					,"/reset-password/token/**"
//					,"/static/**"
//					,"/bower_components/**"
//					,"/reset-password/reset"
//					,"/reset-password/complete"
//					,"/reset-password/expire"
//					,"/reset-password/check-password"
//					"/registerController/**"
//					,"/botdetectcaptcha**"
//					)
//					.permitAll()
					//,"/login_citizen","/login_citizen/submit").permitAll()
			.antMatchers("/citizen/**").hasAuthority("ROLE_CITIZEN")
			.antMatchers("/goverment/**").hasAuthority("ROLE_CITIZEN")
			.anyRequest().fullyAuthenticated()
			.and()
//			.addFilterBefore(authenticationFilter(),UsernamePasswordAuthenticationFilter.class)
			.formLogin()
			.loginPage("/login")
			//.loginPage("/login1")
//			.loginProcessingUrl("/login/submit")
			//.loginProcessingUrl("/login_citizen/submit")
			.failureHandler(failureHandler)
			//.defaultSuccessUrl("/login/success", true)
//			.defaultSuccessUrl("/login_citizen/success", true)
			.usernameParameter("username")
			.passwordParameter("password");

    }
    
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception 
    {
//    	auth.inMemoryAuthentication().withUser("admin").password("p@ssw0rd").roles("ADMIN");
//    	auth.userDetailsService(userDS).passwordEncoder(new BCryptPasswordEncoder());
    	 auth.authenticationProvider(authenticationProvider);
    	
    	
    }
    
    @Bean
    SessionRegistry sessionRegistry() {            
        return new SessionRegistryImpl();
    }

	@Bean
	public UsernamePasswordAuthenticationFilter authenticationFilter() {
		ExtraParamsUsernamePasswordAuthenticationFilter authFilter = new ExtraParamsUsernamePasswordAuthenticationFilter();
		List<AuthenticationProvider> authenticationProviderList = new ArrayList<AuthenticationProvider>();
		authenticationProviderList.add(authenticationProvider);
		AuthenticationManager authenticationManager = new ProviderManager(authenticationProviderList);
		authFilter.setAuthenticationManager(authenticationManager);
		authFilter.setUsernameParameter("username");
		authFilter.setPasswordParameter("password");
		authFilter.setAuthenticationFailureHandler(failureHandler);
		authFilter.addParameters("rdbGroup");
		return authFilter;
	}

	@Bean
	public AuthenFailureHandler authenticationHandlerBean() {
		return new AuthenFailureHandler();
	}
}