package th.go.cgd.epayment.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.server.endpoint.adapter.MessageEndpointAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

@EnableWs
@Configuration
public class SoapConfig extends WsConfigurerAdapter {

	public static final String BILLPAYMENT_NAMESPACE = "http://cgd.go.th/billpayment";
	private static final String[] WS_URL_MAPPINGS = { "/service", "*.wsdl" };

	@Bean
	public ServletRegistrationBean messageDispatcherServletRegistration(ApplicationContext ctx) {
		MessageDispatcherServlet wsDispatcherServlet = new MessageDispatcherServlet();
		wsDispatcherServlet.setApplicationContext(ctx);
		wsDispatcherServlet.setTransformWsdlLocations(true);
		ServletRegistrationBean registration = new ServletRegistrationBean(wsDispatcherServlet, WS_URL_MAPPINGS);
		registration.setLoadOnStartup(2);
		return registration;
	}

	@Bean(name = "billpayment")
	@Qualifier("billpaymentSchema")
	public DefaultWsdl11Definition bankDefaultWsdl11Definition(XsdSchema billPaymentSchema) {
		DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
		wsdl11Definition.setPortTypeName("BillpaymentPort");
		wsdl11Definition.setLocationUri("/service");
		wsdl11Definition.setTargetNamespace(BILLPAYMENT_NAMESPACE);
//		wsdl11Definition.setRequestSuffix("Service");
		wsdl11Definition.setSchema(billPaymentSchema);
		return wsdl11Definition;
	}

	@Bean(name = "billpaymentSchema")
	public XsdSchema billPaymentSchema() {
		return new SimpleXsdSchema(new ClassPathResource("schema/BillPayment.xsd"));
	}

	@Bean
	public MessageEndpointAdapter adapter() {
		return new MessageEndpointAdapter();
	}

	// XwsSecurityInterceptor
	// @Bean
	// public XwsSecurityInterceptor securityInterceptor(){
	// XwsSecurityInterceptor securityInterceptor = new
	// XwsSecurityInterceptor();
	// //Callback Handler -> SimplePasswordValidationCallbackHandler
	// securityInterceptor.setCallbackHandler(callbackHandler());
	// //Security Policy -> securityPolicy.xml
	// securityInterceptor.setPolicyConfiguration(new
	// ClassPathResource("securityPolicy.xml"));
	// return securityInterceptor;
	// }
	//
	// @Bean
	// public SimplePasswordValidationCallbackHandler callbackHandler() {
	// SimplePasswordValidationCallbackHandler handler = new
	// SimplePasswordValidationCallbackHandler();
	// handler.setUsersMap(Collections.singletonMap("user", "password"));
	// return handler;
	// }
	//
	// //Interceptors.add -> XwsSecurityInterceptor
	// @Override
	// public void addInterceptors(List<EndpointInterceptor> interceptors) {
	// interceptors.add(securityInterceptor());
	// }

}