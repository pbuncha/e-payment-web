package th.go.cgd.epayment.config;

import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import th.go.cgd.epayment.config.argumentResolver.WisesoftArgumentResolver;

@Configuration
public class WebMvcConfig extends WebMvcConfigurerAdapter {
	
	@Value("${temporary.path.pdf}")
	private String temporaryPathPdf; 
	
	public WebMvcConfig() {
		Locale.setDefault(Locale.ENGLISH);
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		System.out.println(temporaryPathPdf);
		registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
		registry.addResourceHandler("/barcode/**").addResourceLocations("classpath:/barcode/");
		registry.addResourceHandler("/templates/**").addResourceLocations("classpath:/templates/");
		registry.addResourceHandler("/static/lib/**").addResourceLocations("classpath:/lib/");
		registry.addResourceHandler("/bower_components/**").addResourceLocations("classpath:/bower_components/");
		registry.addResourceHandler("/temporary/**/*").addResourceLocations( "file:"+temporaryPathPdf );
		registry.addResourceHandler("/file_upload/**").addResourceLocations( "classpath:/file_upload/" );
	}

	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		argumentResolvers.add(new WisesoftArgumentResolver());
	}

}