package th.go.cgd.epayment.config.argumentResolver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.springframework.format.AnnotationFormatterFactory;
import org.springframework.format.Parser;
import org.springframework.format.Printer;
import org.springframework.format.annotation.DateTimeFormat;

public class DateFormatter implements
		AnnotationFormatterFactory<DateTimeFormat> {

	@Override
	public Set<Class<?>> getFieldTypes() {
		HashSet<Class<?>> fieldSet = new HashSet<>();
		fieldSet.add(Date.class);
		return fieldSet;
	}

	@Override
	public Printer<?> getPrinter(DateTimeFormat annotation, Class<?> fieldType) {
		return new DatePrinter(annotation.pattern());
	}

	@Override
	public Parser<?> getParser(DateTimeFormat annotation, Class<?> fieldType) {
		return new DateParser(annotation.pattern());
	}

	private static class DatePrinter implements Printer<Date> {
		public String pattern;

		public DatePrinter(String pattern) {
			this.pattern = pattern;
		}

		@Override
		public String print(Date object, Locale locale) {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern, locale);
			return sdf.format(object);
		}
	}

	private static class DateParser implements Parser<Date> {

		public String pattern;

		public DateParser(String pattern) {
			this.pattern = pattern;
		}

		@Override
		public Date parse(String text, Locale locale) throws ParseException {
			SimpleDateFormat sdf = new SimpleDateFormat(pattern, locale);
			return sdf.parse(text);
		}

	}
}
