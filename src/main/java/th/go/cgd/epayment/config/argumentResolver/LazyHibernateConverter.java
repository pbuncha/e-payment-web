package th.go.cgd.epayment.config.argumentResolver;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.hibernate.proxy.HibernateProxy;
import org.hibernate.proxy.LazyInitializer;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalGenericConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
public class LazyHibernateConverter implements ConditionalGenericConverter, ApplicationContextAware{
	
	//@Autowired
	//private ConversionService conversionService;

	private ApplicationContext applicationContext;
	
	private static class LazyConverterCacheKey {
		Class<?> clazz;
		String id;
		@Override
		public int hashCode() {
			return clazz.hashCode() * 3 + id.hashCode() * 11;
		}
		@Override
		public boolean equals(Object obj) {
			if (obj instanceof LazyConverterCacheKey) {
				LazyConverterCacheKey lazyConverterCacheKey = (LazyConverterCacheKey) obj;
				return clazz.equals(lazyConverterCacheKey.clazz) && id.equals(lazyConverterCacheKey.id);
			}
			return false;
		}
	}

	@Override
	public Set<ConvertiblePair> getConvertibleTypes() {
		HashSet<ConvertiblePair> pair = new HashSet<>();
		pair.add(new ConvertiblePair(HibernateProxy.class, String.class));
		pair.add(new ConvertiblePair(String.class, HibernateProxy.class));
		return pair; // damn all hibernate and other type
	}

	@Override
	public Object convert(Object source, TypeDescriptor sourceType, TypeDescriptor targetType) {
		if (HibernateProxy.class.isAssignableFrom(sourceType.getObjectType())) {
			HibernateProxy hibernateProxy = (HibernateProxy)source;
			LazyInitializer lazyMan = hibernateProxy.getHibernateLazyInitializer();
			String id = lazyMan.getIdentifier().toString();
			LazyConverterCacheKey lazyConverterCacheKey = new LazyConverterCacheKey();
			lazyConverterCacheKey.clazz = lazyMan.getPersistentClass();
			lazyConverterCacheKey.id = id;
			getRequestCache().put(lazyConverterCacheKey, hibernateProxy);
			return id;
		}
		
		if (String.class.isAssignableFrom(sourceType.getObjectType()) && HibernateProxy.class.isAssignableFrom(targetType.getObjectType())) {
			Class<?> clazz = targetType.getObjectType().getSuperclass();
			LazyConverterCacheKey lazyConverterCacheKey = new LazyConverterCacheKey();
			lazyConverterCacheKey.id = (String)source;
			lazyConverterCacheKey.clazz = clazz;
			Map<LazyConverterCacheKey, HibernateProxy> cache = getRequestCache();
			if (cache.containsKey(lazyConverterCacheKey)) {
				return cache.get(lazyConverterCacheKey);
			} else {
				return applicationContext.getBean(ConversionService.class).convert(source, clazz);
			}
		}
		
		return null;
		
	}

	@Override
	public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
		if (HibernateProxy.class.isAssignableFrom(sourceType.getObjectType())) {
			return true;
		}
		
		if (String.class.isAssignableFrom(sourceType.getObjectType()) && HibernateProxy.class.isAssignableFrom(targetType.getObjectType())) {
			return true;
		}
		return false;
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}
	
	private static Map<LazyConverterCacheKey, HibernateProxy> getRequestCache() {
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)RequestContextHolder.currentRequestAttributes();
		@SuppressWarnings("unchecked")
		Map<LazyConverterCacheKey, HibernateProxy> cache = (Map<LazyConverterCacheKey, HibernateProxy>) servletRequestAttributes.getRequest().getAttribute("LazyHibernateConverterCacheTypeId");
		if (cache == null) {
			cache = new HashMap<>();
			servletRequestAttributes.getRequest().setAttribute("LazyHibernateConverterCacheTypeId", cache);
		}
		return cache;
	}

}
