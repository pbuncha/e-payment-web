package th.go.cgd.epayment.config.argumentResolver;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import th.go.cgd.epayment.component.SessionUtil;
import th.go.cgd.epayment.model.UserSession;

public class WisesoftArgumentResolver implements HandlerMethodArgumentResolver{
	
	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		//UserSession
		if (UserSession.class.isAssignableFrom(parameter.getParameterType())){
			return true;
		}
		
		//RedirectSession
		return false;
	}

	@Override
	public Object resolveArgument(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) throws Exception {
		//UserSession
		if (UserSession.class.isAssignableFrom(parameter.getParameterType())) {
			return resolveUserSession(parameter, mavContainer, webRequest, binderFactory);
		}
		
		return WebArgumentResolver.UNRESOLVED;
	}
	
	private UserSession resolveUserSession(MethodParameter parameter,
			ModelAndViewContainer mavContainer, NativeWebRequest webRequest,
			WebDataBinderFactory binderFactory) {
		HttpServletRequest request = (HttpServletRequest) webRequest.getNativeRequest();
		return SessionUtil.getUserSession(request);
	}

}
