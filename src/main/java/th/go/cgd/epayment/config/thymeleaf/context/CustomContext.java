package th.go.cgd.epayment.config.thymeleaf.context;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.core.convert.ConversionService;
import org.thymeleaf.context.Context;
import org.thymeleaf.context.IWebContext;
import org.thymeleaf.context.VariablesMap;
import org.thymeleaf.spring4.expression.ThymeleafEvaluationContext;

public class CustomContext extends Context implements IWebContext {
	
	public String contextPath;
	
	public CustomContext(String contextPath, ApplicationContext applicationContext) {
		this.contextPath = contextPath;
		setVariable(ThymeleafEvaluationContext.THYMELEAF_EVALUATION_CONTEXT_CONTEXT_VARIABLE_NAME, new ThymeleafEvaluationContext(applicationContext, applicationContext.getBean(ConversionService.class)));
	}

	@Override
	public HttpServletRequest getHttpServletRequest() {
		return (HttpServletRequest) Proxy.newProxyInstance(CustomContext.class.getClassLoader(), new Class<?>[]{HttpServletRequest.class}, new InvocationHandler() {
			@Override
			public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
				if (method.getName().equals("getContextPath")) {
					return contextPath;
				}
				return null;
			}
		});
	}

	@Override
	public HttpServletResponse getHttpServletResponse() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public HttpSession getHttpSession() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ServletContext getServletContext() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariablesMap<String, String[]> getRequestParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariablesMap<String, Object> getRequestAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariablesMap<String, Object> getSessionAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VariablesMap<String, Object> getApplicationAttributes() {
		// TODO Auto-generated method stub
		return null;
	}

}
