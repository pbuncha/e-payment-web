package th.go.cgd.epayment.config.thymeleaf.dialect;

import org.springframework.stereotype.Component;
import org.thymeleaf.Arguments;
import org.thymeleaf.dom.Element;
import org.thymeleaf.dom.Node;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.processor.AbstractProcessor;
import org.thymeleaf.processor.AttributeNameProcessorMatcher;
import org.thymeleaf.processor.IProcessorMatcher;
import org.thymeleaf.processor.ProcessorMatchingContext;
import org.thymeleaf.processor.ProcessorResult;
import org.thymeleaf.spring4.processor.attr.AbstractSpringFieldAttrProcessor;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.standard.processor.attr.StandardObjectAttrProcessor;

@Component
public class AutoValidationAttributeProcessor extends AbstractProcessor {
	
	@Override
	public IProcessorMatcher<? extends Node> getMatcher() {
		return new AttributeNameProcessorMatcher("autoRules");
	}

	@Override
	public int getPrecedence() {
		return StandardObjectAttrProcessor.ATTR_PRECEDENCE + 1; //after th:object
	}

	@Override
	protected ProcessorResult doProcess(Arguments arguments, ProcessorMatchingContext processorMatchingContext,
			Node node) {
		Element element = (Element)node;
		IStandardExpressionParser standardParser = StandardExpressions.getExpressionParser(arguments.getConfiguration());
		IStandardExpression exp = standardParser.parseExpression(arguments.getConfiguration(), arguments, element.getAttributeValueFromNormalizedName("ws", "autorules"));
		Object exc = exp.execute(arguments.getConfiguration(), arguments);
		if (exc instanceof Boolean) {
			Boolean enable = (Boolean) exc;
			if (enable)
				processElement(arguments, element, arguments.getExpressionSelectionEvaluationRoot());
			element.removeAttribute("ws:autoRules");
		} else {
			throw new TemplateProcessingException("ws:autoRules arguments must be a bool value");
		}
		return ProcessorResult.OK;
	}
	
	private void processElement(Arguments arguments, Element element, Object root) {
		String value = element.getAttributeValueFromNormalizedName("th",AbstractSpringFieldAttrProcessor.ATTR_NAME);
		if (value != null) processAttribute(arguments, element, root, value);
		for(Node node : element.getChildren()) {
			if (node instanceof Element) {
				Element ele = (Element)node;
				if (ele.getAttributeValueFromNormalizedName("ws", "autorules") != null) {
					continue;
				}
				processElement(arguments, ele, root);
			}
		}
	}
	
	private void processAttribute(Arguments arguments, Element element, Object root, String thField) {
		element.setAttribute("ws:autorule", false, thField, true);
		//element.setRecomputeProcessorsImmediately(true);
		element.setProcessable(true);
	}
}
