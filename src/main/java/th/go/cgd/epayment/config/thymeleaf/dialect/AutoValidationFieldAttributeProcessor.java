package th.go.cgd.epayment.config.thymeleaf.dialect;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.apache.log4j.Logger;
import org.hibernate.validator.constraints.Email;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.expression.EvaluationException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.NumberFormat;
import org.springframework.stereotype.Component;
import org.thymeleaf.Arguments;
import org.thymeleaf.dom.Element;
import org.thymeleaf.dom.Node;
import org.thymeleaf.exceptions.TemplateProcessingException;
import org.thymeleaf.processor.AbstractProcessor;
import org.thymeleaf.processor.AttributeNameProcessorMatcher;
import org.thymeleaf.processor.IProcessorMatcher;
import org.thymeleaf.processor.ProcessorMatchingContext;
import org.thymeleaf.processor.ProcessorResult;
import org.thymeleaf.standard.expression.IStandardExpression;
import org.thymeleaf.standard.expression.IStandardExpressionParser;
import org.thymeleaf.standard.expression.SelectionVariableExpression;
import org.thymeleaf.standard.expression.StandardExpressions;
import org.thymeleaf.standard.processor.attr.StandardObjectAttrProcessor;

@Component
public class AutoValidationFieldAttributeProcessor extends AbstractProcessor{
	private Logger logger = Logger.getLogger(this.getClass());

	@Value("${debug}")
	private static boolean isDebug = false;

	@Override
	public IProcessorMatcher<? extends Node> getMatcher() {
		return new AttributeNameProcessorMatcher("autorule");
	}
	
	@Override
	public int getPrecedence() {
		return StandardObjectAttrProcessor.ATTR_PRECEDENCE + 1; //after th:object
	}
	
	@Override
	protected ProcessorResult doProcess(Arguments arguments, ProcessorMatchingContext processorMatchingContext,
			Node node) {
		Element element = (Element)node;
		String thField = element.getAttributeValueFromNormalizedName("ws","autorule");
		try {
			processAttribute(arguments, element, arguments.getExpressionSelectionEvaluationRoot(), thField);
			element.removeAttribute("ws:autorule");
		} catch(Exception e) {
			IStandardExpressionParser standardParser = StandardExpressions.getExpressionParser(arguments.getConfiguration());
			IStandardExpression exp1 = standardParser.parseExpression(arguments.getConfiguration(), arguments, thField);
			final String expString = ((SelectionVariableExpression)exp1).getExpression();
			new IllegalArgumentException(String.format("Error during processing line %d autorule %s \n", node.getLineNumber(), expString), e).printStackTrace(System.err);
			throw e;
		}
		return ProcessorResult.OK;
	}
	
	protected void processTypeDescriptor(TypeDescriptor type, Element element, Locale locale, Warner warner) {
		Class<?> oType = type.getObjectType();
		if (null != type.getAnnotation(Size.class)) {
			Size size = type.getAnnotation(Size.class);
			setOptionalAttribute(element, "minlength", String.valueOf(size.min()));
			setOptionalAttribute(element, "maxlength", String.valueOf(size.max()));
			warner.willWarn = false;
		}
		if (null != type.getAnnotation(Min.class)) {
			Min min = type.getAnnotation(Min.class);
			setOptionalAttribute(element, "min", String.valueOf(min.value()));
			warner.willWarn = false;
		}
		if (null != type.getAnnotation(Max.class)) {
			Max max = type.getAnnotation(Max.class);
			setOptionalAttribute(element, "min", String.valueOf(max.value()));
			warner.willWarn = false;
		}
		if (null != type.getAnnotation(Email.class)) {
			setOptionalAttribute(element, "email", "true");
			warner.willWarn = false;
		}
		if (oType == BigInteger.class || oType == Integer.class || oType == Integer.TYPE || oType == Long.class || oType == Long.TYPE || oType == Short.class || oType == Short.TYPE) {
			setOptionalAttribute(element, "digits", "true");
			setOptionalAttribute(element, "data-format", "integer");
			warner.willWarn = false;
		}
		if (oType == BigDecimal.class || oType == Float.class || oType == Float.TYPE || oType == Double.class || oType == Double.TYPE) {
			setOptionalAttribute(element, "number", "true");
			setOptionalAttribute(element, "data-format", "decimal");
			warner.willWarn = false;
		}
		if (null != type.getAnnotation(NumberFormat.class)) {
			NumberFormat nf = type.getAnnotation(NumberFormat.class);
//			Matcher matcher = Pattern.compile("(?:,|^)(#*)(?:\\.|$)(#*)").matcher(nf.pattern().replace('0', '#'));
//			if(matcher.find()){
//				if (!matcher.group(1).isEmpty()) {
//					setOptionalAttribute(element, "data-numeric-grouping-interval", String.valueOf(matcher.group(1).length()));
//				}
//				if (!matcher.group(2).isEmpty()) {
//					setOptionalAttribute(element, "data-decimal-scale", String.valueOf(matcher.group(2).length()));
//				}
//			}
//			setOptionalAttribute(element, "maxlength", String.valueOf(nf.pattern().length()));
			DecimalFormatSymbols decimalFormat = DecimalFormatSymbols.getInstance(locale);
			String pattern = nf.pattern();
			pattern = pattern.replace(".", "<!@.@!>");
			pattern = pattern.replace(",", "<!@,@!>");
			pattern = pattern.replace("<!@.@!>", String.valueOf(decimalFormat.getDecimalSeparator()));
			pattern = pattern.replace("<!@,@!>", String.valueOf(decimalFormat.getGroupingSeparator()));
			setOptionalAttribute(element, "data-mask-pattern", pattern);
			setOptionalAttribute(element, "maxlength", String.valueOf(pattern.length()));
//			setOptionalAttribute(element, "data-decimal-radix", String.valueOf(decimalFormat.getDecimalSeparator()));
//			setOptionalAttribute(element, "data-numeric-grouping-separator", String.valueOf(decimalFormat.getGroupingSeparator()));
			warner.willWarn = false;
		}
		if (null != type.getAnnotation(DateTimeFormat.class)) {
			// disable Date field warning.
			warner.willWarn = false;
		}
		if (warner.willWarn) {
			warner.warn();
		}
	}
	
	private void processAttribute(Arguments arguments, Element element, final Object root, String thField) {
		IStandardExpressionParser standardParser = StandardExpressions.getExpressionParser(arguments.getConfiguration());
		IStandardExpression exp1 = standardParser.parseExpression(arguments.getConfiguration(), arguments, thField);
		if (!(exp1 instanceof SelectionVariableExpression)) {
			throw new TemplateProcessingException(
	                "Expression \"" + exp1 + "\" is not valid: only selection expressions *{...} are allowed inside th:field while ws:autoRules is enabled");
		}
		final String expString = ((SelectionVariableExpression)exp1).getExpression();
		TypeDescriptor type;
		try {
			type = getOptionalTypeDescriptor(expString, root);
		} catch(EvaluationException e) {
			throw new RuntimeException("Error while get type of expression "+expString, e);
		}
		Warner warner = new Warner() {
			@Override
			void warn() {
				logger.warn(root.getClass().getName() + " property " + expString + " does not have any validation annotation. Is this intend?");
			}
		};
		warner.willWarn = !isDebug;
		if (type == null) {
			throw new RuntimeException("can't find type of " + expString);
		}
		processTypeDescriptor(type, element, arguments.getContext().getLocale(), warner);
	}
	
	private void setOptionalAttribute(Element element, String attrName, String value) {
		if (element.getAttributeValueFromNormalizedName(attrName) == null) {
			element.setAttribute(attrName, value == null, value);
		}
	}
	
	private static TypeDescriptor getOptionalTypeDescriptor(String exp, Object root) {
		TypeDescriptor typeDescriptor = TypeDescriptor.forObject(root);
		Pattern compile = Pattern.compile("([^.]+)");
		Matcher matcher = compile.matcher(exp);
		while (matcher.find()) {
			String childExp = matcher.group(1);
			typeDescriptor = new BeanWrapperImpl(typeDescriptor.getType()).getPropertyTypeDescriptor(childExp);
		} 
		return typeDescriptor;
	}
	
	protected abstract class Warner{
		public boolean willWarn;
		abstract void warn();
	}
}
