package th.go.cgd.epayment.config.thymeleaf.dialect;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.processor.IProcessor;

@Component
public class WisesoftDialect extends AbstractDialect{

	@Autowired
	AutoValidationAttributeProcessor autoValidationAttributeProcessor;
	
	@Autowired
	AutoValidationFieldAttributeProcessor autoValidationFieldAttributeProcessor;
	
	@Override
	public String getPrefix() {
		return "ws";
	}
	
	@Override
	public Set<IProcessor> getProcessors() {
		return new HashSet<IProcessor>(Arrays.asList(new IProcessor[]{autoValidationAttributeProcessor,autoValidationFieldAttributeProcessor}));
	}

}
