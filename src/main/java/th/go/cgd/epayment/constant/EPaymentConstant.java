package th.go.cgd.epayment.constant;

import java.math.BigDecimal;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Value;

public class EPaymentConstant 
{
    public static ResourceBundle bundleError = ResourceBundle.getBundle("ErrorMessage");
	public static final String PDF = "pdf";
	public static final String EXCEL = "xlsx";
	public static final String DOCX = "docx";
	
	public static String CONTENT_TYPE_PDF = "application/pdf";
	public static String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel";
	public static String CONTENT_TYPE_DOCX = "application/msword";
	public static String CONTENT_TYPE_ZIP = "application/zip";
	public static String CONTENT_TYPE_TEXT = "text/plain";
	
	public static final String STATUS_ACTIVE = "A";
	public static final String STATUS_INACTIVE = "I";
	public static final String YES = "Y";
	public static final String NO = "N";
	
	public static final String ADD = "ADD"; 
	public static final String EDIT = "EDIT"; 
	public static final String VIEW = "VIEW"; 
	
//  Session
	public static final String USER_SESSION = "userSession";
	
//	LDAP
	public static final Integer DEFAULT_ROLE = 10015;
	public static final Integer[] DEFAULT_MENU_LIST = new Integer[]{2001,2002,2003,2007,5000}; 
	
//	Catalog Mapping
	public static final String M_REPORT = "mReport";
//	public static final String M_PARAM = "mParam";
	
//	Report
	public static final String RPT_E01 = "RPT_E01";
	
//	Upload File Extension
	public static final String UPLOAD_FILE_EXTENSION_LIST = ".pdf,.xls,.xlsx,.doc,.docx,.one,.pub,.pptx,.vsdx,.accdb,.mdb,.oft,.mpp,.olm,.vsd,.ppt,.png,.jpg,.jpeg,.gif,.bmp,.svg";
	
//	EMail Template
	public static final String EMAIL_TEMPLATE_1 = "email-1";
	
//	Menu
	public static final String DEFAULT_ICON = "glyphicon glyphicon-unchecked";
	public static final String MENU_SELECTED = "active";
	public static final String MENU = "menu";
	public static final String MENU_FLOW = "menuFlow";
	
//	Common Master
	public static final String M_CASE_PERMISSION = "MCasePermission";
	public static final String M_COURT = "MCourt";
	public static final String M_COURT_TYPE = "MCourtType";
	public static final String M_CURRENCY = "MCurrency";
	public static final String M_DOCUMENT_TEMPLATE = "MDocumentTemplate";
	public static final String M_EXPIRY_TYPE = "MExpiryType";
	public static final String M_FORM = "MForm";
	public static final String M_FORM_VI = "MFormVI";
	public static final String M_FORM_CI = "MFormCI";
	public static final String M_FORM_ED = "MFormED";
	public static final String M_FORM_DE = "MFormDE";
	public static final String M_FORM_STATUS = "MFormStatus";
	public static final String M_LAWSUIT_CASE_DECIDED = "MLawsuitCaseDecided";
	public static final String M_LAWSUIT_COMPROMISE_RESULT = "MLawsuitCompromiseResult";
	public static final String M_LAWSUIT_PERSON_DECIDED = "MLawsuitPersonDecided";
	public static final String M_LITIGANTTYPE = "MLitigantType";
	public static final String M_MARITAL_STATUS = "MMaritalStatus";
	public static final String M_MESSAGE_TEMPLATE = "MMessageTemplate";
	public static final String M_MODULE = "MModule";
	public static final String M_MONTH = "MMonth";
	public static final String M_ORGTYPE = "MOrgType";
	public static final String M_PERSONTYPE = "MPersonType";
	public static final String M_PROVINCE = "MProvince";
	public static final String M_RELATIONSHIP = "MRelationship";
	public static final String M_SEND_CASE_TYPE = "MSendCaseType";
	public static final String M_TITLE_NAME = "MTitleName";
	public static final String M_USER_STATUS = "MUserStatus";
	public static final String M_NEWS_STATUS = "MNewsStatus";
	public static final String M_PARAM = "MParam";
	public static final String M_COUNTRY = "MCountry";
	public static final String M_STATUS = "MStatus";
	public static final String M_FLOWCHART = "MFLowchart";
	public static final String M_USER_ROLE = "MUserRole";
	public static final String M_USER_ACCESS_PERMISSION = "MUserAccessPermission";
	public static final String M_CASE_REPORT_RESULT = "MCaseReportResult";
	public static final String M_EMAIL_TEMPLATE = "MEmailTemplate";
	public static final String M_NOTIFICATION_TYPE = "MNotificationType";
	public static final String M_LOG_ACTION = "MLogAction";
	public static final String M_TASK_STATUS = "MTaskStatus";
	public static final String M_PERMISSION_ROLE = "MPermissionRole";
	public static final String M_DISTRICT = "MDistrict";
	public static final String M_SUB_DISTRICT = "MSubDistrict";
	
//	Master for Violation
	public static final String M_VIOLATE_ACCOMPLICE_TYPE = "MViolationAccompliceType";
	public static final String M_VIOLATE_APPEAL_RESULT = "MViolationAppealResult";
	public static final String M_VIOLATE_APPOINTMENT_ORG_TYPE = "MViolationAppointmentOrgType";
	public static final String M_VIOLATE_CASE_TYPE = "MViolationCaseType";
	public static final String M_VIOLATE_CLOSE_CASE_REASON = "MViolationCloseCaseReason";
	public static final String M_VIOLATION_INQUIRY_RESULT = "MViolationInquiryResult";
	public static final String M_VIOLATION_ORDERING_TYPE = "MViolationOrderingType";
	public static final String M_VIOLATE_PRE_INVESTIGATE_INQUIRY = "MViolationPreinvestigateInquiry";
	public static final String M_VIOLATION_PRESCRIPTION_YEAR = "MViolationPrescriptionYear";
	
//	Master for Civil
	public static final String M_CIVIL_ABTT_LITIGANTTYPE = "MCivilAbttLitigantType";
	public static final String M_CIVIL_ARB_MEDITATION_RESULT = "MCivilArbitratorMeditationResult";
	public static final String M_CIVIL_CASE_TYPE = "MCivilCaseType";
	public static final String M_CIVIL_CLOSE_CASE_REASON = "MCivilCloseCaseReason";
	public static final String M_CIVIL_COMPENSATION_TYPE = "MCivilCompensationType";
	public static final String M_CIVIL_MEDITATION_RESULT = "MCivilMeditationResult";
	public static final String M_CIVIL_PRESCRIPTION_YEAR = "MCivilPrescriptionYear";

// Master for All Status
	public static final String M_PROVINCE_ALL_STATUS = "MProvinceAllStatus";
	public static final String M_DISTRICT_ALL_STATUS = "MDistrictAllStatus";
	public static final String M_SUB_DISTRICT_ALL_STATUS = "MSubDistrictAllStatus";
	public static final String M_CURRENCY_ALL_STATUS = "MCurrencyAllStatus";
	public static final String M_RELATIONSHIP_ALL_STATUS = "MRelationshipAllStatus";
	public static final String M_VIOLATE_CASE_TYPE_ALL_STATUS = "MViolationCaseTypeAllStatus";
	public static final String M_VIOLATION_PRESCRIPTION_YEAR_ALL_STATUS = "MViolationPrescriptionYearAllStatus";
	public static final String M_CIVIL_PRESCRIPTION_YEAR_ALL_STATUS = "MCivilPrescriptionYearAllStatus";
	public static final String M_EDUCATION_CASE_TYPE_ALL_STATUS = "MEducationCaseTypeAllStatus";
	public static final String M_EDUCATION_LEVEL_ALL_STATUS = "MEducationLevelAllStatus";
	public static final String M_EDUCATION_TYPE_ALL_STATUS = "MEducationTypeAllStatus";
	public static final String M_COUNTRY_ALL_STATUS = "MCountryAllStatus";
	
// Master for Education
	public static final String M_EDUCATION_CASE_TYPE = "MEducationCaseType";
	public static final String M_EDUCATION_CLOSE_CASE_REASON = "MEducationCloseCaseReason";
	public static final String M_EDUCATION_CONTRACT_TYPE = "MEducationContractType";
	public static final String M_EDUCATION_EXTENSTION_DECISION_RESULT = "MEducationExtensionDecisionResult";
	public static final String M_EDUCATION_LEAVE_TYPE = "MEducationLeaveType";
	public static final String M_EDUCATION_LEVEL = "MEducationLevel";
	public static final String M_EDUCAITON_LIABILITY_TYPE = "MEducationLiabilityType";
	public static final String M_EDUCATION_PERSONAL_TYPE = "MEducationPersonalType";
	public static final String M_EDUCATION_RELATIONSHIP = "MEducationRelationship";
	public static final String M_EDUCATION_REPLACE_TERM = "MEducationReplaceTerm";
	public static final String M_EDUCATION_REPLACE_TYPE = "MEducationReplaceType";
	public static final String M_EDUCATION_SCHOLARSHIP_TYPE = "MEducationScholarshipType";
	public static final String M_EDUCATION_TYPE = "MEducationType";
	public static final String M_EDUCATION_EXPENSE_TYPE = "MEducationExpenseType";
	
// Master for Education
	public static final String M_DEBT_ASSET_TRACKING_RESULT = "MDebtAssetTrackingResult";
	public static final String M_DEBT_BANKRUPT_CASE_DECIDED = "MDebtBankruptCaseDecided";
	public static final String M_DEBT_BANKRUPT_COMPROMISE_RESULT = "MDebtBankruptCompromiseResult";
	public static final String M_DEBT_BANKRUPT_RECEIVING_ORDER = "MDebtBankruptReceivingOrder";
	public static final String M_DEBT_CLOSE_CASE_REASON = "MDebtCloseCaseReason";
	public static final String M_DEBT_DECISION_RESULT = "MDebtDecisionResult";
	public static final String M_DEBT_INSTALLMENT_PERIOD = "MDebtInstallmentPeriod";
	public static final String M_DEBT_INSTALLMENT_PLAN = "MDebtInstallmentPlan";
	public static final String M_DEBT_PAYMENT_TYPE = "MDebtPaymentType";
	public static final String M_DEBT_STATUS = "MDebtStatus";
	public static final String M_DEBT_TYPE = "MDebtType";
	public static final String M_DEBT_WRITE_OFF_DECISION_RESULT = "MDebtWriteOffDecisionResult";
	public static final String M_DEBT_WRITE_OFF_REASON = "MDebtWriteOffReason";
	public static final String M_DEBT_COST_TYPE = "MDebtCostType";
	public static final String M_DEBT_AMOUNT_TYPE = "MDebtAmountType";
	
	public static final int SELECT2_PAGE_LIMIT = 8;
	
	public static interface ACCOMPLICE_TYPE_NAME {
		final String TROUBLEMAKER   = "Troublemaker";
		final String COMMANDER      = "Commander";
		final String RELATED_PERSON = "RelatedPerson";
	}
	
	public static interface ACCOMPLICE_TYPE_ID {
		final short TROUBLEMAKER   = 1;
		final short COMMANDER      = 2;
		final short RELATED_PERSON = 3;
	}
	
	public static interface STATUS {
		final String DRAFT        = "D";
		final String SAVE         = "S";
		final String SEND_APPROVE = "W";
		final String APPROVE 	  = "A";
		final String REJECT 	  = "R";
		final String DELETE 	  = "DE";
	}
	
	public static interface LITIGANT_TYPE_NAME{
		final String CLAIMANT = "Claimant";
		final String OBJECTOR = "Objector";
	}
	
	public static interface LITIGANT_TYPE_ID{
		final short CLAIMANT = 1;
		final short OBJECTOR = 2;
	}
	
	public static int DOWNLOAD_SESSION_EXPIRY = 0;
	
	public static interface PERMISSION_ROLE{
		final String PERMISSION_ROLE_1 = "PermissionRole1";
		final String PERMISSION_ROLE_2 = "PermissionRole2";
	}
	
	public static interface PERMISSION_ROLE_ID{
		final short GENERAL_USER = 1;
		final short TCLD_USER = 2;
	}
	
	public static interface MODULE_ID{
		final String CIVIL = "CI";
		final String VIOLATION = "VI";
		final String DEBT = "DE";
		final String EDUCATION = "ED";
	}
	public static interface AMOUNT_COURTTYPE{
		final BigDecimal TWOMILLION = new BigDecimal(20000000) ;
		final BigDecimal TENMILLION = new BigDecimal(100000000);
		
	}
	    @Value("${barcode.fix}")
	    private static String barcodeFix;
	public static interface BRACODE_DATA{
	    final String DATA_INVOICE = "|0994000159510"+barcodeFix;
	}
	
	public static final Character STATUS_IS_DELETE = '0';
	
	public static class REPORT_TYPE{
		public static String getReportType( String type ){
			switch (type) {
			case "PDF":
				return "application/pdf"; 
			case "EXCEL": 
				return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
			default:
				return "";
			}
		}
		
	}
	
	public static final String []  STATUS_ID = {"0","1","2"};
	
	public static interface INVOICE_STATUS {
		final String WAIT_PAY = "0";
		final String CONFIRM_PAY = "1";
		final String CANCEL = "3";
		final String PAYMENT_ONLINE = "5";
		final String PAYMENT_FAIL = "6";
	}
	
	public static interface CATALOG_STATUS {
		final String ACTIVE = "1";
		final String CANCEL = "N";
	}
	
	public static interface BILL_STATUS {
		final String WAIT_PAY = "0";
	}
	
	public static interface USER_PERMISSION_GROUP_STATUS {
	    	final String IN_ACTIVE = "0";
		final String ACTIVE = "1";
		final String CANCEL = "N";
	}
	
	public static interface BILL_GENERATE {
		final String BILL_PRINT_TYPE = "1";
		final String BILL_PRINT_PROTOTYPE = "00";
		
	}
	
}
