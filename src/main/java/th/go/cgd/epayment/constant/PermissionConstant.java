package th.go.cgd.epayment.constant;

public class PermissionConstant 
{
	public static final short CIVIL_ACTIVE = 201;
	public static final short CIVIL_ADD = 202;
	public static final short CIVIL_DELETE = 203;
	public static final short CIVIL_ADMIN = 204;
	
	public static final short VIOLATION_ACTIVE = 101;
	public static final short VIOLATION_ADD = 102;
	public static final short VIOLATION_DELETE = 103;
	public static final short VIOLATION_ADMIN = 104;
	
	public static final short EDUCATION_ACTIVE = 301;
	public static final short EDUCATION_ADD = 302;
	public static final short EDUCATION_DELETE = 303;
	public static final short EDUCATION_ADMIN = 304;
	
	public static final short DEBT_ACTIVE = 401;
	public static final short DEBT_ADD = 402;
	public static final short DEBT_DELETE = 403;
	public static final short DEBT_ADMIN = 404;
	
	public interface REPORT {
		final short V1 = 539;
		final short V2 = 540;
		final short V3 = 541;
		final short C1 = 542;
		final short E1 = 543;
		final short E2 = 544;
		final short E3 = 545;
		final short E4 = 546;
		final short E5 = 547;
		final short D3 = 550;
		final short D4 = 551;
		final short D5 = 552;
		final short D6 = 553;
		final short D12 = 548;
		final short I1 = 528;
		final short I2 = 529;
		final short I3 = 530;
		final short I4 = 531;
		final short I5 = 532;
		final short I6 = 533;
		final short I7 = 534;
		final short I8 = 535;
		final short I9 = 536;
		final short I10 = 537;
		final short I11 = 538;
		final short O1 = 554;
	}
	
	public interface TASK {
		final short KM_READ = 608;
		final short KM_VIEW = 609;
		final short KM_ADD = 610;
		final short KM_EDIT = 611;
		final short KM_DELETE = 612;
		final short KM_TYPE = 613;
	}
	
	public static interface ADMIN{
		final short ORGANIZATION_VIEW = 710;
		final short ORGANIZATION_ADD = 711;
		final short ORGANIZATION_EDIT = 712;
		final short ORGANIZATION_DELETE = 713;
		
		final short PERMISSION_GROUP_VIEW = 714;
		final short PERMISSION_GROUP_ADD = 715;
		final short PERMISSION_GROUP_EDIT = 716;
		final short PERMISSION_GROUP_DELETE = 717;
		
		final short USER_VIEW = 718;
		final short USER_ADD = 719;
		final short USER_EDIT = 720;
		final short USER_DELETE = 721;
		final short USER_CHANGE_PASSWORD = 722;
		
		final short APPROVE_CONFIG_VIEW = 723;
		final short APPROVE_CONFIG_EDIT = 724;
		
		final short NEWS_VIEW = 725;
		final short NEWS_ADD = 726;
		final short NEWS_EDIT = 727;
		final short NEWS_DELETE = 728;
		final short NEWS_TYPE = 729;
		
		final short MASTER_DATA_VIEW = 730;
		final short MASTER_DATA_ADD = 731;
		final short MASTER_DATA_EDIT = 732;
		final short MASTER_DATA_DELETE = 733;
		
		final short PARAM_VIEW = 734;
		final short PARAM_EDIT = 735;
		
		final short DOCUMENT_TEMPLATE_VIEW = 736;
		final short DOCUMENT_TEMPLATE_EDIT = 737;
		
		final short LOG_ACCESS_VIEW = 738;

		final short PROGRAM_VIEW = 739;
		final short PROGRAM_ADD = 740;
		final short PROGRAM_EDIT = 742;
		final short PROGRAM_DELETE = 743;
		
	}
	
	public static interface ACTION_MODE{
		final String ADD = "add";
		final String EDIT = "edit";
		final String DELETE = "delete";
		final String VIEW = "view";
		final String APPROVE = "approve";
	}
	
	public static final short TASK_VIEW = 604;
}
