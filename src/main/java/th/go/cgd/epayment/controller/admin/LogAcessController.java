package th.go.cgd.epayment.controller.admin;



import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import th.go.cgd.epayment.annotation.ActionPermission;
import th.go.cgd.epayment.constant.PermissionConstant;
import th.go.cgd.epayment.model.DataTable;
import th.go.cgd.epayment.model.DataTableSortable;
import th.go.cgd.epayment.model.LogAccessDataTableBean;
import th.go.cgd.epayment.model.LogAccessSearchBean;
import th.go.cgd.epayment.repository.SimpleDataTableRepository;
import th.go.cgd.epayment.util.SqlWhereCauseBuilder;

@Controller
@RequestMapping("/admin/log-access")
public class LogAcessController {
	
	private static final String LOG_ACESS_LIST = "/views/admin/log-access/log-access-list";
	private static final String ALL_ACTION = "all";
	
	@Autowired
	private SimpleDataTableRepository simpleDataTableRepository;
	
	@ActionPermission(PermissionConstant.ADMIN.LOG_ACCESS_VIEW)
	@RequestMapping()
	public String index(Model model) {
		model.addAttribute("logAccessSearchBean", new LogAccessSearchBean());
		return LOG_ACESS_LIST;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.LOG_ACCESS_VIEW)
	@RequestMapping("/search")
	public ResponseEntity<DataTable<LogAccessDataTableBean>> search(@ModelAttribute("logAccessSearchBean") LogAccessSearchBean searchBean) throws Exception
	{
		String sql = simpleDataTableRepository.getNamedQuery("logAccessDataTable");
		SqlWhereCauseBuilder builder = SqlWhereCauseBuilder.getInstance(false);
		if (StringUtils.isNotBlank(searchBean.getUserName())) {
			builder.append("la.USER_NAME like :username", "username", "%" + searchBean.getUserName() + "%");
		}
		if (!searchBean.getAction().equals(ALL_ACTION)) {
			builder.append("mla.ACTION_ID = :action", "action", searchBean.getAction());
		}
		
		if (searchBean.getStartDate() != null) {
			Date startDate = searchBean.getStartDate();
			if (StringUtils.isNotBlank(searchBean.getStartTime())) {
				startDate = setHourAndMinute(startDate, searchBean.getStartTime());
			}
			
			builder.append("la.ACCESS_DATE > :startDate", "startDate", startDate);
		}
		
		if (searchBean.getEndDate() != null) {
			Date endDate = searchBean.getEndDate();
			if (StringUtils.isNotBlank(searchBean.getEndTime())) {
				endDate = setHourAndMinute(endDate, searchBean.getEndTime());
			}
			
			builder.append("la.ACCESS_DATE < :endDate", "endDate", endDate);
		}
		
		if (StringUtils.isNotBlank(searchBean.getUrl())) {
			builder.append("la.URL like :url", "url", "%" + searchBean.getUrl() + "%");
		}
		if (StringUtils.isNotBlank(searchBean.getIpAddress())) {
			builder.append("la.CLIENT_IP like :ipAddress", "ipAddress", "%" + searchBean.getIpAddress() + "%");
		}
		DataTable<LogAccessDataTableBean> pagingData = simpleDataTableRepository.getPagingData(builder.appendTo(sql), DataTableSortable.getRequestSortable(), builder.getParameterMap(), LogAccessDataTableBean.class);
		return new ResponseEntity<DataTable<LogAccessDataTableBean>>(pagingData, HttpStatus.OK);
	}
	
	private Date setHourAndMinute(Date date, String hourAndMinute) {
		String[] splitString = hourAndMinute.split(":");
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitString[0]));
		cal.set(Calendar.MINUTE, Integer.parseInt(splitString[1]));
		return cal.getTime();
	}
	
}
