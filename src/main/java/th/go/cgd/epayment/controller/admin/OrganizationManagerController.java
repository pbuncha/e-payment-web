package th.go.cgd.epayment.controller.admin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.cgd.epayment.annotation.ActionPermission;
import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.constant.PermissionConstant;
import th.go.cgd.epayment.entity.MStatus;
import th.go.cgd.epayment.entity.Organization;
import th.go.cgd.epayment.model.DataTable;
import th.go.cgd.epayment.model.DataTableSortable;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.SimpleDataTableRepository;
import th.go.cgd.epayment.service.admin.OrganizationService;
import th.go.cgd.epayment.util.CatalogUtil;
import th.go.cgd.epayment.util.SqlWhereCauseBuilder;
import th.go.cgd.epayment.util.datatable.TransformIgnore;
import th.go.cgd.epayment.util.datatable.TransformTarget;

@Controller
@RequestMapping("/admin/organization")
public class OrganizationManagerController {
	
	private static final String ORGANIZATION_VIEW = "/views/admin/organization/organization-search";
	private static final String ORGANIZATION_FORM = "/views/admin/organization/organization-form";
	private static final String ORGANIZATION_TABLE = "/views/admin/organization/organization-table";
	
	private static final String ADD = "ADD";
	private static final String EDIT = "EDIT";
	private static final String VIEW = "VIEW";
	private static final String MODE = "mode";
	
	private static final String ACTIVE = "A";
	
	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private SimpleDataTableRepository simpleDataTableRepository;
	
	@Autowired
	private CatalogUtil catalogUtil;
	
	@TransformIgnore(needAllSource=true, needAllTarget=true)
	private static class OrganizationDataTableBean {
		@TransformTarget("ORG_ID")
		public int orgId;
		@TransformTarget("ORG_NAME")
		public String orgName;
		@TransformTarget("ORG_TYPE_NAME")
		public String orgTypeName;
		@TransformTarget("STATUS_NAME")
		public String statusName;
		@TransformTarget("HAS_CHILD")
		public String hasChild;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.ORGANIZATION_VIEW)
	@RequestMapping()
	public String index(Model model) throws Exception {
		return ORGANIZATION_VIEW;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.ORGANIZATION_ADD)
	@RequestMapping("/add")
	public String add(
			@ModelAttribute(value="organizationObj") Organization organization,
			BindingResult errors,
			@ModelAttribute(value="error") String error,
			Model model) throws Exception {
		
		if (StringUtils.isBlank(error))  {
//			organization.setMStatus((MStatus) catalogUtil.getObject(EPaymentConstant.M_STATUS, ACTIVE));
		} else {
			errors.reject("invalid.organization.name",new Object[]{},"Cannot Save Organization");
			model.addAttribute(BindingResult.MODEL_KEY_PREFIX+"organizationObj",errors);
		}
		
		model.addAttribute(MODE, ADD);
		model.addAttribute("MStatus", catalogUtil.get(EPaymentConstant.M_STATUS));
		
		return ORGANIZATION_FORM;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.ORGANIZATION_EDIT)
	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable(value="id") int orgId,
			@ModelAttribute(value="organizationObj") Organization organization,
			BindingResult errors,
			@ModelAttribute(value="error") String error,
			Model model) throws Exception {
		
		if (StringUtils.isBlank(error)) {			
			model.addAttribute("organizationObj", organizationService.findOne(orgId));
		} else {
			errors.reject("invalid.organization.name",new Object[]{},"Cannot Save Organization");
			model.addAttribute(BindingResult.MODEL_KEY_PREFIX+"organizationObj",errors);
		}
		
		model.addAttribute(MODE, EDIT);
		model.addAttribute("MStatus", catalogUtil.get(EPaymentConstant.M_STATUS));
		return ORGANIZATION_FORM;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.ORGANIZATION_VIEW)
	@RequestMapping("/view/{id}")
	public String view(@PathVariable(value="id") int orgId, Model model) throws Exception {
		model.addAttribute(MODE, VIEW);
		model.addAttribute("organizationObj", organizationService.findOne(orgId));
		return ORGANIZATION_FORM;
	}
	
	@ActionPermission({PermissionConstant.ADMIN.ORGANIZATION_ADD, PermissionConstant.ADMIN.ORGANIZATION_EDIT})
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public String save(
			@ModelAttribute(value="organizationObj") Organization organization, 
			UserSession userSession,
			RedirectAttributes redirectAttributes) throws Exception {
		
		Organization orgCheck = null;
		String url;
		
		if (organization.getOrgId() == 0) {
			
//			orgCheck = organizationService.findByOrgName(organization.getOrgName());
			url = "redirect:/admin/organization/add";
			
		} else {
//			orgCheck = organizationService.findByOrgNameAndOrgIdNot(organization.getOrgName(), organization.getOrgId());
			url = "redirect:/admin/organization/edit/" + organization.getOrgId();
		}
		
		if (orgCheck != null) {
			redirectAttributes.addFlashAttribute("error", "error");
			redirectAttributes.addFlashAttribute("organizationObj", organization);
			return url;
		}
		
		
		organizationService.save(organization, userSession);
		return "redirect:/admin/organization";
	}
	
	@ActionPermission(PermissionConstant.ADMIN.ORGANIZATION_DELETE)
	@ResponseBody
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public boolean delete(@RequestParam(value="deleteId") List<Integer> id) throws Exception {
		List<Organization> organizations = new ArrayList<Organization>();
		for (Integer i : id) {
			organizations.add(organizationService.findOne((int) i));
		}
		boolean canDelete = false;
		try {
			organizationService.delete(organizations);
			canDelete = true;
		} catch (Exception e) {
			canDelete = false;
		}
		return canDelete;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.ORGANIZATION_EDIT)
	@ResponseBody
	@RequestMapping(value="/move", method=RequestMethod.POST)
	public boolean move(@RequestParam(value="moveId") List<Integer> moveId, @RequestParam(value="moveToId", required=false) Integer moveToId) throws Exception {
		boolean canMove;
		Organization organizationMoveTo = null;
		if (moveToId != null) {			
			organizationMoveTo = organizationService.findOne((int) moveToId);
		}
		List<Organization> organizationList = new ArrayList<Organization>();
		for (Integer id : moveId) {
			organizationList.add(organizationService.findOne((int) id));
		}
		try {
			canMove = organizationService.move(organizationMoveTo, organizationList);
		} catch (Exception e) {
			canMove = false;
		}
		
		return canMove;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.ORGANIZATION_VIEW)
	@RequestMapping("/dataTable")
	public ResponseEntity<DataTable<OrganizationDataTableBean>> dataTable(
			@RequestParam(value="orgId", required=false) Integer orgId,
			@RequestParam(value="orgSearchId", required=false) Integer orgSearchId) {
		String sql = simpleDataTableRepository.getNamedQuery("organizationInitDataTable");
		SqlWhereCauseBuilder builder = SqlWhereCauseBuilder.getInstance(false);
		String query = "";
		if (orgId != null || orgSearchId != null) {
			if (orgId != null) {
				builder.append("o.parent_id = :parentId", "parentId", orgId);
			} else if (orgSearchId != null) {
				builder.append("o.org_id = :orgSearchId", "orgSearchId", orgSearchId);
			}
			query = builder.appendTo(sql);
		} else {
			query = builder.appendTo(sql) + " WHERE o.parent_id is null";
		}
		DataTable<OrganizationDataTableBean> pagingData = simpleDataTableRepository.getPagingData(query, DataTableSortable.getRequestSortable(), builder.getParameterMap(), OrganizationDataTableBean.class);
		return new ResponseEntity<DataTable<OrganizationDataTableBean>>(pagingData, HttpStatus.OK);
	}
	
	@ActionPermission(PermissionConstant.ADMIN.ORGANIZATION_VIEW)
	@RequestMapping(value="/getTable", method=RequestMethod.POST)
	public String getTable(
			@RequestParam(value="orgId", required=false) Integer orgId,
			@RequestParam(value="action") String action,
			Model model) throws Exception {
		if (orgId != null) {
//			model.addAttribute("organizationRoute", getOrganizationList(organizationService.findOne((int) orgId)));
		}
		model.addAttribute("action", action);
		return ORGANIZATION_TABLE;
	}
	
//	private List<Organization> getOrganizationList(Organization organization) throws Exception {
//		List<Organization> organizationList = new ArrayList<Organization>();
//		organizationList.add(organization);
//		do {
//			if (organization.getOrganization() != null) {
//				organization = organization.getOrganization();
//				organizationList.add(organization);
//			}
//		} while (organization.getOrganization() != null);
//		Collections.reverse(organizationList);
//		return organizationList;
//	}

}
