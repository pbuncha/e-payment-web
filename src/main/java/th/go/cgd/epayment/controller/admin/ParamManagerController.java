package th.go.cgd.epayment.controller.admin;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.cgd.epayment.annotation.ActionPermission;
import th.go.cgd.epayment.constant.PermissionConstant;
import th.go.cgd.epayment.entity.MParam;
import th.go.cgd.epayment.entity.Organization;
import th.go.cgd.epayment.model.DataTable;
import th.go.cgd.epayment.model.DataTableSortable;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.SimpleDataTableRepository;
import th.go.cgd.epayment.service.admin.ParamService;
import th.go.cgd.epayment.util.SqlWhereCauseBuilder;
import th.go.cgd.epayment.util.datatable.TransformIgnore;
import th.go.cgd.epayment.util.datatable.TransformTarget;

@Controller
@RequestMapping("/admin/param")
public class ParamManagerController {

	private static final String PARAM_VIEW = "/views/admin/param/param-search";
	private static final String PARAM_FORM = "/views/admin/param/param-form";
	private static final String PARAM_TABLE = "/views/admin/param/param-table";
	
	private static final String ADD = "ADD";
	private static final String EDIT = "EDIT";
	private static final String VIEW = "VIEW";
	private static final String MODE = "mode";
	
	private static final String ACTIVE = "A";
	
	@Autowired
	private ParamService paramService;
	
	@Autowired
	private SimpleDataTableRepository simpleDataTableRepository;
	
	@ActionPermission(PermissionConstant.ADMIN.PARAM_VIEW)
	@RequestMapping()
	public String index(Model model) throws Exception {
		return PARAM_VIEW;
	}
	
	@TransformIgnore(needAllSource=true, needAllTarget=true)
	private static class ParamDataTableBean {
		@TransformTarget("NAME")
		public String name;
		@TransformTarget("VALUE")
		public String value;
		@TransformTarget("DESC")
		public String desc;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.PARAM_VIEW)
	@RequestMapping("/dataTable")
	public ResponseEntity<DataTable<ParamDataTableBean>> dataTable(
			@RequestParam(value="name", required=false) String name) {
		String sql = simpleDataTableRepository.getNamedQuery("mParamInitDataTable");
		SqlWhereCauseBuilder builder = SqlWhereCauseBuilder.getInstance(false);
		String query = "";
		if (name != null) {
			builder.append("o.name = :name", "name", name);
			
			query = builder.appendTo(sql);
		} else {
			query = builder.appendTo(sql);
		}
		DataTable<ParamDataTableBean> pagingData = simpleDataTableRepository.getPagingData(query, DataTableSortable.getRequestSortable(), builder.getParameterMap(), ParamDataTableBean.class);
		return new ResponseEntity<DataTable<ParamDataTableBean>>(pagingData, HttpStatus.OK);
	}
	
	@ActionPermission(PermissionConstant.ADMIN.PARAM_VIEW)
	@RequestMapping(value="/getTable", method=RequestMethod.POST)
	public String getTable(
			@RequestParam(value="name", required=false) String name,
			@RequestParam(value="action") String action,
			Model model) throws Exception {
		if (name != null) {
			model.addAttribute("organizationRoute", paramService.findByName(name));
		}
		model.addAttribute("action", action);
		return PARAM_TABLE;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.PARAM_VIEW)
	@RequestMapping("/view/{name}")
	public String view(@PathVariable(value="name") String name, Model model) throws Exception {
		model.addAttribute(MODE, VIEW);
		model.addAttribute("paramObj", paramService.findByName(name));
		return PARAM_FORM;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.PARAM_VIEW)
	@RequestMapping("/edit/{name}")
	public String edit(@PathVariable(value="name") String name,
			@ModelAttribute(value="paramObj") MParam mParam,
			BindingResult errors,
			@ModelAttribute(value="error") String error,
			Model model) throws Exception {
		
		if (StringUtils.isBlank(error)) {			
			model.addAttribute("paramObj", paramService.findByName(name));
		} else {
			errors.reject("invalid.organization.name",new Object[]{},"Cannot Save MParam");
			model.addAttribute(BindingResult.MODEL_KEY_PREFIX+"paramObj",errors);
		}
		
		model.addAttribute(MODE, EDIT);
		return PARAM_FORM;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.PARAM_EDIT)
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public String save(
			@ModelAttribute(value="paramObj") MParam mParam, 
			UserSession userSession,
			RedirectAttributes redirectAttributes) throws Exception {
		
		paramService.save(mParam, userSession);
		return "redirect:/admin/param";
	}
	
}
