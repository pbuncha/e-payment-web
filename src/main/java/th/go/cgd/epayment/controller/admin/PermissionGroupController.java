package th.go.cgd.epayment.controller.admin;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import th.go.cgd.epayment.annotation.ActionPermission;
import th.go.cgd.epayment.config.MenuPermissionConfig;
import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.constant.PermissionConstant;
import th.go.cgd.epayment.entity.*;
import th.go.cgd.epayment.model.MPermissionGroupModel;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.model.form.ProgramModel;
import th.go.cgd.epayment.model.form.SearchPermission;
import th.go.cgd.epayment.model.form.UserCenterBean;
import th.go.cgd.epayment.repository.MStatusRepository;
import th.go.cgd.epayment.security.SecurityUser;
import th.go.cgd.epayment.service.admin.MPermissionGroupProgramService;
import th.go.cgd.epayment.service.admin.MPermissionGroupService;
import th.go.cgd.epayment.service.admin.ProgramService;
import th.go.cgd.epayment.util.CatalogUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/admin/permission-group")
public class PermissionGroupController {
	
	
	@Autowired
	private MenuPermissionConfig menuPermissionConfig;
	
	@Autowired
	private MPermissionGroupService permissionGroupService;
	
	@Autowired
	private ProgramService programService;

	@Autowired
	private MStatusRepository statusRepository;

	@Autowired
	private MPermissionGroupProgramService permissionGroupProgramService;

	@Autowired
	private CatalogUtil catalogUtil;
	
	private static final String PERMISSION_LIST = "/views/admin/permission-group/permission-group-list";
	private static final String PERMISSION_FORM = "/views/admin/permission-group/permission-group-form";
	private static final String PERMISSION_MAP = "/views/admin/map-perm-program/map-perm-program";
	private static final String ACTIVE = "A";
	private static final String MODE = "mode";

	@RequestMapping()
	public String index() throws Exception {
		return PERMISSION_LIST;
	}

	@RequestMapping("/map")
	public String map() throws Exception {
		return PERMISSION_MAP;
	}

	@GetMapping("/getPermission")
	public @ResponseBody
	MPermissionGroupModel getPermission(@RequestParam(value="id") short id) throws Exception {
		MPermissionGroup program = permissionGroupService.findOne(id);
		MPermissionGroupModel p = new MPermissionGroupModel();
		p.setmPermissionGroupId(program.getMPermissionGroupId());
		p.setmPermissionGroupCode(program.getMPermissionGroupCode());
		p.setmPermissionGroupName(program.getMPermissionGroupName());
		p.setDescription(program.getDescription());
		p.setStatus(program.getStatus().getStatus());
		p.setStatusName(program.getStatus().getStatusName());
		p.setCheckPermissionAll(permissionGroupProgramService.findActiveProgramIdByPermissionGroupId(id));
		return p;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.PERMISSION_GROUP_ADD)
	@RequestMapping("/add")
	public String add(@ModelAttribute(value="permissionGroupObj") MPermissionGroup permissionGroup
		,BindingResult errors,@ModelAttribute(value="error") String error
		,Model model) throws Exception {
		System.out.println("xxx");
		if (StringUtils.isBlank(error)) { 			
//			permissionGroup.setMStatus((MStatus) catalogUtil.getObject(EPaymentConstant.M_STATUS, ACTIVE));
		} else {
			errors.reject("invalid.permission.group.name",new Object[]{},"Cannot Save MPermissionGroupModel");
			model.addAttribute(BindingResult.MODEL_KEY_PREFIX+"permissionGroupObj",errors);
		}
		
		model.addAttribute("programList", programService.findAll());
		model.addAttribute("mStatus", statusRepository.findAll());
		model.addAttribute(MODE, EPaymentConstant.ADD);
		return PERMISSION_FORM;
	}

	@RequestMapping("/view")
	public String edit( Model model) throws Exception {

		return PERMISSION_FORM;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.PERMISSION_GROUP_EDIT)
	@RequestMapping("/edit/{id}")
	public String edit(@PathVariable(value="id") int id,
			MPermissionGroup permissionGroup,
			BindingResult errors,
			@ModelAttribute(value="error") String error,
			Model model) throws Exception {
		
		if (StringUtils.isBlank(error)) {
			model.addAttribute("permissionGroupObj", permissionGroupService.findOne(id));
		} else {
			errors.reject("invalid.permission.group.name",new Object[]{},"Cannot Save MPermissionGroupModel");
			model.addAttribute(BindingResult.MODEL_KEY_PREFIX+"permissionGroupObj",errors);
		}
		
		model.addAttribute("permissionList", programService.findAllByOrderByProgramIdAsc());
		model.addAttribute("mStatus", statusRepository.findAll());
		model.addAttribute("checkList",permissionGroupProgramService.findActiveProgramIdByPermissionGroupId(id));
		model.addAttribute(MODE, EPaymentConstant.EDIT);
		return PERMISSION_FORM;
	}
	

	@PostMapping("/save")
	public @ResponseBody boolean save(@RequestBody MPermissionGroupModel permissionGroup) throws Exception {

		System.out.println(permissionGroup);
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		MPermissionGroup groupCheck = null;

		Date date = new Date();
		groupCheck = new MPermissionGroup();

		groupCheck.setCreateBy(user);
		groupCheck.setCreateDate(date);
		groupCheck.setUpdateBy(user);
		groupCheck.setUpdateDate(date);
		groupCheck.setDescription(permissionGroup.getDescription());
		groupCheck.setMPermissionGroupId(permissionGroup.getmPermissionGroupId());
		groupCheck.setMPermissionGroupCode(permissionGroup.getmPermissionGroupCode());
		groupCheck.setMPermissionGroupName(permissionGroup.getmPermissionGroupName());
		groupCheck.setStatus(statusRepository.findOne(permissionGroup.getStatus()));

		groupCheck = permissionGroupService.save(groupCheck);

		System.out.println(permissionGroup.getCheckPermissionAll());

		permissionGroupProgramService.update(groupCheck.getMPermissionGroupId(),permissionGroup.getCheckPermissionAll(),user.getUserId());

		menuPermissionConfig.initialPermission();
		return true;
	}
	


	@ActionPermission(PermissionConstant.ADMIN.PROGRAM_DELETE)
	@ResponseBody
	@RequestMapping(value="/delete",method = RequestMethod.POST)
	public boolean delete(@RequestParam(value="permissionGroupId") int id) throws Exception {


		return permissionGroupService.delete(id);
	}
	
	@RequestMapping(value = {"/deletePermissionGroup/{id}"}, method=RequestMethod.POST)
	public @ResponseBody boolean deletePermissionGroup(@PathVariable("id") int id) throws Exception {
		return permissionGroupService.delete(id);
	}

	@ResponseBody
	@PostMapping("/searchPermissionGroup")
	public List<MPermissionGroupModel> searchPermissionGroup(@RequestBody SearchPermission search){
		List<MPermissionGroupModel> list = new ArrayList<>();
		try {
			List<MPermissionGroup> pList = permissionGroupService.search(search.getPermissionCode(),search.getPermissionName(),search.getDesc(),search.getStatus());
			for (int i = 0; i < pList.size(); i++){
				MPermissionGroupModel model = new MPermissionGroupModel();
				model.setStatus(pList.get(i).getStatus().getStatus());
				model.setStatusName(pList.get(i).getStatus().getStatusName());
				model.setDescription(pList.get(i).getDescription());
				model.setmPermissionGroupCode(pList.get(i).getMPermissionGroupCode());
				model.setmPermissionGroupId(pList.get(i).getMPermissionGroupId());
				model.setmPermissionGroupName(pList.get(i).getMPermissionGroupName());
				list.add(model);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
}
