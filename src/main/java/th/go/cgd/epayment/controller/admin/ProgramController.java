package th.go.cgd.epayment.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.testng.log4testng.Logger;

import th.go.cgd.epayment.annotation.ActionPermission;
import th.go.cgd.epayment.constant.PermissionConstant;
import th.go.cgd.epayment.entity.MProgram;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.form.ProgramModel;
import th.go.cgd.epayment.service.admin.ProgramService;

import java.util.List;

@Controller
@RequestMapping("/admin/program")
public class ProgramController {
	
	private static final Logger log = Logger.getLogger(ProgramController.class);
	
    @Autowired
    private ProgramService programService;


    private static final String PROGRAM_LIST = "/views/admin/program/program-list";
    private static final String PROGRAM_FORM = "/views/admin/program/program-form";

    @RequestMapping()
    public String index(Model model) throws Exception {
    	User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	programService.getPermissionButtonAccess( "B_EDIT",user);
        return PROGRAM_LIST;
    }

    @PostMapping("/search")
    public @ResponseBody List<ProgramModel> index(@RequestBody ProgramModel program) throws Exception {
        System.out.println(program);

        List<ProgramModel> list = programService.search(program.getProgramCode(),program.getProgramName(),program.getDescription(),program.getStatus(), program.getPermissionGroupId());

        return list;
    }


    @GetMapping("/getProgram")
    public @ResponseBody ProgramModel getProgram(@RequestParam(value="id") short id) throws Exception {
        MProgram program = programService.findOne(id);
        ProgramModel p = new ProgramModel();
        p.setProgramId(program.getProgramId());
        p.setProgramId(program.getProgramId());
        p.setProgramCode(program.getProgramCode());
        p.setProgramName(program.getProgramName());
        p.setDescription(program.getDescription());
        p.setStatus(program.getStatus().getStatus());
        p.setStatusName(program.getStatus().getStatusName());
        return p;
    }

    @RequestMapping("/view")
    public String edit( Model model) throws Exception {

        return PROGRAM_FORM;
    }

	@PostMapping("/save/{permissionGroupId}")
	public @ResponseBody boolean save(
			@PathVariable Integer permissionGroupId,
			@RequestBody List<ProgramModel> program) throws Exception {

		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		try{
			programService.savePermissionGroupProgram(permissionGroupId, user, program);
		}catch( Exception ex ){
			log.error(ex);
			throw new Exception(ex);
		}
		
		return true;
	}

    @ActionPermission(PermissionConstant.ADMIN.PROGRAM_DELETE)
    @ResponseBody
    @RequestMapping(value="/delete",method = RequestMethod.POST)
    public boolean delete(@RequestParam(value="programId") int id) throws Exception {

        return programService.delete(id);
    }
    
	@RequestMapping(value = {"/deleteProgram/{id}"}, method=RequestMethod.POST)
	public @ResponseBody boolean deletePermissionGroup(@PathVariable("id") int id) throws Exception {
		return programService.delete(id);
	}
}
