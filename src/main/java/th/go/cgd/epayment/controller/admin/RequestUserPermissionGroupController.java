package th.go.cgd.epayment.controller.admin;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.controller.common.RegisterController;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.AttachmentBean;
import th.go.cgd.epayment.model.MPermissionGroupBean;
import th.go.cgd.epayment.model.MUserPermissiongroupBean;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.model.master.AccountDepositBean;
import th.go.cgd.epayment.service.interfaces.IUserInfoService;

@RestController
@RequestMapping("/admin")

public class RequestUserPermissionGroupController {
    private static final Logger logger = Logger.getLogger(RequestUserPermissionGroupController.class);
    @Autowired
    private IUserInfoService userInfoService;

    @RequestMapping(value="/request-user-permission-group")
	public ModelAndView viewMCostCenter(ModelMap model)
	{
    	
		ModelAndView view = new ModelAndView();
		view.setViewName("/views/admin/request-user-permission-group/request-user-permission-group");

		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{			
			view.addObject("loginUserId", user.getUserId());
//			view.addObject("loginUserCenter", user.getUserCenterId());
			
			//user.getEmail()
			
		}
		return view;
	}
    
	@RequestMapping(value="/search-user-permission-group", method=RequestMethod.POST)
	public List<MPermissionGroupBean> getMPermissionGroup(@RequestBody MPermissionGroupBean obj)
	{
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{			
			List<MPermissionGroupBean> q = userInfoService.getMPermissionGroup(user.getUserId());			
			
			System.out.println("1" + obj.getStatus());
			
			if (!StringUtils.isEmpty(obj.getStatus()))
			{
				System.out.println("2" + obj.getStatus());
				q = q.stream()
						.filter(a -> a.getStatus() != null)
						.filter(a -> a.getStatus().equals(obj.getStatus()))
						.collect(Collectors.toList());
			}
			if (!StringUtils.isEmpty(obj.getmPermissionGroupName()))
			{
				System.out.println("3" + obj.getmPermissionGroupName());
				q = q.stream()
						.filter(a -> a.getmPermissionGroupName() != null)
						.filter(a -> a.getmPermissionGroupName().contains(obj.getmPermissionGroupName()))
						.collect(Collectors.toList());
			}			
			
			return q;
		}
		return userInfoService.getMPermissionGroup(-1);
	}	
	
	
	@RequestMapping(value="/create-user-permission-group", method=RequestMethod.POST)
	public void createUserPermissiongroup(@RequestBody List<MUserPermissiongroupBean> objs)
	{
		System.out.println("createUserPermissiongroup:");
		
		for (MUserPermissiongroupBean obj : objs)
		{
			userInfoService.updateUserPermissiongroup(obj);					
		}		
	}		
	
	@RequestMapping(value = "/getUserLoginDetails/{id}",method=RequestMethod.GET)
	public @ResponseBody UserBean getUserLoginDetails (@PathVariable("id") Integer id)
	{
		return userInfoService.getUserById(id);
	}
	@RequestMapping(value = "/upload",method=RequestMethod.POST)
	public @ResponseBody AttachmentBean upload(@RequestBody MultipartFile file) throws IOException {
		String msg = "success";
		  String filePath  ="";
		    AttachmentBean attachmentBean = new AttachmentBean();
		try{
		    filePath = new ClassPathResource("/").getFile().getAbsolutePath();
		    File checkPath = new File(filePath+"/file_upload/requestPermiss/");
		    if(!checkPath.exists()){
			checkPath.mkdirs();
		    }
		    filePath = checkPath.getAbsolutePath()+"\\"+file.getOriginalFilename();
		}catch(Exception ex ){
		    logger.info("filePath Exception : "+ex);
		}
		
		try{
		    logger.info(" PathFull : " + filePath);
		    if (!file.isEmpty()) {
		        try {
		            byte[] bytes = file.getBytes();
		            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
		            stream.write(bytes);
		            stream.close();
		            logger.info("Creating file: " + filePath);
		            logger.info("You successfully uploaded " + file.getOriginalFilename() + "!");
		            attachmentBean.setData(bytes);
		        } catch (Exception e) {
		            logger.debug("You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
//		            return HttpStatus.FORBIDDEN;
		        }
		    } else {
			logger.info("You failed to upload " + file.getOriginalFilename() + " because the file was empty.");
//		        return HttpStatus.FORBIDDEN;
		    }
		
		    attachmentBean.setFilename(file.getOriginalFilename());
		    attachmentBean.setPath(filePath);
		}catch (Exception e){
			logger.info("Error : "+e);
			msg = e.toString();
		}
		 return attachmentBean;

	}


}
