package th.go.cgd.epayment.controller.admin;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.annotation.ActionPermission;
import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.constant.PermissionConstant;
import th.go.cgd.epayment.entity.MUserAccessPermission;
import th.go.cgd.epayment.entity.MUserStatus;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserAccessPermission;
import th.go.cgd.epayment.entity.UserInfo;
import th.go.cgd.epayment.model.DataTable;
import th.go.cgd.epayment.model.DataTableSortable;
import th.go.cgd.epayment.model.UserRootTreeModel;
import th.go.cgd.epayment.model.UserRootTreeSearchModel;
import th.go.cgd.epayment.model.UserSearchModel;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.SimpleDataTableRepository;
import th.go.cgd.epayment.service.UserInfoService;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.service.admin.OrganizationService;
import th.go.cgd.epayment.service.admin.PermissionGroupService;
import th.go.cgd.epayment.service.admin.PermissionService;
import th.go.cgd.epayment.util.CatalogUtil;
import th.go.cgd.epayment.util.SqlWhereCauseBuilder;
import th.go.cgd.epayment.util.datatable.TransformIgnore;
import th.go.cgd.epayment.util.datatable.TransformTarget;

@Controller
@RequestMapping("/admin/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CatalogUtil catalogUtil;
	
	@Autowired
	private PermissionGroupService permissionGroupService;
	
	@Autowired
	private PermissionService permissionService;
	
	@Autowired
	private OrganizationService organizationService;
	
	@Autowired
	private UserInfoService userInfoService;
	
	@Autowired
	private SimpleDataTableRepository simpleDataTableRepository;
	
	private static final String USER_PAGE = "/views/admin/user/user-init";
	private static final String USER_FORM = "/views/admin/user/user-form";
	private static final String USER_RESET = "/views/admin/user/user-reset";
	
	private static final String MODE = "mode";
	private static final String ACTIVE = "A";
	
	@TransformIgnore(needAllSource=true, needAllTarget=true)
	private static class UserDataTableBean {
		@TransformTarget("EMAIL")
		public String email;
		@TransformTarget("FULL_NAME")
		public String fullName;
		@TransformTarget("ORG_NAME")
		public String orgName;
		@TransformTarget("USER_STATUS_NAME")
		public String userStatusName;
		@TransformTarget("USER_STATUS")
		public String userStatus;
		@TransformTarget("USER_ID")
		public int userId;
		@TransformTarget("ORG_ID")
		public int orgId;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_VIEW)
	@RequestMapping
	public String index(Model model) throws Exception {
		model.addAttribute(new UserSearchModel());
		model.addAttribute("MUserStatus", catalogUtil.get(EPaymentConstant.M_USER_STATUS));
		model.addAttribute("MUserRole", catalogUtil.get(EPaymentConstant.M_USER_ROLE));
		return USER_PAGE;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_ADD)
	@RequestMapping(value="/add", method=RequestMethod.POST) 
	public String add(
			@RequestParam(value="userId", required=false) Integer userId,
			@RequestParam(value="organizationId", required=false) Integer organizationId,
			Model model) throws Exception {

		User user = new User();
		UserInfo userInfo = new UserInfo();
//		user.setMUserStatus((MUserStatus) catalogUtil.getObject(EPaymentConstant.M_USER_STATUS, ACTIVE));
		UserAccessPermission userAccessPermission = new UserAccessPermission();
//		userAccessPermission.setMUserAccessPermission((MUserAccessPermission) catalogUtil.getObject(EPaymentConstant.M_USER_ACCESS_PERMISSION, 1));
//		user.setUserAccessPermission(userAccessPermission);
		
//		if (userId != null && organizationId == null) {
//			UserInfo uInfo = userInfoService.findOne(userId);
//			userInfo.setOrganization(uInfo.getOrganization());
//			userInfo.setUserInfo(uInfo);
//			user.setUserInfo(userInfo);
//		} else if (organizationId != null && userId == null) {
//			userInfo.setOrganization(organizationService.findOne((int) organizationId));
//			userInfo.setUserInfo(null);
//			user.setUserInfo(userInfo);
//		}
		
		model.addAttribute(user);
		model.addAttribute(MODE, EPaymentConstant.ADD);
		model.addAttribute("MUserStatus", catalogUtil.get(EPaymentConstant.M_USER_STATUS));
		model.addAttribute("MUserRole", catalogUtil.get(EPaymentConstant.M_USER_ROLE));
		model.addAttribute("MUserAccessPermission", catalogUtil.get(EPaymentConstant.M_USER_ACCESS_PERMISSION));
		model.addAttribute("permissionGroupList", permissionGroupService.findAll());
		model.addAttribute("permissionList", permissionService.findAllByOrderByPermissionIdAsc());
		return USER_FORM;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_EDIT)
	@RequestMapping(value="/edit", method=RequestMethod.GET)
	public String edit(@RequestParam(value="userId") int userId, Model model) throws Exception {
		model.addAttribute(userService.findOne(userId));
		model.addAttribute(MODE, EPaymentConstant.EDIT);
		model.addAttribute("MUserStatus", catalogUtil.get(EPaymentConstant.M_USER_STATUS));
		model.addAttribute("MUserRole", catalogUtil.get(EPaymentConstant.M_USER_ROLE));
		model.addAttribute("MUserAccessPermission", catalogUtil.get(EPaymentConstant.M_USER_ACCESS_PERMISSION));
		model.addAttribute("permissionGroupList", permissionGroupService.findAll());
		model.addAttribute("permissionList", permissionService.findAllByOrderByPermissionIdAsc());
		return USER_FORM;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_VIEW)
	@RequestMapping(value="/view", method=RequestMethod.GET)
	public String view(@RequestParam(value="userId") int userId, Model model) throws Exception {
		model.addAttribute(userService.findOne(userId));
		model.addAttribute(MODE, EPaymentConstant.VIEW);
		model.addAttribute("MUserStatus", catalogUtil.get(EPaymentConstant.M_USER_STATUS));
		model.addAttribute("MUserRole", catalogUtil.get(EPaymentConstant.M_USER_ROLE));
		model.addAttribute("MUserAccessPermission", catalogUtil.get(EPaymentConstant.M_USER_ACCESS_PERMISSION));
		model.addAttribute("permissionGroupList", permissionGroupService.findAll());
		model.addAttribute("permissionList", permissionService.findAllByOrderByPermissionIdAsc());
		return USER_FORM;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_VIEW)
	@RequestMapping("/dataTable")
	public ResponseEntity<DataTable<UserDataTableBean>> dataTable(@ModelAttribute(value="userSearchModel") UserSearchModel userSearchModel,
			UserSession userSession) {
		String sql = simpleDataTableRepository.getNamedQuery("userSearchDataTable");
		SqlWhereCauseBuilder builder = SqlWhereCauseBuilder.getInstance(false);
		
		if (userSearchModel.getEmail() != null && userSearchModel.getEmail().trim().length() != 0) {
			builder.append("UI.EMAIL like :email", "email", "%" + userSearchModel.getEmail() + "%");
		}
		
		if (userSearchModel.getFirstName() != null && userSearchModel.getFirstName().trim().length() != 0) {
			builder.append("UI.FIRST_NAME like :firstname", "firstname", "%" + userSearchModel.getFirstName() + "%");
		}
		
		if (userSearchModel.getLastName() != null && userSearchModel.getLastName().trim().length() != 0) {
			builder.append("UI.LAST_NAME like :lastname", "lastname", "%" + userSearchModel.getLastName() + "%");
		}
		
		if (userSearchModel.getOrganizationId() != null) {
			builder.append("UI.ORG_ID = :orgId", "orgId", userSearchModel.getOrganizationId());
		}
		
		if (userSearchModel.getMUserStatus() != null) {
			builder.append("U.STATUS = :status", "status", userSearchModel.getMUserStatus().getStatus());
		}
		
		if (userSearchModel.getMUserRole() != null) {
			builder.append("UR.ROLE_ID = :userRoleId", "userRoleId", userSearchModel.getMUserRole().getRoleId());
		}
		
		if (userSession.getUserOrg() != 2) {			
			builder.append("UI.ORG_ID IN (:orgIds)", "orgIds", userSession.getOrgs());
		}
		
		DataTable<UserDataTableBean> pagingData = simpleDataTableRepository.getPagingData(builder.appendTo(sql), DataTableSortable.getRequestSortable(), builder.getParameterMap(), UserDataTableBean.class);
		return new ResponseEntity<DataTable<UserDataTableBean>>(pagingData, HttpStatus.OK);
	}
	
	@ActionPermission({PermissionConstant.ADMIN.USER_ADD, PermissionConstant.ADMIN.USER_EDIT})
	@ResponseBody
	@RequestMapping(value="/save", method=RequestMethod.POST)
	public boolean save(@ModelAttribute(value="user") User user, UserSession userSession,
			@RequestParam(value="notiNewUser", required=false) Integer notiNewUser,
			@RequestParam(value="notiNewUserEmail", required=false) String notiNewUserEmail,
			HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {
		userService.save(user, userSession, notiNewUser, notiNewUserEmail, request, response, locale);
		return true;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_VIEW)
	@ResponseBody
	@RequestMapping(value="/userRootTree", method=RequestMethod.GET)
	public UserRootTreeModel getRootTree(
			@RequestParam(value="orgId", required=false) Integer orgId,
			UserSession userSession) throws Exception {
		return userService.getRootTree(userSession, orgId);
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_VIEW)
	@ResponseBody
	@RequestMapping(value="/userTreeChildren", method=RequestMethod.GET)
	public List<UserRootTreeModel> getChrildrenTree(
			@RequestParam(value="orgId", required=false) Integer orgId,
			@RequestParam(value="userId", required=false) Integer userId) throws Exception {
		return userService.getChildrenTree(orgId, userId);
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_VIEW)
	@RequestMapping(value="/reset", method=RequestMethod.GET)
	public String reset() throws Exception {
		return USER_RESET;
	}
	
	@ResponseBody
	@RequestMapping(value="/checkUsername", method=RequestMethod.POST)
	public boolean checkUserAdd(
			@RequestParam(value="username") String username,
			@RequestParam(value="userId", required=false) Integer userId,
			@RequestParam(value="mode") String mode
			) throws Exception {
		boolean check = false;
		if (mode.equals(EPaymentConstant.ADD)) {			
			check = userService.checkUsername(username, null);
		} else if (mode.equals(EPaymentConstant.EDIT)) {
			check = userService.checkUsername(username, userService.findOne((int) userId));
		}
		return check;
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_VIEW)
	@ResponseBody
	@RequestMapping(value="/userRootSearch", method=RequestMethod.GET)
	public List<UserRootTreeSearchModel> getUserRootSearch(@RequestParam(value="userId") int userId) throws Exception {
		return userService.getUserRootTreeSearchModel(userId);
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_EDIT)
	@RequestMapping(value="/moveInTree", method=RequestMethod.POST)
	public ResponseEntity<Boolean> moveInTree(@RequestParam(value="selfId") int selfId, @RequestParam(value="userId", required=false) Integer userId, @RequestParam(value="orgId", required=false) Integer orgId) throws Exception {
//		userService.moveInTree(selfId, userId, orgId);
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_EDIT)
	@RequestMapping(value="/moveOrganization", method=RequestMethod.POST)
	public ResponseEntity<Boolean> moveOrganization(@ModelAttribute(value="user") User user, @RequestParam(value="organizationMoveSlc") int orgId) throws Exception {
		userService.moveOrganization(user, orgId);
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
	@ActionPermission(PermissionConstant.ADMIN.USER_CHANGE_PASSWORD)
	@ResponseBody
	@RequestMapping(value="/forceChangePassword", method=RequestMethod.POST)
	public boolean forceChangePassword(@ModelAttribute(value="user") User user, @RequestParam(value="newPassword") String newPassword, UserSession userSession) throws Exception {
		return userService.forceChangePassword(user, userSession, newPassword);
	}
	
//	@ActionPermission(PermissionConstant.ADMIN.USER_DELETE)
//	@ResponseBody
//	@RequestMapping(value="/delete", method=RequestMethod.POST)
//	public boolean delete(@ModelAttribute(value="user") User user, UserSession userSession) throws Exception {
//		return userService.delete(user, userSession);
//	}
	
	@ResponseBody
	@RequestMapping(value="/checkPassword", method=RequestMethod.POST)
	public String checkPassword(@RequestParam(value="password") String password) throws Exception {
		return (userService.checkPassword(password)) ? "\"true\"" : "\"กรุณาระบุรหัสผ่านให้ถูกต้องตามแบบ\"";
	}
	
	@RequestMapping(value="/getPassword", method=RequestMethod.GET)
	public ResponseEntity<String> getPassword() throws Exception {
		return new ResponseEntity<String>(userService.getPassword(), HttpStatus.OK);
	}
	
//	@ActionPermission(PermissionConstant.ADMIN.USER_EDIT)
//	@RequestMapping(value="/recover", method=RequestMethod.POST)
//	public ResponseEntity<Boolean> recover(@RequestParam(value="userId") int userId, UserSession userSession) throws Exception {
//		userService.recover(userId, userSession);
//		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
//	}
}
