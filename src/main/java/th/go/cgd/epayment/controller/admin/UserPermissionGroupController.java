package th.go.cgd.epayment.controller.admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import th.go.cgd.epayment.annotation.ActionPermission;
import th.go.cgd.epayment.constant.PermissionConstant;
import th.go.cgd.epayment.entity.*;
import th.go.cgd.epayment.model.StatusBean;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.model.form.UserCenterBean;
import th.go.cgd.epayment.model.form.UserModel;
import th.go.cgd.epayment.repository.*;
import th.go.cgd.epayment.security.SecurityUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

@Controller
@RequestMapping("/admin/user-permission-group")
public class UserPermissionGroupController {

    private static final String PERMISSION_LIST = "/views/admin/user-permission-group/user-permission-group";

    private static final String ACTIVE = "A";
    private static final String MODE = "mode";

    @Autowired
    private UserCenterRepository userCenterRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MStatusRepository statusRepository;

    @Autowired
    private MPermissionGroupRepository permissionGroupRepository;

    @Autowired
    private MUserPermissionGroupRepository userPermissionGroupRepository;

    @RequestMapping()
    public String index(Model model) throws Exception {

        return PERMISSION_LIST;
    }

    @ResponseBody
    @PostMapping("/getUserByCitizen")
    public UserCenterBean getUserByCitizen(@RequestBody String citizen){
        System.out.println(citizen);
        List<UserCenter> list = userCenterRepository.findByCitizenNo(citizen);
        UserCenter uc = null;
        if(list.size() > 0){
            uc = list.get(0);
        }

        UserCenterBean ub = new UserCenterBean();
        ub.setUserCenterId(uc.getUserCenterId());
        ub.setCitizenid(uc.getCitizenNo());
        ub.setFnameTH(uc.getFirstNameTh());
        ub.setFnameEN(uc.getFirstNameEn());
        ub.setLnameEN(uc.getLastNameEn());
        ub.setLnameTH(uc.getLastNameTh());

        List<User> userList = userRepository.findByUserCenterId(uc.getUserCenterId());

        List<UserModel> beanList = new ArrayList<UserModel>();
        for(int i = 0 ; i < userList.size(); i++){
            UserModel bean = new UserModel();
            bean.setUserId(userList.get(i).getUserId());
            bean.setUsername(userList.get(i).getUserName());
            beanList.add(bean);
        }
        ub.setUsers(beanList);

        System.out.println(ub);
        return ub;
    }

    @ResponseBody
    @PostMapping("/getUserByLogin")
    public UserModel getUserByLogin(@RequestBody String citizen){
        System.out.println(citizen);
        List<UserCenter> list = userCenterRepository.findByCitizenNo(citizen.trim());
        User uc = null;
        if (list.size()>0){
            uc = list.get(0).getUserList().get(0);
            
        }

        UserModel ub = new UserModel();
        
        ub.setUsername(uc.getUserName());
        ub.setUserId(uc.getUserId());
        if(null != uc.getUserCenterId()){
            ub.setLnameEN(uc.getUserCenterId().getLastNameEn());
            ub.setFnameEN(uc.getUserCenterId().getFirstNameEn());
            ub.setLnameTH(uc.getUserCenterId().getLastNameTh());
            ub.setFnameTH(uc.getUserCenterId().getFirstNameTh());
        }
        ub.setPerms(getUserPermissionGroup(ub.getUserId()));

        return ub;
    }

    @ResponseBody
    @PostMapping("/getAllStatus")
    public List<StatusBean> getAllStatus(){
        List<StatusBean> list = new ArrayList<StatusBean>();
        List<MStatus> sList = statusRepository.findAll();

        for(int i = 0; i < sList.size(); i++){
            StatusBean bean = new StatusBean();
            bean.setStatus(sList.get(i).getStatus());
            bean.setStatusName(sList.get(i).getStatusName());
            list.add(bean);
        }
        return list;
    }

    @ResponseBody
    @PostMapping("/getUserPermissionGroup")
    public List<Integer> getUserPermissionGroup(@RequestBody int id){
        return userPermissionGroupRepository.findByUserIdAscAsInt(id);
    }

    @ResponseBody
    @PostMapping("/updateUserPermissionGroup/{id}")
    public boolean getUserPermissionGroup(@PathVariable("id") int id , @RequestBody List<Integer> list){
        try{
            User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            System.out.println(id);
            System.out.println(list);
            List<MUserPermissiongroup> mapList = userPermissionGroupRepository.findByUserIdAsc(id);

            for(MUserPermissiongroup usermap : mapList){
        	   userPermissionGroupRepository.delete(usermap.getMUserPermissiongroupId());
            }
         

            List<MPermissionGroup> pList = permissionGroupRepository.findByPermissionGroupIdInList(list);
            mapList.clear();
            System.out.println(mapList.size());

            System.out.println(pList.size());
            User userTb = userRepository.findOne(id);
          Date date =  new Date();
            for (int i = 0; i < pList.size(); i++){
                MUserPermissiongroup bean = new MUserPermissiongroup();
                bean.setCreateBy(user.getUserId());
                bean.setCreateDate(date);
                bean.setUpdateBy(user.getUserId());
                bean.setUpdateDate(date);
                bean.setMPermissionGroupId(pList.get(i));
                bean.setUserId(userTb);
                bean.setStatus("1");
                mapList.add(bean);
            }
            userPermissionGroupRepository.save(mapList);
            return true;
        }catch (Exception e ){
            e.printStackTrace();
            System.out.println("Cannot update UserPermission cause : "+e);
            return false;
        }


    }


}
