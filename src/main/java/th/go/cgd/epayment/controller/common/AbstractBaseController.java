package th.go.cgd.epayment.controller.common;


import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.ServletContextAware;

/**
 * 
 * @author buncha.p
 *
 */
public abstract class AbstractBaseController implements ServletContextAware {
	
	private final Logger log = Logger.getLogger(ReportController.class);
	protected ServletContext servletContext;

	protected Cookie getCookie(HttpServletRequest request, String name, String path) {
		if (request.getCookies() == null)
			return null;

		for (Cookie c : request.getCookies()) {
			if (c.getName().equals(name)) {
				return c;
			}
		}
		return null;
	}

	protected ResponseEntity<byte[]> downloadPDFFile(byte[] contents, String outputName){
		HttpHeaders headers = new HttpHeaders();
	    headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE ));
	    headers.setContentDispositionFormData(outputName, outputName);
	    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
	   return new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
	}
	
	
	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}

	public ServletContext getServletContext() {
		return this.servletContext;
	}
	
	
}
