package th.go.cgd.epayment.controller.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.BatchFileReadResultBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.service.interfaces.IBatchFileReadResultService;
import th.go.cgd.epayment.service.interfaces.IMasterService;


@RestController
@RequestMapping("/batchFileReadResult")
public class BatchFileReadResultController {

	@Autowired
	private IBatchFileReadResultService service;
	
	@Autowired
	private IMasterService masterService;
		
	@RequestMapping(value = "/searchBatchFile", method = RequestMethod.GET)
	public ModelAndView search(ModelMap model) {
		System.out.println("search");
		return new ModelAndView("views/batchFileReadResult/batch-file-read-result", model);
	}
	
	@RequestMapping(value="/findall-batch-file-read-result", method=RequestMethod.GET)
	public List<BatchFileReadResultBean> getDisbursementUnit()
	{
		return service.getAllBatchFileReadResult();
	}
		
	@RequestMapping(value="/search-batch-file-read-result", method=RequestMethod.POST)
	public List<BatchFileReadResultBean> getBatchFileReadResultByCriteria(@RequestBody BatchFileReadResultBean obj)
	{
		return service.getBatchFileReadResultByCriteria(obj);
	}	
	
	@RequestMapping(value="/getBank", method=RequestMethod.GET)
	public List<DropdownBean> getBank()
	{
		return masterService.getBank();
	}

}
