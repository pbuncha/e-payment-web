package th.go.cgd.epayment.controller.common;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.captcha.botdetect.web.servlet.SimpleCaptcha;

import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.security.SecurityUser;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.util.CalendarHelper;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("changePassword")
public class ChangePasswordController {
    private static final Logger logger = Logger.getLogger(ChangePasswordController.class);
    private static final String CHANGEPASSWORD = "change_password";
    private static final String LOGIN = "log_citizen";
	@Autowired
	private UserService userService;


    @RequestMapping()
    public ModelAndView changePassword(ModelMap model, HttpServletRequest req)
    {
       
	User  user = null;
	Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	  if ((principal != null) && (principal instanceof User)) {
	      user = (User) principal;
	      model.addAttribute("login",user.getUserName());
	}
        return new ModelAndView(CHANGEPASSWORD,model);
    }
    
	@RequestMapping(value = "/changePasswordUser/{recaptcha}",method=RequestMethod.POST)
	public @ResponseBody String changePasswordUser(@RequestBody UserBean userBean,@PathVariable("recaptcha") String recaptcha,HttpServletRequest request) {
	    logger.info("changePasswordUser");
		String captchaId = recaptcha;
		String captchaCode = userBean.getCaptcha();
		SimpleCaptcha captcha = SimpleCaptcha.load(request);
		boolean isHuman = captcha.validate(captchaCode, captchaId);
		Date date = new Date();

		if(!isHuman){
			return "\"Captcha incorrect.\"";
		}
		try{
        	    User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        	    String passwordBCrypt =   BCrypt.hashpw(userBean.getPassword(), BCrypt.gensalt());
        	    user.setPassword(passwordBCrypt);
        	    user.setUpdatedBy(user);
        	    user.setUpdatedDate(date);
        	    userService.save(user);
		}catch (Exception e){
			System.out.println(e.getMessage());
			return "\"Send email fail."+e+"\"";

		}
	    return "\"success\"";
	}
}
