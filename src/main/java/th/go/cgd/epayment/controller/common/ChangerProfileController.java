package th.go.cgd.epayment.controller.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.captcha.botdetect.web.servlet.SimpleCaptcha;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.Address;
import th.go.cgd.epayment.entity.MAmphur;
import th.go.cgd.epayment.entity.MBusinessArea;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MOrganizationSubType;
import th.go.cgd.epayment.entity.MOrganizationType;
import th.go.cgd.epayment.entity.MPermissionGroup;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MStatus;
import th.go.cgd.epayment.entity.MTambon;
import th.go.cgd.epayment.entity.MTitleName;
import th.go.cgd.epayment.entity.MUserPermissiongroup;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.entity.UserOrganization;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.EmailBean;
import th.go.cgd.epayment.model.MPermissionGroupModel;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.service.interfaces.IDataMasterService;
import th.go.cgd.epayment.service.interfaces.IMBusinessAreaService;
import th.go.cgd.epayment.service.interfaces.IMCostCenterService;
import th.go.cgd.epayment.service.interfaces.IMDepartmentService;
import th.go.cgd.epayment.service.interfaces.IMOrganizationSubTypeService;
import th.go.cgd.epayment.service.interfaces.IMOrganizationTypeService;
import th.go.cgd.epayment.service.interfaces.IMProvinceService;
import th.go.cgd.epayment.service.interfaces.IMasterService;
import th.go.cgd.epayment.service.interfaces.IUserOrganizationService;
import th.go.cgd.epayment.util.CalendarHelper;

/**
 * @author torgan.p 25 ม.ค. 2561 22:55:01
 *
 */
@RestController
@RequestMapping("/changerProfile")
public class ChangerProfileController {
    private final Logger logger = Logger.getLogger(ChangerProfileController.class);
	@Autowired
	private UserService userService;
	@Autowired 
	public IMasterService masterService;
	@Autowired
	private IDataMasterService dataMasterService;
	@Autowired
	public IMProvinceService provinceService;
	
	@Autowired
	private IMOrganizationTypeService organizationTypeService;
	@Autowired
	private IMDepartmentService departmentService;
	@Autowired
	private IUserOrganizationService userOrganizationService;
	@Autowired
	private IMOrganizationSubTypeService organizationSubTypeService;
	@Autowired
	private IMBusinessAreaService businessAreaService;
	@Autowired
	private IMCostCenterService costCenterService;
	ResourceBundle bundleError =  EPaymentConstant.bundleError ;
	
    @RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView changerProfile(ModelMap model) {
	logger.info("changerProfile");
	return new ModelAndView("views/changerProfile/changerProfile", model);
    }
    
    @RequestMapping(value = "/getOrganizationType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getOrganizationType() {
		logger.info("/getOrganizationType ");
		List<DropdownBean> dropdownBeanList = dataMasterService.getOrganizationType();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getBusinessArea/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getBusinessArea(@PathVariable("id") Integer id) {
		logger.info("/register/getBusinessArea ");
		List<DropdownBean> dropdownBeanList = dataMasterService.getBusinessAreaByDepartment(id);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getOrganizationSubType/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getOrganizationSubType(@PathVariable("id") Integer id) {
		logger.info("/register/getOrganizationSubType ");
		logger.info("id = " + id);
		List<DropdownBean> dropdownBeanList = dataMasterService.getOrganizationSubType(id);
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getDepartment/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getDepartment(@PathVariable("id") Integer id) {
		logger.info("/register/getDepartment ");
		logger.info("id = " + id);
		List<DropdownBean> dropdownBeanList = dataMasterService.getDepartment(id);
		return dropdownBeanList;
	}
	@RequestMapping(value = "/getCostCenter/{did}/{bid}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCostCenter(@PathVariable("bid") Integer bid,@PathVariable("did") Integer did) {
		logger.info("/register/getCostCenter ");
		logger.info("bid = " + bid);
		logger.info("did = " + did);
		List<DropdownBean> dropdownBeanList = dataMasterService.getCostCenter(did, bid);
		return dropdownBeanList;
		//return null;
	}
	
	@RequestMapping(value = "/getProvince",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getProvince() {
		logger.info("/register/getProvince ");
		List<DropdownBean> dropdownBeanList = dataMasterService.getProvince();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getAmphur/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getAmphur(@PathVariable("id") Integer id) {
		logger.info("/register/getAmphur ");
		logger.info("id = " + id);
		List<DropdownBean> dropdownBeanList = dataMasterService.getAmphur(id);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getTambon/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getTambon(@PathVariable("id") Integer id) {
		logger.info("/register/getTambon ");
		logger.info("id = " + id);
		List<DropdownBean> dropdownBeanList = dataMasterService.getTambon(id);
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getUserPermissionGroup", method = RequestMethod.POST)
	public @ResponseBody List<MPermissionGroupModel> getUserPermissionGroup(){
		List<MPermissionGroupModel> list = new ArrayList<>();
		User  user = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(principal !=null){
        		user = (User) principal;
        		UserCenter userCenter = user.getUserCenterId();

        		try {
        		    List<MUserPermissiongroup> userPermissiongroupList =  user.getMUserPermissiongroupList();
        		    for(MUserPermissiongroup userPermissiongroup : userPermissiongroupList){
        			MPermissionGroup permissionGroup = userPermissiongroup.getMPermissionGroupId();;
        			MPermissionGroupModel permissionGroupModel = new MPermissionGroupModel();
        			permissionGroupModel.setmPermissionGroupId(permissionGroup.getMPermissionGroupId());
        			permissionGroupModel.setmPermissionGroupCode(permissionGroup.getMPermissionGroupCode());
        			permissionGroupModel.setmPermissionGroupName(permissionGroup.getMPermissionGroupName());
        			if(null != permissionGroup.getStatus()){
                			MStatus status = permissionGroup.getStatus();
                			permissionGroupModel.setStatus(status.getStatus());
                			permissionGroupModel.setStatusName(status.getStatusName());
        			}
        			
        			permissionGroupModel.setStatusUserPermissionGroup(userPermissiongroup.getStatus());
        			permissionGroupModel.setUserFullName((userCenter.getTitleId()!=null ? userCenter.getTitleId().getTitleName() : "")+" "+userCenter.getFirstNameTh()+" "+userCenter.getLastNameTh());
        			list.add(permissionGroupModel);
        		    }
        		} catch (Exception e) {
        			e.printStackTrace();
        		}
		}
		return list;
	}
    
    @RequestMapping(value = {"/getUserProfile"}, method=RequestMethod.GET)
	public @ResponseBody UserBean getUserProfile() throws Exception{

	User  user = null;
	Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	UserBean userBean = null;
	  if ((principal != null) && (principal instanceof User)) {
	      user = (User) principal;
	      UserCenter userCenter = userService.findOneUserCenter(user.getUserCenterId().getCitizenNo());
		if(userCenter!=null && userCenter.getUserList().size()!=0){
			userBean = new UserBean();
			User userTb = userCenter.getUserList().get(0);
			Address addressTb =  userCenter.getAddressId();
			userBean.setUserId(userTb.getUserId());
			userBean.setFnameTH(userCenter.getFirstNameTh());
			userBean.setLnameTH(userCenter.getLastNameTh());
			userBean.setUserType(user.getUserTypeId());
			if(userCenter.getTitleId()!=null){
				userBean.setTitleThId(userCenter.getTitleId().getTitleId().toString());
				userBean.setTitleEnId(userCenter.getTitleId().getTitleId().toString());
				userBean.setTitleNameTH(userCenter.getTitleId().getTitleName());
				userBean.setTitleNameEN(userCenter.getTitleId().getTitleNameEn());
			}

			userBean.setMnameEN(userCenter.getMiddleNameEn());
			userBean.setMnameTH(userCenter.getMiddleNameTh());
			userBean.setFnameEN(userCenter.getFirstNameEn());
			userBean.setLnameEN(userCenter.getLastNameEn());
			userBean.setBirthDateStr(userCenter.getBirthDate().toString());
			userBean.setAge(CalendarHelper.getAge(userCenter.getBirthDate()));
			userBean.setEmail(userTb.getEmail());
			userBean.setMobile(userTb.getMobile());
			userBean.setAddress_id(addressTb.getAddressId());
			userBean.setNo(addressTb.getNo());
			userBean.setMoo(addressTb.getMoo());
			userBean.setCitizenid(userCenter.getCitizenNo());
			userBean.setSoi(addressTb.getSoi());
			userBean.setRoad(addressTb.getRoad());
			userBean.setRoomNo(addressTb.getRoomNo());
			MTambon tambonTb =  addressTb.getTambonId();
			MAmphur amphurTb = addressTb.getAmphurId();
			MProvince provinceTb = addressTb.getProvinceId();
			if(null != provinceTb){
				userBean.setProvinceId(provinceTb.getProvinceId().toString());
				//				userBean.setProvinceName(provinceTb.getProvinceNameTh());
			}
			if(null != amphurTb){
				userBean.setAmphurId(amphurTb.getAmphurId().toString());
				//				userBean.setAmphurName(amphurTb.getAmphurNameTh());
			}
			if(null != tambonTb){
				userBean.setTambonId(tambonTb.getTambonId().toString());
				//				userBean.setTambonName(tambonTb.getTambonNameTh());
			}
			
			if(null != addressTb){
				userBean.setPostCode(addressTb.getPostcode());
			}

			for(UserOrganization userOrganization : userTb.getUserOrganizationList2()){
			    MOrganizationType organizationType = userOrganization.getOrganizationTypeId();
			    
			    if(organizationType!=null){
        			    DropdownBean dropdownBean = new DropdownBean();
        			    dropdownBean.setId(organizationType.getOrganizationTypeId().toString());
        			    dropdownBean.setDesc(organizationType.getOrganizationTypeName());
        			    dropdownBean.setDesc2(organizationType.getOrganizationTypeName());
        			    userBean.setOrgType(dropdownBean);
			    }
			    MOrganizationSubType organizationSubType = userOrganization.getOrganizationSubTypeId();
			    if(organizationSubType!=null){
        			    DropdownBean dropdownBean = new DropdownBean();
        			    dropdownBean.setId(organizationSubType.getOrganizationSubTypeId().toString());
        			    dropdownBean.setDesc(organizationSubType.getOrganizationSubTypeName());
        			    dropdownBean.setDesc2(organizationSubType.getOrganizationSubTypeName());
        			    userBean.setOrgSubType(dropdownBean);
			    }
			    MDepartment department = userOrganization.getDepartmentId();
			    if(department!=null){
    			    DropdownBean dropdownBean = new DropdownBean();
        			    dropdownBean.setId(department.getDepartmentId().toString());
        			    dropdownBean.setDesc(department.getDepartmentName());
        			    dropdownBean.setDesc2(department.getDepartmentName());
        			    userBean.setDept(dropdownBean);
			    }
			    MBusinessArea businessArea = userOrganization.getBusinessAreaId();
			    if(businessArea!=null){
				DropdownBean dropdownBean = new DropdownBean();
	    			     dropdownBean.setId(businessArea.getBusinessAreaId().toString());
	    			     dropdownBean.setDesc(businessArea.getBusinessAreaName());
	    			     dropdownBean.setDesc2(businessArea.getBusinessAreaName());
	    			     userBean.setBizArea(dropdownBean);
			    }
			    MCostCenter costCenter = userOrganization.getCostCenterId();
			    if(costCenter!=null){
				DropdownBean dropdownBean = new DropdownBean();
	    			     dropdownBean.setId(costCenter.getCostCenterId().toString());
	    			     dropdownBean.setDesc(costCenter.getCostCenterName());
	    			     dropdownBean.setDesc2(costCenter.getCostCenterName());
	    			     userBean.setCosCenter(dropdownBean);
			    }
			    Address govAdd = userOrganization.getAddressId();
			    userBean.setGovAddreNo(govAdd.getNo());
			    userBean.setGovAddreMoo(govAdd.getMoo());
			    userBean.setGovAddreSoi(govAdd.getSoi());
			    userBean.setGovAddreRoad(govAdd.getRoad());
			    userBean.setGovAddreRoomNo(govAdd.getRoomNo());
			  
			    
			    MProvince provinceGov = govAdd.getProvinceId();
			    MAmphur amphurGov = govAdd.getAmphurId();
			    MTambon tambonGov =  govAdd.getTambonId();
			  
				if(null != provinceGov){
					userBean.setGovAddreProvinceId(provinceGov.getProvinceId().toString());
				}
				if(null != amphurGov){
					userBean.setGovAddreAmphurId(amphurGov.getAmphurId().toString());
				}
				if(null != tambonGov){
					userBean.setGovAddreTambonId(tambonGov.getTambonId().toString());
				}
				if(null != govAdd.getPostcode()){
				    userBean.setGovAddrePosCode(govAdd.getPostcode());
				}
			    
			}
			
	

		}
	}


		
		


		return userBean;
	}
    String validateStr = "";
	public Boolean validate(UserBean userBean){
		Boolean result = false;
		if("".equals(userBean.getCitizenid()) || null == userBean.getCitizenid() ){
			validateStr = "Citizen Is Required";
			return  result = true;
		}
		if( null == userBean.getOrgType()){
			validateStr = bundleError.getString("register.orgType");
			return  result = true;
		}
		if(null == userBean.getOrgSubType() ){
			validateStr = bundleError.getString("register.orgSubType");
			return  result = true;
		}
		if(null == userBean.getDept()){
			validateStr = bundleError.getString("register.dept");
			return  result = true;
		}
		if(null == userBean.getBizArea()){
			validateStr = bundleError.getString("register.bizArea");
			return  result = true;
		}
		if(null == userBean.getCosCenter()){
			validateStr = bundleError.getString("register.cosCenter");
			return  result = true;
		}

		return  result;
	}
    @RequestMapping(value = "/updateProfile/{recaptcha}", method = RequestMethod.POST)
	public @ResponseBody String saveUser(@RequestBody UserBean userBean,@PathVariable("recaptcha") String recaptcha,HttpServletRequest request) throws Exception {
		String domainPath = "http://"+request.getServerName()+":"+request.getServerPort() + request.getContextPath();
		String ip = request.getRemoteAddr();
		if(validate(userBean)){

			return "\""+validateStr+"\"";
		}
		String captchaId = recaptcha;
		String captchaCode = userBean.getCaptcha();
		SimpleCaptcha captcha = SimpleCaptcha.load(request);
		boolean isHuman = captcha.validate(captchaCode, captchaId);

		if(!isHuman){
			return "\"Captcha incorrect.\"";
		}
		User  user = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		user = (User) principal;
		Date date = new Date();
		
		UserCenter uc = userService.findOneUserCenter(userBean.getCitizenid());

		

		logger.info("UserCenter - CitizenNo = " + uc.getCitizenNo());
		logger.info("UserCenter - BirthDate = " + uc.getBirthDate());
		logger.info("UserCenter - CreateDate = " + uc.getCreatedDate());
		logger.info("UserCenter - FirstNameEn = " + uc.getFirstNameEn());
		logger.info("UserCenter - MiddleNameTh = " + uc.getMiddleNameTh());
		logger.info("UserCenter - FirstNameTh = " + uc.getFirstNameTh());
		logger.info("UserCenter - IsAccessEwelfare = " + uc.getIsAccessEwelfare());
		logger.info("UserCenter - IsAccessEpayment = " + uc.getIsAccessEpayment());
		logger.info("UserCenter - Password = " + uc.getPassword());
		logger.info("UserCenter - Sex = " + uc.getSex());
		logger.info("Address - Post code = " + userBean.getPostCode());

//		uc.sete
		uc.setUpdatedDate(date);
		uc.setUpdatedBy(user);

		Address add = uc.getAddressId();
		add.setBuildingName(userBean.getBuilding_name());
		add.setVillage(userBean.getVillage());
		add.setNo(userBean.getNo());
		add.setMoo(userBean.getMoo());
		add.setSoi(userBean.getSoi());
		add.setRoad(userBean.getRoad());
		add.setRoomNo(userBean.getRoomNo());
		add.setPostcode(userBean.getPostCode());

		if( null != userBean.getProvinceId()){
    		MProvince province = provinceService.getProvinceById(Integer.parseInt(userBean.getProvinceId()));
    		add.setProvinceId(province);
		}
		if( null != userBean.getAmphurId()){
    		MAmphur amphur = provinceService.getAmphurById(Integer.parseInt(userBean.getAmphurId()));
    		add.setAmphurId(amphur);
		}
		if( null != userBean.getTambonId()){
    		MTambon tambon = provinceService.getSubDistrictById(Integer.parseInt(userBean.getTambonId()));
    		add.setTambonId(tambon);
		}
		uc.setAddressId(add);
		User u  = uc.getUserList().get(0);
		
		logger.info("User - UserName = " + u.getUserName());
		logger.info("User - Passeord = " + u.getPassword());
		logger.info("User - UserCenterId = " + u.getUserCenterId());
		logger.info("User - Email = " + u.getEmail());
		logger.info("User - CreatedDate = " + u.getCreatedDate());
		logger.info("User - Status = " + u.getStatus());
		logger.info("User - UserTypeId = " + u.getUserTypeId());
		logger.info("User - Mobile = " + u.getMobile());
		logger.info("User - CreateBy = " + u.getCreatedBy());
		logger.info("User - UpdateBy = " + u.getUpdatedBy());
		
//		u.setUserName(userBean.getCitizenid());
		u.setMobile(userBean.getMobile());
		u.setEmail(userBean.getEmail());
		u.setUpdatedBy(u);

		if(userBean.getUserType() == 3){


			try{
			    for(UserOrganization userOrganization : u.getUserOrganizationList2()){
				MOrganizationType organTypeTb = organizationTypeService.findMOrganizationTypeById(Integer.parseInt(userBean.getOrgType().getId()));
				MOrganizationSubType organizationSubTypeTb = organizationSubTypeService.findMOrganizationSubTypeById(Integer.parseInt(userBean.getOrgSubType().getId()));
				MDepartment departmentTb = departmentService.findDepartmentById(Integer.parseInt(userBean.getDept().getId()));
				MBusinessArea businessAreaTb = businessAreaService.findBusinessAreaById(Integer.parseInt(userBean.getBizArea().getId()));
				MCostCenter costCenterTb = costCenterService.findMCostCenterById(Integer.parseInt(userBean.getCosCenter().getId()));

				userOrganization.setOrganizationTypeId(organTypeTb);
				userOrganization.setOrganizationSubTypeId(organizationSubTypeTb);
				userOrganization.setDepartmentId(departmentTb);
				userOrganization.setBusinessAreaId(businessAreaTb);
				userOrganization.setCostCenterId(costCenterTb);
				//			    userOrganization.setUserId(u);

				userOrganization.setUpdatedBy(u);
				userOrganization.setUpdatedDate(date);
				userOrganization.setFilepathAuthentication("Path authen");
				Address govAdd  = userOrganization.getAddressId();
				govAdd.setNo(userBean.getGovAddreNo());
				govAdd.setMoo(userBean.getGovAddreMoo());
				govAdd.setSoi(userBean.getGovAddreSoi());
				govAdd.setRoad(userBean.getGovAddreRoad());
				govAdd.setRoomNo(userBean.getGovAddreRoomNo());
				govAdd.setPostcode(userBean.getGovAddrePosCode());
				
				if( null != userBean.getGovAddreProvinceId()){
    				MProvince provinceGov = provinceService.getProvinceById(Integer.parseInt(userBean.getGovAddreProvinceId()));
    				govAdd.setProvinceId(provinceGov);	
				}
				if( null != userBean.getGovAddreAmphurId()){
    				MAmphur amphurGov = provinceService.getAmphurById(Integer.parseInt(userBean.getGovAddreAmphurId()));
    				govAdd.setAmphurId(amphurGov);
				}
				if( null != userBean.getGovAddreTambonId()){
    				MTambon tambonGov = provinceService.getSubDistrictById(Integer.parseInt(userBean.getGovAddreTambonId()));
    				govAdd.setTambonId(tambonGov);
				}
				userOrganization.setAddressId(govAdd);
				
				u.setUserCenterId(uc);
				userOrganization.setUserId(u);
				userOrganization = userOrganizationService.saveUserOrganization(userOrganization);
				
			    }
				
			}catch (Exception e){
				e.printStackTrace();
				return "\"Can not save "+e.getMessage()+"\"";
			}


		}
		return "\"success\"";
	}
}
