package th.go.cgd.epayment.controller.common;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.service.MasterService;
import th.go.cgd.epayment.util.CatalogUtil;

@Controller
public class CommonController
{
	@Autowired
	private CatalogUtil catalogUtil;
	
	@Autowired 
	private MasterService masterService;
	
	@ResponseBody
	@RequestMapping(value="/clearcache", method=RequestMethod.GET)
	public String clearCache() throws IOException 
	{
		try
		{
			catalogUtil.loadCatalog(masterService);
		}
		catch (Exception e)
		{
			return "Clear Cache Fail!";
		}
		
		return "Clear Cache Successfully!";
	}
}
