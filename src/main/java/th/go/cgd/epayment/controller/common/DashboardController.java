package th.go.cgd.epayment.controller.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.service.ChartService;
import th.go.cgd.epayment.service.interfaces.IBaseUrlService;

@Controller
@RequestMapping("/")
public class DashboardController {
	
	private static final String DASHBOARD = "views/dashboard";
	
	@Autowired
	private ChartService chartService;
	@Autowired
	private IBaseUrlService baseUrlService;
	
	@RequestMapping
	public String index(Model model, UserSession userSession) throws Exception {
		model.addAttribute("totalCase", "");
		model.addAttribute("chartStatDamageCivilData", new ObjectMapper().writeValueAsString(chartService.getChartStatDamageCivilModel(userSession)));
		model.addAttribute("chartStatWorkData", new ObjectMapper().writeValueAsString(chartService.getChartStatWorkModel(userSession)));
		model.addAttribute("chartStatCaseData", new ObjectMapper().writeValueAsString(chartService.getChartStatCaseModel(userSession)));
		model.addAttribute("chartStatLoanEducationData", new ObjectMapper().writeValueAsString(chartService.getChartStatLoanEducationModel(userSession)));
		model.addAttribute("chartStatLoanEducationBreakData", new ObjectMapper().writeValueAsString(chartService.getChartStatLoanEducationBreakModel(userSession)));
		return DASHBOARD;
	}
	
//	@RequestMapping
//	public String index(Model model, UserSession userSession) throws Exception {
//		
//		return "views/Example/example2";
//	}

	public IBaseUrlService getBaseUrlService() {
	    return baseUrlService;
	}

	public void setBaseUrlService(IBaseUrlService baseUrlService) {
	    this.baseUrlService = baseUrlService;
	}
	
	
}
