package th.go.cgd.epayment.controller.common;

import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;
import com.sun.jersey.client.apache.ApacheHttpClient;

import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.model.EmailBean;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.service.interfaces.IEmailServiceImpl;

/**
 * @author torgan.p 27 ธ.ค. 2560 20:50:15
 *
 */
@Controller
@RequestMapping("epaymentApiService")
public class EpaymentApiController {
	private static final Logger logger = Logger.getLogger(EpaymentApiController.class);
//	@Autowired
//	private IEmailServiceImpl emailService;
	@Autowired
	private UserService userService;
//	@Autowired 
//	private MPermissionGroupService permissionGroupService;
//	@Autowired
//	private IMDepartmentService departmentService;
//	@Autowired
//	private IMOrganizationTypeService organizationTypeService;
//	@Autowired
//	private IUserOrganizationService userOrganizationService;
//	@Autowired
//	private IMOrganizationSubTypeService organizationSubTypeService;
//	@Autowired
//	private IMBusinessAreaService businessAreaService;
//	@Autowired
//	private IMCostCenterService costCenterService;
//	@Autowired
//	private IUserPermissiongroupService userPermissiongroupService;
//	
//	@Autowired
//	private IProvinceDropdownService iProvinceDropdownService;
//	@Autowired
//	private IUserFavoriteService iUserFavoriteService;
//	@Autowired
//	private IManageCatalogService manageCatalogService;
//	@Autowired
//	private IBillService billService;
	@Autowired
	private IEmailServiceImpl emailService;
	public static ResourceBundle bundle = ResourceBundle.getBundle("TemplateEmail");
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
	
	private static final Locale THAI_LOCALE = new Locale("en", "EN");
	private static final String SHORT_DATE_PATTERN_TH = "ddMMyyyy";
	@Autowired
	private HttpServletRequest request;
	@Autowired
	private HttpServletResponse response;
	
	
	
	 @RequestMapping(value = "/updateUserStatus", method = RequestMethod.GET)
		public void updateUserStatus(HttpServletRequest request, HttpServletResponse response) throws Exception {
	     InetAddress ip = InetAddress.getLocalHost();
//	     logger.info(">>>>>>>> getLocalAddr <<<<<<<<<<"+  request.getLocalAddr());
//	     logger.info(">>>>>>>> getContextPath <<<<<<<<<<"+  request.getContextPath());
//	     logger.info(">>>>>>>> getServerName <<<<<<<<<<"+    request.getServerName());
//	     logger.info(">>>>>>>> getServerPort <<<<<<<<<<"+    request.getServerPort());
//	     logger.info(">>>>>>>> getServletPath <<<<<<<<<<"+    request.getServletPath());
//	     logger.info(">>>>>>>> getServletContext <<<<<<<<<<"+    request.getServletContext());
//	     logger.info(">>>>>>>> getRemoteAddr <<<<<<<<<<"+    request.getRemoteAddr());
//	     logger.info(">>>>>>>> getRemoteHost <<<<<<<<<<"+    request.getRemoteHost());
//	     logger.info(">>>>>>>> getLocalAddr <<<<<<<<<<"+    request.getLocalAddr());
//	     logger.info(">>>>>>>> getProtocol <<<<<<<<<<"+    request.getProtocol());
//	     logger.info(">>>>>>>> getRequestURI <<<<<<<<<<"+    request.getRequestURI());
//	     logger.info(">>>>>>>> getHostAddress <<<<<<<<<<"+    ip.getHostAddress());
//	     logger.info(">>>>>>>> getHostName <<<<<<<<<<"+    ip.getHostName());
	     	String domainPath = "";
	     	String username = request.getParameter("username");
	     	User user = null;
	     	if(username != null){
        	     	String arrayName [] = decodeBase64(username).split("\\|");
        	     	user = userService.findByUserNameByStutas(arrayName[0]);
	     	}

	     	if(null!=user){
	     	   
	     	    userService.updateUserStatus(user);
	     
	     	try{
	     	    	UserCenter userCenter = user.getUserCenterId();
			EmailBean mail = new EmailBean();
			if(user.getUserTypeId() == 2){
				mail.setTo(new String[]{userCenter.getCompanyAuthEmail()});
			}else{
				mail.setTo(new String[]{user.getEmail()});
				
			}
			if(user.getUserTypeId() == 3){
			    domainPath = "http://"+request.getServerName()+":"+request.getServerPort() + request.getContextPath();
			}else{
			    domainPath = "http://119.160.217.6/e-payment/home";
			}
			
			String fullName = (userCenter.getFirstNameTh()!= null ? userCenter.getFirstNameTh() : "")+" "+(userCenter.getLastNameTh() != null ? userCenter.getLastNameTh() : "");

			Object[] objArray = {fullName};
			  String  content = emailService.getTemplateMail(objArray,"register");
			  mail.setSubject(bundle.getString("mail.subject.register"));
			  mail.setText(content);
			  emailService.sendMail(mail);
			  logger.info(">>>>>>>> Send Email Success <<<<<<<<<<");
		}catch (Exception e){
		    e.printStackTrace();
			logger.info(e.getMessage());

		}			
	     }
	     	redirectStrategy.sendRedirect(request, response, domainPath);
	 }
	    public String decodeBase64(String usernane){
		 byte[] valueDecoded= Base64.decodeBase64(usernane);
		    String decoded = new String(valueDecoded);
		    logger.info("Decoded value is usernane " + decoded);
		    return decoded;
	    }
	    
	    
	    

	        @RequestMapping(value = "/forgetPassword", method = RequestMethod.GET)
		public void forgetPassword(HttpServletRequest request, HttpServletResponse response) throws Exception {
	 	String citizenid = request.getParameter("citizenid");
	 	String email = request.getParameter("email");
	 	String domainPath = "http://"+request.getServerName()+":"+request.getServerPort() + request.getContextPath();

	 	 logger.info(citizenid);
	 	 logger.info(email);
        	 	if(null != citizenid  && null != email){
        	 	   User user = userService.findOneUserByCitizen(citizenid);
        	 	   if(user!=null){
        	 	      UserCenter userCenter = user.getUserCenterId();
        	 	      String fullName = userCenter.getFirstNameTh()+" "+userCenter.getLastNameTh();
                	 	   Date dt = new Date();
                	 	   Calendar c = Calendar.getInstance(); 
                	 	   c.setTime(dt); 
                	 	   c.add(Calendar.DATE, 1);
                	 	   dt = c.getTime();
                	 	   String citizenidTemp = encodeBase64(citizenid+"|"+"5a0a6a5d3dbe62171ce59841"+"|"+formatDateTH(dt));
                	 	   EmailBean mail = new EmailBean();
                	 	   mail.setTo(new String[]{email});
                	 	   Object[] objArray = {domainPath,citizenid,citizenidTemp,fullName};
                	 	   String  content = emailService.getTemplateMail(objArray,"forgot.password");
                	 	   mail.setSubject(bundle.getString("forgot.subject.password"));
                	 	   mail.setText(content);
                	 	   emailService.sendMail(mail);
                	 	   logger.info(">>>>>>>> Send Email Success forget password<<<<<<<<<<");
        	 	   }
        	 	}
	    	}
	    
	    @RequestMapping(value = "/redirectForgetPass", method = RequestMethod.GET)
		public void redirectForgetPass(HttpServletRequest request, HttpServletResponse response) throws Exception {
	 	String citizenid = request.getParameter("citizenid");
	 	String urlRedirect = "http://"+request.getServerName()+":"+request.getServerPort() + request.getContextPath()+"/changePassword";
		
	 	if(citizenid != null){
	 	String citizenTemp [] = decodeBase64(citizenid).split("\\|");
	 	  List<javax.ws.rs.core.Cookie> cookies = loginApp(citizenTemp[0]);
		  javax.servlet.http.Cookie cookie  = new javax.servlet.http.Cookie( "JSESSIONID", cookies.get(0).getValue());
		  cookie.setPath(request.getContextPath());
		  response.addCookie(cookie);
		  response.setContentType("application/json;charset=UTF-8");
	 	}
		  redirectStrategy.sendRedirect(request, response,urlRedirect);

	    	} 
	    
	    
	    
	    
	    
		public List<javax.ws.rs.core.Cookie> loginApp(String citizenid) {
		     String domainPath = "http://"+request.getServerName()+":"+request.getServerPort() + request.getContextPath();
		    logger.info("loginApp" );
			List<javax.ws.rs.core.Cookie> cookies = null;
		        try{
		            ClientConfig config = new DefaultClientConfig();
		            ApacheHttpClient client = ApacheHttpClient.create(config);
		            Form form = new Form();
		            form.add("username", citizenid);
		            form.add("secretkey", "5a0a6a5d3dbe62171ce59841");

		            WebResource webResource = client.resource(UriBuilder.fromUri(domainPath+"/login").build());
		            ClientResponse serviceResponse = webResource.type(MediaType.APPLICATION_FORM_URLENCODED)
		        	    .header("header","Accept")
		        	    .accept("Accept","application/json")
		        	    .post(ClientResponse.class, form);
		            cookies = new ArrayList<javax.ws.rs.core.Cookie>(serviceResponse.getCookies());
		            
		            logger.info("cookies : "+cookies);

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        return cookies;
		    }
	    public static String formatDateTH(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat(SHORT_DATE_PATTERN_TH,THAI_LOCALE);
		String date = sdf.format(d);
		 System.out.println("date Str : "+date);
		return date;
	    }
	    @RequestMapping(value = {"/encodeBase64"}, method=RequestMethod.GET)
	    public @ResponseBody String encodeBase64(@RequestParam(value="username", required=false )  String username){
		  String encoded = "";   
		  logger.info(username);
		       byte[]   bytesEncoded = Base64.encodeBase64(username.getBytes());
		     encoded = new String(bytesEncoded);
		    System.out.println("ecncoded value is usernane " + encoded);
		    return encoded.toString();
	    }

}
