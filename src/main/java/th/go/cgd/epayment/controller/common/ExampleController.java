package th.go.cgd.epayment.controller.common;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;

import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.ExampleBean;
import th.go.cgd.epayment.model.InvoiceCodeBean;
import th.go.cgd.epayment.service.interfaces.IExampleService;
import th.go.cgd.epayment.service.interfaces.IGenerateCodeService;



/**
 * @author torgan.p 10 ต.ค. 2560 17:50:25
 *
 */
@RestController
@RequestMapping("/exampleController")
public class ExampleController {
	@Autowired
	private IExampleService exampleService;

	@Autowired
	private IGenerateCodeService generateCodeService;
	
	@RequestMapping(value = "/viewPage", method = RequestMethod.GET)
	public ModelAndView viewPage(ModelMap model) {
		System.out.println("viewPage");

		model.addAttribute("role", "cccc");
		return new ModelAndView("views/Example/example", model);
	}
	
	@RequestMapping(value = "/viewPage2", method = RequestMethod.GET)
	public ModelAndView viewPage2(ModelMap model) {
		System.out.println("viewPage2");

		return new ModelAndView("views/Example/example2", model);
	}
	
	@RequestMapping(value = "/bootstrapHeading", method = RequestMethod.GET)
	public ModelAndView bootstrapHeading(ModelMap model) {
		System.out.println("bootstrapHeading");

		return new ModelAndView("views/Example/bootstrap-heading", model);
	}

	@RequestMapping(value = "/saveExample", method = RequestMethod.POST)
	public @ResponseBody void saveExample(@RequestBody ExampleBean exampleBean) throws Exception {
		System.out.println("saveExample ");
		exampleService.save(exampleBean);
	}

	@RequestMapping(value = "/findAllExampleByName", method = RequestMethod.GET)
	public @ResponseBody List<ExampleBean> findAllExampleByName() throws Exception {
		System.out.println("findAllExampleByName");
		return exampleService.getAll();
	}

	@RequestMapping(value = "/updateExample", method = RequestMethod.PUT)
	public @ResponseBody void updateExample(@RequestBody ExampleBean exampleBean) throws Exception {
		System.out.println("updateExample");
		//	    System.out.println("updateExample : "+exampleBean);
		exampleService.update(exampleBean);

	}
	
	@RequestMapping(value = "/deleteExample/{id}", method = RequestMethod.GET)
	public @ResponseBody void deleteExample(@PathVariable("id") Integer id) throws Exception {
		System.out.println("updateExample");
		//	    System.out.println("updateExample : "+exampleBean);
		exampleService.delete(id);

	}
	
	@RequestMapping(value = "/postExample", method = RequestMethod.POST)
	public @ResponseBody ExampleBean  postExample() throws Exception {
		System.out.println("postExample");
		//	    System.out.println("updateExample : "+exampleBean);
//		exampleService.update(exampleBean); 

		ExampleBean example = new ExampleBean();
		example.setId(001);
		example.setFname("aaa");
		example.setLname("bbbb");
		return example;

	}
	
	@RequestMapping(value = "/getExample", method = RequestMethod.GET)
	public @ResponseBody ExampleBean getExample() throws Exception {
		System.out.println("getExample");
		ExampleBean example = new ExampleBean();
		example.setId(002);
		example.setFname("ccc");
		example.setLname("ddd");
		return example;

	}
	
	//	    Seatch keyword
	@RequestMapping(value = "/angucomplete",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> angucomplete() {
	    System.out.println("example1 ");
	    List<DropdownBean> dropdownBeanList = exampleService.findExampleByIdNativeQuery(null);
	    return dropdownBeanList;
	}

	//	    pagination
	@RequestMapping(value = "/pagination",method=RequestMethod.POST)
	public Page<ExampleBean> pagination() throws JsonProcessingException {
	    System.out.println("pagination ");
	    return exampleService.findAll();
	}

	@RequestMapping(value = "/getSequence/{sequenceName}",method=RequestMethod.GET)
	public @ResponseBody InvoiceCodeBean getSequence(@PathVariable("sequenceName") String sequenceName) throws JsonProcessingException {
	    System.out.println("sequenceName " + sequenceName);
	    return generateCodeService.generateInvoiceCode(new Date());
	}

}
