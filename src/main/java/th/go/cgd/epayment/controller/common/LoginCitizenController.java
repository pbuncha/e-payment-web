package th.go.cgd.epayment.controller.common;

import java.util.Calendar;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.service.UserService;

@Controller
public class LoginCitizenController {
	
	private static final Logger logger = Logger.getLogger(LoginController.class);
	private static final String LOG_IN = "IN";
	private static final String LOG_OUT = "OT";
	private static final String SUCCESS = "Success";
	
	@Autowired
	private UserService userService;
	
//	@Autowired
//	private LogAccessService logAccessService;
	
	@RequestMapping(value = {"/login1"})
	public String loginCitizen(Model model, HttpServletRequest req) 
	{
		System.out.println("In /login_citizen login1 : "  + req.toString());
		//System.out.println(BCrypt.hashpw("p@ssw0rd", BCrypt.gensalt()));
	    return "login_citizen";
	}
	
	@RequestMapping(value = {"/login2"})
	public String loginGoverment(Model model, HttpServletRequest req) 
	{
		System.out.println("In /login_goverment login : "  + req.toString());
		//System.out.println(BCrypt.hashpw("p@ssw0rd", BCrypt.gensalt()));
	    return "login_goverment";
	}
	
	/*@RequestMapping(value = {"/logout"})
	public String logout(HttpServletRequest request) throws Exception {
		if (request.getSession() != null && request.getSession().getAttribute(EPaymentConstant.USER_SESSION) != null) {			
			//createLogAccess(request, LOG_OUT);
			request.getSession().invalidate();
		}
	    return "redirect:/login_citizen";
	}*/
	
	@RequestMapping(value = {"/login_citizen/success"})
	public String goToMain(Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception
	{
		System.out.println("In /login_citizen/success goToMain");
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
		
		//	    logger.info("Welcome to User :::::::> " + user.getUserId());
	    
	    User userFromDb = userService.get(user.getUserId());
	    userFromDb.setLastLogin(Calendar.getInstance().getTime());
	    
	    userService.updateLoginTime(userFromDb);
//	    
	    userService.setUserSession(userFromDb, request);
	    
//		createLogAccess(request, LOG_IN);
		
		
		return "redirect:/";
	}
}
