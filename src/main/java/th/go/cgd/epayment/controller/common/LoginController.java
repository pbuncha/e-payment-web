package th.go.cgd.epayment.controller.common;

import java.util.Calendar;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.LogAccess;
import th.go.cgd.epayment.entity.MLogAction;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.util.ClientMachineUtil;
import th.go.cgd.epayment.util.IPAddressUtil;

@Controller
public class LoginController {

	private static final Logger logger = Logger.getLogger(LoginController.class);
	private static final String LOG_IN = "IN";
	private static final String LOG_OUT = "OT";
	private static final String SUCCESS = "Success";
	
	@Autowired
	private UserService userService;
	
//	@Autowired
//	private LogAccessService logAccessService;
	
	
	@RequestMapping(value = {"/login"})
	public String goToLoginPage(Model model, HttpServletRequest req) 
	{
		System.out.println("In /login");
		System.out.println("req.getQueryString() = " + req.getQueryString());
	    return "login_g";
	}
	
	@RequestMapping(value = {"/login_goverment"})
	public String goToGLoginPage(Model model, HttpServletRequest req) 
	{
		System.out.println("In /login_g");
	    return "login_g";
	}
	
	@RequestMapping(value = {"/login/submit"})
	public String successSubmit(Model model, HttpServletRequest req) 
	{
		System.out.println("In /login/submit");
	    return "login";
	}
	
	@RequestMapping(value = {"/logout"})
	public String logout(HttpServletRequest request) throws Exception {
		if (request.getSession() != null && request.getSession().getAttribute(EPaymentConstant.USER_SESSION) != null) {			
			createLogAccess(request, LOG_OUT);
			request.getSession().invalidate();
		}
	    return "redirect:/login";
	}
	
	@RequestMapping(value = {"/login/success"})
	public String goToMain(Model model, HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception
	{
		System.out.println("In /login/success");
//		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//		System.out.println(SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString());
		
		//	    logger.info("Welcome to User :::::::> " + user.getUserId());
	    
	    User userFromDb = userService.get(2);//user.getUserId());
//	    userFromDb.setLastLogin(Calendar.getInstance().getTime());
	    
//	    userService.updateLoginTime(userFromDb);
//	    
//	    userService.setUserSession(userFromDb, request);
	    
//		createLogAccess(request, LOG_IN);
		
		
		return "redirect:/";
	}
	
	private void createLogAccess(HttpServletRequest request, String method) throws Exception {
		MLogAction mLogAction = new MLogAction();
		mLogAction.setActionId(method);
		LogAccess logAccess = new LogAccess();
		logAccess.setAccessDate(Calendar.getInstance().getTime());
		logAccess.setClientIp(IPAddressUtil.getClientAddress(request));
		logAccess.setClientName(ClientMachineUtil.getClientMachineName(request));
		logAccess.setExecutionTime((short) 0);
//		logAccess.setMLogAction(mLogAction);
		logAccess.setStatus(SUCCESS);
		logAccess.setUrl(request.getRequestURL().toString());
//		logAccess.setUserName(((UserSession) request.getSession().getAttribute(EPaymentConstant.USER_SESSION)).getUser().getUserName());
//		logAccessService.save(logAccess);
	}
}
