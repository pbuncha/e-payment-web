package th.go.cgd.epayment.controller.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.constant.EPaymentConstant.REPORT_TYPE;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.CatalogBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.FileBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.model.master.AccountDepositBean;
import th.go.cgd.epayment.model.master.CostCenterBean;
import th.go.cgd.epayment.model.master.DepartmentBean;
import th.go.cgd.epayment.model.master.GlBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.model.master.StandardRevernueBean;
import th.go.cgd.epayment.service.admin.ProgramService;
import th.go.cgd.epayment.service.interfaces.IManageCatalogService;
import th.go.cgd.epayment.service.interfaces.IMasterService;


@RestController
@RequestMapping("/manageCatalogController")
public class ManageCatalogController {
	@Autowired
	private IManageCatalogService manageCatalogService;
	@Autowired
	private IMasterService masterService;
	@Autowired
	private ProgramService programService;
	
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView search(ModelMap model) {
		System.out.println("search");
		
		
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("button", programService.getPermissionButtonAccess( "CTL10000" , user));
		
		
		return new ModelAndView("views/manageCatalog/ctl10000-search", model);
	}
	
	@RequestMapping(value = "/pagination",method=RequestMethod.POST)
	public Page<CatalogBean> pagination(@RequestBody SearchBean catalogBean) throws JsonProcessingException {
	    System.out.println("newfear pagination");
	    return manageCatalogService.search(catalogBean);
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(ModelMap model) {
		System.out.println("create");
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		model.addAttribute("button", programService.getPermissionButtonAccess( "CTL51000" , user));
		return new ModelAndView("views/manageCatalog/ctl20000-create", model);
	}
	
	@RequestMapping(value = "/getMCatalogType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCatalogType() {
	    System.out.println("getMCatalogType");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getMCatalogType();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getMRevenueType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMRevenueType() {
	    System.out.println("getMRevenueType");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getMRevenueType();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getMLevyType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMLevyType() {
	    System.out.println("getMLevyType");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getMLevyType();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getCostCenter",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCostCenter() {
	    System.out.println("getCostCenter");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getCostCenter();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getCostCenterByCode/{costCenterCode}",method=RequestMethod.GET)
	public @ResponseBody CostCenterBean getCostCenterByCode (@PathVariable("costCenterCode") String costCenterCode) {
	    System.out.println("getCostCenterByCode : "+costCenterCode);
	    CostCenterBean costCenterBean = manageCatalogService.getCostCenterByCode(costCenterCode);
	    return costCenterBean;
	}
	
	@RequestMapping(value = "/getDepartmentByCode/{departmentCode}",method=RequestMethod.GET)
	public @ResponseBody DepartmentBean getDepartmentByCode (@PathVariable("departmentCode") String departmentCode) {
	    System.out.println("getDepartmentByCode : "+departmentCode);
	    DepartmentBean departmentBean = manageCatalogService.getDepartmentByCode(departmentCode);
	    return departmentBean;
	}
	
	@RequestMapping(value = "/getDepartmentById/{departmentId}",method=RequestMethod.GET)
	public @ResponseBody DepartmentBean getDepartmentById (@PathVariable("departmentId") String departmentId) {
	    System.out.println("getDepartmentById : "+departmentId);
	    DepartmentBean departmentBean = manageCatalogService.getDepartmentById(departmentId);
	    return departmentBean;
	}
	
	@RequestMapping(value = "/getGlByCode/{glCode}",method=RequestMethod.GET)
	public @ResponseBody GlBean getGlByCode (@PathVariable("glCode") String glCode) {
	    System.out.println("getGlByCode : "+glCode);
	    GlBean glBean = manageCatalogService.getGlByCode(glCode);
	    return glBean;
	}
	
	
	@RequestMapping(value = "/ctl20001-item", method = RequestMethod.GET)
	public ModelAndView addInvoiceItemPage(ModelMap model){
		return new ModelAndView("views/manageCatalog/ctl20001-item",model);
	}
	
	@RequestMapping(value = "/saveCatalog", method = RequestMethod.POST)
	public @ResponseBody void saveCatalog(@RequestBody CatalogBean catalogBean) throws Exception {
		System.out.println("saveCatalog ");
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		manageCatalogService.save(catalogBean ,user.getUserId());
	}
	
	@RequestMapping(value = "/removeCatalog", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<String> removeCatalog(@RequestBody List<CatalogBean> catalogBean){
		ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
		try {
			manageCatalogService.removeCatalog(catalogBean);
		} catch (Exception e) {
			response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	@RequestMapping(value = "/removeCatalogById/{id}", method = RequestMethod.GET)
	public @ResponseBody HttpEntity<String> removeCatalogById(@PathVariable("id") String id){
		ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
		try {
//		    if(){
//			
//		    }
			manageCatalogService.removeCatalogById(Integer.parseInt(id));
		} catch (Exception e) {
			response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}
	
	@RequestMapping(value = "/getStatusUse",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCatalogByCatType() {
	    List<DropdownBean> dropdownBeanList = masterService.getStatusUse(EPaymentConstant.STATUS_ID);
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getCatalogAllByStatus",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCatalogAllByStatus() {
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getCatalogAllByStatus();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/exportExcel", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean> exportExcel(@RequestBody List<CatalogBean> catalogBean){
		final String rpt_name = "catalog.xlsx";
		byte[] contents = null;
		contents = manageCatalogService.exportExcel(catalogBean);
		FileBean bean = new FileBean(contents, rpt_name);
		bean.setFileType(REPORT_TYPE.getReportType("EXCEL"));
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename="+rpt_name);
		ResponseEntity<FileBean> response = new ResponseEntity<FileBean>(bean, headers, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value = "/editCatalog/{id}", method = RequestMethod.GET)
	public ModelAndView updatePage(@PathVariable("id") Integer id){
		ModelAndView view = new ModelAndView();
		view.setViewName("views/manageCatalog/ctl20000-create");
//		view.addObject("isLogin", false);
//		//~ Check User Login
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		  if ((principal != null) && (principal instanceof User)) {
		    view.addObject("isLogin", true);
		}
		view.addObject("catalog", manageCatalogService.getCatalog(id));
		return view;
	}
	
	@RequestMapping(value = "/getAccountDepositByCode/{accountDepositCode}",method=RequestMethod.GET)
	public @ResponseBody AccountDepositBean getAccountDepositByCode (@PathVariable("accountDepositCode") String accountDepositCode) {
	    System.out.println("getAccountDepositByCode : "+accountDepositCode);
	    AccountDepositBean accountDepositBean = manageCatalogService.getAccountDepositByCode(accountDepositCode);
	    return accountDepositBean;
	}
	
	@RequestMapping(value = "/getStandardRevernueByGlCode/{glCode}",method=RequestMethod.GET)
	public @ResponseBody StandardRevernueBean getStandardRevernueByGlCode (@PathVariable("glCode") String glCode) {
	    System.out.println("getStandardRevernueByGlCode : "+glCode);
	    StandardRevernueBean standardRevernueBean = manageCatalogService.getStandardRevernueByGlCode(glCode);
	    return standardRevernueBean;
	}
	
	@RequestMapping(value = "/getMMoneySourceByGlCode/{glCode}",method=RequestMethod.GET)
	public @ResponseBody MoneySourceBean getMMoneySourceByGlCode (@PathVariable("glCode") String glCode) {
	    System.out.println("getMMoneySourceByGlCode : "+glCode);
	    MoneySourceBean moneySourceBean = manageCatalogService.getMMoneySourceByGlCode(glCode);
	    return moneySourceBean;
	}
	
	@RequestMapping(value = "/copyCatalog", method = RequestMethod.POST)
	public @ResponseBody void copyCatalog(@RequestBody DropdownBean catalogCopy) throws Exception {
		System.out.println("copyCatalog ");
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		manageCatalogService.copyCatalog(catalogCopy ,user.getUserId());
	}

}
