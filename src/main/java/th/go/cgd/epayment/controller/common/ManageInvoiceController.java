/**
 * 
 */
package th.go.cgd.epayment.controller.common;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;

import net.sf.jasperreports.engine.JRException;
import th.go.cgd.epayment.constant.EPaymentConstant.REPORT_TYPE;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.FileBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.InvoiceLoaderBean;
import th.go.cgd.epayment.model.ProvinceDropdownBean;
import th.go.cgd.epayment.report.service.AbstractJasperReport.ReportType;
import th.go.cgd.epayment.service.impl.InvocePostReportServiceImpl;
import th.go.cgd.epayment.service.impl.ReportServiceImpl;
import th.go.cgd.epayment.service.interfaces.IBarcodeService;
import th.go.cgd.epayment.service.interfaces.IInvoiceLoaderService;
import th.go.cgd.epayment.service.interfaces.IInvoiceService;
import th.go.cgd.epayment.service.interfaces.IManageCatalogService;
import th.go.cgd.epayment.service.interfaces.IMasterService;
import th.go.cgd.epayment.service.interfaces.IProvinceDropdownService;
import th.go.cgd.epayment.util.CalendarHelper;

/**
 * @author wichuda.k Oct 31, 2017
 *
 */
@RestController
@RequestMapping("/manageInvoice")
public class ManageInvoiceController extends AbstractBaseController {

	private static final Logger logger = Logger.getLogger(ManageInvoiceController.class);
	private @Autowired InvocePostReportServiceImpl invocePostReportService;
	@Autowired IMasterService masterService;
	@Autowired IInvoiceService invoiceService;
	@Autowired IProvinceDropdownService iProvinceDropdownService;
	@Autowired ReportServiceImpl reportService;
	@Autowired IBarcodeService barcodeService;
	@Autowired IManageCatalogService manageCatalogService;
	@Autowired IInvoiceLoaderService invoiceLoaderService;

	/** 
	 * @author buncha.p
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/import-invoice", method = RequestMethod.GET)
	public ModelAndView importInvoiceHome(ModelMap model) {
		logger.info("Payment");
		ModelAndView view = new ModelAndView();
		view.setViewName("views/manageInvoice/import-invoice/import-invoice");
		view.addObject("isLogin", false);
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ((principal != null) && (principal instanceof User)) {
			model.addAttribute("isLogin", true);
		}
		view.addAllObjects(model);
		return view;
	}


	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView searchPage(ModelMap model){
		return new ModelAndView("views/manageInvoice/ctl70000-search",model);
	}

	@RequestMapping(value = "/pagination",method=RequestMethod.POST)
	public Page<Object> pagination() throws JsonProcessingException {
		System.out.println("manageInvoice/pagination ");
		return new PageImpl<Object>(new ArrayList<Object>(), new PageRequest(1, 10), 10);
	}

	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView createPage(ModelMap model){
		model = new ModelMap();
		model.addAttribute("isLogin", false);
		//~ Check User Login
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ((principal != null) && (principal instanceof User)) {
			model.addAttribute("isLogin", true);
		}
		return new ModelAndView("views/manageInvoice/ctl80000-create",model);
	}

	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public ModelAndView updatePage(@PathVariable("id") Integer id){
		ModelAndView view = new ModelAndView();
		view.setViewName("views/manageInvoice/ctl80000-create");
		view.addObject("isLogin", false);
		//~ Check User Login
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ((principal != null) && (principal instanceof User)) {
			view.addObject("isLogin", true);
		}
		view.addObject("invoice", invoiceService.getInvoice(id));
		return view;
	}

	@RequestMapping(value = "/getInvoice/{id}", method = RequestMethod.GET)
	public InvoiceBean getInvoice(@PathVariable("id") Integer id){
		return invoiceService.getInvoice(id);
	}

	@RequestMapping(value = "/getTitleList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getTitleList() {
		List<DropdownBean> dropdownBeanList = masterService.getTitleList();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getPersonTypeList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getPersonTypeList() {
		List<DropdownBean> dropdownBeanList = masterService.getPersonTypeList();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getdeptList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getdeptList() {
		List<DropdownBean> dropdownBeanList = masterService.getMDepartmentList();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getMCostCenterList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCostCenterList() {
		List<DropdownBean> dropdownBeanList = masterService.getMCostCenterList();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getCatalogList/{mCatalogId}",method=RequestMethod.GET)
	public @ResponseBody  List<DropdownBean> getCatalogList(@PathVariable("mCatalogId") Integer mCatalogId) {
		List<DropdownBean> dropdownBeanList = invoiceService.getCatalogList(mCatalogId);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/ctl80001-item", method = RequestMethod.GET)
	public ModelAndView invoiceItemPage(ModelMap model){
		return new ModelAndView("views/manageInvoice/ctl80001-item",model);
	}


	@RequestMapping(value = "/saveInvoice",method=RequestMethod.POST)
	public @ResponseBody InvoiceBean saveInvoice(@RequestBody InvoiceBean invoiceBean) {
		logger.info("save Invoice");
		return invoiceService.save(invoiceBean);
	}

	@RequestMapping(value = "/getInvoiceList",method=RequestMethod.POST)
	public List<InvoiceBean> getInvoiceList(@RequestBody InvoiceBean invoiceBean){
		System.out.println("manageInvoice/getInvoiceList ");
		List<InvoiceBean> result = invoiceService.getInvoiceByCriteria(invoiceBean);

		return result;
	}

	@RequestMapping(value = "/success", method = RequestMethod.GET)
	public ModelAndView successPage(ModelMap model){
		return new ModelAndView("views/manageInvoice/invoice-success",model);
	}

	@RequestMapping(value = "/getProvince", method = RequestMethod.GET)
	public @ResponseBody List<ProvinceDropdownBean> getProvince() {
		logger.info("getProvince");
		return iProvinceDropdownService.getAllprovince();
	}

	@RequestMapping(value = "/getDistrict/{id}", method = RequestMethod.GET)
	public @ResponseBody List<ProvinceDropdownBean> getDistrict(@PathVariable("id") Integer id) {
		logger.info("getDistrict");
		return iProvinceDropdownService.getDistrictByProvince(id);
	}

	@RequestMapping(value = "/getSubDistrict/{id}", method = RequestMethod.GET)
	public @ResponseBody List<ProvinceDropdownBean> getSubDistrict(@PathVariable("id") Integer id) {
		logger.info("getSubDistrict");
		return iProvinceDropdownService.getSubDistrictByDistrict(id);
	}

	@RequestMapping(value = "/getInvoiceRpt/{id}", method = RequestMethod.GET)	 
	public  @ResponseBody byte[] getRpt1(@PathVariable("id") Integer id){
		logger.info("getInvoiceRpt : "+id);
		List<String> idList = new ArrayList<String>();
		idList.add(id.toString());
		HashMap<String,Object> params = new HashMap<>();
		ClassLoader classLoader = getClass().getClassLoader();
		logger.info("path : "+classLoader.getResource("jasper").getPath());
		params.put("invoiceId", idList);
		params.put("REPORT_DIR",classLoader.getResource("jasper").getPath());
		byte[] contents = null;
		try{
			contents = invocePostReportService.genarateReport(params, ReportType.PDF);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE ));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			headers.add("Content-Disposition", "attachment"); 
			logger.info("END getInvoiceRpt : "+id);
			ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			return contents;

		} catch ( JRException e) {
			e.printStackTrace();
		}
		return null ;
	}
	@RequestMapping(value = "/getInvoiceNormalRpt/{id}", method = RequestMethod.GET)	 
	public  @ResponseBody byte[] getRpt2(@PathVariable("id") Integer id) throws JRException{
		logger.info("getInvoiceRpt : "+id);
		byte[] contents = null;
		contents = invoiceService.generateInvoiceRpt(invoiceService.getInvoidById(id));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE ));
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.add("Content-Disposition", "attachment"); 
		logger.info("END getInvoiceRpt : "+id);
		ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
		return contents;

	}

	@RequestMapping(value = "/getMultiInvoiceRpt/{id}", method = RequestMethod.GET)	 
	public  @ResponseBody byte[] getMultiInvoiceRpt(@PathVariable("id") String id){
		logger.info("getMultiInvoiceRpt : "+id);
		List<String> idList = Arrays.asList(id.split(","));
		HashMap<String,Object> params = new HashMap<>();
		ClassLoader classLoader = getClass().getClassLoader();
		logger.info("path : "+classLoader.getResource("jasper").getPath());
		params.put("invoiceId", idList);
		params.put("REPORT_DIR",classLoader.getResource("jasper").getPath());
		byte[] contents = null;
		try{
			contents = invocePostReportService.genarateReport(params, ReportType.PDF);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE ));
			headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			headers.add("Content-Disposition", "attachment"); 
			logger.info("END getInvoiceRpt : "+id);
			ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			return contents;

		} catch ( JRException e) {
			e.printStackTrace();
		}
		return null ;
	}

	@RequestMapping(value = "/cancel/{id}", method = RequestMethod.GET)
	public @ResponseBody HttpEntity<String> removeCatalog(@PathVariable("id") Integer id){
		ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
		try {
			invoiceService.cancelInvoice(id);
		} catch (Exception e) {
			response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}

	@RequestMapping(value = "/cancelList", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<String> removeCatalog(@RequestBody List<Integer> id){
		ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
		try {
			invoiceService.cancelInvoice(id);
		} catch (Exception e) {
			response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}

	@RequestMapping(value = "/exportExcel", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean> exportExcel(@RequestBody List<InvoiceBean> invoiceBeans){
		final String rpt_name = "invoice_"+CalendarHelper.formatDateTH2(CalendarHelper.getDateTime(new Date()))+".xlsx";
		byte[] contents = null;
		contents = invoiceService.exportExcel(invoiceBeans);
		FileBean bean = new FileBean(contents, rpt_name);
		bean.setFileType(REPORT_TYPE.getReportType("EXCEL"));
		HttpHeaders headers = new HttpHeaders();
		//		headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE ));
		//	    headers.add("content-disposition", "inline;filename=" + outputName);
		headers.add("Content-Disposition", "attachment; filename="+rpt_name);
		ResponseEntity<FileBean> response = new ResponseEntity<FileBean>(bean, headers, HttpStatus.OK);
		return response;
	}

	@RequestMapping(value = "/getMCatalogType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCatalogType() {
		System.out.println("getMCatalogType");
		List<DropdownBean> dropdownBeanList = manageCatalogService.getMCatalogType();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getDetailByCitizenNo/{citizenNo}", method = RequestMethod.GET)
	public @ResponseBody InvoiceBean getDetailByCitizenNo(@PathVariable("citizenNo") String citizenNo) throws Exception{
		logger.info("getDetailByCitizenNo");
		return invoiceService.getInvoiceBy(citizenNo);
	}

	@RequestMapping(value = "/importFile", method = RequestMethod.POST)
	public @ResponseBody void importFile(@RequestParam(value="file") MultipartFile file) throws Exception {
		try{
			invoiceLoaderService.importInvoice(file);
		}catch(Exception e){
			   throw new Exception("File error : "+e.getMessage());
		}
		
		System.out.println(file);
	}

	@RequestMapping(value = "/searchImport", method = RequestMethod.POST)
	public @ResponseBody List<InvoiceLoaderBean> searchImport(@RequestBody InvoiceLoaderBean bean)  throws Exception{
		return invoiceLoaderService.getInvoiceLoaderByCriteria(bean);

	}

	@RequestMapping(value = "/cancelLoader/{code}", method = RequestMethod.GET)
	public @ResponseBody HttpEntity<String> cancelLoader(@PathVariable("code") String code){
		ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
		try {
			invoiceLoaderService.cancel(code);;
		} catch (Exception e) {
			response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}

	@RequestMapping(value = "/confirmLoader/{code}", method = RequestMethod.GET)
	public @ResponseBody HttpEntity<String> confirmLoader(@PathVariable("code") String code){
		ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
		try {
			invoiceLoaderService.confirm(code);
		} catch (Exception e) {
			response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}

	@RequestMapping(value = "/deleteLoader/{code}", method = RequestMethod.GET)
	public @ResponseBody HttpEntity<String> deleteLoader(@PathVariable("code") String code){
		ResponseEntity<String> response = new ResponseEntity<>(HttpStatus.OK);
		try {
			invoiceLoaderService.delete(code);
		} catch (Exception e) {
			response = new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
		}
		return response;
	}

	public static void main(String ...args){
		String[] x = "1,2,3,4,5".split(",");
	}
}
