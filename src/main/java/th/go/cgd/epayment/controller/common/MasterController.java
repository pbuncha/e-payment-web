package th.go.cgd.epayment.controller.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.MParam;
import th.go.cgd.epayment.model.Select2Result;
import th.go.cgd.epayment.repository.MParamRepository;
import th.go.cgd.epayment.util.CatalogUtil;

@Controller
@RequestMapping("master")
public class MasterController {

	@Autowired
	private MParamRepository mParamRepository;
	
	@Autowired
	private CatalogUtil catalogUtil;
	
	@RequestMapping("/getMParam")
	public ResponseEntity<Select2Result> getMParam(@RequestParam(value="term", required=false, defaultValue="") String term, @RequestParam(value="page",required=false,defaultValue="1") int page) {
		//page have indexed 1
		PageRequest pageRequest = new PageRequest(page - 1, EPaymentConstant.SELECT2_PAGE_LIMIT, new Sort(new Sort.Order(Direction.ASC, "name")));
		Page<MParam> result = mParamRepository.findByNameContaining(term, pageRequest);
				
		Select2Result searchResult = new Select2Result();
		for (MParam param : result) {
			searchResult.getResults().add(new Select2Result.Select2Item(param.getValue() + "", param.getName()));
		}
		searchResult.setHasMore(result.getTotalPages() > page);
		return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
	}
}
