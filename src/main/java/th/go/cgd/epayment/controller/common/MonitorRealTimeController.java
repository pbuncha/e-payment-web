package th.go.cgd.epayment.controller.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.core.JsonProcessingException;

import th.go.cgd.epayment.model.CatalogBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.InvoiceBillingBean;
import th.go.cgd.epayment.service.interfaces.IInvoiceBillingService;
import th.go.cgd.epayment.service.interfaces.IMCostCenterService;
import th.go.cgd.epayment.service.interfaces.IMasterService;


@RestController
@RequestMapping("/monitorRealTime")
public class MonitorRealTimeController {

	@Autowired
	private IInvoiceBillingService invoiceBillingService;
	
	@Autowired
	private IMasterService masterService;

	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public ModelAndView search(ModelMap model) {
		System.out.println("search");
		return new ModelAndView("views/monitorRealTime/monitor-search", model);
	}
	
	@RequestMapping(value = "/getInvoiceBillingList",method=RequestMethod.POST)
	public @ResponseBody List<InvoiceBillingBean> getInvoiceBillingList(@RequestBody(required = false) InvoiceBillingBean bean) {
	    return invoiceBillingService.getInvoiceBillingList(bean);
	}
	
	@RequestMapping(value = "/pagination",method=RequestMethod.POST)
	public Page<InvoiceBillingBean> pagination() throws JsonProcessingException {
	    return invoiceBillingService.search();
	}
	
	@RequestMapping(value = "/getMCostCenterList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCostCenterList() {
	    List<DropdownBean> dropdownBeanList = masterService.getMCostCenterList();
	    return dropdownBeanList;
	}	
	
	
/*	@RequestMapping(value = "/pagination",method=RequestMethod.POST)
	public Page<CatalogBean> pagination() throws JsonProcessingException {
	    System.out.println("newfear pagination");
	    return manageCatalogService.search();
	}
	
	@RequestMapping(value = "/create", method = RequestMethod.GET)
	public ModelAndView create(ModelMap model) {
		System.out.println("create");
		return new ModelAndView("views/manageCatalog/ctl20000-create", model);
	}
	
	@RequestMapping(value = "/getMCatalogType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCatalogType() {
	    System.out.println("getMCatalogType");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getMCatalogType();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getMRevenueType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMRevenueType() {
	    System.out.println("getMRevenueType");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getMRevenueType();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getMLevyType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMLevyType() {
	    System.out.println("getMLevyType");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getMLevyType();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getCostCenter",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCostCenter() {
	    System.out.println("getCostCenter");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getCostCenter();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getCostCenterByCode/{costCenterCode}",method=RequestMethod.GET)
	public @ResponseBody CostCenterBean getCostCenterByCode (@PathVariable("costCenterCode") String costCenterCode) {
	    System.out.println("getCostCenterByCode : "+costCenterCode);
	    CostCenterBean costCenterBean = manageCatalogService.getCostCenterByCode(costCenterCode);
	    return costCenterBean;
	}
	
	@RequestMapping(value = "/ctl20001-item", method = RequestMethod.GET)
	public ModelAndView addInvoiceItemPage(ModelMap model){
		return new ModelAndView("views/manageCatalog/ctl20001-item",model);
	}
	
	@RequestMapping(value = "/saveCatalog", method = RequestMethod.POST)
	public @ResponseBody void saveCatalog(@RequestBody CatalogBean catalogBean) throws Exception {
		System.out.println("saveCatalog ");
		manageCatalogService.save(catalogBean);
	}
	*/
	
}
