package th.go.cgd.epayment.controller.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.MStatus;
import th.go.cgd.epayment.entity.Organization;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.OrganizationDataModel;
import th.go.cgd.epayment.model.Select2Result;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.OrganizationRepository;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.service.admin.OrganizationService;
import th.go.cgd.epayment.util.CatalogUtil;

@Controller
@RequestMapping("organization")
public class OrganizationController {

	@Autowired
	OrganizationRepository organizationRepository;
	
	@Autowired
	private OrganizationService service;
	
	@Autowired
	private CatalogUtil catalogUtil;
	
	@Autowired
	private UserService userService;
	
	private static final String ACTIVE = "A";
	
	@RequestMapping("/getOrganization")
	public ResponseEntity<Select2Result> getOrganize(@RequestParam(value="term", required=false, defaultValue="") String term, @RequestParam(value="page",required=false,defaultValue="1") int page, UserSession userSession) throws Exception {
		MStatus mStatus = catalogUtil.getObject(EPaymentConstant.M_STATUS, ACTIVE);
		
		//page have indexed 1
		PageRequest pageRequest = new PageRequest(page - 1, EPaymentConstant.SELECT2_PAGE_LIMIT, new Sort(new Sort.Order(Direction.ASC, "orgName")));
		
		User user = userService.findOne(userSession.getUser().getUserId());
//		int accessPermissionId = user.getUserAccessPermission().getMUserAccessPermission().getAccessPermissionId();
		
		Page<Organization> result = null;
//		if(1==accessPermissionId)
//		{
//			result = organizationRepository.findByMstatusAndOrgName(mStatus, user.getUserInfo().getOrganization().getOrgName(), pageRequest);
//		}
//		else if(2==accessPermissionId)
//		{
//			result = organizationRepository.findByMstatusAndOrgNameContaining(mStatus, term, pageRequest);
//		}
//		else
//		{
//			Long totalRecords = organizationRepository.findByOrgNameCountOrmXml(user.getUserId(), "%"+term+"%");
//			List<Object[]> list = organizationRepository.findByOrgNameOrmXml(user.getUserId(), "%"+term+"%", pageRequest);
//						
//			Select2Result searchResult = new Select2Result();
//			for (Object[] obj : list) {
//				searchResult.getResults().add(new Select2Result.Select2Item(obj[0]+"", obj[1]+""));
//			}
//			long totalPage = (totalRecords+EPaymentConstant.SELECT2_PAGE_LIMIT-1)/EPaymentConstant.SELECT2_PAGE_LIMIT;
//			searchResult.setHasMore(totalPage > page);
//			return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
//		}
		
		Select2Result searchResult = new Select2Result();
		for (Organization org : result) {
			searchResult.getResults().add(new Select2Result.Select2Item(org.getOrgId() + "", org.getOrgName()));
		}
		searchResult.setHasMore(result.getTotalPages() > page);
		return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
	}
	
	@RequestMapping("/getOrganizationNotInOrg")
	public ResponseEntity<Select2Result> getOrganizationNotInorg(
			@RequestParam(value="term", required=false, defaultValue="") String term, 
			@RequestParam(value="page",required=false,defaultValue="1") int page, 
			UserSession userSession,
			@RequestParam(value="orgId") int orgId) throws Exception {
		MStatus mStatus = catalogUtil.getObject(EPaymentConstant.M_STATUS, ACTIVE);
		
		//page have indexed 1
		PageRequest pageRequest = new PageRequest(page - 1, EPaymentConstant.SELECT2_PAGE_LIMIT, new Sort(new Sort.Order(Direction.ASC, "orgName")));
		
		User user = userService.findOne(userSession.getUser().getUserId());
//		int accessPermissionId = user.getUserAccessPermission().getMUserAccessPermission().getAccessPermissionId();
		
		Page<Organization> result = null;
//		if(1==accessPermissionId)
//		{
//			result = organizationRepository.findByMstatusAndOrgNameAndOrgIdNot(mStatus, user.getUserInfo().getOrganization().getOrgName(), orgId, pageRequest);
//		}
//		else if(2==accessPermissionId)
//		{
//			result = organizationRepository.findByMstatusAndOrgNameContainingAndOrgIdNot(mStatus, term, orgId, pageRequest);
//		}
//		else
//		{
//			Long totalRecords = organizationRepository.findByOrgNameCountNotInOrgOrmXml(user.getUserId(), "%"+term+"%", orgId);
//			List<Object[]> list = organizationRepository.findByOrgNameNotInOrgOrmXml(user.getUserId(), "%"+term+"%", orgId, pageRequest);
//						
//			Select2Result searchResult = new Select2Result();
//			for (Object[] obj : list) {
//				searchResult.getResults().add(new Select2Result.Select2Item(obj[0]+"", obj[1]+""));
//			}
//			long totalPage = (totalRecords+EPaymentConstant.SELECT2_PAGE_LIMIT-1)/EPaymentConstant.SELECT2_PAGE_LIMIT;
//			searchResult.setHasMore(totalPage > page);
//			return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
//		}
		
		Select2Result searchResult = new Select2Result();
		for (Organization org : result) {
			searchResult.getResults().add(new Select2Result.Select2Item(org.getOrgId() + "", org.getOrgName()));
		}
		searchResult.setHasMore(result.getTotalPages() > page);
		return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
	}
	
//	@RequestMapping("/getOrganizationAdmin")
//	public ResponseEntity<Select2Result> getOrganizeAdmin(@RequestParam(value="term", required=false, defaultValue="") String term, @RequestParam(value="page",required=false,defaultValue="1") int page, UserSession userSession) throws Exception {
//		MStatus mStatus = catalogUtil.getObject(EPaymentConstant.M_STATUS, ACTIVE);
//		
//		//page have indexed 1
//		PageRequest pageRequest = new PageRequest(page - 1, EPaymentConstant.SELECT2_PAGE_LIMIT, new Sort(new Sort.Order(Direction.ASC, "orgName")));
//		
//		Page<Organization> result = organizationRepository.findByMstatusAndOrgNameContaining(mStatus, term, pageRequest);
//		
//		Select2Result searchResult = new Select2Result();
//		for (Organization org : result) {
//			searchResult.getResults().add(new Select2Result.Select2Item(org.getOrgId() + "", org.getOrgName()));
//		}
//		searchResult.setHasMore(result.getTotalPages() > page);
//		return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
//	}
	
//	@RequestMapping("/getOrganizationIncludeInActive")
//	public ResponseEntity<Select2Result> getOrganizeIncludeInActive(@RequestParam(value="term", required=false, defaultValue="") String term, @RequestParam(value="page",required=false,defaultValue="1") int page) {
//		//page have indexed 1
//		PageRequest pageRequest = new PageRequest(page - 1, EPaymentConstant.SELECT2_PAGE_LIMIT, new Sort(new Sort.Order(Direction.ASC, "orgName")));
//		Page<Organization> result = organizationRepository.findByOrgNameContaining(term, pageRequest);
//		Select2Result searchResult = new Select2Result();
//		for (Organization org : result) {
//			searchResult.getResults().add(new Select2Result.Select2Item(org.getOrgId() + "", org.getOrgName()));
//		}
//		searchResult.setHasMore(result.getTotalPages() > page);
//		return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
//	}

	@ResponseBody
	@RequestMapping("/getOrganizationById")
	public OrganizationDataModel getOrganizationById(@RequestParam(value = "orgId", required=false, defaultValue="") String orgId) throws Exception
	{
		if("".equals(orgId) || "null".equals(orgId))
		{
			return new OrganizationDataModel();
		}
		return service.findOrganizationById(Integer.parseInt(orgId));
	}
	
	@RequestMapping("/getOrganizationParentOnly")
	public ResponseEntity<Select2Result> getOrganizationParentOnly(@RequestParam(value="term", required=false, defaultValue="") String term, @RequestParam(value="page",required=false,defaultValue="1") int page, UserSession userSession) throws Exception {
		MStatus mStatus = catalogUtil.getObject(EPaymentConstant.M_STATUS, ACTIVE);
		
		//page have indexed 1
		PageRequest pageRequest = new PageRequest(page - 1, EPaymentConstant.SELECT2_PAGE_LIMIT, new Sort(new Sort.Order(Direction.ASC, "orgName")));
		
		User user = userService.findOne(userSession.getUser().getUserId());
//		int accessPermissionId = user.getUserAccessPermission().getMUserAccessPermission().getAccessPermissionId();
		
		Page<Organization> result = null;
//		if(1==accessPermissionId)
//		{
//			result = organizationRepository.findByMstatusAndOrgNameAndOrganizationIsNull(mStatus, user.getUserInfo().getOrganization().getOrgName(), pageRequest);
//		}
//		else if(2==accessPermissionId)
//		{
//			result = organizationRepository.findByMstatusAndOrgNameContainingAndOrganizationIsNull(mStatus, term, pageRequest);
//		}
//		else
//		{
//			Long totalRecords = organizationRepository.findByOrgNameCountOrmXml(user.getUserId(), "%"+term+"%");
//			List<Object[]> list = organizationRepository.findByOrgNameOrmXml(user.getUserId(), "%"+term+"%", pageRequest);
//						
//			Select2Result searchResult = new Select2Result();
//			for (Object[] obj : list) {
//				searchResult.getResults().add(new Select2Result.Select2Item(obj[0]+"", obj[1]+""));
//			}
//			long totalPage = (totalRecords+EPaymentConstant.SELECT2_PAGE_LIMIT-1)/EPaymentConstant.SELECT2_PAGE_LIMIT;
//			searchResult.setHasMore(totalPage > page);
//			return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
//		}
		
		Select2Result searchResult = new Select2Result();
		for (Organization org : result) {
			searchResult.getResults().add(new Select2Result.Select2Item(org.getOrgId() + "", org.getOrgName()));
		}
		searchResult.setHasMore(result.getTotalPages() > page);
		return new ResponseEntity<Select2Result>(searchResult, HttpStatus.OK);
	}
}
