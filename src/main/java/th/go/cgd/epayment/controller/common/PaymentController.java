package th.go.cgd.epayment.controller.common;

import java.security.Principal;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.component.EpaymentUserLogin.SecurityUser;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.model.ImageBean;
import th.go.cgd.epayment.model.CreatedInvoiceBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.InvoiceItemBean;
import th.go.cgd.epayment.model.ProvinceDropdownBean;
import th.go.cgd.epayment.model.SaveInvoiceBean;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.service.impl.BarcodeServiceImpl;
import th.go.cgd.epayment.service.impl.InvoiceServiceImpl;
import th.go.cgd.epayment.service.interfaces.IBarcodeService;
import th.go.cgd.epayment.service.interfaces.IBillService;
import th.go.cgd.epayment.service.interfaces.ICatalogService;
import th.go.cgd.epayment.service.interfaces.IDepartmentService;
import th.go.cgd.epayment.service.interfaces.IInvoiceItemService;
import th.go.cgd.epayment.service.interfaces.IInvoiceService;
import th.go.cgd.epayment.service.interfaces.IManageCatalogService;
import th.go.cgd.epayment.service.interfaces.IProvinceDropdownService;
import th.go.cgd.epayment.service.interfaces.IMasterService;
import th.go.cgd.epayment.service.interfaces.IUserFavoriteService;

/**
 * @author torgan.p 5 พ.ย. 2560 03:10:10
 *
 */
@RestController
@RequestMapping("/paymentController")
public class PaymentController {
	private final Logger logger = Logger.getLogger(PaymentController.class);

	@Autowired
	private IInvoiceService invoiceService;
	@Autowired
	private IBarcodeService barcodeService;
	@Autowired
	private IDepartmentService departmentService;
	@Autowired
	private IInvoiceItemService iInvoiceItemService;
	@Autowired
	private ICatalogService iCatalogService;
	@Autowired
	private IUserFavoriteService iUserFavoriteService;
	@Autowired
	private IManageCatalogService manageCatalogService;
	@Autowired
	private IProvinceDropdownService iProvinceDropdownService;
	@Autowired
	private IMasterService masterService;
	@Autowired
	private IBillService billService;
	@Autowired
	private UserService userService;

	

	@RequestMapping(value = "/Payment", method = RequestMethod.GET)
	public ModelAndView paymentMain(ModelMap model) {
		logger.info("Payment");
		ModelAndView view = new ModelAndView();
		view.setViewName("views/payment/payment-main");
		view.addObject("isLogin", false);
		//~ Check User Login
//		User  user = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		  if ((principal != null) && (principal instanceof User)) {
		    view.addObject("isLogin", true);
		}
		return view;
	}

	@RequestMapping(value = "/searchInvoice", method = RequestMethod.POST)
	public @ResponseBody InvoiceBean searchInvoice(@RequestBody Map<String, Integer> obj) {
		logger.info("searchInvoice");
		return invoiceService.getInvoidByUser(obj.get("id").intValue());
	}

	@RequestMapping(value = "/getBarcode", method = RequestMethod.POST)
	public @ResponseBody ImageBean getBarcode(@RequestBody Map<String, Integer> obj) {
		logger.info("getBarcode");
		Invoice invoiceTb = invoiceService.getInvoidById(obj.get("id").intValue());
		String data = barcodeService.getDataBarCode(invoiceTb);
		return barcodeService.generateBarcodeTobyte(data);
	}

	@RequestMapping(value = "/getQRCode", method = RequestMethod.POST)
	public @ResponseBody ImageBean getQRCode(@RequestBody Map<String, Integer> obj) {
		logger.info("getQRCode");
		Invoice invoiceTb = invoiceService.getInvoidById(obj.get("id").intValue());
		String data = barcodeService.getDataQRCode(invoiceTb);
		barcodeService.generateQRCode(data);
		return barcodeService.getDataQRCodeTobyte(data);
	}

	@RequestMapping(value = "/getDepartmentAll", method = RequestMethod.POST)
	public @ResponseBody List<DropdownBean> getDepartmentAll() {
		logger.info("getDepartmentAll");
		return departmentService.getDepartmentAll();
	}

	@RequestMapping(value = "/paymentList", method = RequestMethod.GET)
	public ModelAndView paymentList() {
		logger.info("getPaymentList");
		return new ModelAndView("views/payment/payment-list");
	}

	@RequestMapping(value = "/getPaymentList", method = RequestMethod.GET)
	public @ResponseBody List<InvoiceItemBean> getPaymentList() {
		logger.info("getPaymentList");
		User  user = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		  if ((principal != null) && (principal instanceof User)) {
		      user = (User) principal;
		}
		return iInvoiceItemService.getInvoiceList(user);
	}

	@RequestMapping(value = "/getCatalogList", method = RequestMethod.POST)
	public @ResponseBody List<InvoiceItemBean> getCatalogList(@RequestBody(required = false) InvoiceItemBean bean) {
		logger.info("getCatalogList");
		User  user = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		  if ((principal != null) && (principal instanceof User)) {
		      user = (User) principal;
		}
		
		return iCatalogService.getCatalogList(bean,user);
	}

	@RequestMapping(value = "/saveFavorite", method = RequestMethod.POST)
	public @ResponseBody boolean saveFavorite(@RequestBody List<Integer> listId) {
		return iUserFavoriteService.save(listId);
	}

	@RequestMapping(value = "/getFavoriteByUser", method = RequestMethod.GET)
	public @ResponseBody List<Integer> getFavoriteByUser() {
	    User  user = null;
	    Integer userId = 0;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		  if ((principal != null) && (principal instanceof User)) {
		      user = (User) principal;
		      userId = user.getUserId();
		}
		 
		return iUserFavoriteService.getFavoriteByUser(userId);
	}

	@RequestMapping(value = "/getFavoriteData", method = RequestMethod.POST)
	public @ResponseBody List<InvoiceItemBean> getFavoriteData(@RequestBody List<Integer> listId) {
		return iUserFavoriteService.getFavoriteList(listId);
	}

	@RequestMapping(value = "/getMCatalogType", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCatalogType() {
		System.out.println("getMCatalogType");
		List<DropdownBean> dropdownBeanList = manageCatalogService.getMCatalogType();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getCatalogByCatType/{id}", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCatalogByCatType(@PathVariable("id") Integer id) {
		List<DropdownBean> dropdownBeanList = manageCatalogService.getCatalogByCatType(id);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getProvince", method = RequestMethod.GET)
	public @ResponseBody List<ProvinceDropdownBean> getProvince() {
		return iProvinceDropdownService.getAllprovince();
	}

	@RequestMapping(value = "/getDistrict/{id}", method = RequestMethod.GET)
	public @ResponseBody List<ProvinceDropdownBean> getDistrict(@PathVariable("id") Integer id) {
		return iProvinceDropdownService.getDistrictByProvince(id);
	}

	@RequestMapping(value = "/getSubDistrict/{id}", method = RequestMethod.GET)
	public @ResponseBody List<ProvinceDropdownBean> getSubDistrict(@PathVariable("id") Integer id) {
		return iProvinceDropdownService.getSubDistrictByDistrict(id);
	}
	@RequestMapping(value = "/getBank",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCatalogByCatType() {
	    List<DropdownBean> dropdownBeanList = masterService.getBank();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/saveInvoice", method = RequestMethod.POST)
	public @ResponseBody Integer saveInvoice(@RequestBody SaveInvoiceBean bean){
		logger.info(">>>>>>>>>> " + bean.getInvoice().toString());
		logger.info(">>>>>>>>>> " + bean.getInvoiceItemList().size());
//		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User  user = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		  if ((principal != null) && (principal instanceof User)) {
		      user = (User) principal;
		}
		return billService.saveFromPayment(bean,user);
	}
	
	@RequestMapping(value = "/saveBill",method=RequestMethod.POST)
	public @ResponseBody boolean saveBill(@RequestBody Map<String, Integer> obj){
		return billService.saveBill(obj);
	}
	
	@RequestMapping(value = "/paymentSuccess", method = RequestMethod.GET)
	public ModelAndView paymentSuccess(){
		logger.info("paymentSuccess");
		return new ModelAndView("views/payment/payment-success");
	}
	
	@RequestMapping(value = "/getUserDetails", method=RequestMethod.GET)
	public @ResponseBody CreatedInvoiceBean getUserDetails() throws Exception{
		logger.info("getUserDetails");
		CreatedInvoiceBean bean = new CreatedInvoiceBean();
		User  userDetails = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		userDetails = userService.get(userDetails.getUserId());
		if(userDetails != null){
//			User user = userService.findByUserName(userDetails.getUserName());
			bean.setFirstName(userDetails.getUserCenterId().getFirstNameTh());
			bean.setLastName(userDetails.getUserCenterId().getLastNameTh());
			bean.setCitizenid(userDetails.getUserCenterId().getCitizenNo());
			bean.setUserType(userDetails.getUserTypeId());
			bean.setDistrictId(userDetails.getUserCenterId().getAddressId().getAmphurId().getAmphurId());
			bean.setSubdistrictId(userDetails.getUserCenterId().getAddressId().getTambonId().getTambonId());
			bean.setProvinceId(userDetails.getUserCenterId().getAddressId().getProvinceId().getProvinceId());
			bean.setBuildingName(userDetails.getUserCenterId().getAddressId().getBuildingName());
			bean.setVillage(userDetails.getUserCenterId().getAddressId().getVillage());
			bean.setMoo(userDetails.getUserCenterId().getAddressId().getMoo());
			bean.setNo(userDetails.getUserCenterId().getAddressId().getNo());
			bean.setSoi(userDetails.getUserCenterId().getAddressId().getSoi());
			bean.setRoad(userDetails.getUserCenterId().getAddressId().getRoad());
			bean.setPostcode(userDetails.getUserCenterId().getAddressId().getPostcode());
			bean.setEmail(userDetails.getEmail());
			bean.setTelephone(userDetails.getMobile());
			return bean;
		}else{
			return bean;
		}
		
	}
	

}
