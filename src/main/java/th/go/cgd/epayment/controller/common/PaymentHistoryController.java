package th.go.cgd.epayment.controller.common;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.BillingBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.service.interfaces.IBillService;
import th.go.cgd.epayment.service.interfaces.IInvoiceService;
import th.go.cgd.epayment.service.interfaces.IManageCatalogService;
import th.go.cgd.epayment.service.interfaces.IMasterService;

@RestController
@RequestMapping("/paymentHistoryController")
public class PaymentHistoryController {
	private final Logger logger = Logger.getLogger(PaymentHistoryController.class);
	@Autowired
	private IInvoiceService iInvoiceService;
	@Autowired
	private IManageCatalogService manageCatalogService;
	@Autowired
	private IMasterService masterService;
	@Autowired
	private IBillService billService;

	@RequestMapping(value = "/paymentHistory", method = RequestMethod.GET)
	public ModelAndView paymentHistory(ModelMap model) {
		logger.info("paymentHistory");
		return new ModelAndView("views/paymentHistory/payment-history");
	}

	@RequestMapping(value = "/searchBilling",method =RequestMethod.POST)
	public List<BillingBean> searchBilling (@RequestBody SearchBean searchBean){
	    searchBean.setUserId(1);
		return billService.getBillByUser(searchBean);
	}
	@RequestMapping(value = "/getMCatalogType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCatalogType() {
	    System.out.println("getMCatalogType");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getMCatalogType();
	    return dropdownBeanList;
	}
	@RequestMapping(value = "/getCatalogByCatType/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCatalogByCatType(@PathVariable("id") Integer id) {
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getCatalogByCatType(id);
	    return dropdownBeanList;
	}
	@RequestMapping(value = "/getdeptList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getdeptList() {
	    List<DropdownBean> dropdownBeanList = masterService.getMDepartmentList();
	    return dropdownBeanList;
	}
	

}
