package th.go.cgd.epayment.controller.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.model.Select2Result;
import th.go.cgd.epayment.repository.ProvinceFilterRepository;

@Controller
@RequestMapping("province_filter")
public class ProvinceFilterController 
{
	@Autowired
	ProvinceFilterRepository provinceFilterRepository;

	@ResponseBody
	@RequestMapping(value = "/getDistrict", method = RequestMethod.GET)
	public Select2Result getDistrict(@RequestParam(value = "provinceId") String provinceId) {

		if(null==provinceId || "null".equals(provinceId))
		{
			return new Select2Result();
		}
		
//		List<Object[]> organizationObjArrayList = provinceFilterRepository.findDistrictByProvinceId(Short.parseShort(provinceId));

		Select2Result select2Result = new Select2Result();
		/*
		 * Change to make class compilable and ready for select2.
		 * I didn't touch your buggy JavaScript do it yourselves.
		 * Old contents are put in comment.
		 * @author Thanadee
		 */
//		for (Object[] obj : organizationObjArrayList) {
//			select2Result.getResults().add(new Select2Result.Select2Item(obj[0].toString(), obj[1].toString()));
//		}
		select2Result.setHasMore(false);
//		select2Result.getItems().addAll(organizationObjArrayList);
//		select2Result.setTotal_count(organizationObjArrayList.size());
		
		return select2Result;
	}
	
	@ResponseBody
	@RequestMapping(value = "/getSubDistrict", method = RequestMethod.GET)
	public Select2Result getSubDistrict(@RequestParam(value = "districtId") String districtId) {
		
		if(null==districtId || "null".equals(districtId))
		{
			return new Select2Result();
		}
		
//		List<Object[]> organizationObjArrayList = provinceFilterRepository.findSubDistrictByDistrictId(Short.parseShort(districtId));

		Select2Result select2Result = new Select2Result();
		/*
		 * Change to make class compilable and ready for select2.
		 * I didn't touch your buggy JavaScript do it yourselves.
		 * Old contents are put in comment.
		 * @author Thanadee
		 */
//		for (Object[] obj : organizationObjArrayList) {
//			select2Result.getResults().add(new Select2Result.Select2Item(obj[0].toString(), obj[1].toString()));
//		}
		select2Result.setHasMore(false);
//		select2Result.getItems().addAll(organizationObjArrayList);
//		select2Result.setTotal_count(organizationObjArrayList.size());
		
		return select2Result;
	}
}
