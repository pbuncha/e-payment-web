package th.go.cgd.epayment.controller.common;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.constant.EPaymentConstant.REPORT_TYPE;
import th.go.cgd.epayment.entity.Billing;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.BillingBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.FileBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.report.service.AbstractJasperReport.ReportType;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.service.impl.BillReportServiceImpl;
import th.go.cgd.epayment.service.interfaces.IBillService;
import th.go.cgd.epayment.service.interfaces.IMCostCenterService;
import th.go.cgd.epayment.service.interfaces.IMDisbursementUnitService;
import th.go.cgd.epayment.service.interfaces.IManageCatalogService;
import th.go.cgd.epayment.service.interfaces.IMasterService;
import th.go.cgd.epayment.service.interfaces.IReceiptService;


@RestController
@RequestMapping("/receiptController")
public class ReceiptController {
	private static final String RECEIPT_ZIP_NAME = "ePayment_Receipt[%s]";
	private static final Logger logger = Logger.getLogger(ManageInvoiceController.class);
	@Value("${temporary.path.pdf}")
	private String temporaryPathPdf; 
	@Autowired
	private IBillService billService;
	@Autowired 
	private IMasterService masterService;
	@Autowired 
	private IManageCatalogService manageCatalogService;

	@Autowired
	private BillReportServiceImpl billReportService;
	
	@Autowired 
	private IReceiptService receiptService;
	
	@Autowired
	private MessageResource messageResource;
	
	@Autowired 
	private IMDisbursementUnitService mDisbursementUnitService;
	
	@Autowired 
	private IMCostCenterService mCostCenterService;
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(value = "/searchGov", method = RequestMethod.GET)
	public ModelAndView search(ModelMap model) {
		System.out.println("searchGov");
		return new ModelAndView("views/receipt/receipt-search-gov", model);
	}

	@RequestMapping(value = "/getDefaultData",method=RequestMethod.GET)
	public @ResponseBody SearchBean getDefaultData() {
		SearchBean bean = receiptService.getDefaultData();
		return bean;
	}

	@RequestMapping(value = "/print/{id}", method = RequestMethod.GET, produces = "application/zip")
	public @ResponseBody HttpEntity<byte[]> print( @PathVariable("id") Integer id){

		try {
			final String fileName = generateFileName(new Date());

			BillingBean billingBean = new BillingBean();
			billingBean.setBillingId(id);
			billingBean.setIsCopy("1");	// 0 is a original, 1 is a copy
			billingBean.setIsPreview("0");	// 1 is preview mode
			billingBean.setPrintType("D"); // D=Download , P = Print

			byte[] contents = zipBytes(fileName + ".pdf", getRpt1(billingBean));
//			logger.info("END getInvoiceRpt : " + id);
			return ResponseEntity
					.ok()
					.header("Content-Disposition", "inline; filename=" + fileName + ".zip")
					.contentLength(contents.length)
					.contentType(MediaType.parseMediaType("application/octet-stream"))
					.body(contents);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getBillingRpt : " + e.getMessage());
		}
		return null;
	}
			
	@RequestMapping(value = "/searchWeb", method = RequestMethod.GET)
	public ModelAndView searchWeb(ModelMap model) {
		System.out.println("searchWeb");
		return new ModelAndView("views/receipt/receipt-search-web", model);
	}
	
	@RequestMapping(value = "/searchBilling",method =RequestMethod.POST)
	public List<BillingBean> searchBilling (@RequestBody SearchBean searchBean){
		
		User userAu = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ((principal != null) && (principal instanceof User)) {
			userAu = (User) principal;
		} 		
		User user = userRepository.findOne(userAu.getUserId());
		
		searchBean.setUserId(user.getUserId());
		
		
		return billService.getBillByUser(searchBean);
	}

	@RequestMapping(value = "/getdeptList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getdeptList() {
		List<DropdownBean> dropdownBeanList = masterService.getMDepartmentList();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getMCostCenterList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCostCenterList() {
		List<DropdownBean> dropdownBeanList = masterService.getMCostCenterList();
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getDisbursementUnitList/{departmentId}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getDisbursementUnitList (@PathVariable("departmentId") Integer departmentId) {
	    System.out.println("getDisbursementUnitByDepartmentId : "+departmentId);
	    List<DropdownBean> dropdownBeanList = mDisbursementUnitService.getDisbursementUnitByDepartmentId(departmentId);
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getCostCenterList/{disbursementUnitId}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCostCenterList (@PathVariable("disbursementUnitId") Integer disbursementUnitId) {
	    System.out.println("getCostCenterByDisbursementUnitId : "+disbursementUnitId);
	    List<DropdownBean> dropdownBeanList = mCostCenterService.getCostCenterByDisbursementUnitId(disbursementUnitId);
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getMCatalogType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCatalogType() {
	    System.out.println("getMCatalogType");
	    List<DropdownBean> dropdownBeanList = manageCatalogService.getMCatalogType();
	    return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getBillRpt", method = RequestMethod.POST)	 
	public  @ResponseBody byte[] getRpt1(@RequestBody BillingBean billingBean){
		logger.info("getBillingRpt : "+billingBean.getBillingId());
		logger.info("getBillingRpt isPreview : "+billingBean.getIsPreview());
		logger.info("getBillingRpt isCopy: "+billingBean.getIsCopy()); // 0 is a original, 1 is a copy
		logger.info("getBillingRpt getPrintType : "+billingBean.getPrintType());
		String waterMarkLine1 = null;
		String waterMarkLine2 = null;
		String genCode = null;
		if("1".equals(billingBean.getIsPreview())){
			//preview mode must have waterMark
			waterMarkLine1 = messageResource.getMessage("receipt.print.waterMarkLine1");
			waterMarkLine2 = "e-Payment Portal of Government";
		}else{
			//print mode
			genCode = receiptService.getGenCodeBilling(billingBean.getPrintType(),billingBean.getBillingId());
		}

		
		List<String> idList = new ArrayList<String>();
		idList.add(billingBean.getBillingId().toString());
		HashMap<String,Object> params = new HashMap<>();
		ClassLoader classLoader = getClass().getClassLoader();
		logger.debug("path : "+classLoader.getResource("jasper").getPath());
		params.put("billingId", billingBean.getBillingId());
		params.put("REPORT_DIR",classLoader.getResource("jasper").getPath());
		params.put("REPORT_TYPE",billingBean.getIsCopy());
		params.put("WATER_LABEL_1",waterMarkLine1);
		params.put("WATER_LABEL_2",waterMarkLine2);
		params.put("GENERATE_CODE",genCode);
		
		
		byte[] contents = null;
		try{
			contents = billReportService.genarateReport(params, ReportType.PDF);
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE ));
		    headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		    headers.add("Content-Disposition", "attachment"); 
		    logger.info("END getInvoiceRpt : "+billingBean.getBillingId());
		    ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(contents, headers, HttpStatus.OK);
			return contents;

		} catch ( Exception e) {
			e.printStackTrace();
			logger.error("getBillingRpt : "+e.getMessage());
		}
		return null ;
	}
	
	@RequestMapping(value = "/exportExcel", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean> exportExcel(@RequestBody List<BillingBean> billingBean){
		final String rpt_name = "receipt.xlsx";
		byte[] contents = null;
		contents = receiptService.exportExcel(billingBean);
		FileBean bean = new FileBean(contents, rpt_name);
		bean.setFileType(REPORT_TYPE.getReportType("EXCEL"));
		HttpHeaders headers = new HttpHeaders();
		headers.add("Content-Disposition", "attachment; filename="+rpt_name);
		ResponseEntity<FileBean> response = new ResponseEntity<FileBean>(bean, headers, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value = "/preview/{id}", method = RequestMethod.GET)
	public ModelAndView updatePage(@PathVariable("id") Integer id){
		ModelAndView view = new ModelAndView();
		view.setViewName("views/receipt/receipt-pre-print");
//		view.addObject("isLogin", false);
//		//~ Check User Login
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		  if ((principal != null) && (principal instanceof User)) {
		    view.addObject("isLogin", true);
		}
		view.addObject("billing", receiptService.getBillById(id));
		return view;
	}
	
	@RequestMapping(value = "/print/original", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<String> printOriginal( @RequestBody BillingBean billingBean ){

		try {
			
			final String fileName =  generateFileName(new Date()) + ".pdf";
			
			billingBean.setIsPreview("0");	// 1 is preview mode, 0 is print mode
			billingBean.setPrintType("P");
			byte[] contents = getRpt1(billingBean);
			
			Files.write(Paths.get(temporaryPathPdf+"/"+fileName), contents);
			final String result = String.format("{\"status\":\"%s\", \"fileName\":\"%s\"}", 1+Integer.valueOf(billingBean.getStatus()), fileName);
			return ResponseEntity
					.ok()
					.contentLength(result.length())
					.contentType(MediaType.TEXT_PLAIN)
					.body(result);
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Generate file error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getBillingRpt : " + e.getMessage());
		}
		return ResponseEntity
				.status(HttpStatus.EXPECTATION_FAILED)
				.contentType(MediaType.TEXT_PLAIN)
				.body("Cannot Generate Report.");
	}
	
	
	@RequestMapping(value = "/downloadZip/{ids}", method = RequestMethod.GET, produces = "application/zip")
	public @ResponseBody ResponseEntity<byte[]> downloadZip( @PathVariable("ids")  String ids){

		try {
			final String fileName =  generateFileName(new Date()) ;
			
			 ByteArrayOutputStream baos = new ByteArrayOutputStream();
			 ZipOutputStream zos = new ZipOutputStream(baos);
			for( String id : ids.split(",")){
				Integer uuid = Integer.valueOf(id);
				Billing entity = billService.getbillingById(uuid);
				BillingBean billingBean = new BillingBean ();
				billingBean.setIsPreview("0");	// 1 is preview mode, 0 is print mode
				billingBean.setPrintType("D");
				billingBean.setStatus(String.valueOf(entity.getStatus()));
				billingBean.setBillingId( uuid );
				byte[] contents = getRpt1(billingBean);
				
				ZipEntry entry = new ZipEntry(String.format("%s(%s).pdf",fileName ,entity.getBillingNo()));
				entry.setSize(contents.length);
				zos.putNextEntry(entry);
				zos.write(contents);
				zos.closeEntry();
			}
			
			zos.close();
			byte[] dataResult = baos.toByteArray();

			return ResponseEntity
					.ok()
					.header("Content-Disposition", "inline; filename=" + fileName + ".zip")
					.contentLength(dataResult.length)
					.contentType(MediaType.parseMediaType("application/zip"))
					.body(dataResult);
			
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("Generate file error.", e);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getBillingRpt : " + e.getMessage());
		}
		return null;
	}
	
	
	
	public static byte[] zipBytes(String filename, byte[] data) throws IOException {
	    ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ZipOutputStream zos = new ZipOutputStream(baos);
		ZipEntry entry = new ZipEntry(filename);
		entry.setSize(data.length);
		zos.putNextEntry(entry);
		zos.write(data);
		zos.closeEntry();
		zos.close();
	    return baos.toByteArray();
	}
	
	
	private static String generateFileName( Date now ) {
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy", new Locale("TH", "th"));
		return String.format(RECEIPT_ZIP_NAME, sdf.format(now));
	}
}
