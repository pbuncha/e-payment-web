package th.go.cgd.epayment.controller.common;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;

import com.captcha.botdetect.web.servlet.SimpleCaptcha;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.*;
import th.go.cgd.epayment.model.AddressBean;
import th.go.cgd.epayment.model.AttachmentBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.EmailBean;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.service.admin.MPermissionGroupService;
import th.go.cgd.epayment.service.impl.EmailServiceImpl;
import th.go.cgd.epayment.service.impl.UserOrganizationServiceImpl;
import th.go.cgd.epayment.service.interfaces.IDataMasterService;
import th.go.cgd.epayment.service.interfaces.IEmailServiceImpl;
import th.go.cgd.epayment.service.interfaces.IMBusinessAreaService;
import th.go.cgd.epayment.service.interfaces.IMCostCenterService;
import th.go.cgd.epayment.service.interfaces.IMDepartmentService;
import th.go.cgd.epayment.service.interfaces.IMOrganizationSubTypeService;
import th.go.cgd.epayment.service.interfaces.IMOrganizationTypeService;
import th.go.cgd.epayment.service.interfaces.IMProvinceService;
import th.go.cgd.epayment.service.interfaces.IMasterService;
import th.go.cgd.epayment.service.interfaces.IRegisterService;
import th.go.cgd.epayment.service.interfaces.IUserOrganizationService;
import th.go.cgd.epayment.service.interfaces.IUserPermissiongroupService;
import th.go.cgd.epayment.util.CalendarHelper;

@Controller
@RequestMapping("registerController")
public class RegisterController {
	private static final Logger logger = Logger.getLogger(RegisterController.class);
	@Autowired
	private IRegisterService registerService;
	@Autowired
	private IDataMasterService dataMasterService;
	@Autowired
	private UserService userService;
	@Autowired
	private IEmailServiceImpl emailService;
	@Autowired
	private MPermissionGroupService permissionGroupService;
	@Autowired
	private IMDepartmentService departmentService;
	@Autowired
	private IMOrganizationTypeService organizationTypeService;
	@Autowired
	private IUserOrganizationService userOrganizationService;
	@Autowired
	private IMOrganizationSubTypeService organizationSubTypeService;
	@Autowired
	private IMBusinessAreaService businessAreaService;
	@Autowired
	private IMCostCenterService costCenterService;
	@Autowired
	private IUserPermissiongroupService userPermissiongroupService;

	@Autowired 
	public IMasterService masterService;
	@Autowired
	public IMProvinceService provinceService;
	public static ResourceBundle bundle = ResourceBundle.getBundle("TemplateEmail");


	private static final String REGISTERCITIZEN = "register_citizen";
	private static final String REGISTERCORPORATE = "register_corp";
	private static final String REGISTERGORVERMENT = "register_goverment";
	private static final String LOGIN = "log_citizen";
	ResourceBundle bundleError =  EPaymentConstant.bundleError ;

	@RequestMapping(value = {"/forgot"})
	public ModelAndView forgot() {
		logger.info("forgot");
		return new ModelAndView("views/forgot/forgot");
	}

	@RequestMapping(value = {"/register"})
	public ModelAndView goToRegisterPage(ModelMap model, HttpServletRequest req) 
	{
		UserBean ub = new UserBean();
		ub.setVerifiedType('1');
		model.addAttribute("userBean", ub);
		logger.info("In /register");
		//		String type = req.getQueryString();
		String rdIdentify = req.getParameter("rdIdentify");

		logger.info(rdIdentify);
		/*if (type.equals("type=1"))
			return REGISTERCITIZEN*/;
			if (rdIdentify.equals("2"))
				return new ModelAndView(REGISTERCORPORATE,model);
			else if(rdIdentify.equals("3"))
				return new ModelAndView(REGISTERGORVERMENT,model);
			//return REGISTERGORVERMENT;
			else
				return new ModelAndView(REGISTERCITIZEN,model);
			//return REGISTERCITIZEN;
	}

	@RequestMapping(value = {"/register/getUserCitizen/{citizenid}"}, method=RequestMethod.GET)
	public @ResponseBody UserBean findOneUserCenter( @PathVariable("citizenid") String citizenid) throws Exception{




		UserBean userBean = null;
		UserCenter userCenter = userService.findOneUserCenter(citizenid);
		if(userCenter!=null && userCenter.getUserList().size()!=0){
			userBean = new UserBean();
			User userTb = userCenter.getUserList().get(0);
			Address addressTb =  userCenter.getAddressId();
			userBean.setUserId(userTb.getUserId());
			userBean.setFnameTH(userCenter.getFirstNameTh());
			userBean.setLnameTH(userCenter.getLastNameTh());
			if(userCenter.getTitleId()!=null){
				userBean.setTitleThId(userCenter.getTitleId().getTitleId().toString());
				userBean.setTitleEnId(userCenter.getTitleId().getTitleId().toString());
				userBean.setTitleNameTH(userCenter.getTitleId().getTitleName());
				userBean.setTitleNameEN(userCenter.getTitleId().getTitleNameEn());
			}
			//		    if(userCenter.getTitleId()!=null){
			//			userBean.setTitleNameEN(userCenter.getTitleId().getTitleNameEn());
			//		    }
			userBean.setMnameEN(userCenter.getMiddleNameEn());
			userBean.setMnameTH(userCenter.getMiddleNameTh());
			userBean.setFnameEN(userCenter.getFirstNameEn());
			userBean.setLnameEN(userCenter.getLastNameEn());
			userBean.setBirthDate(userCenter.getBirthDate());
			userBean.setAge(CalendarHelper.getAge(userCenter.getBirthDate()));
			userBean.setEmail(userTb.getEmail());
			userBean.setMobile(userTb.getMobile());
			userBean.setAddress_id(addressTb.getAddressId());
			userBean.setNo(addressTb.getNo());
			userBean.setMoo(addressTb.getMoo());
			userBean.setCitizenid(userCenter.getCitizenNo());
			userBean.setSoi(addressTb.getSoi());
			userBean.setRoad(addressTb.getRoad());
			MTambon tambonTb =  addressTb.getTambonId();
			MAmphur amphurTb = addressTb.getAmphurId();
			MProvince provinceTb = addressTb.getProvinceId();
			if(null != provinceTb){
				userBean.setProvinceId(provinceTb.getProvinceId().toString());
				//				userBean.setProvinceName(provinceTb.getProvinceNameTh());
			}
			if(null != tambonTb){
				userBean.setTambonId(tambonTb.getTambonId().toString());
				//				userBean.setTambonName(tambonTb.getTambonNameTh());
			}
			if(null != amphurTb){
				userBean.setAmphurId(amphurTb.getAmphurId().toString());
				//				userBean.setAmphurName(amphurTb.getAmphurNameTh());
			}
			if(null != addressTb){
				userBean.setPostCode(addressTb.getPostcode());
			}

			userBean.setStartDate(userTb.getStartDate());
			userBean.setEndDate(userTb.getEndDate());
			userBean.setApproveComment(userTb.getApproveComment());
			userBean.setStatus(Character.toString(userTb.getStatus()));
			//			    if(null!=userTb.getUserOrganizationList2()){
			//				UserOrganization userOrganizationTb = userTb.getUserOrganizationList2().get(0);
			//				Address addressGovTb = userOrganizationTb.getAddressId();
			//        			    userBean.setGovAddreNo(addressGovTb.getNo());
			//        			    userBean.setGovAddreMoo(addressGovTb.getMoo());
			//        
			//        			    userBean.setGovAddreSoi(addressGovTb.getSoi());
			//        			    userBean.setGovAddreRoad(addressGovTb.getRoad());
			//        			    MTambon tambonGovTb =  addressGovTb.getTambonId();
			//        			    MAmphur amphurGovTb = addressGovTb.getAmphurId();
			//        			    MProvince provinceGovTb = addressGovTb.getProvinceId();
			//        			    if(null != provinceGovTb){
			//        					userBean.setGovAddreProvinceId(provinceGovTb.getProvinceId().toString());
			//        					
			//        				}
			//        				if(null != tambonGovTb){
			//        					userBean.setGovAddreTambonId(tambonGovTb.getTambonId().toString());
			//        					
			//        				}
			//        				if(null != amphurGovTb){
			//        					userBean.setGovAddreAmphurId(amphurGovTb.getAmphurId().toString());
			//        					
			//        				}
			//        				if(null != addressTb){
			//        					userBean.setGovAddrePosCode(addressGovTb.getPostcode());
			//        				}
			//        
			//    		   
			//    			    }

		}


		return userBean;
	}


	@RequestMapping(value = {"/register/inquiryByID"}, method=RequestMethod.GET)
	public @ResponseBody UserBean getUserInfo(UserBean userBean) throws Exception
	{
		logger.info("In /register/inquiryByID");
		UserBean ub = new UserBean();
		logger.info(ub.getCitizenid());
		return ub;
	}

	@RequestMapping(value = "/register/getOrganizationType",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getOrganizationType() {
		logger.info("/register/getOrganizationType ");
		List<DropdownBean> dropdownBeanList = dataMasterService.getOrganizationType();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/register/getBusinessArea/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getBusinessArea(@PathVariable("id") Integer id) {
		logger.info("/register/getBusinessArea ");
		List<DropdownBean> dropdownBeanList = dataMasterService.getBusinessAreaByDepartment(id);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/register/getOrganizationSubType/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getOrganizationSubType(@PathVariable("id") Integer id) {
		logger.info("/register/getOrganizationSubType ");
		logger.info("id = " + id);
		List<DropdownBean> dropdownBeanList = dataMasterService.getOrganizationSubType(id);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/register/getDepartment/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getDepartment(@PathVariable("id") Integer id) {
		logger.info("/register/getDepartment ");
		logger.info("id = " + id);
		List<DropdownBean> dropdownBeanList = dataMasterService.getDepartment(id);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/register/getDeptAddr/{id}",method=RequestMethod.GET)
	public @ResponseBody UserBean getDeptAddr(@PathVariable("id") Integer id) {
		logger.info("/register/getDepartment ");
		logger.info("id = " + id);
		UserBean bean = new UserBean();
		List<DropdownBean> dropdownBeanList = dataMasterService.getDepartment(id);
		return bean;
	}

	@RequestMapping(value = "/register/getDepartment",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getDepartmentAll() {
		logger.info("/register/getDepartment ");
		List<DropdownBean> dropdownBeanList = dataMasterService.getDepartment(null);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/register/getCostCenter/{did}/{bid}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCostCenter(@PathVariable("bid") Integer bid,@PathVariable("did") Integer did) {
		logger.info("/register/getCostCenter ");
		logger.info("bid = " + bid);
		logger.info("did = " + did);
		List<DropdownBean> dropdownBeanList = dataMasterService.getCostCenter(did, bid);
		return dropdownBeanList;
		//return null;
	}

	@RequestMapping(value = "/register/getProvince",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getProvince() {
		logger.info("/register/getProvince ");
		List<DropdownBean> dropdownBeanList = dataMasterService.getProvince();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/register/getAmphur/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getAmphur(@PathVariable("id") Integer id) {
		logger.info("/register/getAmphur ");
		logger.info("id = " + id);
		List<DropdownBean> dropdownBeanList = dataMasterService.getAmphur(id);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/register/getTambon/{id}",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getTambon(@PathVariable("id") Integer id) {
		logger.info("/register/getTambon ");
		logger.info("id = " + id);
		List<DropdownBean> dropdownBeanList = dataMasterService.getTambon(id);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/upload",method=RequestMethod.POST)
	public @ResponseBody AttachmentBean upload(@RequestBody MultipartFile file) throws IOException {
		String msg = "success";
		  String filePath  ="";
		    AttachmentBean attachmentBean = new AttachmentBean();
		try{
		    filePath = new ClassPathResource("/").getFile().getAbsolutePath();
		    File checkPath = new File(filePath+"/file_upload/register/");
		    if(!checkPath.exists()){
			checkPath.mkdirs();
		    }
		    filePath = checkPath.getAbsolutePath()+"\\"+file.getOriginalFilename();
		}catch(Exception ex ){
		    logger.info("filePath Exception : "+ex);
		}
		
		try{
		    logger.info(" PathFull : " + filePath);
		    if (!file.isEmpty()) {
		        try {
		            byte[] bytes = file.getBytes();
		            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(filePath)));
		            stream.write(bytes);
		            stream.close();
		            logger.info("Creating file: " + filePath);
		            logger.info("You successfully uploaded " + file.getOriginalFilename() + "!");
		            attachmentBean.setData(bytes);
		        } catch (Exception e) {
		            logger.debug("You failed to upload " + file.getOriginalFilename() + " => " + e.getMessage());
//		            return HttpStatus.FORBIDDEN;
		        }
		    } else {
			logger.info("You failed to upload " + file.getOriginalFilename() + " because the file was empty.");
//		        return HttpStatus.FORBIDDEN;
		    }
		
		    attachmentBean.setFilename(file.getOriginalFilename());
		    attachmentBean.setPath(filePath);
		}catch (Exception e){
			logger.info("Error : "+e);
			msg = e.toString();
		}
		 return attachmentBean;

	}


	@RequestMapping(value = "/getTitleList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getTitleList() {
		List<DropdownBean> dropdownBeanList = masterService.getTitleList();
		return dropdownBeanList;
	}

	String validateStr = "";
	public Boolean validate(UserBean userBean){
		Boolean result = false;
		if("".equals(userBean.getCitizenid()) || null == userBean.getCitizenid() ){
			validateStr = "Citizen Is Required";
			return  result = true;
		}
		if( null == userBean.getOrgType()){
			validateStr = bundleError.getString("register.orgType");
			return  result = true;
		}
		if(null == userBean.getOrgSubType() ){
			validateStr = bundleError.getString("register.orgSubType");
			return  result = true;
		}
		if(null == userBean.getDept()){
			validateStr = bundleError.getString("register.dept");
			return  result = true;
		}
		if(null == userBean.getBizArea()){
			validateStr = bundleError.getString("register.bizArea");
			return  result = true;
		}
		if(null == userBean.getCosCenter()){
			validateStr = bundleError.getString("register.cosCenter");
			return  result = true;
		}

		return  result;
	}

	@RequestMapping(value = "/saveUser/{recaptcha}", method = RequestMethod.POST)
	public @ResponseBody String saveUser(@RequestBody UserBean userBean,@PathVariable("recaptcha") String recaptcha,HttpServletRequest request) throws Exception {
		String domainPath = "http://"+request.getServerName()+":"+request.getServerPort() + request.getContextPath();
		String ip = request.getRemoteAddr();
		if(validate(userBean)){

			return "\""+validateStr+"\"";
		}
		String captchaId = recaptcha;
		String captchaCode = userBean.getCaptcha();
		SimpleCaptcha captcha = SimpleCaptcha.load(request);
		boolean isHuman = captcha.validate(captchaCode, captchaId);

		if(!isHuman){
			return "\"Captcha incorrect.\"";
		}

		Date date = new Date();
		User userTb = userService.findOne(1);

		// validate captcha


		UserCenter userCenter = userService.findOneUserCenter(userBean.getCitizenid());
		if(userCenter!=null && userCenter.getUserList().size()!=0){
			User userCheck = userCenter.getUserList().get(0);
			if (userCheck.getUserTypeId() == 3){
				String errormessage = bundleError.getString("register.idcard.already");
				return "\""+errormessage+"\"";
			}
		}
		UserCenter uc = new UserCenter();

		String passwordBCrypt =   BCrypt.hashpw(userBean.getPassword(), BCrypt.gensalt());
		uc.setCitizenNo(userBean.getCitizenid());
		uc.setBirthDate(userBean.getBirthDate());

		MTitleName titleName = masterService.getMTitleNameById(userBean.getTitleThId() != null ? Integer.parseInt(userBean.getTitleThId()) : Integer.parseInt(userBean.getTitleEnId()));
		uc.setTitleId(titleName);



		uc.setCreatedDate(DateTime.now().toDate());
		uc.setFirstNameTh((userBean.getFnameTH() != null ? userBean.getFnameTH() : ""));
		uc.setFirstNameEn((userBean.getFnameEN() != null ? userBean.getFnameEN() : ""));
		uc.setLastNameTh(userBean.getLnameTH() !=null ? userBean.getLnameTH() : "");
		uc.setLastNameEn(userBean.getLnameEN() != null ? userBean.getLnameEN() : "");
		uc.setIsAccessEpayment('1');
		uc.setIsAccessEwelfare('1');
		uc.setPassword(passwordBCrypt);

		uc.setSex('1');
		uc.setCompanyAuthBirthDate(userBean.getCompany_auth_birth_date());
		uc.setCompanyStatus(userBean.getCompany_status());
		uc.setCompanyObjective(userBean.getCompany_objective());
		uc.setCompanyAuthId(userBean.getCompany_auth_id());
		uc.setCompanyAuthEmail(userBean.getCompany_auth_email());
		uc.setCompanyAuthPhone(userBean.getCompany_auth_phone());
		uc.setCompanyAuthDoc(userBean.getCompany_auth_doc());
		uc.setCitizenNo(userBean.getCitizenid());

		uc.setMiddleNameEn(userBean.getMnameEN());
		uc.setMiddleNameTh(userBean.getMnameTH());
		uc.setCreatedDate(DateTime.now().toDate());
		uc.setUpdatedDate(DateTime.now().toDate());

		logger.info("UserCenter - CitizenNo = " + uc.getCitizenNo());
		logger.info("UserCenter - BirthDate = " + uc.getBirthDate());
		logger.info("UserCenter - CreateDate = " + uc.getCreatedDate());
		logger.info("UserCenter - FirstNameEn = " + uc.getFirstNameEn());
		logger.info("UserCenter - MiddleNameTh = " + uc.getMiddleNameTh());
		logger.info("UserCenter - FirstNameTh = " + uc.getFirstNameTh());
		logger.info("UserCenter - IsAccessEwelfare = " + uc.getIsAccessEwelfare());
		logger.info("UserCenter - IsAccessEpayment = " + uc.getIsAccessEpayment());
		logger.info("UserCenter - Password = " + uc.getPassword());
		logger.info("UserCenter - Sex = " + uc.getSex());
		logger.info("Address - Post code = " + userBean.getPostCode());

//		logger.info(message);

		Address add = new Address();
		add.setBuildingName(userBean.getBuilding_name());
		add.setVillage(userBean.getVillage());
		add.setNo(userBean.getNo());
		add.setMoo(userBean.getMoo());
		add.setSoi(userBean.getSoi());
		add.setRoad(userBean.getRoad());
		add.setPostcode(userBean.getPostCode());
		add.setCreatedBy(userTb);
		add.setCreatedDate(date);
		if( null != userBean.getProvinceId()){
        		MProvince province = provinceService.getProvinceById(Integer.parseInt(userBean.getProvinceId()));
        		add.setProvinceId(province);
		}
		if( null != userBean.getAmphurId()){
        		MAmphur amphur = provinceService.getAmphurById(Integer.parseInt(userBean.getAmphurId()));
        		add.setAmphurId(amphur);
		}
		if( null != userBean.getTambonId()){
        		MTambon tambon = provinceService.getSubDistrictById(Integer.parseInt(userBean.getTambonId()));
        		add.setTambonId(tambon);
		}
		User u = new User();
		u.setUserName(userBean.getCitizenid());
		u.setPassword(passwordBCrypt);


		if (userBean.getUserType() == 2)
			u.setEmail(userBean.getCompany_auth_email());
		else
			u.setEmail(userBean.getEmail());
		u.setCreatedDate(date);
		u.setStatus('0');
		u.setUserTypeId(userBean.getUserType());
		u.setVerifiedType(userBean.getVerifiedType());
		u.setMobile(userBean.getMobile());
		u.setFileAttach(userBean.getFilePath());
		u.setCreatedBy(userTb);
		u.setUpdatedBy(userTb);

		logger.info("User - UserName = " + u.getUserName());
		logger.info("User - Passeord = " + u.getPassword());
		logger.info("User - UserCenterId = " + u.getUserCenterId());
		logger.info("User - Email = " + u.getEmail());
		logger.info("User - CreatedDate = " + u.getCreatedDate());
		logger.info("User - Status = " + u.getStatus());
		logger.info("User - UserTypeId = " + u.getUserTypeId());
		logger.info("User - Mobile = " + u.getMobile());
		logger.info("User - CreateBy = " + u.getCreatedBy());
		logger.info("User - UpdateBy = " + u.getUpdatedBy());

		//		User userTemp = userService.save(u);



		if(userBean.getUserType() == 3){


			try{

				UserOrganization userOrganization = new UserOrganization();
				MOrganizationType organTypeTb = organizationTypeService.findMOrganizationTypeById(Integer.parseInt(userBean.getOrgType().getId()));
				MOrganizationSubType organizationSubTypeTb = organizationSubTypeService.findMOrganizationSubTypeById(Integer.parseInt(userBean.getOrgSubType().getId()));
				MDepartment departmentTb = departmentService.findDepartmentById(Integer.parseInt(userBean.getDept().getId()));
				MBusinessArea businessAreaTb = businessAreaService.findBusinessAreaById(Integer.parseInt(userBean.getBizArea().getId()));
				MCostCenter costCenterTb = costCenterService.findMCostCenterById(Integer.parseInt(userBean.getCosCenter().getId()));

				userOrganization.setOrganizationTypeId(organTypeTb);
				userOrganization.setOrganizationSubTypeId(organizationSubTypeTb);
				userOrganization.setDepartmentId(departmentTb);
				userOrganization.setBusinessAreaId(businessAreaTb);
				userOrganization.setCostCenterId(costCenterTb);
				//			    userOrganization.setUserId(u);
				userOrganization.setCreatedBy(userTb);
				userOrganization.setCreatedDate(date);
				userOrganization.setUpdatedBy(userTb);
				userOrganization.setUpdatedDate(date);
				userOrganization.setFilepathAuthentication("Path authen");
				Address govAdd = new Address();
				//				govAdd.setBuildingName(userBean.getg);
				//				govAdd.setVillage(userBean.getg;
				govAdd.setNo(userBean.getGovAddreNo());
				govAdd.setMoo(userBean.getGovAddreMoo());
				govAdd.setSoi(userBean.getGovAddreSoi());
				govAdd.setRoad(userBean.getGovAddreRoad());
				govAdd.setPostcode(userBean.getGovAddrePosCode());
				govAdd.setCreatedBy(userTb);
				govAdd.setCreatedDate(date);
				if( null != userBean.getGovAddreProvinceId()){
        				MProvince provinceGov = provinceService.getProvinceById(Integer.parseInt(userBean.getGovAddreProvinceId()));
        				govAdd.setProvinceId(provinceGov);	
				}
				if( null != userBean.getGovAddreAmphurId()){
        				MAmphur amphurGov = provinceService.getAmphurById(Integer.parseInt(userBean.getGovAddreAmphurId()));
        				govAdd.setAmphurId(amphurGov);
				}
				if( null != userBean.getGovAddreTambonId()){
        				MTambon tambonGov = provinceService.getSubDistrictById(Integer.parseInt(userBean.getGovAddreTambonId()));
        				govAdd.setTambonId(tambonGov);
				}
				userOrganization.setAddressId(govAdd);

				//			    u.setUserOrganizationList(userOrganization);

				List<MPermissionGroup> permissionGroupList =  permissionGroupService.getPermissionGroupById(userBean.getPermissionGroup());
				List<MUserPermissiongroup> mUserPermissiongroupList = new ArrayList<MUserPermissiongroup>();
				for(MPermissionGroup permissionGroupTb : permissionGroupList){
					MUserPermissiongroup userPermissiongroup = new MUserPermissiongroup();
					userPermissiongroup.setUserId(u);
					userPermissiongroup.setCreateBy(1);
					userPermissiongroup.setCreateDate(date);
					userPermissiongroup.setUpdateBy(1);
					userPermissiongroup.setUpdateDate(date);
					userPermissiongroup.setStatus(EPaymentConstant.USER_PERMISSION_GROUP_STATUS.IN_ACTIVE);
					userPermissiongroup.setMPermissionGroupId(permissionGroupTb);
					//				    userPermissiongroupService.saveUserPermissiongroup(userPermissiongroup);
					mUserPermissiongroupList.add(userPermissiongroup);
				}
				u.setMUserPermissiongroupList(mUserPermissiongroupList);
				uc.setAddressId(add);
				u.setUserCenterId(uc);
				//			    u = userService.save(u);
				userOrganization.setUserId(u);
				userOrganization = userOrganizationService.saveUserOrganization(userOrganization);
				logger.info("User - userOrganization = " + userOrganization.getUserOrganizationId());
			}catch (Exception e){
				e.printStackTrace();
				return "\"Can not save "+e.getMessage()+"\"";
			}


		}

		try{
			EmailBean mail = new EmailBean();
			if(userBean.getUserType() == 2){
				mail.setTo(new String[]{userBean.getCompany_auth_email()});
			}else{
				mail.setTo(new String[]{userBean.getEmail()});
			}
			String fullName = (uc.getFirstNameTh()!= null ? uc.getFirstNameTh() : "")+" "+(uc.getLastNameTh() != null ? uc.getLastNameTh() : "");
			String username = encodeBase64(u.getUserName());
			Object[] objArray = {fullName,username,domainPath};
			String  content = emailService.getTemplateMail(objArray,"register.verify");
			mail.setSubject(bundle.getString("register.subject.verify"));
			mail.setText(content);
			emailService.sendMail(mail);
			logger.info("Send Mail Success : "+mail.getTo()[0]);
		}catch (Exception e){
			logger.info(e.getMessage());
			return "\"Send email fail."+e+"\"";

		}

		try {

		}catch (Exception e){

			logger.info(e.getMessage());
			return "\"Can not save user to Database.\"";

		}

		//registerService.save(userBean);

		return "\"success\"";
	}
	@RequestMapping(value = {"/encodeBase64"}, method=RequestMethod.GET)
	public @ResponseBody String encodeBase64(@RequestParam(value="username", required=false )  String username){
		String encoded = "";   
		logger.info(username);
		byte[]   bytesEncoded = Base64.encodeBase64(username.getBytes());
		encoded = new String(bytesEncoded);
		logger.info("ecncoded value is usernane " + encoded);
		return encoded.toString();
	}
	@RequestMapping(value = "/register/getPermissionGroupAll",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getPermissionGroupAll() {
		List<DropdownBean> dropdownBeanList = permissionGroupService.getPermissionGroupAll();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/register/getAddressByDept/{id}",method=RequestMethod.GET)
	public @ResponseBody AddressBean getAddressByDept(@PathVariable("id") Integer id) {
		AddressBean addressBean = departmentService.getAdressBydept(id);
		return addressBean;
	}

	@RequestMapping(value = "/register/getAge",method=RequestMethod.POST)
	public @ResponseBody Integer getAge(@RequestBody Date date) {
		logger.info("date : "+date);

		return CalendarHelper.getAgeInput(date);
	}

}
