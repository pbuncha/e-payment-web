package th.go.cgd.epayment.controller.common;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import net.sf.jasperreports.engine.JRException;
import th.go.cgd.epayment.constant.EPaymentConstant.REPORT_TYPE;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.FileBean;
import th.go.cgd.epayment.report.model.Report20001Bean;
import th.go.cgd.epayment.report.model.Report20002Bean;
import th.go.cgd.epayment.report.model.Report30001Bean;
import th.go.cgd.epayment.report.model.Report30002Bean;
import th.go.cgd.epayment.report.model.Report30003Bean;
import th.go.cgd.epayment.report.model.Report30004Bean;
import th.go.cgd.epayment.report.model.Report30005Bean;
import th.go.cgd.epayment.report.model.Report30007Bean;
import th.go.cgd.epayment.report.model.Report40001Bean;
import th.go.cgd.epayment.report.model.Report70001Bean;
import th.go.cgd.epayment.report.model.Report70002Bean;
import th.go.cgd.epayment.report.model.Report70003Bean;
import th.go.cgd.epayment.report.model.Report70004Bean;
import th.go.cgd.epayment.report.model.Report70005Bean;
import th.go.cgd.epayment.report.model.Report70006Bean;
import th.go.cgd.epayment.report.service.Report20001Service;
import th.go.cgd.epayment.report.service.Report20002Service;
import th.go.cgd.epayment.report.service.Report30001Service;
import th.go.cgd.epayment.report.service.Report30002Service;
import th.go.cgd.epayment.report.service.Report30003Service;
import th.go.cgd.epayment.report.service.Report30004Service;
import th.go.cgd.epayment.report.service.Report30005Service;
import th.go.cgd.epayment.report.service.Report30007Service;
import th.go.cgd.epayment.report.service.Report40001Service;
import th.go.cgd.epayment.report.service.Report70001Service;
import th.go.cgd.epayment.report.service.Report70002Service;
import th.go.cgd.epayment.report.service.Report70003Service;
import th.go.cgd.epayment.report.service.Report70004Service;
import th.go.cgd.epayment.report.service.Report70005Service;
import th.go.cgd.epayment.report.service.Report70006Service;
import th.go.cgd.epayment.service.interfaces.IDepartmentService;
import th.go.cgd.epayment.service.interfaces.IInvoiceService;
import th.go.cgd.epayment.service.interfaces.IMCostCenterService;
import th.go.cgd.epayment.service.interfaces.IMDepartmentService;
import th.go.cgd.epayment.service.interfaces.IMDisbursementUnitService;
import th.go.cgd.epayment.service.interfaces.IMRevenueTypeService;
import th.go.cgd.epayment.service.interfaces.IManageCatalogService;

@RestController
@RequestMapping("/reportController")
public class ReportController extends AbstractBaseController{

	@RequestMapping(value = "/rpt-20001", method = RequestMethod.GET)
	public ModelAndView rpt20001(ModelMap model) {
		return new ModelAndView("views/report/rpt-20001/rpt-20001");
	}

	@RequestMapping(value = "/rpt-20002", method = RequestMethod.GET)
	public ModelAndView rpt20002(ModelMap model) {
		return new ModelAndView("views/report/rpt-20002/rpt-20002");
	}

	@RequestMapping(value = "/rpt-30001", method = RequestMethod.GET)
	public ModelAndView rpt30001(ModelMap model) {
		return new ModelAndView("views/report/rpt-30001/rpt-30001");
	}
	
	@RequestMapping(value = "/rpt-30002", method = RequestMethod.GET)
	public ModelAndView rpt30002(ModelMap model) {
		return new ModelAndView("views/report/rpt-30002/rpt-30002");
	}

	@RequestMapping(value = "/rpt-30003", method = RequestMethod.GET)
	public ModelAndView rpt30003(ModelMap model) {
		return new ModelAndView("views/report/rpt-30003/rpt-30003");
	}

	@RequestMapping(value = "/rpt-30004", method = RequestMethod.GET)
	public ModelAndView rpt30004(ModelMap model) {
		return new ModelAndView("views/report/rpt-30004/rpt-30004");
	}

	@RequestMapping(value = "/rpt-30005", method = RequestMethod.GET)
	public ModelAndView rpt30005(ModelMap model) {
		return new ModelAndView("views/report/rpt-30005/rpt-30005");
	}

	@RequestMapping(value = "/rpt-30007", method = RequestMethod.GET)
	public ModelAndView rpt30007(ModelMap model) {		
		return new ModelAndView("views/report/rpt-30007/rpt-30007");
	}

	@RequestMapping(value = "/rpt-40001", method = RequestMethod.GET)
	public ModelAndView rpt40001(ModelMap model) {
		return new ModelAndView("views/report/rpt-40001/rpt-40001");
	}

	@RequestMapping(value = "/rpt-70001", method = RequestMethod.GET)
	public ModelAndView rpt70001(ModelMap model) {
		return new ModelAndView("views/report/rpt-70001/rpt-70001");
	}

	@RequestMapping(value = "/rpt-70002", method = RequestMethod.GET)
	public ModelAndView rpt70002(ModelMap model) {
		return new ModelAndView("views/report/rpt-70002/rpt-70002");
	}

	@RequestMapping(value = "/rpt-70003", method = RequestMethod.GET)
	public ModelAndView rpt70003(ModelMap model) {
		return new ModelAndView("views/report/rpt-70003/rpt-70003");
	}

	@RequestMapping(value = "/rpt-70004", method = RequestMethod.GET)
	public ModelAndView rpt70004(ModelMap model) {
		return new ModelAndView("views/report/rpt-70004/rpt-70004");
	}

	@RequestMapping(value = "/rpt-70005", method = RequestMethod.GET)
	public ModelAndView rpt70005(ModelMap model) {
		return new ModelAndView("views/report/rpt-70005/rpt-70005");
	}

	@RequestMapping(value = "/rpt-70006", method = RequestMethod.GET)
	public ModelAndView rpt70006(ModelMap model) {
		return new ModelAndView("views/report/rpt-70006/rpt-70006");
	}

	@RequestMapping(value = "/getDepartmentAll", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getDepartmentAll() {
		return departmentService.getDepartmentAll();
	}

	@RequestMapping(value = "/getMinistryAll", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMinistryAll() {
		return departmentService.getMinistryAll();
	}

	@RequestMapping(value = "/getDepartmentByMinistryId/{id}", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getDepartmentByMinistryId(@PathVariable("id") Integer id) {
		return departmentService.getDepartmentByMinistryId(id);
	}

	@RequestMapping(value = "/getMCatalogType", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCatalogType() {
		System.out.println("getMCatalogType");
		List<DropdownBean> dropdownBeanList = manageCatalogService.getMCatalogType();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getMRevenueType", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMRevenueType() {
		System.out.println("getMCatalogType");
		List<DropdownBean> dropdownBeanList = revenueTypeService.getStatusActive();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getCatalogList", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCatalogList() {
		System.out.println("getCatalogList");
		List<DropdownBean> dropdownBeanList = invoiceService.getCatalogList();
		return dropdownBeanList;
	}	

	@RequestMapping(value = "/getCostCenter", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCostCenter() {
		System.out.println("getCostCenter");
		List<DropdownBean> dropdownBeanList = manageCatalogService.getCostCenter();
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getCostCenterByDisbursementUnitId/{disbursementUnitId}", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getCostCenterByDisbursementUnitId(@PathVariable("disbursementUnitId") Integer disbursementUnitId) {
		System.out.println("getCostCenterByDisbursementUnitId");
		List<DropdownBean> dropdownBeanList = costCenterService.getCostCenterByDisbursementUnitId(disbursementUnitId);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getDisbursementUnit", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getDisbursementUnit() {
		System.out.println("getDisbursementUnit");
		List<DropdownBean> dropdownBeanList = disbursementUnitService.getDisbursementUnit();
		return dropdownBeanList;
	}

	@RequestMapping(value = "/getDisbursementUnitByDepartmentId/{departmentId}", method = RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getDisbursementUnit(@PathVariable("departmentId") Integer departmentId) {
		List<DropdownBean> dropdownBeanList = disbursementUnitService.getDisbursementUnitByDepartmentId(departmentId);
		return dropdownBeanList;
	}

	@RequestMapping(value = "/exportReport/rpt-20001", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report20001Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_20001.pdf";
		byte[] contents = null;
			try {
				contents = report20001Service.genarateReport(reportBean,   reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/exportReport/rpt-20002", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report20002Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_20002.pdf";
		byte[] contents = null;
			try {
				contents = report20002Service.genarateReport(reportBean,   reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/exportReport/rpt-30001", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report30001Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_30001.pdf";
		byte[] contents = null;
			try {
				contents = report30001Service.genarateReport(reportBean,   reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/exportReport/rpt-30002", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report30002Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_30002.pdf";
		byte[] contents = null;
			try {
				contents = report30002Service.genarateReport(reportBean,   reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/exportReport/rpt-30003", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report30003Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_30003.pdf";
		byte[] contents = null;
			try {
				contents = report30003Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/exportReport/rpt-30004", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report30004Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_30004.pdf";
		byte[] contents = null;
			try {
				contents = report30004Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}

	@RequestMapping(value = "/exportReport/rpt-30005", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report30005Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_30005.pdf";
		byte[] contents = null;
			try {
				contents = report30005Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}

	@RequestMapping(value = "/exportReport/rpt-30007", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report30007Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_30007.pdf";
		byte[] contents = null;
			try {
				contents = report30007Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/exportReport/rpt-40001", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report40001Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_40001.pdf";
		byte[] contents = null;
			try {
				contents = report40001Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/exportReport/rpt-70001", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report70001Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_70001.pdf";
		byte[] contents = null;
			try {
				contents = report70001Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}

	@RequestMapping(value = "/exportReport/rpt-70002", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report70002Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_70002.pdf";
		byte[] contents = null;
			try {
				contents = report70002Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}

	@RequestMapping(value = "/exportReport/rpt-70003", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report70003Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_70003.pdf";
		byte[] contents = null;
			try {
				contents = report70003Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}

	@RequestMapping(value = "/exportReport/rpt-70004", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report70004Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_70004.pdf";
		byte[] contents = null;
			try {
				contents = report70004Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}

	@RequestMapping(value = "/exportReport/rpt-70005", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report70005Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_70005.pdf";
		byte[] contents = null;
			try {
				contents = report70005Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}

	@RequestMapping(value = "/exportReport/rpt-70006", method = RequestMethod.POST)
	public @ResponseBody HttpEntity<FileBean>  exportReport( @RequestBody Report70006Bean reportBean ) throws IOException {
		final String rpt_name = "rpt_70006.pdf";
		byte[] contents = null;
			try {
				contents = report70006Service.genarateReport(reportBean,  reportBean.getReportType());
			} catch (ClassNotFoundException | SQLException | JRException e) {
				e.printStackTrace();
			}
			FileBean bean = new FileBean(contents, rpt_name);
			bean.setFileType(REPORT_TYPE.getReportType(reportBean.getReportType()));
			
			return new ResponseEntity<FileBean>( bean, HttpStatus.OK);
	}

	@Autowired
	private Report20001Service report20001Service;
	@Autowired
	private Report20002Service report20002Service;
	@Autowired
	private Report30001Service report30001Service;
	@Autowired
	private Report30002Service report30002Service;
	@Autowired
	private Report30003Service report30003Service;
	@Autowired
	private Report30004Service report30004Service;
	@Autowired
	private Report30005Service report30005Service;
	@Autowired
	private Report30007Service report30007Service;
	@Autowired
	private Report40001Service report40001Service;
	@Autowired
	private Report70001Service report70001Service;
	@Autowired
	private Report70002Service report70002Service;
	@Autowired
	private Report70003Service report70003Service;
	@Autowired
	private Report70004Service report70004Service;
	@Autowired
	private Report70005Service report70005Service;
	@Autowired
	private Report70006Service report70006Service;
	@Autowired
	private IInvoiceService invoiceService;
	@Autowired
	private IManageCatalogService manageCatalogService;
	@Autowired
	private IMDepartmentService departmentService;
	@Autowired
	private IMRevenueTypeService revenueTypeService;
	@Autowired
	private IMDisbursementUnitService disbursementUnitService;
	@Autowired
	private IMCostCenterService costCenterService;
	
	
	
	private final Logger log = Logger.getLogger(ReportController.class);
}
