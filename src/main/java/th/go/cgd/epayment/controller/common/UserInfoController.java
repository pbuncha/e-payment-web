package th.go.cgd.epayment.controller.common;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import net.sf.jasperreports.engine.JRException;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.CatalogBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.FileBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.model.UserPermissionGroupBean;
import th.go.cgd.epayment.report.service.AbstractJasperReport.ReportType;
import th.go.cgd.epayment.service.interfaces.IMasterService;
import th.go.cgd.epayment.service.interfaces.IUserInfoService;


@RestController
@RequestMapping("/userInfoController")
public class UserInfoController {
	private static final Logger logger = Logger.getLogger(UserInfoController.class);
	@Autowired 
	private IMasterService masterService;
	@Autowired 
	private IUserInfoService userInfoService;
	
	@RequestMapping(value = "/approve", method = RequestMethod.GET)
	public ModelAndView searchUser(ModelMap model) {
		System.out.println("approve");
		return new ModelAndView("views/userInfo/user-approve", model);
	}
	
	@RequestMapping(value = "/info", method = RequestMethod.GET)
	public ModelAndView manageUser(ModelMap model) {
		System.out.println("info");
		return new ModelAndView("views/userInfo/user-info", model);
	}
	
	@RequestMapping(value = "/getdeptList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getdeptList() {
		List<DropdownBean> dropdownBeanList = masterService.getMDepartmentList();
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getMCostCenterList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMCostCenterList() {
		List<DropdownBean> dropdownBeanList = masterService.getMCostCenterList();
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/getMBusinessAreaList",method=RequestMethod.GET)
	public @ResponseBody List<DropdownBean> getMBusinessAreaList() {
		List<DropdownBean> dropdownBeanList = masterService.getMBusinessAreaList();
		return dropdownBeanList;
	}
	
	@RequestMapping(value = "/search",method =RequestMethod.POST)
	public List<UserBean> search (@RequestBody SearchBean searchBean){
		logger.info("search");
		return userInfoService.search(searchBean);
	}
	
	@RequestMapping(value = "/updateStatusUserPermission", method = RequestMethod.POST)
	public @ResponseBody void updateStatusUserPermission(@RequestBody List<UserBean> userBeanList) throws Exception {
		System.out.println("updateStatusUserPermission ");
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		userInfoService.updateStatusUserPermission(userBeanList ,user.getUserId());
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody void update(@RequestBody List<UserBean> userBeanList) throws Exception {
		System.out.println("updateStatusUser ");
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		userInfoService.update(userBeanList ,user.getUserId());
	}
	
	@RequestMapping(value = "/searchGroup/{id}",method =RequestMethod.GET)
	public List<UserPermissionGroupBean> searchGroup (@PathVariable("id") Integer id){
		logger.info("search");
		return userInfoService.search(id);
	}
	
	@RequestMapping(value = "/updateStatus", method = RequestMethod.POST)
	public @ResponseBody void update(@RequestBody UserBean userBean) throws Exception {
		System.out.println("updateStatus ");
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		userInfoService.update(userBean ,user.getUserId());
	}
	
	@RequestMapping(value = "/getfile", method = RequestMethod.POST)	 
	public  @ResponseBody HttpEntity<FileBean> getRpt1(@RequestBody String path){
		logger.info("getfile : "+path);
		File f = new File(path);
		System.out.println(f.getName());
		ClassLoader classLoader = getClass().getClassLoader();
		logger.info("path : "+classLoader.getResource("jasper").getPath());

		byte[] contents = null;
		contents = userInfoService.getFile(path);
		FileBean bean = new FileBean(contents, f.getName());
		HttpHeaders headers = new HttpHeaders();
		//		headers.setContentType(MediaType.parseMediaType(MediaType.APPLICATION_PDF_VALUE ));
		//	    headers.add("content-disposition", "inline;filename=" + outputName);
		headers.add("Content-Disposition", "attachment; filename="+f.getName());
		ResponseEntity<FileBean> response = new ResponseEntity<FileBean>(bean, headers, HttpStatus.OK);
		logger.info("END getfile : "+path);
		return response;
	}
}
