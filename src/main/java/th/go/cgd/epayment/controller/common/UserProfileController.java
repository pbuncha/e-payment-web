package th.go.cgd.epayment.controller.common;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.service.UserService;

@Controller
@RequestMapping("user")
public class UserProfileController {
	private static final Logger logger = Logger.getLogger(UserProfileController.class);

	private static final String PROFILE = "/views/common/citizenProfile";
	private static final String PROFILECORPORATE = "/views/common/corporateProfile";
	private static final String PROFILEGOVERMNET = "/views/common/govermentProfile";
	private static final String MODE = "mode";
	
	@Autowired
	private UserService userService;
	
	//@RequestMapping("/profile")
	public String profile(Model model, UserSession userSession) throws Exception {
		System.out.println("In /user/profile");
		System.out.println(userSession);
		model.addAttribute(userService.findOne(userSession.getUser().getUserId()));
		model.addAttribute(MODE, EPaymentConstant.VIEW);
		return PROFILE;
	}
	
	@RequestMapping(value = "/profile", method = RequestMethod.GET)
	public String viewProfile(ModelMap model, HttpServletRequest request, @AuthenticationPrincipal User userSession) throws Exception {
		System.out.println("In /user/profile");
		System.out.println(userSession);
		User u = userService.findOne(userSession.getUserId());
		model.addAttribute(u);
		model.addAttribute(MODE, EPaymentConstant.VIEW);
		System.out.println("u.getUserTypeId() = " + u.getUserTypeId());
		int userType = u.getUserTypeId();
		if (userType == 3 || userType == 4)
			return PROFILEGOVERMNET;
		else if (u.getUserTypeId() == 2)
			return PROFILECORPORATE;
		else return PROFILE;
	}
	
	@RequestMapping(value = "/profileData", method = RequestMethod.GET)
	public @ResponseBody UserBean getProfile(ModelMap model, HttpServletRequest request, @AuthenticationPrincipal User userSession) throws Exception {
		System.out.println("In /profileData");
		System.out.println(userSession);
		User u = userService.findOne(userSession.getUserId());
		model.addAttribute(u);
		model.addAttribute(MODE, EPaymentConstant.VIEW);

//		UserCenter uc = userService.findOneUserCenter(u.getUserCenterId());
		UserCenter uc = u.getUserCenterId();
		UserBean ub = new UserBean();
		ub.setEmail(u.getEmail());
		ub.setMobile(u.getMobile());
		if (u.getUserTypeId() == 1){
			if(null != uc.getBirthDate()){
				ub.setBirthDateStr(new SimpleDateFormat("yyyy-MM-dd").format(uc.getBirthDate()));
			}
		}
		else ub.setBirthDateStr("-");
		ub.setCitizenid(uc.getCitizenNo());
		ub.setTitleNameTH(uc.getTitleId().getTitleName());
		ub.setTitleNameEN("-");
		ub.setFnameTH(uc.getFirstNameTh());
		ub.setFnameEN(uc.getFirstNameEn());
		ub.setMnameEN(uc.getMiddleNameEn());
		ub.setMnameTH("-");
		ub.setLnameEN(uc.getLastNameEn());
		ub.setLnameTH(uc.getLastNameTh());
		//ub.setAge(DateTime.now().year() - uc.getBirthDate().getYear());
		
		return ub;
	}
	
	@RequestMapping("/profile/edit")
	public String edit(Model model, UserSession userSession) throws Exception {
		model.addAttribute(userService.findOne(userSession.getUser().getUserId()));
		model.addAttribute(MODE, EPaymentConstant.EDIT);
		return PROFILE;
	}
	
	/*@RequestMapping("/profile/edit")
	public String editProfile(Model model, HttpServletRequest request, @AuthenticationPrincipal User userSession) throws Exception {
		System.out.println("In /user/profile");
		model.addAttribute(userService.findOne(userSession.getUserId()));
		model.addAttribute(MODE, EPaymentConstant.EDIT);
		return PROFILE;
	}*/
	
//	@RequestMapping(value="/profile/save", method=RequestMethod.POST)
//	public String save(@ModelAttribute(value="user") User user, UserSession userSession) throws Exception {
//		userService.updateProfile(user, userSession);
//		return "redirect:/user/profile";
//	}
	
//	@ResponseBody
//	@RequestMapping(value="/profile/changePassword", method=RequestMethod.POST)
//	public boolean changePassword(@RequestParam(value="oldPassword") String oldPassword, 
//			@RequestParam(value="confirmNewPassword") String confirmNewPassword, UserSession userSession) throws Exception {
//		return userService.changePassword(oldPassword, confirmNewPassword, userSession);
//	}
	
}
