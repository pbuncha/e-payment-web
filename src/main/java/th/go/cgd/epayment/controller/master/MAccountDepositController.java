package th.go.cgd.epayment.controller.master;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.AccountDepositBean;
import th.go.cgd.epayment.service.interfaces.IMAccountDepositService;

@RestController
@RequestMapping(value="/master2")
public class MAccountDepositController {

	@Autowired
	private IMAccountDepositService service;	
	
	@RequestMapping(value="/findall-account-deposit", method=RequestMethod.GET)
	public List<AccountDepositBean> getAccountDeposit()
	{
		return service.getAllMAccountDeposits();
	}

	@RequestMapping(value="/findone-account-deposit/{id}", method=RequestMethod.GET)
	public AccountDepositBean getAccountDeposit(@PathVariable("id") Integer id)
	{
		return service.getMAccountDepositById(id);
	}

	@RequestMapping(value="/search-account-deposit", method=RequestMethod.POST)
	public List<AccountDepositBean> searchAccountDeposit(@RequestBody AccountDepositBean obj )
	{
		return service.getMAccountDepositByCriteria(obj);
	}	
	
	@RequestMapping(value="/cancel-account-deposit/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}
	
	@RequestMapping(value="/create-account-deposit", method=RequestMethod.POST)
	public void createAccountDeposit(@RequestBody AccountDepositBean obj)
	{
		service.createMAccountDeposit(obj);
	}

	@RequestMapping(value="/update-account-deposit", method=RequestMethod.PUT)
	public void updateAccountDeposit(@RequestBody AccountDepositBean obj)
	{
		service.updateMAccountDeposit(obj);
	}

	@RequestMapping(value="/delete-account-deposit/{id}", method=RequestMethod.DELETE)
	public void saveAccountDeposit(@PathVariable("id") Integer id)
	{
		service.deleteMAccountDeposit(id);
	}
	
	@RequestMapping(value="/account-deposit-view")
	public ModelAndView viewMAccountDeposit(ModelMap model)
	{
		return new ModelAndView("/views/Master/MAccountDeposit/account-deposit-view", model);
	}

	@RequestMapping(value="/account-deposit-edit/{id}")
	public ModelAndView editMAccountDepositById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MAccountDeposit/account-deposit-edit", model);		
	}

	@RequestMapping(value="/account-deposit-create")
	public ModelAndView createMAccountDepositById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MAccountDeposit/account-deposit-create", model);		
	}

}
