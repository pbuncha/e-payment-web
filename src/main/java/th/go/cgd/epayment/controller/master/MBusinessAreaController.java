package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.BusinessAreaBean;
import th.go.cgd.epayment.service.interfaces.IMBusinessAreaService;

@RestController
@RequestMapping(value="/master2")
public class MBusinessAreaController {

	@Autowired
	private IMBusinessAreaService service;	
	
	@RequestMapping(value="/findall-business-area", method=RequestMethod.GET)
	public List<BusinessAreaBean> getBusinessArea()
	{
		return service.getAllMBusinessAreas();
	}

	@RequestMapping(value="/findone-business-area/{id}", method=RequestMethod.GET)
	public BusinessAreaBean getBusinessArea(@PathVariable("id") Integer id)
	{
		return service.getMBusinessAreaById(id);
	}

	@RequestMapping(value="/search-business-area", method=RequestMethod.POST)
	public List<BusinessAreaBean> searchBusinessArea(@RequestBody BusinessAreaBean obj)
	{
		return service.getMBusinessAreaByCriteria(obj);
	}	

	@RequestMapping(value="/cancel-business-area/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}

	@RequestMapping(value="/create-business-area", method=RequestMethod.POST)
	public void createBusinessArea(@RequestBody BusinessAreaBean obj)
	{
		service.createMBusinessArea(obj);
	}

	@RequestMapping(value="/update-business-area", method=RequestMethod.PUT)
	public void updateBusinessArea(@RequestBody BusinessAreaBean obj)
	{
		service.updateMBusinessArea(obj);
	}

	@RequestMapping(value="/delete-business-area/{id}", method=RequestMethod.DELETE)
	public void saveBusinessArea(@PathVariable("id") Integer id)
	{
		service.deleteMBusinessArea(id);
	}
	
	@RequestMapping(value="/business-area-view")
	public ModelAndView viewMBusinessArea(ModelMap model)
	{
		return new ModelAndView("/views/Master/MBusinessArea/business-area-view", model);
	}

	@RequestMapping(value="/business-area-edit/{id}")
	public ModelAndView editMBusinessAreaById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MBusinessArea/business-area-edit", model);		
	}

	@RequestMapping(value="/business-area-create")
	public ModelAndView createMBusinessAreaById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MBusinessArea/business-area-create", model);		
	}

}
