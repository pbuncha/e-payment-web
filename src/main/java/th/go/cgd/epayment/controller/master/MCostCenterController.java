package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.CostCenterBean;
import th.go.cgd.epayment.service.interfaces.IMCostCenterService;

@RestController
@RequestMapping(value="/master2")
public class MCostCenterController {

	@Autowired
	private IMCostCenterService service;	
	
	@RequestMapping(value="/findall-cost-center", method=RequestMethod.GET)
	public List<CostCenterBean> getCostCenter()
	{
		return service.getAllMCostCenters();
	}

	@RequestMapping(value="/findone-cost-center/{id}", method=RequestMethod.GET)
	public CostCenterBean getCostCenter(@PathVariable("id") Integer id)
	{
		return service.getMCostCenterById(id);
	}

	@RequestMapping(value="/search-cost-center", method=RequestMethod.POST)
	public List<CostCenterBean> searchCostCenter(@RequestBody CostCenterBean obj)
	{
		return service.getMCostCenterByCriteria(obj);
	}	

	@RequestMapping(value="/cancel-cost-center/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}
	
	@RequestMapping(value="/create-cost-center", method=RequestMethod.POST)
	public void createCostCenter(@RequestBody CostCenterBean obj)
	{
		service.createMCostCenter(obj);
	}

	@RequestMapping(value="/update-cost-center", method=RequestMethod.PUT)
	public void updateCostCenter(@RequestBody CostCenterBean obj)
	{
		service.updateMCostCenter(obj);
	}

	@RequestMapping(value="/delete-cost-center/{id}", method=RequestMethod.DELETE)
	public void saveCostCenter(@PathVariable("id") Integer id)
	{
		service.deleteMCostCenter(id);
	}
	
	@RequestMapping(value="/cost-center-view")
	public ModelAndView viewMCostCenter(ModelMap model)
	{
		return new ModelAndView("/views/Master/MCostCenter/cost-center-view", model);
	}

	@RequestMapping(value="/cost-center-edit/{id}")
	public ModelAndView editMCostCenterById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MCostCenter/cost-center-edit", model);		
	}

	@RequestMapping(value="/cost-center-create")
	public ModelAndView createMCostCenterById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MCostCenter/cost-center-create", model);		
	}

}
