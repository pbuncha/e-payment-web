package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.DepartmentBean;
import th.go.cgd.epayment.service.interfaces.IMDepartmentService;

@RestController
@RequestMapping(value="/master2")
public class MDepartmentController {

	@Autowired
	private IMDepartmentService service;	
	
	@RequestMapping(value="/findall-department", method=RequestMethod.GET)
	public List<DepartmentBean> getDepartment()
	{
		return service.getAllMDepartments();
	}

	@RequestMapping(value="/findone-department/{id}", method=RequestMethod.GET)
	public DepartmentBean getDepartment(@PathVariable("id") Integer id)
	{
		return service.getMDepartmentById(id);
	}

	@RequestMapping(value="/search-department", method=RequestMethod.POST)
	public List<DepartmentBean> searchDepartment(@RequestBody DepartmentBean obj)
	{
		return service.getMDepartmentByCriteria(obj);
	}	

	@RequestMapping(value="/cancel-department/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}
	
	@RequestMapping(value="/create-department", method=RequestMethod.POST)
	public void createDepartment(@RequestBody DepartmentBean obj)
	{
		service.createMDepartment(obj);
	}

	@RequestMapping(value="/update-department", method=RequestMethod.PUT)
	public void updateDepartment(@RequestBody DepartmentBean obj)
	{
		service.updateMDepartment(obj);
	}

	@RequestMapping(value="/delete-department/{id}", method=RequestMethod.DELETE)
	public void saveDepartment(@PathVariable("id") Integer id)
	{
		service.deleteMDepartment(id);
	}
	
	@RequestMapping(value="/department-view")
	public ModelAndView viewMDepartment(ModelMap model)
	{
		return new ModelAndView("/views/Master/MDepartment/department-view", model);
	}

	@RequestMapping(value="/department-edit/{id}")
	public ModelAndView editMDepartmentById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MDepartment/department-edit", model);		
	}

	@RequestMapping(value="/department-create")
	public ModelAndView createMDepartmentById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MDepartment/department-create", model);		
	}

}
