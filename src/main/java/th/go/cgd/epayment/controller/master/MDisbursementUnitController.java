package th.go.cgd.epayment.controller.master;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.DisbursementUnitBean;
import th.go.cgd.epayment.service.interfaces.IMDisbursementUnitService;

@RestController
@RequestMapping(value="/master2")
public class MDisbursementUnitController {

	@Autowired
	private IMDisbursementUnitService service;	
	
	@RequestMapping(value="/findall-disbursement-unit", method=RequestMethod.GET)
	public List<DisbursementUnitBean> getDisbursementUnit()
	{
		return service.getAllMDisbursementUnits();
	}

	@RequestMapping(value="/findone-disbursement-unit/{id}", method=RequestMethod.GET)
	public DisbursementUnitBean getDisbursementUnit(@PathVariable("id") Integer id)
	{
		return service.getMDisbursementUnitById(id);
	}

	@RequestMapping(value="/search-disbursement-unit", method=RequestMethod.POST)
	public List<DisbursementUnitBean> searchDisbursementUnit(@RequestBody DisbursementUnitBean obj )
	{
		return service.getMDisbursementUnitByCriteria(obj);
	}	

	@RequestMapping(value="/cancel-disbursement-unit/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}

	@RequestMapping(value="/create-disbursement-unit", method=RequestMethod.POST)
	public void createDisbursementUnit(@RequestBody DisbursementUnitBean obj)
	{
		service.createMDisbursementUnit(obj);
	}

	@RequestMapping(value="/update-disbursement-unit", method=RequestMethod.PUT)
	public void updateDisbursementUnit(@RequestBody DisbursementUnitBean obj)
	{
		service.updateMDisbursementUnit(obj);
	}

	@RequestMapping(value="/delete-disbursement-unit/{id}", method=RequestMethod.DELETE)
	public void saveDisbursementUnit(@PathVariable("id") Integer id)
	{
		service.deleteMDisbursementUnit(id);
	}
	
	@RequestMapping(value="/disbursement-unit-view")
	public ModelAndView viewMDisbursementUnit(ModelMap model)
	{
		return new ModelAndView("/views/Master/MDisbursementUnit/disbursement-unit-view", model);
	}

	@RequestMapping(value="/disbursement-unit-edit/{id}")
	public ModelAndView editMDisbursementUnitById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MDisbursementUnit/disbursement-unit-edit", model);		
	}

	@RequestMapping(value="/disbursement-unit-create")
	public ModelAndView createMDisbursementUnitById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MDisbursementUnit/disbursement-unit-create", model);		
	}

}
