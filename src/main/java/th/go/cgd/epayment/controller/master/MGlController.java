package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.GlBean;
import th.go.cgd.epayment.service.interfaces.IMGlService;

@RestController
@RequestMapping(value="/master2")
public class MGlController {

	@Autowired
	private IMGlService service;	
	
	@RequestMapping(value="/findall-gl", method=RequestMethod.GET)
	public List<GlBean> getGl()
	{
		return service.getAllMGls();
	}

	@RequestMapping(value="/findone-gl/{id}", method=RequestMethod.GET)
	public GlBean getGl(@PathVariable("id") Integer id)
	{
		return service.getMGlById(id);
	}

	@RequestMapping(value="/search-gl", method=RequestMethod.POST)
	public List<GlBean> searchGl(@RequestBody GlBean obj)
	{
		return service.getMGlsByCriteria(obj);
	}	

	@RequestMapping(value="/cancel-gl/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}

	@RequestMapping(value="/create-gl", method=RequestMethod.POST)
	public void createGl(@RequestBody GlBean obj)
	{
		service.createMGl(obj);
	}

	@RequestMapping(value="/update-gl", method=RequestMethod.PUT)
	public void updateGl(@RequestBody GlBean obj)
	{
		service.updateMGl(obj);
	}

	@RequestMapping(value="/delete-gl/{id}", method=RequestMethod.DELETE)
	public void saveGl(@PathVariable("id") Integer id)
	{
		service.deleteMGl(id);
	}
	
	@RequestMapping(value="/gl-view")
	public ModelAndView viewMGl(ModelMap model)
	{
		return new ModelAndView("/views/Master/MGl/gl-view", model);
	}

	@RequestMapping(value="/gl-edit/{id}")
	public ModelAndView editMGlById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MGl/gl-edit", model);		
	}

	@RequestMapping(value="/gl-create")
	public ModelAndView createMGlById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MGl/gl-create", model);		
	}

}
