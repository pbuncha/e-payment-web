package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.service.interfaces.IMMoneySourceService;

@RestController
@RequestMapping(value="/master2")
public class MMoneySourceController {

	@Autowired
	private IMMoneySourceService service;	
	
	@RequestMapping(value="/findall-money-source", method=RequestMethod.GET)
	public List<MoneySourceBean> getMoneySource()
	{
		return service.getAllMMoneySources();
	}

	@RequestMapping(value="/findone-money-source/{id}", method=RequestMethod.GET)
	public MoneySourceBean getMoneySource(@PathVariable("id") Integer id)
	{
		return service.getMMoneySourceById(id);
	}

	@RequestMapping(value="/search-money-source", method=RequestMethod.POST)
	public List<MoneySourceBean> searchMoneySource(@RequestBody MoneySourceBean obj)
	{
		return service.getMMoneySourceByCriteria(obj);
	}	
	
	@RequestMapping(value="/cancel-money-source/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}

	@RequestMapping(value="/create-money-source", method=RequestMethod.POST)
	public void createMoneySource(@RequestBody MoneySourceBean obj)
	{
		service.createMMoneySource(obj);
	}

	@RequestMapping(value="/update-money-source", method=RequestMethod.PUT)
	public void updateMoneySource(@RequestBody MoneySourceBean obj)
	{
		service.updateMMoneySource(obj);
	}

	@RequestMapping(value="/delete-money-source/{id}", method=RequestMethod.DELETE)
	public void saveMoneySource(@PathVariable("id") Integer id)
	{
		service.deleteMMoneySource(id);
	}
	
	@RequestMapping(value="/money-source-view")
	public ModelAndView viewMMoneySource(ModelMap model)
	{
		return new ModelAndView("/views/Master/MMoneySource/money-source-view", model);
	}

	@RequestMapping(value="/money-source-edit/{id}")
	public ModelAndView editMMoneySourceById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MMoneySource/money-source-edit", model);		
	}

	@RequestMapping(value="/money-source-create")
	public ModelAndView createMMoneySourceById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MMoneySource/money-source-create", model);		
	}

}
