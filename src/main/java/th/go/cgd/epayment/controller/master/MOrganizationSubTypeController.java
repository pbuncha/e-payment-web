package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.OrganizationSubTypeBean;
import th.go.cgd.epayment.service.interfaces.IMOrganizationSubTypeService;

@RestController
@RequestMapping(value="/master2")
public class MOrganizationSubTypeController {

	@Autowired
	private IMOrganizationSubTypeService service;	
	
	@RequestMapping(value="/findall-organization-sub-type", method=RequestMethod.GET)
	public List<OrganizationSubTypeBean> getOrganizationSubType()
	{
		return service.getAllMOrganizationSubTypes();
	}

	@RequestMapping(value="/findone-organization-sub-type/{id}", method=RequestMethod.GET)
	public OrganizationSubTypeBean getOrganizationSubType(@PathVariable("id") Integer id)
	{
		return service.getMOrganizationSubTypeById(id);
	}

	@RequestMapping(value="/search-organization-sub-type", method=RequestMethod.GET)
	public List<OrganizationSubTypeBean> searchOrganizationSubType(
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "startDateFrom", required = false) Long startDateFrom,
			@RequestParam(value = "startDateTo", required = false) Long startDateTo,
			@RequestParam(value = "endDateFrom", required = false) Long endDateFrom,
			@RequestParam(value = "endDateTo", required = false) Long endDateTo,
			@RequestParam(value = "status", required = false) String status
			)
	{
		List<OrganizationSubTypeBean> q =  service.getAllMOrganizationSubTypes();

		if (name != null && !name.trim().isEmpty())
		{
			q = q.stream()
					.filter(a -> a.getOrganizationSubTypeName().contains(name))
					.collect(Collectors.toList());
		}
		if (startDateFrom != null)
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() >= startDateFrom )
					.collect(Collectors.toList());						
		}
		if (startDateTo != null)
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= startDateTo )
					.collect(Collectors.toList());						
		}
		if (endDateFrom != null)
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= endDateFrom )
					.collect(Collectors.toList());						
		}
		if (endDateTo != null)
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= endDateTo )
					.collect(Collectors.toList());						
		}
		if (status != null  && !status.trim().isEmpty())
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> status.equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;
	}	
	
	@RequestMapping(value="/create-organization-sub-type", method=RequestMethod.POST)
	public void createOrganizationSubType(@RequestBody OrganizationSubTypeBean obj)
	{
		service.createMOrganizationSubType(obj);
	}

	@RequestMapping(value="/update-organization-sub-type", method=RequestMethod.PUT)
	public void updateOrganizationSubType(@RequestBody OrganizationSubTypeBean obj)
	{
		service.updateMOrganizationSubType(obj);
	}

	@RequestMapping(value="/delete-organization-sub-type/{id}", method=RequestMethod.DELETE)
	public void saveOrganizationSubType(@PathVariable("id") Integer id)
	{
		service.deleteMOrganizationSubType(id);
	}
	
	@RequestMapping(value="/organization-sub-type-view")
	public ModelAndView viewMOrganizationSubType(ModelMap model)
	{
		return new ModelAndView("/views/Master/MOrganizationSubType/organization-sub-type-view", model);
	}

	@RequestMapping(value="/organization-sub-type-edit/{id}")
	public ModelAndView editMOrganizationSubTypeById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MOrganizationSubType/organization-sub-type-edit", model);		
	}

	@RequestMapping(value="/organization-sub-type-create")
	public ModelAndView createMOrganizationSubTypeById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MOrganizationSubType/organization-sub-type-create", model);		
	}

}
