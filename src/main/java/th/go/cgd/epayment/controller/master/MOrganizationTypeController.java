package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.OrganizationTypeBean;
import th.go.cgd.epayment.service.interfaces.IMOrganizationTypeService;

@RestController
@RequestMapping(value="/master2")
public class MOrganizationTypeController {

	@Autowired
	private IMOrganizationTypeService service;	
	
	@RequestMapping(value="/findall-organization-type", method=RequestMethod.GET)
	public List<OrganizationTypeBean> getOrganizationType()
	{
		return service.getAllMOrganizationTypes();
	}

	@RequestMapping(value="/findone-organization-type/{id}", method=RequestMethod.GET)
	public OrganizationTypeBean getOrganizationType(@PathVariable("id") Integer id)
	{
		return service.getMOrganizationTypeById(id);
	}

	@RequestMapping(value="/search-organization-type", method=RequestMethod.GET)
	public List<OrganizationTypeBean> searchOrganizationType(
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "startDateFrom", required = false) Long startDateFrom,
			@RequestParam(value = "startDateTo", required = false) Long startDateTo,
			@RequestParam(value = "endDateFrom", required = false) Long endDateFrom,
			@RequestParam(value = "endDateTo", required = false) Long endDateTo,
			@RequestParam(value = "status", required = false) String status
			)
	{
		List<OrganizationTypeBean> q =  service.getAllMOrganizationTypes();

		if (name != null && !name.trim().isEmpty())
		{
			q = q.stream()
					.filter(a -> a.getOrganizationTypeName().contains(name))
					.collect(Collectors.toList());
		}
		if (startDateFrom != null)
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() >= startDateFrom )
					.collect(Collectors.toList());						
		}
		if (startDateTo != null)
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= startDateTo )
					.collect(Collectors.toList());						
		}
		if (endDateFrom != null)
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= endDateFrom )
					.collect(Collectors.toList());						
		}
		if (endDateTo != null)
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= endDateTo )
					.collect(Collectors.toList());						
		}
		if (status != null  && !status.trim().isEmpty())
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> status.equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;
	}	
	
	@RequestMapping(value="/create-organization-type", method=RequestMethod.POST)
	public void createOrganizationType(@RequestBody OrganizationTypeBean obj)
	{
		service.createMOrganizationType(obj);
	}

	@RequestMapping(value="/update-organization-type", method=RequestMethod.PUT)
	public void updateOrganizationType(@RequestBody OrganizationTypeBean obj)
	{
		service.updateMOrganizationType(obj);
	}

	@RequestMapping(value="/delete-organization-type/{id}", method=RequestMethod.DELETE)
	public void saveOrganizationType(@PathVariable("id") Integer id)
	{
		service.deleteMOrganizationType(id);
	}
	
	@RequestMapping(value="/organization-type-view")
	public ModelAndView viewMOrganizationType(ModelMap model)
	{
		return new ModelAndView("/views/Master/MOrganizationType/organization-type-view", model);
	}

	@RequestMapping(value="/organization-type-edit/{id}")
	public ModelAndView editMOrganizationTypeById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MOrganizationType/organization-type-edit", model);		
	}

	@RequestMapping(value="/organization-type-create")
	public ModelAndView createMOrganizationTypeById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MOrganizationType/organization-type-create", model);		
	}

}
