package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.ProvinceBean;
import th.go.cgd.epayment.service.interfaces.IMProvinceService;

@RestController
@RequestMapping(value="/master2")
public class MProvinceController {

	@Autowired
	private IMProvinceService service;	
	
	@RequestMapping(value="/findall-province", method=RequestMethod.GET)
	public List<ProvinceBean> getProvince()
	{
		return service.getAllMProvinces();
	}

	@RequestMapping(value="/findone-province/{id}", method=RequestMethod.GET)
	public ProvinceBean getProvince(@PathVariable("id") Integer id)
	{
		return service.getMProvinceById(id);
	}

	@RequestMapping(value="/search-province", method=RequestMethod.GET)
	public List<ProvinceBean> searchProvince(
			@RequestParam(value = "nameEn", required = false) String nameEn, 
			@RequestParam(value = "nameTh", required = false) String nameTh
			)
	{
		List<ProvinceBean> q =  service.getAllMProvinces();

		if (nameEn != null && !nameEn.trim().isEmpty())
		{
			q = q.stream()
					.filter(a -> a.getProvinceNameEn().contains(nameEn))
					.collect(Collectors.toList());
		}
		if (nameTh != null && !nameTh.trim().isEmpty())
		{
			q = q.stream()
					.filter(a -> a.getProvinceNameTh().contains(nameTh))
					.collect(Collectors.toList());
		}
		
		return q;
	}	
	
	@RequestMapping(value="/create-province", method=RequestMethod.POST)
	public void createProvince(@RequestBody ProvinceBean obj)
	{
		service.createMProvince(obj);
	}

	@RequestMapping(value="/update-province", method=RequestMethod.PUT)
	public void updateProvince(@RequestBody ProvinceBean obj)
	{
		service.updateMProvince(obj);
	}

	@RequestMapping(value="/delete-province/{id}", method=RequestMethod.DELETE)
	public void saveProvince(@PathVariable("id") Integer id)
	{
		service.deleteMProvince(id);
	}
	
	@RequestMapping(value="/province-view")
	public ModelAndView viewMProvince(ModelMap model)
	{
		return new ModelAndView("/views/Master/MProvince/province-view", model);
	}

	@RequestMapping(value="/province-edit/{id}")
	public ModelAndView editMProvinceById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MProvince/province-edit", model);		
	}

	@RequestMapping(value="/province-create")
	public ModelAndView createMProvinceById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MProvince/province-create", model);		
	}

}
