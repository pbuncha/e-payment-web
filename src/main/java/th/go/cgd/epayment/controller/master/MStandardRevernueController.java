package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.StandardRevernueBean;
import th.go.cgd.epayment.repository.MStandardRevernueRepository;
import th.go.cgd.epayment.service.impl.MStandardRevernueGlServiceImpl;
import th.go.cgd.epayment.service.impl.MStandardRevernueServiceImpl;
import th.go.cgd.epayment.service.interfaces.IMStandardRevernueService;

@RestController
@RequestMapping(value="/master2")
public class MStandardRevernueController {

	@Autowired
	private IMStandardRevernueService service;	

	@RequestMapping(value="/findall-standard-revernue", method=RequestMethod.GET)
	public List<StandardRevernueBean> getStandardRevernue()
	{
		return service.getAllMStandardRevernues();
	}

	@RequestMapping(value="/findone-standard-revernue/{id}", method=RequestMethod.GET)
	public StandardRevernueBean getStandardRevernue(@PathVariable("id") Integer id)
	{
		return service.getMStandardRevernueById(id);
	}

	@RequestMapping(value="/search-standard-revernue", method=RequestMethod.POST)
	public List<StandardRevernueBean> searchStandardRevernue(@RequestBody StandardRevernueBean obj )
	{
		return service.getMStandardRevernuesByCriteria(obj);
	}	
	
	@RequestMapping(value="/cancel-standard-revernue/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}
	
	@RequestMapping(value="/create-standard-revernue", method=RequestMethod.POST)
	public void createStandardRevernue(@RequestBody StandardRevernueBean objBean)
	{
		service.createMStandardRevernue(objBean);
	}

	@RequestMapping(value="/update-standard-revernue", method=RequestMethod.PUT)
	public void updateStandardRevernue(@RequestBody StandardRevernueBean objBean)
	{
		service.updateMStandardRevernue(objBean);
	}

	@RequestMapping(value="/delete-standard-revernue/{id}", method=RequestMethod.DELETE)
	public void saveStandardRevernue(@PathVariable("id") Integer id)
	{
		service.deleteMStandardRevernue(id);
	}
	
	@RequestMapping(value="/standard-revernue-view")
	public ModelAndView viewMStandardRevernue(ModelMap model)
	{
		return new ModelAndView("/views/Master/MStandardRevernue/standard-revernue-view", model);
	}

	@RequestMapping(value="/standard-revernue-edit/{id}")
	public ModelAndView editMStandardRevernueById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MStandardRevernue/standard-revernue-edit", model);		
	}

	@RequestMapping(value="/standard-revernue-create")
	public ModelAndView createMStandardRevernueById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MStandardRevernue/standard-revernue-create", model);		
	}

}
