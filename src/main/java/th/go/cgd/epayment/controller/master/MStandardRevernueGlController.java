package th.go.cgd.epayment.controller.master;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import th.go.cgd.epayment.model.master.StandardRevernueGlBean;
import th.go.cgd.epayment.service.interfaces.IMStandardRevernueGlService;

@RestController
@RequestMapping(value="/master2")
public class MStandardRevernueGlController {

	@Autowired
	private IMStandardRevernueGlService service;	
	
	@RequestMapping(value="/findall-standard-revernue-gl", method=RequestMethod.GET)
	public List<StandardRevernueGlBean> getStandardRevernueGl()
	{
		return service.getAllMStandardRevernueGls();
	}

	@RequestMapping(value="/findone-standard-revernue-gl/{id}", method=RequestMethod.GET)
	public StandardRevernueGlBean getStandardRevernueGl(@PathVariable("id") Integer id)
	{
		return service.getMStandardRevernueGlById(id);
	}

	@RequestMapping(value="/search-standard-revernue-gl", method=RequestMethod.POST)
	public List<StandardRevernueGlBean> searchStandardRevernueGl(@RequestBody StandardRevernueGlBean obj)
	{
		return service.getMStandardRevernueGlByCriteria(obj);
	}	
	
	@RequestMapping(value="/cancel-standard-revernue-gl/{id}", method=RequestMethod.GET)
	public void updateStatusCancel(@PathVariable("id") Integer id)
	{
		service.updateStatusCancel(id);
	}
	
	@RequestMapping(value="/create-standard-revernue-gl", method=RequestMethod.POST)
	public void createStandardRevernueGl(@RequestBody StandardRevernueGlBean standardRevernueGlBean)
	{
		service.createMStandardRevernueGl(standardRevernueGlBean);
	}

	@RequestMapping(value="/update-standard-revernue-gl", method=RequestMethod.PUT)
	public void updateStandardRevernueGl(@RequestBody StandardRevernueGlBean standardRevernueGlBean)
	{
		service.updateMStandardRevernueGl(standardRevernueGlBean);
	}

	@RequestMapping(value="/delete-standard-revernue-gl/{id}", method=RequestMethod.DELETE)
	public void saveStandardRevernueGl(@PathVariable("id") Integer id)
	{
		service.deleteMStandardRevernueGl(id);
	}
	
	@RequestMapping(value="/standard-revernue-gl-view")
	public ModelAndView viewMStandardRevernueGl(ModelMap model)
	{
		return new ModelAndView("/views/Master/MStandardRevernueGl/standard-revernue-gl-view", model);
	}

	@RequestMapping(value="/standard-revernue-gl-edit/{id}")
	public ModelAndView editMStandardRevernueGlById(@PathVariable("id") Integer id, ModelMap model)
	{
		return new ModelAndView("/views/Master/MStandardRevernueGl/standard-revernue-gl-edit", model);		
	}

	@RequestMapping(value="/standard-revernue-gl-create")
	public ModelAndView createMStandardRevernueGlById(ModelMap model)
	{
		return new ModelAndView("/views/Master/MStandardRevernueGl/standard-revernue-gl-create", model);		
	}

}
