package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author buncha.p
 */
@Entity
@Table(name = "ACTION_LOG", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "ActionLog.findAll", query = "SELECT a FROM ActionLog a"),
		@NamedQuery(name = "ActionLog.findByActionlogId", query = "SELECT a FROM ActionLog a WHERE a.actionlogId = :actionlogId"),
		@NamedQuery(name = "ActionLog.findByIp", query = "SELECT a FROM ActionLog a WHERE a.ip = :ip"),
		@NamedQuery(name = "ActionLog.findByUsername", query = "SELECT a FROM ActionLog a WHERE a.username = :username"),
		@NamedQuery(name = "ActionLog.findByTitle", query = "SELECT a FROM ActionLog a WHERE a.title = :title"),
		@NamedQuery(name = "ActionLog.findByDescription", query = "SELECT a FROM ActionLog a WHERE a.description = :description"),
		@NamedQuery(name = "ActionLog.findByCreatedDate", query = "SELECT a FROM ActionLog a WHERE a.createdDate = :createdDate") })
public class ActionLog implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "ACTIONLOG_ID", nullable = false)
	private Integer actionlogId;
	@Column(name = "IP", length = 255)
	private String ip;
	@Column(name = "USERNAME", length = 255)
	private String username;
	@Column(name = "TITLE", length = 255)
	private String title;
	@Column(name = "DESCRIPTION", length = 255)
	private String description;
	@Column(name = "CREATED_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	public ActionLog() {
	}

	public ActionLog(Integer actionlogId) {
		this.actionlogId = actionlogId;
	}

	public Integer getActionlogId() {
		return actionlogId;
	}

	public void setActionlogId(Integer actionlogId) {
		this.actionlogId = actionlogId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (actionlogId != null ? actionlogId.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof ActionLog)) {
			return false;
		}
		ActionLog other = (ActionLog) object;
		if ((this.actionlogId == null && other.actionlogId != null)
				|| (this.actionlogId != null && !this.actionlogId.equals(other.actionlogId))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "entity.ActionLog[ actionlogId=" + actionlogId + " ]";
	}

}
