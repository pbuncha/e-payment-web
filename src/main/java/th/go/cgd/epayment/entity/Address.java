/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "ADDRESS", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Address.findAll", query = "SELECT a FROM Address a"),
    @NamedQuery(name = "Address.findByAddressId", query = "SELECT a FROM Address a WHERE a.addressId = :addressId"),
    @NamedQuery(name = "Address.findByBuildingName", query = "SELECT a FROM Address a WHERE a.buildingName = :buildingName"),
    @NamedQuery(name = "Address.findByCreatedDate", query = "SELECT a FROM Address a WHERE a.createdDate = :createdDate"),
    @NamedQuery(name = "Address.findByIsBilling", query = "SELECT a FROM Address a WHERE a.isBilling = :isBilling"),
    @NamedQuery(name = "Address.findByIsRegistration", query = "SELECT a FROM Address a WHERE a.isRegistration = :isRegistration"),
    @NamedQuery(name = "Address.findByMoo", query = "SELECT a FROM Address a WHERE a.moo = :moo"),
    @NamedQuery(name = "Address.findByNo", query = "SELECT a FROM Address a WHERE a.no = :no"),
    @NamedQuery(name = "Address.findByRoad", query = "SELECT a FROM Address a WHERE a.road = :road"),
    @NamedQuery(name = "Address.findBySoi", query = "SELECT a FROM Address a WHERE a.soi = :soi"),
    @NamedQuery(name = "Address.findByUpdatedDate", query = "SELECT a FROM Address a WHERE a.updatedDate = :updatedDate"),
    @NamedQuery(name = "Address.findByVillage", query = "SELECT a FROM Address a WHERE a.village = :village"),
    @NamedQuery(name = "Address.findByPostcode", query = "SELECT a FROM Address a WHERE a.postcode = :postcode"),
    @NamedQuery(name = "Address.findByLane", query = "SELECT a FROM Address a WHERE a.lane = :lane")})
public class Address implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ADDRESS_ID", nullable = false)
    private Integer addressId;
    @Column(name = "BUILDING_NAME", length = 255)
    private String buildingName;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "IS_BILLING")
    private Character isBilling;
    @Column(name = "IS_REGISTRATION")
    private Character isRegistration;
    @Column(name = "MOO", length = 255)
    private String moo;
    @Column(name = "NO", length = 10)
    private String no;
    @Column(name = "ROAD", length = 255)
    private String road;
    @Column(name = "SOI", length = 255)
    private String soi;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "VILLAGE", length = 255)
    private String village;
    @Column(name = "POSTCODE", length = 20)
    private String postcode;
    @Column(name = "LANE", length = 256)
    private String lane;
    @OneToMany(mappedBy = "addressId", fetch = FetchType.LAZY)
    private List<UserCenter> userCenterList;
    @OneToMany(mappedBy = "addressId", fetch = FetchType.LAZY)
    private List<MDepartment> mDepartmentList;
    @OneToMany(mappedBy = "invoiceAddressIdBilling", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList;
    @OneToMany(mappedBy = "addressIdBilling", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList1;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "TAMBON_ID", referencedColumnName = "TAMBON_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MTambon tambonId;
    @JoinColumn(name = "PROVINCE_ID", referencedColumnName = "PROVINCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MProvince provinceId;
    @JoinColumn(name = "AMPHUR_ID", referencedColumnName = "AMPHUR_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MAmphur amphurId;
    @OneToMany(mappedBy = "addressId", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList;
    @Column(name = "ROOM_NO", length = 10)
    private String roomNo;

    public Address() {
    }

    public Address(Integer addressId) {
        this.addressId = addressId;
    }

    public Address(Integer addressId, Date createdDate) {
        this.addressId = addressId;
        this.createdDate = createdDate;
    }

    public Integer getAddressId() {
        return addressId;
    }

    public void setAddressId(Integer addressId) {
        this.addressId = addressId;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Character getIsBilling() {
        return isBilling;
    }

    public void setIsBilling(Character isBilling) {
        this.isBilling = isBilling;
    }

    public Character getIsRegistration() {
        return isRegistration;
    }

    public void setIsRegistration(Character isRegistration) {
        this.isRegistration = isRegistration;
    }

    public String getMoo() {
        return moo;
    }

    public void setMoo(String moo) {
        this.moo = moo;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getRoad() {
        return road;
    }

    public void setRoad(String road) {
        this.road = road;
    }

    public String getSoi() {
        return soi;
    }

    public void setSoi(String soi) {
        this.soi = soi;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getLane() {
        return lane;
    }

    public void setLane(String lane) {
        this.lane = lane;
    }
    public String getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(String roomNo) {
        this.roomNo = roomNo;
    }
    @XmlTransient
    public List<UserCenter> getUserCenterList() {
        return userCenterList;
    }

    public void setUserCenterList(List<UserCenter> userCenterList) {
        this.userCenterList = userCenterList;
    }

    @XmlTransient
    public List<MDepartment> getMDepartmentList() {
        return mDepartmentList;
    }

    public void setMDepartmentList(List<MDepartment> mDepartmentList) {
        this.mDepartmentList = mDepartmentList;
    }

    @XmlTransient
    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    @XmlTransient
    public List<Invoice> getInvoiceList1() {
        return invoiceList1;
    }

    public void setInvoiceList1(List<Invoice> invoiceList1) {
        this.invoiceList1 = invoiceList1;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public MTambon getTambonId() {
        return tambonId;
    }

    public void setTambonId(MTambon tambonId) {
        this.tambonId = tambonId;
    }

    public MProvince getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(MProvince provinceId) {
        this.provinceId = provinceId;
    }

    public MAmphur getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(MAmphur amphurId) {
        this.amphurId = amphurId;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList() {
        return userOrganizationList;
    }

    public void setUserOrganizationList(List<UserOrganization> userOrganizationList) {
        this.userOrganizationList = userOrganizationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (addressId != null ? addressId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Address)) {
            return false;
        }
        Address other = (Address) object;
        if ((this.addressId == null && other.addressId != null) || (this.addressId != null && !this.addressId.equals(other.addressId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.Address[ addressId=" + addressId + " ]";
    }
    
}
