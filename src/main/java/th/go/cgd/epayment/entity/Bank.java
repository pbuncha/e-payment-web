/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "BANK", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bank.findAll", query = "SELECT b FROM Bank b"),
    @NamedQuery(name = "Bank.findByBankId", query = "SELECT b FROM Bank b WHERE b.bankId = :bankId"),
    @NamedQuery(name = "Bank.findByBankNameEng", query = "SELECT b FROM Bank b WHERE b.bankNameEng = :bankNameEng"),
    @NamedQuery(name = "Bank.findByBankNameTh", query = "SELECT b FROM Bank b WHERE b.bankNameTh = :bankNameTh"),
    @NamedQuery(name = "Bank.findByBankSnEng", query = "SELECT b FROM Bank b WHERE b.bankSnEng = :bankSnEng"),
    @NamedQuery(name = "Bank.findByBankSnThai", query = "SELECT b FROM Bank b WHERE b.bankSnThai = :bankSnThai"),
    @NamedQuery(name = "Bank.findByCreatedDate", query = "SELECT b FROM Bank b WHERE b.createdDate = :createdDate"),
    @NamedQuery(name = "Bank.findByEndDate", query = "SELECT b FROM Bank b WHERE b.endDate = :endDate"),
    @NamedQuery(name = "Bank.findByStartDate", query = "SELECT b FROM Bank b WHERE b.startDate = :startDate"),
    @NamedQuery(name = "Bank.findByStatus", query = "SELECT b FROM Bank b WHERE b.status = :status"),
    @NamedQuery(name = "Bank.findByUpdatedDate", query = "SELECT b FROM Bank b WHERE b.updatedDate = :updatedDate")})
public class Bank implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BANK_ID", nullable = false)
    private Integer bankId;
    @Column(name = "BANK_NAME_ENG", length = 255)
    private String bankNameEng;
    @Column(name = "BANK_NAME_TH", length = 255)
    private String bankNameTh;
    @Column(name = "BANK_SN_ENG", length = 255)
    private String bankSnEng;
    @Column(name = "BANK_SN_THAI", length = 255)
    private String bankSnThai;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "END_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false)
    private char status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bankId", fetch = FetchType.LAZY)
    private List<BankBranch> bankBranchList;

    public Bank() {
    }

    public Bank(Integer bankId) {
        this.bankId = bankId;
    }

    public Bank(Integer bankId, Date endDate, Date startDate, char status) {
        this.bankId = bankId;
        this.endDate = endDate;
        this.startDate = startDate;
        this.status = status;
    }

    public Integer getBankId() {
        return bankId;
    }

    public void setBankId(Integer bankId) {
        this.bankId = bankId;
    }

    public String getBankNameEng() {
        return bankNameEng;
    }

    public void setBankNameEng(String bankNameEng) {
        this.bankNameEng = bankNameEng;
    }

    public String getBankNameTh() {
        return bankNameTh;
    }

    public void setBankNameTh(String bankNameTh) {
        this.bankNameTh = bankNameTh;
    }

    public String getBankSnEng() {
        return bankSnEng;
    }

    public void setBankSnEng(String bankSnEng) {
        this.bankSnEng = bankSnEng;
    }

    public String getBankSnThai() {
        return bankSnThai;
    }

    public void setBankSnThai(String bankSnThai) {
        this.bankSnThai = bankSnThai;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @XmlTransient
    public List<BankBranch> getBankBranchList() {
        return bankBranchList;
    }

    public void setBankBranchList(List<BankBranch> bankBranchList) {
        this.bankBranchList = bankBranchList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bankId != null ? bankId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bank)) {
            return false;
        }
        Bank other = (Bank) object;
        if ((this.bankId == null && other.bankId != null) || (this.bankId != null && !this.bankId.equals(other.bankId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.Bank[ bankId=" + bankId + " ]";
    }
    
}
