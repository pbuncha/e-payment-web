/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "BANK_BRANCH", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BankBranch.findAll", query = "SELECT b FROM BankBranch b"),
    @NamedQuery(name = "BankBranch.findByBankBrId", query = "SELECT b FROM BankBranch b WHERE b.bankBrId = :bankBrId"),
    @NamedQuery(name = "BankBranch.findByBankBrNameEng", query = "SELECT b FROM BankBranch b WHERE b.bankBrNameEng = :bankBrNameEng"),
    @NamedQuery(name = "BankBranch.findByBankBrNameTh", query = "SELECT b FROM BankBranch b WHERE b.bankBrNameTh = :bankBrNameTh"),
    @NamedQuery(name = "BankBranch.findByBankSnEng", query = "SELECT b FROM BankBranch b WHERE b.bankSnEng = :bankSnEng"),
    @NamedQuery(name = "BankBranch.findByBankSnThai", query = "SELECT b FROM BankBranch b WHERE b.bankSnThai = :bankSnThai"),
    @NamedQuery(name = "BankBranch.findByCreatedDate", query = "SELECT b FROM BankBranch b WHERE b.createdDate = :createdDate"),
    @NamedQuery(name = "BankBranch.findByEndDate", query = "SELECT b FROM BankBranch b WHERE b.endDate = :endDate"),
    @NamedQuery(name = "BankBranch.findByStartDate", query = "SELECT b FROM BankBranch b WHERE b.startDate = :startDate"),
    @NamedQuery(name = "BankBranch.findByStatus", query = "SELECT b FROM BankBranch b WHERE b.status = :status"),
    @NamedQuery(name = "BankBranch.findByUpdatedDate", query = "SELECT b FROM BankBranch b WHERE b.updatedDate = :updatedDate")})
public class BankBranch implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BANK_BR_ID", nullable = false)
    private Integer bankBrId;
    @Column(name = "BANK_BR_NAME_ENG", length = 255)
    private String bankBrNameEng;
    @Column(name = "BANK_BR_NAME_TH", length = 255)
    private String bankBrNameTh;
    @Column(name = "BANK_SN_ENG", length = 255)
    private String bankSnEng;
    @Column(name = "BANK_SN_THAI", length = 255)
    private String bankSnThai;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "END_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false)
    private char status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "BANK_ID", referencedColumnName = "BANK_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Bank bankId;

    public BankBranch() {
    }

    public BankBranch(Integer bankBrId) {
        this.bankBrId = bankBrId;
    }

    public BankBranch(Integer bankBrId, Date endDate, Date startDate, char status) {
        this.bankBrId = bankBrId;
        this.endDate = endDate;
        this.startDate = startDate;
        this.status = status;
    }

    public Integer getBankBrId() {
        return bankBrId;
    }

    public void setBankBrId(Integer bankBrId) {
        this.bankBrId = bankBrId;
    }

    public String getBankBrNameEng() {
        return bankBrNameEng;
    }

    public void setBankBrNameEng(String bankBrNameEng) {
        this.bankBrNameEng = bankBrNameEng;
    }

    public String getBankBrNameTh() {
        return bankBrNameTh;
    }

    public void setBankBrNameTh(String bankBrNameTh) {
        this.bankBrNameTh = bankBrNameTh;
    }

    public String getBankSnEng() {
        return bankSnEng;
    }

    public void setBankSnEng(String bankSnEng) {
        this.bankSnEng = bankSnEng;
    }

    public String getBankSnThai() {
        return bankSnThai;
    }

    public void setBankSnThai(String bankSnThai) {
        this.bankSnThai = bankSnThai;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Bank getBankId() {
        return bankId;
    }

    public void setBankId(Bank bankId) {
        this.bankId = bankId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bankBrId != null ? bankBrId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BankBranch)) {
            return false;
        }
        BankBranch other = (BankBranch) object;
        if ((this.bankBrId == null && other.bankBrId != null) || (this.bankBrId != null && !this.bankBrId.equals(other.bankBrId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.BankBranch[ bankBrId=" + bankBrId + " ]";
    }
    
}
