/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "BATCH_FILE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BatchFile.findAll", query = "SELECT b FROM BatchFile b"),
    @NamedQuery(name = "BatchFile.findByBatchFileId", query = "SELECT b FROM BatchFile b WHERE b.batchFileId = :batchFileId"),
    @NamedQuery(name = "BatchFile.findByBatchFileType", query = "SELECT b FROM BatchFile b WHERE b.batchFileType = :batchFileType"),
    @NamedQuery(name = "BatchFile.findByCreatedDate", query = "SELECT b FROM BatchFile b WHERE b.createdDate = :createdDate"),
    @NamedQuery(name = "BatchFile.findByFileDate", query = "SELECT b FROM BatchFile b WHERE b.fileDate = :fileDate"),
    @NamedQuery(name = "BatchFile.findByFileName", query = "SELECT b FROM BatchFile b WHERE b.fileName = :fileName"),
    @NamedQuery(name = "BatchFile.findByStatus", query = "SELECT b FROM BatchFile b WHERE b.status = :status"),
    @NamedQuery(name = "BatchFile.findByTotalAmounts", query = "SELECT b FROM BatchFile b WHERE b.totalAmounts = :totalAmounts"),
    @NamedQuery(name = "BatchFile.findByTotalRecords", query = "SELECT b FROM BatchFile b WHERE b.totalRecords = :totalRecords"),
    @NamedQuery(name = "BatchFile.findByUpdatedDate", query = "SELECT b FROM BatchFile b WHERE b.updatedDate = :updatedDate")})
public class BatchFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BATCH_FILE_ID", nullable = false)
    private Integer batchFileId;
    @Basic(optional = false)
    @Column(name = "BATCH_FILE_TYPE", nullable = false, length = 1)
    private String batchFileType;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "FILE_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date fileDate;
    @Basic(optional = false)
    @Column(name = "FILE_NAME", nullable = false, length = 255)
    private String fileName;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false)
    private char status;
    @Column(name = "TOTAL_AMOUNTS")
    private Integer totalAmounts;
    @Column(name = "TOTAL_RECORDS")
    private Integer totalRecords;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "batchFileId", fetch = FetchType.LAZY)
    private List<BatchProblemFile> batchProblemFileList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "batchFile1", fetch = FetchType.LAZY)
    private BatchFile batchFile;
    @JoinColumn(name = "BATCH_FILE_ID", referencedColumnName = "BATCH_FILE_ID", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private BatchFile batchFile1;

    public BatchFile() {
    }

    public BatchFile(Integer batchFileId) {
        this.batchFileId = batchFileId;
    }

    public BatchFile(Integer batchFileId, String batchFileType, Date createdDate, Date fileDate, String fileName, char status) {
        this.batchFileId = batchFileId;
        this.batchFileType = batchFileType;
        this.createdDate = createdDate;
        this.fileDate = fileDate;
        this.fileName = fileName;
        this.status = status;
    }

    public Integer getBatchFileId() {
        return batchFileId;
    }

    public void setBatchFileId(Integer batchFileId) {
        this.batchFileId = batchFileId;
    }

    public String getBatchFileType() {
        return batchFileType;
    }

    public void setBatchFileType(String batchFileType) {
        this.batchFileType = batchFileType;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getFileDate() {
        return fileDate;
    }

    public void setFileDate(Date fileDate) {
        this.fileDate = fileDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Integer getTotalAmounts() {
        return totalAmounts;
    }

    public void setTotalAmounts(Integer totalAmounts) {
        this.totalAmounts = totalAmounts;
    }

    public Integer getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Integer totalRecords) {
        this.totalRecords = totalRecords;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<BatchProblemFile> getBatchProblemFileList() {
        return batchProblemFileList;
    }

    public void setBatchProblemFileList(List<BatchProblemFile> batchProblemFileList) {
        this.batchProblemFileList = batchProblemFileList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public BatchFile getBatchFile() {
        return batchFile;
    }

    public void setBatchFile(BatchFile batchFile) {
        this.batchFile = batchFile;
    }

    public BatchFile getBatchFile1() {
        return batchFile1;
    }

    public void setBatchFile1(BatchFile batchFile1) {
        this.batchFile1 = batchFile1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (batchFileId != null ? batchFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BatchFile)) {
            return false;
        }
        BatchFile other = (BatchFile) object;
        if ((this.batchFileId == null && other.batchFileId != null) || (this.batchFileId != null && !this.batchFileId.equals(other.batchFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.BatchFile[ batchFileId=" + batchFileId + " ]";
    }
    
}
