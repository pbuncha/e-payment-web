/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "BATCH_FILE_READ_RESULT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "BatchFileReadResult.findAll", query = "SELECT b FROM BatchFileReadResult b") })
public class BatchFileReadResult implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "BATCH_FILE_READ_RESULT_ID", nullable = false)
	private Integer batchFileReadResultId;

	@JoinColumn(name = "BANK_ID", referencedColumnName = "BANK_ID")
	@ManyToOne(fetch = FetchType.LAZY)
	private Bank bankId;

	@Column(name = "BATCH_TYPE_ID")
	private Integer batchTypeId;

	@Column(name = "BATCH_CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date batchCreateDate;

	@Column(name = "BATCH_FILE_NAME", length = 255)
	private String batchFileName;

	@Column(name = "BATCH_ALL_ROW")
	private Integer batchAllRow;

	@Column(name = "BATCH_ALL_AMOUNT")
	private Integer batchAllAmount;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "CREATE_DATE")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createDate;

	@Column(name = "CREATE_BY")
	private Integer createBy;

	public BatchFileReadResult() {
	}

	public Integer getBatchFileReadResultId() {
		return batchFileReadResultId;
	}

	public void setBatchFileReadResultId(Integer batchFileReadResultId) {
		this.batchFileReadResultId = batchFileReadResultId;
	}

	public Bank getBankId() {
		return bankId;
	}

	public void setBankId(Bank bankId) {
		this.bankId = bankId;
	}

	public Integer getBatchTypeId() {
		return batchTypeId;
	}

	public void setBatchTypeId(Integer batchTypeId) {
		this.batchTypeId = batchTypeId;
	}

	public Date getBatchCreateDate() {
		return batchCreateDate;
	}

	public void setBatchCreateDate(Date batchCreateDate) {
		this.batchCreateDate = batchCreateDate;
	}

	public String getBatchFileName() {
		return batchFileName;
	}

	public void setBatchFileName(String batchFileName) {
		this.batchFileName = batchFileName;
	}

	public Integer getBatchAllRow() {
		return batchAllRow;
	}

	public void setBatchAllRow(Integer batchAllRow) {
		this.batchAllRow = batchAllRow;
	}

	public Integer getBatchAllAmount() {
		return batchAllAmount;
	}

	public void setBatchAllAmount(Integer batchAllAmount) {
		this.batchAllAmount = batchAllAmount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getCreateBy() {
		return createBy;
	}

	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}

	@Override
	public String toString() {
		return "th.go.cgd.epayment.entity.batchFileReadResultId[ batchFileReadResultId=" + batchFileReadResultId + " ]";
	}

}
