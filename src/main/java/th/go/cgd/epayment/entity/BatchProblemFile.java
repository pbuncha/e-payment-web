/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "BATCH_PROBLEM_FILE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "BatchProblemFile.findAll", query = "SELECT b FROM BatchProblemFile b"),
    @NamedQuery(name = "BatchProblemFile.findByBatchProblemFileId", query = "SELECT b FROM BatchProblemFile b WHERE b.batchProblemFileId = :batchProblemFileId"),
    @NamedQuery(name = "BatchProblemFile.findByCreatedDate", query = "SELECT b FROM BatchProblemFile b WHERE b.createdDate = :createdDate"),
    @NamedQuery(name = "BatchProblemFile.findByUpdatedDate", query = "SELECT b FROM BatchProblemFile b WHERE b.updatedDate = :updatedDate")})
public class BatchProblemFile implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BATCH_PROBLEM_FILE_ID", nullable = false)
    private Integer batchProblemFileId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "BATCH_FILE_ID", referencedColumnName = "BATCH_FILE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private BatchFile batchFileId;

    public BatchProblemFile() {
    }

    public BatchProblemFile(Integer batchProblemFileId) {
        this.batchProblemFileId = batchProblemFileId;
    }

    public BatchProblemFile(Integer batchProblemFileId, Date createdDate) {
        this.batchProblemFileId = batchProblemFileId;
        this.createdDate = createdDate;
    }

    public Integer getBatchProblemFileId() {
        return batchProblemFileId;
    }

    public void setBatchProblemFileId(Integer batchProblemFileId) {
        this.batchProblemFileId = batchProblemFileId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public BatchFile getBatchFileId() {
        return batchFileId;
    }

    public void setBatchFileId(BatchFile batchFileId) {
        this.batchFileId = batchFileId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (batchProblemFileId != null ? batchProblemFileId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof BatchProblemFile)) {
            return false;
        }
        BatchProblemFile other = (BatchProblemFile) object;
        if ((this.batchProblemFileId == null && other.batchProblemFileId != null) || (this.batchProblemFileId != null && !this.batchProblemFileId.equals(other.batchProblemFileId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.BatchProblemFile[ batchProblemFileId=" + batchProblemFileId + " ]";
    }
    
}
