/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "BILLING", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Billing.findAll", query = "SELECT b FROM Billing b"),
    @NamedQuery(name = "Billing.findByBillingId", query = "SELECT b FROM Billing b WHERE b.billingId = :billingId"),
    @NamedQuery(name = "Billing.findByBillingNo", query = "SELECT b FROM Billing b WHERE b.billingNo = :billingNo"),
    @NamedQuery(name = "Billing.findByCanceledBy", query = "SELECT b FROM Billing b WHERE b.canceledBy = :canceledBy"),
    @NamedQuery(name = "Billing.findByCanceledDate", query = "SELECT b FROM Billing b WHERE b.canceledDate = :canceledDate"),
    @NamedQuery(name = "Billing.findByCreatedDate", query = "SELECT b FROM Billing b WHERE b.createdDate = :createdDate"),
    @NamedQuery(name = "Billing.findByPrintingCopyDate", query = "SELECT b FROM Billing b WHERE b.printingCopyDate = :printingCopyDate"),
    @NamedQuery(name = "Billing.findByPrintingDate", query = "SELECT b FROM Billing b WHERE b.printingDate = :printingDate"),
    @NamedQuery(name = "Billing.findBySeqNo", query = "SELECT b FROM Billing b WHERE b.seqNo = :seqNo"),
    @NamedQuery(name = "Billing.findByStatus", query = "SELECT b FROM Billing b WHERE b.status = :status"),
    @NamedQuery(name = "Billing.findByUpdatedDate", query = "SELECT b FROM Billing b WHERE b.updatedDate = :updatedDate")})
public class Billing implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BILLING_ID", nullable = false)
    private Integer billingId;
    @Basic(optional = false)
    @Column(name = "BILLING_NO", nullable = false, length = 24)
    private String billingNo;
    @Column(name = "CANCELED_BY")
    private Integer canceledBy;
    @Column(name = "CANCELED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date canceledDate;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "PRINTING_COPY_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date printingCopyDate;
    @Column(name = "PRINTING_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date printingDate;
    @Basic(optional = false)
    @Column(name = "SEQ_NO", nullable = false)
    private int seqNo;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false)
    private char status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "billingId", fetch = FetchType.LAZY)
    private List<WithdrawGfmis> withdrawGfmisList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "INVOICE_ID", referencedColumnName = "INVOICE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Invoice invoiceId;
    @Column(name = "GENERATE_NO",length = 25)
    private String generateNo;
    @Column(name = "PRINTED_BY", length = 512)
    private String printedBy;
    @Column(name = "PRINTED_COPY_BY", length = 512)
    private String printedCopyBy;
    

    public Billing() {
    }

    public Billing(Integer billingId) {
        this.billingId = billingId;
    }

    public Billing(Integer billingId, String billingNo, Date createdDate, int seqNo, char status) {
        this.billingId = billingId;
        this.billingNo = billingNo;
        this.createdDate = createdDate;
        this.seqNo = seqNo;
        this.status = status;
    }

    public Integer getBillingId() {
        return billingId;
    }

    public void setBillingId(Integer billingId) {
        this.billingId = billingId;
    }

    public String getBillingNo() {
        return billingNo;
    }

    public void setBillingNo(String billingNo) {
        this.billingNo = billingNo;
    }

    public Integer getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(Integer canceledBy) {
        this.canceledBy = canceledBy;
    }

    public Date getCanceledDate() {
        return canceledDate;
    }

    public void setCanceledDate(Date canceledDate) {
        this.canceledDate = canceledDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getPrintingCopyDate() {
        return printingCopyDate;
    }

    public void setPrintingCopyDate(Date printingCopyDate) {
        this.printingCopyDate = printingCopyDate;
    }

    public Date getPrintingDate() {
        return printingDate;
    }

    public void setPrintingDate(Date printingDate) {
        this.printingDate = printingDate;
    }

    public int getSeqNo() {
        return seqNo;
    }

    public void setSeqNo(int seqNo) {
        this.seqNo = seqNo;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<WithdrawGfmis> getWithdrawGfmisList() {
        return withdrawGfmisList;
    }

    public void setWithdrawGfmisList(List<WithdrawGfmis> withdrawGfmisList) {
        this.withdrawGfmisList = withdrawGfmisList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Invoice getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Invoice invoiceId) {
        this.invoiceId = invoiceId;
    }
    

    public String getGenerateNo() {
        return generateNo;
    }

    public void setGenerateNo(String generateNo) {
        this.generateNo = generateNo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (billingId != null ? billingId.hashCode() : 0);
        return hash;
    }
    public String getPrintedBy() {
        return printedBy;
    }

    public void setPrintedBy(String printedBy) {
        this.printedBy = printedBy;
    }

    public String getPrintedCopyBy() {
        return printedCopyBy;
    }

    public void setPrintedCopyBy(String printedCopyBy) {
        this.printedCopyBy = printedCopyBy;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Billing)) {
            return false;
        }
        Billing other = (Billing) object;
        if ((this.billingId == null && other.billingId != null) || (this.billingId != null && !this.billingId.equals(other.billingId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.Billing[ billingId=" + billingId + " ]";
    }
    
}
