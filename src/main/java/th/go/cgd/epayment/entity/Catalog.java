/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "CATALOG", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Catalog.findAll", query = "SELECT c FROM Catalog c"),
    @NamedQuery(name = "Catalog.findByCatalogId", query = "SELECT c FROM Catalog c WHERE c.catalogId = :catalogId"),
    @NamedQuery(name = "Catalog.findByAmount", query = "SELECT c FROM Catalog c WHERE c.amount = :amount"),
    @NamedQuery(name = "Catalog.findByBreakDate", query = "SELECT c FROM Catalog c WHERE c.breakDate = :breakDate"),
    @NamedQuery(name = "Catalog.findByBreakReason", query = "SELECT c FROM Catalog c WHERE c.breakReason = :breakReason"),
    @NamedQuery(name = "Catalog.findByCatalogCode", query = "SELECT c FROM Catalog c WHERE c.catalogCode = :catalogCode"),
    @NamedQuery(name = "Catalog.findByCatalogName", query = "SELECT c FROM Catalog c WHERE c.catalogName = :catalogName"),
    @NamedQuery(name = "Catalog.findByCreatedDate", query = "SELECT c FROM Catalog c WHERE c.createdDate = :createdDate"),
    @NamedQuery(name = "Catalog.findByEndDate", query = "SELECT c FROM Catalog c WHERE c.endDate = :endDate"),
    @NamedQuery(name = "Catalog.findByIsShow", query = "SELECT c FROM Catalog c WHERE c.isShow = :isShow"),
    @NamedQuery(name = "Catalog.findByStartDate", query = "SELECT c FROM Catalog c WHERE c.startDate = :startDate"),
    @NamedQuery(name = "Catalog.findByStatus", query = "SELECT c FROM Catalog c WHERE c.status = :status"),
    @NamedQuery(name = "Catalog.findByUpdatedDate", query = "SELECT c FROM Catalog c WHERE c.updatedDate = :updatedDate"),
    @NamedQuery(name = "Catalog.findByVersion", query = "SELECT c FROM Catalog c WHERE c.version = :version"),
    @NamedQuery(name = "Catalog.findByApproveDate", query = "SELECT c FROM Catalog c WHERE c.approveDate = :approveDate"),
    @NamedQuery(name = "Catalog.findByCatalogTypeId", query = "SELECT c FROM Catalog c WHERE c.catalogTypeId = :catalogTypeId"),
    @NamedQuery(name = "Catalog.findByApproveBy", query = "SELECT c FROM Catalog c WHERE c.approveBy = :approveBy")})
public class Catalog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CATALOG_ID", nullable = false)
    private Integer catalogId;
    @Basic(optional = false)
    @Column(name = "AMOUNT", nullable = false)
    private int amount;
    @Column(name = "BREAK_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date breakDate;
    @Column(name = "BREAK_REASON", length = 512)
    private String breakReason;
    @Basic(optional = false)
    @Column(name = "CATALOG_CODE", nullable = false, length = 20)
    private String catalogCode;
    @Column(name = "CATALOG_NAME", length = 255)
    private String catalogName;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "IS_SHOW", nullable = false)
    private char isShow;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "VERSION", length = 5)
    private String version;
    @Column(name = "APPROVE_DATE")
    @Temporal(TemporalType.DATE)
    private Date approveDate;
    @Column(name = "APPROVE_BY")
    private Integer approveBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogId", fetch = FetchType.LAZY)
    private List<CatalogConditionText> catalogConditionTextList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogId", fetch = FetchType.LAZY)
    private List<UserFavorite> userFavoriteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogId", fetch = FetchType.LAZY)
    private List<InvoiceItem> invoiceItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogId", fetch = FetchType.LAZY)
    private List<InvoiceLoader> invoiceLoaderList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "REVENUE_TYPE_ID", referencedColumnName = "REVENUE_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MRevenueType revenueTypeId;
    @JoinColumn(name = "CATALOG_TYPE_ID", referencedColumnName = "CATALOG_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MCatalogType catalogTypeId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogId", fetch = FetchType.LAZY)
    private List<CatalogStructure> catalogStructureList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogId", fetch = FetchType.LAZY)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<CatalogCostCenter> catalogCostCenterList;

    public Catalog() {
    }

    public Catalog(Integer catalogId) {
        this.catalogId = catalogId;
    }

    public Catalog(Integer catalogId, int amount, String catalogCode, char isShow, Date startDate) {
        this.catalogId = catalogId;
        this.amount = amount;
        this.catalogCode = catalogCode;
        this.isShow = isShow;
        this.startDate = startDate;
    }

    public Integer getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Integer catalogId) {
        this.catalogId = catalogId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public Date getBreakDate() {
        return breakDate;
    }

    public void setBreakDate(Date breakDate) {
        this.breakDate = breakDate;
    }

    public String getBreakReason() {
        return breakReason;
    }

    public void setBreakReason(String breakReason) {
        this.breakReason = breakReason;
    }

    public String getCatalogCode() {
        return catalogCode;
    }

    public void setCatalogCode(String catalogCode) {
        this.catalogCode = catalogCode;
    }

    public String getCatalogName() {
        return catalogName;
    }

    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public char getIsShow() {
        return isShow;
    }

    public void setIsShow(char isShow) {
        this.isShow = isShow;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Date getApproveDate() {
        return approveDate;
    }

    public void setApproveDate(Date approveDate) {
        this.approveDate = approveDate;
    }

    public Integer getApproveBy() {
        return approveBy;
    }

    public void setApproveBy(Integer approveBy) {
        this.approveBy = approveBy;
    }

    @XmlTransient
    public List<CatalogConditionText> getCatalogConditionTextList() {
        return catalogConditionTextList;
    }

    public void setCatalogConditionTextList(List<CatalogConditionText> catalogConditionTextList) {
        this.catalogConditionTextList = catalogConditionTextList;
    }

    @XmlTransient
    public List<UserFavorite> getUserFavoriteList() {
        return userFavoriteList;
    }

    public void setUserFavoriteList(List<UserFavorite> userFavoriteList) {
        this.userFavoriteList = userFavoriteList;
    }

    @XmlTransient
    public List<InvoiceItem> getInvoiceItemList() {
        return invoiceItemList;
    }

    public void setInvoiceItemList(List<InvoiceItem> invoiceItemList) {
        this.invoiceItemList = invoiceItemList;
    }

    @XmlTransient
    public List<InvoiceLoader> getInvoiceLoaderList() {
        return invoiceLoaderList;
    }

    public void setInvoiceLoaderList(List<InvoiceLoader> invoiceLoaderList) {
        this.invoiceLoaderList = invoiceLoaderList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MRevenueType getRevenueTypeId() {
        return revenueTypeId;
    }

    public void setRevenueTypeId(MRevenueType revenueTypeId) {
        this.revenueTypeId = revenueTypeId;
    }

    public MCatalogType getCatalogTypeId() {
        return catalogTypeId;
    }

    public void setCatalogTypeId(MCatalogType catalogTypeId) {
        this.catalogTypeId = catalogTypeId;
    }

    @XmlTransient
    public List<CatalogStructure> getCatalogStructureList() {
        return catalogStructureList;
    }

    public void setCatalogStructureList(List<CatalogStructure> catalogStructureList) {
        this.catalogStructureList = catalogStructureList;
    }
    @XmlTransient
    public List<CatalogCostCenter> getCatalogCostCenterList() {
        return catalogCostCenterList;
    }

    public void setCatalogCostCenterList(List<CatalogCostCenter> catalogCostCenterList) {
        this.catalogCostCenterList = catalogCostCenterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catalogId != null ? catalogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Catalog)) {
            return false;
        }
        Catalog other = (Catalog) object;
        if ((this.catalogId == null && other.catalogId != null) || (this.catalogId != null && !this.catalogId.equals(other.catalogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.Catalog[ catalogId=" + catalogId + " ]";
    }
    
}
