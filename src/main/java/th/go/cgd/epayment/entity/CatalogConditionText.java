/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "CATALOG_CONDITION_TEXT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatalogConditionText.findAll", query = "SELECT c FROM CatalogConditionText c"),
    @NamedQuery(name = "CatalogConditionText.findByCatalogConditionTextId", query = "SELECT c FROM CatalogConditionText c WHERE c.catalogConditionTextId = :catalogConditionTextId"),
    @NamedQuery(name = "CatalogConditionText.findByConditionText", query = "SELECT c FROM CatalogConditionText c WHERE c.conditionText = :conditionText"),
    @NamedQuery(name = "CatalogConditionText.findByCreatedDate", query = "SELECT c FROM CatalogConditionText c WHERE c.createdDate = :createdDate"),
    @NamedQuery(name = "CatalogConditionText.findByDisplayNo", query = "SELECT c FROM CatalogConditionText c WHERE c.displayNo = :displayNo"),
    @NamedQuery(name = "CatalogConditionText.findByUpdatedDate", query = "SELECT c FROM CatalogConditionText c WHERE c.updatedDate = :updatedDate")})
public class CatalogConditionText implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CATALOG_CONDITION_TEXT_ID", nullable = false)
    private Integer catalogConditionTextId;
    @Basic(optional = false)
    @Column(name = "CONDITION_TEXT", nullable = false, length = 512)
    private String conditionText;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "DISPLAY_NO", nullable = false)
    private int displayNo;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "CATALOG_ID", referencedColumnName = "CATALOG_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Catalog catalogId;

    public CatalogConditionText() {
    }

    public CatalogConditionText(Integer catalogConditionTextId) {
        this.catalogConditionTextId = catalogConditionTextId;
    }

    public CatalogConditionText(Integer catalogConditionTextId, String conditionText, Date createdDate, int displayNo) {
        this.catalogConditionTextId = catalogConditionTextId;
        this.conditionText = conditionText;
        this.createdDate = createdDate;
        this.displayNo = displayNo;
    }

    public Integer getCatalogConditionTextId() {
        return catalogConditionTextId;
    }

    public void setCatalogConditionTextId(Integer catalogConditionTextId) {
        this.catalogConditionTextId = catalogConditionTextId;
    }

    public String getConditionText() {
        return conditionText;
    }

    public void setConditionText(String conditionText) {
        this.conditionText = conditionText;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getDisplayNo() {
        return displayNo;
    }

    public void setDisplayNo(int displayNo) {
        this.displayNo = displayNo;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Catalog getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Catalog catalogId) {
        this.catalogId = catalogId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catalogConditionTextId != null ? catalogConditionTextId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatalogConditionText)) {
            return false;
        }
        CatalogConditionText other = (CatalogConditionText) object;
        if ((this.catalogConditionTextId == null && other.catalogConditionTextId != null) || (this.catalogConditionTextId != null && !this.catalogConditionTextId.equals(other.catalogConditionTextId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.CatalogConditionText[ catalogConditionTextId=" + catalogConditionTextId + " ]";
    }
    
}
