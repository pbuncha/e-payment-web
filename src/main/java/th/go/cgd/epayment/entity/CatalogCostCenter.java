/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "CATALOG_COST_CENTER", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatalogCostCenter.findAll", query = "SELECT c FROM CatalogCostCenter c"),
    @NamedQuery(name = "CatalogCostCenter.findByCatalogCostCenterId", query = "SELECT c FROM CatalogCostCenter c WHERE c.catalogCostCenterId = :catalogCostCenterId"),
    @NamedQuery(name = "CatalogCostCenter.findByCreatedDate", query = "SELECT c FROM CatalogCostCenter c WHERE c.createdDate = :createdDate"),
    @NamedQuery(name = "CatalogCostCenter.findByUpdatedDate", query = "SELECT c FROM CatalogCostCenter c WHERE c.updatedDate = :updatedDate")})
public class CatalogCostCenter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CATALOG_COST_CENTER_ID", nullable = false)
    private Integer catalogCostCenterId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "COST_CENTER_ID", referencedColumnName = "COST_CENTER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MCostCenter costCenterId;
    @JoinColumn(name = "CATALOG_ID", referencedColumnName = "CATALOG_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Catalog catalogId;

    public CatalogCostCenter() {
    }

    public CatalogCostCenter(Integer catalogCostCenterId) {
        this.catalogCostCenterId = catalogCostCenterId;
    }

    public CatalogCostCenter(Integer catalogCostCenterId, Date createdDate) {
        this.catalogCostCenterId = catalogCostCenterId;
        this.createdDate = createdDate;
    }

    public Integer getCatalogCostCenterId() {
        return catalogCostCenterId;
    }

    public void setCatalogCostCenterId(Integer catalogCostCenterId) {
        this.catalogCostCenterId = catalogCostCenterId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MCostCenter getCostCenterId() {
        return costCenterId;
    }

    public void setCostCenterId(MCostCenter costCenterId) {
        this.costCenterId = costCenterId;
    }

    public Catalog getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Catalog catalogId) {
        this.catalogId = catalogId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catalogCostCenterId != null ? catalogCostCenterId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatalogCostCenter)) {
            return false;
        }
        CatalogCostCenter other = (CatalogCostCenter) object;
        if ((this.catalogCostCenterId == null && other.catalogCostCenterId != null) || (this.catalogCostCenterId != null && !this.catalogCostCenterId.equals(other.catalogCostCenterId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.CatalogCostCenter[ catalogCostCenterId=" + catalogCostCenterId + " ]";
    }
    
}
