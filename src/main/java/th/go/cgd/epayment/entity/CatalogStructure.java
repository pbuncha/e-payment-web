/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "CATALOG_STRUCTURE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatalogStructure.findAll", query = "SELECT c FROM CatalogStructure c"),
    @NamedQuery(name = "CatalogStructure.findByCatalogStructureId", query = "SELECT c FROM CatalogStructure c WHERE c.catalogStructureId = :catalogStructureId"),
    @NamedQuery(name = "CatalogStructure.findByCreatedDate", query = "SELECT c FROM CatalogStructure c WHERE c.createdDate = :createdDate"),
    @NamedQuery(name = "CatalogStructure.findByStatus", query = "SELECT c FROM CatalogStructure c WHERE c.status = :status"),
    @NamedQuery(name = "CatalogStructure.findByUpdatedDate", query = "SELECT c FROM CatalogStructure c WHERE c.updatedDate = :updatedDate")})
public class CatalogStructure implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CATALOG_STRUCTURE_ID", nullable = false)
    private Integer catalogStructureId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false)
    private char status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogStructureId", fetch = FetchType.LAZY)
    private List<CatalogStructureItem> catalogStructureItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogId", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<CatalogCostCenter> catalogCostCenterList;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CATALOG_ID", referencedColumnName = "CATALOG_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Catalog catalogId;

    public CatalogStructure() {
    }

    public CatalogStructure(Integer catalogStructureId) {
        this.catalogStructureId = catalogStructureId;
    }

    public CatalogStructure(Integer catalogStructureId, Date createdDate, char status) {
        this.catalogStructureId = catalogStructureId;
        this.createdDate = createdDate;
        this.status = status;
    }

    public Integer getCatalogStructureId() {
        return catalogStructureId;
    }

    public void setCatalogStructureId(Integer catalogStructureId) {
        this.catalogStructureId = catalogStructureId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<CatalogStructureItem> getCatalogStructureItemList() {
        return catalogStructureItemList;
    }

    public void setCatalogStructureItemList(List<CatalogStructureItem> catalogStructureItemList) {
        this.catalogStructureItemList = catalogStructureItemList;
    }

    @XmlTransient
    public List<CatalogCostCenter> getCatalogCostCenterList() {
        return catalogCostCenterList;
    }

    public void setCatalogCostCenterList(List<CatalogCostCenter> catalogCostCenterList) {
        this.catalogCostCenterList = catalogCostCenterList;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Catalog getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Catalog catalogId) {
        this.catalogId = catalogId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catalogStructureId != null ? catalogStructureId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatalogStructure)) {
            return false;
        }
        CatalogStructure other = (CatalogStructure) object;
        if ((this.catalogStructureId == null && other.catalogStructureId != null) || (this.catalogStructureId != null && !this.catalogStructureId.equals(other.catalogStructureId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.CatalogStructure[ catalogStructureId=" + catalogStructureId + " ]";
    }
    
}
