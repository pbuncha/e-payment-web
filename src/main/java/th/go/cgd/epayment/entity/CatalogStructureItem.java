/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "CATALOG_STRUCTURE_ITEM", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CatalogStructureItem.findAll", query = "SELECT c FROM CatalogStructureItem c"),
    @NamedQuery(name = "CatalogStructureItem.findByCatalogStructureItemId", query = "SELECT c FROM CatalogStructureItem c WHERE c.catalogStructureItemId = :catalogStructureItemId"),
    @NamedQuery(name = "CatalogStructureItem.findByCreatedDate", query = "SELECT c FROM CatalogStructureItem c WHERE c.createdDate = :createdDate"),
    @NamedQuery(name = "CatalogStructureItem.findByGlId", query = "SELECT c FROM CatalogStructureItem c WHERE c.glId = :glId"),
    @NamedQuery(name = "CatalogStructureItem.findByPercent", query = "SELECT c FROM CatalogStructureItem c WHERE c.percent = :percent"),
    @NamedQuery(name = "CatalogStructureItem.findByUpdatedDate", query = "SELECT c FROM CatalogStructureItem c WHERE c.updatedDate = :updatedDate")})
public class CatalogStructureItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CATALOG_STRUCTURE_ITEM_ID", nullable = false)
    private Integer catalogStructureItemId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "GL_ID", nullable = false)
    private int glId;
    @Basic(optional = false)
    @Column(name = "PERCENT", nullable = false)
    private int percent;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "REVENUE_TYPE_ID", referencedColumnName = "REVENUE_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MRevenueType revenueTypeId;
    @JoinColumn(name = "LEVY_TYPE_ID", referencedColumnName = "LEVY_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MLevyType levyTypeId;
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "DEPARTMENT_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MDepartment departmentId;
    @JoinColumn(name = "COST_CENTER_ID", referencedColumnName = "COST_CENTER_ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MCostCenter costCenterId;
    @JoinColumn(name = "ACCOUNT_DEPOSIT_ID", referencedColumnName = "ACCOUNT_DEPOSIT_ID")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MAccountDeposit accountDepositId;
    @JoinColumn(name = "CATALOG_STRUCTURE_ID", referencedColumnName = "CATALOG_STRUCTURE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private CatalogStructure catalogStructureId;

    public CatalogStructureItem() {
    }

    public CatalogStructureItem(Integer catalogStructureItemId) {
        this.catalogStructureItemId = catalogStructureItemId;
    }

    public CatalogStructureItem(Integer catalogStructureItemId, Date createdDate, int glId, int percent) {
        this.catalogStructureItemId = catalogStructureItemId;
        this.createdDate = createdDate;
        this.glId = glId;
        this.percent = percent;
    }

    public Integer getCatalogStructureItemId() {
        return catalogStructureItemId;
    }

    public void setCatalogStructureItemId(Integer catalogStructureItemId) {
        this.catalogStructureItemId = catalogStructureItemId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getGlId() {
        return glId;
    }

    public void setGlId(int glId) {
        this.glId = glId;
    }

    public int getPercent() {
        return percent;
    }

    public void setPercent(int percent) {
        this.percent = percent;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public MRevenueType getRevenueTypeId() {
        return revenueTypeId;
    }

    public void setRevenueTypeId(MRevenueType revenueTypeId) {
        this.revenueTypeId = revenueTypeId;
    }

    public MLevyType getLevyTypeId() {
        return levyTypeId;
    }

    public void setLevyTypeId(MLevyType levyTypeId) {
        this.levyTypeId = levyTypeId;
    }

    public MDepartment getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(MDepartment departmentId) {
        this.departmentId = departmentId;
    }

    public MCostCenter getCostCenterId() {
        return costCenterId;
    }

    public void setCostCenterId(MCostCenter costCenterId) {
        this.costCenterId = costCenterId;
    }

    public MAccountDeposit getAccountDepositId() {
        return accountDepositId;
    }

    public void setAccountDepositId(MAccountDeposit accountDepositId) {
        this.accountDepositId = accountDepositId;
    }

    public CatalogStructure getCatalogStructureId() {
        return catalogStructureId;
    }

    public void setCatalogStructureId(CatalogStructure catalogStructureId) {
        this.catalogStructureId = catalogStructureId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catalogStructureItemId != null ? catalogStructureItemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CatalogStructureItem)) {
            return false;
        }
        CatalogStructureItem other = (CatalogStructureItem) object;
        if ((this.catalogStructureItemId == null && other.catalogStructureItemId != null) || (this.catalogStructureItemId != null && !this.catalogStructureItemId.equals(other.catalogStructureItemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.CatalogStructureItem[ catalogStructureItemId=" + catalogStructureItemId + " ]";
    }
    
}
