/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "COMMON_PERSON", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CommonPerson.findAll", query = "SELECT c FROM CommonPerson c"),
    @NamedQuery(name = "CommonPerson.findByPersonId", query = "SELECT c FROM CommonPerson c WHERE c.personId = :personId"),
    @NamedQuery(name = "CommonPerson.findByAcknowledgeDeathDate", query = "SELECT c FROM CommonPerson c WHERE c.acknowledgeDeathDate = :acknowledgeDeathDate"),
    @NamedQuery(name = "CommonPerson.findByAddress", query = "SELECT c FROM CommonPerson c WHERE c.address = :address"),
    @NamedQuery(name = "CommonPerson.findByAge", query = "SELECT c FROM CommonPerson c WHERE c.age = :age"),
    @NamedQuery(name = "CommonPerson.findByBirthDate", query = "SELECT c FROM CommonPerson c WHERE c.birthDate = :birthDate"),
    @NamedQuery(name = "CommonPerson.findByCorporationName", query = "SELECT c FROM CommonPerson c WHERE c.corporationName = :corporationName"),
    @NamedQuery(name = "CommonPerson.findByCurrentPosition", query = "SELECT c FROM CommonPerson c WHERE c.currentPosition = :currentPosition"),
    @NamedQuery(name = "CommonPerson.findByDamagePosition", query = "SELECT c FROM CommonPerson c WHERE c.damagePosition = :damagePosition"),
    @NamedQuery(name = "CommonPerson.findByDeathDate", query = "SELECT c FROM CommonPerson c WHERE c.deathDate = :deathDate"),
    @NamedQuery(name = "CommonPerson.findByDeathFlag", query = "SELECT c FROM CommonPerson c WHERE c.deathFlag = :deathFlag"),
    @NamedQuery(name = "CommonPerson.findByDeathPrescriptionDate", query = "SELECT c FROM CommonPerson c WHERE c.deathPrescriptionDate = :deathPrescriptionDate"),
    @NamedQuery(name = "CommonPerson.findByDeathPrescriptionYear", query = "SELECT c FROM CommonPerson c WHERE c.deathPrescriptionYear = :deathPrescriptionYear"),
    @NamedQuery(name = "CommonPerson.findByExecutorAs", query = "SELECT c FROM CommonPerson c WHERE c.executorAs = :executorAs"),
    @NamedQuery(name = "CommonPerson.findByFirstName", query = "SELECT c FROM CommonPerson c WHERE c.firstName = :firstName"),
    @NamedQuery(name = "CommonPerson.findByGovNo", query = "SELECT c FROM CommonPerson c WHERE c.govNo = :govNo"),
    @NamedQuery(name = "CommonPerson.findByHeritageDate", query = "SELECT c FROM CommonPerson c WHERE c.heritageDate = :heritageDate"),
    @NamedQuery(name = "CommonPerson.findByHeritageYear", query = "SELECT c FROM CommonPerson c WHERE c.heritageYear = :heritageYear"),
    @NamedQuery(name = "CommonPerson.findByLastName", query = "SELECT c FROM CommonPerson c WHERE c.lastName = :lastName"),
    @NamedQuery(name = "CommonPerson.findByOccupation", query = "SELECT c FROM CommonPerson c WHERE c.occupation = :occupation"),
    @NamedQuery(name = "CommonPerson.findByPassportNo", query = "SELECT c FROM CommonPerson c WHERE c.passportNo = :passportNo"),
    @NamedQuery(name = "CommonPerson.findByPersonalNo", query = "SELECT c FROM CommonPerson c WHERE c.personalNo = :personalNo"),
    @NamedQuery(name = "CommonPerson.findByTaxNo", query = "SELECT c FROM CommonPerson c WHERE c.taxNo = :taxNo"),
    @NamedQuery(name = "CommonPerson.findByTitleOther", query = "SELECT c FROM CommonPerson c WHERE c.titleOther = :titleOther"),
    @NamedQuery(name = "CommonPerson.findByZipCode", query = "SELECT c FROM CommonPerson c WHERE c.zipCode = :zipCode")})
public class CommonPerson implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "PERSON_ID", nullable = false)
    private Integer personId;
    @Column(name = "ACKNOWLEDGE_DEATH_DATE")
    @Temporal(TemporalType.DATE)
    private Date acknowledgeDeathDate;
    @Column(name = "ADDRESS", length = 300)
    private String address;
    @Column(name = "AGE")
    private Short age;
    @Column(name = "BIRTH_DATE")
    @Temporal(TemporalType.DATE)
    private Date birthDate;
    @Column(name = "CORPORATION_NAME", length = 200)
    private String corporationName;
    @Column(name = "CURRENT_POSITION", length = 300)
    private String currentPosition;
    @Column(name = "DAMAGE_POSITION", length = 300)
    private String damagePosition;
    @Column(name = "DEATH_DATE")
    @Temporal(TemporalType.DATE)
    private Date deathDate;
    @Column(name = "DEATH_FLAG", length = 1)
    private String deathFlag;
    @Column(name = "DEATH_PRESCRIPTION_DATE")
    @Temporal(TemporalType.DATE)
    private Date deathPrescriptionDate;
    @Column(name = "DEATH_PRESCRIPTION_YEAR")
    private Short deathPrescriptionYear;
    @Column(name = "EXECUTOR_AS", length = 100)
    private String executorAs;
    @Column(name = "FIRST_NAME", length = 100)
    private String firstName;
    @Column(name = "GOV_NO", length = 50)
    private String govNo;
    @Column(name = "HERITAGE_DATE")
    @Temporal(TemporalType.DATE)
    private Date heritageDate;
    @Column(name = "HERITAGE_YEAR")
    private Short heritageYear;
    @Column(name = "LAST_NAME", length = 100)
    private String lastName;
    @Column(name = "OCCUPATION", length = 100)
    private String occupation;
    @Column(name = "PASSPORT_NO", length = 50)
    private String passportNo;
    @Column(name = "PERSONAL_NO", length = 13)
    private String personalNo;
    @Column(name = "TAX_NO", length = 13)
    private String taxNo;
    @Column(name = "TITLE_OTHER", length = 50)
    private String titleOther;
    @Column(name = "ZIP_CODE", length = 5)
    private String zipCode;
    @JoinColumn(name = "ORG_ID", referencedColumnName = "ORG_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Organization orgId;
    @JoinColumn(name = "TITLE_ID", referencedColumnName = "TITLE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MTitleName titleId;
    @JoinColumn(name = "SUB_DISTRICT_ID", referencedColumnName = "SUBDISTRICT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MSubdistrict subDistrictId;
    @JoinColumn(name = "PROVINCE_ID", referencedColumnName = "PROVINCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MProvince provinceId;
    @JoinColumn(name = "PERSON_TYPE_ID", referencedColumnName = "PERSON_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MPersonType personTypeId;
    @JoinColumn(name = "ORG_TYPE_ID", referencedColumnName = "ORG_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MOrgType orgTypeId;
    @JoinColumn(name = "MARITAL_ID", referencedColumnName = "MARITAL_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MMaritalStatus maritalId;
    @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "DISTRICT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MDistrict districtId;

    public CommonPerson() {
    }

    public CommonPerson(Integer personId) {
        this.personId = personId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Date getAcknowledgeDeathDate() {
        return acknowledgeDeathDate;
    }

    public void setAcknowledgeDeathDate(Date acknowledgeDeathDate) {
        this.acknowledgeDeathDate = acknowledgeDeathDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Short getAge() {
        return age;
    }

    public void setAge(Short age) {
        this.age = age;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCorporationName() {
        return corporationName;
    }

    public void setCorporationName(String corporationName) {
        this.corporationName = corporationName;
    }

    public String getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(String currentPosition) {
        this.currentPosition = currentPosition;
    }

    public String getDamagePosition() {
        return damagePosition;
    }

    public void setDamagePosition(String damagePosition) {
        this.damagePosition = damagePosition;
    }

    public Date getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(Date deathDate) {
        this.deathDate = deathDate;
    }

    public String getDeathFlag() {
        return deathFlag;
    }

    public void setDeathFlag(String deathFlag) {
        this.deathFlag = deathFlag;
    }

    public Date getDeathPrescriptionDate() {
        return deathPrescriptionDate;
    }

    public void setDeathPrescriptionDate(Date deathPrescriptionDate) {
        this.deathPrescriptionDate = deathPrescriptionDate;
    }

    public Short getDeathPrescriptionYear() {
        return deathPrescriptionYear;
    }

    public void setDeathPrescriptionYear(Short deathPrescriptionYear) {
        this.deathPrescriptionYear = deathPrescriptionYear;
    }

    public String getExecutorAs() {
        return executorAs;
    }

    public void setExecutorAs(String executorAs) {
        this.executorAs = executorAs;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGovNo() {
        return govNo;
    }

    public void setGovNo(String govNo) {
        this.govNo = govNo;
    }

    public Date getHeritageDate() {
        return heritageDate;
    }

    public void setHeritageDate(Date heritageDate) {
        this.heritageDate = heritageDate;
    }

    public Short getHeritageYear() {
        return heritageYear;
    }

    public void setHeritageYear(Short heritageYear) {
        this.heritageYear = heritageYear;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getPassportNo() {
        return passportNo;
    }

    public void setPassportNo(String passportNo) {
        this.passportNo = passportNo;
    }

    public String getPersonalNo() {
        return personalNo;
    }

    public void setPersonalNo(String personalNo) {
        this.personalNo = personalNo;
    }

    public String getTaxNo() {
        return taxNo;
    }

    public void setTaxNo(String taxNo) {
        this.taxNo = taxNo;
    }

    public String getTitleOther() {
        return titleOther;
    }

    public void setTitleOther(String titleOther) {
        this.titleOther = titleOther;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public Organization getOrgId() {
        return orgId;
    }

    public void setOrgId(Organization orgId) {
        this.orgId = orgId;
    }

    public MTitleName getTitleId() {
        return titleId;
    }

    public void setTitleId(MTitleName titleId) {
        this.titleId = titleId;
    }

    public MSubdistrict getSubDistrictId() {
        return subDistrictId;
    }

    public void setSubDistrictId(MSubdistrict subDistrictId) {
        this.subDistrictId = subDistrictId;
    }

    public MProvince getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(MProvince provinceId) {
        this.provinceId = provinceId;
    }

    public MPersonType getPersonTypeId() {
        return personTypeId;
    }

    public void setPersonTypeId(MPersonType personTypeId) {
        this.personTypeId = personTypeId;
    }

    public MOrgType getOrgTypeId() {
        return orgTypeId;
    }

    public void setOrgTypeId(MOrgType orgTypeId) {
        this.orgTypeId = orgTypeId;
    }

    public MMaritalStatus getMaritalId() {
        return maritalId;
    }

    public void setMaritalId(MMaritalStatus maritalId) {
        this.maritalId = maritalId;
    }

    public MDistrict getDistrictId() {
        return districtId;
    }

    public void setDistrictId(MDistrict districtId) {
        this.districtId = districtId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personId != null ? personId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CommonPerson)) {
            return false;
        }
        CommonPerson other = (CommonPerson) object;
        if ((this.personId == null && other.personId != null) || (this.personId != null && !this.personId.equals(other.personId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.CommonPerson[ personId=" + personId + " ]";
    }
    
}
