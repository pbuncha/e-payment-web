/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "GENERATE_CODE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GenerateCode.findAll", query = "SELECT g FROM GenerateCode g"),
    @NamedQuery(name = "GenerateCode.findByGenerateId", query = "SELECT g FROM GenerateCode g WHERE g.generateId = :generateId"),
    @NamedQuery(name = "GenerateCode.findByGenerateKey", query = "SELECT g FROM GenerateCode g WHERE g.generateKey = :generateKey"),
    @NamedQuery(name = "GenerateCode.findByGenerateValue", query = "SELECT g FROM GenerateCode g WHERE g.generateValue = :generateValue"),
    @NamedQuery(name = "GenerateCode.findByYears", query = "SELECT g FROM GenerateCode g WHERE g.years = :years"),
    @NamedQuery(name = "GenerateCode.findByCreateDate", query = "SELECT g FROM GenerateCode g WHERE g.createDate = :createDate"),
    @NamedQuery(name = "GenerateCode.findByCreateBy", query = "SELECT g FROM GenerateCode g WHERE g.createBy = :createBy"),
    @NamedQuery(name = "GenerateCode.findByUpdateDate", query = "SELECT g FROM GenerateCode g WHERE g.updateDate = :updateDate"),
    @NamedQuery(name = "GenerateCode.findByUpdateBy", query = "SELECT g FROM GenerateCode g WHERE g.updateBy = :updateBy"),
    @NamedQuery(name = "GenerateCode.findByIsDelete", query = "SELECT g FROM GenerateCode g WHERE g.isDelete = :isDelete")})
public class GenerateCode implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GENERATE_ID", nullable = false)
    private Integer generateId;
    @Basic(optional = false)
    @Column(name = "GENERATE_KEY", nullable = false, length = 25)
    private String generateKey;
    @Column(name = "GENERATE_VALUE")
    private BigInteger generateValue;
    @Column(name = "YEARS", length = 8)
    private String years;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "CREATE_BY", nullable = false, length = 50)
    private String createBy;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "UPDATE_BY", length = 50)
    private String updateBy;
    @Column(name = "IS_DELETE")
    private Character isDelete;

    public GenerateCode() {
    }

    public GenerateCode(Integer generateId) {
        this.generateId = generateId;
    }

    public GenerateCode(Integer generateId, String generateKey, Date createDate, String createBy) {
        this.generateId = generateId;
        this.generateKey = generateKey;
        this.createDate = createDate;
        this.createBy = createBy;
    }

    public Integer getGenerateId() {
        return generateId;
    }

    public void setGenerateId(Integer generateId) {
        this.generateId = generateId;
    }

    public String getGenerateKey() {
        return generateKey;
    }

    public void setGenerateKey(String generateKey) {
        this.generateKey = generateKey;
    }

    public BigInteger getGenerateValue() {
        return generateValue;
    }

    public void setGenerateValue(BigInteger generateValue) {
        this.generateValue = generateValue;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Character getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Character isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (generateId != null ? generateId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GenerateCode)) {
            return false;
        }
        GenerateCode other = (GenerateCode) object;
        if ((this.generateId == null && other.generateId != null) || (this.generateId != null && !this.generateId.equals(other.generateId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.GenerateCode[ generateId=" + generateId + " ]";
    }
    
}
