/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "GFMIS_BATCH_DETAIL_LOG", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GfmisBatchDetailLog.findAll", query = "SELECT g FROM GfmisBatchDetailLog g"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByGfmisBatchDatailId", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.gfmisBatchDatailId = :gfmisBatchDatailId"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByAccKGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.accKGf = :accKGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByAccOwnKGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.accOwnKGf = :accOwnKGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByBookBank", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.bookBank = :bookBank"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByCreatedDate", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.createdDate = :createdDate"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByDatePaid", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.datePaid = :datePaid"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByGfmisTypeDocId", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.gfmisTypeDocId = :gfmisTypeDocId"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByGfmisTypePaidId", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.gfmisTypePaidId = :gfmisTypePaidId"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByGlLGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.glLGf = :glLGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByGlMoneyGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.glMoneyGf = :glMoneyGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByGlOGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.glOGf = :glOGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByGlWGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.glWGf = :glWGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByLMoneyGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.lMoneyGf = :lMoneyGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByOMoneyGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.oMoneyGf = :oMoneyGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findBySeq", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.seq = :seq"),
    @NamedQuery(name = "GfmisBatchDetailLog.findBySourceLGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.sourceLGf = :sourceLGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findBySourceOGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.sourceOGf = :sourceOGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByStatus", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.status = :status"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByStdRevenue", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.stdRevenue = :stdRevenue"),
    @NamedQuery(name = "GfmisBatchDetailLog.findBySubDepartGf", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.subDepartGf = :subDepartGf"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByTotal", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.total = :total"),
    @NamedQuery(name = "GfmisBatchDetailLog.findByUpdatedDate", query = "SELECT g FROM GfmisBatchDetailLog g WHERE g.updatedDate = :updatedDate")})
public class GfmisBatchDetailLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GFMIS_BATCH_DATAIL_ID", nullable = false)
    private Integer gfmisBatchDatailId;
    @Column(name = "ACC_K_GF", length = 6)
    private String accKGf;
    @Column(name = "ACC_OWN_K_GF", length = 12)
    private String accOwnKGf;
    @Column(name = "BOOK_BANK", length = 10)
    private String bookBank;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "DATE_PAID")
    @Temporal(TemporalType.DATE)
    private Date datePaid;
    @Basic(optional = false)
    @Column(name = "GFMIS_TYPE_DOC_ID", nullable = false, length = 3)
    private String gfmisTypeDocId;
    @Basic(optional = false)
    @Column(name = "GFMIS_TYPE_PAID_ID", nullable = false, length = 2)
    private String gfmisTypePaidId;
    @Column(name = "GL_L_GF", length = 12)
    private String glLGf;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "GL_MONEY_GF", precision = 19, scale = 2)
    private BigDecimal glMoneyGf;
    @Column(name = "GL_O_GF", length = 12)
    private String glOGf;
    @Column(name = "GL_W_GF", length = 12)
    private String glWGf;
    @Column(name = "L_MONEY_GF", precision = 19, scale = 2)
    private BigDecimal lMoneyGf;
    @Column(name = "O_MONEY_GF", precision = 19, scale = 2)
    private BigDecimal oMoneyGf;
    @Column(name = "SEQ")
    private Integer seq;
    @Column(name = "SOURCE_L_GF", length = 10)
    private String sourceLGf;
    @Column(name = "SOURCE_O_GF", length = 10)
    private String sourceOGf;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "STD_REVENUE", length = 5)
    private String stdRevenue;
    @Column(name = "SUB_DEPART_GF")
    private Integer subDepartGf;
    @Column(name = "TOTAL", precision = 19, scale = 2)
    private BigDecimal total;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "GFMIS_BATCH_H_ID", referencedColumnName = "GFMIS_BATCH_H_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GfmisBatchHLog gfmisBatchHId;
    @JoinColumn(name = "GFMIS_BATCH_F_ID", referencedColumnName = "GFMIS_BATCH_F_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GfmisBatchFLog gfmisBatchFId;

    public GfmisBatchDetailLog() {
    }

    public GfmisBatchDetailLog(Integer gfmisBatchDatailId) {
        this.gfmisBatchDatailId = gfmisBatchDatailId;
    }

    public GfmisBatchDetailLog(Integer gfmisBatchDatailId, String gfmisTypeDocId, String gfmisTypePaidId) {
        this.gfmisBatchDatailId = gfmisBatchDatailId;
        this.gfmisTypeDocId = gfmisTypeDocId;
        this.gfmisTypePaidId = gfmisTypePaidId;
    }

    public Integer getGfmisBatchDatailId() {
        return gfmisBatchDatailId;
    }

    public void setGfmisBatchDatailId(Integer gfmisBatchDatailId) {
        this.gfmisBatchDatailId = gfmisBatchDatailId;
    }

    public String getAccKGf() {
        return accKGf;
    }

    public void setAccKGf(String accKGf) {
        this.accKGf = accKGf;
    }

    public String getAccOwnKGf() {
        return accOwnKGf;
    }

    public void setAccOwnKGf(String accOwnKGf) {
        this.accOwnKGf = accOwnKGf;
    }

    public String getBookBank() {
        return bookBank;
    }

    public void setBookBank(String bookBank) {
        this.bookBank = bookBank;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getDatePaid() {
        return datePaid;
    }

    public void setDatePaid(Date datePaid) {
        this.datePaid = datePaid;
    }

    public String getGfmisTypeDocId() {
        return gfmisTypeDocId;
    }

    public void setGfmisTypeDocId(String gfmisTypeDocId) {
        this.gfmisTypeDocId = gfmisTypeDocId;
    }

    public String getGfmisTypePaidId() {
        return gfmisTypePaidId;
    }

    public void setGfmisTypePaidId(String gfmisTypePaidId) {
        this.gfmisTypePaidId = gfmisTypePaidId;
    }

    public String getGlLGf() {
        return glLGf;
    }

    public void setGlLGf(String glLGf) {
        this.glLGf = glLGf;
    }

    public BigDecimal getGlMoneyGf() {
        return glMoneyGf;
    }

    public void setGlMoneyGf(BigDecimal glMoneyGf) {
        this.glMoneyGf = glMoneyGf;
    }

    public String getGlOGf() {
        return glOGf;
    }

    public void setGlOGf(String glOGf) {
        this.glOGf = glOGf;
    }

    public String getGlWGf() {
        return glWGf;
    }

    public void setGlWGf(String glWGf) {
        this.glWGf = glWGf;
    }

    public BigDecimal getLMoneyGf() {
        return lMoneyGf;
    }

    public void setLMoneyGf(BigDecimal lMoneyGf) {
        this.lMoneyGf = lMoneyGf;
    }

    public BigDecimal getOMoneyGf() {
        return oMoneyGf;
    }

    public void setOMoneyGf(BigDecimal oMoneyGf) {
        this.oMoneyGf = oMoneyGf;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getSourceLGf() {
        return sourceLGf;
    }

    public void setSourceLGf(String sourceLGf) {
        this.sourceLGf = sourceLGf;
    }

    public String getSourceOGf() {
        return sourceOGf;
    }

    public void setSourceOGf(String sourceOGf) {
        this.sourceOGf = sourceOGf;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStdRevenue() {
        return stdRevenue;
    }

    public void setStdRevenue(String stdRevenue) {
        this.stdRevenue = stdRevenue;
    }

    public Integer getSubDepartGf() {
        return subDepartGf;
    }

    public void setSubDepartGf(Integer subDepartGf) {
        this.subDepartGf = subDepartGf;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(BigDecimal total) {
        this.total = total;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public GfmisBatchHLog getGfmisBatchHId() {
        return gfmisBatchHId;
    }

    public void setGfmisBatchHId(GfmisBatchHLog gfmisBatchHId) {
        this.gfmisBatchHId = gfmisBatchHId;
    }

    public GfmisBatchFLog getGfmisBatchFId() {
        return gfmisBatchFId;
    }

    public void setGfmisBatchFId(GfmisBatchFLog gfmisBatchFId) {
        this.gfmisBatchFId = gfmisBatchFId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gfmisBatchDatailId != null ? gfmisBatchDatailId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GfmisBatchDetailLog)) {
            return false;
        }
        GfmisBatchDetailLog other = (GfmisBatchDetailLog) object;
        if ((this.gfmisBatchDatailId == null && other.gfmisBatchDatailId != null) || (this.gfmisBatchDatailId != null && !this.gfmisBatchDatailId.equals(other.gfmisBatchDatailId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.GfmisBatchDetailLog[ gfmisBatchDatailId=" + gfmisBatchDatailId + " ]";
    }
    
}
