/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "GFMIS_BATCH_F_LOG", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GfmisBatchFLog.findAll", query = "SELECT g FROM GfmisBatchFLog g"),
    @NamedQuery(name = "GfmisBatchFLog.findByGfmisBatchFId", query = "SELECT g FROM GfmisBatchFLog g WHERE g.gfmisBatchFId = :gfmisBatchFId"),
    @NamedQuery(name = "GfmisBatchFLog.findByCreatedDate", query = "SELECT g FROM GfmisBatchFLog g WHERE g.createdDate = :createdDate"),
    @NamedQuery(name = "GfmisBatchFLog.findByStatus", query = "SELECT g FROM GfmisBatchFLog g WHERE g.status = :status"),
    @NamedQuery(name = "GfmisBatchFLog.findByTotalAmount", query = "SELECT g FROM GfmisBatchFLog g WHERE g.totalAmount = :totalAmount"),
    @NamedQuery(name = "GfmisBatchFLog.findByTotalRecord", query = "SELECT g FROM GfmisBatchFLog g WHERE g.totalRecord = :totalRecord"),
    @NamedQuery(name = "GfmisBatchFLog.findByUpdatedDate", query = "SELECT g FROM GfmisBatchFLog g WHERE g.updatedDate = :updatedDate")})
public class GfmisBatchFLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GFMIS_BATCH_F_ID", nullable = false)
    private Integer gfmisBatchFId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "STATUS", length = 1)
    private String status;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "TOTAL_AMOUNT", precision = 19, scale = 2)
    private BigDecimal totalAmount;
    @Column(name = "TOTAL_RECORD")
    private Integer totalRecord;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gfmisBatchFId", fetch = FetchType.LAZY)
    private List<GfmisBatchDetailLog> gfmisBatchDetailLogList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;

    public GfmisBatchFLog() {
    }

    public GfmisBatchFLog(Integer gfmisBatchFId) {
        this.gfmisBatchFId = gfmisBatchFId;
    }

    public Integer getGfmisBatchFId() {
        return gfmisBatchFId;
    }

    public void setGfmisBatchFId(Integer gfmisBatchFId) {
        this.gfmisBatchFId = gfmisBatchFId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(Integer totalRecord) {
        this.totalRecord = totalRecord;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<GfmisBatchDetailLog> getGfmisBatchDetailLogList() {
        return gfmisBatchDetailLogList;
    }

    public void setGfmisBatchDetailLogList(List<GfmisBatchDetailLog> gfmisBatchDetailLogList) {
        this.gfmisBatchDetailLogList = gfmisBatchDetailLogList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gfmisBatchFId != null ? gfmisBatchFId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GfmisBatchFLog)) {
            return false;
        }
        GfmisBatchFLog other = (GfmisBatchFLog) object;
        if ((this.gfmisBatchFId == null && other.gfmisBatchFId != null) || (this.gfmisBatchFId != null && !this.gfmisBatchFId.equals(other.gfmisBatchFId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.GfmisBatchFLog[ gfmisBatchFId=" + gfmisBatchFId + " ]";
    }
    
}
