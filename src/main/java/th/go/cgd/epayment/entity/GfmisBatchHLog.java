/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "GFMIS_BATCH_H_LOG", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GfmisBatchHLog.findAll", query = "SELECT g FROM GfmisBatchHLog g"),
    @NamedQuery(name = "GfmisBatchHLog.findByGfmisBatchHId", query = "SELECT g FROM GfmisBatchHLog g WHERE g.gfmisBatchHId = :gfmisBatchHId"),
    @NamedQuery(name = "GfmisBatchHLog.findByCreatedDate", query = "SELECT g FROM GfmisBatchHLog g WHERE g.createdDate = :createdDate"),
    @NamedQuery(name = "GfmisBatchHLog.findByFileName", query = "SELECT g FROM GfmisBatchHLog g WHERE g.fileName = :fileName"),
    @NamedQuery(name = "GfmisBatchHLog.findBySentDate", query = "SELECT g FROM GfmisBatchHLog g WHERE g.sentDate = :sentDate"),
    @NamedQuery(name = "GfmisBatchHLog.findByStatus", query = "SELECT g FROM GfmisBatchHLog g WHERE g.status = :status"),
    @NamedQuery(name = "GfmisBatchHLog.findByUpdatedDate", query = "SELECT g FROM GfmisBatchHLog g WHERE g.updatedDate = :updatedDate")})
public class GfmisBatchHLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GFMIS_BATCH_H_ID", nullable = false)
    private Integer gfmisBatchHId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "FILE_NAME", length = 30)
    private String fileName;
    @Column(name = "SENT_DATE")
    @Temporal(TemporalType.DATE)
    private Date sentDate;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gfmisBatchHId", fetch = FetchType.LAZY)
    private List<GfmisBatchDetailLog> gfmisBatchDetailLogList;

    public GfmisBatchHLog() {
    }

    public GfmisBatchHLog(Integer gfmisBatchHId) {
        this.gfmisBatchHId = gfmisBatchHId;
    }

    public Integer getGfmisBatchHId() {
        return gfmisBatchHId;
    }

    public void setGfmisBatchHId(Integer gfmisBatchHId) {
        this.gfmisBatchHId = gfmisBatchHId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @XmlTransient
    public List<GfmisBatchDetailLog> getGfmisBatchDetailLogList() {
        return gfmisBatchDetailLogList;
    }

    public void setGfmisBatchDetailLogList(List<GfmisBatchDetailLog> gfmisBatchDetailLogList) {
        this.gfmisBatchDetailLogList = gfmisBatchDetailLogList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gfmisBatchHId != null ? gfmisBatchHId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GfmisBatchHLog)) {
            return false;
        }
        GfmisBatchHLog other = (GfmisBatchHLog) object;
        if ((this.gfmisBatchHId == null && other.gfmisBatchHId != null) || (this.gfmisBatchHId != null && !this.gfmisBatchHId.equals(other.gfmisBatchHId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.GfmisBatchHLog[ gfmisBatchHId=" + gfmisBatchHId + " ]";
    }
    
}
