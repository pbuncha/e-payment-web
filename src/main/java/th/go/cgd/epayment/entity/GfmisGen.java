/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "GFMIS_GEN", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GfmisGen.findAll", query = "SELECT g FROM GfmisGen g"),
    @NamedQuery(name = "GfmisGen.findByGfmisGenId", query = "SELECT g FROM GfmisGen g WHERE g.gfmisGenId = :gfmisGenId"),
    @NamedQuery(name = "GfmisGen.findByCreatedDate", query = "SELECT g FROM GfmisGen g WHERE g.createdDate = :createdDate"),
    @NamedQuery(name = "GfmisGen.findByFileName", query = "SELECT g FROM GfmisGen g WHERE g.fileName = :fileName"),
    @NamedQuery(name = "GfmisGen.findByUpdatedDate", query = "SELECT g FROM GfmisGen g WHERE g.updatedDate = :updatedDate")})
public class GfmisGen implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GFMIS_GEN_ID", nullable = false)
    private Integer gfmisGenId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "FILE_NAME", nullable = false, length = 50)
    private String fileName;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gfmisGenId", fetch = FetchType.LAZY)
    private List<GfmisGenItem> gfmisGenItemList;

    public GfmisGen() {
    }

    public GfmisGen(Integer gfmisGenId) {
        this.gfmisGenId = gfmisGenId;
    }

    public GfmisGen(Integer gfmisGenId, Date createdDate, String fileName) {
        this.gfmisGenId = gfmisGenId;
        this.createdDate = createdDate;
        this.fileName = fileName;
    }

    public Integer getGfmisGenId() {
        return gfmisGenId;
    }

    public void setGfmisGenId(Integer gfmisGenId) {
        this.gfmisGenId = gfmisGenId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @XmlTransient
    public List<GfmisGenItem> getGfmisGenItemList() {
        return gfmisGenItemList;
    }

    public void setGfmisGenItemList(List<GfmisGenItem> gfmisGenItemList) {
        this.gfmisGenItemList = gfmisGenItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gfmisGenId != null ? gfmisGenId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GfmisGen)) {
            return false;
        }
        GfmisGen other = (GfmisGen) object;
        if ((this.gfmisGenId == null && other.gfmisGenId != null) || (this.gfmisGenId != null && !this.gfmisGenId.equals(other.gfmisGenId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.GfmisGen[ gfmisGenId=" + gfmisGenId + " ]";
    }
    
}
