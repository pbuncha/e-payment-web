/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "GFMIS_GEN_ITEM", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GfmisGenItem.findAll", query = "SELECT g FROM GfmisGenItem g"),
    @NamedQuery(name = "GfmisGenItem.findByGfmisGenItemId", query = "SELECT g FROM GfmisGenItem g WHERE g.gfmisGenItemId = :gfmisGenItemId"),
    @NamedQuery(name = "GfmisGenItem.findByAmount", query = "SELECT g FROM GfmisGenItem g WHERE g.amount = :amount"),
    @NamedQuery(name = "GfmisGenItem.findByBankBook", query = "SELECT g FROM GfmisGenItem g WHERE g.bankBook = :bankBook"),
    @NamedQuery(name = "GfmisGenItem.findByCreatedDate", query = "SELECT g FROM GfmisGenItem g WHERE g.createdDate = :createdDate"),
    @NamedQuery(name = "GfmisGenItem.findByGlId", query = "SELECT g FROM GfmisGenItem g WHERE g.glId = :glId"),
    @NamedQuery(name = "GfmisGenItem.findByUpdatedDate", query = "SELECT g FROM GfmisGenItem g WHERE g.updatedDate = :updatedDate")})
public class GfmisGenItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GFMIS_GEN_ITEM_ID", nullable = false)
    private Integer gfmisGenItemId;
    @Basic(optional = false)
    @Column(name = "AMOUNT", nullable = false)
    private int amount;
    @Basic(optional = false)
    @Column(name = "BANK_BOOK", nullable = false, length = 6)
    private String bankBook;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "GL_ID", nullable = false)
    private int glId;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "SOURCE_ID", referencedColumnName = "MONEY_SOURCE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MMoneySource sourceId;
    @JoinColumn(name = "COST_CENTER_ID", referencedColumnName = "COST_CENTER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MCostCenter costCenterId;
    @JoinColumn(name = "GFMIS_GEN_ID", referencedColumnName = "GFMIS_GEN_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private GfmisGen gfmisGenId;

    public GfmisGenItem() {
    }

    public GfmisGenItem(Integer gfmisGenItemId) {
        this.gfmisGenItemId = gfmisGenItemId;
    }

    public GfmisGenItem(Integer gfmisGenItemId, int amount, String bankBook, Date createdDate, int glId) {
        this.gfmisGenItemId = gfmisGenItemId;
        this.amount = amount;
        this.bankBook = bankBook;
        this.createdDate = createdDate;
        this.glId = glId;
    }

    public Integer getGfmisGenItemId() {
        return gfmisGenItemId;
    }

    public void setGfmisGenItemId(Integer gfmisGenItemId) {
        this.gfmisGenItemId = gfmisGenItemId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getBankBook() {
        return bankBook;
    }

    public void setBankBook(String bankBook) {
        this.bankBook = bankBook;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public int getGlId() {
        return glId;
    }

    public void setGlId(int glId) {
        this.glId = glId;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MMoneySource getSourceId() {
        return sourceId;
    }

    public void setSourceId(MMoneySource sourceId) {
        this.sourceId = sourceId;
    }

    public MCostCenter getCostCenterId() {
        return costCenterId;
    }

    public void setCostCenterId(MCostCenter costCenterId) {
        this.costCenterId = costCenterId;
    }

    public GfmisGen getGfmisGenId() {
        return gfmisGenId;
    }

    public void setGfmisGenId(GfmisGen gfmisGenId) {
        this.gfmisGenId = gfmisGenId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gfmisGenItemId != null ? gfmisGenItemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GfmisGenItem)) {
            return false;
        }
        GfmisGenItem other = (GfmisGenItem) object;
        if ((this.gfmisGenItemId == null && other.gfmisGenItemId != null) || (this.gfmisGenItemId != null && !this.gfmisGenItemId.equals(other.gfmisGenItemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.GfmisGenItem[ gfmisGenItemId=" + gfmisGenItemId + " ]";
    }
    
}
