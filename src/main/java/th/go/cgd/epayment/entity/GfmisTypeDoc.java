/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "GFMIS_TYPE_DOC", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GfmisTypeDoc.findAll", query = "SELECT g FROM GfmisTypeDoc g"),
    @NamedQuery(name = "GfmisTypeDoc.findByGfmisTypeDocId", query = "SELECT g FROM GfmisTypeDoc g WHERE g.gfmisTypeDocId = :gfmisTypeDocId"),
    @NamedQuery(name = "GfmisTypeDoc.findByCreatedDate", query = "SELECT g FROM GfmisTypeDoc g WHERE g.createdDate = :createdDate"),
    @NamedQuery(name = "GfmisTypeDoc.findByGfmisTypeDocName", query = "SELECT g FROM GfmisTypeDoc g WHERE g.gfmisTypeDocName = :gfmisTypeDocName"),
    @NamedQuery(name = "GfmisTypeDoc.findByUpdatedDate", query = "SELECT g FROM GfmisTypeDoc g WHERE g.updatedDate = :updatedDate")})
public class GfmisTypeDoc implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GFMIS_TYPE_DOC_ID", nullable = false)
    private Integer gfmisTypeDocId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "GFMIS_TYPE_DOC_NAME", length = 255)
    private String gfmisTypeDocName;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;

    public GfmisTypeDoc() {
    }

    public GfmisTypeDoc(Integer gfmisTypeDocId) {
        this.gfmisTypeDocId = gfmisTypeDocId;
    }

    public Integer getGfmisTypeDocId() {
        return gfmisTypeDocId;
    }

    public void setGfmisTypeDocId(Integer gfmisTypeDocId) {
        this.gfmisTypeDocId = gfmisTypeDocId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getGfmisTypeDocName() {
        return gfmisTypeDocName;
    }

    public void setGfmisTypeDocName(String gfmisTypeDocName) {
        this.gfmisTypeDocName = gfmisTypeDocName;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gfmisTypeDocId != null ? gfmisTypeDocId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GfmisTypeDoc)) {
            return false;
        }
        GfmisTypeDoc other = (GfmisTypeDoc) object;
        if ((this.gfmisTypeDocId == null && other.gfmisTypeDocId != null) || (this.gfmisTypeDocId != null && !this.gfmisTypeDocId.equals(other.gfmisTypeDocId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.GfmisTypeDoc[ gfmisTypeDocId=" + gfmisTypeDocId + " ]";
    }
    
}
