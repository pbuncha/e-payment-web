/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "GP_STD_REVERNUE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GpStdRevernue.findAll", query = "SELECT g FROM GpStdRevernue g"),
    @NamedQuery(name = "GpStdRevernue.findByGpStdRevernueId", query = "SELECT g FROM GpStdRevernue g WHERE g.gpStdRevernueId = :gpStdRevernueId"),
    @NamedQuery(name = "GpStdRevernue.findByGpStdRevernueName", query = "SELECT g FROM GpStdRevernue g WHERE g.gpStdRevernueName = :gpStdRevernueName"),
    @NamedQuery(name = "GpStdRevernue.findByCreatedBy", query = "SELECT g FROM GpStdRevernue g WHERE g.createdBy = :createdBy"),
    @NamedQuery(name = "GpStdRevernue.findByUpdatedBy", query = "SELECT g FROM GpStdRevernue g WHERE g.updatedBy = :updatedBy"),
    @NamedQuery(name = "GpStdRevernue.findByUpdatedDate", query = "SELECT g FROM GpStdRevernue g WHERE g.updatedDate = :updatedDate"),
    @NamedQuery(name = "GpStdRevernue.findByCreatedDate", query = "SELECT g FROM GpStdRevernue g WHERE g.createdDate = :createdDate")})
public class GpStdRevernue implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "GP_STD_REVERNUE_ID", nullable = false)
    private Integer gpStdRevernueId;
    @Column(name = "GP_STD_REVERNUE_NAME", length = 10)
    private String gpStdRevernueName;
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    public GpStdRevernue() {
    }

    public GpStdRevernue(Integer gpStdRevernueId) {
        this.gpStdRevernueId = gpStdRevernueId;
    }

    public Integer getGpStdRevernueId() {
        return gpStdRevernueId;
    }

    public void setGpStdRevernueId(Integer gpStdRevernueId) {
        this.gpStdRevernueId = gpStdRevernueId;
    }

    public String getGpStdRevernueName() {
        return gpStdRevernueName;
    }

    public void setGpStdRevernueName(String gpStdRevernueName) {
        this.gpStdRevernueName = gpStdRevernueName;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gpStdRevernueId != null ? gpStdRevernueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GpStdRevernue)) {
            return false;
        }
        GpStdRevernue other = (GpStdRevernue) object;
        if ((this.gpStdRevernueId == null && other.gpStdRevernueId != null) || (this.gpStdRevernueId != null && !this.gpStdRevernueId.equals(other.gpStdRevernueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.GpStdRevernue[ gpStdRevernueId=" + gpStdRevernueId + " ]";
    }
    
}
