/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "INVOICE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Invoice.findAll", query = "SELECT i FROM Invoice i"),
    @NamedQuery(name = "Invoice.findByInvoiceId", query = "SELECT i FROM Invoice i WHERE i.invoiceId = :invoiceId"),
    @NamedQuery(name = "Invoice.findByCitizenNo", query = "SELECT i FROM Invoice i WHERE i.citizenNo = :citizenNo"),
    @NamedQuery(name = "Invoice.findByCreatedDate", query = "SELECT i FROM Invoice i WHERE i.createdDate = :createdDate"),
    @NamedQuery(name = "Invoice.findByEmail", query = "SELECT i FROM Invoice i WHERE i.email = :email"),
    @NamedQuery(name = "Invoice.findByEndDate", query = "SELECT i FROM Invoice i WHERE i.endDate = :endDate"),
    @NamedQuery(name = "Invoice.findByFirstName", query = "SELECT i FROM Invoice i WHERE i.firstName = :firstName"),
    @NamedQuery(name = "Invoice.findByInvoiceNo", query = "SELECT i FROM Invoice i WHERE i.invoiceNo = :invoiceNo"),
    @NamedQuery(name = "Invoice.findByInvoiceType", query = "SELECT i FROM Invoice i WHERE i.invoiceType = :invoiceType"),
    @NamedQuery(name = "Invoice.findByLastName", query = "SELECT i FROM Invoice i WHERE i.lastName = :lastName"),
    @NamedQuery(name = "Invoice.findByMiddleName", query = "SELECT i FROM Invoice i WHERE i.middleName = :middleName"),
    @NamedQuery(name = "Invoice.findByPaymentDate", query = "SELECT i FROM Invoice i WHERE i.paymentDate = :paymentDate"),
    @NamedQuery(name = "Invoice.findByRef1", query = "SELECT i FROM Invoice i WHERE i.ref1 = :ref1"),
    @NamedQuery(name = "Invoice.findByRef2", query = "SELECT i FROM Invoice i WHERE i.ref2 = :ref2"),
    @NamedQuery(name = "Invoice.findByRef3", query = "SELECT i FROM Invoice i WHERE i.ref3 = :ref3"),
    @NamedQuery(name = "Invoice.findByStartDate", query = "SELECT i FROM Invoice i WHERE i.startDate = :startDate"),
    @NamedQuery(name = "Invoice.findByStatus", query = "SELECT i FROM Invoice i WHERE i.status = :status"),
    @NamedQuery(name = "Invoice.findByTelephone", query = "SELECT i FROM Invoice i WHERE i.telephone = :telephone"),
    @NamedQuery(name = "Invoice.findByTotalAmount", query = "SELECT i FROM Invoice i WHERE i.totalAmount = :totalAmount"),
    @NamedQuery(name = "Invoice.findByUpdatedDate", query = "SELECT i FROM Invoice i WHERE i.updatedDate = :updatedDate"),
    @NamedQuery(name = "Invoice.findByUserTypeId", query = "SELECT i FROM Invoice i WHERE i.userTypeId = :userTypeId")})
public class Invoice implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INVOICE_ID", nullable = false)
    private Integer invoiceId;
    @Basic(optional = false)
    @Column(name = "CITIZEN_NO", nullable = false, length = 20)
    private String citizenNo;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "EMAIL", length = 255)
    private String email;
    @Basic(optional = false)
    @Column(name = "END_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "FIRST_NAME", nullable = false, length = 255)
    private String firstName;
    @Basic(optional = false)
    @Column(name = "INVOICE_NO", nullable = false, length = 255)
    private String invoiceNo;
    @Basic(optional = false)
    @Column(name = "INVOICE_TYPE", nullable = false)
    private Character invoiceType;
    @Basic(optional = false)
    @Column(name = "LAST_NAME", nullable = false, length = 255)
    private String lastName;
    @Column(name = "MIDDLE_NAME", length = 255)
    private String middleName;
    @Column(name = "PAYMENT_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date paymentDate;
    @Column(name = "REF_1", length = 20)
    private String ref1;
    @Column(name = "REF_2", length = 20)
    private String ref2;
    @Column(name = "REF_3", length = 20)
    private String ref3;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false)
    private Character status;
    @Basic(optional = false)
    @Column(name = "TELEPHONE", nullable = false, length = 50)
    private String telephone;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "TOTAL_AMOUNT", nullable = false, precision = 18, scale = 2)
    private BigDecimal totalAmount;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Basic(optional = false)
    @Column(name = "USER_TYPE_ID", nullable = false)
    private Character userTypeId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoiceId", fetch = FetchType.LAZY)
    private List<InvoiceItem> invoiceItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoiceId", fetch = FetchType.LAZY)
    private List<Billing> billingList;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User userId;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "TITLE_ID", referencedColumnName = "TITLE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MTitleName titleId;
    @JoinColumn(name = "PAYMENT_TYPE_ID", referencedColumnName = "PAYMENT_TYPE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MPaymentType paymentTypeId;
    @JoinColumn(name = "PAYMENT_CHANNEL_ID", referencedColumnName = "PAYMENT_CHANNEL_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MPaymentChannel paymentChannelId;
    @JoinColumn(name = "INVOICE_ADDRESS_ID_BILLING", referencedColumnName = "ADDRESS_ID")
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Address invoiceAddressIdBilling;
    @JoinColumn(name = "ADDRESS_ID_BILLING", referencedColumnName = "ADDRESS_ID")
    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private Address addressIdBilling;
    @Column(name = "PERSON_TYPE_ID")
    private Integer personTypeId;
    @Column(name = "CAN_SEND_EMAIL")
    private Character canSendEmail;
    @Column(name = "CAN_SEND_MOBILE")
    private Character canSendMobile;
    
    public Invoice() {
    }

    public Invoice(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Invoice(Integer invoiceId, String citizenNo, Date endDate, String firstName, String invoiceNo, Character invoiceType, String lastName, Date startDate, Character status, String telephone, BigDecimal totalAmount, Character userTypeId) {
        this.invoiceId = invoiceId;
        this.citizenNo = citizenNo;
        this.endDate = endDate;
        this.firstName = firstName;
        this.invoiceNo = invoiceNo;
        this.invoiceType = invoiceType;
        this.lastName = lastName;
        this.startDate = startDate;
        this.status = status;
        this.telephone = telephone;
        this.totalAmount = totalAmount;
        this.userTypeId = userTypeId;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getCitizenNo() {
        return citizenNo;
    }

    public void setCitizenNo(String citizenNo) {
        this.citizenNo = citizenNo;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Character getInvoiceType() {
        return invoiceType;
    }

    public void setInvoiceType(Character invoiceType) {
        this.invoiceType = invoiceType;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getRef1() {
        return ref1;
    }

    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    public String getRef2() {
        return ref2;
    }

    public void setRef2(String ref2) {
        this.ref2 = ref2;
    }

    public String getRef3() {
        return ref3;
    }

    public void setRef3(String ref3) {
        this.ref3 = ref3;
    }
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Character getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Character userTypeId) {
        this.userTypeId = userTypeId;
    }

    @XmlTransient
    public List<InvoiceItem> getInvoiceItemList() {
        return invoiceItemList;
    }

    public void setInvoiceItemList(List<InvoiceItem> invoiceItemList) {
        this.invoiceItemList = invoiceItemList;
    }

    @XmlTransient
    public List<Billing> getBillingList() {
        return billingList;
    }

    public void setBillingList(List<Billing> billingList) {
        this.billingList = billingList;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MTitleName getTitleId() {
        return titleId;
    }

    public void setTitleId(MTitleName titleId) {
        this.titleId = titleId;
    }

    public MPaymentType getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(MPaymentType paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public MPaymentChannel getPaymentChannelId() {
        return paymentChannelId;
    }

    public void setPaymentChannelId(MPaymentChannel paymentChannelId) {
        this.paymentChannelId = paymentChannelId;
    }

    public Address getInvoiceAddressIdBilling() {
        return invoiceAddressIdBilling;
    }

    public void setInvoiceAddressIdBilling(Address invoiceAddressIdBilling) {
        this.invoiceAddressIdBilling = invoiceAddressIdBilling;
    }

    public Address getAddressIdBilling() {
        return addressIdBilling;
    }

    public void setAddressIdBilling(Address addressIdBilling) {
        this.addressIdBilling = addressIdBilling;
    }
    
    public Integer getPersonTypeId() {
        return personTypeId;
    }

    public void setPersonTypeId(Integer personTypeId) {
        this.personTypeId = personTypeId;
    }


    public Character getCanSendEmail() {
		return canSendEmail;
	}

	public void setCanSendEmail(Character canSendEmail) {
		this.canSendEmail = canSendEmail;
	}

	public Character getCanSendMobile() {
		return canSendMobile;
	}

	public void setCanSendMobile(Character canSendMobile) {
		this.canSendMobile = canSendMobile;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (invoiceId != null ? invoiceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Invoice)) {
            return false;
        }
        Invoice other = (Invoice) object;
        if ((this.invoiceId == null && other.invoiceId != null) || (this.invoiceId != null && !this.invoiceId.equals(other.invoiceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.Invoice[ invoiceId=" + invoiceId + " ]";
    }
    
}
