/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "INVOICE_LOADER", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "InvoiceLoader.findAll", query = "SELECT i FROM InvoiceLoader i"),
    @NamedQuery(name = "InvoiceLoader.findByCatalogId", query = "SELECT i FROM InvoiceLoader i WHERE i.catalogId = :catalogId"),
    @NamedQuery(name = "InvoiceLoader.findByCitizenNo", query = "SELECT i FROM InvoiceLoader i WHERE i.citizenNo = :citizenNo"),
    @NamedQuery(name = "InvoiceLoader.findByCreatedBy", query = "SELECT i FROM InvoiceLoader i WHERE i.createdBy = :createdBy"),
    @NamedQuery(name = "InvoiceLoader.findByCreatedDate", query = "SELECT i FROM InvoiceLoader i WHERE i.createdDate = :createdDate"),
    @NamedQuery(name = "InvoiceLoader.findByFileName", query = "SELECT i FROM InvoiceLoader i WHERE i.fileName = :fileName"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceLoaderId", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceLoaderId = :invoiceLoaderId"),
    @NamedQuery(name = "InvoiceLoader.findByLoadDate", query = "SELECT i FROM InvoiceLoader i WHERE i.loadDate = :loadDate"),
    @NamedQuery(name = "InvoiceLoader.findByUpdatedBy", query = "SELECT i FROM InvoiceLoader i WHERE i.updatedBy = :updatedBy"),
    @NamedQuery(name = "InvoiceLoader.findByUpdatedDate", query = "SELECT i FROM InvoiceLoader i WHERE i.updatedDate = :updatedDate"),
    @NamedQuery(name = "InvoiceLoader.findByRunningNo", query = "SELECT i FROM InvoiceLoader i WHERE i.runningNo = :runningNo"),
    @NamedQuery(name = "InvoiceLoader.findByCostCenterCode", query = "SELECT i FROM InvoiceLoader i WHERE i.costCenterCode = :costCenterCode"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceItemDesc", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceItemDesc = :invoiceItemDesc"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceStartDate", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceStartDate = :invoiceStartDate"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceEndDate", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceEndDate = :invoiceEndDate"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceItemAmount", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceItemAmount = :invoiceItemAmount"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceItemRef1", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceItemRef1 = :invoiceItemRef1"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceItemRef2", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceItemRef2 = :invoiceItemRef2"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceItemRef3", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceItemRef3 = :invoiceItemRef3"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceCitizenNo", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceCitizenNo = :invoiceCitizenNo"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceTitle", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceTitle = :invoiceTitle"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceFirstName", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceFirstName = :invoiceFirstName"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceMiddleName", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceMiddleName = :invoiceMiddleName"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceLastName", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceLastName = :invoiceLastName"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceBusinessNo1", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceBusinessNo1 = :invoiceBusinessNo1"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceBusinessNo2", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceBusinessNo2 = :invoiceBusinessNo2"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceBusinessName1", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceBusinessName1 = :invoiceBusinessName1"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceBusinessName2", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceBusinessName2 = :invoiceBusinessName2"),
    @NamedQuery(name = "InvoiceLoader.findByAddressNo", query = "SELECT i FROM InvoiceLoader i WHERE i.addressNo = :addressNo"),
    @NamedQuery(name = "InvoiceLoader.findByAddressBuildingName", query = "SELECT i FROM InvoiceLoader i WHERE i.addressBuildingName = :addressBuildingName"),
    @NamedQuery(name = "InvoiceLoader.findByAddressMoo", query = "SELECT i FROM InvoiceLoader i WHERE i.addressMoo = :addressMoo"),
    @NamedQuery(name = "InvoiceLoader.findByAddressLane", query = "SELECT i FROM InvoiceLoader i WHERE i.addressLane = :addressLane"),
    @NamedQuery(name = "InvoiceLoader.findByAddressSoi", query = "SELECT i FROM InvoiceLoader i WHERE i.addressSoi = :addressSoi"),
    @NamedQuery(name = "InvoiceLoader.findByAddressRoad", query = "SELECT i FROM InvoiceLoader i WHERE i.addressRoad = :addressRoad"),
    @NamedQuery(name = "InvoiceLoader.findByAddressAmphur", query = "SELECT i FROM InvoiceLoader i WHERE i.addressAmphur = :addressAmphur"),
    @NamedQuery(name = "InvoiceLoader.findByAddressTambon", query = "SELECT i FROM InvoiceLoader i WHERE i.addressTambon = :addressTambon"),
    @NamedQuery(name = "InvoiceLoader.findByAddressProvince", query = "SELECT i FROM InvoiceLoader i WHERE i.addressProvince = :addressProvince"),
    @NamedQuery(name = "InvoiceLoader.findByAddressPostcode", query = "SELECT i FROM InvoiceLoader i WHERE i.addressPostcode = :addressPostcode"),
    @NamedQuery(name = "InvoiceLoader.findByUserMobile", query = "SELECT i FROM InvoiceLoader i WHERE i.userMobile = :userMobile"),
    @NamedQuery(name = "InvoiceLoader.findByCanSendMobile", query = "SELECT i FROM InvoiceLoader i WHERE i.canSendMobile = :canSendMobile"),
    @NamedQuery(name = "InvoiceLoader.findByUserEmail", query = "SELECT i FROM InvoiceLoader i WHERE i.userEmail = :userEmail"),
    @NamedQuery(name = "InvoiceLoader.findByCanSendEmail", query = "SELECT i FROM InvoiceLoader i WHERE i.canSendEmail = :canSendEmail"),
    @NamedQuery(name = "InvoiceLoader.findByColumn1", query = "SELECT i FROM InvoiceLoader i WHERE i.column1 = :column1"),
    @NamedQuery(name = "InvoiceLoader.findByGovOtherDetail1", query = "SELECT i FROM InvoiceLoader i WHERE i.govOtherDetail1 = :govOtherDetail1"),
    @NamedQuery(name = "InvoiceLoader.findByGovOtherDetail2", query = "SELECT i FROM InvoiceLoader i WHERE i.govOtherDetail2 = :govOtherDetail2"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceCondition", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceCondition = :invoiceCondition"),
    @NamedQuery(name = "InvoiceLoader.findByGroupBy", query = "SELECT i FROM InvoiceLoader i WHERE i.groupBy = :groupBy"),
    @NamedQuery(name = "InvoiceLoader.findByInvoiceLoaderCode", query = "SELECT i FROM InvoiceLoader i WHERE i.invoiceLoaderCode = :invoiceLoaderCode order by groupBy"),
    @NamedQuery(name = "InvoiceLoader.findByComfirmDate", query = "SELECT i FROM InvoiceLoader i WHERE i.comfirmDate = :comfirmDate"),
    @NamedQuery(name = "InvoiceLoader.findByCancelDate", query = "SELECT i FROM InvoiceLoader i WHERE i.cancelDate = :cancelDate")})
public class InvoiceLoader implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "CATALOG_ID", length = 20 , nullable = false)
    private String catalogId;
    @Column(name = "CITIZEN_NO", length = 20)
    private String citizenNo;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "FILE_NAME", length = 255)
    private String fileName;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "INVOICE_LOADER_ID", nullable = false)
    private Integer invoiceLoaderId;
    @Column(name = "LOAD_DATE")
    @Temporal(TemporalType.DATE)
    private Date loadDate;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "RUNNING_NO")
    private Integer runningNo;
    @Column(name = "COST_CENTER_CODE", length = 20)
    private String costCenterCode;
    @Column(name = "INVOICE_ITEM_DESC", length = 256)
    private String invoiceItemDesc;
    @Column(name = "INVOICE_START_DATE")
    @Temporal(TemporalType.DATE)
    private Date invoiceStartDate;
    @Column(name = "INVOICE_END_DATE")
    @Temporal(TemporalType.DATE)
    private Date invoiceEndDate;
    @Column(name = "INVOICE_ITEM_AMOUNT", precision = 18, scale = 2)
    private BigDecimal invoiceItemAmount;
    @Column(name = "INVOICE_ITEM_REF_1", length = 20)
    private String invoiceItemRef1;
    @Column(name = "INVOICE_ITEM_REF_2", length = 20)
    private String invoiceItemRef2;
    @Column(name = "INVOICE_ITEM_REF_3", length = 20)
    private String invoiceItemRef3;
    @Column(name = "INVOICE_CITIZEN_NO", length = 50)
    private String invoiceCitizenNo;
    @Column(name = "INVOICE_TITLE", length = 50)
    private String invoiceTitle;
    @Column(name = "INVOICE_FIRST_NAME", length = 256)
    private String invoiceFirstName;
    @Column(name = "INVOICE_MIDDLE_NAME", length = 256)
    private String invoiceMiddleName;
    @Column(name = "INVOICE_LAST_NAME", length = 256)
    private String invoiceLastName;
    @Column(name = "INVOICE_BUSINESS_NO_1", length = 50)
    private String invoiceBusinessNo1;
    @Column(name = "INVOICE_BUSINESS_NO_2", length = 50)
    private String invoiceBusinessNo2;
    @Column(name = "INVOICE_BUSINESS_NAME_1", length = 256)
    private String invoiceBusinessName1;
    @Column(name = "INVOICE_BUSINESS_NAME_2", length = 256)
    private String invoiceBusinessName2;
    @Column(name = "ADDRESS_NO", length = 50)
    private String addressNo;
    @Column(name = "ADDRESS_BUILDING_NAME", length = 256)
    private String addressBuildingName;
    @Column(name = "ADDRESS_MOO", length = 50)
    private String addressMoo;
    @Column(name = "ADDRESS_LANE", length = 256)
    private String addressLane;
    @Column(name = "ADDRESS_SOI", length = 256)
    private String addressSoi;
    @Column(name = "ADDRESS_ROAD", length = 256)
    private String addressRoad;
    @Column(name = "ADDRESS_AMPHUR", length = 256)
    private String addressAmphur;
    @Column(name = "ADDRESS_TAMBON", length = 256)
    private String addressTambon;
    @Column(name = "ADDRESS_PROVINCE", length = 256)
    private String addressProvince;
    @Column(name = "ADDRESS_POSTCODE", length = 50)
    private String addressPostcode;
    @Column(name = "USER_MOBILE", length = 50)
    private String userMobile;
    @Column(name = "CAN_SEND_MOBILE")
    private Character canSendMobile;
    @Column(name = "USER_EMAIL", length = 128)
    private String userEmail;
    @Column(name = "CAN_SEND_EMAIL")
    private Character canSendEmail;
    @Column(name = "COLUMN1", length = 5)
    private String column1;
    @Column(name = "GOV_OTHER_DETAIL1", length = 512)
    private String govOtherDetail1;
    @Column(name = "GOV_OTHER_DETAIL2", length = 512)
    private String govOtherDetail2;
    @Column(name = "INVOICE_CONDITION", length = 512)
    private String invoiceCondition;
    @Column(name = "GROUP_BY", length = 2)
    private String groupBy;
    @Column(name = "INVOICE_LOADER_CODE", length = 50)
    private String invoiceLoaderCode;
    @Column(name = "COMFIRM_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date comfirmDate;
    @Column(name = "CANCEL_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date cancelDate;
    
    public InvoiceLoader() {
    }

    public InvoiceLoader(Integer invoiceLoaderId) {
        this.invoiceLoaderId = invoiceLoaderId;
    }

    public InvoiceLoader(Integer invoiceLoaderId, String catalogId) {
        this.invoiceLoaderId = invoiceLoaderId;
        this.catalogId = catalogId;
    }

    public String getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(String catalogId) {
        this.catalogId = catalogId;
    }

    public String getCitizenNo() {
        return citizenNo;
    }

    public void setCitizenNo(String citizenNo) {
        this.citizenNo = citizenNo;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Integer getInvoiceLoaderId() {
        return invoiceLoaderId;
    }

    public void setInvoiceLoaderId(Integer invoiceLoaderId) {
        this.invoiceLoaderId = invoiceLoaderId;
    }

    public Date getLoadDate() {
        return loadDate;
    }

    public void setLoadDate(Date loadDate) {
        this.loadDate = loadDate;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getRunningNo() {
        return runningNo;
    }

    public void setRunningNo(Integer runningNo) {
        this.runningNo = runningNo;
    }

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public String getInvoiceItemDesc() {
        return invoiceItemDesc;
    }

    public void setInvoiceItemDesc(String invoiceItemDesc) {
        this.invoiceItemDesc = invoiceItemDesc;
    }

    public Date getInvoiceStartDate() {
        return invoiceStartDate;
    }

    public void setInvoiceStartDate(Date invoiceStartDate) {
        this.invoiceStartDate = invoiceStartDate;
    }

    public Date getInvoiceEndDate() {
        return invoiceEndDate;
    }

    public void setInvoiceEndDate(Date invoiceEndDate) {
        this.invoiceEndDate = invoiceEndDate;
    }

    public BigDecimal getInvoiceItemAmount() {
        return invoiceItemAmount;
    }

    public void setInvoiceItemAmount(BigDecimal invoiceItemAmount) {
        this.invoiceItemAmount = invoiceItemAmount;
    }

    public String getInvoiceItemRef1() {
        return invoiceItemRef1;
    }

    public void setInvoiceItemRef1(String invoiceItemRef1) {
        this.invoiceItemRef1 = invoiceItemRef1;
    }

    public String getInvoiceItemRef2() {
        return invoiceItemRef2;
    }

    public void setInvoiceItemRef2(String invoiceItemRef2) {
        this.invoiceItemRef2 = invoiceItemRef2;
    }

    public String getInvoiceItemRef3() {
        return invoiceItemRef3;
    }

    public void setInvoiceItemRef3(String invoiceItemRef3) {
        this.invoiceItemRef3 = invoiceItemRef3;
    }

    public String getInvoiceCitizenNo() {
        return invoiceCitizenNo;
    }

    public void setInvoiceCitizenNo(String invoiceCitizenNo) {
        this.invoiceCitizenNo = invoiceCitizenNo;
    }

    public String getInvoiceTitle() {
        return invoiceTitle;
    }

    public void setInvoiceTitle(String invoiceTitle) {
        this.invoiceTitle = invoiceTitle;
    }

    public String getInvoiceFirstName() {
        return invoiceFirstName;
    }

    public void setInvoiceFirstName(String invoiceFirstName) {
        this.invoiceFirstName = invoiceFirstName;
    }

    public String getInvoiceMiddleName() {
        return invoiceMiddleName;
    }

    public void setInvoiceMiddleName(String invoiceMiddleName) {
        this.invoiceMiddleName = invoiceMiddleName;
    }

    public String getInvoiceLastName() {
        return invoiceLastName;
    }

    public void setInvoiceLastName(String invoiceLastName) {
        this.invoiceLastName = invoiceLastName;
    }

    public String getInvoiceBusinessNo1() {
        return invoiceBusinessNo1;
    }

    public void setInvoiceBusinessNo1(String invoiceBusinessNo1) {
        this.invoiceBusinessNo1 = invoiceBusinessNo1;
    }

    public String getInvoiceBusinessNo2() {
        return invoiceBusinessNo2;
    }

    public void setInvoiceBusinessNo2(String invoiceBusinessNo2) {
        this.invoiceBusinessNo2 = invoiceBusinessNo2;
    }

    public String getInvoiceBusinessName1() {
        return invoiceBusinessName1;
    }

    public void setInvoiceBusinessName1(String invoiceBusinessName1) {
        this.invoiceBusinessName1 = invoiceBusinessName1;
    }

    public String getInvoiceBusinessName2() {
        return invoiceBusinessName2;
    }

    public void setInvoiceBusinessName2(String invoiceBusinessName2) {
        this.invoiceBusinessName2 = invoiceBusinessName2;
    }

    public String getAddressNo() {
        return addressNo;
    }

    public void setAddressNo(String addressNo) {
        this.addressNo = addressNo;
    }

    public String getAddressBuildingName() {
        return addressBuildingName;
    }

    public void setAddressBuildingName(String addressBuildingName) {
        this.addressBuildingName = addressBuildingName;
    }

    public String getAddressMoo() {
        return addressMoo;
    }

    public void setAddressMoo(String addressMoo) {
        this.addressMoo = addressMoo;
    }

    public String getAddressLane() {
        return addressLane;
    }

    public void setAddressLane(String addressLane) {
        this.addressLane = addressLane;
    }

    public String getAddressSoi() {
        return addressSoi;
    }

    public void setAddressSoi(String addressSoi) {
        this.addressSoi = addressSoi;
    }

    public String getAddressRoad() {
        return addressRoad;
    }

    public void setAddressRoad(String addressRoad) {
        this.addressRoad = addressRoad;
    }

    public String getAddressAmphur() {
        return addressAmphur;
    }

    public void setAddressAmphur(String addressAmphur) {
        this.addressAmphur = addressAmphur;
    }

    public String getAddressTambon() {
        return addressTambon;
    }

    public void setAddressTambon(String addressTambon) {
        this.addressTambon = addressTambon;
    }

    public String getAddressProvince() {
        return addressProvince;
    }

    public void setAddressProvince(String addressProvince) {
        this.addressProvince = addressProvince;
    }

    public String getAddressPostcode() {
        return addressPostcode;
    }

    public void setAddressPostcode(String addressPostcode) {
        this.addressPostcode = addressPostcode;
    }

    public String getUserMobile() {
        return userMobile;
    }

    public void setUserMobile(String userMobile) {
        this.userMobile = userMobile;
    }

    public Character getCanSendMobile() {
        return canSendMobile;
    }

    public void setCanSendMobile(Character canSendMobile) {
        this.canSendMobile = canSendMobile;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public Character getCanSendEmail() {
        return canSendEmail;
    }

    public void setCanSendEmail(Character canSendEmail) {
        this.canSendEmail = canSendEmail;
    }

    public String getColumn1() {
        return column1;
    }

    public void setColumn1(String column1) {
        this.column1 = column1;
    }

    public String getGovOtherDetail1() {
        return govOtherDetail1;
    }

    public void setGovOtherDetail1(String govOtherDetail1) {
        this.govOtherDetail1 = govOtherDetail1;
    }

    public String getGovOtherDetail2() {
        return govOtherDetail2;
    }

    public void setGovOtherDetail2(String govOtherDetail2) {
        this.govOtherDetail2 = govOtherDetail2;
    }

    public String getInvoiceCondition() {
        return invoiceCondition;
    }

    public void setInvoiceCondition(String invoiceCondition) {
        this.invoiceCondition = invoiceCondition;
    }

    public String getGroupBy() {
        return groupBy;
    }

    public void setGroupBy(String groupBy) {
        this.groupBy = groupBy;
    }
    
    public String getInvoiceLoaderCode() {
        return invoiceLoaderCode;
    }

    public void setInvoiceLoaderCode(String invoiceLoaderCode) {
        this.invoiceLoaderCode = invoiceLoaderCode;
    }

    public Date getComfirmDate() {
        return comfirmDate;
    }

    public void setComfirmDate(Date comfirmDate) {
        this.comfirmDate = comfirmDate;
    }

    public Date getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Date cancelDate) {
        this.cancelDate = cancelDate;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (invoiceLoaderId != null ? invoiceLoaderId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InvoiceLoader)) {
            return false;
        }
        InvoiceLoader other = (InvoiceLoader) object;
        if ((this.invoiceLoaderId == null && other.invoiceLoaderId != null) || (this.invoiceLoaderId != null && !this.invoiceLoaderId.equals(other.invoiceLoaderId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.InvoiceLoader[ invoiceLoaderId=" + invoiceLoaderId + " ]";
    }
    
}
