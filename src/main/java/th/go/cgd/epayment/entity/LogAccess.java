/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "LOG_ACCESS", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LogAccess.findAll", query = "SELECT l FROM LogAccess l"),
    @NamedQuery(name = "LogAccess.findById", query = "SELECT l FROM LogAccess l WHERE l.id = :id"),
    @NamedQuery(name = "LogAccess.findByAccessdate", query = "SELECT l FROM LogAccess l WHERE l.accessdate = :accessdate"),
    @NamedQuery(name = "LogAccess.findByClientip", query = "SELECT l FROM LogAccess l WHERE l.clientip = :clientip"),
    @NamedQuery(name = "LogAccess.findByClientname", query = "SELECT l FROM LogAccess l WHERE l.clientname = :clientname"),
    @NamedQuery(name = "LogAccess.findByExecutiontime", query = "SELECT l FROM LogAccess l WHERE l.executiontime = :executiontime"),
    @NamedQuery(name = "LogAccess.findByStatus", query = "SELECT l FROM LogAccess l WHERE l.status = :status"),
    @NamedQuery(name = "LogAccess.findByUrl", query = "SELECT l FROM LogAccess l WHERE l.url = :url"),
    @NamedQuery(name = "LogAccess.findByUsername", query = "SELECT l FROM LogAccess l WHERE l.username = :username"),
    @NamedQuery(name = "LogAccess.findByAccessDate", query = "SELECT l FROM LogAccess l WHERE l.accessDate = :accessDate"),
    @NamedQuery(name = "LogAccess.findByClientIp", query = "SELECT l FROM LogAccess l WHERE l.clientIp = :clientIp"),
    @NamedQuery(name = "LogAccess.findByClientName", query = "SELECT l FROM LogAccess l WHERE l.clientName = :clientName"),
    @NamedQuery(name = "LogAccess.findByExecutionTime", query = "SELECT l FROM LogAccess l WHERE l.executionTime = :executionTime")})
public class LogAccess implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;
    @Column(name = "ACCESSDATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date accessdate;
    @Column(name = "CLIENTIP", length = 255)
    private String clientip;
    @Column(name = "CLIENTNAME", length = 255)
    private String clientname;
//    @Basic(optional = false)
    @Column(name = "EXECUTIONTIME")
    private int executiontime;
    @Column(name = "STATUS", length = 255)
    private String status;
    @Column(name = "URL", length = 255)
    private String url;
    @Column(name = "USERNAME", length = 255)
    private String username;
    @Lob
    @Column(name = "MLOG_ACTION")
    private byte[] mlogAction;
    @Column(name = "ACCESS_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date accessDate;
    @Column(name = "CLIENT_IP", length = 255)
    private String clientIp;
    @Column(name = "CLIENT_NAME", length = 255)
    private String clientName;
    @Column(name = "EXECUTION_TIME")
    private Short executionTime;
    @JoinColumn(name = "MLOGACTION", referencedColumnName = "ACTION_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MLogAction mlogaction;

    public LogAccess() {
    }

    public LogAccess(Long id) {
        this.id = id;
    }

    public LogAccess(Long id, int executiontime) {
        this.id = id;
        this.executiontime = executiontime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getAccessdate() {
        return accessdate;
    }

    public void setAccessdate(Date accessdate) {
        this.accessdate = accessdate;
    }

    public String getClientip() {
        return clientip;
    }

    public void setClientip(String clientip) {
        this.clientip = clientip;
    }

    public String getClientname() {
        return clientname;
    }

    public void setClientname(String clientname) {
        this.clientname = clientname;
    }

    public int getExecutiontime() {
        return executiontime;
    }

    public void setExecutiontime(int executiontime) {
        this.executiontime = executiontime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public byte[] getMlogAction() {
        return mlogAction;
    }

    public void setMlogAction(byte[] mlogAction) {
        this.mlogAction = mlogAction;
    }

    public Date getAccessDate() {
        return accessDate;
    }

    public void setAccessDate(Date accessDate) {
        this.accessDate = accessDate;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public Short getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Short executionTime) {
        this.executionTime = executionTime;
    }

    public MLogAction getMlogaction() {
        return mlogaction;
    }

    public void setMlogaction(MLogAction mlogaction) {
        this.mlogaction = mlogaction;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LogAccess)) {
            return false;
        }
        LogAccess other = (LogAccess) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.LogAccess[ id=" + id + " ]";
    }
    
}
