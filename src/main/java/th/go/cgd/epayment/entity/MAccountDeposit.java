/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_ACCOUNT_DEPOSIT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MAccountDeposit.findAll", query = "SELECT m FROM MAccountDeposit m"),
    @NamedQuery(name = "MAccountDeposit.findByAccountDepositId", query = "SELECT m FROM MAccountDeposit m WHERE m.accountDepositId = :accountDepositId"),
    @NamedQuery(name = "MAccountDeposit.findByAccountCode", query = "SELECT m FROM MAccountDeposit m WHERE m.accountCode = :accountCode"),
    @NamedQuery(name = "MAccountDeposit.findByAccountName", query = "SELECT m FROM MAccountDeposit m WHERE m.accountName = :accountName"),
    @NamedQuery(name = "MAccountDeposit.findByCreatedBy", query = "SELECT m FROM MAccountDeposit m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MAccountDeposit.findByCreatedDate", query = "SELECT m FROM MAccountDeposit m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MAccountDeposit.findByEndDate", query = "SELECT m FROM MAccountDeposit m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MAccountDeposit.findByOwner", query = "SELECT m FROM MAccountDeposit m WHERE m.owner = :owner"),
    @NamedQuery(name = "MAccountDeposit.findByRemark", query = "SELECT m FROM MAccountDeposit m WHERE m.remark = :remark"),
    @NamedQuery(name = "MAccountDeposit.findByStartDate", query = "SELECT m FROM MAccountDeposit m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MAccountDeposit.findByStatus", query = "SELECT m FROM MAccountDeposit m WHERE m.status = :status"),
    @NamedQuery(name = "MAccountDeposit.findByUpdatedBy", query = "SELECT m FROM MAccountDeposit m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MAccountDeposit.findByUpdatedDate", query = "SELECT m FROM MAccountDeposit m WHERE m.updatedDate = :updatedDate")})
public class MAccountDeposit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ACCOUNT_DEPOSIT_ID", nullable = false)
    private Integer accountDepositId;
    @Basic(optional = false)
    @Column(name = "ACCOUNT_CODE", nullable = false, length = 6)
    private String accountCode;
    @Basic(optional = false)
    @Column(name = "ACCOUNT_NAME", nullable = false, length = 255)
    private String accountName;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "OWNER", nullable = false, length = 255)
    private String owner;
    @Column(name = "REMARK", length = 255)
    private String remark;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accountDepositId", fetch = FetchType.LAZY)
    private List<CatalogStructureItem> catalogStructureItemList;

    public MAccountDeposit() {
    }

    public MAccountDeposit(Integer accountDepositId) {
        this.accountDepositId = accountDepositId;
    }

    public MAccountDeposit(Integer accountDepositId, String accountCode, String accountName, String owner, Date startDate) {
        this.accountDepositId = accountDepositId;
        this.accountCode = accountCode;
        this.accountName = accountName;
        this.owner = owner;
        this.startDate = startDate;
    }

    public Integer getAccountDepositId() {
        return accountDepositId;
    }

    public void setAccountDepositId(Integer accountDepositId) {
        this.accountDepositId = accountDepositId;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<CatalogStructureItem> getCatalogStructureItemList() {
        return catalogStructureItemList;
    }

    public void setCatalogStructureItemList(List<CatalogStructureItem> catalogStructureItemList) {
        this.catalogStructureItemList = catalogStructureItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accountDepositId != null ? accountDepositId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MAccountDeposit)) {
            return false;
        }
        MAccountDeposit other = (MAccountDeposit) object;
        if ((this.accountDepositId == null && other.accountDepositId != null) || (this.accountDepositId != null && !this.accountDepositId.equals(other.accountDepositId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MAccountDeposit[ accountDepositId=" + accountDepositId + " ]";
    }
    
}
