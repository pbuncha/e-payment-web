/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_AMPHUR", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MAmphur.findAll", query = "SELECT m FROM MAmphur m"),
    @NamedQuery(name = "MAmphur.findByAmphurId", query = "SELECT m FROM MAmphur m WHERE m.amphurId = :amphurId"),
    @NamedQuery(name = "MAmphur.findByAmphurNameEn", query = "SELECT m FROM MAmphur m WHERE m.amphurNameEn = :amphurNameEn"),
    @NamedQuery(name = "MAmphur.findByAmphurNameTh", query = "SELECT m FROM MAmphur m WHERE m.amphurNameTh = :amphurNameTh"),
    @NamedQuery(name = "MAmphur.findByCreatedBy", query = "SELECT m FROM MAmphur m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MAmphur.findByCreatedDate", query = "SELECT m FROM MAmphur m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MAmphur.findByUpdatedBy", query = "SELECT m FROM MAmphur m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MAmphur.findByUpdatedDate", query = "SELECT m FROM MAmphur m WHERE m.updatedDate = :updatedDate")})
public class MAmphur implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "AMPHUR_ID", nullable = false)
    private Integer amphurId;
    @Column(name = "AMPHUR_NAME_EN", length = 255)
    private String amphurNameEn;
    @Basic(optional = false)
    @Column(name = "AMPHUR_NAME_TH", nullable = false, length = 255)
    private String amphurNameTh;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false)
    private int createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provinceId", fetch = FetchType.LAZY)
    private List<MAmphur> mAmphurList;
    @JoinColumn(name = "PROVINCE_ID", referencedColumnName = "PROVINCE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MProvince provinceId;
    @OneToMany(mappedBy = "amphurId", fetch = FetchType.LAZY)
    private List<Address> addressList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "amphurId", fetch = FetchType.LAZY)
    private List<MTambon> mTambonList;

    public MAmphur() {
    }

    public MAmphur(Integer amphurId) {
        this.amphurId = amphurId;
    }

    public MAmphur(Integer amphurId, String amphurNameTh, int createdBy, Date createdDate) {
        this.amphurId = amphurId;
        this.amphurNameTh = amphurNameTh;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }

    public Integer getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(Integer amphurId) {
        this.amphurId = amphurId;
    }

    public String getAmphurNameEn() {
        return amphurNameEn;
    }

    public void setAmphurNameEn(String amphurNameEn) {
        this.amphurNameEn = amphurNameEn;
    }

    public String getAmphurNameTh() {
        return amphurNameTh;
    }

    public void setAmphurNameTh(String amphurNameTh) {
        this.amphurNameTh = amphurNameTh;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<MAmphur> getMAmphurList() {
        return mAmphurList;
    }

    public void setMAmphurList(List<MAmphur> mAmphurList) {
        this.mAmphurList = mAmphurList;
    }

    public MProvince getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(MProvince provinceId) {
        this.provinceId = provinceId;
    }

    @XmlTransient
    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    @XmlTransient
    public List<MTambon> getMTambonList() {
        return mTambonList;
    }

    public void setMTambonList(List<MTambon> mTambonList) {
        this.mTambonList = mTambonList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (amphurId != null ? amphurId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MAmphur)) {
            return false;
        }
        MAmphur other = (MAmphur) object;
        if ((this.amphurId == null && other.amphurId != null) || (this.amphurId != null && !this.amphurId.equals(other.amphurId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MAmphur[ amphurId=" + amphurId + " ]";
    }
    
}
