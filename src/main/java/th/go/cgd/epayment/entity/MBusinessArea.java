/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_BUSINESS_AREA", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MBusinessArea.findAll", query = "SELECT m FROM MBusinessArea m"),
    @NamedQuery(name = "MBusinessArea.findByBusinessAreaId", query = "SELECT m FROM MBusinessArea m WHERE m.businessAreaId = :businessAreaId"),
    @NamedQuery(name = "MBusinessArea.findByBusinessAreaCode", query = "SELECT m FROM MBusinessArea m WHERE m.businessAreaCode = :businessAreaCode"),
    @NamedQuery(name = "MBusinessArea.findByBusinessAreaName", query = "SELECT m FROM MBusinessArea m WHERE m.businessAreaName = :businessAreaName"),
    @NamedQuery(name = "MBusinessArea.findByStartDate", query = "SELECT m FROM MBusinessArea m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MBusinessArea.findByStatus", query = "SELECT m FROM MBusinessArea m WHERE m.status = :status"),
    @NamedQuery(name = "MBusinessArea.findByEndDate", query = "SELECT m FROM MBusinessArea m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MBusinessArea.findByCreatedDate", query = "SELECT m FROM MBusinessArea m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MBusinessArea.findByUpdatedDate", query = "SELECT m FROM MBusinessArea m WHERE m.updatedDate = :updatedDate")})
public class MBusinessArea implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "BUSINESS_AREA_ID", nullable = false)
    private Integer businessAreaId;
    @Basic(optional = false)
    @Column(name = "BUSINESS_AREA_CODE", nullable = false, length = 10)
    private String businessAreaCode;
    @Basic(optional = false)
    @Column(name = "BUSINESS_AREA_NAME", nullable = false, length = 255)
    private String businessAreaName;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "businessAreaId", fetch = FetchType.LAZY)
    private List<MCostCenter> mCostCenterList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "PROVINCE_ID", referencedColumnName = "PROVINCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MProvince provinceId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "businessAreaId", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList;

    public MBusinessArea() {
    }

    public MBusinessArea(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    public MBusinessArea(Integer businessAreaId, String businessAreaCode, String businessAreaName, Date startDate) {
        this.businessAreaId = businessAreaId;
        this.businessAreaCode = businessAreaCode;
        this.businessAreaName = businessAreaName;
        this.startDate = startDate;
    }

    public Integer getBusinessAreaId() {
        return businessAreaId;
    }

    public void setBusinessAreaId(Integer businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    public String getBusinessAreaCode() {
        return businessAreaCode;
    }

    public void setBusinessAreaCode(String businessAreaCode) {
        this.businessAreaCode = businessAreaCode;
    }

    public String getBusinessAreaName() {
        return businessAreaName;
    }

    public void setBusinessAreaName(String businessAreaName) {
        this.businessAreaName = businessAreaName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<MCostCenter> getMCostCenterList() {
        return mCostCenterList;
    }

    public void setMCostCenterList(List<MCostCenter> mCostCenterList) {
        this.mCostCenterList = mCostCenterList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MProvince getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(MProvince provinceId) {
        this.provinceId = provinceId;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList() {
        return userOrganizationList;
    }

    public void setUserOrganizationList(List<UserOrganization> userOrganizationList) {
        this.userOrganizationList = userOrganizationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (businessAreaId != null ? businessAreaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MBusinessArea)) {
            return false;
        }
        MBusinessArea other = (MBusinessArea) object;
        if ((this.businessAreaId == null && other.businessAreaId != null) || (this.businessAreaId != null && !this.businessAreaId.equals(other.businessAreaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MBusinessArea[ businessAreaId=" + businessAreaId + " ]";
    }
    
}
