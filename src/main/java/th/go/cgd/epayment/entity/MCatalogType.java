/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_CATALOG_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MCatalogType.findAll", query = "SELECT m FROM MCatalogType m"),
    @NamedQuery(name = "MCatalogType.findByCatalogTypeId", query = "SELECT m FROM MCatalogType m WHERE m.catalogTypeId = :catalogTypeId"),
    @NamedQuery(name = "MCatalogType.findByCatalogTypeName", query = "SELECT m FROM MCatalogType m WHERE m.catalogTypeName = :catalogTypeName"),
    @NamedQuery(name = "MCatalogType.findByCatalogTypeNameEn", query = "SELECT m FROM MCatalogType m WHERE m.catalogTypeNameEn = :catalogTypeNameEn"),
    @NamedQuery(name = "MCatalogType.findByCreatedBy", query = "SELECT m FROM MCatalogType m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MCatalogType.findByCreatedDate", query = "SELECT m FROM MCatalogType m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MCatalogType.findByEndDate", query = "SELECT m FROM MCatalogType m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MCatalogType.findByStartDate", query = "SELECT m FROM MCatalogType m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MCatalogType.findByStatus", query = "SELECT m FROM MCatalogType m WHERE m.status = :status"),
    @NamedQuery(name = "MCatalogType.findByUpdatedBy", query = "SELECT m FROM MCatalogType m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MCatalogType.findByUpdatedDate", query = "SELECT m FROM MCatalogType m WHERE m.updatedDate = :updatedDate")})
public class MCatalogType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CATALOG_TYPE_ID", nullable = false)
    private Integer catalogTypeId;
    @Basic(optional = false)
    @Column(name = "CATALOG_TYPE_NAME", nullable = false, length = 255)
    private String catalogTypeName;
    @Basic(optional = false)
    @Column(name = "CATALOG_TYPE_NAME_EN", nullable = false, length = 255)
    private String catalogTypeNameEn;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "catalogTypeId", fetch = FetchType.LAZY)
    private List<Catalog> catalogList;

    public MCatalogType() {
    }

    public MCatalogType(Integer catalogTypeId) {
        this.catalogTypeId = catalogTypeId;
    }

    public MCatalogType(Integer catalogTypeId, String catalogTypeName, String catalogTypeNameEn, Date startDate) {
        this.catalogTypeId = catalogTypeId;
        this.catalogTypeName = catalogTypeName;
        this.catalogTypeNameEn = catalogTypeNameEn;
        this.startDate = startDate;
    }

    public Integer getCatalogTypeId() {
        return catalogTypeId;
    }

    public void setCatalogTypeId(Integer catalogTypeId) {
        this.catalogTypeId = catalogTypeId;
    }

    public String getCatalogTypeName() {
        return catalogTypeName;
    }

    public void setCatalogTypeName(String catalogTypeName) {
        this.catalogTypeName = catalogTypeName;
    }

    public String getCatalogTypeNameEn() {
        return catalogTypeNameEn;
    }

    public void setCatalogTypeNameEn(String catalogTypeNameEn) {
        this.catalogTypeNameEn = catalogTypeNameEn;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<Catalog> getCatalogList() {
        return catalogList;
    }

    public void setCatalogList(List<Catalog> catalogList) {
        this.catalogList = catalogList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (catalogTypeId != null ? catalogTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MCatalogType)) {
            return false;
        }
        MCatalogType other = (MCatalogType) object;
        if ((this.catalogTypeId == null && other.catalogTypeId != null) || (this.catalogTypeId != null && !this.catalogTypeId.equals(other.catalogTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MCatalogType[ catalogTypeId=" + catalogTypeId + " ]";
    }
    
}
