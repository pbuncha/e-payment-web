/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_COST_CENTER", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MCostCenter.findAll", query = "SELECT m FROM MCostCenter m"),
    @NamedQuery(name = "MCostCenter.findByCostCenterId", query = "SELECT m FROM MCostCenter m WHERE m.costCenterId = :costCenterId"),
    @NamedQuery(name = "MCostCenter.findByCostCenterCode", query = "SELECT m FROM MCostCenter m WHERE m.costCenterCode = :costCenterCode"),
    @NamedQuery(name = "MCostCenter.findByCostCenterName", query = "SELECT m FROM MCostCenter m WHERE m.costCenterName = :costCenterName"),
    @NamedQuery(name = "MCostCenter.findByStartDate", query = "SELECT m FROM MCostCenter m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MCostCenter.findByEndDate", query = "SELECT m FROM MCostCenter m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MCostCenter.findByStatus", query = "SELECT m FROM MCostCenter m WHERE m.status = :status"),
    @NamedQuery(name = "MCostCenter.findByCreatedDate", query = "SELECT m FROM MCostCenter m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MCostCenter.findByUpdatedDate", query = "SELECT m FROM MCostCenter m WHERE m.updatedDate = :updatedDate")})
public class MCostCenter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "COST_CENTER_ID", nullable = false)
    private Integer costCenterId;
    @Basic(optional = false)
    @Column(name = "COST_CENTER_CODE", nullable = false, length = 10)
    private String costCenterCode;
    @Basic(optional = false)
    @Column(name = "COST_CENTER_NAME", nullable = false, length = 255)
    private String costCenterName;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "DISBURSEMENT_UNIT_ID", referencedColumnName = "DISBURSEMENT_UNIT_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MDisbursementUnit disbursementUnitId;
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "DEPARTMENT_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MDepartment departmentId;
    @JoinColumn(name = "BUSINESS_AREA_ID", referencedColumnName = "BUSINESS_AREA_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MBusinessArea businessAreaId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "costCenterId", fetch = FetchType.LAZY)
    private List<GfmisGenItem> gfmisGenItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "costCenterId", fetch = FetchType.LAZY)
    private List<CatalogStructureItem> catalogStructureItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "costCenterId", fetch = FetchType.LAZY)
    private List<CatalogCostCenter> catalogCostCenterList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "costCenterId", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList;

    public MCostCenter() {
    }

    public MCostCenter(Integer costCenterId) {
        this.costCenterId = costCenterId;
    }

    public MCostCenter(Integer costCenterId, String costCenterCode, String costCenterName, Date startDate) {
        this.costCenterId = costCenterId;
        this.costCenterCode = costCenterCode;
        this.costCenterName = costCenterName;
        this.startDate = startDate;
    }

    public Integer getCostCenterId() {
        return costCenterId;
    }

    public void setCostCenterId(Integer costCenterId) {
        this.costCenterId = costCenterId;
    }

    public String getCostCenterCode() {
        return costCenterCode;
    }

    public void setCostCenterCode(String costCenterCode) {
        this.costCenterCode = costCenterCode;
    }

    public String getCostCenterName() {
        return costCenterName;
    }

    public void setCostCenterName(String costCenterName) {
        this.costCenterName = costCenterName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MDisbursementUnit getDisbursementUnitId() {
        return disbursementUnitId;
    }

    public void setDisbursementUnitId(MDisbursementUnit disbursementUnitId) {
        this.disbursementUnitId = disbursementUnitId;
    }

    public MDepartment getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(MDepartment departmentId) {
        this.departmentId = departmentId;
    }

    public MBusinessArea getBusinessAreaId() {
        return businessAreaId;
    }

    public void setBusinessAreaId(MBusinessArea businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    @XmlTransient
    public List<GfmisGenItem> getGfmisGenItemList() {
        return gfmisGenItemList;
    }

    public void setGfmisGenItemList(List<GfmisGenItem> gfmisGenItemList) {
        this.gfmisGenItemList = gfmisGenItemList;
    }

    @XmlTransient
    public List<CatalogStructureItem> getCatalogStructureItemList() {
        return catalogStructureItemList;
    }

    public void setCatalogStructureItemList(List<CatalogStructureItem> catalogStructureItemList) {
        this.catalogStructureItemList = catalogStructureItemList;
    }

    @XmlTransient
    public List<CatalogCostCenter> getCatalogCostCenterList() {
        return catalogCostCenterList;
    }

    public void setCatalogCostCenterList(List<CatalogCostCenter> catalogCostCenterList) {
        this.catalogCostCenterList = catalogCostCenterList;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList() {
        return userOrganizationList;
    }

    public void setUserOrganizationList(List<UserOrganization> userOrganizationList) {
        this.userOrganizationList = userOrganizationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (costCenterId != null ? costCenterId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MCostCenter)) {
            return false;
        }
        MCostCenter other = (MCostCenter) object;
        if ((this.costCenterId == null && other.costCenterId != null) || (this.costCenterId != null && !this.costCenterId.equals(other.costCenterId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MCostCenter[ costCenterId=" + costCenterId + " ]";
    }
    
}
