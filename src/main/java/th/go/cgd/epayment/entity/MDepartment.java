/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_DEPARTMENT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MDepartment.findAll", query = "SELECT m FROM MDepartment m"),
    @NamedQuery(name = "MDepartment.findByDepartmentId", query = "SELECT m FROM MDepartment m WHERE m.departmentId = :departmentId"),
    @NamedQuery(name = "MDepartment.findByCreatedDate", query = "SELECT m FROM MDepartment m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MDepartment.findByDepartmentCode", query = "SELECT m FROM MDepartment m WHERE m.departmentCode = :departmentCode"),
    @NamedQuery(name = "MDepartment.findByDepartmentName", query = "SELECT m FROM MDepartment m WHERE m.departmentName = :departmentName"),
    @NamedQuery(name = "MDepartment.findByEndDate", query = "SELECT m FROM MDepartment m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MDepartment.findByStartDate", query = "SELECT m FROM MDepartment m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MDepartment.findByStatus", query = "SELECT m FROM MDepartment m WHERE m.status = :status"),
    @NamedQuery(name = "MDepartment.findByUpdatedDate", query = "SELECT m FROM MDepartment m WHERE m.updatedDate = :updatedDate"),
    @NamedQuery(name = "MDepartment.findByParentDepartmentId", query = "SELECT m FROM MDepartment m WHERE m.parentDepartmentId = :parentDepartmentId"),
    @NamedQuery(name = "MDepartment.findByPostDetail", query = "SELECT m FROM MDepartment m WHERE m.postDetail = :postDetail"),
    @NamedQuery(name = "MDepartment.findByDepartmentNameEn", query = "SELECT m FROM MDepartment m WHERE m.departmentNameEn = :departmentNameEn")})
public class MDepartment implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "DEPARTMENT_ID", nullable = false)
    private Integer departmentId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "DEPARTMENT_CODE", length = 20)
    private String departmentCode;
    @Basic(optional = false)
    @Column(name = "DEPARTMENT_NAME", nullable = false, length = 200)
    private String departmentName;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "PARENT_DEPARTMENT_ID")
    private Integer parentDepartmentId;
    @Column(name = "POST_DETAIL", length = 256)
    private String postDetail;
    @Column(name = "DEPARTMENT_NAME_EN", length = 256)
    private String departmentNameEn;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "departmentId", fetch = FetchType.LAZY)
    private List<MCostCenter> mCostCenterList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "departmentId", fetch = FetchType.LAZY)
    private List<CatalogStructureItem> catalogStructureItemList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "ORGANIZATION_SUB_TYPE_ID", referencedColumnName = "ORGANIZATION_SUB_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MOrganizationSubType organizationSubTypeId;
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "ADDRESS_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Address addressId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "departmentId", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList;

    public MDepartment() {
    }

    public MDepartment(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public MDepartment(Integer departmentId, String departmentName, Date startDate) {
        this.departmentId = departmentId;
        this.departmentName = departmentName;
        this.startDate = startDate;
    }

    public Integer getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getParentDepartmentId() {
        return parentDepartmentId;
    }

    public void setParentDepartmentId(Integer parentDepartmentId) {
        this.parentDepartmentId = parentDepartmentId;
    }

    public String getPostDetail() {
        return postDetail;
    }

    public void setPostDetail(String postDetail) {
        this.postDetail = postDetail;
    }

    public String getDepartmentNameEn() {
        return departmentNameEn;
    }

    public void setDepartmentNameEn(String departmentNameEn) {
        this.departmentNameEn = departmentNameEn;
    }

    @XmlTransient
    public List<MCostCenter> getMCostCenterList() {
        return mCostCenterList;
    }

    public void setMCostCenterList(List<MCostCenter> mCostCenterList) {
        this.mCostCenterList = mCostCenterList;
    }

    @XmlTransient
    public List<CatalogStructureItem> getCatalogStructureItemList() {
        return catalogStructureItemList;
    }

    public void setCatalogStructureItemList(List<CatalogStructureItem> catalogStructureItemList) {
        this.catalogStructureItemList = catalogStructureItemList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MOrganizationSubType getOrganizationSubTypeId() {
        return organizationSubTypeId;
    }

    public void setOrganizationSubTypeId(MOrganizationSubType organizationSubTypeId) {
        this.organizationSubTypeId = organizationSubTypeId;
    }

    public Address getAddressId() {
        return addressId;
    }

    public void setAddressId(Address addressId) {
        this.addressId = addressId;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList() {
        return userOrganizationList;
    }

    public void setUserOrganizationList(List<UserOrganization> userOrganizationList) {
        this.userOrganizationList = userOrganizationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (departmentId != null ? departmentId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MDepartment)) {
            return false;
        }
        MDepartment other = (MDepartment) object;
        if ((this.departmentId == null && other.departmentId != null) || (this.departmentId != null && !this.departmentId.equals(other.departmentId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MDepartment[ departmentId=" + departmentId + " ]";
    }
    
}
