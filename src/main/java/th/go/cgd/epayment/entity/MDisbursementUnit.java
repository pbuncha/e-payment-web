/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_DISBURSEMENT_UNIT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MDisbursementUnit.findAll", query = "SELECT m FROM MDisbursementUnit m"),
    @NamedQuery(name = "MDisbursementUnit.findByDisbursementUnitId", query = "SELECT m FROM MDisbursementUnit m WHERE m.disbursementUnitId = :disbursementUnitId"),
    @NamedQuery(name = "MDisbursementUnit.findByDisbursementUnitCode", query = "SELECT m FROM MDisbursementUnit m WHERE m.disbursementUnitCode = :disbursementUnitCode"),
    @NamedQuery(name = "MDisbursementUnit.findByDisbursementUnitDesc", query = "SELECT m FROM MDisbursementUnit m WHERE m.disbursementUnitDesc = :disbursementUnitDesc"),
    @NamedQuery(name = "MDisbursementUnit.findByDisbursementUnitLongDesc", query = "SELECT m FROM MDisbursementUnit m WHERE m.disbursementUnitLongDesc = :disbursementUnitLongDesc"),
    @NamedQuery(name = "MDisbursementUnit.findByStartDate", query = "SELECT m FROM MDisbursementUnit m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MDisbursementUnit.findByEndDate", query = "SELECT m FROM MDisbursementUnit m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MDisbursementUnit.findByStatus", query = "SELECT m FROM MDisbursementUnit m WHERE m.status = :status"),
    @NamedQuery(name = "MDisbursementUnit.findByCreatedDate", query = "SELECT m FROM MDisbursementUnit m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MDisbursementUnit.findByUpdatedDate", query = "SELECT m FROM MDisbursementUnit m WHERE m.updatedDate = :updatedDate")})
public class MDisbursementUnit implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "DISBURSEMENT_UNIT_ID", nullable = false)
    private Integer disbursementUnitId;
    @Basic(optional = false)
    @Column(name = "DISBURSEMENT_UNIT_CODE", nullable = false, length = 10)
    private String disbursementUnitCode;
    @Basic(optional = false)
    @Column(name = "DISBURSEMENT_UNIT_DESC", nullable = false, length = 255)
    private String disbursementUnitDesc;
    @Basic(optional = false)
    @Column(name = "DISBURSEMENT_UNIT_LONG_DESC", nullable = false, length = 512)
    private String disbursementUnitLongDesc;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "disbursementUnitId", fetch = FetchType.LAZY)
    private List<MCostCenter> mCostCenterList;

    public MDisbursementUnit() {
    }

    public MDisbursementUnit(Integer disbursementUnitId) {
        this.disbursementUnitId = disbursementUnitId;
    }

    public MDisbursementUnit(Integer disbursementUnitId, String disbursementUnitCode, String disbursementUnitDesc, String disbursementUnitLongDesc, Date startDate) {
        this.disbursementUnitId = disbursementUnitId;
        this.disbursementUnitCode = disbursementUnitCode;
        this.disbursementUnitDesc = disbursementUnitDesc;
        this.disbursementUnitLongDesc = disbursementUnitLongDesc;
        this.startDate = startDate;
    }

    public Integer getDisbursementUnitId() {
        return disbursementUnitId;
    }

    public void setDisbursementUnitId(Integer disbursementUnitId) {
        this.disbursementUnitId = disbursementUnitId;
    }

    public String getDisbursementUnitCode() {
        return disbursementUnitCode;
    }

    public void setDisbursementUnitCode(String disbursementUnitCode) {
        this.disbursementUnitCode = disbursementUnitCode;
    }

    public String getDisbursementUnitDesc() {
        return disbursementUnitDesc;
    }

    public void setDisbursementUnitDesc(String disbursementUnitDesc) {
        this.disbursementUnitDesc = disbursementUnitDesc;
    }

    public String getDisbursementUnitLongDesc() {
        return disbursementUnitLongDesc;
    }

    public void setDisbursementUnitLongDesc(String disbursementUnitLongDesc) {
        this.disbursementUnitLongDesc = disbursementUnitLongDesc;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @XmlTransient
    public List<MCostCenter> getMCostCenterList() {
        return mCostCenterList;
    }

    public void setMCostCenterList(List<MCostCenter> mCostCenterList) {
        this.mCostCenterList = mCostCenterList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (disbursementUnitId != null ? disbursementUnitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MDisbursementUnit)) {
            return false;
        }
        MDisbursementUnit other = (MDisbursementUnit) object;
        if ((this.disbursementUnitId == null && other.disbursementUnitId != null) || (this.disbursementUnitId != null && !this.disbursementUnitId.equals(other.disbursementUnitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MDisbursementUnit[ disbursementUnitId=" + disbursementUnitId + " ]";
    }
    
}
