/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_DISTRICT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MDistrict.findAll", query = "SELECT m FROM MDistrict m"),
    @NamedQuery(name = "MDistrict.findByDistrictId", query = "SELECT m FROM MDistrict m WHERE m.districtId = :districtId"),
    @NamedQuery(name = "MDistrict.findByDistrictName", query = "SELECT m FROM MDistrict m WHERE m.districtName = :districtName")})
public class MDistrict implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "DISTRICT_ID", nullable = false)
    private Short districtId;
    @Basic(optional = false)
    @Column(name = "DISTRICT_NAME", nullable = false, length = 200)
    private String districtName;
    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MStatus status;
    @JoinColumn(name = "PROVINCE_ID", referencedColumnName = "PROVINCE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MProvince provinceId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "districtId", fetch = FetchType.LAZY)
    private List<MSubdistrict> mSubdistrictList;

    public MDistrict() {
    }

    public MDistrict(Short districtId) {
        this.districtId = districtId;
    }

    public MDistrict(Short districtId, String districtName) {
        this.districtId = districtId;
        this.districtName = districtName;
    }

    public Short getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Short districtId) {
        this.districtId = districtId;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public MStatus getStatus() {
        return status;
    }

    public void setStatus(MStatus status) {
        this.status = status;
    }

    public MProvince getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(MProvince provinceId) {
        this.provinceId = provinceId;
    }

    @XmlTransient
    public List<MSubdistrict> getMSubdistrictList() {
        return mSubdistrictList;
    }

    public void setMSubdistrictList(List<MSubdistrict> mSubdistrictList) {
        this.mSubdistrictList = mSubdistrictList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (districtId != null ? districtId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MDistrict)) {
            return false;
        }
        MDistrict other = (MDistrict) object;
        if ((this.districtId == null && other.districtId != null) || (this.districtId != null && !this.districtId.equals(other.districtId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MDistrict[ districtId=" + districtId + " ]";
    }
    
}
