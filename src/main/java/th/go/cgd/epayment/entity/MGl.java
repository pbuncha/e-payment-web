/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_GL", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MGl.findAll", query = "SELECT m FROM MGl m"),
    @NamedQuery(name = "MGl.findByGlId", query = "SELECT m FROM MGl m WHERE m.glId = :glId"),
    @NamedQuery(name = "MGl.findByCreatedDate", query = "SELECT m FROM MGl m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MGl.findByEndDate", query = "SELECT m FROM MGl m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MGl.findByGlCode", query = "SELECT m FROM MGl m WHERE m.glCode = :glCode"),
    @NamedQuery(name = "MGl.findByGlName", query = "SELECT m FROM MGl m WHERE m.glName = :glName"),
    @NamedQuery(name = "MGl.findByStandardRevernueId", query = "SELECT m FROM MGl m WHERE m.standardRevernueId = :standardRevernueId"),
    @NamedQuery(name = "MGl.findByStartDate", query = "SELECT m FROM MGl m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MGl.findByStatus", query = "SELECT m FROM MGl m WHERE m.status = :status"),
    @NamedQuery(name = "MGl.findByUpdatedDate", query = "SELECT m FROM MGl m WHERE m.updatedDate = :updatedDate")})
public class MGl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "GL_ID", nullable = false)
    private Integer glId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "GL_CODE", nullable = false, length = 32)
    private String glCode;
    @Basic(optional = false)
    @Column(name = "GL_NAME", nullable = false, length = 255)
    private String glName;
    @Column(name = "STANDARD_REVERNUE_ID")
    private Integer standardRevernueId;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(mappedBy = "glFinishId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<MStandardRevernueGl> mStandardRevernueGlList;
    @OneToMany(mappedBy = "glStartId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<MStandardRevernueGl> mStandardRevernueGlList1;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;

    public MGl() {
    }

    public MGl(Integer glId) {
        this.glId = glId;
    }

    public MGl(Integer glId, String glCode, String glName, Date startDate) {
        this.glId = glId;
        this.glCode = glCode;
        this.glName = glName;
        this.startDate = startDate;
    }

    public Integer getGlId() {
        return glId;
    }

    public void setGlId(Integer glId) {
        this.glId = glId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getGlCode() {
        return glCode;
    }

    public void setGlCode(String glCode) {
        this.glCode = glCode;
    }

    public String getGlName() {
        return glName;
    }

    public void setGlName(String glName) {
        this.glName = glName;
    }

    public Integer getStandardRevernueId() {
        return standardRevernueId;
    }

    public void setStandardRevernueId(Integer standardRevernueId) {
        this.standardRevernueId = standardRevernueId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<MStandardRevernueGl> getMStandardRevernueGlList() {
        return mStandardRevernueGlList;
    }

    public void setMStandardRevernueGlList(List<MStandardRevernueGl> mStandardRevernueGlList) {
        this.mStandardRevernueGlList = mStandardRevernueGlList;
    }

    @XmlTransient
    public List<MStandardRevernueGl> getMStandardRevernueGlList1() {
        return mStandardRevernueGlList1;
    }

    public void setMStandardRevernueGlList1(List<MStandardRevernueGl> mStandardRevernueGlList1) {
        this.mStandardRevernueGlList1 = mStandardRevernueGlList1;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (glId != null ? glId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MGl)) {
            return false;
        }
        MGl other = (MGl) object;
        if ((this.glId == null && other.glId != null) || (this.glId != null && !this.glId.equals(other.glId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MGl[ glId=" + glId + " ]";
    }
    
}
