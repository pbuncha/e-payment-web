/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_INCOME_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MIncomeType.findAll", query = "SELECT m FROM MIncomeType m"),
    @NamedQuery(name = "MIncomeType.findByIncomeTypeId", query = "SELECT m FROM MIncomeType m WHERE m.incomeTypeId = :incomeTypeId"),
    @NamedQuery(name = "MIncomeType.findByCreatedBy", query = "SELECT m FROM MIncomeType m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MIncomeType.findByCreatedDate", query = "SELECT m FROM MIncomeType m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MIncomeType.findByEndDate", query = "SELECT m FROM MIncomeType m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MIncomeType.findByIncomeTypeName", query = "SELECT m FROM MIncomeType m WHERE m.incomeTypeName = :incomeTypeName"),
    @NamedQuery(name = "MIncomeType.findByIncomeTypeNameEn", query = "SELECT m FROM MIncomeType m WHERE m.incomeTypeNameEn = :incomeTypeNameEn"),
    @NamedQuery(name = "MIncomeType.findByStartDate", query = "SELECT m FROM MIncomeType m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MIncomeType.findByStatus", query = "SELECT m FROM MIncomeType m WHERE m.status = :status"),
    @NamedQuery(name = "MIncomeType.findByUpdatedBy", query = "SELECT m FROM MIncomeType m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MIncomeType.findByUpdatedDate", query = "SELECT m FROM MIncomeType m WHERE m.updatedDate = :updatedDate")})
public class MIncomeType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "INCOME_TYPE_ID", nullable = false)
    private Integer incomeTypeId;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "INCOME_TYPE_NAME", nullable = false, length = 255)
    private String incomeTypeName;
    @Basic(optional = false)
    @Column(name = "INCOME_TYPE_NAME_EN", nullable = false, length = 255)
    private String incomeTypeNameEn;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;

    public MIncomeType() {
    }

    public MIncomeType(Integer incomeTypeId) {
        this.incomeTypeId = incomeTypeId;
    }

    public MIncomeType(Integer incomeTypeId, String incomeTypeName, String incomeTypeNameEn, Date startDate) {
        this.incomeTypeId = incomeTypeId;
        this.incomeTypeName = incomeTypeName;
        this.incomeTypeNameEn = incomeTypeNameEn;
        this.startDate = startDate;
    }

    public Integer getIncomeTypeId() {
        return incomeTypeId;
    }

    public void setIncomeTypeId(Integer incomeTypeId) {
        this.incomeTypeId = incomeTypeId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getIncomeTypeName() {
        return incomeTypeName;
    }

    public void setIncomeTypeName(String incomeTypeName) {
        this.incomeTypeName = incomeTypeName;
    }

    public String getIncomeTypeNameEn() {
        return incomeTypeNameEn;
    }

    public void setIncomeTypeNameEn(String incomeTypeNameEn) {
        this.incomeTypeNameEn = incomeTypeNameEn;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (incomeTypeId != null ? incomeTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MIncomeType)) {
            return false;
        }
        MIncomeType other = (MIncomeType) object;
        if ((this.incomeTypeId == null && other.incomeTypeId != null) || (this.incomeTypeId != null && !this.incomeTypeId.equals(other.incomeTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MIncomeType[ incomeTypeId=" + incomeTypeId + " ]";
    }
    
}
