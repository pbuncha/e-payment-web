/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_LEVY_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MLevyType.findAll", query = "SELECT m FROM MLevyType m"),
    @NamedQuery(name = "MLevyType.findByLevyTypeId", query = "SELECT m FROM MLevyType m WHERE m.levyTypeId = :levyTypeId"),
    @NamedQuery(name = "MLevyType.findByCreatedBy", query = "SELECT m FROM MLevyType m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MLevyType.findByCreatedDate", query = "SELECT m FROM MLevyType m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MLevyType.findByEndDate", query = "SELECT m FROM MLevyType m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MLevyType.findByLevyTypeName", query = "SELECT m FROM MLevyType m WHERE m.levyTypeName = :levyTypeName"),
    @NamedQuery(name = "MLevyType.findByLevyTypeNameEn", query = "SELECT m FROM MLevyType m WHERE m.levyTypeNameEn = :levyTypeNameEn"),
    @NamedQuery(name = "MLevyType.findByStartDate", query = "SELECT m FROM MLevyType m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MLevyType.findByStatus", query = "SELECT m FROM MLevyType m WHERE m.status = :status"),
    @NamedQuery(name = "MLevyType.findByUpdatedBy", query = "SELECT m FROM MLevyType m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MLevyType.findByUpdatedDate", query = "SELECT m FROM MLevyType m WHERE m.updatedDate = :updatedDate")})
public class MLevyType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "LEVY_TYPE_ID", nullable = false)
    private Integer levyTypeId;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "LEVY_TYPE_NAME", nullable = false, length = 255)
    private String levyTypeName;
    @Basic(optional = false)
    @Column(name = "LEVY_TYPE_NAME_EN", nullable = false, length = 255)
    private String levyTypeNameEn;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "levyTypeId", fetch = FetchType.LAZY)
    private List<CatalogStructureItem> catalogStructureItemList;

    public MLevyType() {
    }

    public MLevyType(Integer levyTypeId) {
        this.levyTypeId = levyTypeId;
    }

    public MLevyType(Integer levyTypeId, String levyTypeName, String levyTypeNameEn, Date startDate) {
        this.levyTypeId = levyTypeId;
        this.levyTypeName = levyTypeName;
        this.levyTypeNameEn = levyTypeNameEn;
        this.startDate = startDate;
    }

    public Integer getLevyTypeId() {
        return levyTypeId;
    }

    public void setLevyTypeId(Integer levyTypeId) {
        this.levyTypeId = levyTypeId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getLevyTypeName() {
        return levyTypeName;
    }

    public void setLevyTypeName(String levyTypeName) {
        this.levyTypeName = levyTypeName;
    }

    public String getLevyTypeNameEn() {
        return levyTypeNameEn;
    }

    public void setLevyTypeNameEn(String levyTypeNameEn) {
        this.levyTypeNameEn = levyTypeNameEn;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<CatalogStructureItem> getCatalogStructureItemList() {
        return catalogStructureItemList;
    }

    public void setCatalogStructureItemList(List<CatalogStructureItem> catalogStructureItemList) {
        this.catalogStructureItemList = catalogStructureItemList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (levyTypeId != null ? levyTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MLevyType)) {
            return false;
        }
        MLevyType other = (MLevyType) object;
        if ((this.levyTypeId == null && other.levyTypeId != null) || (this.levyTypeId != null && !this.levyTypeId.equals(other.levyTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MLevyType[ levyTypeId=" + levyTypeId + " ]";
    }
    
}
