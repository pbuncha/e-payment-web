/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_LOG_ACTION", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MLogAction.findAll", query = "SELECT m FROM MLogAction m"),
    @NamedQuery(name = "MLogAction.findByActionId", query = "SELECT m FROM MLogAction m WHERE m.actionId = :actionId"),
    @NamedQuery(name = "MLogAction.findByActionName", query = "SELECT m FROM MLogAction m WHERE m.actionName = :actionName")})
public class MLogAction implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ACTION_ID", nullable = false, length = 2)
    private String actionId;
    @Basic(optional = false)
    @Column(name = "ACTION_NAME", nullable = false, length = 50)
    private String actionName;
    @OneToMany(mappedBy = "mlogaction", fetch = FetchType.LAZY)
    private List<LogAccess> logAccessList;

    public MLogAction() {
    }

    public MLogAction(String actionId) {
        this.actionId = actionId;
    }

    public MLogAction(String actionId, String actionName) {
        this.actionId = actionId;
        this.actionName = actionName;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    @XmlTransient
    public List<LogAccess> getLogAccessList() {
        return logAccessList;
    }

    public void setLogAccessList(List<LogAccess> logAccessList) {
        this.logAccessList = logAccessList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actionId != null ? actionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MLogAction)) {
            return false;
        }
        MLogAction other = (MLogAction) object;
        if ((this.actionId == null && other.actionId != null) || (this.actionId != null && !this.actionId.equals(other.actionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MLogAction[ actionId=" + actionId + " ]";
    }
    
}
