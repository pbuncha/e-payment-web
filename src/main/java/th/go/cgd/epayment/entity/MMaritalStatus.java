/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_MARITAL_STATUS", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MMaritalStatus.findAll", query = "SELECT m FROM MMaritalStatus m"),
    @NamedQuery(name = "MMaritalStatus.findByMaritalId", query = "SELECT m FROM MMaritalStatus m WHERE m.maritalId = :maritalId"),
    @NamedQuery(name = "MMaritalStatus.findByMaritalName", query = "SELECT m FROM MMaritalStatus m WHERE m.maritalName = :maritalName")})
public class MMaritalStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MARITAL_ID", nullable = false)
    private Short maritalId;
    @Basic(optional = false)
    @Column(name = "MARITAL_NAME", nullable = false, length = 100)
    private String maritalName;

    public MMaritalStatus() {
    }

    public MMaritalStatus(Short maritalId) {
        this.maritalId = maritalId;
    }

    public MMaritalStatus(Short maritalId, String maritalName) {
        this.maritalId = maritalId;
        this.maritalName = maritalName;
    }

    public Short getMaritalId() {
        return maritalId;
    }

    public void setMaritalId(Short maritalId) {
        this.maritalId = maritalId;
    }

    public String getMaritalName() {
        return maritalName;
    }

    public void setMaritalName(String maritalName) {
        this.maritalName = maritalName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (maritalId != null ? maritalId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MMaritalStatus)) {
            return false;
        }
        MMaritalStatus other = (MMaritalStatus) object;
        if ((this.maritalId == null && other.maritalId != null) || (this.maritalId != null && !this.maritalId.equals(other.maritalId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MMaritalStatus[ maritalId=" + maritalId + " ]";
    }
    
}
