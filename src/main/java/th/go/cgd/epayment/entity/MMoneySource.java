/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_MONEY_SOURCE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MMoneySource.findAll", query = "SELECT m FROM MMoneySource m"),
    @NamedQuery(name = "MMoneySource.findByMoneySourceId", query = "SELECT m FROM MMoneySource m WHERE m.moneySourceId = :moneySourceId"),
    @NamedQuery(name = "MMoneySource.findByCreatedBy", query = "SELECT m FROM MMoneySource m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MMoneySource.findByCreatedDate", query = "SELECT m FROM MMoneySource m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MMoneySource.findByEndDate", query = "SELECT m FROM MMoneySource m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MMoneySource.findByMoneySourceCode", query = "SELECT m FROM MMoneySource m WHERE m.moneySourceCode = :moneySourceCode"),
    @NamedQuery(name = "MMoneySource.findByMoneySourceName", query = "SELECT m FROM MMoneySource m WHERE m.moneySourceName = :moneySourceName"),
    @NamedQuery(name = "MMoneySource.findByMoneySourceType", query = "SELECT m FROM MMoneySource m WHERE m.moneySourceType = :moneySourceType"),
    @NamedQuery(name = "MMoneySource.findByStartDate", query = "SELECT m FROM MMoneySource m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MMoneySource.findByStatus", query = "SELECT m FROM MMoneySource m WHERE m.status = :status"),
    @NamedQuery(name = "MMoneySource.findByUpdatedBy", query = "SELECT m FROM MMoneySource m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MMoneySource.findByUpdatedDate", query = "SELECT m FROM MMoneySource m WHERE m.updatedDate = :updatedDate")})
public class MMoneySource implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MONEY_SOURCE_ID", nullable = false)
    private Integer moneySourceId;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "MONEY_SOURCE_CODE", nullable = false, length = 5)
    private String moneySourceCode;
    @Basic(optional = false)
    @Column(name = "MONEY_SOURCE_NAME", nullable = false, length = 255)
    private String moneySourceName;
    @Basic(optional = false)
    @Column(name = "MONEY_SOURCE_TYPE", nullable = false)
    private String moneySourceType;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sourceId", fetch = FetchType.LAZY)
    private List<GfmisGenItem> gfmisGenItemList;
    @OneToMany(mappedBy = "moneySourceId", fetch = FetchType.LAZY)
    private List<MStandardRevernue> mStandardRevernueList;

    public MMoneySource() {
    }

    public MMoneySource(Integer moneySourceId) {
        this.moneySourceId = moneySourceId;
    }

    public MMoneySource(Integer moneySourceId, String moneySourceCode, String moneySourceName, String moneySourceType, Date startDate) {
        this.moneySourceId = moneySourceId;
        this.moneySourceCode = moneySourceCode;
        this.moneySourceName = moneySourceName;
        this.moneySourceType = moneySourceType;
        this.startDate = startDate;
    }

    public Integer getMoneySourceId() {
        return moneySourceId;
    }

    public void setMoneySourceId(Integer moneySourceId) {
        this.moneySourceId = moneySourceId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getMoneySourceCode() {
        return moneySourceCode;
    }

    public void setMoneySourceCode(String moneySourceCode) {
        this.moneySourceCode = moneySourceCode;
    }

    public String getMoneySourceName() {
        return moneySourceName;
    }

    public void setMoneySourceName(String moneySourceName) {
        this.moneySourceName = moneySourceName;
    }

    public String getMoneySourceType() {
        return moneySourceType;
    }

    public void setMoneySourceType(String moneySourceType) {
        this.moneySourceType = moneySourceType;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<GfmisGenItem> getGfmisGenItemList() {
        return gfmisGenItemList;
    }

    public void setGfmisGenItemList(List<GfmisGenItem> gfmisGenItemList) {
        this.gfmisGenItemList = gfmisGenItemList;
    }

    @XmlTransient
    public List<MStandardRevernue> getMStandardRevernueList() {
        return mStandardRevernueList;
    }

    public void setMStandardRevernueList(List<MStandardRevernue> mStandardRevernueList) {
        this.mStandardRevernueList = mStandardRevernueList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (moneySourceId != null ? moneySourceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MMoneySource)) {
            return false;
        }
        MMoneySource other = (MMoneySource) object;
        if ((this.moneySourceId == null && other.moneySourceId != null) || (this.moneySourceId != null && !this.moneySourceId.equals(other.moneySourceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MMoneySource[ moneySourceId=" + moneySourceId + " ]";
    }
    
}
