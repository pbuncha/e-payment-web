/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_ORG_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MOrgType.findAll", query = "SELECT m FROM MOrgType m"),
    @NamedQuery(name = "MOrgType.findByOrgTypeId", query = "SELECT m FROM MOrgType m WHERE m.orgTypeId = :orgTypeId"),
    @NamedQuery(name = "MOrgType.findByOrgTypeName", query = "SELECT m FROM MOrgType m WHERE m.orgTypeName = :orgTypeName")})
public class MOrgType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ORG_TYPE_ID", nullable = false)
    private Integer orgTypeId;
    @Basic(optional = false)
    @Column(name = "ORG_TYPE_NAME", nullable = false, length = 100)
    private String orgTypeName;
    @OneToMany(mappedBy = "orgTypeId", fetch = FetchType.LAZY)
    private List<CommonPerson> commonPersonList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orgTypeId", fetch = FetchType.LAZY)
    private List<Organization> organizationList;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orgTypeId", fetch = FetchType.LAZY)
//    private List<UserOrganization> userOrganizationList;

    public MOrgType() {
    }

    public MOrgType(Integer orgTypeId) {
        this.orgTypeId = orgTypeId;
    }

    public MOrgType(Integer orgTypeId, String orgTypeName) {
        this.orgTypeId = orgTypeId;
        this.orgTypeName = orgTypeName;
    }

    public Integer getOrgTypeId() {
        return orgTypeId;
    }

    public void setOrgTypeId(Integer orgTypeId) {
        this.orgTypeId = orgTypeId;
    }

    public String getOrgTypeName() {
        return orgTypeName;
    }

    public void setOrgTypeName(String orgTypeName) {
        this.orgTypeName = orgTypeName;
    }

    @XmlTransient
    public List<CommonPerson> getCommonPersonList() {
        return commonPersonList;
    }

    public void setCommonPersonList(List<CommonPerson> commonPersonList) {
        this.commonPersonList = commonPersonList;
    }

    @XmlTransient
    public List<Organization> getOrganizationList() {
        return organizationList;
    }

    public void setOrganizationList(List<Organization> organizationList) {
        this.organizationList = organizationList;
    }

//    @XmlTransient
//    public List<UserOrganization> getUserOrganizationList() {
//        return userOrganizationList;
//    }
//
//    public void setUserOrganizationList(List<UserOrganization> userOrganizationList) {
//        this.userOrganizationList = userOrganizationList;
//    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orgTypeId != null ? orgTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MOrgType)) {
            return false;
        }
        MOrgType other = (MOrgType) object;
        if ((this.orgTypeId == null && other.orgTypeId != null) || (this.orgTypeId != null && !this.orgTypeId.equals(other.orgTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MOrgType[ orgTypeId=" + orgTypeId + " ]";
    }
    
}
