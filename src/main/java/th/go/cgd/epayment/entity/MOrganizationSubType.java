/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_ORGANIZATION_SUB_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MOrganizationSubType.findAll", query = "SELECT m FROM MOrganizationSubType m"),
    @NamedQuery(name = "MOrganizationSubType.findByOrganizationSubTypeId", query = "SELECT m FROM MOrganizationSubType m WHERE m.organizationSubTypeId = :organizationSubTypeId"),
    @NamedQuery(name = "MOrganizationSubType.findByCreatedDate", query = "SELECT m FROM MOrganizationSubType m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MOrganizationSubType.findByEndDate", query = "SELECT m FROM MOrganizationSubType m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MOrganizationSubType.findByOrganizationSubTypeName", query = "SELECT m FROM MOrganizationSubType m WHERE m.organizationSubTypeName = :organizationSubTypeName"),
    @NamedQuery(name = "MOrganizationSubType.findByStartDate", query = "SELECT m FROM MOrganizationSubType m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MOrganizationSubType.findByStatus", query = "SELECT m FROM MOrganizationSubType m WHERE m.status = :status"),
    @NamedQuery(name = "MOrganizationSubType.findByUpdatedDate", query = "SELECT m FROM MOrganizationSubType m WHERE m.updatedDate = :updatedDate")})
public class MOrganizationSubType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ORGANIZATION_SUB_TYPE_ID", nullable = false)
    private Integer organizationSubTypeId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "ORGANIZATION_SUB_TYPE_NAME", nullable = false, length = 255)
    private String organizationSubTypeName;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "ORGANIZATION_TYPE_ID", referencedColumnName = "ORGANIZATION_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MOrganizationType organizationTypeId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organizationSubTypeId", fetch = FetchType.LAZY)
    private List<MDepartment> mDepartmentList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organizationSubTypeId", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList;

    public MOrganizationSubType() {
    }

    public MOrganizationSubType(Integer organizationSubTypeId) {
        this.organizationSubTypeId = organizationSubTypeId;
    }

    public MOrganizationSubType(Integer organizationSubTypeId, String organizationSubTypeName, Date startDate) {
        this.organizationSubTypeId = organizationSubTypeId;
        this.organizationSubTypeName = organizationSubTypeName;
        this.startDate = startDate;
    }

    public Integer getOrganizationSubTypeId() {
        return organizationSubTypeId;
    }

    public void setOrganizationSubTypeId(Integer organizationSubTypeId) {
        this.organizationSubTypeId = organizationSubTypeId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getOrganizationSubTypeName() {
        return organizationSubTypeName;
    }

    public void setOrganizationSubTypeName(String organizationSubTypeName) {
        this.organizationSubTypeName = organizationSubTypeName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MOrganizationType getOrganizationTypeId() {
        return organizationTypeId;
    }

    public void setOrganizationTypeId(MOrganizationType organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    @XmlTransient
    public List<MDepartment> getMDepartmentList() {
        return mDepartmentList;
    }

    public void setMDepartmentList(List<MDepartment> mDepartmentList) {
        this.mDepartmentList = mDepartmentList;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList() {
        return userOrganizationList;
    }

    public void setUserOrganizationList(List<UserOrganization> userOrganizationList) {
        this.userOrganizationList = userOrganizationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (organizationSubTypeId != null ? organizationSubTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MOrganizationSubType)) {
            return false;
        }
        MOrganizationSubType other = (MOrganizationSubType) object;
        if ((this.organizationSubTypeId == null && other.organizationSubTypeId != null) || (this.organizationSubTypeId != null && !this.organizationSubTypeId.equals(other.organizationSubTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MOrganizationSubType[ organizationSubTypeId=" + organizationSubTypeId + " ]";
    }
    
}
