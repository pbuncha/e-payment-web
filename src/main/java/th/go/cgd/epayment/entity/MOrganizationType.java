/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_ORGANIZATION_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MOrganizationType.findAll", query = "SELECT m FROM MOrganizationType m"),
    @NamedQuery(name = "MOrganizationType.findByOrganizationTypeId", query = "SELECT m FROM MOrganizationType m WHERE m.organizationTypeId = :organizationTypeId"),
    @NamedQuery(name = "MOrganizationType.findByCreatedDate", query = "SELECT m FROM MOrganizationType m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MOrganizationType.findByEndDate", query = "SELECT m FROM MOrganizationType m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MOrganizationType.findByOrganizationTypeName", query = "SELECT m FROM MOrganizationType m WHERE m.organizationTypeName = :organizationTypeName"),
    @NamedQuery(name = "MOrganizationType.findByStartDate", query = "SELECT m FROM MOrganizationType m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MOrganizationType.findByStatus", query = "SELECT m FROM MOrganizationType m WHERE m.status = :status"),
    @NamedQuery(name = "MOrganizationType.findByUpdatedDate", query = "SELECT m FROM MOrganizationType m WHERE m.updatedDate = :updatedDate")})
public class MOrganizationType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ORGANIZATION_TYPE_ID", nullable = false)
    private Integer organizationTypeId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "ORGANIZATION_TYPE_NAME", nullable = false, length = 255)
    private String organizationTypeName;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organizationTypeId", fetch = FetchType.LAZY)
    private List<MOrganizationSubType> mOrganizationSubTypeList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "organizationTypeId", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList;

    public MOrganizationType() {
    }

    public MOrganizationType(Integer organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    public MOrganizationType(Integer organizationTypeId, String organizationTypeName, Date startDate) {
        this.organizationTypeId = organizationTypeId;
        this.organizationTypeName = organizationTypeName;
        this.startDate = startDate;
    }

    public Integer getOrganizationTypeId() {
        return organizationTypeId;
    }

    public void setOrganizationTypeId(Integer organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getOrganizationTypeName() {
        return organizationTypeName;
    }

    public void setOrganizationTypeName(String organizationTypeName) {
        this.organizationTypeName = organizationTypeName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<MOrganizationSubType> getMOrganizationSubTypeList() {
        return mOrganizationSubTypeList;
    }

    public void setMOrganizationSubTypeList(List<MOrganizationSubType> mOrganizationSubTypeList) {
        this.mOrganizationSubTypeList = mOrganizationSubTypeList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList() {
        return userOrganizationList;
    }

    public void setUserOrganizationList(List<UserOrganization> userOrganizationList) {
        this.userOrganizationList = userOrganizationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (organizationTypeId != null ? organizationTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MOrganizationType)) {
            return false;
        }
        MOrganizationType other = (MOrganizationType) object;
        if ((this.organizationTypeId == null && other.organizationTypeId != null) || (this.organizationTypeId != null && !this.organizationTypeId.equals(other.organizationTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MOrganizationType[ organizationTypeId=" + organizationTypeId + " ]";
    }
    
}
