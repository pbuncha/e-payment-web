/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_PARAM", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MParam.findAll", query = "SELECT m FROM MParam m"),
    @NamedQuery(name = "MParam.findByName", query = "SELECT m FROM MParam m WHERE m.name = :name"),
    @NamedQuery(name = "MParam.findByDesc", query = "SELECT m FROM MParam m WHERE m.desc = :desc"),
    @NamedQuery(name = "MParam.findByValue", query = "SELECT m FROM MParam m WHERE m.value = :value")})
public class MParam implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "NAME", nullable = false, length = 50)
    private String name;
    @Basic(optional = false)
    @Column(name = "DESC", nullable = false, length = 500)
    private String desc;
    @Basic(optional = false)
    @Column(name = "VALUE", nullable = false, length = 500)
    private String value;

    public MParam() {
    }

    public MParam(String name) {
        this.name = name;
    }

    public MParam(String name, String desc, String value) {
        this.name = name;
        this.desc = desc;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (name != null ? name.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MParam)) {
            return false;
        }
        MParam other = (MParam) object;
        if ((this.name == null && other.name != null) || (this.name != null && !this.name.equals(other.name))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MParam[ name=" + name + " ]";
    }
    
}
