/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_PAYMENT_CHANNEL", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MPaymentChannel.findAll", query = "SELECT m FROM MPaymentChannel m"),
    @NamedQuery(name = "MPaymentChannel.findByPaymentChannelId", query = "SELECT m FROM MPaymentChannel m WHERE m.paymentChannelId = :paymentChannelId"),
    @NamedQuery(name = "MPaymentChannel.findByCreatedBy", query = "SELECT m FROM MPaymentChannel m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MPaymentChannel.findByCreatedDate", query = "SELECT m FROM MPaymentChannel m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MPaymentChannel.findByEndDate", query = "SELECT m FROM MPaymentChannel m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MPaymentChannel.findByPaymentChannelName", query = "SELECT m FROM MPaymentChannel m WHERE m.paymentChannelName = :paymentChannelName"),
    @NamedQuery(name = "MPaymentChannel.findByStartDate", query = "SELECT m FROM MPaymentChannel m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MPaymentChannel.findByStatus", query = "SELECT m FROM MPaymentChannel m WHERE m.status = :status"),
    @NamedQuery(name = "MPaymentChannel.findByUpdatedBy", query = "SELECT m FROM MPaymentChannel m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MPaymentChannel.findByUpdatedDate", query = "SELECT m FROM MPaymentChannel m WHERE m.updatedDate = :updatedDate")})
public class MPaymentChannel implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PAYMENT_CHANNEL_ID", nullable = false)
    private Integer paymentChannelId;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "PAYMENT_CHANNEL_NAME", nullable = false, length = 255)
    private String paymentChannelName;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(mappedBy = "paymentChannelId", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList;

    public MPaymentChannel() {
    }

    public MPaymentChannel(Integer paymentChannelId) {
        this.paymentChannelId = paymentChannelId;
    }

    public MPaymentChannel(Integer paymentChannelId, String paymentChannelName, Date startDate) {
        this.paymentChannelId = paymentChannelId;
        this.paymentChannelName = paymentChannelName;
        this.startDate = startDate;
    }

    public Integer getPaymentChannelId() {
        return paymentChannelId;
    }

    public void setPaymentChannelId(Integer paymentChannelId) {
        this.paymentChannelId = paymentChannelId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPaymentChannelName() {
        return paymentChannelName;
    }

    public void setPaymentChannelName(String paymentChannelName) {
        this.paymentChannelName = paymentChannelName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paymentChannelId != null ? paymentChannelId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MPaymentChannel)) {
            return false;
        }
        MPaymentChannel other = (MPaymentChannel) object;
        if ((this.paymentChannelId == null && other.paymentChannelId != null) || (this.paymentChannelId != null && !this.paymentChannelId.equals(other.paymentChannelId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MPaymentChannel[ paymentChannelId=" + paymentChannelId + " ]";
    }
    
}
