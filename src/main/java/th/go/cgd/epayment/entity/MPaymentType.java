/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_PAYMENT_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MPaymentType.findAll", query = "SELECT m FROM MPaymentType m"),
    @NamedQuery(name = "MPaymentType.findByPaymentTypeId", query = "SELECT m FROM MPaymentType m WHERE m.paymentTypeId = :paymentTypeId"),
    @NamedQuery(name = "MPaymentType.findByCreatedBy", query = "SELECT m FROM MPaymentType m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MPaymentType.findByCreatedDate", query = "SELECT m FROM MPaymentType m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MPaymentType.findByEndDate", query = "SELECT m FROM MPaymentType m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MPaymentType.findByPaymentChannelName", query = "SELECT m FROM MPaymentType m WHERE m.paymentChannelName = :paymentChannelName"),
    @NamedQuery(name = "MPaymentType.findByStartDate", query = "SELECT m FROM MPaymentType m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MPaymentType.findByStatus", query = "SELECT m FROM MPaymentType m WHERE m.status = :status"),
    @NamedQuery(name = "MPaymentType.findByUpdatedBy", query = "SELECT m FROM MPaymentType m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MPaymentType.findByUpdatedDate", query = "SELECT m FROM MPaymentType m WHERE m.updatedDate = :updatedDate")})
public class MPaymentType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PAYMENT_TYPE_ID", nullable = false)
    private Integer paymentTypeId;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "PAYMENT_CHANNEL_NAME", nullable = false, length = 255)
    private String paymentChannelName;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(mappedBy = "paymentTypeId", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList;

    public MPaymentType() {
    }

    public MPaymentType(Integer paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public MPaymentType(Integer paymentTypeId, String paymentChannelName, Date startDate) {
        this.paymentTypeId = paymentTypeId;
        this.paymentChannelName = paymentChannelName;
        this.startDate = startDate;
    }

    public Integer getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(Integer paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getPaymentChannelName() {
        return paymentChannelName;
    }

    public void setPaymentChannelName(String paymentChannelName) {
        this.paymentChannelName = paymentChannelName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paymentTypeId != null ? paymentTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MPaymentType)) {
            return false;
        }
        MPaymentType other = (MPaymentType) object;
        if ((this.paymentTypeId == null && other.paymentTypeId != null) || (this.paymentTypeId != null && !this.paymentTypeId.equals(other.paymentTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MPaymentType[ paymentTypeId=" + paymentTypeId + " ]";
    }
    
}
