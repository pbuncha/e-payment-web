/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_PERMISSION_GROUP", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MPermissionGroup.findAll", query = "SELECT m FROM MPermissionGroup m"),
    @NamedQuery(name = "MPermissionGroup.findByMPermissionGroupId", query = "SELECT m FROM MPermissionGroup m WHERE m.mPermissionGroupId = :mPermissionGroupId"),
    @NamedQuery(name = "MPermissionGroup.findByCreateDate", query = "SELECT m FROM MPermissionGroup m WHERE m.createDate = :createDate"),
    @NamedQuery(name = "MPermissionGroup.findByDescription", query = "SELECT m FROM MPermissionGroup m WHERE m.description = :description"),
    @NamedQuery(name = "MPermissionGroup.findByMPermissionGroupName", query = "SELECT m FROM MPermissionGroup m WHERE m.mPermissionGroupName = :mPermissionGroupName"),
    @NamedQuery(name = "MPermissionGroup.findByUpdateDate", query = "SELECT m FROM MPermissionGroup m WHERE m.updateDate = :updateDate"),
    @NamedQuery(name = "MPermissionGroup.findByMPermissionGroupCode", query = "SELECT m FROM MPermissionGroup m WHERE m.mPermissionGroupCode = :mPermissionGroupCode")})
public class MPermissionGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "M_PERMISSION_GROUP_ID", nullable = false)
    private Integer mPermissionGroupId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "DESCRIPTION", length = 1000)
    private String description;
    @Basic(optional = false)
    @Column(name = "M_PERMISSION_GROUP_NAME", nullable = false, length = 200)
    private String mPermissionGroupName;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "M_PERMISSION_GROUP_CODE", length = 50)
    private String mPermissionGroupCode;
    @JoinColumn(name = "UPDATE_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User updateBy;
    @JoinColumn(name = "CREATE_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createBy;
    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private MStatus status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mPermissionGroupId", fetch = FetchType.LAZY)
    private List<MUserPermissiongroup> mUserPermissiongroupList;

    public MPermissionGroup() {
    }

    public MPermissionGroup(Integer mPermissionGroupId) {
        this.mPermissionGroupId = mPermissionGroupId;
    }

    public MPermissionGroup(Integer mPermissionGroupId, Date createDate, String mPermissionGroupName, Date updateDate) {
        this.mPermissionGroupId = mPermissionGroupId;
        this.createDate = createDate;
        this.mPermissionGroupName = mPermissionGroupName;
        this.updateDate = updateDate;
    }

    public Integer getMPermissionGroupId() {
        return mPermissionGroupId;
    }

    public void setMPermissionGroupId(Integer mPermissionGroupId) {
        this.mPermissionGroupId = mPermissionGroupId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMPermissionGroupName() {
        return mPermissionGroupName;
    }

    public void setMPermissionGroupName(String mPermissionGroupName) {
        this.mPermissionGroupName = mPermissionGroupName;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getMPermissionGroupCode() {
        return mPermissionGroupCode;
    }

    public void setMPermissionGroupCode(String mPermissionGroupCode) {
        this.mPermissionGroupCode = mPermissionGroupCode;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public MStatus getStatus() {
        return status;
    }

    public void setStatus(MStatus status) {
        this.status = status;
    }

    @XmlTransient
    public List<MUserPermissiongroup> getMUserPermissiongroupList() {
        return mUserPermissiongroupList;
    }

    public void setMUserPermissiongroupList(List<MUserPermissiongroup> mUserPermissiongroupList) {
        this.mUserPermissiongroupList = mUserPermissiongroupList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mPermissionGroupId != null ? mPermissionGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MPermissionGroup)) {
            return false;
        }
        MPermissionGroup other = (MPermissionGroup) object;
        if ((this.mPermissionGroupId == null && other.mPermissionGroupId != null) || (this.mPermissionGroupId != null && !this.mPermissionGroupId.equals(other.mPermissionGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MPermissionGroup[ mPermissionGroupId=" + mPermissionGroupId + " ]";
    }
    
}
