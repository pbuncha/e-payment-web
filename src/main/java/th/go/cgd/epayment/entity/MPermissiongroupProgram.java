/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_PERMISSIONGROUP_PROGRAM", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MPermissiongroupProgram.findAll", query = "SELECT m FROM MPermissiongroupProgram m"),
    @NamedQuery(name = "MPermissiongroupProgram.findByProgramId", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.programId = :programId"),
    @NamedQuery(name = "MPermissiongroupProgram.findByMPermissionGroupId", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.mPermissionGroupId = :mPermissionGroupId"),
    @NamedQuery(name = "MPermissiongroupProgram.findByUpdateDate", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.updateDate = :updateDate"),
    @NamedQuery(name = "MPermissiongroupProgram.findByCreateDate", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.createDate = :createDate"),
    @NamedQuery(name = "MPermissiongroupProgram.findByCanview", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.canview = :canview"),
    @NamedQuery(name = "MPermissiongroupProgram.findByMPermissiongroupProgramId", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.mPermissiongroupProgramId = :mPermissiongroupProgramId"),
    @NamedQuery(name = "MPermissiongroupProgram.findByCanedit", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.canedit = :canedit"),
    @NamedQuery(name = "MPermissiongroupProgram.findByCanexcel", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.canexcel = :canexcel"),
    @NamedQuery(name = "MPermissiongroupProgram.findByCanpdf", query = "SELECT m FROM MPermissiongroupProgram m WHERE m.canpdf = :canpdf")})
public class MPermissiongroupProgram implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @Column(name = "PROGRAM_ID", nullable = false)
    private int programId;
    @Basic(optional = false)
    @Column(name = "M_PERMISSION_GROUP_ID", nullable = false)
    private int mPermissionGroupId;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "CANVIEW")
    private Character canview;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "M_PERMISSIONGROUP_PROGRAM_ID", nullable = false)
    private Integer mPermissiongroupProgramId;
    @Column(name = "CANEDIT")
    private Character canedit;
    @Column(name = "CANEXCEL")
    private Character canexcel;
    @Column(name = "CANPDF")
    private Character canpdf;
    @JoinColumn(name = "UPDATE_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User updateBy;
    @JoinColumn(name = "CREATE_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createBy;
    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MStatus status;

    public MPermissiongroupProgram() {
    }

    public MPermissiongroupProgram(Integer mPermissiongroupProgramId) {
        this.mPermissiongroupProgramId = mPermissiongroupProgramId;
    }

    public MPermissiongroupProgram(Integer mPermissiongroupProgramId, int programId, int mPermissionGroupId, Date updateDate, Date createDate) {
        this.mPermissiongroupProgramId = mPermissiongroupProgramId;
        this.programId = programId;
        this.mPermissionGroupId = mPermissionGroupId;
        this.updateDate = updateDate;
        this.createDate = createDate;
    }

    public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public int getMPermissionGroupId() {
        return mPermissionGroupId;
    }

    public void setMPermissionGroupId(int mPermissionGroupId) {
        this.mPermissionGroupId = mPermissionGroupId;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Character getCanview() {
        return canview;
    }

    public void setCanview(Character canview) {
        this.canview = canview;
    }

    public Integer getMPermissiongroupProgramId() {
        return mPermissiongroupProgramId;
    }

    public void setMPermissiongroupProgramId(Integer mPermissiongroupProgramId) {
        this.mPermissiongroupProgramId = mPermissiongroupProgramId;
    }

    public Character getCanedit() {
        return canedit;
    }

    public void setCanedit(Character canedit) {
        this.canedit = canedit;
    }

    public Character getCanexcel() {
        return canexcel;
    }

    public void setCanexcel(Character canexcel) {
        this.canexcel = canexcel;
    }

    public Character getCanpdf() {
        return canpdf;
    }

    public void setCanpdf(Character canpdf) {
        this.canpdf = canpdf;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public MStatus getStatus() {
        return status;
    }

    public void setStatus(MStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (mPermissiongroupProgramId != null ? mPermissiongroupProgramId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MPermissiongroupProgram)) {
            return false;
        }
        MPermissiongroupProgram other = (MPermissiongroupProgram) object;
        if ((this.mPermissiongroupProgramId == null && other.mPermissiongroupProgramId != null) || (this.mPermissiongroupProgramId != null && !this.mPermissiongroupProgramId.equals(other.mPermissiongroupProgramId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MPermissiongroupProgram[ mPermissiongroupProgramId=" + mPermissiongroupProgramId + " ]";
    }
    
}
