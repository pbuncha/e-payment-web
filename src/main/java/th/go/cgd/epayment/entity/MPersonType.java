/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_PERSON_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MPersonType.findAll", query = "SELECT m FROM MPersonType m"),
    @NamedQuery(name = "MPersonType.findByPersonTypeId", query = "SELECT m FROM MPersonType m WHERE m.personTypeId = :personTypeId"),
    @NamedQuery(name = "MPersonType.findByPersonTypeName", query = "SELECT m FROM MPersonType m WHERE m.personTypeName = :personTypeName")})
public class MPersonType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PERSON_TYPE_ID", nullable = false)
    private Integer personTypeId;
    @Basic(optional = false)
    @Column(name = "PERSON_TYPE_NAME", nullable = false, length = 100)
    private String personTypeName;

    public MPersonType() {
    }

    public MPersonType(Integer personTypeId) {
        this.personTypeId = personTypeId;
    }

    public MPersonType(Integer personTypeId, String personTypeName) {
        this.personTypeId = personTypeId;
        this.personTypeName = personTypeName;
    }

    public Integer getPersonTypeId() {
        return personTypeId;
    }

    public void setPersonTypeId(Integer personTypeId) {
        this.personTypeId = personTypeId;
    }

    public String getPersonTypeName() {
        return personTypeName;
    }

    public void setPersonTypeName(String personTypeName) {
        this.personTypeName = personTypeName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (personTypeId != null ? personTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MPersonType)) {
            return false;
        }
        MPersonType other = (MPersonType) object;
        if ((this.personTypeId == null && other.personTypeId != null) || (this.personTypeId != null && !this.personTypeId.equals(other.personTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MPersonType[ personTypeId=" + personTypeId + " ]";
    }
    
}
