/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_PROGRAM", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MProgram.findAll", query = "SELECT m FROM MProgram m"),
    @NamedQuery(name = "MProgram.findByProgramId", query = "SELECT m FROM MProgram m WHERE m.programId = :programId"),
    @NamedQuery(name = "MProgram.findByCreateDate", query = "SELECT m FROM MProgram m WHERE m.createDate = :createDate"),
    @NamedQuery(name = "MProgram.findByDescription", query = "SELECT m FROM MProgram m WHERE m.description = :description"),
    @NamedQuery(name = "MProgram.findByProgramName", query = "SELECT m FROM MProgram m WHERE m.programName = :programName"),
    @NamedQuery(name = "MProgram.findByUpdateDate", query = "SELECT m FROM MProgram m WHERE m.updateDate = :updateDate"),
    @NamedQuery(name = "MProgram.findByProgramCode", query = "SELECT m FROM MProgram m WHERE m.programCode = :programCode")})
public class MProgram implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PROGRAM_ID", nullable = false)
    private Integer programId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "DESCRIPTION", length = 1000)
    private String description;
    @Basic(optional = false)
    @Column(name = "PROGRAM_NAME", nullable = false, length = 200)
    private String programName;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "PROGRAM_CODE", length = 50)
    private String programCode;
    @JoinColumn(name = "UPDATE_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User updateBy;
    @JoinColumn(name = "CREATE_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createBy;
    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MStatus status;

    public MProgram() {
    }

    public MProgram(Integer programId) {
        this.programId = programId;
    }

    public MProgram(Integer programId, Date createDate, String programName, Date updateDate) {
        this.programId = programId;
        this.createDate = createDate;
        this.programName = programName;
        this.updateDate = updateDate;
    }

    public Integer getProgramId() {
        return programId;
    }

    public void setProgramId(Integer programId) {
        this.programId = programId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public MStatus getStatus() {
        return status;
    }

    public void setStatus(MStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (programId != null ? programId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MProgram)) {
            return false;
        }
        MProgram other = (MProgram) object;
        if ((this.programId == null && other.programId != null) || (this.programId != null && !this.programId.equals(other.programId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MProgram[ programId=" + programId + " ]";
    }
    
}
