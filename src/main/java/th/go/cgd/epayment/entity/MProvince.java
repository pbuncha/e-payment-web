/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_PROVINCE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MProvince.findAll", query = "SELECT m FROM MProvince m"),
    @NamedQuery(name = "MProvince.findByProvinceId", query = "SELECT m FROM MProvince m WHERE m.provinceId = :provinceId"),
    @NamedQuery(name = "MProvince.findByCreatedBy", query = "SELECT m FROM MProvince m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MProvince.findByCreatedDate", query = "SELECT m FROM MProvince m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MProvince.findByProvinceNameEn", query = "SELECT m FROM MProvince m WHERE m.provinceNameEn = :provinceNameEn"),
    @NamedQuery(name = "MProvince.findByProvinceNameTh", query = "SELECT m FROM MProvince m WHERE m.provinceNameTh = :provinceNameTh"),
    @NamedQuery(name = "MProvince.findByUpdatedBy", query = "SELECT m FROM MProvince m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MProvince.findByUpdatedDate", query = "SELECT m FROM MProvince m WHERE m.updatedDate = :updatedDate")})
public class MProvince implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PROVINCE_ID", nullable = false)
    private Integer provinceId;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false)
    private Integer createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "PROVINCE_NAME_EN", length = 255)
    private String provinceNameEn;
    @Basic(optional = false)
    @Column(name = "PROVINCE_NAME_TH", nullable = false, length = 255)
    private String provinceNameTh;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "provinceId", fetch = FetchType.LAZY)
    private List<MDistrict> mDistrictList;
    @OneToMany(mappedBy = "provinceId", fetch = FetchType.LAZY)
    private List<MBusinessArea> mBusinessAreaList;
    @OneToMany(mappedBy = "provinceId", fetch = FetchType.LAZY)
    private List<Address> addressList;

    public MProvince() {
    }

    public MProvince(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public MProvince(Integer provinceId, int createdBy, Date createdDate, String provinceNameTh) {
        this.provinceId = provinceId;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.provinceNameTh = provinceNameTh;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getProvinceNameEn() {
        return provinceNameEn;
    }

    public void setProvinceNameEn(String provinceNameEn) {
        this.provinceNameEn = provinceNameEn;
    }

    public String getProvinceNameTh() {
        return provinceNameTh;
    }

    public void setProvinceNameTh(String provinceNameTh) {
        this.provinceNameTh = provinceNameTh;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<MDistrict> getMDistrictList() {
        return mDistrictList;
    }

    public void setMDistrictList(List<MDistrict> mDistrictList) {
        this.mDistrictList = mDistrictList;
    }

    @XmlTransient
    public List<MBusinessArea> getMBusinessAreaList() {
        return mBusinessAreaList;
    }

    public void setMBusinessAreaList(List<MBusinessArea> mBusinessAreaList) {
        this.mBusinessAreaList = mBusinessAreaList;
    }

    @XmlTransient
    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (provinceId != null ? provinceId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MProvince)) {
            return false;
        }
        MProvince other = (MProvince) object;
        if ((this.provinceId == null && other.provinceId != null) || (this.provinceId != null && !this.provinceId.equals(other.provinceId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MProvince[ provinceId=" + provinceId + " ]";
    }
    
}
