/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_REPORT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MReport.findAll", query = "SELECT m FROM MReport m"),
    @NamedQuery(name = "MReport.findByReportId", query = "SELECT m FROM MReport m WHERE m.reportId = :reportId"),
    @NamedQuery(name = "MReport.findByReportCode", query = "SELECT m FROM MReport m WHERE m.reportCode = :reportCode"),
    @NamedQuery(name = "MReport.findByReportDir", query = "SELECT m FROM MReport m WHERE m.reportDir = :reportDir"),
    @NamedQuery(name = "MReport.findByReportFile", query = "SELECT m FROM MReport m WHERE m.reportFile = :reportFile"),
    @NamedQuery(name = "MReport.findByStatus", query = "SELECT m FROM MReport m WHERE m.status = :status"),
    @NamedQuery(name = "MReport.findByUrl", query = "SELECT m FROM MReport m WHERE m.url = :url")})
public class MReport implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REPORT_ID", nullable = false)
    private Integer reportId;
    @Basic(optional = false)
    @Column(name = "REPORT_CODE", nullable = false, length = 30)
    private String reportCode;
    @Basic(optional = false)
    @Column(name = "REPORT_DIR", nullable = false, length = 50)
    private String reportDir;
    @Basic(optional = false)
    @Column(name = "REPORT_FILE", nullable = false, length = 30)
    private String reportFile;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false, length = 5)
    private String status;
    @Basic(optional = false)
    @Column(name = "URL", nullable = false, length = 100)
    private String url;
//    @JoinTable(name = "PERMISSION_REPORT", joinColumns = {
//        @JoinColumn(name = "REPORT_ID", referencedColumnName = "REPORT_ID", nullable = false)}, inverseJoinColumns = {
//        @JoinColumn(name = "PERMISSION_ID", referencedColumnName = "PERMISSION_ID", nullable = false)})
//    @ManyToMany(fetch = FetchType.LAZY)
//    private List<Permission> permissionList;
    @JoinColumn(name = "REPORT_GROUP_ID", referencedColumnName = "REPORT_GROUP_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MReportGroup reportGroupId;

    public MReport() {
    }

    public MReport(Integer reportId) {
        this.reportId = reportId;
    }

    public MReport(Integer reportId, String reportCode, String reportDir, String reportFile, String status, String url) {
        this.reportId = reportId;
        this.reportCode = reportCode;
        this.reportDir = reportDir;
        this.reportFile = reportFile;
        this.status = status;
        this.url = url;
    }

    public Integer getReportId() {
        return reportId;
    }

    public void setReportId(Integer reportId) {
        this.reportId = reportId;
    }

    public String getReportCode() {
        return reportCode;
    }

    public void setReportCode(String reportCode) {
        this.reportCode = reportCode;
    }

    public String getReportDir() {
        return reportDir;
    }

    public void setReportDir(String reportDir) {
        this.reportDir = reportDir;
    }

    public String getReportFile() {
        return reportFile;
    }

    public void setReportFile(String reportFile) {
        this.reportFile = reportFile;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

//    @XmlTransient
//    public List<Permission> getPermissionList() {
//        return permissionList;
//    }
//
//    public void setPermissionList(List<Permission> permissionList) {
//        this.permissionList = permissionList;
//    }

    public MReportGroup getReportGroupId() {
        return reportGroupId;
    }

    public void setReportGroupId(MReportGroup reportGroupId) {
        this.reportGroupId = reportGroupId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportId != null ? reportId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MReport)) {
            return false;
        }
        MReport other = (MReport) object;
        if ((this.reportId == null && other.reportId != null) || (this.reportId != null && !this.reportId.equals(other.reportId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MReport[ reportId=" + reportId + " ]";
    }
    
}
