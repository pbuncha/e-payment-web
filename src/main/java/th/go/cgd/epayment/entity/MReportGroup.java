/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_REPORT_GROUP", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MReportGroup.findAll", query = "SELECT m FROM MReportGroup m"),
    @NamedQuery(name = "MReportGroup.findByReportGroupId", query = "SELECT m FROM MReportGroup m WHERE m.reportGroupId = :reportGroupId"),
    @NamedQuery(name = "MReportGroup.findByReportGroupName", query = "SELECT m FROM MReportGroup m WHERE m.reportGroupName = :reportGroupName"),
    @NamedQuery(name = "MReportGroup.findByStatus", query = "SELECT m FROM MReportGroup m WHERE m.status = :status")})
public class MReportGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REPORT_GROUP_ID", nullable = false)
    private Integer reportGroupId;
    @Basic(optional = false)
    @Column(name = "REPORT_GROUP_NAME", nullable = false, length = 1000)
    private String reportGroupName;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false, length = 5)
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "reportGroupId", fetch = FetchType.LAZY)
    private List<MReport> mReportList;

    public MReportGroup() {
    }

    public MReportGroup(Integer reportGroupId) {
        this.reportGroupId = reportGroupId;
    }

    public MReportGroup(Integer reportGroupId, String reportGroupName, String status) {
        this.reportGroupId = reportGroupId;
        this.reportGroupName = reportGroupName;
        this.status = status;
    }

    public Integer getReportGroupId() {
        return reportGroupId;
    }

    public void setReportGroupId(Integer reportGroupId) {
        this.reportGroupId = reportGroupId;
    }

    public String getReportGroupName() {
        return reportGroupName;
    }

    public void setReportGroupName(String reportGroupName) {
        this.reportGroupName = reportGroupName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @XmlTransient
    public List<MReport> getMReportList() {
        return mReportList;
    }

    public void setMReportList(List<MReport> mReportList) {
        this.mReportList = mReportList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (reportGroupId != null ? reportGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MReportGroup)) {
            return false;
        }
        MReportGroup other = (MReportGroup) object;
        if ((this.reportGroupId == null && other.reportGroupId != null) || (this.reportGroupId != null && !this.reportGroupId.equals(other.reportGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MReportGroup[ reportGroupId=" + reportGroupId + " ]";
    }
    
}
