/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_REVENUE_TYPE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MRevenueType.findAll", query = "SELECT m FROM MRevenueType m"),
    @NamedQuery(name = "MRevenueType.findByRevenueTypeId", query = "SELECT m FROM MRevenueType m WHERE m.revenueTypeId = :revenueTypeId"),
    @NamedQuery(name = "MRevenueType.findByCreatedBy", query = "SELECT m FROM MRevenueType m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MRevenueType.findByCreatedDate", query = "SELECT m FROM MRevenueType m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MRevenueType.findByEndDate", query = "SELECT m FROM MRevenueType m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MRevenueType.findByRevenueTypeName", query = "SELECT m FROM MRevenueType m WHERE m.revenueTypeName = :revenueTypeName"),
    @NamedQuery(name = "MRevenueType.findByRevenueTypeNameEn", query = "SELECT m FROM MRevenueType m WHERE m.revenueTypeNameEn = :revenueTypeNameEn"),
    @NamedQuery(name = "MRevenueType.findByStartDate", query = "SELECT m FROM MRevenueType m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MRevenueType.findByStatus", query = "SELECT m FROM MRevenueType m WHERE m.status = :status"),
    @NamedQuery(name = "MRevenueType.findByUpdatedBy", query = "SELECT m FROM MRevenueType m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MRevenueType.findByUpdatedDate", query = "SELECT m FROM MRevenueType m WHERE m.updatedDate = :updatedDate")})
public class MRevenueType implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "REVENUE_TYPE_ID", nullable = false)
    private Integer revenueTypeId;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "REVENUE_TYPE_NAME", nullable = false, length = 255)
    private String revenueTypeName;
    @Basic(optional = false)
    @Column(name = "REVENUE_TYPE_NAME_EN", nullable = false, length = 255)
    private String revenueTypeNameEn;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revenueTypeId", fetch = FetchType.LAZY)
    private List<CatalogStructureItem> catalogStructureItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revenueTypeId", fetch = FetchType.LAZY)
    private List<Catalog> catalogList;

    public MRevenueType() {
    }

    public MRevenueType(Integer revenueTypeId) {
        this.revenueTypeId = revenueTypeId;
    }

    public MRevenueType(Integer revenueTypeId, String revenueTypeName, String revenueTypeNameEn, Date startDate) {
        this.revenueTypeId = revenueTypeId;
        this.revenueTypeName = revenueTypeName;
        this.revenueTypeNameEn = revenueTypeNameEn;
        this.startDate = startDate;
    }

    public Integer getRevenueTypeId() {
        return revenueTypeId;
    }

    public void setRevenueTypeId(Integer revenueTypeId) {
        this.revenueTypeId = revenueTypeId;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getRevenueTypeName() {
        return revenueTypeName;
    }

    public void setRevenueTypeName(String revenueTypeName) {
        this.revenueTypeName = revenueTypeName;
    }

    public String getRevenueTypeNameEn() {
        return revenueTypeNameEn;
    }

    public void setRevenueTypeNameEn(String revenueTypeNameEn) {
        this.revenueTypeNameEn = revenueTypeNameEn;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<CatalogStructureItem> getCatalogStructureItemList() {
        return catalogStructureItemList;
    }

    public void setCatalogStructureItemList(List<CatalogStructureItem> catalogStructureItemList) {
        this.catalogStructureItemList = catalogStructureItemList;
    }

    @XmlTransient
    public List<Catalog> getCatalogList() {
        return catalogList;
    }

    public void setCatalogList(List<Catalog> catalogList) {
        this.catalogList = catalogList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (revenueTypeId != null ? revenueTypeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MRevenueType)) {
            return false;
        }
        MRevenueType other = (MRevenueType) object;
        if ((this.revenueTypeId == null && other.revenueTypeId != null) || (this.revenueTypeId != null && !this.revenueTypeId.equals(other.revenueTypeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MRevenueType[ revenueTypeId=" + revenueTypeId + " ]";
    }
    
}
