/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_STANDARD_REVERNUE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MStandardRevernue.findAll", query = "SELECT m FROM MStandardRevernue m"),
    @NamedQuery(name = "MStandardRevernue.findByStandardRevernueId", query = "SELECT m FROM MStandardRevernue m WHERE m.standardRevernueId = :standardRevernueId"),
    @NamedQuery(name = "MStandardRevernue.findByCreatedDate", query = "SELECT m FROM MStandardRevernue m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MStandardRevernue.findByEndDate", query = "SELECT m FROM MStandardRevernue m WHERE m.endDate = :endDate"),
    @NamedQuery(name = "MStandardRevernue.findByStandardRevernueName", query = "SELECT m FROM MStandardRevernue m WHERE m.standardRevernueName = :standardRevernueName"),
    @NamedQuery(name = "MStandardRevernue.findByStartDate", query = "SELECT m FROM MStandardRevernue m WHERE m.startDate = :startDate"),
    @NamedQuery(name = "MStandardRevernue.findByStatus", query = "SELECT m FROM MStandardRevernue m WHERE m.status = :status"),
    @NamedQuery(name = "MStandardRevernue.findByUpdatedDate", query = "SELECT m FROM MStandardRevernue m WHERE m.updatedDate = :updatedDate"),
    @NamedQuery(name = "MStandardRevernue.findByStandardRevernueDesc", query = "SELECT m FROM MStandardRevernue m WHERE m.standardRevernueDesc = :standardRevernueDesc")})
public class MStandardRevernue implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "STANDARD_REVERNUE_ID", nullable = false)
    private Integer standardRevernueId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Basic(optional = false)
    @Column(name = "STANDARD_REVERNUE_NAME", nullable = false, length = 255)
    private String standardRevernueName;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "STATUS")
    private String status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "STANDARD_REVERNUE_DESC", length = 1024)
    private String standardRevernueDesc;
    @OneToMany(mappedBy = "standardRevernueId", fetch = FetchType.LAZY)
    private List<MStandardRevernueGl> mStandardRevernueGlList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "MONEY_SOURCE_ID", referencedColumnName = "MONEY_SOURCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MMoneySource moneySourceId;
    @Column(name = "STANDARD_REVERNUE_CODE", length = 20)
    private String standardRevernueCode;

    public MStandardRevernue() {
    }

    public MStandardRevernue(Integer standardRevernueId) {
        this.standardRevernueId = standardRevernueId;
    }

    public MStandardRevernue(Integer standardRevernueId, String standardRevernueName, Date startDate) {
        this.standardRevernueId = standardRevernueId;
        this.standardRevernueName = standardRevernueName;
        this.startDate = startDate;
    }

    public Integer getStandardRevernueId() {
        return standardRevernueId;
    }

    public void setStandardRevernueId(Integer standardRevernueId) {
        this.standardRevernueId = standardRevernueId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getStandardRevernueName() {
        return standardRevernueName;
    }

    public void setStandardRevernueName(String standardRevernueName) {
        this.standardRevernueName = standardRevernueName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getStandardRevernueDesc() {
        return standardRevernueDesc;
    }

    public void setStandardRevernueDesc(String standardRevernueDesc) {
        this.standardRevernueDesc = standardRevernueDesc;
    }

    @XmlTransient
    public List<MStandardRevernueGl> getMStandardRevernueGlList() {
        return mStandardRevernueGlList;
    }

    public void setMStandardRevernueGlList(List<MStandardRevernueGl> mStandardRevernueGlList) {
        this.mStandardRevernueGlList = mStandardRevernueGlList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MMoneySource getMoneySourceId() {
        return moneySourceId;
    }

    public void setMoneySourceId(MMoneySource moneySourceId) {
        this.moneySourceId = moneySourceId;
    }
    public String getStandardRevernueCode() {
        return standardRevernueCode;
    }

    public void setStandardRevernueCode(String standardRevernueCode) {
        this.standardRevernueCode = standardRevernueCode;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (standardRevernueId != null ? standardRevernueId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MStandardRevernue)) {
            return false;
        }
        MStandardRevernue other = (MStandardRevernue) object;
        if ((this.standardRevernueId == null && other.standardRevernueId != null) || (this.standardRevernueId != null && !this.standardRevernueId.equals(other.standardRevernueId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MStandardRevernue[ standardRevernueId=" + standardRevernueId + " ]";
    }
    
}
