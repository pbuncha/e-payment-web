/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_STANDARD_REVERNUE_GL", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MStandardRevernueGl.findAll", query = "SELECT m FROM MStandardRevernueGl m"),
    @NamedQuery(name = "MStandardRevernueGl.findByStandardRevernueGlId", query = "SELECT m FROM MStandardRevernueGl m WHERE m.standardRevernueGlId = :standardRevernueGlId"),
    @NamedQuery(name = "MStandardRevernueGl.findByOrder", query = "SELECT m FROM MStandardRevernueGl m WHERE m.order = :order"),
    @NamedQuery(name = "MStandardRevernueGl.findByCreatedBy", query = "SELECT m FROM MStandardRevernueGl m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MStandardRevernueGl.findByCreatedDate", query = "SELECT m FROM MStandardRevernueGl m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MStandardRevernueGl.findByUpdatedDate", query = "SELECT m FROM MStandardRevernueGl m WHERE m.updatedDate = :updatedDate"),
    @NamedQuery(name = "MStandardRevernueGl.findByUpdatedBy", query = "SELECT m FROM MStandardRevernueGl m WHERE m.updatedBy = :updatedBy")})
public class MStandardRevernueGl implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "STANDARD_REVERNUE_GL_ID", nullable = false)
    private Integer standardRevernueGlId;
    @Column(name = "ORDER")
    private Integer order;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false)
    private int createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @JoinColumn(name = "STANDARD_REVERNUE_ID", referencedColumnName = "STANDARD_REVERNUE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MStandardRevernue standardRevernueId;
    
    @Column(name = "GL_FINISH_ID")
    private Long glFinishId;

    @Column(name = "GL_START_ID")
    private Long glStartId;

    @Column(name = "STATUS")
    private String status;

    public MStandardRevernueGl() {
    }

    public MStandardRevernueGl(Integer standardRevernueGlId) {
        this.standardRevernueGlId = standardRevernueGlId;
    }

    public MStandardRevernueGl(Integer standardRevernueGlId, int createdBy, Date createdDate) {
        this.standardRevernueGlId = standardRevernueGlId;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
    }

    public Integer getStandardRevernueGlId() {
        return standardRevernueGlId;
    }

    public void setStandardRevernueGlId(Integer standardRevernueGlId) {
        this.standardRevernueGlId = standardRevernueGlId;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public MStandardRevernue getStandardRevernueId() {
        return standardRevernueId;
    }

    public void setStandardRevernueId(MStandardRevernue standardRevernueId) {
        this.standardRevernueId = standardRevernueId;
    }

    public Long getGlFinishId() {
        return glFinishId;
    }

    public void setGlFinishId(Long glFinishId) {
        this.glFinishId = glFinishId;
    }

    public Long getGlStartId() {
        return glStartId;
    }

    public void setGlStartId(Long glStartId) {
        this.glStartId = glStartId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (standardRevernueGlId != null ? standardRevernueGlId.hashCode() : 0);
        return hash;
    }

	@Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MStandardRevernueGl)) {
            return false;
        }
        MStandardRevernueGl other = (MStandardRevernueGl) object;
        if ((this.standardRevernueGlId == null && other.standardRevernueGlId != null) || (this.standardRevernueGlId != null && !this.standardRevernueGlId.equals(other.standardRevernueGlId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MStandardRevernueGl[ standardRevernueGlId=" + standardRevernueGlId + " ]";
    }
    
}
