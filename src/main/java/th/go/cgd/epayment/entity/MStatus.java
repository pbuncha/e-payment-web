/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_STATUS", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MStatus.findAll", query = "SELECT m FROM MStatus m"),
    @NamedQuery(name = "MStatus.findByStatus", query = "SELECT m FROM MStatus m WHERE m.status = :status"),
    @NamedQuery(name = "MStatus.findByStatusName", query = "SELECT m FROM MStatus m WHERE m.statusName = :statusName")})
public class MStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false, length = 5)
    private String status;
    @Basic(optional = false)
    @Column(name = "STATUS_NAME", nullable = false, length = 50)
    private String statusName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status", fetch = FetchType.LAZY)
    private List<MDistrict> mDistrictList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status", fetch = FetchType.LAZY)
    private List<PermissionGroup> permissionGroupList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "status", fetch = FetchType.LAZY)
    private List<MSubdistrict> mSubdistrictList;


    public MStatus() {
    }

    public MStatus(String status) {
        this.status = status;
    }

    public MStatus(String status, String statusName) {
        this.status = status;
        this.statusName = statusName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @XmlTransient
    public List<MDistrict> getMDistrictList() {
        return mDistrictList;
    }

    public void setMDistrictList(List<MDistrict> mDistrictList) {
        this.mDistrictList = mDistrictList;
    }

    @XmlTransient
    public List<PermissionGroup> getPermissionGroupList() {
        return permissionGroupList;
    }

    public void setPermissionGroupList(List<PermissionGroup> permissionGroupList) {
        this.permissionGroupList = permissionGroupList;
    }

    @XmlTransient
    public List<MSubdistrict> getMSubdistrictList() {
        return mSubdistrictList;
    }

    public void setMSubdistrictList(List<MSubdistrict> mSubdistrictList) {
        this.mSubdistrictList = mSubdistrictList;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (status != null ? status.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MStatus)) {
            return false;
        }
        MStatus other = (MStatus) object;
        if ((this.status == null && other.status != null) || (this.status != null && !this.status.equals(other.status))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MStatus[ status=" + status + " ]";
    }
    
}
