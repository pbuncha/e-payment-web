/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_SUBDISTRICT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MSubdistrict.findAll", query = "SELECT m FROM MSubdistrict m"),
    @NamedQuery(name = "MSubdistrict.findBySubdistrictId", query = "SELECT m FROM MSubdistrict m WHERE m.subdistrictId = :subdistrictId"),
    @NamedQuery(name = "MSubdistrict.findBySubdistrictName", query = "SELECT m FROM MSubdistrict m WHERE m.subdistrictName = :subdistrictName")})
public class MSubdistrict implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "SUBDISTRICT_ID", nullable = false)
    private Short subdistrictId;
    @Basic(optional = false)
    @Column(name = "SUBDISTRICT_NAME", nullable = false, length = 200)
    private String subdistrictName;
    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MStatus status;
    @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "DISTRICT_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MDistrict districtId;

    public MSubdistrict() {
    }

    public MSubdistrict(Short subdistrictId) {
        this.subdistrictId = subdistrictId;
    }

    public MSubdistrict(Short subdistrictId, String subdistrictName) {
        this.subdistrictId = subdistrictId;
        this.subdistrictName = subdistrictName;
    }

    public Short getSubdistrictId() {
        return subdistrictId;
    }

    public void setSubdistrictId(Short subdistrictId) {
        this.subdistrictId = subdistrictId;
    }

    public String getSubdistrictName() {
        return subdistrictName;
    }

    public void setSubdistrictName(String subdistrictName) {
        this.subdistrictName = subdistrictName;
    }

    public MStatus getStatus() {
        return status;
    }

    public void setStatus(MStatus status) {
        this.status = status;
    }

    public MDistrict getDistrictId() {
        return districtId;
    }

    public void setDistrictId(MDistrict districtId) {
        this.districtId = districtId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (subdistrictId != null ? subdistrictId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MSubdistrict)) {
            return false;
        }
        MSubdistrict other = (MSubdistrict) object;
        if ((this.subdistrictId == null && other.subdistrictId != null) || (this.subdistrictId != null && !this.subdistrictId.equals(other.subdistrictId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MSubdistrict[ subdistrictId=" + subdistrictId + " ]";
    }
    
}
