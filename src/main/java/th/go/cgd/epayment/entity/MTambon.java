/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_TAMBON", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MTambon.findAll", query = "SELECT m FROM MTambon m"),
    @NamedQuery(name = "MTambon.findByTambonId", query = "SELECT m FROM MTambon m WHERE m.tambonId = :tambonId"),
    @NamedQuery(name = "MTambon.findByCreatedBy", query = "SELECT m FROM MTambon m WHERE m.createdBy = :createdBy"),
    @NamedQuery(name = "MTambon.findByCreatedDate", query = "SELECT m FROM MTambon m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MTambon.findByTambonNameEn", query = "SELECT m FROM MTambon m WHERE m.tambonNameEn = :tambonNameEn"),
    @NamedQuery(name = "MTambon.findByTambonNameTh", query = "SELECT m FROM MTambon m WHERE m.tambonNameTh = :tambonNameTh"),
    @NamedQuery(name = "MTambon.findByUpdatedBy", query = "SELECT m FROM MTambon m WHERE m.updatedBy = :updatedBy"),
    @NamedQuery(name = "MTambon.findByUpdatedDate", query = "SELECT m FROM MTambon m WHERE m.updatedDate = :updatedDate")})
public class MTambon implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TAMBON_ID", nullable = false)
    private Integer tambonId;
    @Basic(optional = false)
    @Column(name = "CREATED_BY", nullable = false)
    private int createdBy;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "TAMBON_NAME_EN", length = 255)
    private String tambonNameEn;
    @Basic(optional = false)
    @Column(name = "TAMBON_NAME_TH", nullable = false, length = 255)
    private String tambonNameTh;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(mappedBy = "tambonId", fetch = FetchType.LAZY)
    private List<Address> addressList;
    @JoinColumn(name = "AMPHUR_ID", referencedColumnName = "AMPHUR_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MAmphur amphurId;

    public MTambon() {
    }

    public MTambon(Integer tambonId) {
        this.tambonId = tambonId;
    }

    public MTambon(Integer tambonId, int createdBy, Date createdDate, String tambonNameTh) {
        this.tambonId = tambonId;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.tambonNameTh = tambonNameTh;
    }

    public Integer getTambonId() {
        return tambonId;
    }

    public void setTambonId(Integer tambonId) {
        this.tambonId = tambonId;
    }

    public int getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(int createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getTambonNameEn() {
        return tambonNameEn;
    }

    public void setTambonNameEn(String tambonNameEn) {
        this.tambonNameEn = tambonNameEn;
    }

    public String getTambonNameTh() {
        return tambonNameTh;
    }

    public void setTambonNameTh(String tambonNameTh) {
        this.tambonNameTh = tambonNameTh;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    public MAmphur getAmphurId() {
        return amphurId;
    }

    public void setAmphurId(MAmphur amphurId) {
        this.amphurId = amphurId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tambonId != null ? tambonId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MTambon)) {
            return false;
        }
        MTambon other = (MTambon) object;
        if ((this.tambonId == null && other.tambonId != null) || (this.tambonId != null && !this.tambonId.equals(other.tambonId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MTambon[ tambonId=" + tambonId + " ]";
    }
    
}
