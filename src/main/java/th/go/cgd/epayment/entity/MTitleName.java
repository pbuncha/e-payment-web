/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_TITLE_NAME", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MTitleName.findAll", query = "SELECT m FROM MTitleName m"),
    @NamedQuery(name = "MTitleName.findByTitleId", query = "SELECT m FROM MTitleName m WHERE m.titleId = :titleId"),
    @NamedQuery(name = "MTitleName.findByTitleName", query = "SELECT m FROM MTitleName m WHERE m.titleName = :titleName"),
    @NamedQuery(name = "MTitleName.findByStatus", query = "SELECT m FROM MTitleName m WHERE m.status = :status"),
    @NamedQuery(name = "MTitleName.findByCreatedDate", query = "SELECT m FROM MTitleName m WHERE m.createdDate = :createdDate"),
    @NamedQuery(name = "MTitleName.findByUpdatedDate", query = "SELECT m FROM MTitleName m WHERE m.updatedDate = :updatedDate")})
public class MTitleName implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "TITLE_ID", nullable = false)
    private Integer titleId;
    @Basic(optional = false)
    @Column(name = "TITLE_NAME", nullable = false, length = 100)
    private String titleName;
    @Column(name = "TITLE_NAME_EN", length = 100)
    private String titleNameEn;
    @Column(name = "STATUS", length = 1)
    private String status;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @OneToMany(mappedBy = "titleId", fetch = FetchType.LAZY)
    private List<UserCenter> userCenterList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "titleId", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;

    public MTitleName() {
    }

    public MTitleName(Integer titleId) {
        this.titleId = titleId;
    }

    public MTitleName(Integer titleId, String titleName) {
        this.titleId = titleId;
        this.titleName = titleName;
    }

    public Integer getTitleId() {
        return titleId;
    }

    public void setTitleId(Integer titleId) {
        this.titleId = titleId;
    }

    public String getTitleName() {
        return titleName;
    }

    public void setTitleName(String titleName) {
        this.titleName = titleName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @XmlTransient
    public List<UserCenter> getUserCenterList() {
        return userCenterList;
    }

    public void setUserCenterList(List<UserCenter> userCenterList) {
        this.userCenterList = userCenterList;
    }

    @XmlTransient
    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public String getTitleNameEn() {
        return titleNameEn;
    }

    public void setTitleNameEn(String titleNameEn) {
        this.titleNameEn = titleNameEn;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (titleId != null ? titleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MTitleName)) {
            return false;
        }
        MTitleName other = (MTitleName) object;
        if ((this.titleId == null && other.titleId != null) || (this.titleId != null && !this.titleId.equals(other.titleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MTitleName[ titleId=" + titleId + " ]";
    }
    
}
