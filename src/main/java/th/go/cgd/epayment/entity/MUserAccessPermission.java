/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_USER_ACCESS_PERMISSION", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MUserAccessPermission.findAll", query = "SELECT m FROM MUserAccessPermission m"),
    @NamedQuery(name = "MUserAccessPermission.findByAccessPermissionId", query = "SELECT m FROM MUserAccessPermission m WHERE m.accessPermissionId = :accessPermissionId"),
    @NamedQuery(name = "MUserAccessPermission.findByAccessPermissionName", query = "SELECT m FROM MUserAccessPermission m WHERE m.accessPermissionName = :accessPermissionName")})
public class MUserAccessPermission implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ACCESS_PERMISSION_ID", nullable = false)
    private Integer accessPermissionId;
    @Basic(optional = false)
    @Column(name = "ACCESS_PERMISSION_NAME", nullable = false, length = 100)
    private String accessPermissionName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "accessPermissionId", fetch = FetchType.LAZY)
    private List<UserAccessPermission> userAccessPermissionList;

    public MUserAccessPermission() {
    }

    public MUserAccessPermission(Integer accessPermissionId) {
        this.accessPermissionId = accessPermissionId;
    }

    public MUserAccessPermission(Integer accessPermissionId, String accessPermissionName) {
        this.accessPermissionId = accessPermissionId;
        this.accessPermissionName = accessPermissionName;
    }

    public Integer getAccessPermissionId() {
        return accessPermissionId;
    }

    public void setAccessPermissionId(Integer accessPermissionId) {
        this.accessPermissionId = accessPermissionId;
    }

    public String getAccessPermissionName() {
        return accessPermissionName;
    }

    public void setAccessPermissionName(String accessPermissionName) {
        this.accessPermissionName = accessPermissionName;
    }

    @XmlTransient
    public List<UserAccessPermission> getUserAccessPermissionList() {
        return userAccessPermissionList;
    }

    public void setUserAccessPermissionList(List<UserAccessPermission> userAccessPermissionList) {
        this.userAccessPermissionList = userAccessPermissionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (accessPermissionId != null ? accessPermissionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MUserAccessPermission)) {
            return false;
        }
        MUserAccessPermission other = (MUserAccessPermission) object;
        if ((this.accessPermissionId == null && other.accessPermissionId != null) || (this.accessPermissionId != null && !this.accessPermissionId.equals(other.accessPermissionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MUserAccessPermission[ accessPermissionId=" + accessPermissionId + " ]";
    }
    
}
