/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_USER_PERMISSIONGROUP", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MUserPermissiongroup.findAll", query = "SELECT m FROM MUserPermissiongroup m"),
    @NamedQuery(name = "MUserPermissiongroup.findByUserId", query = "SELECT m FROM MUserPermissiongroup m WHERE m.userId = :userId"),
    @NamedQuery(name = "MUserPermissiongroup.findByUpdateBy", query = "SELECT m FROM MUserPermissiongroup m WHERE m.updateBy = :updateBy"),
    @NamedQuery(name = "MUserPermissiongroup.findByUpdateDate", query = "SELECT m FROM MUserPermissiongroup m WHERE m.updateDate = :updateDate"),
    @NamedQuery(name = "MUserPermissiongroup.findByCreateBy", query = "SELECT m FROM MUserPermissiongroup m WHERE m.createBy = :createBy"),
    @NamedQuery(name = "MUserPermissiongroup.findByCreateDate", query = "SELECT m FROM MUserPermissiongroup m WHERE m.createDate = :createDate"),
    @NamedQuery(name = "MUserPermissiongroup.findByStatus", query = "SELECT m FROM MUserPermissiongroup m WHERE m.status = :status"),
    @NamedQuery(name = "MUserPermissiongroup.findByMUserPermissiongroupId", query = "SELECT m FROM MUserPermissiongroup m WHERE m.mUserPermissiongroupId = :mUserPermissiongroupId")})
public class MUserPermissiongroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User userId;
    @Basic(optional = false)
    @Column(name = "UPDATE_BY", nullable = false)
    private int updateBy;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Basic(optional = false)
    @Column(name = "CREATE_BY", nullable = false)
    private int createBy;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false, length = 5)
    private String status;
    @Basic(optional = false)
    @Column(name = "ATTACH_FILE", nullable = true, length = 200)
    private String attachFile;
    @Basic(optional = false)
    @Column(name = "REQUEST_TYPE", nullable = true, length = 5)
    private String requestType;
    @Column(name = "COMMENT", length = 512)
    private String comment;
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "M_USER_PERMISSIONGROUP_ID", nullable = false)
    private Integer mUserPermissiongroupId;
    @JoinColumn(name = "M_PERMISSION_GROUP_ID", referencedColumnName = "M_PERMISSION_GROUP_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private MPermissionGroup mPermissionGroupId;

    public MUserPermissiongroup() {
    }

    public MUserPermissiongroup(Integer mUserPermissiongroupId) {
        this.mUserPermissiongroupId = mUserPermissiongroupId;
    }

    public MUserPermissiongroup(Integer mUserPermissiongroupId,  int updateBy, Date updateDate, int createBy, Date createDate, String status) {
        this.mUserPermissiongroupId = mUserPermissiongroupId;
        this.updateBy = updateBy;
        this.updateDate = updateDate;
        this.createBy = createBy;
        this.createDate = createDate;
        this.status = status;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public int getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(int updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public int getCreateBy() {
        return createBy;
    }

    public void setCreateBy(int createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getMUserPermissiongroupId() {
        return mUserPermissiongroupId;
    }

    public void setMUserPermissiongroupId(Integer mUserPermissiongroupId) {
        this.mUserPermissiongroupId = mUserPermissiongroupId;
    }

    public MPermissionGroup getMPermissionGroupId() {
        return mPermissionGroupId;
    }

    public void setMPermissionGroupId(MPermissionGroup mPermissionGroupId) {
        this.mPermissionGroupId = mPermissionGroupId;
    }
    
    public String getAttachFile() {
		return attachFile;
	}

	public void setAttachFile(String attachFile) {
		this.attachFile = attachFile;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
	public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (mUserPermissiongroupId != null ? mUserPermissiongroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MUserPermissiongroup)) {
            return false;
        }
        MUserPermissiongroup other = (MUserPermissiongroup) object;
        if ((this.mUserPermissiongroupId == null && other.mUserPermissiongroupId != null) || (this.mUserPermissiongroupId != null && !this.mUserPermissiongroupId.equals(other.mUserPermissiongroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MUserPermissiongroup[ mUserPermissiongroupId=" + mUserPermissiongroupId + " ]";
    }
    
}
