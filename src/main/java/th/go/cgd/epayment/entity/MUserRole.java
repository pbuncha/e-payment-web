/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_USER_ROLE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MUserRole.findAll", query = "SELECT m FROM MUserRole m"),
    @NamedQuery(name = "MUserRole.findByRoleId", query = "SELECT m FROM MUserRole m WHERE m.roleId = :roleId"),
    @NamedQuery(name = "MUserRole.findByRoleName", query = "SELECT m FROM MUserRole m WHERE m.roleName = :roleName")})
public class MUserRole implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ROLE_ID", nullable = false)
    private Short roleId;
    @Basic(optional = false)
    @Column(name = "ROLE_NAME", nullable = false, length = 50)
    private String roleName;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "mUserRole", fetch = FetchType.LAZY)
    private List<UserRole> userRoleList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "roleId", fetch = FetchType.LAZY)
    private List<UserAuthorization> userAuthorizationList;

    public MUserRole() {
    }

    public MUserRole(Short roleId) {
        this.roleId = roleId;
    }

    public MUserRole(Short roleId, String roleName) {
        this.roleId = roleId;
        this.roleName = roleName;
    }

    public Short getRoleId() {
        return roleId;
    }

    public void setRoleId(Short roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @XmlTransient
    public List<UserRole> getUserRoleList() {
        return userRoleList;
    }

    public void setUserRoleList(List<UserRole> userRoleList) {
        this.userRoleList = userRoleList;
    }

    @XmlTransient
    public List<UserAuthorization> getUserAuthorizationList() {
        return userAuthorizationList;
    }

    public void setUserAuthorizationList(List<UserAuthorization> userAuthorizationList) {
        this.userAuthorizationList = userAuthorizationList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (roleId != null ? roleId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MUserRole)) {
            return false;
        }
        MUserRole other = (MUserRole) object;
        if ((this.roleId == null && other.roleId != null) || (this.roleId != null && !this.roleId.equals(other.roleId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MUserRole[ roleId=" + roleId + " ]";
    }
    
}
