/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "M_USER_STATUS", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MUserStatus.findAll", query = "SELECT m FROM MUserStatus m"),
    @NamedQuery(name = "MUserStatus.findByStatus", query = "SELECT m FROM MUserStatus m WHERE m.status = :status"),
    @NamedQuery(name = "MUserStatus.findByStatusName", query = "SELECT m FROM MUserStatus m WHERE m.statusName = :statusName")})
public class MUserStatus implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false, length = 1)
    private String status;
    @Basic(optional = false)
    @Column(name = "STATUS_NAME", nullable = false, length = 20)
    private String statusName;

    public MUserStatus() {
    }

    public MUserStatus(String status) {
        this.status = status;
    }

    public MUserStatus(String status, String statusName) {
        this.status = status;
        this.statusName = statusName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (status != null ? status.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MUserStatus)) {
            return false;
        }
        MUserStatus other = (MUserStatus) object;
        if ((this.status == null && other.status != null) || (this.status != null && !this.status.equals(other.status))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.MUserStatus[ status=" + status + " ]";
    }
    
}
