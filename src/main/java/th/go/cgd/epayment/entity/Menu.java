/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "MENU", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m"),
    @NamedQuery(name = "Menu.findByMenuId", query = "SELECT m FROM Menu m WHERE m.menuId = :menuId"),
    @NamedQuery(name = "Menu.findByIsLastChild", query = "SELECT m FROM Menu m WHERE m.isLastChild = :isLastChild"),
    @NamedQuery(name = "Menu.findByIsUnderlined", query = "SELECT m FROM Menu m WHERE m.isUnderlined = :isUnderlined"),
    @NamedQuery(name = "Menu.findByLevelNo", query = "SELECT m FROM Menu m WHERE m.levelNo = :levelNo"),
    @NamedQuery(name = "Menu.findByMenuIcon", query = "SELECT m FROM Menu m WHERE m.menuIcon = :menuIcon"),
    @NamedQuery(name = "Menu.findByMenuName", query = "SELECT m FROM Menu m WHERE m.menuName = :menuName"),
    @NamedQuery(name = "Menu.findByOrderNo", query = "SELECT m FROM Menu m WHERE m.orderNo = :orderNo"),
    @NamedQuery(name = "Menu.findByStatus", query = "SELECT m FROM Menu m WHERE m.status = :status"),
    @NamedQuery(name = "Menu.findByUrl", query = "SELECT m FROM Menu m WHERE m.url = :url")})
public class Menu implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "MENU_ID", nullable = false)
    private Short menuId;
    @Column(name = "IS_LAST_CHILD", length = 1)
    private String isLastChild;
    @Column(name = "IS_UNDERLINED", length = 1)
    private String isUnderlined;
    @Basic(optional = false)
    @Column(name = "LEVEL_NO", nullable = false)
    private short levelNo;
    @Column(name = "MENU_ICON", length = 100)
    private String menuIcon;
    @Basic(optional = false)
    @Column(name = "MENU_NAME", nullable = false, length = 100)
    private String menuName;
    @Basic(optional = false)
    @Column(name = "ORDER_NO", nullable = false)
    private short orderNo;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false, length = 1)
    private String status;
    @Column(name = "URL", length = 100)
    private String url;
    @JoinTable(name = "MENU_PERMISSION", joinColumns = {
        @JoinColumn(name = "MENU_ID", referencedColumnName = "MENU_ID", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "PERMISSION_ITEM_ID", referencedColumnName = "PERMISSION_ITEM_ID", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<PermissionItem> permissionItemList;
    @OneToMany(mappedBy = "parentId", fetch = FetchType.LAZY)
    private List<Menu> menuList;
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "MENU_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Menu parentId;

    public Menu() {
    }

    public Menu(Short menuId) {
        this.menuId = menuId;
    }

    public Menu(Short menuId, short levelNo, String menuName, short orderNo, String status) {
        this.menuId = menuId;
        this.levelNo = levelNo;
        this.menuName = menuName;
        this.orderNo = orderNo;
        this.status = status;
    }

    public Short getMenuId() {
        return menuId;
    }

    public void setMenuId(Short menuId) {
        this.menuId = menuId;
    }

    public String getIsLastChild() {
        return isLastChild;
    }

    public void setIsLastChild(String isLastChild) {
        this.isLastChild = isLastChild;
    }

    public String getIsUnderlined() {
        return isUnderlined;
    }

    public void setIsUnderlined(String isUnderlined) {
        this.isUnderlined = isUnderlined;
    }

    public short getLevelNo() {
        return levelNo;
    }

    public void setLevelNo(short levelNo) {
        this.levelNo = levelNo;
    }

    public String getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(String menuIcon) {
        this.menuIcon = menuIcon;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public short getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(short orderNo) {
        this.orderNo = orderNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @XmlTransient
    public List<PermissionItem> getPermissionItemList() {
        return permissionItemList;
    }

    public void setPermissionItemList(List<PermissionItem> permissionItemList) {
        this.permissionItemList = permissionItemList;
    }

    @XmlTransient
    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public Menu getParentId() {
        return parentId;
    }

    public void setParentId(Menu parentId) {
        this.parentId = parentId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menuId != null ? menuId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.menuId == null && other.menuId != null) || (this.menuId != null && !this.menuId.equals(other.menuId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.Menu[ menuId=" + menuId + " ]";
    }
    
}
