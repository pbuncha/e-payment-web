/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "ORGANIZATION", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Organization.findAll", query = "SELECT o FROM Organization o"),
    @NamedQuery(name = "Organization.findByOrgId", query = "SELECT o FROM Organization o WHERE o.orgId = :orgId"),
    @NamedQuery(name = "Organization.findByAddress", query = "SELECT o FROM Organization o WHERE o.address = :address"),
    @NamedQuery(name = "Organization.findByCreateDate", query = "SELECT o FROM Organization o WHERE o.createDate = :createDate"),
    @NamedQuery(name = "Organization.findByGfmisBudget", query = "SELECT o FROM Organization o WHERE o.gfmisBudget = :gfmisBudget"),
    @NamedQuery(name = "Organization.findByGfmisCode", query = "SELECT o FROM Organization o WHERE o.gfmisCode = :gfmisCode"),
    @NamedQuery(name = "Organization.findByMoiId", query = "SELECT o FROM Organization o WHERE o.moiId = :moiId"),
    @NamedQuery(name = "Organization.findByOrgCode", query = "SELECT o FROM Organization o WHERE o.orgCode = :orgCode"),
    @NamedQuery(name = "Organization.findByOrgName", query = "SELECT o FROM Organization o WHERE o.orgName = :orgName"),
    @NamedQuery(name = "Organization.findByUpdateDate", query = "SELECT o FROM Organization o WHERE o.updateDate = :updateDate"),
    @NamedQuery(name = "Organization.findByZipCode", query = "SELECT o FROM Organization o WHERE o.zipCode = :zipCode")})
public class Organization implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ORG_ID", nullable = false)
    private Integer orgId;
    @Column(name = "ADDRESS", length = 300)
    private String address;
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "GFMIS_BUDGET", length = 50)
    private String gfmisBudget;
    @Column(name = "GFMIS_CODE", length = 50)
    private String gfmisCode;
    @Column(name = "MOI_ID", length = 13)
    private String moiId;
    @Column(name = "ORG_CODE", length = 20)
    private String orgCode;
    @Basic(optional = false)
    @Column(name = "ORG_NAME", nullable = false, length = 300)
    private String orgName;
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @Column(name = "ZIP_CODE", length = 5)
    private String zipCode;
    @OneToMany(mappedBy = "orgId", fetch = FetchType.LAZY)
    private List<CommonPerson> commonPersonList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "orgId", fetch = FetchType.LAZY)
    private List<UserInfo> userInfoList;
    @JoinColumn(name = "CREATE_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createBy;
    @JoinColumn(name = "UPDATE_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updateBy;
    @OneToMany(mappedBy = "parentId", fetch = FetchType.LAZY)
    private List<Organization> organizationList;
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "ORG_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Organization parentId;
    @JoinColumn(name = "SUB_DISTRICT_ID", referencedColumnName = "SUBDISTRICT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MSubdistrict subDistrictId;
    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MStatus status;
    @JoinColumn(name = "PROVINCE_ID", referencedColumnName = "PROVINCE_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MProvince provinceId;
    @JoinColumn(name = "ORG_TYPE_ID", referencedColumnName = "ORG_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MOrgType orgTypeId;
    @JoinColumn(name = "DISTRICT_ID", referencedColumnName = "DISTRICT_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private MDistrict districtId;

    public Organization() {
    }

    public Organization(Integer orgId) {
        this.orgId = orgId;
    }

    public Organization(Integer orgId, String orgName) {
        this.orgId = orgId;
        this.orgName = orgName;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getGfmisBudget() {
        return gfmisBudget;
    }

    public void setGfmisBudget(String gfmisBudget) {
        this.gfmisBudget = gfmisBudget;
    }

    public String getGfmisCode() {
        return gfmisCode;
    }

    public void setGfmisCode(String gfmisCode) {
        this.gfmisCode = gfmisCode;
    }

    public String getMoiId() {
        return moiId;
    }

    public void setMoiId(String moiId) {
        this.moiId = moiId;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @XmlTransient
    public List<CommonPerson> getCommonPersonList() {
        return commonPersonList;
    }

    public void setCommonPersonList(List<CommonPerson> commonPersonList) {
        this.commonPersonList = commonPersonList;
    }

    @XmlTransient
    public List<UserInfo> getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(List<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    @XmlTransient
    public List<Organization> getOrganizationList() {
        return organizationList;
    }

    public void setOrganizationList(List<Organization> organizationList) {
        this.organizationList = organizationList;
    }

    public Organization getParentId() {
        return parentId;
    }

    public void setParentId(Organization parentId) {
        this.parentId = parentId;
    }

    public MSubdistrict getSubDistrictId() {
        return subDistrictId;
    }

    public void setSubDistrictId(MSubdistrict subDistrictId) {
        this.subDistrictId = subDistrictId;
    }

    public MStatus getStatus() {
        return status;
    }

    public void setStatus(MStatus status) {
        this.status = status;
    }

    public MProvince getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(MProvince provinceId) {
        this.provinceId = provinceId;
    }

    public MOrgType getOrgTypeId() {
        return orgTypeId;
    }

    public void setOrgTypeId(MOrgType orgTypeId) {
        this.orgTypeId = orgTypeId;
    }

    public MDistrict getDistrictId() {
        return districtId;
    }

    public void setDistrictId(MDistrict districtId) {
        this.districtId = districtId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (orgId != null ? orgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Organization)) {
            return false;
        }
        Organization other = (Organization) object;
        if ((this.orgId == null && other.orgId != null) || (this.orgId != null && !this.orgId.equals(other.orgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.Organization[ orgId=" + orgId + " ]";
    }
    
}
