/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "PERMISSION_GROUP", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PermissionGroup.findAll", query = "SELECT p FROM PermissionGroup p"),
    @NamedQuery(name = "PermissionGroup.findByPermissionGroupId", query = "SELECT p FROM PermissionGroup p WHERE p.permissionGroupId = :permissionGroupId"),
    @NamedQuery(name = "PermissionGroup.findByCreateDate", query = "SELECT p FROM PermissionGroup p WHERE p.createDate = :createDate"),
    @NamedQuery(name = "PermissionGroup.findByDescription", query = "SELECT p FROM PermissionGroup p WHERE p.description = :description"),
    @NamedQuery(name = "PermissionGroup.findByPermissionGroupName", query = "SELECT p FROM PermissionGroup p WHERE p.permissionGroupName = :permissionGroupName"),
    @NamedQuery(name = "PermissionGroup.findByUpdateDate", query = "SELECT p FROM PermissionGroup p WHERE p.updateDate = :updateDate")})
public class PermissionGroup implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_ID", nullable = false)
    private Short permissionGroupId;
    @Basic(optional = false)
    @Column(name = "CREATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createDate;
    @Column(name = "DESCRIPTION", length = 1000)
    private String description;
    @Basic(optional = false)
    @Column(name = "PERMISSION_GROUP_NAME", nullable = false, length = 200)
    private String permissionGroupName;
    @Basic(optional = false)
    @Column(name = "UPDATE_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date updateDate;
    @JoinTable(name = "PERMISSION_GROUP_ITEM", joinColumns = {
        @JoinColumn(name = "PERMISSION_GROUP_ID", referencedColumnName = "PERMISSION_GROUP_ID", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "PERMISSION_ITEM_ID", referencedColumnName = "PERMISSION_ITEM_ID", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<PermissionItem> permissionItemList;
    @JoinTable(name = "USER_GROUP_PERMISSION", joinColumns = {
        @JoinColumn(name = "GROUP_PERMISSION_ID", referencedColumnName = "PERMISSION_GROUP_ID", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> userList;
    @JoinColumn(name = "UPDATE_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User updateBy;
    @JoinColumn(name = "CREATE_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createBy;
    @JoinColumn(name = "STATUS", referencedColumnName = "STATUS", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MStatus status;

    public PermissionGroup() {
    }

    public PermissionGroup(Short permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public PermissionGroup(Short permissionGroupId, Date createDate, String permissionGroupName, Date updateDate) {
        this.permissionGroupId = permissionGroupId;
        this.createDate = createDate;
        this.permissionGroupName = permissionGroupName;
        this.updateDate = updateDate;
    }

    public Short getPermissionGroupId() {
        return permissionGroupId;
    }

    public void setPermissionGroupId(Short permissionGroupId) {
        this.permissionGroupId = permissionGroupId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPermissionGroupName() {
        return permissionGroupName;
    }

    public void setPermissionGroupName(String permissionGroupName) {
        this.permissionGroupName = permissionGroupName;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    @XmlTransient
    public List<PermissionItem> getPermissionItemList() {
        return permissionItemList;
    }

    public void setPermissionItemList(List<PermissionItem> permissionItemList) {
        this.permissionItemList = permissionItemList;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public User getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(User updateBy) {
        this.updateBy = updateBy;
    }

    public User getCreateBy() {
        return createBy;
    }

    public void setCreateBy(User createBy) {
        this.createBy = createBy;
    }

    public MStatus getStatus() {
        return status;
    }

    public void setStatus(MStatus status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionGroupId != null ? permissionGroupId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionGroup)) {
            return false;
        }
        PermissionGroup other = (PermissionGroup) object;
        if ((this.permissionGroupId == null && other.permissionGroupId != null) || (this.permissionGroupId != null && !this.permissionGroupId.equals(other.permissionGroupId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.PermissionGroup[ permissionGroupId=" + permissionGroupId + " ]";
    }
    
}
