/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "PERMISSION_ITEM", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PermissionItem.findAll", query = "SELECT p FROM PermissionItem p"),
    @NamedQuery(name = "PermissionItem.findByPermissionItemId", query = "SELECT p FROM PermissionItem p WHERE p.permissionItemId = :permissionItemId"),
    @NamedQuery(name = "PermissionItem.findByPermissionItemName", query = "SELECT p FROM PermissionItem p WHERE p.permissionItemName = :permissionItemName")})
public class PermissionItem implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "PERMISSION_ITEM_ID", nullable = false)
    private Short permissionItemId;
    @Basic(optional = false)
    @Column(name = "PERMISSION_ITEM_NAME", nullable = false, length = 200)
    private String permissionItemName;
    @JoinTable(name = "USER_PERMISSION_ITEM", joinColumns = {
        @JoinColumn(name = "PERMISSION_ITEM_ID", referencedColumnName = "PERMISSION_ITEM_ID", nullable = false)}, inverseJoinColumns = {
        @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)})
    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> userList;
    @ManyToMany(mappedBy = "permissionItemList", fetch = FetchType.LAZY)
    private List<PermissionGroup> permissionGroupList;
    @ManyToMany(mappedBy = "permissionItemList", fetch = FetchType.LAZY)
    private List<Menu> menuList;
    @JoinColumn(name = "PERMISSION_ID", referencedColumnName = "PERMISSION_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Permission permissionId;

    public PermissionItem() {
    }

    public PermissionItem(Short permissionItemId) {
        this.permissionItemId = permissionItemId;
    }

    public PermissionItem(Short permissionItemId, String permissionItemName) {
        this.permissionItemId = permissionItemId;
        this.permissionItemName = permissionItemName;
    }

    public Short getPermissionItemId() {
        return permissionItemId;
    }

    public void setPermissionItemId(Short permissionItemId) {
        this.permissionItemId = permissionItemId;
    }

    public String getPermissionItemName() {
        return permissionItemName;
    }

    public void setPermissionItemName(String permissionItemName) {
        this.permissionItemName = permissionItemName;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    @XmlTransient
    public List<PermissionGroup> getPermissionGroupList() {
        return permissionGroupList;
    }

    public void setPermissionGroupList(List<PermissionGroup> permissionGroupList) {
        this.permissionGroupList = permissionGroupList;
    }

    @XmlTransient
    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public Permission getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Permission permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionItemId != null ? permissionItemId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionItem)) {
            return false;
        }
        PermissionItem other = (PermissionItem) object;
        if ((this.permissionItemId == null && other.permissionItemId != null) || (this.permissionItemId != null && !this.permissionItemId.equals(other.permissionItemId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.PermissionItem[ permissionItemId=" + permissionItemId + " ]";
    }
    
}
