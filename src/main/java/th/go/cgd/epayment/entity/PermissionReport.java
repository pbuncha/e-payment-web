/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "PERMISSION_REPORT", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PermissionReport.findAll", query = "SELECT p FROM PermissionReport p"),
    @NamedQuery(name = "PermissionReport.findByPermissionId", query = "SELECT p FROM PermissionReport p WHERE p.permissionReportPK.permissionId = :permissionId"),
    @NamedQuery(name = "PermissionReport.findByReportId", query = "SELECT p FROM PermissionReport p WHERE p.permissionReportPK.reportId = :reportId")})
public class PermissionReport implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected PermissionReportPK permissionReportPK;
    @JoinColumn(name = "PERMISSION_ID", referencedColumnName = "PERMISSION_ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Permission permission;

    public PermissionReport() {
    }

    public PermissionReport(PermissionReportPK permissionReportPK) {
        this.permissionReportPK = permissionReportPK;
    }

    public PermissionReport(Permission permissionId, MReport reportId) {
        this.permissionReportPK = new PermissionReportPK(permissionId, reportId);
    }

    public PermissionReportPK getPermissionReportPK() {
        return permissionReportPK;
    }

    public void setPermissionReportPK(PermissionReportPK permissionReportPK) {
        this.permissionReportPK = permissionReportPK;
    }

    public Permission getPermission() {
        return permission;
    }

    public void setPermission(Permission permission) {
        this.permission = permission;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (permissionReportPK != null ? permissionReportPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionReport)) {
            return false;
        }
        PermissionReport other = (PermissionReport) object;
        if ((this.permissionReportPK == null && other.permissionReportPK != null) || (this.permissionReportPK != null && !this.permissionReportPK.equals(other.permissionReportPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.PermissionReport[ permissionReportPK=" + permissionReportPK + " ]";
    }
    
}
