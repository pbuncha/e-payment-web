/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author DominoMan
 */
@Embeddable
public class PermissionReportPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "PERMISSION_ID", nullable = false)
    private Permission permissionId;
    @Basic(optional = false)
    @Column(name = "REPORT_ID", nullable = false)
    private MReport reportId;

    public PermissionReportPK() {
    }

    public PermissionReportPK(Permission permissionId, MReport reportId) {
        this.permissionId = permissionId;
        this.reportId = reportId;
    }

    public Permission getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Permission permissionId) {
        this.permissionId = permissionId;
    }

    public MReport getReportId() {
        return reportId;
    }

    public void setReportId(MReport reportId) {
        this.reportId = reportId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) permissionId.getPermissionId();
        hash += (int) reportId.getReportId();
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PermissionReportPK)) {
            return false;
        }
        PermissionReportPK other = (PermissionReportPK) object;
        if (this.permissionId != other.permissionId) {
            return false;
        }
        if (this.reportId != other.reportId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.PermissionReportPK[ permissionId=" + permissionId + ", reportId=" + reportId + " ]";
    }
    
}
