/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "REV_GOV", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RevGov.findAll", query = "SELECT r FROM RevGov r"),
    @NamedQuery(name = "RevGov.findByRevGovId", query = "SELECT r FROM RevGov r WHERE r.revGovId = :revGovId"),
    @NamedQuery(name = "RevGov.findByRevGovName", query = "SELECT r FROM RevGov r WHERE r.revGovName = :revGovName"),
    @NamedQuery(name = "RevGov.findByRemark", query = "SELECT r FROM RevGov r WHERE r.remark = :remark"),
    @NamedQuery(name = "RevGov.findByReson", query = "SELECT r FROM RevGov r WHERE r.reson = :reson"),
    @NamedQuery(name = "RevGov.findByStartDate", query = "SELECT r FROM RevGov r WHERE r.startDate = :startDate"),
    @NamedQuery(name = "RevGov.findByEndDate", query = "SELECT r FROM RevGov r WHERE r.endDate = :endDate"),
    @NamedQuery(name = "RevGov.findByStatus", query = "SELECT r FROM RevGov r WHERE r.status = :status"),
    @NamedQuery(name = "RevGov.findByCreatedDate", query = "SELECT r FROM RevGov r WHERE r.createdDate = :createdDate"),
    @NamedQuery(name = "RevGov.findByCreatedBy", query = "SELECT r FROM RevGov r WHERE r.createdBy = :createdBy"),
    @NamedQuery(name = "RevGov.findByUpdatedDate", query = "SELECT r FROM RevGov r WHERE r.updatedDate = :updatedDate"),
    @NamedQuery(name = "RevGov.findByUpdatedBy", query = "SELECT r FROM RevGov r WHERE r.updatedBy = :updatedBy")})
public class RevGov implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REV_GOV_ID", nullable = false)
    private Integer revGovId;
    @Column(name = "REV_GOV_NAME", length = 255)
    private String revGovName;
    @Column(name = "REMARK", length = 1000)
    private String remark;
    @Column(name = "RESON", length = 2000)
    private String reson;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "revGovId", fetch = FetchType.LAZY)
    private List<RevGovDetail> revGovDetailList;
    @JoinColumn(name = "KEED_ID", referencedColumnName = "KEED_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private TypeKeep keedId;

    public RevGov() {
    }

    public RevGov(Integer revGovId) {
        this.revGovId = revGovId;
    }

    public RevGov(Integer revGovId, Date startDate) {
        this.revGovId = revGovId;
        this.startDate = startDate;
    }

    public Integer getRevGovId() {
        return revGovId;
    }

    public void setRevGovId(Integer revGovId) {
        this.revGovId = revGovId;
    }

    public String getRevGovName() {
        return revGovName;
    }

    public void setRevGovName(String revGovName) {
        this.revGovName = revGovName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReson() {
        return reson;
    }

    public void setReson(String reson) {
        this.reson = reson;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @XmlTransient
    public List<RevGovDetail> getRevGovDetailList() {
        return revGovDetailList;
    }

    public void setRevGovDetailList(List<RevGovDetail> revGovDetailList) {
        this.revGovDetailList = revGovDetailList;
    }

    public TypeKeep getKeedId() {
        return keedId;
    }

    public void setKeedId(TypeKeep keedId) {
        this.keedId = keedId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (revGovId != null ? revGovId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RevGov)) {
            return false;
        }
        RevGov other = (RevGov) object;
        if ((this.revGovId == null && other.revGovId != null) || (this.revGovId != null && !this.revGovId.equals(other.revGovId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.RevGov[ revGovId=" + revGovId + " ]";
    }
    
}
