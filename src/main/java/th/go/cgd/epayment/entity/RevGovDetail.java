/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "REV_GOV_DETAIL", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RevGovDetail.findAll", query = "SELECT r FROM RevGovDetail r"),
    @NamedQuery(name = "RevGovDetail.findByRevGovSeq", query = "SELECT r FROM RevGovDetail r WHERE r.revGovSeq = :revGovSeq"),
    @NamedQuery(name = "RevGovDetail.findByKeepRevenue", query = "SELECT r FROM RevGovDetail r WHERE r.keepRevenue = :keepRevenue"),
    @NamedQuery(name = "RevGovDetail.findByPercentBySeq", query = "SELECT r FROM RevGovDetail r WHERE r.percentBySeq = :percentBySeq"),
    @NamedQuery(name = "RevGovDetail.findByOwerCatalog", query = "SELECT r FROM RevGovDetail r WHERE r.owerCatalog = :owerCatalog"),
    @NamedQuery(name = "RevGovDetail.findByCreatedBy", query = "SELECT r FROM RevGovDetail r WHERE r.createdBy = :createdBy"),
    @NamedQuery(name = "RevGovDetail.findByUpdatedBy", query = "SELECT r FROM RevGovDetail r WHERE r.updatedBy = :updatedBy"),
    @NamedQuery(name = "RevGovDetail.findByUpdatedDate", query = "SELECT r FROM RevGovDetail r WHERE r.updatedDate = :updatedDate"),
    @NamedQuery(name = "RevGovDetail.findByCreatedDate", query = "SELECT r FROM RevGovDetail r WHERE r.createdDate = :createdDate"),
    @NamedQuery(name = "RevGovDetail.findByAccKId", query = "SELECT r FROM RevGovDetail r WHERE r.accKId = :accKId")})
public class RevGovDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "REV_GOV_SEQ", nullable = false)
    private Integer revGovSeq;
    @Column(name = "KEEP_REVENUE")
    private Integer keepRevenue;
    @Column(name = "PERCENT_BY_SEQ")
    private BigInteger percentBySeq;
    @Column(name = "OWER_CATALOG", length = 1000)
    private String owerCatalog;
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "ACC_K_ID", nullable = false, length = 6)
    private String accKId;
    @JoinColumn(name = "REV_GOV_ID", referencedColumnName = "REV_GOV_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private RevGov revGovId;
    @JoinColumn(name = "GL_ID", referencedColumnName = "GL_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MGl glId;

    public RevGovDetail() {
    }

    public RevGovDetail(Integer revGovSeq) {
        this.revGovSeq = revGovSeq;
    }

    public RevGovDetail(Integer revGovSeq, String accKId) {
        this.revGovSeq = revGovSeq;
        this.accKId = accKId;
    }

    public Integer getRevGovSeq() {
        return revGovSeq;
    }

    public void setRevGovSeq(Integer revGovSeq) {
        this.revGovSeq = revGovSeq;
    }

    public Integer getKeepRevenue() {
        return keepRevenue;
    }

    public void setKeepRevenue(Integer keepRevenue) {
        this.keepRevenue = keepRevenue;
    }

    public BigInteger getPercentBySeq() {
        return percentBySeq;
    }

    public void setPercentBySeq(BigInteger percentBySeq) {
        this.percentBySeq = percentBySeq;
    }

    public String getOwerCatalog() {
        return owerCatalog;
    }

    public void setOwerCatalog(String owerCatalog) {
        this.owerCatalog = owerCatalog;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getAccKId() {
        return accKId;
    }

    public void setAccKId(String accKId) {
        this.accKId = accKId;
    }

    public RevGov getRevGovId() {
        return revGovId;
    }

    public void setRevGovId(RevGov revGovId) {
        this.revGovId = revGovId;
    }

    public MGl getGlId() {
        return glId;
    }

    public void setGlId(MGl glId) {
        this.glId = glId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (revGovSeq != null ? revGovSeq.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RevGovDetail)) {
            return false;
        }
        RevGovDetail other = (RevGovDetail) object;
        if ((this.revGovSeq == null && other.revGovSeq != null) || (this.revGovSeq != null && !this.revGovSeq.equals(other.revGovSeq))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.RevGovDetail[ revGovSeq=" + revGovSeq + " ]";
    }
    
}
