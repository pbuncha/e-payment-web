/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "SECURITY_LOG", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SecurityLog.findAll", query = "SELECT s FROM SecurityLog s"),
    @NamedQuery(name = "SecurityLog.findByLogId", query = "SELECT s FROM SecurityLog s WHERE s.logId = :logId"),
    @NamedQuery(name = "SecurityLog.findByCreatedDate", query = "SELECT s FROM SecurityLog s WHERE s.createdDate = :createdDate"),
    @NamedQuery(name = "SecurityLog.findByLocalip", query = "SELECT s FROM SecurityLog s WHERE s.localip = :localip"),
    @NamedQuery(name = "SecurityLog.findByNewvalue", query = "SELECT s FROM SecurityLog s WHERE s.newvalue = :newvalue"),
    @NamedQuery(name = "SecurityLog.findByOriginalvalue", query = "SELECT s FROM SecurityLog s WHERE s.originalvalue = :originalvalue"),
    @NamedQuery(name = "SecurityLog.findByParametername", query = "SELECT s FROM SecurityLog s WHERE s.parametername = :parametername"),
    @NamedQuery(name = "SecurityLog.findByRemoteip", query = "SELECT s FROM SecurityLog s WHERE s.remoteip = :remoteip"),
    @NamedQuery(name = "SecurityLog.findByTarget", query = "SELECT s FROM SecurityLog s WHERE s.target = :target"),
    @NamedQuery(name = "SecurityLog.findByType", query = "SELECT s FROM SecurityLog s WHERE s.type = :type"),
    @NamedQuery(name = "SecurityLog.findByUpdatedDate", query = "SELECT s FROM SecurityLog s WHERE s.updatedDate = :updatedDate")})
public class SecurityLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "LOG_ID", nullable = false)
    private Integer logId;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "LOCALIP", length = 255)
    private String localip;
    @Column(name = "NEWVALUE", length = 255)
    private String newvalue;
    @Column(name = "ORIGINALVALUE", length = 1000)
    private String originalvalue;
    @Column(name = "PARAMETERNAME", length = 1000)
    private String parametername;
    @Column(name = "REMOTEIP", length = 255)
    private String remoteip;
    @Column(name = "TARGET", length = 255)
    private String target;
    @Column(name = "TYPE", length = 255)
    private String type;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;

    public SecurityLog() {
    }

    public SecurityLog(Integer logId) {
        this.logId = logId;
    }

    public Integer getLogId() {
        return logId;
    }

    public void setLogId(Integer logId) {
        this.logId = logId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getLocalip() {
        return localip;
    }

    public void setLocalip(String localip) {
        this.localip = localip;
    }

    public String getNewvalue() {
        return newvalue;
    }

    public void setNewvalue(String newvalue) {
        this.newvalue = newvalue;
    }

    public String getOriginalvalue() {
        return originalvalue;
    }

    public void setOriginalvalue(String originalvalue) {
        this.originalvalue = originalvalue;
    }

    public String getParametername() {
        return parametername;
    }

    public void setParametername(String parametername) {
        this.parametername = parametername;
    }

    public String getRemoteip() {
        return remoteip;
    }

    public void setRemoteip(String remoteip) {
        this.remoteip = remoteip;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (logId != null ? logId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SecurityLog)) {
            return false;
        }
        SecurityLog other = (SecurityLog) object;
        if ((this.logId == null && other.logId != null) || (this.logId != null && !this.logId.equals(other.logId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.SecurityLog[ logId=" + logId + " ]";
    }
    
}
