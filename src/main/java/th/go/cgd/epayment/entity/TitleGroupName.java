/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "TITLE_GROUP_NAME", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TitleGroupName.findAll", query = "SELECT t FROM TitleGroupName t"),
    @NamedQuery(name = "TitleGroupName.findByTitleGrpId", query = "SELECT t FROM TitleGroupName t WHERE t.titleGrpId = :titleGrpId"),
    @NamedQuery(name = "TitleGroupName.findByTitleGrpName", query = "SELECT t FROM TitleGroupName t WHERE t.titleGrpName = :titleGrpName"),
    @NamedQuery(name = "TitleGroupName.findByStartDate", query = "SELECT t FROM TitleGroupName t WHERE t.startDate = :startDate"),
    @NamedQuery(name = "TitleGroupName.findByEndDate", query = "SELECT t FROM TitleGroupName t WHERE t.endDate = :endDate"),
    @NamedQuery(name = "TitleGroupName.findByStatus", query = "SELECT t FROM TitleGroupName t WHERE t.status = :status"),
    @NamedQuery(name = "TitleGroupName.findByCreatedDate", query = "SELECT t FROM TitleGroupName t WHERE t.createdDate = :createdDate"),
    @NamedQuery(name = "TitleGroupName.findByCreatedBy", query = "SELECT t FROM TitleGroupName t WHERE t.createdBy = :createdBy"),
    @NamedQuery(name = "TitleGroupName.findByUpdatedDate", query = "SELECT t FROM TitleGroupName t WHERE t.updatedDate = :updatedDate"),
    @NamedQuery(name = "TitleGroupName.findByUpdatedBy", query = "SELECT t FROM TitleGroupName t WHERE t.updatedBy = :updatedBy")})
public class TitleGroupName implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "TITLE_GRP_ID", nullable = false)
    private Integer titleGrpId;
    @Column(name = "TITLE_GRP_NAME", length = 50)
    private String titleGrpName;
    @Basic(optional = false)
    @Column(name = "START_DATE", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "STATUS")
    private Character status;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "CREATED_BY")
    private Integer createdBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "UPDATED_BY")
    private Integer updatedBy;

    public TitleGroupName() {
    }

    public TitleGroupName(Integer titleGrpId) {
        this.titleGrpId = titleGrpId;
    }

    public TitleGroupName(Integer titleGrpId, Date startDate) {
        this.titleGrpId = titleGrpId;
        this.startDate = startDate;
    }

    public Integer getTitleGrpId() {
        return titleGrpId;
    }

    public void setTitleGrpId(Integer titleGrpId) {
        this.titleGrpId = titleGrpId;
    }

    public String getTitleGrpName() {
        return titleGrpName;
    }

    public void setTitleGrpName(String titleGrpName) {
        this.titleGrpName = titleGrpName;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Integer getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Integer createdBy) {
        this.createdBy = createdBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Integer getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Integer updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (titleGrpId != null ? titleGrpId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TitleGroupName)) {
            return false;
        }
        TitleGroupName other = (TitleGroupName) object;
        if ((this.titleGrpId == null && other.titleGrpId != null) || (this.titleGrpId != null && !this.titleGrpId.equals(other.titleGrpId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.TitleGroupName[ titleGrpId=" + titleGrpId + " ]";
    }
    
}
