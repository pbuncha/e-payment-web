/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "TYPE_KEEP", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypeKeep.findAll", query = "SELECT t FROM TypeKeep t"),
    @NamedQuery(name = "TypeKeep.findByKeedId", query = "SELECT t FROM TypeKeep t WHERE t.keedId = :keedId"),
    @NamedQuery(name = "TypeKeep.findByKeepName", query = "SELECT t FROM TypeKeep t WHERE t.keepName = :keepName"),
    @NamedQuery(name = "TypeKeep.findByRemark", query = "SELECT t FROM TypeKeep t WHERE t.remark = :remark"),
    @NamedQuery(name = "TypeKeep.findByCreatedBy", query = "SELECT t FROM TypeKeep t WHERE t.createdBy = :createdBy"),
    @NamedQuery(name = "TypeKeep.findByUpdatedBy", query = "SELECT t FROM TypeKeep t WHERE t.updatedBy = :updatedBy"),
    @NamedQuery(name = "TypeKeep.findByUpdatedDate", query = "SELECT t FROM TypeKeep t WHERE t.updatedDate = :updatedDate"),
    @NamedQuery(name = "TypeKeep.findByCreatedDate", query = "SELECT t FROM TypeKeep t WHERE t.createdDate = :createdDate")})
public class TypeKeep implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "KEED_ID", nullable = false)
    private Integer keedId;
    @Column(name = "KEEP_NAME", length = 255)
    private String keepName;
    @Column(name = "REMARK", length = 1000)
    private String remark;
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @OneToMany(mappedBy = "keedId", fetch = FetchType.LAZY)
    private List<RevGov> revGovList;

    public TypeKeep() {
    }

    public TypeKeep(Integer keedId) {
        this.keedId = keedId;
    }

    public Integer getKeedId() {
        return keedId;
    }

    public void setKeedId(Integer keedId) {
        this.keedId = keedId;
    }

    public String getKeepName() {
        return keepName;
    }

    public void setKeepName(String keepName) {
        this.keepName = keepName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @XmlTransient
    public List<RevGov> getRevGovList() {
        return revGovList;
    }

    public void setRevGovList(List<RevGov> revGovList) {
        this.revGovList = revGovList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (keedId != null ? keedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypeKeep)) {
            return false;
        }
        TypeKeep other = (TypeKeep) object;
        if ((this.keedId == null && other.keedId != null) || (this.keedId != null && !this.keedId.equals(other.keedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.TypeKeep[ keedId=" + keedId + " ]";
    }
    
}
