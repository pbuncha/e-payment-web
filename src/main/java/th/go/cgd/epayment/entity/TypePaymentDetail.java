/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "TYPE_PAYMENT_DETAIL", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TypePaymentDetail.findAll", query = "SELECT t FROM TypePaymentDetail t"),
    @NamedQuery(name = "TypePaymentDetail.findById", query = "SELECT t FROM TypePaymentDetail t WHERE t.id = :id"),
    @NamedQuery(name = "TypePaymentDetail.findByCcCardNumber", query = "SELECT t FROM TypePaymentDetail t WHERE t.ccCardNumber = :ccCardNumber"),
    @NamedQuery(name = "TypePaymentDetail.findByCcCcvNumber", query = "SELECT t FROM TypePaymentDetail t WHERE t.ccCcvNumber = :ccCcvNumber"),
    @NamedQuery(name = "TypePaymentDetail.findByCcExpiresOn", query = "SELECT t FROM TypePaymentDetail t WHERE t.ccExpiresOn = :ccExpiresOn"),
    @NamedQuery(name = "TypePaymentDetail.findByCcNameOnCard", query = "SELECT t FROM TypePaymentDetail t WHERE t.ccNameOnCard = :ccNameOnCard"),
    @NamedQuery(name = "TypePaymentDetail.findByType", query = "SELECT t FROM TypePaymentDetail t WHERE t.type = :type"),
    @NamedQuery(name = "TypePaymentDetail.findByCcName", query = "SELECT t FROM TypePaymentDetail t WHERE t.ccName = :ccName"),
    @NamedQuery(name = "TypePaymentDetail.findBySeq", query = "SELECT t FROM TypePaymentDetail t WHERE t.seq = :seq"),
    @NamedQuery(name = "TypePaymentDetail.findByCreatedBy", query = "SELECT t FROM TypePaymentDetail t WHERE t.createdBy = :createdBy"),
    @NamedQuery(name = "TypePaymentDetail.findByUpdatedBy", query = "SELECT t FROM TypePaymentDetail t WHERE t.updatedBy = :updatedBy"),
    @NamedQuery(name = "TypePaymentDetail.findByUpdatedDate", query = "SELECT t FROM TypePaymentDetail t WHERE t.updatedDate = :updatedDate"),
    @NamedQuery(name = "TypePaymentDetail.findByCreatedDate", query = "SELECT t FROM TypePaymentDetail t WHERE t.createdDate = :createdDate"),
    @NamedQuery(name = "TypePaymentDetail.findByChanelId", query = "SELECT t FROM TypePaymentDetail t WHERE t.chanelId = :chanelId")})
//    @NamedQuery(name = "TypePaymentDetail.findById1", query = "SELECT t FROM TypePaymentDetail t WHERE t.id1 = :id1")})
public class TypePaymentDetail implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    @Column(name = "CC_CARD_NUMBER", length = 20)
    private String ccCardNumber;
    @Column(name = "CC_CCV_NUMBER", length = 5)
    private String ccCcvNumber;
    @Column(name = "CC_EXPIRES_ON", length = 7)
    private String ccExpiresOn;
    @Column(name = "CC_NAME_ON_CARD", length = 255)
    private String ccNameOnCard;
    @Column(name = "TYPE", length = 2)
    private String type;
    @Column(name = "CC_NAME", length = 255)
    private String ccName;
    @Column(name = "SEQ")
    private Integer seq;
    @Column(name = "CREATED_BY", length = 50)
    private String createdBy;
    @Column(name = "UPDATED_BY", length = 50)
    private String updatedBy;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "CHANEL_ID", nullable = false, length = 2)
    private String chanelId;
    @Basic(optional = false)
    @Column(name = "GATEWAY_BANK_ID", nullable = false, length = 2)
    private String gatewayBankId;

    public TypePaymentDetail() {
    }

    public TypePaymentDetail(Integer id) {
        this.id = id;
    }

    public TypePaymentDetail(Integer id, String chanelId, String gatewayBankId) {
        this.id = id;
        this.chanelId = chanelId;
        this.gatewayBankId = gatewayBankId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCcCardNumber() {
        return ccCardNumber;
    }

    public void setCcCardNumber(String ccCardNumber) {
        this.ccCardNumber = ccCardNumber;
    }

    public String getCcCcvNumber() {
        return ccCcvNumber;
    }

    public void setCcCcvNumber(String ccCcvNumber) {
        this.ccCcvNumber = ccCcvNumber;
    }

    public String getCcExpiresOn() {
        return ccExpiresOn;
    }

    public void setCcExpiresOn(String ccExpiresOn) {
        this.ccExpiresOn = ccExpiresOn;
    }

    public String getCcNameOnCard() {
        return ccNameOnCard;
    }

    public void setCcNameOnCard(String ccNameOnCard) {
        this.ccNameOnCard = ccNameOnCard;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCcName() {
        return ccName;
    }

    public void setCcName(String ccName) {
        this.ccName = ccName;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getChanelId() {
        return chanelId;
    }

    public void setChanelId(String chanelId) {
        this.chanelId = chanelId;
    }

    public String getGatewayBankId() {
        return gatewayBankId;
    }

    public void setGatewayBankId(String gatewayBankId) {
        this.gatewayBankId = gatewayBankId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TypePaymentDetail)) {
            return false;
        }
        TypePaymentDetail other = (TypePaymentDetail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.TypePaymentDetail[ id=" + id + " ]";
    }
    
}
