/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
    @NamedQuery(name = "User.findByUserId", query = "SELECT u FROM User u WHERE u.userId = :userId"),
    @NamedQuery(name = "User.findByCreatedDate", query = "SELECT u FROM User u WHERE u.createdDate = :createdDate"),
    @NamedQuery(name = "User.findByEmail", query = "SELECT u FROM User u WHERE u.email = :email"),
    @NamedQuery(name = "User.findByFailedLoginAttempt", query = "SELECT u FROM User u WHERE u.failedLoginAttempt = :failedLoginAttempt"),
    @NamedQuery(name = "User.findByForceChangePassword", query = "SELECT u FROM User u WHERE u.forceChangePassword = :forceChangePassword"),
    @NamedQuery(name = "User.findByLastChangePasswordDate", query = "SELECT u FROM User u WHERE u.lastChangePasswordDate = :lastChangePasswordDate"),
    @NamedQuery(name = "User.findByLastLogin", query = "SELECT u FROM User u WHERE u.lastLogin = :lastLogin"),
    @NamedQuery(name = "User.findByMobile", query = "SELECT u FROM User u WHERE u.mobile = :mobile"),
    @NamedQuery(name = "User.findByPassword", query = "SELECT u FROM User u WHERE u.password = :password"),
    @NamedQuery(name = "User.findByStatus", query = "SELECT u FROM User u WHERE u.status = :status"),
    @NamedQuery(name = "User.findByUpdatedDate", query = "SELECT u FROM User u WHERE u.updatedDate = :updatedDate"),
    @NamedQuery(name = "User.findByUserName", query = "SELECT u FROM User u WHERE u.userName = :userName"),
    @NamedQuery(name = "User.findByUserTypeId", query = "SELECT u FROM User u WHERE u.userTypeId = :userTypeId"),
    @NamedQuery(name = "User.findByVerifiedType", query = "SELECT u FROM User u WHERE u.verifiedType = :verifiedType")})
public class User implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_ID", nullable = false)
    private Integer userId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "EMAIL", nullable = false, length = 255)
    private String email;
    @Column(name = "FAILED_LOGIN_ATTEMPT")
    private Short failedLoginAttempt;
    @Column(name = "FORCE_CHANGE_PASSWORD", length = 1)
    private String forceChangePassword;
    @Column(name = "LAST_CHANGE_PASSWORD_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastChangePasswordDate;
    @Column(name = "LAST_LOGIN")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastLogin;
    @Column(name = "MOBILE", length = 20)
    private String mobile;
    @Column(name = "PASSWORD", length = 100)
    private String password;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false)
    private char status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "USER_NAME", length = 20)
    private String userName;
    @Basic(optional = false)
    @Column(name = "USER_TYPE_ID", nullable = false)
    private int userTypeId;
    @Column(name = "VERIFIED_TYPE")
    private Character verifiedType;
    @ManyToMany(mappedBy = "userList", fetch = FetchType.LAZY)
    private List<PermissionItem> permissionItemList;
    @ManyToMany(mappedBy = "userList", fetch = FetchType.LAZY)
    private List<PermissionGroup> permissionGroupList;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<BatchProblemFile> batchProblemFileList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<BatchProblemFile> batchProblemFileList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MDisbursementUnit> mDisbursementUnitList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MDisbursementUnit> mDisbursementUnitList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<GfmisBatchHLog> gfmisBatchHLogList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<GfmisBatchHLog> gfmisBatchHLogList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<CatalogConditionText> catalogConditionTextList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<CatalogConditionText> catalogConditionTextList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MOrganizationSubType> mOrganizationSubTypeList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MOrganizationSubType> mOrganizationSubTypeList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<GfmisGen> gfmisGenList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<GfmisGen> gfmisGenList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<AuditLog> auditLogList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<AuditLog> auditLogList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "updateBy", fetch = FetchType.LAZY)
    private List<PermissionGroup> permissionGroupList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createBy", fetch = FetchType.LAZY)
    private List<PermissionGroup> permissionGroupList2;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MOrganizationType> mOrganizationTypeList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MOrganizationType> mOrganizationTypeList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<SecurityLog> securityLogList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<SecurityLog> securityLogList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MCostCenter> mCostCenterList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MCostCenter> mCostCenterList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<GfmisBatchDetailLog> gfmisBatchDetailLogList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<GfmisBatchDetailLog> gfmisBatchDetailLogList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<UserFavorite> userFavoriteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<UserFavorite> userFavoriteList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId", fetch = FetchType.LAZY)
    private List<UserFavorite> userFavoriteList2;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<WithdrawGfmis> withdrawGfmisList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<WithdrawGfmis> withdrawGfmisList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<GfmisGenItem> gfmisGenItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<GfmisGenItem> gfmisGenItemList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<GfmisBatchFLog> gfmisBatchFLogList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<GfmisBatchFLog> gfmisBatchFLogList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<CatalogStructureItem> catalogStructureItemList;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<CatalogStructureItem> catalogStructureItemList1;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<UserCenter> userCenterList;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<UserCenter> userCenterList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<InvoiceItem> invoiceItemList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<InvoiceItem> invoiceItemList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<GfmisTypeDoc> gfmisTypeDocList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<GfmisTypeDoc> gfmisTypeDocList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MBusinessArea> mBusinessAreaList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MBusinessArea> mBusinessAreaList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<Billing> billingList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<Billing> billingList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<UserOtp> userOtpList;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<CatalogCostCenter> catalogCostCenterList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<CatalogCostCenter> catalogCostCenterList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MGl> mGlList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MGl> mGlList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<InvoiceLoader> invoiceLoaderList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<InvoiceLoader> invoiceLoaderList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<Bank> bankList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<Bank> bankList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<BankBranch> bankBranchList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<BankBranch> bankBranchList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MDepartment> mDepartmentList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MDepartment> mDepartmentList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<Catalog> catalogList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<Catalog> catalogList1;
    @OneToMany(mappedBy = "userId", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList1;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<Invoice> invoiceList2;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<GfmisTypePaid> gfmisTypePaidList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<GfmisTypePaid> gfmisTypePaidList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MTitleName> mTitleNameList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MTitleName> mTitleNameList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<Address> addressList;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<Address> addressList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<BatchFile> batchFileList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<BatchFile> batchFileList1;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<MStandardRevernue> mStandardRevernueList;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<MStandardRevernue> mStandardRevernueList1;
    @JoinColumn(name = "USER_CENTER_ID", referencedColumnName = "USER_CENTER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    private UserCenter userCenterId;
    @OneToMany(mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<User> userList;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<User> userList1;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId", fetch = FetchType.LAZY)
    private List<UserOrganization> userOrganizationList2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId", fetch = FetchType.LAZY)
    private List<UserAuthorization> userAuthorizationList;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<UserAuthorization> userAuthorizationList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<UserAuthorization> userAuthorizationList2;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "createdBy", fetch = FetchType.LAZY)
    private List<CatalogStructure> catalogStructureList;
    @OneToMany(mappedBy = "updatedBy", fetch = FetchType.LAZY)
    private List<CatalogStructure> catalogStructureList1;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId", fetch = FetchType.EAGER)
    private List<MUserPermissiongroup> mUserPermissiongroupList;
    @Column(name = "APPROVE_COMMENT", length = 512)
    private String approveComment;
    @Column(name = "START_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Column(name = "END_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    @Column(name = "FILE_ATTACH", length = 512)
    private String fileAttach;

    public User() {
    }

    public User(Integer userId) {
        this.userId = userId;
    }

    public User(Integer userId, Date createdDate, String email, char status, int userTypeId) {
        this.userId = userId;
        this.createdDate = createdDate;
        this.email = email;
        this.status = status;
        this.userTypeId = userTypeId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Short getFailedLoginAttempt() {
        return failedLoginAttempt;
    }

    public void setFailedLoginAttempt(Short failedLoginAttempt) {
        this.failedLoginAttempt = failedLoginAttempt;
    }

    public String getForceChangePassword() {
        return forceChangePassword;
    }

    public void setForceChangePassword(String forceChangePassword) {
        this.forceChangePassword = forceChangePassword;
    }

    public Date getLastChangePasswordDate() {
        return lastChangePasswordDate;
    }

    public void setLastChangePasswordDate(Date lastChangePasswordDate) {
        this.lastChangePasswordDate = lastChangePasswordDate;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public Character getVerifiedType() {
        return verifiedType;
    }

    public void setVerifiedType(Character verifiedType) {
        this.verifiedType = verifiedType;
    }

    @XmlTransient
    public List<PermissionItem> getPermissionItemList() {
        return permissionItemList;
    }

    public void setPermissionItemList(List<PermissionItem> permissionItemList) {
        this.permissionItemList = permissionItemList;
    }

    @XmlTransient
    public List<PermissionGroup> getPermissionGroupList() {
        return permissionGroupList;
    }

    public void setPermissionGroupList(List<PermissionGroup> permissionGroupList) {
        this.permissionGroupList = permissionGroupList;
    }

    @XmlTransient
    public List<BatchProblemFile> getBatchProblemFileList() {
        return batchProblemFileList;
    }

    public void setBatchProblemFileList(List<BatchProblemFile> batchProblemFileList) {
        this.batchProblemFileList = batchProblemFileList;
    }

    @XmlTransient
    public List<BatchProblemFile> getBatchProblemFileList1() {
        return batchProblemFileList1;
    }

    public void setBatchProblemFileList1(List<BatchProblemFile> batchProblemFileList1) {
        this.batchProblemFileList1 = batchProblemFileList1;
    }

    @XmlTransient
    public List<MDisbursementUnit> getMDisbursementUnitList() {
        return mDisbursementUnitList;
    }

    public void setMDisbursementUnitList(List<MDisbursementUnit> mDisbursementUnitList) {
        this.mDisbursementUnitList = mDisbursementUnitList;
    }

    @XmlTransient
    public List<MDisbursementUnit> getMDisbursementUnitList1() {
        return mDisbursementUnitList1;
    }

    public void setMDisbursementUnitList1(List<MDisbursementUnit> mDisbursementUnitList1) {
        this.mDisbursementUnitList1 = mDisbursementUnitList1;
    }

    @XmlTransient
    public List<GfmisBatchHLog> getGfmisBatchHLogList() {
        return gfmisBatchHLogList;
    }

    public void setGfmisBatchHLogList(List<GfmisBatchHLog> gfmisBatchHLogList) {
        this.gfmisBatchHLogList = gfmisBatchHLogList;
    }

    @XmlTransient
    public List<GfmisBatchHLog> getGfmisBatchHLogList1() {
        return gfmisBatchHLogList1;
    }

    public void setGfmisBatchHLogList1(List<GfmisBatchHLog> gfmisBatchHLogList1) {
        this.gfmisBatchHLogList1 = gfmisBatchHLogList1;
    }

    @XmlTransient
    public List<CatalogConditionText> getCatalogConditionTextList() {
        return catalogConditionTextList;
    }

    public void setCatalogConditionTextList(List<CatalogConditionText> catalogConditionTextList) {
        this.catalogConditionTextList = catalogConditionTextList;
    }

    @XmlTransient
    public List<CatalogConditionText> getCatalogConditionTextList1() {
        return catalogConditionTextList1;
    }

    public void setCatalogConditionTextList1(List<CatalogConditionText> catalogConditionTextList1) {
        this.catalogConditionTextList1 = catalogConditionTextList1;
    }

    @XmlTransient
    public List<MOrganizationSubType> getMOrganizationSubTypeList() {
        return mOrganizationSubTypeList;
    }

    public void setMOrganizationSubTypeList(List<MOrganizationSubType> mOrganizationSubTypeList) {
        this.mOrganizationSubTypeList = mOrganizationSubTypeList;
    }

    @XmlTransient
    public List<MOrganizationSubType> getMOrganizationSubTypeList1() {
        return mOrganizationSubTypeList1;
    }

    public void setMOrganizationSubTypeList1(List<MOrganizationSubType> mOrganizationSubTypeList1) {
        this.mOrganizationSubTypeList1 = mOrganizationSubTypeList1;
    }

    @XmlTransient
    public List<GfmisGen> getGfmisGenList() {
        return gfmisGenList;
    }

    public void setGfmisGenList(List<GfmisGen> gfmisGenList) {
        this.gfmisGenList = gfmisGenList;
    }

    @XmlTransient
    public List<GfmisGen> getGfmisGenList1() {
        return gfmisGenList1;
    }

    public void setGfmisGenList1(List<GfmisGen> gfmisGenList1) {
        this.gfmisGenList1 = gfmisGenList1;
    }

    @XmlTransient
    public List<AuditLog> getAuditLogList() {
        return auditLogList;
    }

    public void setAuditLogList(List<AuditLog> auditLogList) {
        this.auditLogList = auditLogList;
    }

    @XmlTransient
    public List<AuditLog> getAuditLogList1() {
        return auditLogList1;
    }

    public void setAuditLogList1(List<AuditLog> auditLogList1) {
        this.auditLogList1 = auditLogList1;
    }

    @XmlTransient
    public List<PermissionGroup> getPermissionGroupList1() {
        return permissionGroupList1;
    }

    public void setPermissionGroupList1(List<PermissionGroup> permissionGroupList1) {
        this.permissionGroupList1 = permissionGroupList1;
    }

    @XmlTransient
    public List<PermissionGroup> getPermissionGroupList2() {
        return permissionGroupList2;
    }

    public void setPermissionGroupList2(List<PermissionGroup> permissionGroupList2) {
        this.permissionGroupList2 = permissionGroupList2;
    }

    @XmlTransient
    public List<MOrganizationType> getMOrganizationTypeList() {
        return mOrganizationTypeList;
    }

    public void setMOrganizationTypeList(List<MOrganizationType> mOrganizationTypeList) {
        this.mOrganizationTypeList = mOrganizationTypeList;
    }

    @XmlTransient
    public List<MOrganizationType> getMOrganizationTypeList1() {
        return mOrganizationTypeList1;
    }

    public void setMOrganizationTypeList1(List<MOrganizationType> mOrganizationTypeList1) {
        this.mOrganizationTypeList1 = mOrganizationTypeList1;
    }

    @XmlTransient
    public List<SecurityLog> getSecurityLogList() {
        return securityLogList;
    }

    public void setSecurityLogList(List<SecurityLog> securityLogList) {
        this.securityLogList = securityLogList;
    }

    @XmlTransient
    public List<SecurityLog> getSecurityLogList1() {
        return securityLogList1;
    }

    public void setSecurityLogList1(List<SecurityLog> securityLogList1) {
        this.securityLogList1 = securityLogList1;
    }

    @XmlTransient
    public List<MCostCenter> getMCostCenterList() {
        return mCostCenterList;
    }

    public void setMCostCenterList(List<MCostCenter> mCostCenterList) {
        this.mCostCenterList = mCostCenterList;
    }

    @XmlTransient
    public List<MCostCenter> getMCostCenterList1() {
        return mCostCenterList1;
    }

    public void setMCostCenterList1(List<MCostCenter> mCostCenterList1) {
        this.mCostCenterList1 = mCostCenterList1;
    }

    @XmlTransient
    public List<GfmisBatchDetailLog> getGfmisBatchDetailLogList() {
        return gfmisBatchDetailLogList;
    }

    public void setGfmisBatchDetailLogList(List<GfmisBatchDetailLog> gfmisBatchDetailLogList) {
        this.gfmisBatchDetailLogList = gfmisBatchDetailLogList;
    }

    @XmlTransient
    public List<GfmisBatchDetailLog> getGfmisBatchDetailLogList1() {
        return gfmisBatchDetailLogList1;
    }

    public void setGfmisBatchDetailLogList1(List<GfmisBatchDetailLog> gfmisBatchDetailLogList1) {
        this.gfmisBatchDetailLogList1 = gfmisBatchDetailLogList1;
    }

    @XmlTransient
    public List<UserFavorite> getUserFavoriteList() {
        return userFavoriteList;
    }

    public void setUserFavoriteList(List<UserFavorite> userFavoriteList) {
        this.userFavoriteList = userFavoriteList;
    }

    @XmlTransient
    public List<UserFavorite> getUserFavoriteList1() {
        return userFavoriteList1;
    }

    public void setUserFavoriteList1(List<UserFavorite> userFavoriteList1) {
        this.userFavoriteList1 = userFavoriteList1;
    }

    @XmlTransient
    public List<UserFavorite> getUserFavoriteList2() {
        return userFavoriteList2;
    }

    public void setUserFavoriteList2(List<UserFavorite> userFavoriteList2) {
        this.userFavoriteList2 = userFavoriteList2;
    }

    @XmlTransient
    public List<WithdrawGfmis> getWithdrawGfmisList() {
        return withdrawGfmisList;
    }

    public void setWithdrawGfmisList(List<WithdrawGfmis> withdrawGfmisList) {
        this.withdrawGfmisList = withdrawGfmisList;
    }

    @XmlTransient
    public List<WithdrawGfmis> getWithdrawGfmisList1() {
        return withdrawGfmisList1;
    }

    public void setWithdrawGfmisList1(List<WithdrawGfmis> withdrawGfmisList1) {
        this.withdrawGfmisList1 = withdrawGfmisList1;
    }

    @XmlTransient
    public List<GfmisGenItem> getGfmisGenItemList() {
        return gfmisGenItemList;
    }

    public void setGfmisGenItemList(List<GfmisGenItem> gfmisGenItemList) {
        this.gfmisGenItemList = gfmisGenItemList;
    }

    @XmlTransient
    public List<GfmisGenItem> getGfmisGenItemList1() {
        return gfmisGenItemList1;
    }

    public void setGfmisGenItemList1(List<GfmisGenItem> gfmisGenItemList1) {
        this.gfmisGenItemList1 = gfmisGenItemList1;
    }

    @XmlTransient
    public List<GfmisBatchFLog> getGfmisBatchFLogList() {
        return gfmisBatchFLogList;
    }

    public void setGfmisBatchFLogList(List<GfmisBatchFLog> gfmisBatchFLogList) {
        this.gfmisBatchFLogList = gfmisBatchFLogList;
    }

    @XmlTransient
    public List<GfmisBatchFLog> getGfmisBatchFLogList1() {
        return gfmisBatchFLogList1;
    }

    public void setGfmisBatchFLogList1(List<GfmisBatchFLog> gfmisBatchFLogList1) {
        this.gfmisBatchFLogList1 = gfmisBatchFLogList1;
    }

    @XmlTransient
    public List<CatalogStructureItem> getCatalogStructureItemList() {
        return catalogStructureItemList;
    }

    public void setCatalogStructureItemList(List<CatalogStructureItem> catalogStructureItemList) {
        this.catalogStructureItemList = catalogStructureItemList;
    }

    @XmlTransient
    public List<CatalogStructureItem> getCatalogStructureItemList1() {
        return catalogStructureItemList1;
    }

    public void setCatalogStructureItemList1(List<CatalogStructureItem> catalogStructureItemList1) {
        this.catalogStructureItemList1 = catalogStructureItemList1;
    }

    @XmlTransient
    public List<UserCenter> getUserCenterList() {
        return userCenterList;
    }

    public void setUserCenterList(List<UserCenter> userCenterList) {
        this.userCenterList = userCenterList;
    }

    @XmlTransient
    public List<UserCenter> getUserCenterList1() {
        return userCenterList1;
    }

    public void setUserCenterList1(List<UserCenter> userCenterList1) {
        this.userCenterList1 = userCenterList1;
    }

    @XmlTransient
    public List<InvoiceItem> getInvoiceItemList() {
        return invoiceItemList;
    }

    public void setInvoiceItemList(List<InvoiceItem> invoiceItemList) {
        this.invoiceItemList = invoiceItemList;
    }

    @XmlTransient
    public List<InvoiceItem> getInvoiceItemList1() {
        return invoiceItemList1;
    }

    public void setInvoiceItemList1(List<InvoiceItem> invoiceItemList1) {
        this.invoiceItemList1 = invoiceItemList1;
    }

    @XmlTransient
    public List<GfmisTypeDoc> getGfmisTypeDocList() {
        return gfmisTypeDocList;
    }

    public void setGfmisTypeDocList(List<GfmisTypeDoc> gfmisTypeDocList) {
        this.gfmisTypeDocList = gfmisTypeDocList;
    }

    @XmlTransient
    public List<GfmisTypeDoc> getGfmisTypeDocList1() {
        return gfmisTypeDocList1;
    }

    public void setGfmisTypeDocList1(List<GfmisTypeDoc> gfmisTypeDocList1) {
        this.gfmisTypeDocList1 = gfmisTypeDocList1;
    }

    @XmlTransient
    public List<MBusinessArea> getMBusinessAreaList() {
        return mBusinessAreaList;
    }

    public void setMBusinessAreaList(List<MBusinessArea> mBusinessAreaList) {
        this.mBusinessAreaList = mBusinessAreaList;
    }

    @XmlTransient
    public List<MBusinessArea> getMBusinessAreaList1() {
        return mBusinessAreaList1;
    }

    public void setMBusinessAreaList1(List<MBusinessArea> mBusinessAreaList1) {
        this.mBusinessAreaList1 = mBusinessAreaList1;
    }

    @XmlTransient
    public List<Billing> getBillingList() {
        return billingList;
    }

    public void setBillingList(List<Billing> billingList) {
        this.billingList = billingList;
    }

    @XmlTransient
    public List<Billing> getBillingList1() {
        return billingList1;
    }

    public void setBillingList1(List<Billing> billingList1) {
        this.billingList1 = billingList1;
    }

    @XmlTransient
    public List<UserOtp> getUserOtpList() {
        return userOtpList;
    }

    public void setUserOtpList(List<UserOtp> userOtpList) {
        this.userOtpList = userOtpList;
    }

    @XmlTransient
    public List<CatalogCostCenter> getCatalogCostCenterList() {
        return catalogCostCenterList;
    }

    public void setCatalogCostCenterList(List<CatalogCostCenter> catalogCostCenterList) {
        this.catalogCostCenterList = catalogCostCenterList;
    }

    @XmlTransient
    public List<CatalogCostCenter> getCatalogCostCenterList1() {
        return catalogCostCenterList1;
    }

    public void setCatalogCostCenterList1(List<CatalogCostCenter> catalogCostCenterList1) {
        this.catalogCostCenterList1 = catalogCostCenterList1;
    }

    @XmlTransient
    public List<MGl> getMGlList() {
        return mGlList;
    }

    public void setMGlList(List<MGl> mGlList) {
        this.mGlList = mGlList;
    }

    @XmlTransient
    public List<MGl> getMGlList1() {
        return mGlList1;
    }

    public void setMGlList1(List<MGl> mGlList1) {
        this.mGlList1 = mGlList1;
    }

    @XmlTransient
    public List<InvoiceLoader> getInvoiceLoaderList() {
        return invoiceLoaderList;
    }

    public void setInvoiceLoaderList(List<InvoiceLoader> invoiceLoaderList) {
        this.invoiceLoaderList = invoiceLoaderList;
    }

    @XmlTransient
    public List<InvoiceLoader> getInvoiceLoaderList1() {
        return invoiceLoaderList1;
    }

    public void setInvoiceLoaderList1(List<InvoiceLoader> invoiceLoaderList1) {
        this.invoiceLoaderList1 = invoiceLoaderList1;
    }

    @XmlTransient
    public List<Bank> getBankList() {
        return bankList;
    }

    public void setBankList(List<Bank> bankList) {
        this.bankList = bankList;
    }

    @XmlTransient
    public List<Bank> getBankList1() {
        return bankList1;
    }

    public void setBankList1(List<Bank> bankList1) {
        this.bankList1 = bankList1;
    }

    @XmlTransient
    public List<BankBranch> getBankBranchList() {
        return bankBranchList;
    }

    public void setBankBranchList(List<BankBranch> bankBranchList) {
        this.bankBranchList = bankBranchList;
    }

    @XmlTransient
    public List<BankBranch> getBankBranchList1() {
        return bankBranchList1;
    }

    public void setBankBranchList1(List<BankBranch> bankBranchList1) {
        this.bankBranchList1 = bankBranchList1;
    }

    @XmlTransient
    public List<MDepartment> getMDepartmentList() {
        return mDepartmentList;
    }

    public void setMDepartmentList(List<MDepartment> mDepartmentList) {
        this.mDepartmentList = mDepartmentList;
    }

    @XmlTransient
    public List<MDepartment> getMDepartmentList1() {
        return mDepartmentList1;
    }

    public void setMDepartmentList1(List<MDepartment> mDepartmentList1) {
        this.mDepartmentList1 = mDepartmentList1;
    }

    @XmlTransient
    public List<Catalog> getCatalogList() {
        return catalogList;
    }

    public void setCatalogList(List<Catalog> catalogList) {
        this.catalogList = catalogList;
    }

    @XmlTransient
    public List<Catalog> getCatalogList1() {
        return catalogList1;
    }

    public void setCatalogList1(List<Catalog> catalogList1) {
        this.catalogList1 = catalogList1;
    }

    @XmlTransient
    public List<Invoice> getInvoiceList() {
        return invoiceList;
    }

    public void setInvoiceList(List<Invoice> invoiceList) {
        this.invoiceList = invoiceList;
    }

    @XmlTransient
    public List<Invoice> getInvoiceList1() {
        return invoiceList1;
    }

    public void setInvoiceList1(List<Invoice> invoiceList1) {
        this.invoiceList1 = invoiceList1;
    }

    @XmlTransient
    public List<Invoice> getInvoiceList2() {
        return invoiceList2;
    }

    public void setInvoiceList2(List<Invoice> invoiceList2) {
        this.invoiceList2 = invoiceList2;
    }

    @XmlTransient
    public List<GfmisTypePaid> getGfmisTypePaidList() {
        return gfmisTypePaidList;
    }

    public void setGfmisTypePaidList(List<GfmisTypePaid> gfmisTypePaidList) {
        this.gfmisTypePaidList = gfmisTypePaidList;
    }

    @XmlTransient
    public List<GfmisTypePaid> getGfmisTypePaidList1() {
        return gfmisTypePaidList1;
    }

    public void setGfmisTypePaidList1(List<GfmisTypePaid> gfmisTypePaidList1) {
        this.gfmisTypePaidList1 = gfmisTypePaidList1;
    }

    @XmlTransient
    public List<MTitleName> getMTitleNameList() {
        return mTitleNameList;
    }

    public void setMTitleNameList(List<MTitleName> mTitleNameList) {
        this.mTitleNameList = mTitleNameList;
    }

    @XmlTransient
    public List<MTitleName> getMTitleNameList1() {
        return mTitleNameList1;
    }

    public void setMTitleNameList1(List<MTitleName> mTitleNameList1) {
        this.mTitleNameList1 = mTitleNameList1;
    }

    @XmlTransient
    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    @XmlTransient
    public List<Address> getAddressList1() {
        return addressList1;
    }

    public void setAddressList1(List<Address> addressList1) {
        this.addressList1 = addressList1;
    }

    @XmlTransient
    public List<BatchFile> getBatchFileList() {
        return batchFileList;
    }

    public void setBatchFileList(List<BatchFile> batchFileList) {
        this.batchFileList = batchFileList;
    }

    @XmlTransient
    public List<BatchFile> getBatchFileList1() {
        return batchFileList1;
    }

    public void setBatchFileList1(List<BatchFile> batchFileList1) {
        this.batchFileList1 = batchFileList1;
    }

    @XmlTransient
    public List<MStandardRevernue> getMStandardRevernueList() {
        return mStandardRevernueList;
    }

    public void setMStandardRevernueList(List<MStandardRevernue> mStandardRevernueList) {
        this.mStandardRevernueList = mStandardRevernueList;
    }

    @XmlTransient
    public List<MStandardRevernue> getMStandardRevernueList1() {
        return mStandardRevernueList1;
    }

    public void setMStandardRevernueList1(List<MStandardRevernue> mStandardRevernueList1) {
        this.mStandardRevernueList1 = mStandardRevernueList1;
    }

    public UserCenter getUserCenterId() {
        return userCenterId;
    }

    public void setUserCenterId(UserCenter userCenterId) {
        this.userCenterId = userCenterId;
    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @XmlTransient
    public List<User> getUserList1() {
        return userList1;
    }

    public void setUserList1(List<User> userList1) {
        this.userList1 = userList1;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList() {
        return userOrganizationList;
    }

    public void setUserOrganizationList(List<UserOrganization> userOrganizationList) {
        this.userOrganizationList = userOrganizationList;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList1() {
        return userOrganizationList1;
    }

    public void setUserOrganizationList1(List<UserOrganization> userOrganizationList1) {
        this.userOrganizationList1 = userOrganizationList1;
    }

    @XmlTransient
    public List<UserOrganization> getUserOrganizationList2() {
        return userOrganizationList2;
    }

    public void setUserOrganizationList2(List<UserOrganization> userOrganizationList2) {
        this.userOrganizationList2 = userOrganizationList2;
    }

    @XmlTransient
    public List<UserAuthorization> getUserAuthorizationList() {
        return userAuthorizationList;
    }

    public void setUserAuthorizationList(List<UserAuthorization> userAuthorizationList) {
        this.userAuthorizationList = userAuthorizationList;
    }

    @XmlTransient
    public List<UserAuthorization> getUserAuthorizationList1() {
        return userAuthorizationList1;
    }

    public void setUserAuthorizationList1(List<UserAuthorization> userAuthorizationList1) {
        this.userAuthorizationList1 = userAuthorizationList1;
    }

    @XmlTransient
    public List<UserAuthorization> getUserAuthorizationList2() {
        return userAuthorizationList2;
    }

    public void setUserAuthorizationList2(List<UserAuthorization> userAuthorizationList2) {
        this.userAuthorizationList2 = userAuthorizationList2;
    }

    @XmlTransient
    public List<CatalogStructure> getCatalogStructureList() {
        return catalogStructureList;
    }

    public void setCatalogStructureList(List<CatalogStructure> catalogStructureList) {
        this.catalogStructureList = catalogStructureList;
    }

    @XmlTransient
    public List<CatalogStructure> getCatalogStructureList1() {
        return catalogStructureList1;
    }

    public void setCatalogStructureList1(List<CatalogStructure> catalogStructureList1) {
        this.catalogStructureList1 = catalogStructureList1;
    }
    @XmlTransient
    public List<MUserPermissiongroup> getMUserPermissiongroupList() {
        return mUserPermissiongroupList;
    }

    public void setMUserPermissiongroupList(List<MUserPermissiongroup> mUserPermissiongroupList) {
        this.mUserPermissiongroupList = mUserPermissiongroupList;
    }
    
    public String getFileAttach() {
        return fileAttach;
    }

    public void setFileAttach(String fileAttach) {
        this.fileAttach = fileAttach;
    }
    
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    
    public String getApproveComment() {
        return approveComment;
    }

    public void setApproveComment(String approveComment) {
        this.approveComment = approveComment;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof User)) {
            return false;
        }
        User other = (User) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.User[ userId=" + userId + " ]";
    }
    
}
