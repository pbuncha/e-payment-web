/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER_ACCESS_PERMISSION", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserAccessPermission.findAll", query = "SELECT u FROM UserAccessPermission u"),
    @NamedQuery(name = "UserAccessPermission.findByUserId", query = "SELECT u FROM UserAccessPermission u WHERE u.userId = :userId")})
public class UserAccessPermission implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_ID", nullable = false)
    private Integer userId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userAccessPermission", fetch = FetchType.LAZY)
    private List<UserAccessPermissionOrg> userAccessPermissionOrgList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userAccessPermission1", fetch = FetchType.LAZY)
    private UserAccessPermission userAccessPermission;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private UserAccessPermission userAccessPermission1;
    @JoinColumn(name = "ACCESS_PERMISSION_ID", referencedColumnName = "ACCESS_PERMISSION_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MUserAccessPermission accessPermissionId;

    public UserAccessPermission() {
    }

    public UserAccessPermission(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @XmlTransient
    public List<UserAccessPermissionOrg> getUserAccessPermissionOrgList() {
        return userAccessPermissionOrgList;
    }

    public void setUserAccessPermissionOrgList(List<UserAccessPermissionOrg> userAccessPermissionOrgList) {
        this.userAccessPermissionOrgList = userAccessPermissionOrgList;
    }

    public UserAccessPermission getUserAccessPermission() {
        return userAccessPermission;
    }

    public void setUserAccessPermission(UserAccessPermission userAccessPermission) {
        this.userAccessPermission = userAccessPermission;
    }

    public UserAccessPermission getUserAccessPermission1() {
        return userAccessPermission1;
    }

    public void setUserAccessPermission1(UserAccessPermission userAccessPermission1) {
        this.userAccessPermission1 = userAccessPermission1;
    }

    public MUserAccessPermission getAccessPermissionId() {
        return accessPermissionId;
    }

    public void setAccessPermissionId(MUserAccessPermission accessPermissionId) {
        this.accessPermissionId = accessPermissionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccessPermission)) {
            return false;
        }
        UserAccessPermission other = (UserAccessPermission) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserAccessPermission[ userId=" + userId + " ]";
    }
    
}
