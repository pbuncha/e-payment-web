/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER_ACCESS_PERMISSION_ORG", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserAccessPermissionOrg.findAll", query = "SELECT u FROM UserAccessPermissionOrg u"),
    @NamedQuery(name = "UserAccessPermissionOrg.findByOrgId", query = "SELECT u FROM UserAccessPermissionOrg u WHERE u.userAccessPermissionOrgPK.orgId = :orgId"),
    @NamedQuery(name = "UserAccessPermissionOrg.findByUserId", query = "SELECT u FROM UserAccessPermissionOrg u WHERE u.userAccessPermissionOrgPK.userId = :userId")})
public class UserAccessPermissionOrg implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected UserAccessPermissionOrgPK userAccessPermissionOrgPK;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private UserAccessPermission userAccessPermission;

    public UserAccessPermissionOrg() {
    }

    public UserAccessPermissionOrg(UserAccessPermissionOrgPK userAccessPermissionOrgPK) {
        this.userAccessPermissionOrgPK = userAccessPermissionOrgPK;
    }

    public UserAccessPermissionOrg(int orgId, int userId) {
        this.userAccessPermissionOrgPK = new UserAccessPermissionOrgPK(orgId, userId);
    }

    public UserAccessPermissionOrgPK getUserAccessPermissionOrgPK() {
        return userAccessPermissionOrgPK;
    }

    public void setUserAccessPermissionOrgPK(UserAccessPermissionOrgPK userAccessPermissionOrgPK) {
        this.userAccessPermissionOrgPK = userAccessPermissionOrgPK;
    }

    public UserAccessPermission getUserAccessPermission() {
        return userAccessPermission;
    }

    public void setUserAccessPermission(UserAccessPermission userAccessPermission) {
        this.userAccessPermission = userAccessPermission;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userAccessPermissionOrgPK != null ? userAccessPermissionOrgPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccessPermissionOrg)) {
            return false;
        }
        UserAccessPermissionOrg other = (UserAccessPermissionOrg) object;
        if ((this.userAccessPermissionOrgPK == null && other.userAccessPermissionOrgPK != null) || (this.userAccessPermissionOrgPK != null && !this.userAccessPermissionOrgPK.equals(other.userAccessPermissionOrgPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserAccessPermissionOrg[ userAccessPermissionOrgPK=" + userAccessPermissionOrgPK + " ]";
    }
    
}
