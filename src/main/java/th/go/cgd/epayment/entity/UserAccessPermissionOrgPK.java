/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author DominoMan
 */
@Embeddable
public class UserAccessPermissionOrgPK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ORG_ID", nullable = false)
    private int orgId;
    @Basic(optional = false)
    @Column(name = "USER_ID", nullable = false)
    private int userId;

    public UserAccessPermissionOrgPK() {
    }

    public UserAccessPermissionOrgPK(int orgId, int userId) {
        this.orgId = orgId;
        this.userId = userId;
    }

    public int getOrgId() {
        return orgId;
    }

    public void setOrgId(int orgId) {
        this.orgId = orgId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) orgId;
        hash += (int) userId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAccessPermissionOrgPK)) {
            return false;
        }
        UserAccessPermissionOrgPK other = (UserAccessPermissionOrgPK) object;
        if (this.orgId != other.orgId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserAccessPermissionOrgPK[ orgId=" + orgId + ", userId=" + userId + " ]";
    }
    
}
