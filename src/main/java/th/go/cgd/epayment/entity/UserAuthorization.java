/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER_AUTHORIZATION", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserAuthorization.findAll", query = "SELECT u FROM UserAuthorization u"),
    @NamedQuery(name = "UserAuthorization.findByUserAuthorizationId", query = "SELECT u FROM UserAuthorization u WHERE u.userAuthorizationId = :userAuthorizationId"),
    @NamedQuery(name = "UserAuthorization.findByCreatedDate", query = "SELECT u FROM UserAuthorization u WHERE u.createdDate = :createdDate"),
    @NamedQuery(name = "UserAuthorization.findByStatus", query = "SELECT u FROM UserAuthorization u WHERE u.status = :status"),
    @NamedQuery(name = "UserAuthorization.findByUpdatedDate", query = "SELECT u FROM UserAuthorization u WHERE u.updatedDate = :updatedDate")})
public class UserAuthorization implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_AUTHORIZATION_ID", nullable = false)
    private Integer userAuthorizationId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "STATUS", nullable = false)
    private char status;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User userId;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ROLE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MUserRole roleId;

    public UserAuthorization() {
    }

    public UserAuthorization(Integer userAuthorizationId) {
        this.userAuthorizationId = userAuthorizationId;
    }

    public UserAuthorization(Integer userAuthorizationId, Date createdDate, char status) {
        this.userAuthorizationId = userAuthorizationId;
        this.createdDate = createdDate;
        this.status = status;
    }

    public Integer getUserAuthorizationId() {
        return userAuthorizationId;
    }

    public void setUserAuthorizationId(Integer userAuthorizationId) {
        this.userAuthorizationId = userAuthorizationId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public MUserRole getRoleId() {
        return roleId;
    }

    public void setRoleId(MUserRole roleId) {
        this.roleId = roleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userAuthorizationId != null ? userAuthorizationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserAuthorization)) {
            return false;
        }
        UserAuthorization other = (UserAuthorization) object;
        if ((this.userAuthorizationId == null && other.userAuthorizationId != null) || (this.userAuthorizationId != null && !this.userAuthorizationId.equals(other.userAuthorizationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserAuthorization[ userAuthorizationId=" + userAuthorizationId + " ]";
    }
    
}
