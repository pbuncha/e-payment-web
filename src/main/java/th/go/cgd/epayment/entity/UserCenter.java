/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER_CENTER", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserCenter.findAll", query = "SELECT u FROM UserCenter u"),
    @NamedQuery(name = "UserCenter.findByUserCenterId", query = "SELECT u FROM UserCenter u WHERE u.userCenterId = :userCenterId"),
    @NamedQuery(name = "UserCenter.findByBirthDate", query = "SELECT u FROM UserCenter u WHERE u.birthDate = :birthDate"),
    @NamedQuery(name = "UserCenter.findByCitizenNo", query = "SELECT u FROM UserCenter u WHERE u.citizenNo = :citizenNo"),
    @NamedQuery(name = "UserCenter.findByCreatedDate", query = "SELECT u FROM UserCenter u WHERE u.createdDate = :createdDate"),
    @NamedQuery(name = "UserCenter.findByFirstNameEn", query = "SELECT u FROM UserCenter u WHERE u.firstNameEn = :firstNameEn"),
    @NamedQuery(name = "UserCenter.findByFirstNameTh", query = "SELECT u FROM UserCenter u WHERE u.firstNameTh = :firstNameTh"),
    @NamedQuery(name = "UserCenter.findByIsAccessEpayment", query = "SELECT u FROM UserCenter u WHERE u.isAccessEpayment = :isAccessEpayment"),
    @NamedQuery(name = "UserCenter.findByIsAccessEwelfare", query = "SELECT u FROM UserCenter u WHERE u.isAccessEwelfare = :isAccessEwelfare"),
    @NamedQuery(name = "UserCenter.findByLastNameEn", query = "SELECT u FROM UserCenter u WHERE u.lastNameEn = :lastNameEn"),
    @NamedQuery(name = "UserCenter.findByLastNameTh", query = "SELECT u FROM UserCenter u WHERE u.lastNameTh = :lastNameTh"),
    @NamedQuery(name = "UserCenter.findByMiddleNameEn", query = "SELECT u FROM UserCenter u WHERE u.middleNameEn = :middleNameEn"),
    @NamedQuery(name = "UserCenter.findByPassword", query = "SELECT u FROM UserCenter u WHERE u.password = :password"),
    @NamedQuery(name = "UserCenter.findBySex", query = "SELECT u FROM UserCenter u WHERE u.sex = :sex"),
    @NamedQuery(name = "UserCenter.findByUpdatedDate", query = "SELECT u FROM UserCenter u WHERE u.updatedDate = :updatedDate"),
    @NamedQuery(name = "UserCenter.findByCompanyStatus", query = "SELECT u FROM UserCenter u WHERE u.companyStatus = :companyStatus"),
    @NamedQuery(name = "UserCenter.findByCompanyObjective", query = "SELECT u FROM UserCenter u WHERE u.companyObjective = :companyObjective"),
    @NamedQuery(name = "UserCenter.findByCompanyAuthId", query = "SELECT u FROM UserCenter u WHERE u.companyAuthId = :companyAuthId"),
    @NamedQuery(name = "UserCenter.findByCompanyAuthBirthDate", query = "SELECT u FROM UserCenter u WHERE u.companyAuthBirthDate = :companyAuthBirthDate"),
    @NamedQuery(name = "UserCenter.findByCompanyAuthEmail", query = "SELECT u FROM UserCenter u WHERE u.companyAuthEmail = :companyAuthEmail"),
    @NamedQuery(name = "UserCenter.findByCompanyAuthPhone", query = "SELECT u FROM UserCenter u WHERE u.companyAuthPhone = :companyAuthPhone"),
    @NamedQuery(name = "UserCenter.findByCompanyAuthDoc", query = "SELECT u FROM UserCenter u WHERE u.companyAuthDoc = :companyAuthDoc")})
public class UserCenter implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_CENTER_ID", nullable = false)
    private Integer userCenterId;
    @Column(name = "BIRTH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date birthDate;
    @Basic(optional = false)
    @Column(name = "CITIZEN_NO", nullable = false, length = 20)
    private String citizenNo;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "FIRST_NAME_EN", nullable = false, length = 255)
    private String firstNameEn;
    @Basic(optional = false)
    @Column(name = "FIRST_NAME_TH", nullable = false, length = 255)
    private String firstNameTh;
    @Basic(optional = false)
    @Column(name = "IS_ACCESS_EPAYMENT", nullable = false)
    private char isAccessEpayment;
    @Basic(optional = false)
    @Column(name = "IS_ACCESS_EWELFARE", nullable = false)
    private char isAccessEwelfare;
    @Column(name = "LAST_NAME_EN", length = 255)
    private String lastNameEn;
    @Column(name = "LAST_NAME_TH", length = 255)
    private String lastNameTh;
    @Column(name = "MIDDLE_NAME_EN", length = 255)
    private String middleNameEn;
    @Column(name = "MIDDLE_NAME_TH", length = 255)
    private String middleNameTh;
    @Basic(optional = false)
    @Column(name = "PASSWORD", nullable = false, length = 100)
    private String password;
    @Basic(optional = false)
    @Column(name = "SEX", nullable = false)
    private char sex;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "COMPANY_STATUS", length = 50)
    private String companyStatus;
    @Column(name = "COMPANY_OBJECTIVE", length = 255)
    private String companyObjective;
    @Column(name = "COMPANY_AUTH_ID", length = 20)
    private String companyAuthId;
    @Column(name = "COMPANY_AUTH_BIRTH_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date companyAuthBirthDate;
    @Column(name = "COMPANY_AUTH_EMAIL", length = 255)
    private String companyAuthEmail;
    @Column(name = "COMPANY_AUTH_PHONE", length = 20)
    private String companyAuthPhone;
    @Column(name = "COMPANY_AUTH_DOC", length = 255)
    private String companyAuthDoc;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "TITLE_ID", referencedColumnName = "TITLE_ID")
    @ManyToOne(fetch = FetchType.EAGER)
    private MTitleName titleId;
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "ADDRESS_ID")
    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Address addressId;
//    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userCenterId", fetch = FetchType.LAZY)
//    private List<InvoiceLoader> invoiceLoaderList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userCenterId", fetch = FetchType.LAZY)
    private List<User> userList;

    public UserCenter() {
    }

    public UserCenter(Integer userCenterId) {
        this.userCenterId = userCenterId;
    }

    public UserCenter(Integer userCenterId, String citizenNo, Date createdDate, String firstNameEn, String firstNameTh, char isAccessEpayment, char isAccessEwelfare, String password, char sex) {
        this.userCenterId = userCenterId;
        this.citizenNo = citizenNo;
        this.createdDate = createdDate;
        this.firstNameEn = firstNameEn;
        this.firstNameTh = firstNameTh;
        this.isAccessEpayment = isAccessEpayment;
        this.isAccessEwelfare = isAccessEwelfare;
        this.password = password;
        this.sex = sex;
    }

    public Integer getUserCenterId() {
        return userCenterId;
    }

    public void setUserCenterId(Integer userCenterId) {
        this.userCenterId = userCenterId;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getCitizenNo() {
        return citizenNo;
    }

    public void setCitizenNo(String citizenNo) {
        this.citizenNo = citizenNo;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getFirstNameEn() {
        return firstNameEn;
    }

    public void setFirstNameEn(String firstNameEn) {
        this.firstNameEn = firstNameEn;
    }

    public String getFirstNameTh() {
        return firstNameTh;
    }

    public void setFirstNameTh(String firstNameTh) {
        this.firstNameTh = firstNameTh;
    }

    public char getIsAccessEpayment() {
        return isAccessEpayment;
    }

    public void setIsAccessEpayment(char isAccessEpayment) {
        this.isAccessEpayment = isAccessEpayment;
    }

    public char getIsAccessEwelfare() {
        return isAccessEwelfare;
    }

    public void setIsAccessEwelfare(char isAccessEwelfare) {
        this.isAccessEwelfare = isAccessEwelfare;
    }

    public String getLastNameEn() {
        return lastNameEn;
    }

    public void setLastNameEn(String lastNameEn) {
        this.lastNameEn = lastNameEn;
    }

    public String getLastNameTh() {
        return lastNameTh;
    }

    public void setLastNameTh(String lastNameTh) {
        this.lastNameTh = lastNameTh;
    }

    public String getMiddleNameEn() {
        return middleNameEn;
    }

    public void setMiddleNameEn(String middleNameEn) {
        this.middleNameEn = middleNameEn;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(String companyStatus) {
        this.companyStatus = companyStatus;
    }

    public String getCompanyObjective() {
        return companyObjective;
    }

    public void setCompanyObjective(String companyObjective) {
        this.companyObjective = companyObjective;
    }

    public String getCompanyAuthId() {
        return companyAuthId;
    }

    public void setCompanyAuthId(String companyAuthId) {
        this.companyAuthId = companyAuthId;
    }

    public Date getCompanyAuthBirthDate() {
        return companyAuthBirthDate;
    }

    public void setCompanyAuthBirthDate(Date companyAuthBirthDate) {
        this.companyAuthBirthDate = companyAuthBirthDate;
    }

    public String getCompanyAuthEmail() {
        return companyAuthEmail;
    }

    public void setCompanyAuthEmail(String companyAuthEmail) {
        this.companyAuthEmail = companyAuthEmail;
    }

    public String getCompanyAuthPhone() {
        return companyAuthPhone;
    }

    public void setCompanyAuthPhone(String companyAuthPhone) {
        this.companyAuthPhone = companyAuthPhone;
    }

    public String getCompanyAuthDoc() {
        return companyAuthDoc;
    }

    public void setCompanyAuthDoc(String companyAuthDoc) {
        this.companyAuthDoc = companyAuthDoc;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public MTitleName getTitleId() {
        return titleId;
    }

    public void setTitleId(MTitleName titleId) {
        this.titleId = titleId;
    }

    public Address getAddressId() {
        return addressId;
    }

    public void setAddressId(Address addressId) {
        this.addressId = addressId;
    }

//    @XmlTransient
//    public List<InvoiceLoader> getInvoiceLoaderList() {
//        return invoiceLoaderList;
//    }
//
//    public void setInvoiceLoaderList(List<InvoiceLoader> invoiceLoaderList) {
//        this.invoiceLoaderList = invoiceLoaderList;
//    }

    @XmlTransient
    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
    

    public String getMiddleNameTh() {
        return middleNameTh;
    }

    public void setMiddleNameTh(String middleNameTh) {
        this.middleNameTh = middleNameTh;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userCenterId != null ? userCenterId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserCenter)) {
            return false;
        }
        UserCenter other = (UserCenter) object;
        if ((this.userCenterId == null && other.userCenterId != null) || (this.userCenterId != null && !this.userCenterId.equals(other.userCenterId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserCenter[ userCenterId=" + userCenterId + " ]";
    }
    
}
