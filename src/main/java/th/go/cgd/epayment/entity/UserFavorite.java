/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER_FAVORITE", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserFavorite.findAll", query = "SELECT u FROM UserFavorite u"),
    @NamedQuery(name = "UserFavorite.findByUserFavoriteId", query = "SELECT u FROM UserFavorite u WHERE u.userFavoriteId = :userFavoriteId"),
    @NamedQuery(name = "UserFavorite.findByCreatedDate", query = "SELECT u FROM UserFavorite u WHERE u.createdDate = :createdDate"),
    @NamedQuery(name = "UserFavorite.findByUpdatedDate", query = "SELECT u FROM UserFavorite u WHERE u.updatedDate = :updatedDate")})
public class UserFavorite implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_FAVORITE_ID", nullable = false)
    private Integer userFavoriteId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User userId;
    @JoinColumn(name = "CATALOG_ID", referencedColumnName = "CATALOG_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Catalog catalogId;

    public UserFavorite() {
    }

    public UserFavorite(Integer userFavoriteId) {
        this.userFavoriteId = userFavoriteId;
    }

    public UserFavorite(Integer userFavoriteId, Date createdDate) {
        this.userFavoriteId = userFavoriteId;
        this.createdDate = createdDate;
    }

    public Integer getUserFavoriteId() {
        return userFavoriteId;
    }

    public void setUserFavoriteId(Integer userFavoriteId) {
        this.userFavoriteId = userFavoriteId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Catalog getCatalogId() {
        return catalogId;
    }

    public void setCatalogId(Catalog catalogId) {
        this.catalogId = catalogId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userFavoriteId != null ? userFavoriteId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserFavorite)) {
            return false;
        }
        UserFavorite other = (UserFavorite) object;
        if ((this.userFavoriteId == null && other.userFavoriteId != null) || (this.userFavoriteId != null && !this.userFavoriteId.equals(other.userFavoriteId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserFavorite[ userFavoriteId=" + userFavoriteId + " ]";
    }
    
}
