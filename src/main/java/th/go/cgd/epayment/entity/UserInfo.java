/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER_INFO", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserInfo.findAll", query = "SELECT u FROM UserInfo u"),
    @NamedQuery(name = "UserInfo.findByUserId", query = "SELECT u FROM UserInfo u WHERE u.userId = :userId"),
    @NamedQuery(name = "UserInfo.findByCitizenNo", query = "SELECT u FROM UserInfo u WHERE u.citizenNo = :citizenNo"),
    @NamedQuery(name = "UserInfo.findByEmail", query = "SELECT u FROM UserInfo u WHERE u.email = :email"),
    @NamedQuery(name = "UserInfo.findByFirstName", query = "SELECT u FROM UserInfo u WHERE u.firstName = :firstName"),
    @NamedQuery(name = "UserInfo.findByLastName", query = "SELECT u FROM UserInfo u WHERE u.lastName = :lastName"),
    @NamedQuery(name = "UserInfo.findByMobileNo", query = "SELECT u FROM UserInfo u WHERE u.mobileNo = :mobileNo"),
    @NamedQuery(name = "UserInfo.findByPhoneNo", query = "SELECT u FROM UserInfo u WHERE u.phoneNo = :phoneNo"),
    @NamedQuery(name = "UserInfo.findByPosition", query = "SELECT u FROM UserInfo u WHERE u.position = :position"),
    @NamedQuery(name = "UserInfo.findByTitleOther", query = "SELECT u FROM UserInfo u WHERE u.titleOther = :titleOther")})
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "USER_ID", nullable = false)
    private Integer userId;
    @Column(name = "CITIZEN_NO", length = 13)
    private String citizenNo;
    @Column(name = "EMAIL", length = 50)
    private String email;
    @Basic(optional = false)
    @Column(name = "FIRST_NAME", nullable = false, length = 100)
    private String firstName;
    @Basic(optional = false)
    @Column(name = "LAST_NAME", nullable = false, length = 100)
    private String lastName;
    @Column(name = "MOBILE_NO", length = 10)
    private String mobileNo;
    @Column(name = "PHONE_NO", length = 50)
    private String phoneNo;
    @Column(name = "POSITION", length = 200)
    private String position;
    @Column(name = "TITLE_OTHER", length = 50)
    private String titleOther;
    @OneToMany(mappedBy = "parentId", fetch = FetchType.LAZY)
    private List<UserInfo> userInfoList;
    @JoinColumn(name = "PARENT_ID", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private UserInfo parentId;
    @JoinColumn(name = "ORG_ID", referencedColumnName = "ORG_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Organization orgId;
    @JoinColumn(name = "TITLE_ID", referencedColumnName = "TITLE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MTitleName titleId;

    public UserInfo() {
    }

    public UserInfo(Integer userId) {
        this.userId = userId;
    }

    public UserInfo(Integer userId, String firstName, String lastName) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getCitizenNo() {
        return citizenNo;
    }

    public void setCitizenNo(String citizenNo) {
        this.citizenNo = citizenNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getTitleOther() {
        return titleOther;
    }

    public void setTitleOther(String titleOther) {
        this.titleOther = titleOther;
    }

    @XmlTransient
    public List<UserInfo> getUserInfoList() {
        return userInfoList;
    }

    public void setUserInfoList(List<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }

    public UserInfo getParentId() {
        return parentId;
    }

    public void setParentId(UserInfo parentId) {
        this.parentId = parentId;
    }

    public Organization getOrgId() {
        return orgId;
    }

    public void setOrgId(Organization orgId) {
        this.orgId = orgId;
    }

    public MTitleName getTitleId() {
        return titleId;
    }

    public void setTitleId(MTitleName titleId) {
        this.titleId = titleId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserInfo)) {
            return false;
        }
        UserInfo other = (UserInfo) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserInfo[ userId=" + userId + " ]";
    }
    
}
