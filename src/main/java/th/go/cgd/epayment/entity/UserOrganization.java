/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER_ORGANIZATION", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserOrganization.findAll", query = "SELECT u FROM UserOrganization u"),
    @NamedQuery(name = "UserOrganization.findByUserOrganizationId", query = "SELECT u FROM UserOrganization u WHERE u.userOrganizationId = :userOrganizationId"),
    @NamedQuery(name = "UserOrganization.findByFilepathAuthentication", query = "SELECT u FROM UserOrganization u WHERE u.filepathAuthentication = :filepathAuthentication"),
    @NamedQuery(name = "UserOrganization.findByCreatedDate", query = "SELECT u FROM UserOrganization u WHERE u.createdDate = :createdDate"),
    @NamedQuery(name = "UserOrganization.findByUpdatedDate", query = "SELECT u FROM UserOrganization u WHERE u.updatedDate = :updatedDate")})
public class UserOrganization implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_ORGANIZATION_ID", nullable = false)
    private Integer userOrganizationId;
    @Basic(optional = false)
    @Column(name = "FILEPATH_AUTHENTICATION", nullable = false, length = 255)
    private String filepathAuthentication;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "USER_ID", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    private User userId;
    @JoinColumn(name = "ORGANIZATION_TYPE_ID", referencedColumnName = "ORGANIZATION_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MOrganizationType organizationTypeId;
    @JoinColumn(name = "ORGANIZATION_SUB_TYPE_ID", referencedColumnName = "ORGANIZATION_SUB_TYPE_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MOrganizationSubType organizationSubTypeId;
    @JoinColumn(name = "DEPARTMENT_ID", referencedColumnName = "DEPARTMENT_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MDepartment departmentId;
    @JoinColumn(name = "COST_CENTER_ID", referencedColumnName = "COST_CENTER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MCostCenter costCenterId;
    @JoinColumn(name = "BUSINESS_AREA_ID", referencedColumnName = "BUSINESS_AREA_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private MBusinessArea businessAreaId;
    @JoinColumn(name = "ADDRESS_ID", referencedColumnName = "ADDRESS_ID")
    @ManyToOne(fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    private Address addressId;

    public UserOrganization() {
    }

    public UserOrganization(Integer userOrganizationId) {
        this.userOrganizationId = userOrganizationId;
    }

    public UserOrganization(Integer userOrganizationId, String filepathAuthentication, Date createdDate) {
        this.userOrganizationId = userOrganizationId;
        this.filepathAuthentication = filepathAuthentication;
        this.createdDate = createdDate;
    }

    public Integer getUserOrganizationId() {
        return userOrganizationId;
    }

    public void setUserOrganizationId(Integer userOrganizationId) {
        this.userOrganizationId = userOrganizationId;
    }

    public String getFilepathAuthentication() {
        return filepathAuthentication;
    }

    public void setFilepathAuthentication(String filepathAuthentication) {
        this.filepathAuthentication = filepathAuthentication;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public MOrganizationType getOrganizationTypeId() {
        return organizationTypeId;
    }

    public void setOrganizationTypeId(MOrganizationType organizationTypeId) {
        this.organizationTypeId = organizationTypeId;
    }

    public MOrganizationSubType getOrganizationSubTypeId() {
        return organizationSubTypeId;
    }

    public void setOrganizationSubTypeId(MOrganizationSubType organizationSubTypeId) {
        this.organizationSubTypeId = organizationSubTypeId;
    }

    public MDepartment getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(MDepartment departmentId) {
        this.departmentId = departmentId;
    }

    public MCostCenter getCostCenterId() {
        return costCenterId;
    }

    public void setCostCenterId(MCostCenter costCenterId) {
        this.costCenterId = costCenterId;
    }

    public MBusinessArea getBusinessAreaId() {
        return businessAreaId;
    }

    public void setBusinessAreaId(MBusinessArea businessAreaId) {
        this.businessAreaId = businessAreaId;
    }

    public Address getAddressId() {
        return addressId;
    }

    public void setAddressId(Address addressId) {
        this.addressId = addressId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userOrganizationId != null ? userOrganizationId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserOrganization)) {
            return false;
        }
        UserOrganization other = (UserOrganization) object;
        if ((this.userOrganizationId == null && other.userOrganizationId != null) || (this.userOrganizationId != null && !this.userOrganizationId.equals(other.userOrganizationId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserOrganization[ userOrganizationId=" + userOrganizationId + " ]";
    }
    
}
