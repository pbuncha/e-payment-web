/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "USER_OTP", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserOtp.findAll", query = "SELECT u FROM UserOtp u"),
    @NamedQuery(name = "UserOtp.findByUserId", query = "SELECT u FROM UserOtp u WHERE u.userId = :userId"),
    @NamedQuery(name = "UserOtp.findByCreatedDate", query = "SELECT u FROM UserOtp u WHERE u.createdDate = :createdDate"),
    @NamedQuery(name = "UserOtp.findByOtp", query = "SELECT u FROM UserOtp u WHERE u.otp = :otp"),
    @NamedQuery(name = "UserOtp.findByRefNo", query = "SELECT u FROM UserOtp u WHERE u.refNo = :refNo"),
    @NamedQuery(name = "UserOtp.findBySDate", query = "SELECT u FROM UserOtp u WHERE u.sDate = :sDate")})
public class UserOtp implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "USER_ID", nullable = false)
    private Integer userId;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Basic(optional = false)
    @Column(name = "OTP", nullable = false, length = 6)
    private String otp;
    @Basic(optional = false)
    @Column(name = "REF_NO", nullable = false, length = 255)
    private String refNo;
    @Basic(optional = false)
    @Column(name = "S_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date sDate;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private User createdBy;

    public UserOtp() {
    }

    public UserOtp(Integer userId) {
        this.userId = userId;
    }

    public UserOtp(Integer userId, Date createdDate, String otp, String refNo, Date sDate) {
        this.userId = userId;
        this.createdDate = createdDate;
        this.otp = otp;
        this.refNo = refNo;
        this.sDate = sDate;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Date getSDate() {
        return sDate;
    }

    public void setSDate(Date sDate) {
        this.sDate = sDate;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserOtp)) {
            return false;
        }
        UserOtp other = (UserOtp) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserOtp[ userId=" + userId + " ]";
    }
    
}
