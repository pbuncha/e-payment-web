/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author DominoMan
 */
@Embeddable
public class UserRolePK implements Serializable {
    @Basic(optional = false)
    @Column(name = "ROLE_ID", nullable = false)
    private short roleId;
    @Basic(optional = false)
    @Column(name = "USER_ID", nullable = false)
    private int userId;

    public UserRolePK() {
    }

    public UserRolePK(short roleId, int userId) {
        this.roleId = roleId;
        this.userId = userId;
    }

    public short getRoleId() {
        return roleId;
    }

    public void setRoleId(short roleId) {
        this.roleId = roleId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) roleId;
        hash += (int) userId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserRolePK)) {
            return false;
        }
        UserRolePK other = (UserRolePK) object;
        if (this.roleId != other.roleId) {
            return false;
        }
        if (this.userId != other.userId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.UserRolePK[ roleId=" + roleId + ", userId=" + userId + " ]";
    }
    
}
