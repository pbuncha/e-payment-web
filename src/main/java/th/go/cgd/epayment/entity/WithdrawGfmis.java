/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "WITHDRAW_GFMIS", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WithdrawGfmis.findAll", query = "SELECT w FROM WithdrawGfmis w"),
    @NamedQuery(name = "WithdrawGfmis.findById", query = "SELECT w FROM WithdrawGfmis w WHERE w.id = :id"),
    @NamedQuery(name = "WithdrawGfmis.findByAmount", query = "SELECT w FROM WithdrawGfmis w WHERE w.amount = :amount"),
    @NamedQuery(name = "WithdrawGfmis.findByCreatedDate", query = "SELECT w FROM WithdrawGfmis w WHERE w.createdDate = :createdDate"),
    @NamedQuery(name = "WithdrawGfmis.findByRemark", query = "SELECT w FROM WithdrawGfmis w WHERE w.remark = :remark"),
    @NamedQuery(name = "WithdrawGfmis.findByUpdatedDate", query = "SELECT w FROM WithdrawGfmis w WHERE w.updatedDate = :updatedDate"),
    @NamedQuery(name = "WithdrawGfmis.findByWithdrawDateGf", query = "SELECT w FROM WithdrawGfmis w WHERE w.withdrawDateGf = :withdrawDateGf")})
public class WithdrawGfmis implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Integer id;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "AMOUNT", nullable = false, precision = 19, scale = 2)
    private BigDecimal amount;
    @Column(name = "CREATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;
    @Column(name = "REMARK", length = 2000)
    private String remark;
    @Column(name = "UPDATED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDate;
    @Column(name = "WITHDRAW_DATE_GF")
    @Temporal(TemporalType.DATE)
    private Date withdrawDateGf;
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User updatedBy;
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "USER_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;
    @JoinColumn(name = "BILLING_ID", referencedColumnName = "BILLING_ID", nullable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Billing billingId;

    public WithdrawGfmis() {
    }

    public WithdrawGfmis(Integer id) {
        this.id = id;
    }

    public WithdrawGfmis(Integer id, BigDecimal amount) {
        this.id = id;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Date getWithdrawDateGf() {
        return withdrawDateGf;
    }

    public void setWithdrawDateGf(Date withdrawDateGf) {
        this.withdrawDateGf = withdrawDateGf;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public Billing getBillingId() {
        return billingId;
    }

    public void setBillingId(Billing billingId) {
        this.billingId = billingId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WithdrawGfmis)) {
            return false;
        }
        WithdrawGfmis other = (WithdrawGfmis) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.WithdrawGfmis[ id=" + id + " ]";
    }
    
}
