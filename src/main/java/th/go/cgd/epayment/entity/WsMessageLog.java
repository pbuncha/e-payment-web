/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package th.go.cgd.epayment.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author DominoMan
 */
@Entity
@Table(name = "WS_MESSAGE_LOG", catalog = "", schema = "PAYDB")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "WsMessageLog.findAll", query = "SELECT w FROM WsMessageLog w"),
    @NamedQuery(name = "WsMessageLog.findByWsMessageLog", query = "SELECT w FROM WsMessageLog w WHERE w.wsMessageLog = :wsMessageLog"),
    @NamedQuery(name = "WsMessageLog.findByUsername", query = "SELECT w FROM WsMessageLog w WHERE w.username = :username"),
    @NamedQuery(name = "WsMessageLog.findByTxId", query = "SELECT w FROM WsMessageLog w WHERE w.txId = :txId"),
    @NamedQuery(name = "WsMessageLog.findByTxType", query = "SELECT w FROM WsMessageLog w WHERE w.txType = :txType"),
    @NamedQuery(name = "WsMessageLog.findByMsgTimestamp", query = "SELECT w FROM WsMessageLog w WHERE w.msgTimestamp = :msgTimestamp"),
    @NamedQuery(name = "WsMessageLog.findByRefNo1", query = "SELECT w FROM WsMessageLog w WHERE w.refNo1 = :refNo1"),
    @NamedQuery(name = "WsMessageLog.findByRefNo2", query = "SELECT w FROM WsMessageLog w WHERE w.refNo2 = :refNo2"),
    @NamedQuery(name = "WsMessageLog.findByBankCode", query = "SELECT w FROM WsMessageLog w WHERE w.bankCode = :bankCode"),
    @NamedQuery(name = "WsMessageLog.findByAmount", query = "SELECT w FROM WsMessageLog w WHERE w.amount = :amount"),
    @NamedQuery(name = "WsMessageLog.findByResponseCode", query = "SELECT w FROM WsMessageLog w WHERE w.responseCode = :responseCode"),
    @NamedQuery(name = "WsMessageLog.findByBillCode", query = "SELECT w FROM WsMessageLog w WHERE w.billCode = :billCode"),
    @NamedQuery(name = "WsMessageLog.findByResponseTimestamp", query = "SELECT w FROM WsMessageLog w WHERE w.responseTimestamp = :responseTimestamp"),
    @NamedQuery(name = "WsMessageLog.findByCreatedDate", query = "SELECT w FROM WsMessageLog w WHERE w.createdDate = :createdDate")})
public class WsMessageLog implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "WS_MESSAGE_LOG", nullable = false)
    private Integer wsMessageLog;
    @Basic(optional = false)
    @Column(name = "USERNAME", nullable = false, length = 50)
    private String username;
    @Basic(optional = false)
    @Column(name = "TX_ID", nullable = false, length = 50)
    private String txId;
    @Column(name = "TX_TYPE", length = 10)
    private String txType;
    @Column(name = "MSG_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date msgTimestamp;
    @Basic(optional = false)
    @Column(name = "REF_NO1", nullable = false, length = 20)
    private String refNo1;
    @Column(name = "REF_NO2", length = 20)
    private String refNo2;
    @Column(name = "BANK_CODE", length = 20)
    private String bankCode;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "AMOUNT", precision = 19, scale = 2)
    private BigDecimal amount;
    @Column(name = "RESPONSE_CODE", length = 20)
    private String responseCode;
    @Column(name = "BILL_CODE", length = 255)
    private String billCode;
    @Column(name = "RESPONSE_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseTimestamp;
    @Basic(optional = false)
    @Column(name = "CREATED_DATE", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    public WsMessageLog() {
    }

    public WsMessageLog(Integer wsMessageLog) {
        this.wsMessageLog = wsMessageLog;
    }

    public WsMessageLog(Integer wsMessageLog, String username, String txId, String refNo1, Date createdDate) {
        this.wsMessageLog = wsMessageLog;
        this.username = username;
        this.txId = txId;
        this.refNo1 = refNo1;
        this.createdDate = createdDate;
    }

    public Integer getWsMessageLog() {
        return wsMessageLog;
    }

    public void setWsMessageLog(Integer wsMessageLog) {
        this.wsMessageLog = wsMessageLog;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTxId() {
        return txId;
    }

    public void setTxId(String txId) {
        this.txId = txId;
    }

    public String getTxType() {
        return txType;
    }

    public void setTxType(String txType) {
        this.txType = txType;
    }

    public Date getMsgTimestamp() {
        return msgTimestamp;
    }

    public void setMsgTimestamp(Date msgTimestamp) {
        this.msgTimestamp = msgTimestamp;
    }

    public String getRefNo1() {
        return refNo1;
    }

    public void setRefNo1(String refNo1) {
        this.refNo1 = refNo1;
    }

    public String getRefNo2() {
        return refNo2;
    }

    public void setRefNo2(String refNo2) {
        this.refNo2 = refNo2;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    public Date getResponseTimestamp() {
        return responseTimestamp;
    }

    public void setResponseTimestamp(Date responseTimestamp) {
        this.responseTimestamp = responseTimestamp;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (wsMessageLog != null ? wsMessageLog.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof WsMessageLog)) {
            return false;
        }
        WsMessageLog other = (WsMessageLog) object;
        if ((this.wsMessageLog == null && other.wsMessageLog != null) || (this.wsMessageLog != null && !this.wsMessageLog.equals(other.wsMessageLog))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "th.go.cgd.epayment.entity.WsMessageLog[ wsMessageLog=" + wsMessageLog + " ]";
    }
    
}
