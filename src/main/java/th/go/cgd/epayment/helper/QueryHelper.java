package th.go.cgd.epayment.helper;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import th.go.cgd.epayment.util.OrmXmlManager;

@Component
public class QueryHelper 
{
	@Autowired
	private SessionFactory sessionFactory;
	
	public String getNamedQuery(String queryName)
	{
		return OrmXmlManager.getQuery(queryName);
	}
	
	public Session getCurrentSession()
	{
		Session session = null;
		
		try {
			session = sessionFactory.getCurrentSession();
		} catch (HibernateException ex) {
			session = sessionFactory.openSession();
		}
		
		return session;
	}
}
