package th.go.cgd.epayment.interceptor;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.LogAccess;
import th.go.cgd.epayment.entity.MLogAction;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.util.CatalogUtil;
import th.go.cgd.epayment.util.ClientMachineUtil;
import th.go.cgd.epayment.util.IPAddressUtil;

@Configuration
public class LogAccessInterceptor extends HandlerInterceptorAdapter {
	
	private static final String LOG_ACCESS = "LOG_ACCESS";
	private static final String ACCESS = "AC";
	
//	@Autowired
//	private LogAccessService logAccessService;
	
//	@Autowired
//	private CatalogUtil catalogUtil;
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws ServletException, IOException, Exception
	{
		HttpSession session = request.getSession();
		String userName = "-";
		
		
		if(session != null) {
			UserSession userSession = (UserSession) session.getAttribute(EPaymentConstant.USER_SESSION);
			if (userSession != null) {				
				userName = userSession.getUser().getUserName();
			}
		}

		String currentURL = request.getRequestURL().toString();
		
		String computerName = ClientMachineUtil.getClientMachineName(request);
		
//	    MLogAction mLogAction = catalogUtil.getObject(EPaymentConstant.M_LOG_ACTION, ACCESS);
//		LogAccess logAccess = new LogAccess();
//		logAccess.setUrl(currentURL);
//		logAccess.setUserName(userName);
//		logAccess.setClientIp(IPAddressUtil.getClientAddress(request));
//		logAccess.setClientName(computerName);
//		logAccess.setAccessDate(Calendar.getInstance().getTime());
//		logAccess.setMLogAction(mLogAction);
		
//		request.setAttribute(LOG_ACCESS, logAccess);
		
		return true;
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception 
	{
		super.afterCompletion(request, response, handler, ex);
		
		LogAccess logAccess = (LogAccess) request.getAttribute(LOG_ACCESS);
		
		if(logAccess != null) {
			logAccess.setStatus(String.valueOf(response.getStatus()));
			logAccess.setExecutionTime((short) TimeUnit.MILLISECONDS.toSeconds(Calendar.getInstance().getTimeInMillis() - logAccess.getAccessDate().getTime()));
			
//			logAccessService.save(logAccess);
			
			request.removeAttribute(LOG_ACCESS);
		}
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception 
	{
		super.postHandle(request, response, handler, modelAndView);
	}
	
}
