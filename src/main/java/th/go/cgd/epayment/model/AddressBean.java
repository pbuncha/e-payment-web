/**
 * 
 */
package th.go.cgd.epayment.model;

/**
 * @author wichuda.k Nov 27, 2017
 *
 */
public class AddressBean {
	private Integer id;
	private String no;
	private String village;
	private String moo;
	private String lane;
	private String soi;
	private Integer provinceId;
	private Integer amphurId;
	private Integer tambonId;
	private String posCode;
	private String road;
	private String roomNo;
	private String provinceName;
	private String amphurName;
	private String tambonName;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getMoo() {
		return moo;
	}
	public void setMoo(String moo) {
		this.moo = moo;
	}
	public String getLane() {
		return lane;
	}
	public void setLane(String lane) {
		this.lane = lane;
	}
	public String getSoi() {
		return soi;
	}
	public void setSoi(String soi) {
		this.soi = soi;
	}
	public Integer getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}
	public Integer getAmphurId() {
		return amphurId;
	}
	public void setAmphurId(Integer amphurId) {
		this.amphurId = amphurId;
	}
	public Integer getTambonId() {
		return tambonId;
	}
	public void setTambonId(Integer tambonId) {
		this.tambonId = tambonId;
	}
	public String getPosCode() {
		return posCode;
	}
	public void setPosCode(String posCode) {
		this.posCode = posCode;
	}
	public String getRoad() {
		return road;
	}
	public void setRoad(String road) {
		this.road = road;
	}
	public String getRoomNo() {
	    return roomNo;
	}
	public void setRoomNo(String roomNo) {
	    this.roomNo = roomNo;
	}
	public String getProvinceName() {
	    return provinceName;
	}
	public void setProvinceName(String provinceName) {
	    this.provinceName = provinceName;
	}
	public String getAmphurName() {
	    return amphurName;
	}
	public void setAmphurName(String amphurName) {
	    this.amphurName = amphurName;
	}
	public String getTambonName() {
	    return tambonName;
	}
	public void setTambonName(String tambonName) {
	    this.tambonName = tambonName;
	}
	
	
	

}
