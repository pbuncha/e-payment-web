package th.go.cgd.epayment.model;

/**
 * @author torgan.p 14 ธ.ค. 2560 17:48:00
 *
 */
public class AttachmentBean {
    	private Integer id;
	private byte[] data;
	private String filename;
	private String mimeType;
	private String path;
	public byte[] getData() {
	    return data;
	}
	public void setData(byte[] data) {
	    this.data = data;
	}
	public String getFilename() {
	    return filename;
	}
	public void setFilename(String filename) {
	    this.filename = filename;
	}
	public String getMimeType() {
	    return mimeType;
	}
	public void setMimeType(String mimeType) {
	    this.mimeType = mimeType;
	}
	public Integer getId() {
	    return id;
	}
	public void setId(Integer id) {
	    this.id = id;
	}
	public String getPath() {
	    return path;
	}
	public void setPath(String path) {
	    this.path = path;
	}
	
	
}
