package th.go.cgd.epayment.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

public class BatchFileReadResultBean {

    private Integer batchFileReadResultId;
    private Integer bankId;
    private Integer batchTypeId;
    private Date batchCreateDate;
    private String batchFileName;
    private Integer batchAllRow;
    private Integer batchAllAmount;
    private String status;
    private Date createDate;
    private Integer createBy;
    
    private String bankNameTH;
    private Date batchCreateDateFrom;
    private Date batchCreateDateTo;
    
    
    
	public Date getBatchCreateDateFrom() {
		return batchCreateDateFrom;
	}
	public void setBatchCreateDateFrom(Date batchCreateDateFrom) {
		this.batchCreateDateFrom = batchCreateDateFrom;
	}
	public Date getBatchCreateDateTo() {
		return batchCreateDateTo;
	}
	public void setBatchCreateDateTo(Date batchCreateDateTo) {
		this.batchCreateDateTo = batchCreateDateTo;
	}
	public String getBankNameTH() {
		return bankNameTH;
	}
	public void setBankNameTH(String bankNameTH) {
		this.bankNameTH = bankNameTH;
	}
	public Integer getBatchFileReadResultId() {
		return batchFileReadResultId;
	}
	public void setBatchFileReadResultId(Integer batchFileReadResultId) {
		this.batchFileReadResultId = batchFileReadResultId;
	}
	public Integer getBankId() {
		return bankId;
	}
	public void setBankId(Integer bankId) {
		this.bankId = bankId;
	}
	public Integer getBatchTypeId() {
		return batchTypeId;
	}
	public void setBatchTypeId(Integer batchTypeId) {
		this.batchTypeId = batchTypeId;
	}
	public Date getBatchCreateDate() {
		return batchCreateDate;
	}
	public void setBatchCreateDate(Date batchCreateDate) {
		this.batchCreateDate = batchCreateDate;
	}
	public String getBatchFileName() {
		return batchFileName;
	}
	public void setBatchFileName(String batchFileName) {
		this.batchFileName = batchFileName;
	}
	public Integer getBatchAllRow() {
		return batchAllRow;
	}
	public void setBatchAllRow(Integer batchAllRow) {
		this.batchAllRow = batchAllRow;
	}
	public Integer getBatchAllAmount() {
		return batchAllAmount;
	}
	public void setBatchAllAmount(Integer batchAllAmount) {
		this.batchAllAmount = batchAllAmount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Integer getCreateBy() {
		return createBy;
	}
	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}
    
    
}
