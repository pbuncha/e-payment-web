package th.go.cgd.epayment.model;

import java.math.BigDecimal;
import java.util.Date;

public class BillingBean {
	private String billingNo;
	private String invoiceNo;
	private BigDecimal totalAmount;
	private Date printingDate;
	private Date printingCopyDate;
	private Date createdDate;
	private String createdDateStr;
	private Integer invoiceId;
	private InvoiceBean invoice;
	private Integer billingId;
	private String status;
	private String statusName;
	private String isPreview;
	private String isCopy = "0"; //Default "0" (original)
	private String genCodePrint;
	private String printType = "D";
	
	private String printingDateStr;
	private String printingCopyDateStr;
	
	public String getBillingNo() {
		return billingNo;
	}

	public void setBillingNo(String billingNo) {
		this.billingNo = billingNo;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getPrintingDate() {
		return printingDate;
	}

	public void setPrintingDate(Date printingDate) {
		this.printingDate = printingDate;
	}

	public Date getPrintingCopyDate() {
		return printingCopyDate;
	}

	public void setPrintingCopyDate(Date printingCopyDate) {
		this.printingCopyDate = printingCopyDate;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public Integer getInvoiceId() {
	    return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
	    this.invoiceId = invoiceId;
	}

	public InvoiceBean getInvoice() {
		return invoice;
	}

	public void setInvoice(InvoiceBean invoice) {
		this.invoice = invoice;
	}

	public Integer getBillingId() {
		return billingId;
	}

	public void setBillingId(Integer billingId) {
		this.billingId = billingId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public String getCreatedDateStr() {
		return createdDateStr;
	}

	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}

	public String getIsPreview() {
		return isPreview;
	}

	public void setIsPreview(String isPreview) {
		this.isPreview = isPreview;
	}

	public String getIsCopy() {
		return isCopy;
	}

	public void setIsCopy(String isCopy) {
		this.isCopy = isCopy;
	}

	public String getGenCodePrint() {
		return genCodePrint;
	}

	public void setGenCodePrint(String genCodePrint) {
		this.genCodePrint = genCodePrint;
	}

	public String getPrintType() {
		return printType;
	}

	public void setPrintType(String printType) {
		this.printType = printType;
	}

	public String getPrintingDateStr() {
		return printingDateStr;
	}

	public void setPrintingDateStr(String printingDateStr) {
		this.printingDateStr = printingDateStr;
	}

	public String getPrintingCopyDateStr() {
		return printingCopyDateStr;
	}

	public void setPrintingCopyDateStr(String printingCopyDateStr) {
		this.printingCopyDateStr = printingCopyDateStr;
	}

}
