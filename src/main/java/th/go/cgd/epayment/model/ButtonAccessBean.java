package th.go.cgd.epayment.model;

public class ButtonAccessBean {

	private Boolean pdf = false;
	private Boolean view = false;
	private Boolean edit = false;
	private Boolean excel = false;
	
	
	public Boolean getPdf() {
		return pdf;
	}
	public void setPdf(Boolean pdf) {
		this.pdf = pdf;
	}
	public Boolean getView() {
		return view;
	}
	public void setView(Boolean view) {
		this.view = view;
	}
	public Boolean getEdit() {
		return edit;
	}
	public void setEdit(Boolean edit) {
		this.edit = edit;
	}
	public Boolean getExcel() {
		return excel;
	}
	public void setExcel(Boolean excel) {
		this.excel = excel;
	}
	
	
}
