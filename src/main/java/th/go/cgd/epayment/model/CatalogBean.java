package th.go.cgd.epayment.model;

import java.util.Date;
import java.util.List;

public class CatalogBean {

	private String id;
	private String catalogCode;
	private String catalogName;
	private String catalogTypeId;
	private String catalogTypeName;
	private String revenueTypeId;
	private String revenueTypeName;
	private String isAmount;
	private String amount;
	private String isShow;
	private Date startDate;
	private Date endDate;
	private String startDateStr;
	private String endDateStr;
	private String isBreak;
	private Date breakDate;
	private String breakReason;
	private List<CatalogConditionTextBean> conditionText;
	private List<CatalogCostCenterBean> costCenter;
	private List<CatalogStructureItemBean> structureItem;
	private String status;
	private String isDelete;
	private String version;
	List<DropdownBean> costCenterList;
	private String isCopy;
	private String isEdit;
	private String statusUse;
	private String statusApprove;
	private String statusUpdate;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCatalogCode() {
		return catalogCode;
	}
	public void setCatalogCode(String catalogCode) {
		this.catalogCode = catalogCode;
	}
	public String getCatalogName() {
		return catalogName;
	}
	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}
	public String getCatalogTypeId() {
		return catalogTypeId;
	}
	public void setCatalogTypeId(String catalogTypeId) {
		this.catalogTypeId = catalogTypeId;
	}
	public String getCatalogTypeName() {
		return catalogTypeName;
	}
	public void setCatalogTypeName(String catalogTypeName) {
		this.catalogTypeName = catalogTypeName;
	}
	public String getRevenueTypeId() {
		return revenueTypeId;
	}
	public void setRevenueTypeId(String revenueTypeId) {
		this.revenueTypeId = revenueTypeId;
	}
	public String getRevenueTypeName() {
		return revenueTypeName;
	}
	public void setRevenueTypeName(String revenueTypeName) {
		this.revenueTypeName = revenueTypeName;
	}
	public String getIsAmount() {
		return isAmount;
	}
	public void setIsAmount(String isAmount) {
		this.isAmount = isAmount;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getIsShow() {
		return isShow;
	}
	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStartDateStr() {
		return startDateStr;
	}
	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}
	public String getEndDateStr() {
		return endDateStr;
	}
	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}
	public String getIsBreak() {
		return isBreak;
	}
	public void setIsBreak(String isBreak) {
		this.isBreak = isBreak;
	}
	public Date getBreakDate() {
		return breakDate;
	}
	public void setBreakDate(Date breakDate) {
		this.breakDate = breakDate;
	}
	public String getBreakReason() {
		return breakReason;
	}
	public void setBreakReason(String breakReason) {
		this.breakReason = breakReason;
	}
	public List<CatalogConditionTextBean> getConditionText() {
		return conditionText;
	}
	public void setConditionText(List<CatalogConditionTextBean> conditionText) {
		this.conditionText = conditionText;
	}
	public List<CatalogCostCenterBean> getCostCenter() {
		return costCenter;
	}
	public void setCostCenter(List<CatalogCostCenterBean> costCenter) {
		this.costCenter = costCenter;
	}
	public List<CatalogStructureItemBean> getStructureItem() {
		return structureItem;
	}
	public void setStructureItem(List<CatalogStructureItemBean> structureItem) {
		this.structureItem = structureItem;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getIsDelete() {
		return isDelete;
	}
	public void setIsDelete(String isDelete) {
		this.isDelete = isDelete;
	}
	public String getVersion() {
	    return version;
	}
	public void setVersion(String version) {
	    this.version = version;
	}
	public List<DropdownBean> getCostCenterList() {
	    return costCenterList;
	}
	public void setCostCenterList(List<DropdownBean> costCenterList) {
	    this.costCenterList = costCenterList;
	}
	public String getIsCopy() {
		return isCopy;
	}
	public void setIsCopy(String isCopy) {
		this.isCopy = isCopy;
	}
	public String getIsEdit() {
		return isEdit;
	}
	public void setIsEdit(String isEdit) {
		this.isEdit = isEdit;
	}
	public String getStatusUse() {
		return statusUse;
	}
	public void setStatusUse(String statusUse) {
		this.statusUse = statusUse;
	}
	public String getStatusApprove() {
		return statusApprove;
	}
	public void setStatusApprove(String statusApprove) {
		this.statusApprove = statusApprove;
	}
	public String getStatusUpdate() {
		return statusUpdate;
	}
	public void setStatusUpdate(String statusUpdate) {
		this.statusUpdate = statusUpdate;
	}
	
}
