package th.go.cgd.epayment.model;

public class CatalogConditionTextBean {

	private String id;
	private String catalogId;
	private String conditionText;
	private String displayNo;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCatalogId() {
		return catalogId;
	}
	public void setCatalogId(String catalogId) {
		this.catalogId = catalogId;
	}
	public String getConditionText() {
		return conditionText;
	}
	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}
	public String getDisplayNo() {
		return displayNo;
	}
	public void setDisplayNo(String displayNo) {
		this.displayNo = displayNo;
	}
	
}
