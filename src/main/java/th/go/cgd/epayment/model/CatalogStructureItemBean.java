package th.go.cgd.epayment.model;

public class CatalogStructureItemBean {

	private String id;
	private String revenueTypeId;
	private String levyTypeId;
	private String percent;
	private String costCenterId;
	private String costCenterCode;
	private String costCenterName;
	private String departmentId;
	private String departmentCode;
	private String departmentName;
	private String glId;
	private String glCode;
	private String glName;
	private String moneySourceId;
	private String moneySourceCode;
	private String moneySourceName;
	private String accountDepositId;
	private String accountDepositCode;
	private String accountDepositName;
	private String accountDepositOwner;
	private String standardRevernueId;
	private String standardRevernueCode;
	private String standardRevernueName;
	private String budgetCode;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRevenueTypeId() {
		return revenueTypeId;
	}
	public void setRevenueTypeId(String revenueTypeId) {
		this.revenueTypeId = revenueTypeId;
	}
	public String getLevyTypeId() {
		return levyTypeId;
	}
	public void setLevyTypeId(String levyTypeId) {
		this.levyTypeId = levyTypeId;
	}
	public String getPercent() {
		return percent;
	}
	public void setPercent(String percent) {
		this.percent = percent;
	}
	public String getCostCenterId() {
		return costCenterId;
	}
	public void setCostCenterId(String costCenterId) {
		this.costCenterId = costCenterId;
	}
	public String getCostCenterCode() {
		return costCenterCode;
	}
	public void setCostCenterCode(String costCenterCode) {
		this.costCenterCode = costCenterCode;
	}
	public String getCostCenterName() {
		return costCenterName;
	}
	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public String getGlId() {
		return glId;
	}
	public void setGlId(String glId) {
		this.glId = glId;
	}
	public String getGlCode() {
		return glCode;
	}
	public void setGlCode(String glCode) {
		this.glCode = glCode;
	}
	public String getGlName() {
		return glName;
	}
	public void setGlName(String glName) {
		this.glName = glName;
	}
	public String getMoneySourceId() {
		return moneySourceId;
	}
	public void setMoneySourceId(String moneySourceId) {
		this.moneySourceId = moneySourceId;
	}
	public String getMoneySourceCode() {
		return moneySourceCode;
	}
	public void setMoneySourceCode(String moneySourceCode) {
		this.moneySourceCode = moneySourceCode;
	}
	public String getMoneySourceName() {
		return moneySourceName;
	}
	public void setMoneySourceName(String moneySourceName) {
		this.moneySourceName = moneySourceName;
	}
	public String getAccountDepositId() {
		return accountDepositId;
	}
	public void setAccountDepositId(String accountDepositId) {
		this.accountDepositId = accountDepositId;
	}
	public String getAccountDepositCode() {
		return accountDepositCode;
	}
	public void setAccountDepositCode(String accountDepositCode) {
		this.accountDepositCode = accountDepositCode;
	}
	public String getAccountDepositName() {
		return accountDepositName;
	}
	public void setAccountDepositName(String accountDepositName) {
		this.accountDepositName = accountDepositName;
	}
	public String getAccountDepositOwner() {
		return accountDepositOwner;
	}
	public void setAccountDepositOwner(String accountDepositOwner) {
		this.accountDepositOwner = accountDepositOwner;
	}
	public String getStandardRevernueId() {
		return standardRevernueId;
	}
	public void setStandardRevernueId(String standardRevernueId) {
		this.standardRevernueId = standardRevernueId;
	}
	public String getStandardRevernueCode() {
		return standardRevernueCode;
	}
	public void setStandardRevernueCode(String standardRevernueCode) {
		this.standardRevernueCode = standardRevernueCode;
	}
	public String getStandardRevernueName() {
		return standardRevernueName;
	}
	public void setStandardRevernueName(String standardRevernueName) {
		this.standardRevernueName = standardRevernueName;
	}
	public String getBudgetCode() {
		return budgetCode;
	}
	public void setBudgetCode(String budgetCode) {
		this.budgetCode = budgetCode;
	}
	

}
