package th.go.cgd.epayment.model;
/**
 * @author scOnTz
 * 
 * **/
public class CreatedInvoiceBean {
	private String email;
	private String citizenid;
	private String mobile;
	private String firstName;
	private String lastName;
	private Integer userType;

	private String buildingName;
	private String moo;
	private String no;
	private String road;
	private String soi;
	private String village;
	private Integer provinceId;
	private Integer districtId;
	private Integer subdistrictId;
	private String postcode;
	private String telephone;
	
	private String citizenid1; 

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCitizenid() {
		return citizenid;
	}

	public void setCitizenid(String citizenid) {
		this.citizenid = citizenid;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getMoo() {
		return moo;
	}

	public void setMoo(String moo) {
		this.moo = moo;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public String getSoi() {
		return soi;
	}

	public void setSoi(String soi) {
		this.soi = soi;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public Integer getProvinceId() {
		return provinceId;
	}

	public void setProvinceId(Integer provinceId) {
		this.provinceId = provinceId;
	}

	public Integer getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Integer districtId) {
		this.districtId = districtId;
	}

	public Integer getSubdistrictId() {
		return subdistrictId;
	}

	public void setSubdistrictId(Integer subdistrictId) {
		this.subdistrictId = subdistrictId;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getCitizenid1() {
	    return citizenid1;
	}

	public void setCitizenid1(String citizenid1) {
	    this.citizenid1 = citizenid1;
	}

	@Override
	public String toString() {
		return "CreatedInvoiceBean [email=" + email + ", citizenid=" + citizenid + ", mobile=" + mobile + ", firstName="
				+ firstName + ", lastName=" + lastName + ", userType=" + userType + ", buildingName=" + buildingName
				+ ", moo=" + moo + ", no=" + no + ", road=" + road + ", soi=" + soi + ", village=" + village
				+ ", provinceId=" + provinceId + ", districtId=" + districtId + ", subdistrictId=" + subdistrictId
				+ ", postcode=" + postcode + ", telephone=" + telephone + "]";
	}

}
