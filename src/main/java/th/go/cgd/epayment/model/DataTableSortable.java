package th.go.cgd.epayment.model;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import th.go.cgd.epayment.util.datatable.AnnotationResultTransformer;

public class DataTableSortable {
	
	public static class Order {
		int colNo;
		String colDir;
		private Order(int a, String b) {
			colNo = a;
			colDir = b;
		}
	}
	
	public List<Order> orders;
	public List<String> colNames;
	public int skip;
	public int length;
	
	public void setOrderByCause(StringBuilder builder, Class<?> clazz) {
		if (orders.isEmpty()) {
			return;
		}
		AnnotationResultTransformer art = new AnnotationResultTransformer(clazz);
		boolean first = true;
		for (Order order : orders) {
			if (first) {
				builder.append(" order by ");
				first = false;
			} else {
				builder.append(", ");
			}
			builder.append(art.getColumnNameFromPropertyName(colNames.get(order.colNo)));
			if (order.colDir.equals("asc")) {
				builder.append(" ASC");
			} else {
				builder.append(" DESC");
			}
		}
	}
	
	public static DataTableSortable getRequestSortable() {
		DataTableSortable result = new DataTableSortable();
		ServletRequestAttributes servlet = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = servlet.getRequest();
		
		int colLength = Integer.parseInt(request.getParameter("iColumns"));
		result.colNames = new ArrayList<>(colLength);
		for (int i = 0; i < colLength; i++) {
			result.colNames.add(request.getParameter("mDataProp_" + i));
		}
		
		result.skip = Integer.parseInt(request.getParameter("iDisplayStart"));
		result.length = Integer.parseInt(request.getParameter("iDisplayLength"));
		
		int orderLength = Integer.parseInt(request.getParameter("iSortingCols"));
		result.orders = new ArrayList<>(orderLength);
		for (int i = 0; i < orderLength; i++) {
			result.orders.add(new Order(Integer.parseInt(request.getParameter("iSortCol_" + i)), request.getParameter("sSortDir_" + i)));
		}
		
		return result;
	}
}
