package th.go.cgd.epayment.model;

/**
 * @author torgan.p 14 ต.ค. 2560 22:36:40
 *
 */
public class DropdownBean {
    private String id;
    private String desc;
    private String desc2;
    private String desc3;
    
    public DropdownBean(){
    	
    }
    public DropdownBean(String id, String desc, String desc2, String desc3) {
		super();
		this.id = id;
		this.desc = desc;
		this.desc2 = desc2;
		this.desc3 = desc3;
	}
    
    
	public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getDesc2() {
        return desc2;
    }
    public void setDesc2(String desc2) {
        this.desc2 = desc2;
    }
    public String getDesc3() {
        return desc3;
    }
    public void setDesc3(String desc3) {
        this.desc3 = desc3;
    }
    
    
}
