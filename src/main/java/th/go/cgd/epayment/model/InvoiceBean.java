package th.go.cgd.epayment.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class InvoiceBean {
	private Integer id;
	private Integer persTypeId;
	private String citizenNo;
	private Integer titleId;
	private String firstName;
	private String midName;
	private String lastName;
	private Integer titleEnId;
	private String firstNameEn;
	private String midNameEn;
	private String lastNameEn;
	private String compName;
	private String groupName;
	private String mobile;
	private String email;
	private Date startDate;
	private Date endDate;
	private String startDateStr;
	private String endDateStr;
	private BigDecimal totalAmount;
	private String invoiceNo;
	private String ref1;
	private String ref2;
	private String name;
	private String status;
	private Integer departmentId;
	private Integer costCenterPayId;
	private Integer costCenterId;
	private Integer catalogId;
	private String itemName;
	private List<InvoiceItemBean> items;
	private AddressBean addrHome;
	private AddressBean addrBilling;
	private Boolean homeIsBill;
	private List<InvoiceItemBean> deleteItem;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCitizenNo() {
		return citizenNo;
	}

	public void setCitizenNo(String citizenNo) {
		this.citizenNo = citizenNo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMidName() {
		return midName;
	}

	public void setMidName(String midName) {
		this.midName = midName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstNameEn() {
		return firstNameEn;
	}

	public void setFirstNameEn(String firstNameEn) {
		this.firstNameEn = firstNameEn;
	}

	public String getMidNameEn() {
		return midNameEn;
	}

	public void setMidNameEn(String midNameEn) {
		this.midNameEn = midNameEn;
	}

	public String getLastNameEn() {
		return lastNameEn;
	}

	public void setLastNameEn(String lastNameEn) {
		this.lastNameEn = lastNameEn;
	}

	public String getCompName() {
		return compName;
	}

	public void setCompName(String compName) {
		this.compName = compName;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<InvoiceItemBean> getItems() {
		return items;
	}

	public void setItems(List<InvoiceItemBean> items) {
		this.items = items;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Integer getPersTypeId() {
		return persTypeId;
	}

	public void setPersTypeId(Integer persTypeId) {
		this.persTypeId = persTypeId;
	}

	public Integer getTitleId() {
		return titleId;
	}

	public void setTitleId(Integer titleId) {
		this.titleId = titleId;
	}

	public Integer getTitleEnId() {
		return titleEnId;
	}

	public void setTitleEnId(Integer titleEnId) {
		this.titleEnId = titleEnId;
	}

	public String getRef1() {
		return ref1;
	}

	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}

	public String getRef2() {
		return ref2;
	}

	public void setRef2(String ref2) {
		this.ref2 = ref2;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public Integer getCostCenterPayId() {
		return costCenterPayId;
	}

	public void setCostCenterPayId(Integer costCenterPayId) {
		this.costCenterPayId = costCenterPayId;
	}

	public Integer getCostCenterId() {
		return costCenterId;
	}

	public void setCostCenterId(Integer costCenterId) {
		this.costCenterId = costCenterId;
	}

	public Integer getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Integer catalogId) {
		this.catalogId = catalogId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public AddressBean getAddrHome() {
		return addrHome;
	}

	public void setAddrHome(AddressBean addrHome) {
		this.addrHome = addrHome;
	}
	public AddressBean getAddrBilling() {
		return addrBilling;
	}

	public void setAddrBilling(AddressBean addrBilling) {
		this.addrBilling = addrBilling;
	}

	public Boolean getHomeIsBill() {
		return homeIsBill;
	}

	public void setHomeIsBill(Boolean homeIsBill) {
		this.homeIsBill = homeIsBill;
	}

	public List<InvoiceItemBean> getDeleteItem() {
		return deleteItem;
	}

	public void setDeleteItem(List<InvoiceItemBean> deleteItem) {
		this.deleteItem = deleteItem;
	}

	public String getStartDateStr() {
		return startDateStr;
	}

	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}

	public String getEndDateStr() {
		return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}

}
