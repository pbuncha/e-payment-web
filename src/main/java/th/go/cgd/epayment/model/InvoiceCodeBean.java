package th.go.cgd.epayment.model;

public class InvoiceCodeBean {

	private String ref1;
	private String ref2;
	private String invoiceCode;
	
	public InvoiceCodeBean() {
		
	}
	public InvoiceCodeBean(String ref1, String ref2, String invoiceCode) {
		super();
		this.ref1 = ref1;
		this.ref2 = ref2;
		this.invoiceCode = invoiceCode;
	}
	public String getRef1() {
		return ref1;
	}
	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}
	public String getRef2() {
		return ref2;
	}
	public void setRef2(String ref2) {
		this.ref2 = ref2;
	}
	public String getInvoiceCode() {
		return invoiceCode;
	}
	public void setInvoiceCode(String invoiceCode) {
		this.invoiceCode = invoiceCode;
	}
	
	
}
