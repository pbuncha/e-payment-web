package th.go.cgd.epayment.model;

import java.math.BigDecimal;
import java.util.Date;

public class InvoiceItemBean {
	private Integer invoiceId;
	private BigDecimal totalAmount;
	private String invoiceNo;
	private String ref1;
	private String ref2;
	private Date endDate;
	private Integer departmentId;
	private String departmentName;
	private Integer id;
	private Integer catalogId;
	private Integer mCatalogId;
	private String catalogCode;
	private String itemName;
	private BigDecimal amount;
	private String catalogName;
	private Date breakDate;
	private String breakReason;
	private String conditionText;
	private boolean isFavorite;
	private Integer catalogTypeId;
	private String endDateStr;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCatalogId() {
		return catalogId;
	}

	public void setCatalogId(Integer catalogId) {
		this.catalogId = catalogId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public String getCatalogName() {
		return catalogName;
	}

	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}

	public Date getBreakDate() {
		return breakDate;
	}

	public void setBreakDate(Date breakDate) {
		this.breakDate = breakDate;
	}

	public String getBreakReason() {
		return breakReason;
	}

	public void setBreakReason(String breakReason) {
		this.breakReason = breakReason;
	}

	public String getCatalogCode() {
		return catalogCode;
	}

	public void setCatalogCode(String catalogCode) {
		this.catalogCode = catalogCode;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getRef1() {
		return ref1;
	}

	public void setRef1(String ref1) {
		this.ref1 = ref1;
	}

	public String getRef2() {
		return ref2;
	}

	public void setRef2(String ref2) {
		this.ref2 = ref2;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public String getConditionText() {
		return conditionText;
	}

	public void setConditionText(String conditionText) {
		this.conditionText = conditionText;
	}

	public boolean isFavorite() {
		return isFavorite;
	}

	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}

	public Integer getCatalogTypeId() {
	    return catalogTypeId;
	}

	public void setCatalogTypeId(Integer catalogTypeId) {
	    this.catalogTypeId = catalogTypeId;
	}

	public Integer getmCatalogId() {
		return mCatalogId;
	}

	public void setmCatalogId(Integer mCatalogId) {
		this.mCatalogId = mCatalogId;
	}

	public String getEndDateStr() {
	    return endDateStr;
	}

	public void setEndDateStr(String endDateStr) {
	    this.endDateStr = endDateStr;
	}


	
	
}
