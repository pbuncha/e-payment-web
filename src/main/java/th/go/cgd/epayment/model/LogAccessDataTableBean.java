package th.go.cgd.epayment.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import th.go.cgd.epayment.util.json_serializer.JsonDateTimeSerializer;

public class LogAccessDataTableBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8610508268205156196L;
	private String userName;
	private Date accessDate;
	private String clientIp;
	private String action;
	private String url;
	private Short executeTime;
	private String status;
	
	@Column(name="USERNAME")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@JsonSerialize(using=JsonDateTimeSerializer.class)
	@Column(name="ACCESS_DATE")
	public Date getAccessDate() {
		return accessDate;
	}
	public void setAccessDate(Date accessDate) {
		this.accessDate = accessDate;
	}
	@Column(name="CLIENT_IP")
	public String getClientIp() {
		return clientIp;
	}
	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}
	@Column(name="ACTION_NAME")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Column(name="URL")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	@Column(name="EXECUTE_TIME")
	public Short getExecuteTime() {
		return executeTime;
	}
	public void setExecuteTime(Short executeTime) {
		this.executeTime = executeTime;
	}
	@Column(name="STATUS")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	

}
