package th.go.cgd.epayment.model;

import java.util.Date;

public class MPermissionGroupBean {

    private Integer mPermissionGroupId;
    private Date createDate;
    private String description;
    private String mPermissionGroupName;
    private Date updateDate;
    private String mPermissionGroupCode;
    private Integer updateBy;
    private Integer createBy;
    private String status;
    
    private Integer statusId;
    
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public Integer getmPermissionGroupId() {
		return mPermissionGroupId;
	}
	public void setmPermissionGroupId(Integer mPermissionGroupId) {
		this.mPermissionGroupId = mPermissionGroupId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getmPermissionGroupName() {
		return mPermissionGroupName;
	}
	public void setmPermissionGroupName(String mPermissionGroupName) {
		this.mPermissionGroupName = mPermissionGroupName;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getmPermissionGroupCode() {
		return mPermissionGroupCode;
	}
	public void setmPermissionGroupCode(String mPermissionGroupCode) {
		this.mPermissionGroupCode = mPermissionGroupCode;
	}
	public Integer getUpdateBy() {
		return updateBy;
	}
	public void setUpdateBy(Integer updateBy) {
		this.updateBy = updateBy;
	}
	public Integer getCreateBy() {
		return createBy;
	}
	public void setCreateBy(Integer createBy) {
		this.createBy = createBy;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

    
    
    
}
