package th.go.cgd.epayment.model;

import java.util.ArrayList;
import java.util.List;

public class MPermissionGroupModel {

	private Integer mPermissionGroupId;

	private String mPermissionGroupCode;

	private String mPermissionGroupName;

	private String description;

	private String status;

	private String statusName;
	
	private String statusUserPermissionGroup;
	
	private String userFullName;

	private List<Integer> checkPermissionAll;

	public Integer getmPermissionGroupId() {
		return mPermissionGroupId;
	}

	public void setmPermissionGroupId(Integer mPermissionGroupId) {
		this.mPermissionGroupId = mPermissionGroupId;
	}

	public String getmPermissionGroupName() {
		return mPermissionGroupName;
	}

	public void setmPermissionGroupName(String mPermissionGroupName) {
		this.mPermissionGroupName = mPermissionGroupName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public List<Integer> getCheckPermissionAll() {
		if (null == checkPermissionAll) {
			checkPermissionAll = new ArrayList<Integer>();
		}
		return checkPermissionAll;
	}

	public void setCheckPermissionAll(List<Integer> checkPermissionAll) {
		if (null == checkPermissionAll) {
			this.checkPermissionAll = new ArrayList<Integer>();
		} else {
			this.checkPermissionAll = checkPermissionAll;
		}
	}

	public String getmPermissionGroupCode() {
		return mPermissionGroupCode;
	}

	public void setmPermissionGroupCode(String mPermissionGroupCode) {
		this.mPermissionGroupCode = mPermissionGroupCode;
	}



	public String getUserFullName() {
	    return userFullName;
	}

	public void setUserFullName(String userFullName) {
	    this.userFullName = userFullName;
	}

	public String getStatusUserPermissionGroup() {
	    return statusUserPermissionGroup;
	}

	public void setStatusUserPermissionGroup(String statusUserPermissionGroup) {
	    this.statusUserPermissionGroup = statusUserPermissionGroup;
	}

	@Override
	public String toString() {
		return "MPermissionGroupModel{" + "mPermissionGroupId=" + mPermissionGroupId + ", mPermissionGroupCode='"
				+ mPermissionGroupCode + '\'' + ", mPermissionGroupName='" + mPermissionGroupName + '\''
				+ ", description='" + description + '\'' + ", status='" + status + '\'' + ", checkPermissionAll="
				+ checkPermissionAll + '}';
	}
}
