package th.go.cgd.epayment.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.Menu;

public class MenuItem implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8214955099381136375L;
	
	private int menuId;
	private String name;
	private String url;
	private Short orderNo;
	private Long parentId;
	private SubMenuItem subMenuItem;
	private String menuIcon;
	private boolean childIsMenuFlow;
	
	public MenuItem(Menu menu)
    {
		this.name = menu.getMenuName();
        this.orderNo = menu.getOrderNo();
        this.menuId = menu.getMenuId();
        this.url = menu.getUrl();
        this.menuIcon = menu.getMenuIcon();
//        this.childIsMenuFlow = (menu.getMenuFlows().size()>0);
    }
	
	public MenuItem locateChild(int id)
    {
		MenuItem child = null;

        if (null != this.subMenuItem)
        {
            for (MenuItem menuItem : this.subMenuItem.itemdata)
            {
                if (id == menuItem.menuId)
                {
                    child = menuItem;
                    break;
                }
                else
                {
                    MenuItem menuItemTmp = menuItem.locateChild(id);

                    if (null != menuItemTmp)
                    {
                        child = menuItemTmp;
                        break;
                    }
                }
            }
        }
        
        return child;
    }
	
	public int getMenuId() {
		return menuId;
	}
	public void setMenuId(int menuId) {
		this.menuId = menuId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Short getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(Short orderNo) {
		this.orderNo = orderNo;
	}
	public Long getParentId() {
		return parentId;
	}
	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}
	public SubMenuItem getSubMenuItem() {
		return subMenuItem;
	}
	public void setSubMenuItem(SubMenuItem subMenuItem) {
		this.subMenuItem = subMenuItem;
	}
	public String getMenuIcon() {
		return menuIcon;
	}
	public void setMenuIcon(String menuIcon) {
		this.menuIcon = menuIcon;
	}
	public boolean isChildIsMenuFlow() {
		return childIsMenuFlow;
	}

	public void setChildIsMenuFlow(boolean childIsMenuFlow) {
		this.childIsMenuFlow = childIsMenuFlow;
	}

	public boolean contains(int id)
    {
        if (null != this.subMenuItem)
        {
            for (MenuItem menuItem : this.subMenuItem.itemdata)
            {
                if ((menuItem.menuId == id) || menuItem.contains(id))
                {
                    return true;
                }
            }
        }
        return false;
    }
	
	public MenuItem addChild(MenuItem child)
    {
        if (null == this.subMenuItem)
        {
            this.subMenuItem = new SubMenuItem(String.valueOf(child.getMenuId()));
        }
        return this.subMenuItem.addItem(child);
    }
	
	@Override
	public String toString() {
		return "MenuItem [menuId=" + menuId + ", name=" + name + ", url=" + url
				+ ", orderNo=" + orderNo + ", parentId=" + parentId + ", subMenuItem=" + subMenuItem + ", menuIcon="
				+ menuIcon + "]";
	}

	public class SubMenuItem
    {
        List<MenuItem> itemdata = new ArrayList<MenuItem>();
        String id;
        String parentId;

        SubMenuItem(String parentId)
        {
            this.parentId = parentId;
            this.id = String.format("subMenuItem-%1$s", UUID.randomUUID());
        }

        SubMenuItem(String parentId, Set<Menu> menus)
        {
            this(parentId);
            for (Menu menu : menus)
            {
                if (null!=menu.getStatus() && EPaymentConstant.STATUS_ACTIVE.equals(menu.getStatus()))
                {
                    itemdata.add(new MenuItem(menu));
                }
            }
            Collections.sort(itemdata, new ItemComparator());
        }
        
        public List<MenuItem> getItemdata() {
			return itemdata;
		}

		public void setItemdata(List<MenuItem> itemdata) {
			this.itemdata = itemdata;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getParentId() {
			return parentId;
		}

		public void setParentId(String parentId) {
			this.parentId = parentId;
		}

		public MenuItem addItem(MenuItem item)
        {
            this.itemdata.add(item);
            Collections.sort(this.itemdata, new ItemComparator());
            return item;
        }

		@Override
		public String toString() {
			return "SubMenuItem [itemdata=" + itemdata + ", id=" + id
					+ ", parentId=" + parentId + "]";
		}
    }
	
	public static class ItemComparator implements Comparator<MenuItem>
    {
        public int compare(MenuItem o1, MenuItem o2)
        {
        	return o1.getOrderNo().compareTo(o2.getOrderNo());
        }
    }
}
