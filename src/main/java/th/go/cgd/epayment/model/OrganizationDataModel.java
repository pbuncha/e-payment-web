package th.go.cgd.epayment.model;

import java.io.Serializable;

import th.go.cgd.epayment.util.datatable.TransformIgnore;
import th.go.cgd.epayment.util.datatable.TransformTarget;

@TransformIgnore(needAllSource=false, needAllTarget=true)
public class OrganizationDataModel implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2821940544753727422L;

	@TransformTarget("ADDRESS")
	public String address;
	@TransformTarget("PROVINCE_ID")
	public Short provinceId;
	@TransformTarget("PROVINCE_NAME")
	public String provinceName;
	@TransformTarget("DISTRICT_ID")
	public Short districtId;
	@TransformTarget("DISTRICT_NAME")
	public String districtName;
	@TransformTarget("SUBDISTRICT_ID")
	public Short subDistrictId;
	@TransformTarget("SUBDISTRICT_NAME")
	public String subDistrictName;
	@TransformTarget("ZIP_CODE")
	public String zipCode;
}
