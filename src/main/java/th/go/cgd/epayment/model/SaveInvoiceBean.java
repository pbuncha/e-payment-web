package th.go.cgd.epayment.model;

import java.util.List;

public class SaveInvoiceBean {
	private CreatedInvoiceBean invoice;
	private List<InvoiceItemBean> invoiceItemList;

	public CreatedInvoiceBean getInvoice() {
		return invoice;
	}

	public void setInvoice(CreatedInvoiceBean invoice) {
		this.invoice = invoice;
	}

	public List<InvoiceItemBean> getInvoiceItemList() {
		return invoiceItemList;
	}

	public void setInvoiceItemList(List<InvoiceItemBean> invoiceItemList) {
		this.invoiceItemList = invoiceItemList;
	}

}
