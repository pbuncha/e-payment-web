package th.go.cgd.epayment.model;

import java.util.Date;

/**
 * @author torgan.p 3 ธ.ค. 2560 21:29:30
 *
 */
public class SearchBean {
    private Date startDate;
    private Date endDate;
    private Integer departmentId;
    private Integer catalogTypeId;
    private Integer catalogId;
    private Integer userId;
    private String statusUse;
    private String statusApprove;
    private String catalogName;
    private String catalogCode;
    private Date dateStartForm;
    private Date dateStartTo;
    private Date dateEndFrom;
    private Date dateEndTo;
    private String catalogTypeName;
    private InvoiceBean invoice;
    private BillingBean billing ;
    private Integer revenueTypeId;
    private Integer mCatalogId;
    private String pending;
    
    private int begin;
    private int end;
	
    private Date billStartDate;
    private Date billEndDate;
    private Integer costCenterId;
    
    private String userType;
    private String citizenid;
    private String name;
    private Integer businessAreaId;
    
    private Integer disbursementUnitId;
    
    public Date getStartDate() {
        return startDate;
    }
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    public Date getEndDate() {
        return endDate;
    }
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    public Integer getDepartmentId() {
        return departmentId;
    }
    public void setDepartmentId(Integer departmentId) {
        this.departmentId = departmentId;
    }
    public Integer getCatalogTypeId() {
        return catalogTypeId;
    }
    public void setCatalogTypeId(Integer catalogTypeId) {
        this.catalogTypeId = catalogTypeId;
    }
    public Integer getCatalogId() {
        return catalogId;
    }
    public void setCatalogId(Integer catalogId) {
        this.catalogId = catalogId;
    }
    public Integer getUserId() {
        return userId;
    }
    public void setUserId(Integer userId) {
        this.userId = userId;
    }
    public Date getDateStartForm() {
        return dateStartForm;
    }
    public void setDateStartForm(Date dateStartForm) {
        this.dateStartForm = dateStartForm;
    }
    public Date getDateStartTo() {
        return dateStartTo;
    }
    public void setDateStartTo(Date dateStartTo) {
        this.dateStartTo = dateStartTo;
    }
    public Date getDateEndFrom() {
        return dateEndFrom;
    }
    public void setDateEndFrom(Date dateEndFrom) {
        this.dateEndFrom = dateEndFrom;
    }
    public Date getDateEndTo() {
        return dateEndTo;
    }
    public void setDateEndTo(Date dateEndTo) {
        this.dateEndTo = dateEndTo;
    }
    public String getCatalogTypeName() {
        return catalogTypeName;
    }
    public void setCatalogTypeName(String catalogTypeName) {
        this.catalogTypeName = catalogTypeName;
    }
    public String getStatusUse() {
        return statusUse;
    }
    public void setStatusUse(String statusUse) {
        this.statusUse = statusUse;
    }
    public String getStatusApprove() {
        return statusApprove;
    }
    public void setStatusApprove(String statusApprove) {
        this.statusApprove = statusApprove;
    }
    public String getCatalogName() {
        return catalogName;
    }
    public void setCatalogName(String catalogName) {
        this.catalogName = catalogName;
    }
    public int getBegin() {
        return begin;
    }
    public void setBegin(int begin) {
        this.begin = begin;
    }
    public int getEnd() {
        return end;
    }
    public void setEnd(int end) {
        this.end = end;
    }
	public InvoiceBean getInvoice() {
		return invoice;
	}
	public void setInvoice(InvoiceBean invoice) {
		this.invoice = invoice;
	}
	public BillingBean getBilling() {
		return billing;
	}
	public void setBilling(BillingBean billing) {
		this.billing = billing;
	}
	public String getCatalogCode() {
		return catalogCode;
	}
	public void setCatalogCode(String catalogCode) {
		this.catalogCode = catalogCode;
	}
	public Integer getRevenueTypeId() {
	    return revenueTypeId;
	}
	public void setRevenueTypeId(Integer revenueTypeId) {
	    this.revenueTypeId = revenueTypeId;
	}
	public Integer getmCatalogId() {
	    return mCatalogId;
	}
	public void setmCatalogId(Integer mCatalogId) {
	    this.mCatalogId = mCatalogId;
	}
	public String getPending() {
	    return pending;
	}
	public void setPending(String pending) {
	    this.pending = pending;
	}
	public Date getBillStartDate() {
		return billStartDate;
	}
	public void setBillStartDate(Date billStartDate) {
		this.billStartDate = billStartDate;
	}
	public Date getBillEndDate() {
		return billEndDate;
	}
	public void setBillEndDate(Date billEndDate) {
		this.billEndDate = billEndDate;
	}
	public Integer getCostCenterId() {
		return costCenterId;
	}
	public void setCostCenterId(Integer costCenterId) {
		this.costCenterId = costCenterId;
	}
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getCitizenid() {
		return citizenid;
	}
	public void setCitizenid(String citizenid) {
		this.citizenid = citizenid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getBusinessAreaId() {
		return businessAreaId;
	}
	public void setBusinessAreaId(Integer businessAreaId) {
		this.businessAreaId = businessAreaId;
	}
	public Integer getDisbursementUnitId() {
		return disbursementUnitId;
	}
	public void setDisbursementUnitId(Integer disbursementUnitId) {
		this.disbursementUnitId = disbursementUnitId;
	}



	
}
