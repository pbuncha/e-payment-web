package th.go.cgd.epayment.model;

import java.util.ArrayList;
import java.util.List;

public class Select2Result {
	private List<Select2Item> results = new ArrayList<>();
	private Pagination pagination = new Pagination();
	
	public void setHasMore(boolean more) {
		this.pagination.more = more;
	}
	
	public Pagination getPagination(){
		return pagination;
	}
	
	public List<Select2Item> getResults() {
		return results;
	}

	public void setResults(List<Select2Item> results) {
		this.results = results;
	}

	public static class Select2Item {
		public String id;
		public String text;
		public Select2Item(String id, String text) {
			this.id = id;
			this.text = text;
		}
	}
	
	public static class Pagination {
		public boolean more;
	}
}
