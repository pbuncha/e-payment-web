package th.go.cgd.epayment.model;

public class StatusBean {

    private String status;
    private String statusName;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    @Override
    public String toString() {
        return "StatusBean{" +
                "status='" + status + '\'' +
                ", statusName='" + statusName + '\'' +
                '}';
    }
}
