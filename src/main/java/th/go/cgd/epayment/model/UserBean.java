package th.go.cgd.epayment.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import bsh.ParseException;
import scala.Char;

public class UserBean {
	
	private Integer userid;
	private String username;
	private String password;
	private String email;
	private String citizenid;
	private String mobile;
	private String fnameEN;
	private String fnameTH;
	private String sex;
	private Integer userType;
	private Date birthDate;
	private String birthDateStr;
	private Character verifiedType;
	private String titleNameTH;
	private String titleNameEN;
	private String mnameEN;
	private String mnameTH;
	private String lnameEN;
	private String lnameTH;
	private Integer age;
	private String company_status;
	private String company_objective;
	private String company_auth_id;
	private Date company_auth_birth_date;
	private String company_auth_email;
	private String company_auth_phone;
	private String company_auth_doc;
	private Integer address_id;

	private DropdownBean orgType;
	private DropdownBean orgSubType;
	private DropdownBean dept;
	private DropdownBean bizArea;
	private DropdownBean cosCenter;
	private List<DropdownBean> department;
	
	private String building_name;
	private String moo;
	private String no;
	private String road;
	private String soi;
	private String village;
	private String postCode;
	private String roomNo;
	private String provinceId;
	private String provinceName;
	private String amphurId;
	private String amphurName;
	private String tambonId;
	private String tambonName;
	private String captcha;
	private List<Integer> permissionGroup;
	private List<UserPermissionGroupBean> userPermissionGroup;
	private String titleThId;
	private String titleEnId;
	
	private String govAddreNo;
	private String govAddreMoo;
	private String govAddreRoomNo;
	private String govAddreSoi;
	private String govAddreRoad;
	private String govAddreProvinceId;
	private String govAddreAmphurId;
	private String govAddreTambonId;
	private String govAddrePosCode;

	private String status;
	private String name;
	private String departmentName;
	private String approveComment;
	private Date startDate;
	private Date endDate;
	private String filePath;

	
	private String businessAreaName;
	private String costCenterName;
	private String requestType;
	private Integer userPermissionGroupId;
	private String attachFile;
	
	
	public String getBusinessAreaName() {
		return businessAreaName;
	}

	public void setBusinessAreaName(String businessAreaName) {
		this.businessAreaName = businessAreaName;
	}

	public String getCostCenterName() {
		return costCenterName;
	}

	public void setCostCenterName(String costCenterName) {
		this.costCenterName = costCenterName;
	}

	public String getFnameEN() {
		return fnameEN;
	}

	public void setFnameEN(String fnameEN) {
		this.fnameEN = fnameEN;
	}
	
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public UserBean(){
		super();
	}
	
	public void setUserId(Integer userid){
		this.userid = userid;
	}
	
	public Integer getUserId(){
		return userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCitizenid() {
		return citizenid;
	}

	public void setCitizenid(String citizenid) {
		this.citizenid = citizenid;
	}

	public String getFnameTH() {
		return fnameTH;
	}

	public void setFnameTH(String fnameTH) {
		this.fnameTH = fnameTH;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public Integer getUserType() {
		return userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Character getVerifiedType() {
		return verifiedType;
	}

	public void setVerifiedType(Character verifiedType) {
		this.verifiedType = verifiedType;
	}

	public String getBirthDateStr() {
		if(null != birthDateStr){
			return dateThai(birthDateStr);
		}else {
			return null;
		}

	}

	public void setBirthDateStr(String birthDateStr) {
		this.birthDateStr = birthDateStr;
	}
	
	private String dateThai(String strDate)
	{
		String Months[] = {
			      "มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน",
			      "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม",
			      "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
		
		int year=0,month=0,day=0;
		try {
			Date date = df.parse(strDate);
			Calendar c = Calendar.getInstance();
			c.setTime(date);  
			
			year = c.get(Calendar.YEAR);
			month = c.get(Calendar.MONTH);
			day = c.get(Calendar.DATE);
			
		} catch (java.text.ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return String.format("%s %s %s%s", day,Months[month],"พ.ศ. ",year+543);
	}
	
	

	public String getTitleNameTH() {
		return titleNameTH;
	}

	public void setTitleNameTH(String titleNameTH) {
		this.titleNameTH = titleNameTH;
	}

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public String getTitleNameEN() {
		return titleNameEN;
	}

	public void setTitleNameEN(String titleNameEN) {
		this.titleNameEN = titleNameEN;
	}

	public String getMnameEN() {
		return mnameEN;
	}

	public void setMnameEN(String mnameEN) {
		this.mnameEN = mnameEN;
	}

	public String getMnameTH() {
		return mnameTH;
	}

	public void setMnameTH(String mnameTH) {
		this.mnameTH = mnameTH;
	}

	public String getLnameEN() {
		return lnameEN;
	}

	public void setLnameEN(String lnameEN) {
		this.lnameEN = lnameEN;
	}

	public String getLnameTH() {
		return lnameTH;
	}

	public void setLnameTH(String lnameTH) {
		this.lnameTH = lnameTH;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getCompany_status() {
		return company_status;
	}

	public void setCompany_status(String company_status) {
		this.company_status = company_status;
	}

	public String getCompany_objective() {
		return company_objective;
	}

	public void setCompany_objective(String company_objective) {
		this.company_objective = company_objective;
	}

	public String getCompany_auth_id() {
		return company_auth_id;
	}

	public void setCompany_auth_id(String company_auth_id) {
		this.company_auth_id = company_auth_id;
	}

	public Date getCompany_auth_birth_date() {
		return company_auth_birth_date;
	}

	public void setCompany_auth_birth_date(Date company_auth_birth_date) {
		this.company_auth_birth_date = company_auth_birth_date;
	}

	public String getCompany_auth_email() {
		return company_auth_email;
	}

	public void setCompany_auth_email(String company_auth_email) {
		this.company_auth_email = company_auth_email;
	}

	public String getCompany_auth_phone() {
		return company_auth_phone;
	}

	public void setCompany_auth_phone(String company_auth_phone) {
		this.company_auth_phone = company_auth_phone;
	}

	public String getCompany_auth_doc() {
		return company_auth_doc;
	}

	public void setCompany_auth_doc(String company_auth_doc) {
		this.company_auth_doc = company_auth_doc;
	}

	public Integer getAddress_id() {
		return address_id;
	}

	public void setAddress_id(Integer address_id) {
		this.address_id = address_id;
	}

	public String getBuilding_name() {
		return building_name;
	}

	public void setBuilding_name(String building_name) {
		this.building_name = building_name;
	}

	public String getMoo() {
		return moo;
	}

	public void setMoo(String moo) {
		this.moo = moo;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getRoad() {
		return road;
	}

	public void setRoad(String road) {
		this.road = road;
	}

	public String getSoi() {
		return soi;
	}

	public void setSoi(String soi) {
		this.soi = soi;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getRoomNo() {
	    return roomNo;
	}

	public void setRoomNo(String roomNo) {
	    this.roomNo = roomNo;
	}

	public String getProvinceId() {
	    return provinceId;
	}

	public void setProvinceId(String provinceId) {
	    this.provinceId = provinceId;
	}

	public String getAmphurId() {
	    return amphurId;
	}

	public void setAmphurId(String amphurId) {
	    this.amphurId = amphurId;
	}

	public String getTambonId() {
	    return tambonId;
	}

	public void setTambonId(String tambonId) {
	    this.tambonId = tambonId;
	}

	public String getCaptcha() {
		return captcha;
	}

	public void setCaptcha(String captcha) {
		this.captcha = captcha;
	}

	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}

	public String getAmphurName() {
		return amphurName;
	}

	public void setAmphurName(String amphurName) {
		this.amphurName = amphurName;
	}

	public String getTambonName() {
		return tambonName;
	}

	public void setTambonName(String tambonName) {
		this.tambonName = tambonName;
	}

	public DropdownBean getOrgType() {
		return orgType;
	}

	public void setOrgType(DropdownBean orgType) {
		this.orgType = orgType;
	}

	public DropdownBean getOrgSubType() {
		return orgSubType;
	}

	public void setOrgSubType(DropdownBean orgSubType) {
		this.orgSubType = orgSubType;
	}

	public DropdownBean getDept() {
		return dept;
	}

	public void setDept(DropdownBean dept) {
		this.dept = dept;
	}

	public DropdownBean getBizArea() {
		return bizArea;
	}

	public void setBizArea(DropdownBean bizArea) {
		this.bizArea = bizArea;
	}

	public DropdownBean getCosCenter() {
		return cosCenter;
	}

	public void setCosCenter(DropdownBean cosCenter) {
		this.cosCenter = cosCenter;
	}

	public List<DropdownBean> getDepartment() {
		return department;
	}

	public void setDepartment(List<DropdownBean> department) {
		this.department = department;
	}

	public List<Integer> getPermissionGroup() {
	    return permissionGroup;
	}

	public void setPermissionGroup(List<Integer> permissionGroup) {
	    this.permissionGroup = permissionGroup;
	}

	public String getTitleThId() {
	    return titleThId;
	}

	public void setTitleThId(String titleThId) {
	    this.titleThId = titleThId;
	}

	public String getTitleEnId() {
	    return titleEnId;
	}

	public void setTitleEnId(String titleEnId) {
	    this.titleEnId = titleEnId;
	}

	public String getGovAddreNo() {
	    return govAddreNo;
	}

	public void setGovAddreNo(String govAddreNo) {
	    this.govAddreNo = govAddreNo;
	}

	public String getGovAddreMoo() {
	    return govAddreMoo;
	}

	public void setGovAddreMoo(String govAddreMoo) {
	    this.govAddreMoo = govAddreMoo;
	}

	public String getGovAddreRoomNo() {
	    return govAddreRoomNo;
	}

	public void setGovAddreRoomNo(String govAddreRoomNo) {
	    this.govAddreRoomNo = govAddreRoomNo;
	}

	public String getGovAddreSoi() {
	    return govAddreSoi;
	}

	public void setGovAddreSoi(String govAddreSoi) {
	    this.govAddreSoi = govAddreSoi;
	}

	public String getGovAddreRoad() {
	    return govAddreRoad;
	}

	public void setGovAddreRoad(String govAddreRoad) {
	    this.govAddreRoad = govAddreRoad;
	}

	public String getGovAddreProvinceId() {
	    return govAddreProvinceId;
	}

	public void setGovAddreProvinceId(String govAddreProvinceId) {
	    this.govAddreProvinceId = govAddreProvinceId;
	}

	public String getGovAddreAmphurId() {
	    return govAddreAmphurId;
	}

	public void setGovAddreAmphurId(String govAddreAmphurId) {
	    this.govAddreAmphurId = govAddreAmphurId;
	}

	public String getGovAddreTambonId() {
	    return govAddreTambonId;
	}

	public void setGovAddreTambonId(String govAddreTambonId) {
	    this.govAddreTambonId = govAddreTambonId;
	}

	public String getGovAddrePosCode() {
	    return govAddrePosCode;
	}

	public void setGovAddrePosCode(String govAddrePosCode) {
	    this.govAddrePosCode = govAddrePosCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

	public String getApproveComment() {
		return approveComment;
	}

	public void setApproveComment(String approveComment) {
		this.approveComment = approveComment;
	}

	public List<UserPermissionGroupBean> getUserPermissionGroup() {
		return userPermissionGroup;
	}

	public void setUserPermissionGroup(List<UserPermissionGroupBean> userPermissionGroup) {
		this.userPermissionGroup = userPermissionGroup;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getFilePath() {
	    return filePath;
	}

	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public Integer getUserPermissionGroupId() {
		return userPermissionGroupId;
	}

	public void setUserPermissionGroupId(Integer userPermissionGroupId) {
		this.userPermissionGroupId = userPermissionGroupId;
	}

	public String getAttachFile() {
		return attachFile;
	}

	public void setAttachFile(String attachFile) {
		this.attachFile = attachFile;
	}


	


	
}
