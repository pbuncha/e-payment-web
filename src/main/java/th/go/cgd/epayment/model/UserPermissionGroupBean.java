/**
 * 
 */
package th.go.cgd.epayment.model;

import java.util.Date;

/**
 * @author wichuda.k Jan 17, 2018
 *
 */
public class UserPermissionGroupBean {
	private String permisName;
	private String sendStatus;
	private Date sendDate;
	private String status;
	private Date startDate;
	private Date endDate;
	private String appvStatus;
	private String attachFile;
	private Integer permisId;
	
	public String getPermisName() {
		return permisName;
	}
	public void setPermisName(String permisName) {
		this.permisName = permisName;
	}
	public String getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}
	public Date getSendDate() {
		return sendDate;
	}
	public void setSendDate(Date sendDate) {
		this.sendDate = sendDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getAppvStatus() {
		return appvStatus;
	}
	public void setAppvStatus(String appvStatus) {
		this.appvStatus = appvStatus;
	}
	public String getAttachFile() {
		return attachFile;
	}
	public void setAttachFile(String attachFile) {
		this.attachFile = attachFile;
	}
	public Integer getPermisId() {
		return permisId;
	}
	public void setPermisId(Integer permisId) {
		this.permisId = permisId;
	}
	
	
}
