package th.go.cgd.epayment.model;

import java.io.Serializable;

public class UserRootTreeModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7311239339240870883L;
	private String text;
	private boolean children;
	private boolean rootTree;
	private int orgId;
	private int userId;
	private UserRootTreeStateModel state;
	private String type;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public boolean isChildren() {
		return children;
	}
	public void setChildren(boolean children) {
		this.children = children;
	}
	public int getOrgId() {
		return orgId;
	}
	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}
	public boolean isRootTree() {
		return rootTree;
	}
	public void setRootTree(boolean rootTree) {
		this.rootTree = rootTree;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public UserRootTreeStateModel getState() {
		return state;
	}
	public void setState(UserRootTreeStateModel state) {
		this.state = state;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
	
	
}
