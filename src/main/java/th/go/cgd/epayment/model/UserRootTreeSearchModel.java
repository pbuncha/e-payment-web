package th.go.cgd.epayment.model;

import java.io.Serializable;

public class UserRootTreeSearchModel implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1150883013980079721L;
	private String id;
	private String parent;
	private String text;
	private boolean children;
	private boolean rootTree;
	private int orgId;
	private int userId;
	private UserRootTreeStateModel state;
	private String type;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public boolean isChildren() {
		return children;
	}
	public void setChildren(boolean children) {
		this.children = children;
	}
	public boolean isRootTree() {
		return rootTree;
	}
	public void setRootTree(boolean rootTree) {
		this.rootTree = rootTree;
	}
	public int getOrgId() {
		return orgId;
	}
	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public UserRootTreeStateModel getState() {
		return state;
	}
	public void setState(UserRootTreeStateModel state) {
		this.state = state;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
