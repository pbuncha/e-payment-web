package th.go.cgd.epayment.model;

import java.io.Serializable;

public class UserRootTreeStateModel implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3946899740503651030L;
	private boolean opened;
	private boolean selected;
	public boolean isOpened() {
		return opened;
	}
	public void setOpened(boolean opened) {
		this.opened = opened;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}
	
	
}
