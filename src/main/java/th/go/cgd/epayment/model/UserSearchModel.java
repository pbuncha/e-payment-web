package th.go.cgd.epayment.model;

import java.io.Serializable;

import th.go.cgd.epayment.entity.MUserRole;
import th.go.cgd.epayment.entity.MUserStatus;

public class UserSearchModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5117101480344214981L;
	private String email;
	private Integer organizationId;
	private String firstName;
	private String lastName;
	private int userType;
	private MUserStatus MUserStatus;
	private MUserRole MUserRole;
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getUserType() {
		return userType;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	public MUserStatus getMUserStatus() {
		return MUserStatus;
	}
	public void setMUserStatus(MUserStatus mUserStatus) {
		MUserStatus = mUserStatus;
	}
	public MUserRole getMUserRole() {
		return MUserRole;
	}
	public void setMUserRole(MUserRole mUserRole) {
		MUserRole = mUserRole;
	}
	
	

}
