package th.go.cgd.epayment.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import th.go.cgd.epayment.entity.MReportGroup;
import th.go.cgd.epayment.entity.Menu;
import th.go.cgd.epayment.entity.User;

public class UserSession implements Serializable 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -453481528948958111L;

	private User user;
	private List<MenuItem> menus;
	private List<Menu> userMenus;
	private List<Short> perms = new ArrayList<Short>();
	private List<Integer> orgs = new ArrayList<Integer>();
	private Integer userOrg;
	private Boolean hasAdminRole;
	private List<MReportGroup> userReportGroups = new ArrayList<MReportGroup>();
	
	public UserSession() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UserSession(User user) 
	{
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<MenuItem> getMenus() {
		return menus;
	}
	public void setMenus(List<MenuItem> menus) {
		this.menus = menus;
	}

	public List<Menu> getUserMenus() {
		return userMenus;
	}

	public void setUserMenus(List<Menu> userMenus) {
		this.userMenus = userMenus;
	}

	public List<Short> getPerms() {
		return perms;
	}

	public void setPerms(List<Short> perms) {
		this.perms = perms;
	}
	
	public List<Integer> getOrgs() {
		return orgs;
	}

	public void setOrgs(List<Integer> orgs) {
		this.orgs = orgs;
	}
	
	public Integer getUserOrg() {
		return userOrg;
	}

	public void setUserOrg(Integer userOrg) {
		this.userOrg = userOrg;
	}

	public Boolean getHasAdminRole() {
		return hasAdminRole;
	}

	public void setHasAdminRole(Boolean hasAdminRole) {
		this.hasAdminRole = hasAdminRole;
	}

	public boolean hasPerms(short val) {
		return perms.contains(val);
	}

	public List<MReportGroup> getUserReportGroups() {
		return userReportGroups;
	}

	public void setUserReportGroups(List<MReportGroup> userReportGroups) {
		this.userReportGroups = userReportGroups;
	}
}