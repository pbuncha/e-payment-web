package th.go.cgd.epayment.model.chart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChartStatCaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2998026718764021207L;
	private List<String> labels = new ArrayList<String>();
	private List<ChartStatCaseDatasetModel> datasets = new ArrayList<ChartStatCaseDatasetModel>();
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public List<ChartStatCaseDatasetModel> getDatasets() {
		return datasets;
	}
	public void setDatasets(List<ChartStatCaseDatasetModel> datasets) {
		this.datasets = datasets;
	}
	
	
}
