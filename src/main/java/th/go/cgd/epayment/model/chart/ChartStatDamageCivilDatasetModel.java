package th.go.cgd.epayment.model.chart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChartStatDamageCivilDatasetModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 518879361831134396L;
	private String label;
	private String backgroundColor;
	private List<Integer> data = new ArrayList<Integer>();
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	public List<Integer> getData() {
		return data;
	}
	public void setData(List<Integer> data) {
		this.data = data;
	}
	
}
