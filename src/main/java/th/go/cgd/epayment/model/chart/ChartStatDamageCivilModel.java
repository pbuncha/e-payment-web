package th.go.cgd.epayment.model.chart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChartStatDamageCivilModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5745846846561658362L;
	private List<String> labels = new ArrayList<String>();
	private List<ChartStatDamageCivilDatasetModel> datasets = new ArrayList<ChartStatDamageCivilDatasetModel>();
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public List<ChartStatDamageCivilDatasetModel> getDatasets() {
		return datasets;
	}
	public void setDatasets(List<ChartStatDamageCivilDatasetModel> datasets) {
		this.datasets = datasets;
	}
	

}
