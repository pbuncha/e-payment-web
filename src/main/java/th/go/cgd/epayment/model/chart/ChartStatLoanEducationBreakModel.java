package th.go.cgd.epayment.model.chart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChartStatLoanEducationBreakModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1282291855698853761L;
	private List<String> labels = new ArrayList<String>();
	private List<ChartStatLoanEducationBreakDatasetModel> datasets = new ArrayList<ChartStatLoanEducationBreakDatasetModel>();
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public List<ChartStatLoanEducationBreakDatasetModel> getDatasets() {
		return datasets;
	}
	public void setDatasets(List<ChartStatLoanEducationBreakDatasetModel> datasets) {
		this.datasets = datasets;
	}
	
}
