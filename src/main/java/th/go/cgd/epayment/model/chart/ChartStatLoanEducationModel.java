package th.go.cgd.epayment.model.chart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChartStatLoanEducationModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1317651673199237980L;
	private List<String> labels = new ArrayList<String>();
	private List<ChartStatLoanEducationDatasetModel> datasets = new ArrayList<ChartStatLoanEducationDatasetModel>();
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public List<ChartStatLoanEducationDatasetModel> getDatasets() {
		return datasets;
	}
	public void setDatasets(List<ChartStatLoanEducationDatasetModel> datasets) {
		this.datasets = datasets;
	}
	
	
	
}
