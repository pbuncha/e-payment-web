package th.go.cgd.epayment.model.chart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChartStatWorkDatasetModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 940516846687152550L;
	private String label;
	private Boolean fill;
    private Double lineTension;
    private String backgroundColor;
    private String borderColor;
    private String borderCapStyle;
    private List<String> borderDash = new ArrayList<String>();
    private Double borderDashOffset;
    private String borderJoinStyle;
    private String pointBorderColor;
    private String pointBackgroundColor;
    private Integer pointBorderWidth;
    private Integer pointHoverRadius;
    private String pointHoverBackgroundColor;
    private String pointHoverBorderColor;
   	private Integer pointHoverBorderWidth;
    private Integer pointRadius;
    private Integer pointHitRadius;
    private List<Integer> data = new ArrayList<Integer>();
    private Boolean spanGaps;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public Boolean getFill() {
		return fill;
	}
	public void setFill(Boolean fill) {
		this.fill = fill;
	}
	public Double getLineTension() {
		return lineTension;
	}
	public void setLineTension(Double lineTension) {
		this.lineTension = lineTension;
	}
	public String getBackgroundColor() {
		return backgroundColor;
	}
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}
	public String getBorderColor() {
		return borderColor;
	}
	public void setBorderColor(String borderColor) {
		this.borderColor = borderColor;
	}
	public String getBorderCapStyle() {
		return borderCapStyle;
	}
	public void setBorderCapStyle(String borderCapStyle) {
		this.borderCapStyle = borderCapStyle;
	}
	public List<String> getBorderDash() {
		return borderDash;
	}
	public void setBorderDash(List<String> borderDash) {
		this.borderDash = borderDash;
	}
	public Double getBorderDashOffset() {
		return borderDashOffset;
	}
	public void setBorderDashOffset(Double borderDashOffset) {
		this.borderDashOffset = borderDashOffset;
	}
	public String getBorderJoinStyle() {
		return borderJoinStyle;
	}
	public void setBorderJoinStyle(String borderJoinStyle) {
		this.borderJoinStyle = borderJoinStyle;
	}
	public String getPointBorderColor() {
		return pointBorderColor;
	}
	public void setPointBorderColor(String pointBorderColor) {
		this.pointBorderColor = pointBorderColor;
	}
	public String getPointBackgroundColor() {
		return pointBackgroundColor;
	}
	public void setPointBackgroundColor(String pointBackgroundColor) {
		this.pointBackgroundColor = pointBackgroundColor;
	}
	public Integer getPointBorderWidth() {
		return pointBorderWidth;
	}
	public void setPointBorderWidth(Integer pointBorderWidth) {
		this.pointBorderWidth = pointBorderWidth;
	}
	public Integer getPointHoverRadius() {
		return pointHoverRadius;
	}
	public void setPointHoverRadius(Integer pointHoverRadius) {
		this.pointHoverRadius = pointHoverRadius;
	}
	public String getPointHoverBackgroundColor() {
		return pointHoverBackgroundColor;
	}
	public void setPointHoverBackgroundColor(String pointHoverBackgroundColor) {
		this.pointHoverBackgroundColor = pointHoverBackgroundColor;
	}
	public String getPointHoverBorderColor() {
		return pointHoverBorderColor;
	}
	public void setPointHoverBorderColor(String pointHoverBorderColor) {
		this.pointHoverBorderColor = pointHoverBorderColor;
	}
	public Integer getPointHoverBorderWidth() {
		return pointHoverBorderWidth;
	}
	public void setPointHoverBorderWidth(Integer pointHoverBorderWidth) {
		this.pointHoverBorderWidth = pointHoverBorderWidth;
	}
	public Integer getPointRadius() {
		return pointRadius;
	}
	public void setPointRadius(Integer pointRadius) {
		this.pointRadius = pointRadius;
	}
	public Integer getPointHitRadius() {
		return pointHitRadius;
	}
	public void setPointHitRadius(Integer pointHitRadius) {
		this.pointHitRadius = pointHitRadius;
	}
	public List<Integer> getData() {
		return data;
	}
	public void setData(List<Integer> data) {
		this.data = data;
	}
	public Boolean getSpanGaps() {
		return spanGaps;
	}
	public void setSpanGaps(Boolean spanGaps) {
		this.spanGaps = spanGaps;
	}
	
	
}
