package th.go.cgd.epayment.model.chart;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ChartStatWorkModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5792912546636348648L;
	private List<String> labels = new ArrayList<String>();
	private List<ChartStatWorkDatasetModel> datasets = new ArrayList<ChartStatWorkDatasetModel>();
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public List<ChartStatWorkDatasetModel> getDatasets() {
		return datasets;
	}
	public void setDatasets(List<ChartStatWorkDatasetModel> datasets) {
		this.datasets = datasets;
	}
	
	
	
	
}
