package th.go.cgd.epayment.model.email;

import java.io.Serializable;

public class NotiNewUserModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4085633391257658625L;
	private String userName;
	private String password;
	private String fullName;
	private String sendTo;
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getSendTo() {
		return sendTo;
	}
	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}
	
	
}
