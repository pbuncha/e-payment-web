package th.go.cgd.epayment.model.form;

public class ProgramModel {

	private String permissionGroupId; 
	
    private int programId;

    private String programCode;

    private String programName;

    private String description;

    private String status;

    private String statusName;
    
	private Character isView;
	
	private Character isPdf;
	
	private Character isExcel;
	
	private Character isEdit;
	
	
	private Character isSelected;
	
	
    public Character getIsSelected() {
		return isSelected;
	}

	public void setIsSelected(Character isSelected) {
		this.isSelected = isSelected;
	}

	public int getProgramId() {
        return programId;
    }

    public void setProgramId(int programId) {
        this.programId = programId;
    }

    public String getProgramCode() {
        return programCode;
    }

    public void setProgramCode(String programCode) {
        this.programCode = programCode;
    }

    public String getProgramName() {
        return programName;
    }

    public void setProgramName(String programName) {
        this.programName = programName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
    
	public String getPermissionGroupId() {
		return permissionGroupId;
	}

	public void setPermissionGroupId(String permissionGroupId) {
		this.permissionGroupId = permissionGroupId;
	}

	public Character getIsView() {
		return isView;
	}

	public void setIsView(Character isView) {
		this.isView = isView;
	}

	public Character getIsPdf() {
		return isPdf;
	}

	public void setIsPdf(Character isPdf) {
		this.isPdf = isPdf;
	}

	public Character getIsExcel() {
		return isExcel;
	}

	public void setIsExcel(Character isExcel) {
		this.isExcel = isExcel;
	}

	public Character getIsEdit() {
		return isEdit;
	}

	public void setIsEdit(Character isEdit) {
		this.isEdit = isEdit;
	}

	@Override
	public String toString() {
		return "ProgramModel [permissionGroupId=" + permissionGroupId + ", programId=" + programId + ", programCode="
				+ programCode + ", programName=" + programName + ", description=" + description + ", status=" + status
				+ ", statusName=" + statusName + ", isView=" + isView + ", isPdf=" + isPdf + ", isExcel=" + isExcel
				+ ", isEdit=" + isEdit + "]";
	}

}
