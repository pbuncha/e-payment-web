package th.go.cgd.epayment.model.form;

import th.go.cgd.epayment.model.UserBean;

import java.util.List;

public class UserCenterBean {

    private Integer userCenterId;
    private String email;
    private String citizenid;
    private String mobile;
    private String fnameEN;
    private String fnameTH;
    private String lnameEN;
    private String lnameTH;
    private List<UserModel> users;

    public Integer getUserCenterId() {
        return userCenterId;
    }

    public void setUserCenterId(Integer userCenterId) {
        this.userCenterId = userCenterId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCitizenid() {
        return citizenid;
    }

    public void setCitizenid(String citizenid) {
        this.citizenid = citizenid;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFnameEN() {
        return fnameEN;
    }

    public void setFnameEN(String fnameEN) {
        this.fnameEN = fnameEN;
    }

    public String getFnameTH() {
        return fnameTH;
    }

    public void setFnameTH(String fnameTH) {
        this.fnameTH = fnameTH;
    }

    public String getLnameEN() {
        return lnameEN;
    }

    public void setLnameEN(String lnameEN) {
        this.lnameEN = lnameEN;
    }

    public String getLnameTH() {
        return lnameTH;
    }

    public void setLnameTH(String lnameTH) {
        this.lnameTH = lnameTH;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "UserCenterBean{" +
                "userCenterId=" + userCenterId +
                ", email='" + email + '\'' +
                ", citizenid='" + citizenid + '\'' +
                ", mobile='" + mobile + '\'' +
                ", fnameEN='" + fnameEN + '\'' +
                ", fnameTH='" + fnameTH + '\'' +
                ", lnameEN='" + lnameEN + '\'' +
                ", lnameTH='" + lnameTH + '\'' +
                ", users=" + users +
                '}';
    }
}
