package th.go.cgd.epayment.model.form;

import java.util.List;

public class UserModel {

    private Integer userId;
    private String email;
    private String username;
    private String mobile;
    private String fnameTH;
    private String lnameTH;
    private String fnameEN;
    private String lnameEN;
    private List<Integer> perms;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getFnameTH() {
        return fnameTH;
    }

    public void setFnameTH(String fnameTH) {
        this.fnameTH = fnameTH;
    }

    public String getLnameTH() {
        return lnameTH;
    }

    public void setLnameTH(String lnameTH) {
        this.lnameTH = lnameTH;
    }

    public String getFnameEN() {
        return fnameEN;
    }

    public void setFnameEN(String fnameEN) {
        this.fnameEN = fnameEN;
    }

    public String getLnameEN() {
        return lnameEN;
    }

    public void setLnameEN(String lnameEN) {
        this.lnameEN = lnameEN;
    }

    public List<Integer> getPerms() {
        return perms;
    }

    public void setPerms(List<Integer> perms) {
        this.perms = perms;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "userId=" + userId +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", mobile='" + mobile + '\'' +
                ", fnameTH='" + fnameTH + '\'' +
                ", lnameTH='" + lnameTH + '\'' +
                ", fnameEN='" + fnameEN + '\'' +
                ", lnameEN='" + lnameEN + '\'' +
                ", perms=" + perms +
                '}';
    }
}
