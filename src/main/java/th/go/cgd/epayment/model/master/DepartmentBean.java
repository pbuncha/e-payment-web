package th.go.cgd.epayment.model.master;

import java.util.Date;

public class DepartmentBean {

    private Integer departmentId;
    private String departmentCode;
    private String departmentName;
    private Date endDate;
    private Date startDate;
    private String status;
    private Date updatedDate;
    private Integer updatedBy;
    private Date createdDate;
    private Integer createdBy;
    private Integer organizationSubTypeId;
    private String organizationSubTypeName;
    private Integer parentDepartmentId;
    private String parentDepartmentName;
    
    private Date startDateFrom;
    private Date endDateFrom;
    private Date startDateTo;
    private Date endDateTo;
        
    public Integer getParentDepartmentId() {
		return parentDepartmentId;
	}
	public void setParentDepartmentId(Integer parentDepartmentId) {
		this.parentDepartmentId = parentDepartmentId;
	}
	public String getParentDepartmentName() {
		return parentDepartmentName;
	}
	public void setParentDepartmentName(String parentDepartmentName) {
		this.parentDepartmentName = parentDepartmentName;
	}
	public Date getStartDateFrom() {
		return startDateFrom;
	}
	public void setStartDateFrom(Date startDateFrom) {
		this.startDateFrom = startDateFrom;
	}
	public Date getEndDateFrom() {
		return endDateFrom;
	}
	public void setEndDateFrom(Date endDateFrom) {
		this.endDateFrom = endDateFrom;
	}
	public Date getStartDateTo() {
		return startDateTo;
	}
	public void setStartDateTo(Date startDateTo) {
		this.startDateTo = startDateTo;
	}
	public Date getEndDateTo() {
		return endDateTo;
	}
	public void setEndDateTo(Date endDateTo) {
		this.endDateTo = endDateTo;
	}    
	
	public Integer getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}
	public String getDepartmentCode() {
		return departmentCode;
	}
	public void setDepartmentCode(String departmentCode) {
		this.departmentCode = departmentCode;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getOrganizationSubTypeId() {
		return organizationSubTypeId;
	}
	public void setOrganizationSubTypeId(Integer organizationSubTypeId) {
		this.organizationSubTypeId = organizationSubTypeId;
	}
	public String getOrganizationSubTypeName() {
		return organizationSubTypeName;
	}
	public void setOrganizationSubTypeName(String string) {
		this.organizationSubTypeName = string;
	}
    
    
    
}
