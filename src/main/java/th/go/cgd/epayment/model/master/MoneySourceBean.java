package th.go.cgd.epayment.model.master;

import java.util.Date;

public class MoneySourceBean {
	
    private Integer moneySourceId;
    private String moneySourceCode;
    private String moneySourceName;
    private String moneySourceType;
    private Date startDate;
    private Date endDate;
    private String status;
    private Integer createdBy;
    private Date createdDate;
    private Integer updatedBy;
    private Date updatedDate;

    private Date startDateFrom;
    private Date endDateFrom;
    private Date startDateTo;
    private Date endDateTo;
    
    public Date getStartDateFrom() {
		return startDateFrom;
	}
	public void setStartDateFrom(Date startDateFrom) {
		this.startDateFrom = startDateFrom;
	}
	public Date getEndDateFrom() {
		return endDateFrom;
	}
	public void setEndDateFrom(Date endDateFrom) {
		this.endDateFrom = endDateFrom;
	}
	public Date getStartDateTo() {
		return startDateTo;
	}
	public void setStartDateTo(Date startDateTo) {
		this.startDateTo = startDateTo;
	}
	public Date getEndDateTo() {
		return endDateTo;
	}
	public void setEndDateTo(Date endDateTo) {
		this.endDateTo = endDateTo;
	}
	public Integer getMoneySourceId() {
		return moneySourceId;
	}
	public void setMoneySourceId(Integer moneySourceId) {
		this.moneySourceId = moneySourceId;
	}
	public String getMoneySourceCode() {
		return moneySourceCode;
	}
	public void setMoneySourceCode(String moneySourceCode) {
		this.moneySourceCode = moneySourceCode;
	}
	public String getMoneySourceName() {
		return moneySourceName;
	}
	public void setMoneySourceName(String moneySourceName) {
		this.moneySourceName = moneySourceName;
	}
	public String getMoneySourceType() {
		return moneySourceType;
	}
	public void setMoneySourceType(String moneySourceType) {
		this.moneySourceType = moneySourceType;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

    
    
}
