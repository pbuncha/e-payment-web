package th.go.cgd.epayment.model.master;

import java.util.Date;

public class OrganizationSubTypeBean {

	private Integer organizationSubTypeId;
	private String organizationSubTypeName;
	private Date startDate;
	private Date endDate;
	private String status;
	private Date updatedDate;
	private Date createdDate;
	private Integer updatedBy;
	private Integer createdBy;
	private Integer organizationTypeId;
	private String organizationTypeName;
	public Integer getOrganizationSubTypeId() {
		return organizationSubTypeId;
	}
	public void setOrganizationSubTypeId(Integer organizationSubTypeId) {
		this.organizationSubTypeId = organizationSubTypeId;
	}
	public String getOrganizationSubTypeName() {
		return organizationSubTypeName;
	}
	public void setOrganizationSubTypeName(String organizationSubTypeName) {
		this.organizationSubTypeName = organizationSubTypeName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Integer getOrganizationTypeId() {
		return organizationTypeId;
	}
	public void setOrganizationTypeId(Integer organizationTypeId) {
		this.organizationTypeId = organizationTypeId;
	}
	public String getOrganizationTypeName() {
		return organizationTypeName;
	}
	public void setOrganizationTypeName(String organizationTypeName) {
		this.organizationTypeName = organizationTypeName;
	}

}
