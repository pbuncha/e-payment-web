package th.go.cgd.epayment.model.master;

import java.util.Date;

public class StandardRevernueBean {
	
    private Integer standardRevernueId;
    private String standardRevernueCode;
    private String standardRevernueName;
    private Date startDate;
    private Date endDate;
    private String status;
    private String standardRevernueDesc;
    private Date createdDate;
    private Integer createdBy;
    private Date updatedDate;
    private Integer updatedBy;
    private Integer moneySourceId;
    private String moneySourceName;
    private Date startDateFrom;
    private Date startDateTo;
    private Date endDateFrom;
    private Date endDateTo;
    
	public Integer getStandardRevernueId() {
		return standardRevernueId;
	}
	public void setStandardRevernueId(Integer standardRevernueId) {
		this.standardRevernueId = standardRevernueId;
	}
	public String getStandardRevernueCode() {
		return standardRevernueCode;
	}
	public void setStandardRevernueCode(String standardRevernueCode) {
		this.standardRevernueCode = standardRevernueCode;
	}
	public String getStandardRevernueName() {
		return standardRevernueName;
	}
	public void setStandardRevernueName(String standardRevernueName) {
		this.standardRevernueName = standardRevernueName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public Date getStartDateFrom() {
		return startDateFrom;
	}
	public void setStartDateFrom(Date startDateFrom) {
		this.startDateFrom = startDateFrom;
	}
	public Date getStartDateTo() {
		return startDateTo;
	}
	public void setStartDateTo(Date startDateTo) {
		this.startDateTo = startDateTo;
	}
	public Date getEndDateFrom() {
		return endDateFrom;
	}
	public void setEndDateFrom(Date endDateFrom) {
		this.endDateFrom = endDateFrom;
	}
	public Date getEndDateTo() {
		return endDateTo;
	}
	public void setEndDateTo(Date endDateTo) {
		this.endDateTo = endDateTo;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStandardRevernueDesc() {
		return standardRevernueDesc;
	}
	public void setStandardRevernueDesc(String standardRevernueDesc) {
		this.standardRevernueDesc = standardRevernueDesc;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getMoneySourceId() {
		return moneySourceId;
	}
	public void setMoneySourceId(Integer moneySourceId) {
		this.moneySourceId = moneySourceId;
	}
	public String getMoneySourceName() {
		return moneySourceName;
	}
	public void setMoneySourceName(String moneySourceName) {
		this.moneySourceName = moneySourceName;
	}

    
    
}


