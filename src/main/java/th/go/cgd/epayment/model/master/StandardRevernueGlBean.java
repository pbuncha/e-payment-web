package th.go.cgd.epayment.model.master;

import java.util.Date;

public class StandardRevernueGlBean {

	private Integer standardRevernueGlId;
	private Integer order;
	private int createdBy;
	private Date createdDate;
	private Date updatedDate;
	private Integer updatedBy;
	private Integer standardRevernueId;
	private String standardRevernueName;
	private Long glFinishId;
	private Long glStartId;
    private String status;

	public Integer getStandardRevernueGlId() {
		return standardRevernueGlId;
	}
	public void setStandardRevernueGlId(Integer standardRevernueGlId) {
		this.standardRevernueGlId = standardRevernueGlId;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public int getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public Integer getUpdatedBy() {
		return updatedBy;
	}
	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}
	public Integer getStandardRevernueId() {
		return standardRevernueId;
	}
	public void setStandardRevernueId(Integer standardRevernueId) {
		this.standardRevernueId = standardRevernueId;
	}
	public Long getGlFinishId() {
		return glFinishId;
	}
	public void setGlFinishId(Long glFinishId) {
		this.glFinishId = glFinishId;
	}
	public Long getGlStartId() {
		return glStartId;
	}
	public void setGlStartId(Long glStartId) {
		this.glStartId = glStartId;
	}
	public String getStandardRevernueName() {
		return standardRevernueName;
	}
	public void setStandardRevernueName(String standardRevernueName) {
		this.standardRevernueName = standardRevernueName;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}	
	
}


