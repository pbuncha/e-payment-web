package th.go.cgd.epayment.report.model;

import java.util.Date;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import th.go.cgd.epayment.entity.User;

public class Report20002Bean {
	private String userLoginId= "";
	private String ministryId = "";
	private String departmentId = "";
	private String revenueTypeId = "";
	private String catalogTypeId = "";
	private String statusId = "";
	private Date catalogStartDate;
	private String reportType  = "";
	private String ministry = "";
	private String department = "";
	private String revenueType = "";
	private String catalogType = "";

	private Date startDate;
	private Date endDate;	
	private String startTime;
	private String endTime;	
	private String workTypeId = "";	

	private String employeeId = "";
	private String employeeTypeId = "";
	
	
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getEmployeeTypeId() {
		return employeeTypeId;
	}
	public void setEmployeeTypeId(String employeeTypeId) {
		this.employeeTypeId = employeeTypeId;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getWorkTypeId() {
		return workTypeId;
	}
	public void setWorkTypeId(String workTypeId) {
		this.workTypeId = workTypeId;
	}
	private String user;
	
	public String getUser() {
		User  q = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (q != null)
		{
			return q.getUserName();
		}
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	
	public String getUserLoginId() {
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			return user.getUserId()+"";
		}
		return userLoginId;
	}
	public void setUserLoginId(String userLoginId) {
		this.userLoginId = userLoginId;
	}
	public String getMinistryId() {
		return ministryId;
	}
	public void setMinistryId(String ministryId) {
		this.ministryId = ministryId;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getRevenueTypeId() {
		return revenueTypeId;
	}
	public void setRevenueTypeId(String revenueTypeId) {
		this.revenueTypeId = revenueTypeId;
	}
	public String getCatalogTypeId() {
		return catalogTypeId;
	}
	public void setCatalogTypeId(String catalogTypeId) {
		this.catalogTypeId = catalogTypeId;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public Date getCatalogStartDate() {
		return catalogStartDate;
	}
	public void setCatalogStartDate(Date catalogStartDate) {
		this.catalogStartDate = catalogStartDate;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getMinistry() {
		return ministry;
	}
	public void setMinistry(String ministry) {
		this.ministry = ministry;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getRevenueType() {
		return revenueType;
	}
	public void setRevenueType(String revenueType) {
		this.revenueType = revenueType;
	}
	public String getCatalogType() {
		return catalogType;
	}
	public void setCatalogType(String catalogType) {
		this.catalogType = catalogType;
	}
	@Override
	public String toString() {
		return "Report20002Bean [ministryId=" + ministryId + ", departmentId=" + departmentId + ", revenueTypeId="
				+ revenueTypeId + ", catalogTypeId=" + catalogTypeId + ", statusId=" + statusId + ", catalogStartDate="
				+ catalogStartDate + ", reportType=" + reportType + ", ministry=" + ministry + ", department="
				+ department + ", revenueType=" + revenueType + ", catalogType=" + catalogType + "]";
	}

	
}
