package th.go.cgd.epayment.report.model;

import java.util.Date;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import th.go.cgd.epayment.entity.User;

public class Report70006Bean {
	private String userLoginId= "";
	private String ministryId = "";
	private String departmentId = "";
	private String revenueTypeId = "";
	private String catalogTypeId = "";
	private String statusId = "";
	private Date catalogStartDate;
	private String reportType  = "";
	private String ministry = "";
	private String department = "";
	private String revenueType = "";
	private String catalogType = "";
	private String catalogCode = "";

	private String costCenterId = "";
	private String disbursementUnitId = "";
	private String status = "";
	private Date startDate;
	private Date endDate;	
	
	private String user;
	
	private String rptType;
	private Date startMonth;
	private Date endMonth;	
	private Date startYear;
	private Date endYear;	
	
	public String getRptType() {
		return rptType;
	}
	public void setRptType(String rptType) {
		this.rptType = rptType;
	}
	public Date getStartMonth() {
		return startMonth;
	}
	public void setStartMonth(Date startMonth) {
		this.startMonth = startMonth;
	}
	public Date getEndMonth() {
		return endMonth;
	}
	public void setEndMonth(Date endMonth) {
		this.endMonth = endMonth;
	}
	public Date getStartYear() {
		return startYear;
	}
	public void setStartYear(Date startYear) {
		this.startYear = startYear;
	}
	public Date getEndYear() {
		return endYear;
	}
	public void setEndYear(Date endYear) {
		this.endYear = endYear;
	}
	public String getUser() {
		User  q = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (q != null)
		{
			return q.getUserName();
		}
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}

	public String getCostCenterId() {
		return costCenterId;
	}
	public void setCostCenterId(String costCenterId) {
		this.costCenterId = costCenterId;
	}
	public String getDisbursementUnitId() {
		return disbursementUnitId;
	}
	public void setDisbursementUnitId(String disbursementUnitId) {
		this.disbursementUnitId = disbursementUnitId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getUserLoginId() {		
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			return user.getUserId()+"";
		}
		return userLoginId;
	}
	public void setUserLoginId(String userLoginId) {
		this.userLoginId = userLoginId;
	}
	public String getMinistryId() {
		return ministryId;
	}
	public void setMinistryId(String ministryId) {
		this.ministryId = ministryId;
	}
	public String getDepartmentId() {
		return departmentId;
	}
	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}
	public String getRevenueTypeId() {
		return revenueTypeId;
	}
	public void setRevenueTypeId(String revenueTypeId) {
		this.revenueTypeId = revenueTypeId;
	}
	public String getCatalogTypeId() {
		return catalogTypeId;
	}
	public void setCatalogTypeId(String catalogTypeId) {
		this.catalogTypeId = catalogTypeId;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public Date getCatalogStartDate() {
		return catalogStartDate;
	}
	public void setCatalogStartDate(Date catalogStartDate) {
		this.catalogStartDate = catalogStartDate;
	}
	public String getReportType() {
		return reportType;
	}
	public void setReportType(String reportType) {
		this.reportType = reportType;
	}
	public String getMinistry() {
		return ministry;
	}
	public void setMinistry(String ministry) {
		this.ministry = ministry;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getRevenueType() {
		return revenueType;
	}
	public void setRevenueType(String revenueType) {
		this.revenueType = revenueType;
	}
	public String getCatalogType() {
		return catalogType;
	}
	public void setCatalogType(String catalogType) {
		this.catalogType = catalogType;
	}			
	
	public String getCatalogCode() {
		return catalogCode;
	}
	public void setCatalogCode(String catalogCode) {
		this.catalogCode = catalogCode;
	}
	@Override
	public String toString() {
		return "Report70006Bean [ministryId=" + ministryId + ", departmentId=" + departmentId + ", revenueTypeId="
				+ revenueTypeId + ", catalogTypeId=" + catalogTypeId + ", statusId=" + statusId + ", catalogStartDate="
				+ catalogStartDate + ", reportType=" + reportType + ", ministry=" + ministry + ", department="
				+ department + ", revenueType=" + revenueType + ", catalogType=" + catalogType + "]";
	}

	
}
