package th.go.cgd.epayment.report.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.ExporterInput;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 * 
 * @author buncha.p
 *
 */
public abstract class AbstractJasperReport {

	private static final ObjectMapper mapper = new ObjectMapper();
	@Autowired
	private Connection jdbcConnection;
	public String jrxmlName;
	private File outDir;
	
	@Value("${report.jrxml.path}")
	private String reportJrxmlPath;

//	@Value("${report.pdf.path}")
//	private String reportPdfPath;

	public AbstractJasperReport(String jrxmlName) {
		super();
		this.jrxmlName = jrxmlName;
        // Make sure the output directory exists.
//	      this.outDir = new File("");
//	      if (!outDir.exists()) {
//	          outDir.mkdirs();
//	      }
	}
	
	protected abstract Map<String, Object> generateParameter(Object parameter);
	
	private final JasperReport compileJasper(){
		JasperReport jasperReport = null;
		ClassLoader classLoader = getClass().getClassLoader();
		try (InputStream inputStream = classLoader.getResourceAsStream(jrxmlName)) {
			jasperReport = JasperCompileManager.compileReport(inputStream);
		} catch (JRException | IOException e) {
			e.printStackTrace();
			return null;
		}
		return jasperReport;
	}

	public byte[] genarateReport(Object parameters,  String ReportType)
			throws ClassNotFoundException, SQLException, JRException {
		Map<String, Object> map = generateParameter(parameters);
		return genarateReport(map,  ReportType);
	}

	public byte[] genarateReport(Map<String, Object> parameters, String reportType)
			throws JRException {

		JasperPrint print = JasperFillManager.fillReport(compileJasper(), parameters, this.jdbcConnection);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		createDocument( print, os, reportType);
		return os.toByteArray();
	}

	private byte[] exportToPdf(JasperPrint print, String exportFileName) throws JRException{

		// PDF Exportor.
		JRPdfExporter exporter = new JRPdfExporter();

		ExporterInput exporterInput = new SimpleExporterInput(print);
		// ExporterInput
		exporter.setExporterInput(exporterInput);

		// ExporterOutput
		OutputStreamExporterOutput exporterOutput = new SimpleOutputStreamExporterOutput(
				outDir.getAbsolutePath() + "/" + exportFileName);
		// Output
		exporter.setExporterOutput(exporterOutput);

		//
		SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
		exporter.setConfiguration(configuration);
		exporter.exportReport();

		return JasperExportManager.exportReportToPdf(print);
	}
	

	private void createDocument(JasperPrint jasperPrint, ByteArrayOutputStream os, final String reportType) throws JRException  {
		
	    switch (reportType) {
	        case ReportType.PDF:
	            JasperExportManager.exportReportToPdfStream(jasperPrint, os);
	            break;
	        case ReportType.EXCEL:
	            JRXlsxExporter xlsxExporter = new JRXlsxExporter();
	            xlsxExporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
	            xlsxExporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.TRUE);
	            xlsxExporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
	            xlsxExporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
	            xlsxExporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, os);
	            xlsxExporter.exportReport();
	            break;
	        default:
	            break;
	    }
	}
	
	@SuppressWarnings("unchecked")
	public <T> Map<String, Object> objectToMap(T parameters) {
		return mapper.convertValue(parameters, Map.class);
	}

	public String getJrxmlName() {
		return jrxmlName;
	}

	public void setJrxmlName(String jrxmlName) {
		this.jrxmlName = jrxmlName;
	}
	
	
	
	public static class ReportType{
		public final static String PDF = "PDF";
		public final static String EXCEL = "EXCEL";
	}

}