package th.go.cgd.epayment.report.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.entity.MCatalogType;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MRevenueType;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.report.model.Report20001Bean;
import th.go.cgd.epayment.repository.MCatalogTypeRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MRevenueTypeRepository;
import th.go.cgd.epayment.repository.UserCenterRepository;

@Service
public class Report20001Service extends AbstractJasperReport{
	
	@Autowired
	private MessageResource messageResource;
	
	public Report20001Service(@Value("${report.jrxml.20001.path}") String jrxmlName) {
		super(jrxmlName);
	}

	@Override
	protected  Map<String, Object> generateParameter(Object parameter ) {
		
	    SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat dt2 = new SimpleDateFormat("d", new Locale("th","TH"));
	    SimpleDateFormat dt3 = new SimpleDateFormat("d MMMMM yyyy", new Locale("th","TH"));
	    SimpleDateFormat dt4 = new SimpleDateFormat("dd");
		
		
//		final String SQL =
//				"SELECT "+
//				" 	O_TOTAL ,O_REVENUE_TYPE_PUBLIC ,O_REVENUE_TYPE_EXCHEQURE ,O_REVENUE_TYPE_RATIO "+
//			    "FROM TABLE(PAYDB.FN_RPT20001_SUMMARY( :I_USERLOGIN, :I_MINISTRY_ID, :I_DEPARTMENT_ID, :I_REVENUE_TYPE_ID, :I_CATALOG_TYPE_ID, :I_STATUS_ID, :I_YEAR)"+
//			    ") AS RPT20001";
		
		Map<String, Object> param = new HashMap<String, Object>();//objectToMap(parameter);
		Report20001Bean bean = (Report20001Bean) parameter;
		
		param.put("I_DEPARTMENT", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		if( StringUtils.isNotEmpty(bean.getDepartmentId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()) );
			param.put("I_DEPARTMENT", department.getDepartmentName());
		}
		param.put("I_MINISTRY",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		if( StringUtils.isNotEmpty(bean.getMinistryId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()) );
			param.put("I_MINISTRY", department.getDepartmentName());
		}
		param.put("I_REVENUE_TYPE",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		if( StringUtils.isNotEmpty(bean.getRevenueTypeId() )){
			MRevenueType revenueType = revenueTypeRepository.findOne( Integer.valueOf(bean.getRevenueTypeId()) );
			param.put("I_REVENUE_TYPE", revenueType.getRevenueTypeName());
		}
		param.put("I_CATALOG_TYPE",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		if(  StringUtils.isNotEmpty(bean.getCatalogTypeId() )){
			MCatalogType catalogType = catalogTypeRepository.findOne( Integer.valueOf(bean.getCatalogTypeId()) );
			param.put("I_CATALOG_TYPE", catalogType.getCatalogTypeName());
		}
		
		param.put("I_STATUS",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		
//		final Calendar cal = Calendar.getInstance();
//		cal.setTime(bean.getCatalogStartDate());
//		int YEAR = cal.get(Calendar.YEAR);
		

//		Object result = null;
//		try{
//			result = entityManager.createNativeQuery(SQL)
//				.setParameter("I_USERLOGIN", bean.getUserLoginId())
//				.setParameter("I_MINISTRY_ID", bean.getMinistryId())
//				.setParameter("I_DEPARTMENT_ID", bean.getDepartmentId())
//				.setParameter("I_REVENUE_TYPE_ID", bean.getRevenueTypeId())
//				.setParameter("I_CATALOG_TYPE_ID", bean.getCatalogTypeId())
//				.setParameter("I_STATUS_ID", bean.getStatusId())
//				.setParameter("I_YEAR", YEAR)
//				.setMaxResults(1)
//				.getSingleResult();
//			
//			Object[] column = (Object[]) result;
//			param.put("O_TOTAL", String.valueOf(column[0]));
//			param.put("O_REVENUE_TYPE_PUBLIC", String.valueOf(column[1]));
//			param.put("O_REVENUE_TYPE_EXCHEQURE", String.valueOf(column[2]));
//			param.put("O_REVENUE_TYPE_RATIO", String.valueOf(column[3]));
//			
//			
//		}catch( NoResultException err){
//			param.put("O_TOTAL", "0");
//			param.put("O_REVENUE_TYPE_PUBLIC", "0");
//			param.put("O_REVENUE_TYPE_EXCHEQURE", "0");
//			param.put("O_REVENUE_TYPE_RATIO", "0");
//		}

		param.put("I_DATE_FROM", dt3.format(bean.getStartDate()));
		param.put("I_DATE_TO", dt3.format(bean.getEndDate()));

		param.put("I_START_DATE", dt1.format(bean.getStartDate()));
		param.put("I_END_DATE", dt1.format(bean.getEndDate()));
		param.put("I_START_TIME", bean.getStartTime());
		param.put("I_END_TIME", bean.getEndTime());
		param.put("I_WORK_TYPE_ID", bean.getWorkTypeId());
		
		param.put("I_USERLOGIN", bean.getUserLoginId());
//		param.put("I_USER", bean.getUser());
		
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			UserCenter u = userCenterRepository.findOne(user.getUserCenterId().getUserCenterId());
			String I_USER = u.getTitleId().getTitleName() + u.getFirstNameTh() + " " + u.getLastNameTh();
			param.put("I_USER", I_USER);
		}

		for (Map.Entry entry : param.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
		
		
		return param;
	}
	

	@Autowired
	private UserCenterRepository userCenterRepository;
	@Autowired
	private MDepartmentRepository departmentRepository;
	@Autowired
	private MRevenueTypeRepository revenueTypeRepository;
	@Autowired
	private MCatalogTypeRepository catalogTypeRepository;
	@PersistenceContext
	private EntityManager entityManager;
}
