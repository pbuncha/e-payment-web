package th.go.cgd.epayment.report.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.entity.MCatalogType;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MRevenueType;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.report.model.Report20002Bean;
import th.go.cgd.epayment.repository.MCatalogTypeRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MRevenueTypeRepository;
import th.go.cgd.epayment.repository.UserCenterRepository;

@Service
public class Report20002Service extends AbstractJasperReport{
	
	@Autowired
	private MessageResource messageResource;
	
	public Report20002Service(@Value("${report.jrxml.20002.path}") String jrxmlName) {
		super(jrxmlName);
	}

	@Override
	protected  Map<String, Object> generateParameter(Object parameter ) {
		
	    SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat dt2 = new SimpleDateFormat("d", new Locale("th","TH"));
	    SimpleDateFormat dt3 = new SimpleDateFormat("d MMMMM yyyy", new Locale("th","TH"));
	    SimpleDateFormat dt4 = new SimpleDateFormat("dd");
		
		
		final String SQL =
				"SELECT "+
				" 	O_STATUS_1, O_STATUS_2, O_STATUS_3, O_STATUS_4, O_STATUS_5 "+
			    "FROM TABLE(PAYDB.FN_RPT20002_SUMMARY( :I_USERLOGIN, :I_START_DATE, :I_END_DATE, :I_EMPLOYEE_TYPE_ID, :I_STATUS_ID, :I_WORK_TYPE_ID, :I_EMPLOYEE_ID)"+
			    ") AS RPT20002";
		
		Map<String, Object> param = new HashMap<String, Object>();//objectToMap(parameter);
		Report20002Bean bean = (Report20002Bean) parameter;
		
		param.put("I_DEPARTMENT", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		if( StringUtils.isNotEmpty(bean.getDepartmentId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()) );
			param.put("I_DEPARTMENT", department.getDepartmentName());
		}
		param.put("I_MINISTRY",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		if( StringUtils.isNotEmpty(bean.getMinistryId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()) );
			param.put("I_MINISTRY", department.getDepartmentName());
		}
		param.put("I_REVENUE_TYPE",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		if( StringUtils.isNotEmpty(bean.getRevenueTypeId() )){
			MRevenueType revenueType = revenueTypeRepository.findOne( Integer.valueOf(bean.getRevenueTypeId()) );
			param.put("I_REVENUE_TYPE", revenueType.getRevenueTypeName());
		}
		param.put("I_CATALOG_TYPE",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		if(  StringUtils.isNotEmpty(bean.getCatalogTypeId() )){
			MCatalogType catalogType = catalogTypeRepository.findOne( Integer.valueOf(bean.getCatalogTypeId()) );
			param.put("I_CATALOG_TYPE", catalogType.getCatalogTypeName());
		}
		
		param.put("I_STATUS",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
		
//		final Calendar cal = Calendar.getInstance();
//		cal.setTime(bean.getCatalogStartDate());
//		int YEAR = cal.get(Calendar.YEAR);
		
		System.out.println("Check1");
		
//		Object result = null;
//		try{
//			result = entityManager.createNativeQuery(SQL)
//				.setParameter("I_USERLOGIN", bean.getUserLoginId())
//				.setParameter("I_START_DATE", bean.getStartDate() != null? dt1.format(bean.getStartDate()) : "")
//				.setParameter("I_END_DATE", bean.getEndDate() != null? dt1.format(bean.getEndDate()) : "")
//				.setParameter("I_EMPLOYEE_TYPE_ID", bean.getEmployeeTypeId() )
//				.setParameter("I_STATUS_ID", bean.getStatusId())
//				.setParameter("I_WORK_TYPE_ID", bean.getWorkTypeId())
//				.setParameter("I_EMPLOYEE_ID", bean.getEmployeeId())
//				.setMaxResults(1)
//				.getSingleResult();
//			
//			Object[] column = (Object[]) result;
//			param.put("O_STATUS_1", Integer.valueOf(String.valueOf(column[0]==null?0:column[0])));
//			param.put("O_STATUS_2", Integer.valueOf(String.valueOf(column[1]==null?0:column[1])));
//			param.put("O_STATUS_3", Integer.valueOf(String.valueOf(column[2]==null?0:column[2])));
//			param.put("O_STATUS_4", Integer.valueOf(String.valueOf(column[3]==null?0:column[3])));
//			param.put("O_STATUS_5", Integer.valueOf(String.valueOf(column[4]==null?0:column[4])));
//						
//		}catch( NoResultException err){
//			param.put("O_STATUS_1", 0);
//			param.put("O_STATUS_2", 0);
//			param.put("O_STATUS_3", 0);
//			param.put("O_STATUS_4", 0);
//			param.put("O_STATUS_5", 0);
//		}


		param.put("O_STATUS_1", 0);
		param.put("O_STATUS_2", 0);
		param.put("O_STATUS_3", 0);
		param.put("O_STATUS_4", 0);
		param.put("O_STATUS_5", 0);
		
		System.out.println("Check2");
		
		param.put("I_DATE_FROM", dt3.format(bean.getStartDate()));
		param.put("I_DATE_TO", dt3.format(bean.getEndDate()));

		param.put("I_START_DATE", dt1.format(bean.getStartDate()));
		param.put("I_END_DATE", dt1.format(bean.getEndDate()));
		param.put("I_EMPLOYEE_TYPE_ID", bean.getEmployeeTypeId() );
		param.put("I_EMPLOYEE_ID", bean.getEmployeeId() );
		param.put("I_STATUS_ID", bean.getStatusId() );
		param.put("I_WORK_TYPE_ID", bean.getWorkTypeId());
		
		param.put("I_USERLOGIN", bean.getUserLoginId());
//		param.put("I_USER", bean.getUser());
		
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			UserCenter u = userCenterRepository.findOne(user.getUserCenterId().getUserCenterId());
			String I_USER = u.getTitleId().getTitleName() + u.getFirstNameTh() + " " + u.getLastNameTh();
			param.put("I_USER", I_USER);
		}

		for (Map.Entry entry : param.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
		
		
		return param;
	}
	

	@Autowired
	private UserCenterRepository userCenterRepository;
	@Autowired
	private MDepartmentRepository departmentRepository;
	@Autowired
	private MRevenueTypeRepository revenueTypeRepository;
	@Autowired
	private MCatalogTypeRepository catalogTypeRepository;
	@PersistenceContext
	private EntityManager entityManager;
}
