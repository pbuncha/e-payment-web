package th.go.cgd.epayment.report.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MDisbursementUnit;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.report.model.Report30007Bean;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MDisbursementUnitRepository;
import th.go.cgd.epayment.repository.UserCenterRepository;

@Service
public class Report30007Service extends AbstractJasperReport{
	
	@Autowired
	private MessageResource messageResource;

	public Report30007Service(@Value("${report.jrxml.30007.path}") String jrxmlName) {
		super(jrxmlName);
	}

	@Override
	protected  Map<String, Object> generateParameter(Object parameter ) {
		
	    SimpleDateFormat dt1 = new SimpleDateFormat("yyyy");
	    SimpleDateFormat dt2 = new SimpleDateFormat("d", new Locale("th","TH"));
	    SimpleDateFormat dt3 = new SimpleDateFormat("yyyy", new Locale("th","TH"));

//	    final String SQL =
//				"SELECT "+
//				"O_ROWS, O_TOTAL " +
//			    "FROM TABLE(PAYDB.FN_RPT30007_SUMMARY( :I_USERLOGIN, :I_DEPARTMENT, :I_N1, :I_N2, :I_N3, :I_START_DATE, :I_END_DATE)"+
//			    ") AS RPT30007";
		
		Map<String, Object> param = new HashMap<String, Object>();//objectToMap(parameter);
		Report30007Bean bean = (Report30007Bean) parameter;

		final Calendar cal1 = Calendar.getInstance();
		cal1.setTime(bean.getStartDate());
		int yearFrom = cal1.get(Calendar.YEAR);

		final Calendar cal2 = Calendar.getInstance();
		cal2.setTime(bean.getEndDate());
		int yearTo = cal2.get(Calendar.YEAR);
		
//		Object result = null;
//		try{
//			result = entityManager.createNativeQuery(SQL)
//				.setParameter("I_USERLOGIN", bean.getUserLoginId())
//				.setParameter("I_DEPARTMENT", bean.getDepartmentId())
//				.setParameter("I_N1", "" )		//I_DISBURSE
//				.setParameter("I_N2", "")		//I_COST_CENTER
//				.setParameter("I_N3", "")	//I_STATUS
//				.setParameter("I_START_DATE", "")
//				.setParameter("I_END_DATE", "")
//				.setMaxResults(1)
//				.getSingleResult();
//			
//			Object[] column = (Object[]) result;
//			param.put("O_ROWS", String.valueOf(column[0]));
//			param.put("O_TOTAL", String.valueOf(column[0]));
//			
//			
//		}catch( NoResultException err){
//			param.put("O_ROWS", "0");
//			param.put("O_TOTAL", "0");
//		}
		
//		param.put("I_MINISTRY",  messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));			
//		if( StringUtils.isNotEmpty(bean.getMinistryId())){
//			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getMinistryId() ) );
//			param.put("I_MINISTRY", department.getDepartmentName());
//		}
		param.put("I_DEPARTMENT", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getDepartmentId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()) );
			param.put("I_DEPARTMENT", department.getDepartmentName());
		}
		param.put("I_DISBURSE", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getDisbursementUnitId())){
			MDisbursementUnit disbursementUnit = disbursementUnitRepository.findOne( Integer.valueOf(bean.getDisbursementUnitId()) );
			param.put("I_DISBURSE", disbursementUnit.getDisbursementUnitDesc());
		}
		param.put("I_COST_CENTER", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getCostCenterId())){
			MCostCenter costcenter = costCenterRepository.findOne( Integer.valueOf(bean.getCostCenterId()) );
			param.put("I_COST_CENTER", costcenter.getCostCenterName());
		}
		if(bean.getStartDate() != null){
			param.put("I_START_YEAR", dt1.format(bean.getStartDate()));
			param.put("I_YEAR_FROM", dt3.format(bean.getStartDate()));
		}
		if( bean.getEndDate() != null){
			param.put("I_END_YEAR", dt1.format(bean.getEndDate()));
			param.put("I_YEAR_TO", dt3.format(bean.getEndDate()));
		}

		param.put("I_USERLOGIN", bean.getUserLoginId());
//		param.put("I_USER", bean.getUser());
		param.put("I_DEPARTMENT_ID", bean.getDepartmentId());
		param.put("I_DISBURSE_ID", bean.getDisbursementUnitId());
		param.put("I_COST_CENTER_ID", bean.getCostCenterId());
				
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			UserCenter u = userCenterRepository.findOne(user.getUserCenterId().getUserCenterId());
			String I_USER = u.getTitleId().getTitleName() + u.getFirstNameTh() + " " + u.getLastNameTh();
			param.put("I_USER", I_USER);
		}

		for (Map.Entry entry : param.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
		
		return param;
	}
	

	@Autowired
	private UserCenterRepository userCenterRepository;
	@Autowired
	private MDepartmentRepository departmentRepository;
	@Autowired
	private MCostCenterRepository costCenterRepository;
	@Autowired
	private MDisbursementUnitRepository disbursementUnitRepository;
//	@PersistenceContext
//	private EntityManager entityManager;
}
