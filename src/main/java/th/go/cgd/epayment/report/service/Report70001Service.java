package th.go.cgd.epayment.report.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MDisbursementUnit;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.report.model.Report70001Bean;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MDisbursementUnitRepository;
import th.go.cgd.epayment.repository.UserCenterRepository;
import th.go.cgd.epayment.service.UserService;

@Service
public class Report70001Service extends AbstractJasperReport{
	
	@Autowired
	private MessageResource messageResource;

	public Report70001Service(@Value("${report.jrxml.70001.path}") String jrxmlName) {
		super(jrxmlName);
	}

	@Override
	protected  Map<String, Object> generateParameter(Object parameter ) {
		
	    SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat dt2 = new SimpleDateFormat("d", new Locale("th","TH"));
	    SimpleDateFormat dt3 = new SimpleDateFormat("d MMMMM yyyy", new Locale("th","TH"));
	    
	    

	    final String SQL =
	    		"SELECT O_BILL_TOTAL, O_BILL_WPRINT,O_BILL_PRINT,O_BILL_PRINT_COPY FROM TABLE(PAYDB.FN_RPT70001_SUMMARY("
	    		+ ":I_USERLOGIN, "
	    		+ ":I_DEPARTMENT_ID, "
	    		+ ":I_DISBURSE_ID, "
	    		+ ":I_COST_CENTER_ID, "
	    		+ ":I_STATUS_ID, "
	    		+ ":I_START_DATE, "
	    		+ ":I_END_DATE)) AS RPT70001_SUMMARY";
		
		Map<String, Object> param = new HashMap<String, Object>();//objectToMap(parameter);
		Report70001Bean bean = (Report70001Bean) parameter;

//		final Calendar cal = Calendar.getInstance();
//		cal.setTime(bean.getCatalogStartDate());
//		int YEAR = cal.get(Calendar.YEAR);
		
		Object result = null;
		try{
			result = entityManager.createNativeQuery(SQL)
				.setParameter("I_USERLOGIN", bean.getUserLoginId())
				.setParameter("I_DEPARTMENT_ID", bean.getDepartmentId())
				.setParameter("I_DISBURSE_ID", bean.getDisbursementUnitId() )		//I_DISBURSE
				.setParameter("I_COST_CENTER_ID", bean.getCostCenterId())		//I_COST_CENTER
				.setParameter("I_STATUS_ID", StringUtils.isNotEmpty(bean.getStatus())?bean.getStatus().split(":",2)[0]:"")	//I_STATUS
				.setParameter("I_START_DATE", bean.getStartDate() != null? dt1.format(bean.getStartDate()) : "")
				.setParameter("I_END_DATE", bean.getEndDate() != null? dt1.format(bean.getEndDate()) : "")
				.setMaxResults(1)
				.getSingleResult();
			
			Object[] column = (Object[]) result;
			param.put("O_BILL_TOTAL", 		Integer.valueOf(String.valueOf(column[0]==null?0:column[0])));
			param.put("O_BILL_WPRINT", 		Integer.valueOf(String.valueOf(column[1]==null?0:column[1])));
			param.put("O_BILL_PRINT", 		Integer.valueOf(String.valueOf(column[2]==null?0:column[2])));
			param.put("O_BILL_PRINT_COPY", 	Integer.valueOf(String.valueOf(column[3]==null?0:column[3])));
			
		}catch( NoResultException err){
			param.put("O_BILL_TOTAL", 0);
			param.put("O_BILL_WPRINT", 0);
			param.put("O_BILL_PRINT", 0);
			param.put("O_BILL_PRINT_COPY", 0);
		}
		
		param.put("I_DEPARTMENT", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getDepartmentId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()) );
			param.put("I_DEPARTMENT", department.getDepartmentName());
		}
		param.put("I_DISBURSE", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getDisbursementUnitId())){
			MDisbursementUnit disbursementUnit = disbursementUnitRepository.findOne( Integer.valueOf(bean.getDisbursementUnitId()) );
			param.put("I_DISBURSE", disbursementUnit.getDisbursementUnitDesc());
		}
		param.put("I_COST_CENTER", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getCostCenterId())){
			MCostCenter costcenter = costCenterRepository.findOne( Integer.valueOf(bean.getCostCenterId()) );
			param.put("I_COST_CENTER", costcenter.getCostCenterName());
		}
		if(bean.getStartDate() != null){
			param.put("I_START_DATE", dt1.format(bean.getStartDate()));
			param.put("I_DATE_FROM", dt2.format(bean.getStartDate()));

			Calendar cal1 = Calendar.getInstance();
			cal1.setTime(bean.getStartDate());
			int startMonth = cal1.get(Calendar.MONTH);

			Calendar cal2 = Calendar.getInstance();
			cal2.setTime(bean.getEndDate());
			int endMonth = cal2.get(Calendar.MONTH);

			if (startMonth != endMonth) {
				param.put("I_DATE_FROM", dt3.format(bean.getStartDate()));			
			}
		}
		if( bean.getEndDate() != null){
			param.put("I_END_DATE", dt1.format(bean.getEndDate()));
			param.put("I_DATE_TO", dt3.format(bean.getEndDate()));
		}
		param.put("I_STATUS", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		param.put("I_STATUS_ID", "");
		if( StringUtils.isNotEmpty(bean.getStatus())){
			param.put("I_STATUS_ID", bean.getStatus().split(":",2)[0]);
			param.put("I_STATUS", bean.getStatus().split(":",2)[1]);	//  0|รอพิมพ์
		}

		param.put("I_USERLOGIN", bean.getUserLoginId());
		//param.put("I_USER", bean.getUser());
		param.put("I_DEPARTMENT_ID", bean.getDepartmentId());
		param.put("I_DISBURSE_ID", bean.getDisbursementUnitId());
		param.put("I_COST_CENTER_ID", bean.getCostCenterId());
				
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			UserCenter u = userCenterRepository.findOne(user.getUserCenterId().getUserCenterId());
			String I_USER = u.getTitleId().getTitleName() + u.getFirstNameTh() + " " + u.getLastNameTh();
			param.put("I_USER", I_USER);
		}
		
		for (Map.Entry entry : param.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
		
		return param;
	}
	
	@Autowired
	private UserCenterRepository userCenterRepository;
	@Autowired
	private MDepartmentRepository departmentRepository;
	@Autowired
	private MCostCenterRepository costCenterRepository;
	@Autowired
	private MDisbursementUnitRepository disbursementUnitRepository;
	@PersistenceContext
	private EntityManager entityManager;
}
