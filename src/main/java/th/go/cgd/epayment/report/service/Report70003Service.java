package th.go.cgd.epayment.report.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MDisbursementUnit;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.report.model.Report70003Bean;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MDisbursementUnitRepository;
import th.go.cgd.epayment.repository.UserCenterRepository;

@Service
public class Report70003Service extends AbstractJasperReport{
	
	@Autowired
	private MessageResource messageResource;

	@Value("${report.jrxml.70003.path}")
	private String RPT70003;
	@Value("${report.jrxml.70004.path}")
	private String RPT70004;
	@Value("${report.jrxml.70004_1.path}")
	private String RPT70004_1;
	@Value("${report.jrxml.70004_2.path}")
	private String RPT70004_2;
	@Value("${report.jrxml.70005.path}")
	private String RPT70005;
	@Value("${report.jrxml.70005_1.path}")
	private String RPT70005_1;
	@Value("${report.jrxml.70005_2.path}")
	private String RPT70005_2;

	public Report70003Service(@Value("${report.jrxml.70003.path}") String jrxmlName) {
		super(jrxmlName);
	}

	@Override
	protected  Map<String, Object> generateParameter(Object parameter ) {
		
	    SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat dt2 = new SimpleDateFormat("d", new Locale("th","TH"));
	    SimpleDateFormat dt3 = new SimpleDateFormat("d MMMMM yyyy", new Locale("th","TH"));
	    SimpleDateFormat dt4 = new SimpleDateFormat("dd");
	    SimpleDateFormat dt5 = new SimpleDateFormat("yyyy", new Locale("th","TH"));
	    SimpleDateFormat dt6 = new SimpleDateFormat("yyyy");
	    
	    

//	    final String SQL =
//	    		"SELECT O_BILL_TOTAL,O_BILL_AMT,O_PBILL_TOTAL,O_PBILL_AMT,O_WPBILL_TOTAL,O_WPBILL_AMT FROM TABLE(PAYDB.FN_RPT70003("
//	    		+ ":I_USERLOGIN, "
//	    		+ ":I_MINISTRY_ID, "
//	    		+ ":I_DISBURSE_ID, "
//	    		+ ":I_COST_CENTER_ID, "
//	    		+ ":I_RECEIPT_DATE)) AS RPT70003";
		
		Map<String, Object> param = new HashMap<String, Object>();//objectToMap(parameter);
		Report70003Bean bean = (Report70003Bean) parameter;

//		jrxmlName = "jasper/RPT70003.jrxml";
//		System.out.println("jrxmlName0:" +jrxmlName);
//		if (bean.getRptType() == "1")
//		{
//			jrxmlName = "jasper/RPT70003.jrxml";
//		}
//		else if (bean.getRptType() == "2")
//		{
//			jrxmlName = "jasper/RPT70004.jrxml";
//			if( StringUtils.isEmpty(bean.getDepartmentId())){
//				jrxmlName = "jasper/RPT70004_1.jrxml";
//				if( StringUtils.isEmpty(bean.getMinistryId())){
//					jrxmlName = "jasper/RPT70004_2.jrxml";
//				}
//			}			
//		}
//		else if (bean.getRptType() == "3")
//		{
//			jrxmlName = "jasper/RPT70005.jrxml";
//			if( StringUtils.isEmpty(bean.getDepartmentId())){
//				jrxmlName = "jasper/RPT70005_1.jrxml";
//				if( StringUtils.isEmpty(bean.getMinistryId())){
//					jrxmlName = "jasper/RPT70005_2.jrxml";
//				}
//			}			
//		}
//		
//		System.out.println("bean.getDepartmentId():" +bean.getDepartmentId());
//		System.out.println("bean.getMinistryId():" +bean.getMinistryId());
		
		
//		final Calendar cal = Calendar.getInstance();
//		cal.setTime(bean.getCatalogStartDate());
//		int YEAR = cal.get(Calendar.YEAR);
		
//		Object result = null;
//		try{
//			result = entityManager.createNativeQuery(SQL)
//				.setParameter("I_USERLOGIN", bean.getUserLoginId())
//				.setParameter("I_MINISTRY_ID", bean.getMinistryId())
//				.setParameter("I_DISBURSE_ID", bean.getDisbursementUnitId() )		//I_DISBURSE
//				.setParameter("I_COST_CENTER_ID", bean.getCostCenterId())		//I_COST_CENTER
//				.setParameter("I_RECEIPT_DATE", bean.getStartDate() != null? dt1.format(bean.getStartDate()) : "")
//				.setMaxResults(1)
//				.getSingleResult();
//			
//			Object[] column = (Object[]) result;
//			param.put("O_BILL_TOTAL", String.valueOf(column[0]));
//			param.put("O_BILL_AMT", String.valueOf(column[1]));
//			param.put("O_PBILL_TOTAL", String.valueOf(column[2]));
//			param.put("O_PBILL_AMT", String.valueOf(column[3]));
//			param.put("O_WPBILL_TOTAL", String.valueOf(column[4]));
//			param.put("O_WPBILL_AMT", String.valueOf(column[5]));
//
//		}catch( NoResultException err){
//			param.put("O_BILL_TOTAL", "0");
//			param.put("O_BILL_AMT", "0");
//			param.put("O_PBILL_TOTAL", "0");
//			param.put("O_PBILL_AMT", "0");
//			param.put("O_WPBILL_TOTAL", "0");
//			param.put("O_WPBILL_AMT", "0");
//		}
		
		param.put("I_MINISTRY", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getMinistryId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getMinistryId()));
			param.put("I_MINISTRY", department.getDepartmentName());
		}
		param.put("I_DEPARTMENT", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getDepartmentId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()));
			param.put("I_DEPARTMENT", department.getDepartmentName());
		}
		param.put("I_DISBURSE", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getDisbursementUnitId())){
			MDisbursementUnit disbursementUnit = disbursementUnitRepository.findOne( Integer.valueOf(bean.getDisbursementUnitId()) );
			param.put("I_DISBURSE", disbursementUnit.getDisbursementUnitDesc());
		}
		param.put("I_COST_CENTER", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getCostCenterId())){
			MCostCenter costcenter = costCenterRepository.findOne( Integer.valueOf(bean.getCostCenterId()) );
			param.put("I_COST_CENTER", costcenter.getCostCenterName());
		}

		param.put("I_USERLOGIN", bean.getUserLoginId());
		param.put("I_USER", bean.getUser());
		param.put("I_MINISTRY_ID", bean.getMinistryId());
		param.put("I_DEPARTMENT_ID", bean.getDepartmentId());
		param.put("I_DISBURSE_ID", bean.getDisbursementUnitId());
		param.put("I_COST_CENTER_ID", bean.getCostCenterId());
		param.put("I_RPT_TYPE", bean.getRptType());
		
		if ("1".equals(bean.getRptType()))
		{
			jrxmlName = RPT70003;

			param.put("I_DATE_FROM", dt3.format(bean.getStartDate()));
			param.put("I_DATE_TO", dt3.format(bean.getEndDate()));

			param.put("I_START_DATE", dt1.format(bean.getStartDate()));
			param.put("I_END_DATE", dt1.format(bean.getEndDate()));
			
			Calendar c = Calendar.getInstance();
			c.setTime(bean.getStartDate());
			c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
			Date lastDayOfMonth = c.getTime();			
			param.put("I_EOD", dt4.format(lastDayOfMonth));			
		}
		else if ("2".equals(bean.getRptType()))
		{
			jrxmlName = RPT70004;
			if( StringUtils.isEmpty(bean.getDepartmentId())){
				jrxmlName = RPT70004_1;
				if( StringUtils.isEmpty(bean.getMinistryId())){
					jrxmlName = RPT70004_2;
				}
			}			
			param.put("I_YEAR_FROM", dt5.format(bean.getStartYear()));
			param.put("I_START_YEAR", dt6.format(bean.getStartYear()));

//			param.put("I_DATE_FROM", dt3.format(bean.getStartMonth()));
//			param.put("I_DATE_TO", dt3.format(bean.getEndMonth()));
//
//			param.put("I_START_DATE", dt1.format(bean.getStartMonth()));
//			param.put("I_END_DATE", dt1.format(bean.getEndMonth()));
//
//			Calendar c = Calendar.getInstance();
//			c.setTime(bean.getStartMonth());
//			c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
//			Date lastDayOfMonth = c.getTime();			
//			param.put("I_EOD", dt4.format(lastDayOfMonth));			
		}
		else if ("3".equals(bean.getRptType()))
		{
			jrxmlName = RPT70005;
			if( StringUtils.isEmpty(bean.getDepartmentId())){
				jrxmlName = RPT70005_1;
				if( StringUtils.isEmpty(bean.getMinistryId())){
					jrxmlName = RPT70005_2;
				}
			}			
			param.put("I_YEAR_FROM", dt5.format(bean.getStartYear()));
			param.put("I_YEAR_TO", dt5.format(bean.getEndYear()));
			param.put("I_START_YEAR", dt6.format(bean.getStartYear()));
			param.put("I_END_YEAR", dt6.format(bean.getEndYear()));

//			param.put("I_DATE_FROM", dt3.format(bean.getStartYear()));
//			param.put("I_DATE_TO", dt3.format(bean.getEndYear()));
//
//			param.put("I_START_DATE", dt1.format(bean.getStartYear()));
//			param.put("I_END_DATE", dt1.format(bean.getEndYear()));
//
//			Calendar c = Calendar.getInstance();
//			c.setTime(bean.getStartYear());
//			c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
//			Date lastDayOfMonth = c.getTime();			
//			param.put("I_EOD", dt4.format(lastDayOfMonth));			
		}		
		System.out.println("jrxmlName:" +jrxmlName);		

		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			UserCenter u = userCenterRepository.findOne(user.getUserCenterId().getUserCenterId());
			String I_USER = u.getTitleId().getTitleName() + u.getFirstNameTh() + " " + u.getLastNameTh();
			param.put("I_USER", I_USER);
		}

		for (Map.Entry entry : param.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
		
		return param;
	}
	

	@Autowired
	private UserCenterRepository userCenterRepository;
	@Autowired
	private MDepartmentRepository departmentRepository;
	@Autowired
	private MCostCenterRepository costCenterRepository;
	@Autowired
	private MDisbursementUnitRepository disbursementUnitRepository;
	@PersistenceContext
	private EntityManager entityManager;
}
