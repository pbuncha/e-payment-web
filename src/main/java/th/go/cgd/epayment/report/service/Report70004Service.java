package th.go.cgd.epayment.report.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MDisbursementUnit;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.report.model.Report70004Bean;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MDisbursementUnitRepository;
import th.go.cgd.epayment.repository.UserCenterRepository;

@Service
public class Report70004Service extends AbstractJasperReport{
	
	@Autowired
	private MessageResource messageResource;

	public Report70004Service(@Value("${report.jrxml.70004.path}") String jrxmlName) {
		super(jrxmlName);
	}

	@Override
	protected  Map<String, Object> generateParameter(Object parameter ) {
		
	    SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat dt2 = new SimpleDateFormat("d", new Locale("th","TH"));
	    SimpleDateFormat dt3 = new SimpleDateFormat("d MMMMM yyyy", new Locale("th","TH"));
	    SimpleDateFormat dt4 = new SimpleDateFormat("dd");
	    SimpleDateFormat dt5 = new SimpleDateFormat("yyyy", new Locale("th","TH"));
	    SimpleDateFormat dt6 = new SimpleDateFormat("yyyy");
	    
	    

//	    final String SQL =
//	    		"SELECT O_BILL_TOTAL,O_BILL_AMT,O_PBILL_TOTAL,O_PBILL_AMT,O_WPBILL_TOTAL,O_WPBILL_AMT FROM TABLE(PAYDB.FN_RPT70004("
//	    		+ ":I_USERLOGIN, "
//	    		+ ":I_MINISTRY_ID, "
//	    		+ ":I_DISBURSE_ID, "
//	    		+ ":I_COST_CENTER_ID, "
//	    		+ ":I_RECEIPT_DATE)) AS RPT70004";
		
		Map<String, Object> param = new HashMap<String, Object>();//objectToMap(parameter);
		Report70004Bean bean = (Report70004Bean) parameter;


		System.out.println("jrxmlName0:" +jrxmlName);

		jrxmlName = "jasper/RPT70004.jrxml";
		if( StringUtils.isEmpty(bean.getDepartmentId())){
			jrxmlName = "jasper/RPT70004_1.jrxml";
			if( StringUtils.isEmpty(bean.getMinistryId())){
				jrxmlName = "jasper/RPT70004_2.jrxml";
			}
		}				
		
		System.out.println("bean.getDepartmentId():" +bean.getDepartmentId());
		System.out.println("bean.getMinistryId():" +bean.getMinistryId());
		System.out.println("jrxmlName:" +jrxmlName);
		
//		final Calendar cal = Calendar.getInstance();
//		cal.setTime(bean.getCatalogStartDate());
//		int YEAR = cal.get(Calendar.YEAR);
		
//		Object result = null;
//		try{
//			result = entityManager.createNativeQuery(SQL)
//				.setParameter("I_USERLOGIN", bean.getUserLoginId())
//				.setParameter("I_MINISTRY_ID", bean.getMinistryId())
//				.setParameter("I_DISBURSE_ID", bean.getDisbursementUnitId() )		//I_DISBURSE
//				.setParameter("I_COST_CENTER_ID", bean.getCostCenterId())		//I_COST_CENTER
//				.setParameter("I_RECEIPT_DATE", bean.getStartDate() != null? dt1.format(bean.getStartDate()) : "")
//				.setMaxResults(1)
//				.getSingleResult();
//			
//			Object[] column = (Object[]) result;
//			param.put("O_BILL_TOTAL", String.valueOf(column[0]));
//			param.put("O_BILL_AMT", String.valueOf(column[1]));
//			param.put("O_PBILL_TOTAL", String.valueOf(column[2]));
//			param.put("O_PBILL_AMT", String.valueOf(column[3]));
//			param.put("O_WPBILL_TOTAL", String.valueOf(column[4]));
//			param.put("O_WPBILL_AMT", String.valueOf(column[5]));
//
//		}catch( NoResultException err){
//			param.put("O_BILL_TOTAL", "0");
//			param.put("O_BILL_AMT", "0");
//			param.put("O_PBILL_TOTAL", "0");
//			param.put("O_PBILL_AMT", "0");
//			param.put("O_WPBILL_TOTAL", "0");
//			param.put("O_WPBILL_AMT", "0");
//		}
		
		param.put("I_MINISTRY", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getMinistryId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getMinistryId()));
			param.put("I_MINISTRY", department.getDepartmentName());
		}
		param.put("I_DEPARTMENT", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getDepartmentId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()));
			param.put("I_DEPARTMENT", department.getDepartmentName());
		}
		param.put("I_DISBURSE", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getDisbursementUnitId())){
			MDisbursementUnit disbursementUnit = disbursementUnitRepository.findOne( Integer.valueOf(bean.getDisbursementUnitId()) );
			param.put("I_DISBURSE", disbursementUnit.getDisbursementUnitDesc());
		}
		param.put("I_COST_CENTER", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( StringUtils.isNotEmpty(bean.getCostCenterId())){
			MCostCenter costcenter = costCenterRepository.findOne( Integer.valueOf(bean.getCostCenterId()) );
			param.put("I_COST_CENTER", costcenter.getCostCenterName());
		}

		param.put("I_USERLOGIN", bean.getUserLoginId());
//		param.put("I_USER", bean.getUser());
		param.put("I_MINISTRY_ID", bean.getMinistryId());
		param.put("I_DEPARTMENT_ID", bean.getDepartmentId());
		param.put("I_DISBURSE_ID", bean.getDisbursementUnitId());
		param.put("I_COST_CENTER_ID", bean.getCostCenterId());
		param.put("I_YEAR_FROM", dt5.format(bean.getStartYear()));
		param.put("I_START_YEAR", dt6.format(bean.getStartYear()));

		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			UserCenter u = userCenterRepository.findOne(user.getUserCenterId().getUserCenterId());
			String I_USER = u.getTitleId().getTitleName() + u.getFirstNameTh() + " " + u.getLastNameTh();
			param.put("I_USER", I_USER);
		}

		for (Map.Entry entry : param.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
		
		return param;
	}
	

	@Autowired
	private UserCenterRepository userCenterRepository;
	@Autowired
	private MDepartmentRepository departmentRepository;
	@Autowired
	private MCostCenterRepository costCenterRepository;
	@Autowired
	private MDisbursementUnitRepository disbursementUnitRepository;
	@PersistenceContext
	private EntityManager entityManager;
}
