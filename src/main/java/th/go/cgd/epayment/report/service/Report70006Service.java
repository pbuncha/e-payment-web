package th.go.cgd.epayment.report.service;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MDisbursementUnit;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.report.model.Report70006Bean;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MDisbursementUnitRepository;
import th.go.cgd.epayment.repository.UserCenterRepository;

@Service
public class Report70006Service extends AbstractJasperReport{
	
	@Autowired
	private MessageResource messageResource;

	public Report70006Service(@Value("${report.jrxml.70006.path}") String jrxmlName) {
		super(jrxmlName);
	}

	@Override
	protected  Map<String, Object> generateParameter(Object parameter ) {
		
	    SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
	    SimpleDateFormat dt2 = new SimpleDateFormat("d", new Locale("th","TH"));
	    SimpleDateFormat dt3 = new SimpleDateFormat("d MMMMM yyyy", new Locale("th","TH"));
	    SimpleDateFormat dt4 = new SimpleDateFormat("dd");
	    
	    

	    final String SQL =
	    		"SELECT O_ROWS, O_TOTAL_PAY_AMT "
	    		+ "FROM TABLE(PAYDB.FN_RPT70006_SUMMARY("
	    		+ ":I_USERLOGIN, "
	    		+ ":I_MINISTRY_ID, "
	    		+ ":I_DEPARTMENT_ID, "
	    		+ ":I_CAT_COST_CENTER_ID, "
	    		+ ":I_START_DATE, "
	    		+ ":I_END_DATE)) AS RPT70006";
		
		Map<String, Object> param = new HashMap<String, Object>();//objectToMap(parameter);
		Report70006Bean bean = (Report70006Bean) parameter;

//		final Calendar cal = Calendar.getInstance();
//		cal.setTime(bean.getCatalogStartDate());
//		int YEAR = cal.get(Calendar.YEAR);
		
		Object result = null;
		try{
			result = entityManager.createNativeQuery(SQL)
				.setParameter("I_USERLOGIN", bean.getUserLoginId())
				.setParameter("I_MINISTRY_ID", bean.getMinistryId())
				.setParameter("I_DEPARTMENT_ID", bean.getDepartmentId() )
				.setParameter("I_CAT_COST_CENTER_ID", bean.getCostCenterId())
				.setParameter("I_START_DATE", bean.getStartDate() != null? dt1.format(bean.getStartDate()) : "")
				.setParameter("I_END_DATE", bean.getEndDate() != null? dt1.format(bean.getEndDate()) : "")
				.setMaxResults(1)
				.getSingleResult();
			
			Object[] column = (Object[]) result;
						
//			DecimalFormat df = new DecimalFormat("#,##0.00");
//			
//			System.out.println("String.valueOf(column[1])=" +String.valueOf(column[1]));	
//			
//			BigDecimal zz = BigDecimal.ZERO;			
//			if (!StringUtils.isEmpty(column[1]))
//			{
//				zz = new BigDecimal(String.valueOf(column[1]));				
//			}
//			
//			System.out.println(zz);
//						
//			String O_TOTAL_PAY_AMT = df.format(zz);
//			System.out.println("O_TOTAL_PAY_AMT="+O_TOTAL_PAY_AMT);
						
			param.put("O_TOTAL_PAY_AMT", new BigDecimal(String.valueOf(column[1]==null?0:column[1])));

		}catch( NoResultException err){
			param.put("O_TOTAL_PAY_AMT", "0");
		}
		
		param.put("I_MINISTRY", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( !StringUtils.isEmpty(bean.getMinistryId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getMinistryId()));
			param.put("I_MINISTRY", department.getDepartmentName());
		}
		param.put("I_DEPARTMENT", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( !StringUtils.isEmpty(bean.getDepartmentId())){
			MDepartment department = departmentRepository.findOne( Integer.valueOf(bean.getDepartmentId()));
			param.put("I_DEPARTMENT", department.getDepartmentName());
		}
		param.put("I_DISBURSE", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( !StringUtils.isEmpty(bean.getDisbursementUnitId())){
			MDisbursementUnit disbursementUnit = disbursementUnitRepository.findOne( Integer.valueOf(bean.getDisbursementUnitId()) );
			param.put("I_DISBURSE", disbursementUnit.getDisbursementUnitDesc());
		}
		param.put("I_CAT_COST_CENTER", messageResource.getMessage(MessageResource.REPORT_PICK_ALL_DESC));
		if( !StringUtils.isEmpty(bean.getCostCenterId())){
			MCostCenter costcenter = costCenterRepository.findOne( Integer.valueOf(bean.getCostCenterId()) );
			param.put("I_CAT_COST_CENTER", costcenter.getCostCenterName());
		}

		param.put("I_USERLOGIN", bean.getUserLoginId());
//		param.put("I_USER", bean.getUser());
		param.put("I_MINISTRY_ID", bean.getMinistryId());
		param.put("I_DEPARTMENT_ID", bean.getDepartmentId());
		param.put("I_DISBURSE_ID", bean.getDisbursementUnitId());
		param.put("I_COST_CENTER_ID", bean.getCostCenterId());
		param.put("I_CAT_COST_CENTER_ID", bean.getCostCenterId());
		param.put("I_DATE_FROM", dt3.format(bean.getStartDate()));
		param.put("I_DATE_TO", dt3.format(bean.getEndDate()));
		param.put("I_START_DATE", dt1.format(bean.getStartDate()));
		param.put("I_END_DATE", dt1.format(bean.getEndDate()));
		
		User  user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();		
		if (user != null)
		{
			UserCenter u = userCenterRepository.findOne(user.getUserCenterId().getUserCenterId());
			String I_USER = u.getTitleId().getTitleName() + u.getFirstNameTh() + " " + u.getLastNameTh();
			param.put("I_USER", I_USER);
		}

		for (Map.Entry entry : param.entrySet()) {
		    System.out.println(entry.getKey() + ", " + entry.getValue());
		}
		
		return param;
	}
	

	@Autowired
	private UserCenterRepository userCenterRepository;
	@Autowired
	private MDepartmentRepository departmentRepository;
	@Autowired
	private MCostCenterRepository costCenterRepository;
	@Autowired
	private MDisbursementUnitRepository disbursementUnitRepository;
	@PersistenceContext
	private EntityManager entityManager;
}
