package th.go.cgd.epayment.report.service;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.*;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ReportUtilService {

	/**
	 * 
	 * @param header
	 * @param datatypes
	 * @param sheetName
	 * @return
	 */
	public static byte[] genarateCustomExcelReport( Object[][] datatypes, String sheetName) {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet;
		
		if (StringUtils.isNotEmpty(sheetName)) {
			sheet = workbook.createSheet(sheetName);
		} else {
			sheet = workbook.createSheet();
		}

		int rowNum = 0;
		for (Object[] datatype : datatypes) {
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			for (Object field : datatype) {
				Cell cell = row.createCell(colNum++);
				setCell(cell, field);
			}
		}

		try {
			workbook.write(bos);
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();

		} finally {
			try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println( bos.size() );
		return bos.toByteArray();
	}
	
	
	public static byte[] genarateCustomPdfReport( Object[][] datatypes) throws FileNotFoundException, DocumentException {
		
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet  = workbook.createSheet();
		
		Document iText_xls_2_pdf = new Document(PageSize.A4, 0.75F, 0.75F, 25F, 25F);
		PdfWriter.getInstance(iText_xls_2_pdf,  bos);
		iText_xls_2_pdf.open();

		PdfPTable my_table = new PdfPTable(datatypes[0].length);
        //We will use the object below to dynamically add new data to the table
        PdfPCell table_cell = null;
        
		int rowNum = 0;


		for (Object[] datatype : datatypes) {
			Row row = sheet.createRow(rowNum++);
			int colNum = 0;
			for (Object field : datatype) {
				Cell cell = row.createCell(colNum++);
//				setCell(cell, field);
				
				
				 
				 if (field instanceof String) {
						cell.setCellValue((String) field);
						table_cell=new PdfPCell(new Phrase(cell.getStringCellValue()));
					} else if (field instanceof Integer) {
						cell.setCellValue((Integer) field);
						table_cell=new PdfPCell(new Phrase( String.valueOf(cell.getNumericCellValue()) ));
					}
				 
				 my_table.addCell(table_cell);
			}
		}

		try {
//			workbook.write(bos);
			  //Finally add the table to PDF document
            iText_xls_2_pdf.add(my_table);      
            iText_xls_2_pdf.close();                
            //we created our pdf file..
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();

		} finally {
			try {
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println( bos.size() );
		return bos.toByteArray();
	}
	
	private static void setCell(Cell cell, Object field) {
		if (field instanceof String) {
			cell.setCellValue((String) field);
		} else if (field instanceof Integer) {
			cell.setCellValue((Integer) field);
		}else if(field instanceof BigDecimal){
			cell.setCellValue(Double.parseDouble(field.toString()));
			cell.setCellType(CellType.NUMERIC);
		}else if(field instanceof Date){
			SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy", new Locale( "th" , "TH" ));			
			cell.setCellValue(dt1.format(field));
		}
	}
	
}
