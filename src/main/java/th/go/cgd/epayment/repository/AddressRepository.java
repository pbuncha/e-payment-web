package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import th.go.cgd.epayment.entity.Address;

public interface AddressRepository extends JpaRepository<Address, Integer>
{
	public Address findByMoo(@Param("moo") String moo);
}
