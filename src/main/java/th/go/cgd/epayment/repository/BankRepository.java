package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.Bank;


/**
 * @author torgan.p 25 พ.ย. 2560 23:52:00
 *
 */
public interface BankRepository extends JpaRepository<Bank, Integer> {

}
