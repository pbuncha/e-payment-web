package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.BatchFileReadResult;

/**
 * @author torgan.p 16 ม.ค. 2561 20:11:05
 *
 */
public interface BatchFileReadResultRepository extends JpaRepository<BatchFileReadResult, Integer> {

    
	
}
