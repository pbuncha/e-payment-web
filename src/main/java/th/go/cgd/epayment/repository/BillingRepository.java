package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.Billing;
import th.go.cgd.epayment.entity.Invoice;
@Repository
public interface BillingRepository extends JpaRepository<Billing, Integer> {
    
    @Query("SELECT b FROM Billing b WHERE b.invoiceId.userId.userId = :userId")
    public List<Billing> getBillingByInvoidUser(@Param("userId") Integer userId);

}
