package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.CatalogCostCenter;

@Repository
public interface CatalogCostCenterRepository  extends JpaRepository<CatalogCostCenter, Integer> {
	 // @Query("SELECT m FROM CatalogCostCenter m WHERE m.catalogId.catalogId.catalogId = :catalogId")
	   // public List<CatalogCostCenter> findCostCenterByCatId(@Param("catalogId") String catalogId);
	
}
