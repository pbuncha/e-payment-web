package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.Catalog;
import th.go.cgd.epayment.entity.Example;
import th.go.cgd.epayment.entity.MCatalogType;

@Repository
public interface CatalogRepository extends JpaRepository<Catalog, Integer> {
	@Query("SELECT m FROM Example m WHERE m.fname = :fname")
	public Example findExampleByName(@Param("fname") String fname);

	@Query("SELECT c FROM MCatalogType c WHERE c.catalogTypeId = :id")
	public MCatalogType getCatalogByCatType(@Param("id") Integer id);

	@Query("SELECT c FROM Catalog c WHERE c.catalogCode = :catalogCode AND c.version = :version")
	public List<Catalog> getCatalogAllByStatus(@Param("catalogCode") String catalogCode,
			@Param("version") String version);

	@Query("SELECT c FROM Catalog c WHERE c.status = :status AND c.catalogTypeId.id = :id")
	public List<Catalog> getCatalogBy(@Param("status") Character status, @Param("id") Integer id);

	@Query(value="SELECT c FROM Catalog c WHERE c.catalogCode = :catalogCode AND c.status = '1' ORDER BY c.version DESC LIMIT 1", nativeQuery = true)
	public Catalog findLatestVersoinByCode(@Param("catalogCode") String catalogCode);

}
