package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.CatalogStructure;
import th.go.cgd.epayment.entity.Example;

@Repository
public interface CatalogStructureRepository  extends JpaRepository<CatalogStructure, Integer> {
	  @Query("SELECT m FROM CatalogStructure m WHERE m.catalogId.catalogId = :catalogId")
	    public CatalogStructure findCatalogStructureByCatId(@Param("catalogId") Integer catalogId);
	
}
