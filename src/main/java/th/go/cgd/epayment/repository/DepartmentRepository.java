package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MDepartment;

/**
 * @author torgan.p 18 พ.ย. 2560 22:57:14
 *
 */
@Repository
public interface DepartmentRepository extends JpaRepository<MDepartment, Integer> {

//	@Query("SELECT m FROM MDepartment m WHERE m.status = :status")
//	public List<MDepartment> findByStatus(@Param("status") String status);
}
