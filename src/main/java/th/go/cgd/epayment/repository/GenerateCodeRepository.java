package th.go.cgd.epayment.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.GenerateCode;
@Repository
public interface GenerateCodeRepository  extends JpaRepository<GenerateCode, Integer> {
	
	@Query( "SELECT g FROM GenerateCode g WHERE g.generateKey = :generateKey and g.years = :years")
	public List<GenerateCode>  findByGenerateKey(@Param("generateKey") String generateKey, @Param("years") String years);
}
