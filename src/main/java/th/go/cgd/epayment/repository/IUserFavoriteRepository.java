package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.UserFavorite;
@Repository
public interface IUserFavoriteRepository extends JpaRepository<UserFavorite, Integer> {
	
	@Query("SELECT u FROM UserFavorite u WHERE u.userId.userId = :userId")
	public List<UserFavorite> getUserFavoriteByUser(@Param("userId") Integer userId);
	@Modifying
	@Query("DELETE FROM UserFavorite u WHERE u.userId.userId = :userId")
	public void deleteAllFavorite(@Param("userId") Integer userId);

}
