package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.InvoiceItem;import th.go.cgd.epayment.model.InvoiceItemBean;

@Repository
public interface InvoiceItemRepository extends JpaRepository<InvoiceItem, Integer> {
  
  @Query("SELECT i FROM InvoiceItem i WHERE i.invoiceId.userId = :userId")
  public List<InvoiceItem> getInvoiceItemByUser(@Param("userId") Integer userId);

}
