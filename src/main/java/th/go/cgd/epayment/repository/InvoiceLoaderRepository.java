package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.InvoiceItem;
import th.go.cgd.epayment.entity.InvoiceLoader;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.InvoiceItemBean;

@Repository
public interface InvoiceLoaderRepository extends JpaRepository<InvoiceLoader, Integer> {
 
	@Query("SELECT i FROM InvoiceLoader i WHERE i.invoiceLoaderCode = :invoiceLoaderCode AND i.groupBy = :groupBy")
	public List<InvoiceLoader> findBy(@Param("invoiceLoaderCode") String invoiceLoaderCode,@Param("groupBy") String groupBy);

}
