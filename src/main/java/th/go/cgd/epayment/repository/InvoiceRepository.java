/**
 * 
 */
package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.InvoiceItem;

/**
 * @author wichuda.k Nov 12, 2017
 *
 */
@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Integer> {



  @Query("SELECT i FROM Invoice i WHERE i.userId.userId = :userId")
  public List<Invoice> getInvoidListByUser(@Param("userId") Integer userId);
  
  @Query("SELECT i FROM InvoiceItem i")
  public List<InvoiceItem> getInvoiceItem();

}
