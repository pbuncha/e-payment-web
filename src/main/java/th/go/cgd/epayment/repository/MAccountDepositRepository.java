package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MAccountDeposit;

@Repository
public interface MAccountDepositRepository 
extends JpaRepository<MAccountDeposit, Integer> {

	@Query("SELECT m FROM MAccountDeposit m WHERE m.accountCode = :accountDepositCode and m.owner like :deptCode%")
	public MAccountDeposit findMAccountDepositByCode(@Param("accountDepositCode") String accountDepositCode,@Param("deptCode") String deptCode);
}
