
package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MAmphur;


@Repository
public interface MAmphurRepository extends JpaRepository<MAmphur, Integer> {
	@Query("SELECT m FROM MAmphur m WHERE m.provinceId.provinceId = :provinceId")
	List<MAmphur> getAmphurByProvince(@Param("provinceId") Integer provinceId);
	
}
