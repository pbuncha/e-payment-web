package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.Example;
import th.go.cgd.epayment.entity.MBusinessArea;

@Repository
public interface MBusinessAreaRepository 
extends JpaRepository<MBusinessArea, Integer> 
{

}
