package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.Example;
import th.go.cgd.epayment.entity.MCatalogType;

@Repository
public interface MCatalogTypeRepository  extends JpaRepository<MCatalogType, Integer> {


}
