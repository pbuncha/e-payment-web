package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import th.go.cgd.epayment.entity.MCostCenter;

@Repository
public interface MCostCenterRepository extends JpaRepository<MCostCenter, Integer> {
	 
	@Query("SELECT m FROM MCostCenter m WHERE m.costCenterCode = :costCenterCode")
	public MCostCenter findMCostCenterByCode(@Param("costCenterCode") String costCenterCode);
	
	@Query("SELECT m FROM MCostCenter m WHERE m.disbursementUnitId.disbursementUnitId = :disbursementUnitId")
	public List<MCostCenter> getCostCenterByDisbursementUnitId(@Param("disbursementUnitId") Integer disbursementUnitId);
}
