package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MDepartment;

@Repository
public interface MDepartmentRepository extends JpaRepository<MDepartment, Integer> {

	@Query("SELECT m FROM MDepartment m WHERE m.departmentCode = :departmentCode")
	public MDepartment findMDepartmentByCode(@Param("departmentCode") String departmentCode);
}
