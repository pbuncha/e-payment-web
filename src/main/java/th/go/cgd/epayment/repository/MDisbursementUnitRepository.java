package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MDisbursementUnit;

@Repository
public interface MDisbursementUnitRepository 
extends JpaRepository<MDisbursementUnit, Integer> 
{
	@Query( nativeQuery=true, value="SELECT * FROM  PAYDB.M_DISBURSEMENT_UNIT d WHERE d.DISBURSEMENT_UNIT_ID IN (SELECT c.DISBURSEMENT_UNIT_ID FROM PAYDB.M_COST_CENTER c WHERE c.DEPARTMENT_ID = ?1)")
	public List<MDisbursementUnit> getDisbursementUnitAllByDepartmentId(@Param("departmentId") Integer departmentId);

}
