package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MGl;

@Repository
public interface MGlRepository 
extends JpaRepository<MGl, Integer> {
	
	@Query("SELECT m FROM MGl m WHERE m.glCode = :glCode")
	public MGl findMGlByCode(@Param("glCode") String glCode);

}
