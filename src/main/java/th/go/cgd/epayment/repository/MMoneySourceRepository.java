package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MMoneySource;

@Repository
public interface MMoneySourceRepository 
extends JpaRepository<MMoneySource, Integer> {
	
	@Query("SELECT a.standardRevernueId.moneySourceId FROM MStandardRevernueGl a WHERE (a.glStartId >= :glCode and a.glFinishId <= :glCode) "
//			+ "AND c.moneySourceId.moneySourceId = b.moneySourceId "
//			+ "AND b.standardRevernueId.standardRevernueId = a.standardRevernueId"
			)
	public MMoneySource findMMoneySourceByGlCode(@Param("glCode") Long glCode);

}
