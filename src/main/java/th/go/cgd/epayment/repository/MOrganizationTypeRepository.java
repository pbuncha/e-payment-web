package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MOrganizationType;

@Repository
public interface MOrganizationTypeRepository 
extends JpaRepository<MOrganizationType, Integer> 
{

}
