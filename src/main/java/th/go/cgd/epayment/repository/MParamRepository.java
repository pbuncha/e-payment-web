package th.go.cgd.epayment.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.MParam;

public interface MParamRepository extends JpaRepository<MParam, Short> {
	
	Page<MParam> findByNameContaining(String name,Pageable pageable);
	
	MParam findByName(String name);

}
