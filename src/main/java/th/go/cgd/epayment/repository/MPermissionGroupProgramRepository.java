package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import th.go.cgd.epayment.entity.MPermissiongroupProgram;

import java.util.List;

public interface MPermissionGroupProgramRepository extends JpaRepository<MPermissiongroupProgram,Integer>{

    @Query("SELECT m FROM MPermissiongroupProgram m WHERE m.mPermissionGroupId = :id ORDER BY m.programId ASC")
    public List<MPermissiongroupProgram> findByPermissionGroupIdAsc(@Param("id") int id);

    @Query("SELECT m FROM MPermissiongroupProgram m WHERE m.mPermissionGroupId = :id AND m.status.status = '1' ORDER BY m.programId ASC")
    public List<MPermissiongroupProgram> findByPermissionGroupIdActiveAsc(@Param("id") int id);

    @Query("SELECT m.programId FROM MPermissiongroupProgram m WHERE m.mPermissionGroupId = :id AND m.status.status = '1' ORDER BY m.programId ASC")
    public List<Integer> findByPermissionGroupIdActiveAscAsInt(@Param("id") int id);


    @Query("SELECT m FROM MPermissiongroupProgram m WHERE m.mPermissionGroupId in :id AND m.programId not in :list")
    public List<MPermissiongroupProgram> findByPermissionGroupIdProgramNotIn(@Param("id") int id ,@Param("list") List<Integer> list);

    @Modifying
    @Query("DELETE FROM MPermissiongroupProgram WHERE mPermissionGroupId = :permissionGroupId)")
    public void deleteByPermissionGroupId(@Param("permissionGroupId") int permissionGroupId );

    
}
