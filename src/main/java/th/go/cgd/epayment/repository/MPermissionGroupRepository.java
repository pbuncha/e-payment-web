package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import th.go.cgd.epayment.entity.MPermissionGroup;

import java.util.List;

public interface MPermissionGroupRepository extends JpaRepository<MPermissionGroup,Integer>{


    @Query("SELECT COUNT(*) FROM MUserPermissiongroup m1 WHERE m1.mPermissionGroupId.mPermissionGroupId = :id")
    public int checkPermissionGroupCanDelete(@Param("id") int id);

    @Query("SELECT t FROM MPermissionGroup t WHERE t.mPermissionGroupName = :name")
    public MPermissionGroup findByPermissionGroupName(@Param("name") String name);

    @Query("SELECT t FROM MPermissionGroup t WHERE t.mPermissionGroupId in :list")
    public List<MPermissionGroup> findByPermissionGroupIdInList(@Param("list") List<Integer> list);

    @Query("SELECT t FROM MPermissionGroup t WHERE t.mPermissionGroupName = :name AND not (t.mPermissionGroupId = :id)")
    public MPermissionGroup findByPermissionGroupNameAndPermissionGroupIdNot(@Param("name") String name,@Param("id") int id);

    @Query("SELECT m FROM MPermissionGroup m, IN (m.mUserPermissiongroupList) u WHERE u.userId.userId = :userId")
    public List<MPermissionGroup> findByUserId(@Param("userId") int userId);
    
}
