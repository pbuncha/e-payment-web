/**
 * 
 */
package th.go.cgd.epayment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MPersonType;


/**
 * @author newfear
 *
 */
@Repository
public interface MPersonTypeRepository extends CrudRepository<MPersonType, Integer>{

}
