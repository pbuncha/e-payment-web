package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import th.go.cgd.epayment.entity.MProgram;

import java.util.List;

public interface MProgramRepository extends JpaRepository<MProgram, Integer> {

    @Query("SELECT m FROM MProgram m ORDER BY m.programId ASC")
    public List<MProgram> findAllByOrderByProgramIdAsc();

    @Query("SELECT COUNT(m) FROM MPermissiongroupProgram m WHERE m.programId = :id")
    public int checkProgramCanDelete(@Param("id") int id);

    @Query("SELECT m FROM MProgram m WHERE m.programName = :name")
    public MProgram findByProgramName(@Param("name") String name);

    @Query("SELECT m FROM MProgram m WHERE m.programName = :name AND not (programId = :id)")
    public MProgram findByProgramNameAndProgramIdNot(@Param("name") String name,@Param("id") int id);

}
