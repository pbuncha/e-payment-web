package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.Example;
import th.go.cgd.epayment.entity.MRevenueType;

@Repository
public interface MRevenueTypeRepository  extends JpaRepository<MRevenueType, Integer> {
	  @Query("SELECT m FROM Example m WHERE m.fname = :fname")
	  public Example findExampleByName(@Param("fname") String fname);
	  
}
