package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MStandardRevernueGl;

@Repository
public interface MStandardRevernueGlRepository 
extends JpaRepository<MStandardRevernueGl, Integer> {
	
	@Query("SELECT m FROM MStandardRevernueGl m WHERE (m.glStartId >= :glCode and m.glFinishId <= :glCode)")
	public MStandardRevernueGl findMStandardRevernueGlByGlCode(@Param("glCode") Long glCode);

}
