package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MStandardRevernue;

@Repository
public interface MStandardRevernueRepository 
extends JpaRepository<MStandardRevernue, Integer> 
{
	@Modifying
	@Query("UPDATE MStandardRevernue u set status = '3' WHERE u.standardRevernueId = :id")
	public void updateStatusCancel(@Param("id") Integer id);

}
