package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import th.go.cgd.epayment.entity.Billing;
import th.go.cgd.epayment.entity.MStatus;

public interface MStatusRepository extends JpaRepository<MStatus, String> {
    
    @Query("SELECT s FROM MStatus s WHERE s.status IN :status")
    public List<MStatus> getMStatusById(@Param("status") String [] status);
	

}
