package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MTambon;

@Repository
public interface MTambonRepository extends JpaRepository<MTambon, Integer> {
	@Query("SELECT m FROM MTambon m WHERE m.amphurId.amphurId = :amphurId")
	List<MTambon> getTambonByAmphur(@Param("amphurId") Integer amphurId);

}
