package th.go.cgd.epayment.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.MTitleName;


/**
 * @author torgan.p 15 พ.ย. 2560 20:51:24
 *
 */
@Repository
public interface MTitleNameRepository  extends CrudRepository<MTitleName, Integer> {

}
