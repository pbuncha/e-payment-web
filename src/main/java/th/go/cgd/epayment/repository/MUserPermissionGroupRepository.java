package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import th.go.cgd.epayment.entity.MUserPermissiongroup;

import java.util.List;

public interface MUserPermissionGroupRepository extends JpaRepository<MUserPermissiongroup,Integer> {

    @Query("SELECT m FROM MUserPermissiongroup m WHERE m.userId.userId = :id")
    public List<MUserPermissiongroup> findByUserIdAsc(@Param("id") int id);

    @Query("SELECT m.mPermissionGroupId.mPermissionGroupId FROM MUserPermissiongroup m WHERE m.userId.userId = :id")
    public List<Integer> findByUserIdAscAsInt(@Param("id") int id);
}