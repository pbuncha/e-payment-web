package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.MUserRole;

public interface MUserRoleRepository extends JpaRepository<MUserRole, Short> {

}
