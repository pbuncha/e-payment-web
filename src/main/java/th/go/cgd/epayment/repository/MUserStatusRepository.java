package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.MUserStatus;

public interface MUserStatusRepository extends JpaRepository<MUserStatus, String> {

}
