package th.go.cgd.epayment.repository;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MasterRepository
{
	@Autowired
    private SessionFactory sessionFactory;
    
    @SuppressWarnings("unchecked")
    public <T> List<T> findAll(Class<T> clazz, String columnOrder)
	{
    	String columnOrder_ = columnOrder;
    	if(null==columnOrder)
    	{
    		columnOrder_ = "id";
    	}
    	return (List<T>)(sessionFactory.getCurrentSession().createQuery("from " + clazz.getName()+ " c order by c."+columnOrder_+" asc")).list();
	}
    
    @SuppressWarnings("unchecked")
    public <T> List<T> findAllStatusActive(Class<T> clazz, String columnOrder)
    {
    	String columnOrder_ = columnOrder;
    	if(null==columnOrder)
    	{
    		columnOrder_ = "id";
    	}
    	return (List<T>)(sessionFactory.getCurrentSession().createQuery("from " + clazz.getName()+ " c where status = 'A' order by c."+columnOrder_+" asc")).list();
    }
    
    public <T> T save(T o)
    {
    	sessionFactory.getCurrentSession().saveOrUpdate(o);
    	return o;
    }
    
    public <T> boolean delete(Class<T> clazz, Object id)
    {
    	Query query = sessionFactory.getCurrentSession().createQuery("delete from "+ clazz.getName() + " c where c.id = :id");
    	if(id instanceof Short)
		{
			query.setShort("id", (Short)id);
		}
		else if(id instanceof Integer)
		{
			query.setInteger("id", (Integer)id);
		}
		else if(id instanceof Long)
		{
			query.setLong("id", (Long)id);
		}
		else if(id instanceof String)
		{
			query.setString("id", (String)id);
		}
		return (1==query.executeUpdate());
    }
}