package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.Menu;

public interface MenuRepository extends JpaRepository<Menu, Integer> 
{
	
}
