package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import th.go.cgd.epayment.entity.MStatus;
import th.go.cgd.epayment.entity.Organization;

public interface OrganizationRepository extends JpaRepository<Organization, Integer> {
	
//	Page<Organization> findByOrgNameContaining(String orgName,Pageable pageable);
//	Page<Organization> findByMstatusAndOrgName(MStatus mstatus, String orgName, Pageable pageable);
//	Page<Organization> findByMstatusAndOrgNameContaining(MStatus mstatus, String orgName, Pageable pageable);
//	Page<Organization> findByMstatusAndOrgNameAndOrgIdNot(MStatus mstatus, String orgName, int orgId,Pageable pageable);
//	Page<Organization> findByMstatusAndOrgNameContainingAndOrgIdNot(MStatus mstatus, String orgName, int orgId,Pageable pageable);
//	
//	@Query(nativeQuery=true)
//	Long findByOrgNameCountOrmXml(Integer userId, String orgName);
//	@Query(nativeQuery=true)
//	List<Object[]> findByOrgNameOrmXml(Integer userId, String orgName,Pageable pageable);
//	@Query(nativeQuery=true)
//	Long findByOrgNameCountNotInOrgOrmXml(Integer userId, String orgName, int orgId);
//	@Query(nativeQuery=true)
//	List<Object[]> findByOrgNameNotInOrgOrmXml(Integer userId, String orgName, int orgId,Pageable pageable);
//	
//	
//	Organization findByOrgName(String orgName);
//	Organization findByOrgNameAndOrgIdNot(String orgName, int orgId);
//	
//	Page<Organization> findByMstatusAndOrgNameAndOrganizationIsNull(MStatus mstatus, String orgName, Pageable pageable);
//	Page<Organization> findByMstatusAndOrgNameContainingAndOrganizationIsNull(MStatus mstatus, String orgName, Pageable pageable);
//	
//	@Query(nativeQuery=true)
//	Long findByOrgNameCountParentOnlyOrmXml(Integer userId, String orgName);
//	@Query(nativeQuery=true)
//	List<Object[]> findByOrgNameParentOnlyOrmXml(Integer userId, String orgName,Pageable pageable);
//	
//	List<Organization> findByMstatusAndOrgNameAndOrganizationIsNull(MStatus mstatus, String orgName);
//	
//	@Query(nativeQuery=true)
//	List<Integer> findByOrgAccessType3OrmXml(Integer userId);
//	
//	List<Organization> findByMstatusAndOrganizationIsNull(MStatus mstatus);
}