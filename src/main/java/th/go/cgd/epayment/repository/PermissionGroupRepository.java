package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import th.go.cgd.epayment.entity.PermissionGroup;

public interface PermissionGroupRepository extends JpaRepository<PermissionGroup, Short> {
	
	@Query(nativeQuery=true, value="SELECT COUNT(1) FROM USER_GROUP_PERMISSION WHERE GROUP_PERMISSION_ID = ?1")
	public int checkPermissionGroupCanDelete(short id);
	
	public PermissionGroup findByPermissionGroupName(String permissionGroupName);
	public PermissionGroup findByPermissionGroupNameAndPermissionGroupIdNot(String permissionGroupName, short permissionGroupId);
}
