package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.PermissionItem;

public interface PermissionItemRepository extends JpaRepository<PermissionItem, Short> {

}
