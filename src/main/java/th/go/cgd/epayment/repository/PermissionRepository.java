package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.Permission;

public interface PermissionRepository extends JpaRepository<Permission, Short> {
	
	public List<Permission> findAllByOrderByPermissionIdAsc();
}
