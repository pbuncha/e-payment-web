package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import th.go.cgd.epayment.entity.MProvince;

public interface ProvinceFilterRepository extends JpaRepository<MProvince, Short> {
//	@Query(nativeQuery=true)
//	List<Object[]> findDistrictByProvinceId(short provinceId);
//	
//	@Query(nativeQuery=true)
//	List<Object[]> findSubDistrictByDistrictId(short districtId);
}