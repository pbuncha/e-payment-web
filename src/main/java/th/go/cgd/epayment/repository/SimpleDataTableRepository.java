package th.go.cgd.epayment.repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.ScrollableResults;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.helper.QueryHelper;
import th.go.cgd.epayment.model.DataTable;
import th.go.cgd.epayment.model.DataTableSortable;
import th.go.cgd.epayment.util.SqlUtil;
import th.go.cgd.epayment.util.datatable.AnnotationResultTransformer;

@Repository
@Transactional
public class SimpleDataTableRepository{
	
	@Autowired
	SessionFactory sessionFactory;
	
	@Autowired
	QueryHelper queryHelper;
	
	public String getNamedQuery(String sql) {
		return queryHelper.getNamedQuery(sql);
	}
	
	public <T> List<T> getList(String sql, Map<String, Object> params, Class<T> clazz) {
		SQLQuery stmt = sessionFactory.getCurrentSession().createSQLQuery(sql);
		for (String key : params.keySet()) {
			Object paramValue = params.get(key);
			if (paramValue instanceof Object[]) {
				stmt.setParameterList(key, (Object[])paramValue);
			} else if (paramValue instanceof Collection) {
				stmt.setParameterList(key, (Collection<?>) paramValue);
			} else {
				stmt.setParameter(key, paramValue);
			}
		}
		stmt.setResultTransformer(new AnnotationResultTransformer(clazz));
		@SuppressWarnings("unchecked")
		List<T> list = stmt.list();
		return list;
	}
	
	public <T> DataTable<T> getPagingData(String sql, DataTableSortable paging, Map<String, Object> params, Class<T> clazz) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(sql);
		return getPagingData(stringBuilder, paging, params, clazz);
	}
	
	public <T> DataTable<T> getPagingData(StringBuilder sqlBuilder, DataTableSortable paging, Map<String, Object> params, Class<T> clazz) {
		paging.setOrderByCause(sqlBuilder, clazz);
		String sql = sqlBuilder.toString();
		return getPagingData(sql, params, paging.skip, paging.length, clazz);
	}
	
	public <T> DataTable<T> getPagingData(String sql, Map<String, Object> params, int skip, int length, Class<T> clazz) {
		SQLQuery stmt = sessionFactory.getCurrentSession().createSQLQuery(sql);
		for (String key : params.keySet()) {
			Object paramValue = params.get(key);
			if (paramValue instanceof Object[]) {
				stmt.setParameterList(key, (Object[])paramValue);
			} else if (paramValue instanceof Collection) {
				stmt.setParameterList(key, (Collection<?>) paramValue);
			} else {
				stmt.setParameter(key, paramValue);
			}
		}
		ScrollableResults scroll = null;
		try {
			scroll = stmt.scroll();
//			String[] columnNamesFromSql = SqlUtil.getColumnNamesFromSql(sql);
			String[] columnNamesFromSql = SqlUtil.getColumnNamesResultSetFromHackishScrollAbleResult(scroll);
			return getPagingByCursor(scroll, columnNamesFromSql, skip, length, clazz);
		} finally {
			if (scroll != null) {
				scroll.close();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	<T> DataTable<T> getPagingByCursor(ScrollableResults cursor, String[] colNames, int skip, int length, Class<T> clazz) {
		cursor.last();
		int total = cursor.getRowNumber() + 1;
		DataTable<T> dataTable = new DataTable<T>();
		dataTable.setRecordsTotal(total);
		dataTable.setRecordsFiltered(total);
		
		if (!cursor.first()) {
			dataTable.setData(Collections.<T>emptyList());
			return dataTable;
		}
		
		
		if (cursor.scroll(skip)) {
			List<T> data = new ArrayList<T>();
			AnnotationResultTransformer art = new AnnotationResultTransformer(clazz);
			for (int i = 0; i < length; i++) {
				data.add((T) art.transformTuple(cursor.get(), colNames));
				if(!cursor.next()) {
					break;
				}
			}
			dataTable.setData(art.transformList(data));
		} else {
			dataTable.setData((List<T>)Collections.emptyList());
		}
		return dataTable;
	}
}
