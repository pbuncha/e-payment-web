package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.UserCenter;

@Repository
public interface UserCenterRepository extends JpaRepository<UserCenter, Integer>
{
	@Query("SELECT u FROM UserCenter u WHERE u.citizenNo = :citizenNo")
	public List<UserCenter> findByCitizenNo(@Param("citizenNo") String citizenNo);
	
}
