package th.go.cgd.epayment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.Organization;
import th.go.cgd.epayment.entity.UserInfo;

public interface UserInfoRepository extends JpaRepository<UserInfo, Integer> {
	
//	public List<UserInfo> findByOrganizationAndUserInfoIsNull(Organization organization);
//	
//	public List<UserInfo> findByUserInfo(UserInfo userInfo);
//	
//	public UserInfo findByEmailAndCitizenNo(String email, String citizenNo);
	
}
