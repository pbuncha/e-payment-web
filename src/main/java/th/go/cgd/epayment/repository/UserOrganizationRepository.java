package th.go.cgd.epayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import th.go.cgd.epayment.entity.UserOrganization;


/**
 * @author torgan.p 26 ธ.ค. 2560 15:19:41
 *
 */
public interface UserOrganizationRepository extends JpaRepository<UserOrganization, Integer> {

}
