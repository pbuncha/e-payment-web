package th.go.cgd.epayment.repository;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import th.go.cgd.epayment.entity.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>
{
	@Query("SELECT u FROM User u WHERE u.userName = :userName AND u.status = '1'")
	public List<User> findByUserName(@Param("userName") String userName);

	@Query("SELECT u FROM User u WHERE u.userCenterId.userCenterId = :id")
	public List<User> findByUserCenterId(@Param("id") int id);

	@Query(value = "SELECT u FROM User u LIMIT 1", nativeQuery = true)
	public User getMockupCreateUser();
	
	@Query("SELECT u FROM User u WHERE u.userName = :userName AND u.status = '0'")
	public List<User> findByUserNameByStutas(@Param("userName") String userName);


	
//	@Query("select u from User u left join fetch u.menus m where m.status = 'A'")
//	public List<User> findAll();
	
//	List<User> findByUserNameContaining(String userName,Pageable pageable);
//	
//	@Query(nativeQuery=true)
//	Page<Object[]> findByUserNameOrmXml(String fullName,Pageable pageable);
//	
//	@Query(nativeQuery=true)
//	Slice<Object[]> findByUserFullNameAndRoleIdAndStatusInOrmXml(String fullName, short roleId, List<String> statuses, Pageable pageable);
//
//	Page<User> findByUserRoles_RoleId(short roleId, Pageable pageable);
//	
//	public User findByUserNameAndUserIdNot(String userName, int userId);
//	
//	@Query(nativeQuery=true)
//	public List<Object[]> findEmailParentTCLD(int civilId);
//	
//	@Query(nativeQuery=true)
//	public List<Integer> allUserExceptParentTCLDWithAccessPermissionType1(int civilId, int userId, int userCreated);
//	
//	@Query(nativeQuery=true)
//	public List<Integer> allUserExceptParentTCLDWithAccessPermissionType2(int civilId, int userCreated);
//	
//	@Query(nativeQuery=true)
//	public List<Integer> allUserExceptParentTCLDWithAccessPermissionType3(int civilId, int userId, int userCreated);
//	
//	@Query(nativeQuery=true)
//	public List<Integer> allAdminTCLDWithAccessPermissionType1(int userId);
//	
//	@Query(nativeQuery=true)
//	public List<Integer> allAdminTCLDWithAccessPermissionType2();
//	
//	@Query(nativeQuery=true)
//	public List<Integer> allAdminTCLDWithAccessPermissionType3(int userId);
//	
//	@Query(nativeQuery=true)
//	Slice<Object[]> findByUserFullNameAndOrganizationOfCivilInOrmXml(String fullName, short roleId, int orgId,int orgIds, int civilId, Pageable pageable);
//	
//	@Query(nativeQuery=true)
//	Slice<Object[]> findByUserFullNameAndOrganizationOfViolationInOrmXml(String fullName, short roleId, int orgId,int orgIds, int violationId, Pageable pageable);
//	
//	@Query(nativeQuery=true)
//	Slice<Object[]> findByUserFullNameAndOrganizationOfEducationInOrmXml(String fullName, short roleId, int orgId,int orgIds, int educationId, Pageable pageable);
//	
//	@Query(nativeQuery=true)
//	Slice<Object[]> findByUserFullNameAndOrganizationOfDebtInOrmXml(String fullName, short roleId, int orgId,int orgIds, int educationId, Pageable pageable);
//	
//	@Query(nativeQuery=true)
//	public List<Integer> findByOrganizationAndPermissionItem(int orgId, int permissionItemId);
//	
//	@Query(nativeQuery=true)
//	public List<Integer> findByUserAndPermissionItem(int userId, int permissionItemId);

//	List<User> findByUserRoles_RoleId(Short i );
	
}
