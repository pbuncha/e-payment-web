package th.go.cgd.epayment.security;


import java.io.IOException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import th.go.cgd.epayment.entity.LogAccess;
import th.go.cgd.epayment.entity.MLogAction;
import th.go.cgd.epayment.util.ClientMachineUtil;
import th.go.cgd.epayment.util.IPAddressUtil;


public class AuthenFailureHandler extends SimpleUrlAuthenticationFailureHandler implements AuthenticationFailureHandler {

    private static final long serialVersionUID = 1L;

    private static final Logger logger = Logger.getLogger(AuthenFailureHandler.class);

    private static final String LOG_IN = "IN";

    private static final String LOGIN = "/login";
    private static final String LOGIN_G = "/login_goverment";

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException ex) throws IOException, ServletException {
        String username = request.getParameter("username");
        String status = "";
        System.out.println("In onAuthenticationFailure");
        System.out.println(ex);
//        Boolean bFromLoginG =  request.getHeader("Referer").contains("login_goverment");
//        System.out.println(bFromLoginG);
        String redirectPage = LOGIN;
        if(ex instanceof BadCredentialsException) { 
            redirectStrategy.sendRedirect(request, response, redirectPage + "?error=2");
            status = "BAD_CREDENTIAL";
        } else if(ex instanceof UsernameNotFoundException) {
        redirectStrategy.sendRedirect(request, response, redirectPage + "?error=1");
            //response.sendRedirect("login?disabled=1"); // USER_DISABLED
            status = "BAD_CREDENTIAL";
        } else { System.out.println("In OTHER_EXCEPTION");
            redirectStrategy.sendRedirect(request, response, redirectPage + "?disabled=1");
            //response.sendRedirect("login?error=1"); // OTHER_EXCEPTION
            status = "OTHER_EXCEPTION";
        }

        String computerName = ClientMachineUtil.getClientMachineName(request);
        MLogAction mLogAction = new MLogAction();
        mLogAction.setActionId(LOG_IN);
        LogAccess logAccess = new LogAccess();
        logAccess.setAccessDate(Calendar.getInstance().getTime());
        logAccess.setExecutionTime((short) 0);
        logAccess.setClientIp(IPAddressUtil.getClientAddress(request));
        logAccess.setUrl(request.getRequestURL().toString());
        logAccess.setClientName(computerName);
        logAccess.setStatus(status);

    }
}
