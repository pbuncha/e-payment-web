package th.go.cgd.epayment.security;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AuthenticationLogoutHandler extends SimpleUrlLogoutSuccessHandler {
	// Just for setting the default target URL
	   //public AuthenticationLogoutHandler(String defaultTargetURL) {
	   public AuthenticationLogoutHandler(){
	        this.setDefaultTargetUrl("/login");
	   }

	   @Override
	   public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

	        // do whatever you want
	        super.onLogoutSuccess(request, response, authentication);
	   }

}
