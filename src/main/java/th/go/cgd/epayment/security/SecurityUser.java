package th.go.cgd.epayment.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import th.go.cgd.epayment.entity.User;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SecurityUser extends User implements UserDetails{
    private static final long serialVersionUID = 1L;
    public SecurityUser(User user) {
        System.out.println("in SecurityUser");

        if(user != null){
            List<String> roleUser = new ArrayList<String>();
            roleUser.add("ADMIN");


//				user.setUserRole(roleUser);
//				user.setAuthorities(listRole);
//

            this.setUserId(user.getUserId());
            this.setUserName(user.getUserName());
            this.setPassword(user.getPassword());
            this.setStatus(user.getStatus());
            this.setLastLogin(user.getLastLogin());
            System.out.println(user.getPassword());
            //System.out.println(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
//				this.setUserRoles(listRole);
//				this.setMUserStatus(user.getMUserStatus());


        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        Collection<GrantedAuthority> authorities = new ArrayList<>();
//			List<Role> userRoles = this.getRoles();
//
//			if(userRoles != null)
//			{
//				for (Role role : userRoles) {
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ADMIN");
        authorities.add(authority);
//				}
//			}
        return authorities;
    }

    @Override
    public String getUsername() {
        return super.getUserName();
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        //if(super.getMUserStatus() == null || !super.getMUserStatus().getStatus().equals("A")){
        if(super.getStatus() != '1'){
            return false;
        }
        return true;
    }
}
