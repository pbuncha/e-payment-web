package th.go.cgd.epayment.security.filter;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExtraParamsUsernamePasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final Logger log = Logger.getLogger(this.getClass());

    private static final String LOGIN_SUCCESS_URL = "/";
    private static final String LOGIN_ERROR_URL = "/login?error";
    private List<String> parameters = new ArrayList<String>();
    private String delimiter = ":";
    private Map<String,String> values = new HashMap<>();

    public ExtraParamsUsernamePasswordAuthenticationFilter(){
        parameters.add("rdbGroup");
        parameters.add("rdIdentify");
    }



    @Override
    protected String obtainUsername(HttpServletRequest request) {
        String username = request.getParameter(getUsernameParameter());
        StringBuilder combinedUsername = new StringBuilder(username);
        List<String> list = getParameters();
        for (int i = 0 ; i < list.size(); i++){

            String temp = request.getParameter(list.get(i));

            log.debug("params : "+list.get(i)+" value : "+temp);

            values.put(list.get(i),temp);
            if(null == temp){
                temp = " ";
            }

            combinedUsername.append(getDelimiter()+temp);

        }

        log.debug("Combined username = " + combinedUsername.toString());
        return combinedUsername.toString();
    }



    public List<String> getParameters() {
        return parameters;
    }

    public void addParameters(String parameter) {
        this.parameters.add(parameter);
    }

    public String getDelimiter() {
        return delimiter;
    }

    public void setDelimiter(String delimiter) {
        this.delimiter = delimiter;
    }

    public String getValue(String parameter) {
        return values.get(parameter);
    }

    public void addValue(String paramenter,String value) {
        values.put(paramenter,value);
    }
}
