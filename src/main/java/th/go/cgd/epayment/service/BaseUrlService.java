package th.go.cgd.epayment.service;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.service.interfaces.IBaseUrlService;

/**
 * @author torgan.p 14 ต.ค. 2560 17:55:20
 *
 */
@Service
public class BaseUrlService implements IBaseUrlService{
	 private String baseUrl;

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl the baseUrl to set
	 */
	@Autowired
	public void setBaseUrl(ServletContext servletContext) {
		this.baseUrl =  servletContext.getContextPath();;
	}
	 
}
