package th.go.cgd.epayment.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.model.chart.ChartStatCaseDatasetModel;
import th.go.cgd.epayment.model.chart.ChartStatCaseModel;
import th.go.cgd.epayment.model.chart.ChartStatDamageCivilDatasetModel;
import th.go.cgd.epayment.model.chart.ChartStatDamageCivilModel;
import th.go.cgd.epayment.model.chart.ChartStatLoanEducationBreakDatasetModel;
import th.go.cgd.epayment.model.chart.ChartStatLoanEducationBreakModel;
import th.go.cgd.epayment.model.chart.ChartStatLoanEducationDatasetModel;
import th.go.cgd.epayment.model.chart.ChartStatLoanEducationModel;
import th.go.cgd.epayment.model.chart.ChartStatWorkDatasetModel;
import th.go.cgd.epayment.model.chart.ChartStatWorkModel;

@Service
@Transactional
public class ChartService {
	
	private static final String POINT_BORDER_COLOR = "rgba(220,220,220,1)";
	
	private static final String COLOR_WHITE = "#FFFFFF";
	private static final String COLOR1 = "#6892C5";
	private static final String COLOR2 = "#CC6961";
	private static final String COLOR3 = "#ADC26E";
	private static final String COLOR4 = "#967BB2";
	private static final String COLOR5 = "#58A0B5";
	private static final String COLOR6 = "#F2A662";
	private static final String COLOR7 = "#324669";
	private static final String COLOR8 = "#4E2728";
	
	public ChartStatDamageCivilModel getChartStatDamageCivilModel(UserSession userSession) {
		ChartStatDamageCivilModel chartStatDamageCivilModel = new ChartStatDamageCivilModel();
		chartStatDamageCivilModel.setLabels(new ArrayList<String>() {{
			 add("ทุจริต/เงินขาดบัญชี"); 
			 add("ไม่ปฏิบัติตามกฏหรือระเบียบ (การเงินการคลัง)"); 
			 add("ไม่ปฏิบัติตามกฏหรือระเบียบ (พัสดุ)");
			 add("ไม่ปฏิบัติตามกฏหรือระเบียบ (อื่นๆ)");
			 add("คนร้ายกระทำจรรกมหรือทรัพย์สินสูญหาย");
			 add("อาคารสถานที่ถูกเพลิงไหม้/ภัยธรรมชาติ");
			 add("อุบัติเหตุ");
			 add("เหตุอื่นๆ");
		}});
		
		List<ChartStatDamageCivilDatasetModel> chartStatDamageCivilDatasetModels = new ArrayList<ChartStatDamageCivilDatasetModel>();
		
		ChartStatDamageCivilDatasetModel dataset1 = new ChartStatDamageCivilDatasetModel();
		dataset1.setLabel("ค่าเสียหายตามสำนวน");
		dataset1.setBackgroundColor(COLOR1);
		dataset1.setData(new ArrayList<Integer>() {{
			add(65471000);
			add(82000000); 
			add(78000000); 
			add(23500000); 
			add(51300000); 
			add(17200000); 
			add(42000000); 
			add(91100000);
		}});
		
		ChartStatDamageCivilDatasetModel dataset2 = new ChartStatDamageCivilDatasetModel();
		dataset2.setLabel("ค่าเสียหายที่คณะกรรมการพิจารณาความรับผิดทางแพ่ง");
		dataset2.setBackgroundColor(COLOR2);
		dataset2.setData(new ArrayList<Integer>() {{
			add(65000000); 
			add(85000000); 
			add(85000000); 
			add(26000000); 
			add(45700000); 
			add(20400000);
			add(43200000); 
			add(89000000);
		}});
		
		ChartStatDamageCivilDatasetModel dataset3 = new ChartStatDamageCivilDatasetModel();
		dataset3.setLabel("ค่าเสียหายที่ออกคำสั่งเรียกให้ชดใช้");
		dataset3.setBackgroundColor(COLOR3);
		dataset3.setData(new ArrayList<Integer>() {{
			add(64000000); 
			add(84000000); 
			add(84000000);
			add(29000000);
			add(38000000); 
			add(27900000); 
			add(51100000); 
			add(85700000); 
		}});
		
		ChartStatDamageCivilDatasetModel dataset4 = new ChartStatDamageCivilDatasetModel();
		dataset4.setLabel("ค่าเสียหายตามคำพิพากษา (บาท)");
		dataset4.setBackgroundColor(COLOR4);
		dataset4.setData(new ArrayList<Integer>() {{
			add(66000000);
			add(90000000);
			add(80000000); 
			add(34000000); 
			add(32100000);
			add(21000000);
			add(48900000);
			add(97000000);
		}});
		
		
		chartStatDamageCivilDatasetModels.add(dataset1);
		chartStatDamageCivilDatasetModels.add(dataset2);
		chartStatDamageCivilDatasetModels.add(dataset3);
		chartStatDamageCivilDatasetModels.add(dataset4);
		
		chartStatDamageCivilModel.setDatasets(chartStatDamageCivilDatasetModels);
		
		return chartStatDamageCivilModel;
	}
	
	public ChartStatWorkModel getChartStatWorkModel(UserSession userSession) {
		
		ChartStatWorkModel chartStatWorkModel = new ChartStatWorkModel();
		chartStatWorkModel.setLabels(new ArrayList<String>() {{
			 add("ม.ค.59");
			 add("ก.พ.59");
			 add("มี.ค.59");
			 add("เม.ย.59");
			 add("พ.ค.59");
			 add("มิ.ย.59");
			 add("ก.ค.59");
			 add("ส.ค.59");
			 add("ก.ย.59");
			 add("ต.ค.59");
			 add("พ.ย.59");
			 add("ธ.ค.59");
		}});
		
		List<ChartStatWorkDatasetModel> chartStatWorkDatasetModels = new ArrayList<ChartStatWorkDatasetModel>();
		
		ChartStatWorkDatasetModel dataset1 = new ChartStatWorkDatasetModel();
		dataset1.setLabel("งานทางละเมิด");
		dataset1.setFill(false);
		dataset1.setLineTension(0.1);
        dataset1.setBackgroundColor(COLOR1);
        dataset1.setBorderColor(COLOR1);
        dataset1.setBorderCapStyle("butt");
        dataset1.setBorderDash(new ArrayList<String>());
        dataset1.setBorderDashOffset(0.0);
        dataset1.setBorderJoinStyle("miter");
        dataset1.setPointBorderColor(COLOR1);
        dataset1.setPointBackgroundColor(COLOR_WHITE);
        dataset1.setPointBorderWidth(1);
        dataset1.setPointHoverRadius(5);
        dataset1.setPointHoverBackgroundColor(COLOR1);
        dataset1.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset1.setPointHoverBorderWidth(2);;
       	dataset1.setPointRadius(5);
        dataset1.setPointHitRadius(10);
        dataset1.setData(new ArrayList<Integer>() {{
        	add(70);
        	add(70);
        	add(65);
        	add(59);
        	add(75);
        	add(44); 
        	add(79); 
        	add(88); 
        	add(78); 
        	add(48); 
        	add(77); 
        	add(54); 
        	add(43);
        }});
        dataset1.setSpanGaps(false);
        
        ChartStatWorkDatasetModel dataset2 = new ChartStatWorkDatasetModel();
		dataset2.setLabel("งานทางแพ่ง");
		dataset2.setFill(false);
		dataset2.setLineTension(0.1);
        dataset2.setBackgroundColor(COLOR2);
        dataset2.setBorderColor(COLOR2);
        dataset2.setBorderCapStyle("butt");
        dataset2.setBorderDash(new ArrayList<String>());
        dataset2.setBorderDashOffset(0.0);
        dataset2.setBorderJoinStyle("miter");
        dataset2.setPointBorderColor(COLOR2);
        dataset2.setPointBackgroundColor(COLOR_WHITE);
        dataset2.setPointBorderWidth(1);
        dataset2.setPointHoverRadius(5);
        dataset2.setPointHoverBackgroundColor(COLOR2);
        dataset2.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset2.setPointHoverBorderWidth(2);;
       	dataset2.setPointRadius(5);
        dataset2.setPointHitRadius(10);
        dataset2.setData(new ArrayList<Integer>() {{
        	add(80);
        	add(55);
        	add(61);
        	add(61);
        	add(72);
        	add(52);
        	add(25);
        	add(69);
        	add(52);
        	add(55);
        	add(65); 
        	add(26);
        }});
        dataset2.setSpanGaps(false);
		
        ChartStatWorkDatasetModel dataset3 = new ChartStatWorkDatasetModel();
		dataset3.setLabel("งานลาศึกษา");
		dataset3.setFill(false);
		dataset3.setLineTension(0.1);
        dataset3.setBackgroundColor(COLOR3);
        dataset3.setBorderColor(COLOR3);
        dataset3.setBorderCapStyle("butt");
        dataset3.setBorderDash(new ArrayList<String>());
        dataset3.setBorderDashOffset(0.0);
        dataset3.setBorderJoinStyle("miter");
        dataset3.setPointBorderColor(COLOR3);
        dataset3.setPointBackgroundColor(COLOR_WHITE);
        dataset3.setPointBorderWidth(1);
        dataset3.setPointHoverRadius(5);
        dataset3.setPointHoverBackgroundColor(COLOR3);
        dataset3.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset3.setPointHoverBorderWidth(2);;
       	dataset3.setPointRadius(5);
        dataset3.setPointHitRadius(10);
        dataset3.setData(new ArrayList<Integer>() {{
        	add(52);
        	add(34);
        	add(25);
        	add(28);
        	add(47);
        	add(46);
        	add(65);
        	add(54);
        	add(87);
        	add(66);
        	add(37);
        	add(20);
        }});
        dataset3.setSpanGaps(false);
        
        ChartStatWorkDatasetModel dataset4 = new ChartStatWorkDatasetModel();
		dataset4.setLabel("ฐานข้อมูลลูกหนี้");
		dataset4.setFill(false);
		dataset4.setLineTension(0.1);
        dataset4.setBackgroundColor(COLOR4);
        dataset4.setBorderColor(COLOR4);
        dataset4.setBorderCapStyle("butt");
        dataset4.setBorderDash(new ArrayList<String>());
        dataset4.setBorderDashOffset(0.0);
        dataset4.setBorderJoinStyle("miter");
        dataset4.setPointBorderColor(COLOR4);
        dataset4.setPointBackgroundColor(COLOR_WHITE);
        dataset4.setPointBorderWidth(1);
        dataset4.setPointHoverRadius(5);
        dataset4.setPointHoverBackgroundColor(COLOR4);
        dataset4.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset4.setPointHoverBorderWidth(2);;
       	dataset4.setPointRadius(5);
        dataset4.setPointHitRadius(10);
        dataset4.setData(new ArrayList<Integer>() {{
        	add(102);
        	add(72);
        	add(46);
        	add(69);
        	add(58);
        	add(85);
        	add(36);
        	add(77);
        	add(56);
        	add(99);
        	add(59);
        	add(50);
        }});
        dataset4.setSpanGaps(false);
        
        chartStatWorkDatasetModels.add(dataset1);
        chartStatWorkDatasetModels.add(dataset2);
        chartStatWorkDatasetModels.add(dataset3);
        chartStatWorkDatasetModels.add(dataset4);
        
        chartStatWorkModel.setDatasets(chartStatWorkDatasetModels);
		
		return chartStatWorkModel;
		
	}
	
	public ChartStatCaseModel getChartStatCaseModel(UserSession userSession) {
		ChartStatCaseModel chartStatCaseModel = new ChartStatCaseModel();
		chartStatCaseModel.setLabels(new ArrayList<String>() {{
			 add("ม.ค.59");
			 add("ก.พ.59");
			 add("มี.ค.59");
			 add("เม.ย.59");
			 add("พ.ค.59");
			 add("มิ.ย.59");
			 add("ก.ค.59");
			 add("ส.ค.59");
			 add("ก.ย.59");
			 add("ต.ค.59");
			 add("พ.ย.59");
			 add("ธ.ค.59");
		}});
		
		List<ChartStatCaseDatasetModel> chartStatCaseDatasetModels = new ArrayList<ChartStatCaseDatasetModel>();
		
		ChartStatCaseDatasetModel dataset1 = new ChartStatCaseDatasetModel();
		dataset1.setLabel("ทุจริต/เงินขาดบัญชี");
		dataset1.setFill(false);
		dataset1.setLineTension(0.1);
        dataset1.setBackgroundColor(COLOR1);
        dataset1.setBorderColor(COLOR1);
        dataset1.setBorderCapStyle("butt");
        dataset1.setBorderDash(new ArrayList<String>());
        dataset1.setBorderDashOffset(0.0);
        dataset1.setBorderJoinStyle("miter");
        dataset1.setPointBorderColor(COLOR1);
        dataset1.setPointBackgroundColor(COLOR_WHITE);
        dataset1.setPointBorderWidth(1);
        dataset1.setPointHoverRadius(5);
        dataset1.setPointHoverBackgroundColor(COLOR1);
        dataset1.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset1.setPointHoverBorderWidth(2);;
       	dataset1.setPointRadius(5);
        dataset1.setPointHitRadius(10);
        dataset1.setData(new ArrayList<Integer>() {{
        	add(27);
        	add(18);
        	add(32);
        	add(14);
        	add(15);
        	add(5);
        	add(36);
        	add(5);
        	add(12);
        	add(11);
        	add(26);
        	add(21);
        }});
        dataset1.setSpanGaps(false);
        
        ChartStatCaseDatasetModel dataset2 = new ChartStatCaseDatasetModel();
        dataset2.setLabel("ไม่ปฏิบัติตามกฏหรือระเบียบ (การเงินการคลัง)");
		dataset2.setFill(false);
		dataset2.setLineTension(0.1);
        dataset2.setBackgroundColor(COLOR2);
        dataset2.setBorderColor(COLOR2);
        dataset2.setBorderCapStyle("butt");
        dataset2.setBorderDash(new ArrayList<String>());
        dataset2.setBorderDashOffset(0.0);
        dataset2.setBorderJoinStyle("miter");
        dataset2.setPointBorderColor(COLOR2);
        dataset2.setPointBackgroundColor(COLOR_WHITE);
        dataset2.setPointBorderWidth(1);
        dataset2.setPointHoverRadius(5);
        dataset2.setPointHoverBackgroundColor(COLOR2);
        dataset2.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset2.setPointHoverBorderWidth(2);;
       	dataset2.setPointRadius(5);
        dataset2.setPointHitRadius(10);
        dataset2.setData(new ArrayList<Integer>() {{
        	add(5);
        	add(7);
        	add(12);
        	add(2); 
        	add(9); 
        	add(17);
        	add(12);
        	add(10);
        	add(15);
        	add(16);
        	add(18); 
        	add(8);
        }});
        dataset2.setSpanGaps(false);
		
        ChartStatCaseDatasetModel dataset3 = new ChartStatCaseDatasetModel();
        dataset3.setLabel("ไม่ปฏิบัติตามกฏหรือระเบียบ (พัสดุ)");
		dataset3.setFill(false);
		dataset3.setLineTension(0.1);
        dataset3.setBackgroundColor(COLOR3);
        dataset3.setBorderColor(COLOR3);
        dataset3.setBorderCapStyle("butt");
        dataset3.setBorderDash(new ArrayList<String>());
        dataset3.setBorderDashOffset(0.0);
        dataset3.setBorderJoinStyle("miter");
        dataset3.setPointBorderColor(COLOR3);
        dataset3.setPointBackgroundColor(COLOR_WHITE);
        dataset3.setPointBorderWidth(1);
        dataset3.setPointHoverRadius(5);
        dataset3.setPointHoverBackgroundColor(COLOR3);
        dataset3.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset3.setPointHoverBorderWidth(2);;
       	dataset3.setPointRadius(5);
        dataset3.setPointHitRadius(10);
        dataset3.setData(new ArrayList<Integer>() {{
        	add(11);
        	add(13);
        	add(17);
        	add(2);
        	add(9);
        	add(24);
        	add(19);
        	add(10);
        	add(22);
        	add(13);
        	add(14);
        	add(9);
        }});
        dataset3.setSpanGaps(false);
        
        ChartStatCaseDatasetModel dataset4 = new ChartStatCaseDatasetModel();
        dataset4.setLabel("ไม่ปฏิบัติตามกฏหรือระเบียบ (อื่นๆ)");
		dataset4.setFill(false);
		dataset4.setLineTension(0.1);
        dataset4.setBackgroundColor(COLOR4);
        dataset4.setBorderColor(COLOR4);
        dataset4.setBorderCapStyle("butt");
        dataset4.setBorderDash(new ArrayList<String>());
        dataset4.setBorderDashOffset(0.0);
        dataset4.setBorderJoinStyle("miter");
        dataset4.setPointBorderColor(COLOR4);
        dataset4.setPointBackgroundColor(COLOR_WHITE);
        dataset4.setPointBorderWidth(1);
        dataset4.setPointHoverRadius(5);
        dataset4.setPointHoverBackgroundColor(COLOR4);
        dataset4.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset4.setPointHoverBorderWidth(2);;
       	dataset4.setPointRadius(5);
        dataset4.setPointHitRadius(10);
        dataset4.setData(new ArrayList<Integer>() {{
        	add(4);
        	add(7);
        	add(10);
        	add(2);
        	add(7);
        	add(30);
        	add(25);
        	add(19);
        	add(8);
        	add(7);
        	add(15);
        	add(13);
        }});
        dataset4.setSpanGaps(false);
        
        ChartStatCaseDatasetModel dataset5 = new ChartStatCaseDatasetModel();
        dataset5.setLabel("คนร้ายกระทำโจรกรรมหรือทรัพย์สินสูญหาย");
		dataset5.setFill(false);
		dataset5.setLineTension(0.1);
        dataset5.setBackgroundColor(COLOR5);
        dataset5.setBorderColor(COLOR5);
        dataset5.setBorderCapStyle("butt");
        dataset5.setBorderDash(new ArrayList<String>());
        dataset5.setBorderDashOffset(0.0);
        dataset5.setBorderJoinStyle("miter");
        dataset5.setPointBorderColor(COLOR5);
        dataset5.setPointBackgroundColor(COLOR_WHITE);
        dataset5.setPointBorderWidth(1);
        dataset5.setPointHoverRadius(5);
        dataset5.setPointHoverBackgroundColor(COLOR5);
        dataset5.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset5.setPointHoverBorderWidth(2);;
       	dataset5.setPointRadius(5);
        dataset5.setPointHitRadius(10);
        dataset5.setData(new ArrayList<Integer>() {{
        	add(12);
        	add(10);
        	add(2);
        	add(14);
        	add(15);
        	add(5);
        	add(36);
        	add(5);
        	add(12);
        	add(11);
        	add(26);
        	add(21);
        }});
        dataset5.setSpanGaps(false);
        
        ChartStatCaseDatasetModel dataset6 = new ChartStatCaseDatasetModel();
        dataset6.setLabel("อาคารสถานที่ถูกเพลิงไหม้/ภัยธรรมชาติ");
		dataset6.setFill(false);
		dataset6.setLineTension(0.1);
        dataset6.setBackgroundColor(COLOR6);
        dataset6.setBorderColor(COLOR6);
        dataset6.setBorderCapStyle("butt");
        dataset6.setBorderDash(new ArrayList<String>());
        dataset6.setBorderDashOffset(0.0);
        dataset6.setBorderJoinStyle("miter");
        dataset6.setPointBorderColor(COLOR6);
        dataset6.setPointBackgroundColor(COLOR_WHITE);
        dataset6.setPointBorderWidth(1);
        dataset6.setPointHoverRadius(5);
        dataset6.setPointHoverBackgroundColor(COLOR6);
        dataset6.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset6.setPointHoverBorderWidth(2);;
       	dataset6.setPointRadius(5);
        dataset6.setPointHitRadius(10);
        dataset6.setData(new ArrayList<Integer>() {{
        	add(2);
        	add(3);
        	add(6);
        	add(2);
        	add(1);
        	add(8);
        	add(10);
        	add(17);
        	add(7);
        	add(9);
        	add(13);
        	add(17);
        }});
        dataset6.setSpanGaps(false);
        
        ChartStatCaseDatasetModel dataset7 = new ChartStatCaseDatasetModel();
        dataset7.setLabel("อุบัติเหตุ");
		dataset7.setFill(false);
		dataset7.setLineTension(0.1);
        dataset7.setBackgroundColor(COLOR7);
        dataset7.setBorderColor(COLOR7);
        dataset7.setBorderCapStyle("butt");
        dataset7.setBorderDash(new ArrayList<String>());
        dataset7.setBorderDashOffset(0.0);
        dataset7.setBorderJoinStyle("miter");
        dataset7.setPointBorderColor(COLOR7);
        dataset7.setPointBackgroundColor(COLOR_WHITE);
        dataset7.setPointBorderWidth(1);
        dataset7.setPointHoverRadius(5);
        dataset7.setPointHoverBackgroundColor(COLOR7);
        dataset7.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset7.setPointHoverBorderWidth(2);;
       	dataset7.setPointRadius(5);
        dataset7.setPointHitRadius(10);
        dataset7.setData(new ArrayList<Integer>() {{
        	add(3);
        	add(3);
        	add(1);
        	add(7);
        	add(4);
        	add(11);
        	add(5);
        	add(9);
        	add(5);
        	add(11);
        	add(20);
        	add(4);
        }});
        dataset7.setSpanGaps(false);
        
        ChartStatCaseDatasetModel dataset8 = new ChartStatCaseDatasetModel();
        dataset8.setLabel("เหตุอื่นๆ");
		dataset8.setFill(false);
		dataset8.setLineTension(0.1);
        dataset8.setBackgroundColor(COLOR8);
        dataset8.setBorderColor(COLOR8);
        dataset8.setBorderCapStyle("butt");
        dataset8.setBorderDash(new ArrayList<String>());
        dataset8.setBorderDashOffset(0.0);
        dataset8.setBorderJoinStyle("miter");
        dataset8.setPointBorderColor(COLOR8);
        dataset8.setPointBackgroundColor(COLOR_WHITE);
        dataset8.setPointBorderWidth(1);
        dataset8.setPointHoverRadius(5);
        dataset8.setPointHoverBackgroundColor(COLOR8);
        dataset8.setPointHoverBorderColor(POINT_BORDER_COLOR);
        dataset8.setPointHoverBorderWidth(2);;
       	dataset8.setPointRadius(5);
        dataset8.setPointHitRadius(10);
        dataset8.setData(new ArrayList<Integer>() {{
        	add(7);
        	add(5);
        	add(1);
        	add(13);
        	add(6);
        	add(0);
        	add(7);
        	add(9);
        	add(11);
        	add(17);
        	add(16);
        	add(5);
        }});
        dataset8.setSpanGaps(false);
        
        chartStatCaseDatasetModels.add(dataset1);
        chartStatCaseDatasetModels.add(dataset2);
        chartStatCaseDatasetModels.add(dataset3);
        chartStatCaseDatasetModels.add(dataset4);
        chartStatCaseDatasetModels.add(dataset5);
        chartStatCaseDatasetModels.add(dataset6);
        chartStatCaseDatasetModels.add(dataset7);
        chartStatCaseDatasetModels.add(dataset8);
        
        chartStatCaseModel.setDatasets(chartStatCaseDatasetModels);
        
		return chartStatCaseModel;
	}
	
	public ChartStatLoanEducationModel getChartStatLoanEducationModel(UserSession userSession) {
		ChartStatLoanEducationModel chartStatLoanEducationModel = new ChartStatLoanEducationModel();
		
		chartStatLoanEducationModel.setLabels(new ArrayList<String>() {{
			add("กำลังศึกษา");
			add("ปฏิบัติราชการชดใช้");
			add("ผิดสัญญา");
		 	add("ผ่อนผันรับราชการ");
		 	add("ดำเนินคดี");
		}});
		
		ChartStatLoanEducationDatasetModel dataset = new ChartStatLoanEducationDatasetModel();
		dataset.setData(new ArrayList<Integer>() {{
        	add(365);
        	add(200);
        	add(10);
        	add(78);
        	add(50);
        }});
		dataset.setBackgroundColor(new ArrayList<String>() {{
			add(COLOR1);
			add(COLOR2);
			add(COLOR3);
		 	add(COLOR4);
		 	add(COLOR5);
		}});
		dataset.setHoverBackgroundColor(new ArrayList<String>() {{
			add(COLOR1);
			add(COLOR2);
			add(COLOR3);
		 	add(COLOR4);
		 	add(COLOR5);
		}});
		
		List<ChartStatLoanEducationDatasetModel> chartStatLoanEducationDatasetModels = new ArrayList<ChartStatLoanEducationDatasetModel>();
		chartStatLoanEducationDatasetModels.add(dataset);
		
		chartStatLoanEducationModel.setDatasets(chartStatLoanEducationDatasetModels);
		
		return chartStatLoanEducationModel;
	}
	
	public ChartStatLoanEducationBreakModel getChartStatLoanEducationBreakModel(UserSession userSession) {
		ChartStatLoanEducationBreakModel chartStatLoanEducationBreakModel = new ChartStatLoanEducationBreakModel();
		chartStatLoanEducationBreakModel.setLabels(new ArrayList<String>() {{
			add("ชดใช้แล้ว");
			add("ยังไม่ได้ชดใช้");
		}});
		
		ChartStatLoanEducationBreakDatasetModel dataset = new ChartStatLoanEducationBreakDatasetModel();
		dataset.setData(new ArrayList<Integer>() {{
        	add(240000000);
        	add(524910000);
        }});
		dataset.setBackgroundColor(new ArrayList<String>() {{
			add(COLOR1);
			add(COLOR2);
		}});
		dataset.setHoverBackgroundColor(new ArrayList<String>() {{
			add(COLOR1);
			add(COLOR2);
		}});
		
		List<ChartStatLoanEducationBreakDatasetModel> chartStatLoanEducationBreakDatasetModels = new ArrayList<ChartStatLoanEducationBreakDatasetModel>();
		chartStatLoanEducationBreakDatasetModels.add(dataset);
		
		chartStatLoanEducationBreakModel.setDatasets(chartStatLoanEducationBreakDatasetModels);
		
		return chartStatLoanEducationBreakModel;
	}

}
