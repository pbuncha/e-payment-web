package th.go.cgd.epayment.service;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

@Service
@Transactional
public class IdentificationCardService {
	
	public boolean checkIdentificationCardNo(String cardNo) {
		int sum = 0;
		if (cardNo.length() != 13) {
			return false;
		}		
		for (int i = 0; i < 12; i++) {
			sum += Integer.parseInt(String.valueOf(cardNo.charAt(i)))*(13-i);
		}
		if((11 - sum % 11) %10 != Integer.parseInt(String.valueOf(cardNo.charAt(12)))) {
			return false; 
		}
		return true;
	}
}
