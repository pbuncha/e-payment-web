package th.go.cgd.epayment.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hazelcast.core.HazelcastInstance;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.MDistrict;
import th.go.cgd.epayment.entity.MLogAction;
import th.go.cgd.epayment.entity.MMaritalStatus;
import th.go.cgd.epayment.entity.MOrgType;
import th.go.cgd.epayment.entity.MParam;
import th.go.cgd.epayment.entity.MPersonType;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MReport;
import th.go.cgd.epayment.entity.MStatus;
import th.go.cgd.epayment.entity.MSubdistrict;
import th.go.cgd.epayment.entity.MTitleName;
import th.go.cgd.epayment.entity.MUserAccessPermission;
import th.go.cgd.epayment.entity.MUserRole;
import th.go.cgd.epayment.entity.MUserStatus;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.repository.MasterRepository;

@Service
@Transactional
public class MasterService {
	
	private static final Logger logger = Logger.getLogger(MasterService.class);
	
	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	@Autowired
	private MasterRepository repository;
	
    @PersistenceContext
    private EntityManager entityManager;
	
	private <T> void set(List<Object> list, Class<T> clazz)
	{
		if(null!=list)
		{
			list.clear();
		}
		list.addAll(repository.findAll(clazz, null));
	}
	
    private <T> void set(List<Object> list, Class<T> clazz, String columnOrder)
    {
		if(null!=list)
		{
			list.clear();
		}
		list.addAll(repository.findAll(clazz, columnOrder));
	}
    
    private <T> void setDataActive(List<Object> list, Class<T> clazz)
    {
    	if(null!=list)
    	{
    		list.clear();
    	}
    	list.addAll(repository.findAllStatusActive(clazz, null));
    }
    
    public <T> void reloadDataActive(String key, Class<T> clazz)
	{
		setDataActive(hazelcastInstance.getList(key), clazz);
	}
	
	public <T> void reload(String key, Class<T> clazz)
	{
		set(hazelcastInstance.getList(key), clazz);
	}
	
	public <T> T save(T o)
	{
		return repository.save(o);
	}
	
	public <T> void delete(Class<T> clazz, Short id)
	{
		repository.delete(clazz, id);
	}
	
//	public void setMLitigantType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_LITIGANTTYPE), MLitigantType.class);
//	}
	
	public void setMMaritalStatus(){
		set(hazelcastInstance.getList(EPaymentConstant.M_MARITAL_STATUS), MMaritalStatus.class);
	}
	
//	public void setMModule(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_MODULE), MModule.class, "order");
//	}
//	
//	public void setMMonth(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_MONTH), MMonth.class);
//	}
//	
//	public void setMSendCaseType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_SEND_CASE_TYPE), MSendCaseType.class);
//	}
//	
//	public void setMExpiryType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_EXPIRY_TYPE), MExpiryType.class);
//	}
	
	public void setMTitleName(){
		set(hazelcastInstance.getList(EPaymentConstant.M_TITLE_NAME), MTitleName.class);
	}
	
	public void setMUserStatus(){
		set(hazelcastInstance.getList(EPaymentConstant.M_USER_STATUS), MUserStatus.class);
	}
	
	public void setMProvince(){
		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_PROVINCE), MProvince.class);
	}
	
	public void setMProvinceAllStatus(){
		set(hazelcastInstance.getList(EPaymentConstant.M_PROVINCE_ALL_STATUS), MProvince.class);
	}
	
//	public void setMRelationship(){
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_RELATIONSHIP), MRelationship.class);
//	}
//	
//	public void setMRelationshipAllStatus(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_RELATIONSHIP_ALL_STATUS), MRelationship.class);
//	}
	
	public void setMPersonType(){
		set(hazelcastInstance.getList(EPaymentConstant.M_PERSONTYPE), MPersonType.class);
	}
	
	public void setMOrgType(){
		set(hazelcastInstance.getList(EPaymentConstant.M_ORGTYPE), MOrgType.class);
	}
	
//	public void setMCasePermission(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CASE_PERMISSION), MCasePermission.class);
//	}
//	
//	public void setMCourt(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_COURT), MCourt.class);
//	}
	
//	public void setMCourtType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_COURT_TYPE), MCourtType.class);
//	}
//	
//	public void setMCurrency(){
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_CURRENCY), MCurrency.class);
//	}
//	
//	public void setMCurrencyAllStatus(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CURRENCY_ALL_STATUS), MCurrency.class);
//	}
//	
//	public void setMDocumentTemplate(){
//		List list = hazelcastInstance.getList(EPaymentConstant.M_DOCUMENT_TEMPLATE);
//		
//		set(list, MDocumentTemplate.class, "order");
//		
//		List<MDocumentTemplate> ci0002List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_CI0002 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> ci0004List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_CI0004 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> de0001List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_DE0001 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> de0008List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_DE0008 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> de0009List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_DE0009 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> de0021List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_DE0021 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> de0023List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_DE0023 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> ed0002List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_ED0002 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> ed0005List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_ED0005 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> ed0011List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_ED0011 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0001List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0001 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0002List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0002 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0003List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0003 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0004List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0004 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0005List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0005 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0006List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0006 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0007List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0007 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0008List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0008 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0009List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0009 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0010List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0010 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0013List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0013 = new ArrayList<MDocumentTemplate>();
//		
//		List<MDocumentTemplate> vi0015List = new ArrayList<MDocumentTemplate>();
//		EPaymentConstant.DOC_TEMPLATE_VI0015 = new ArrayList<MDocumentTemplate>();
//		
//		
//		for(Object o : list)
//		{
//			MDocumentTemplate mDocTemplate = (MDocumentTemplate)o;
//			if(!EPaymentConstant.STATUS_ACTIVE.equals(mDocTemplate.getStatus()))
//			{
//				continue;
//			}
//			if(mDocTemplate.getMForm().getFormId().equals("CI0002"))
//			{
//				ci0002List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("CI0004"))
//			{
//				ci0004List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("DE0001"))
//			{
//				de0001List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("DE0008"))
//			{
//				de0008List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("DE0009"))
//			{
//				de0009List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("DE0021"))
//			{
//				de0021List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("DE0023"))
//			{
//				de0023List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("ED0002"))
//			{
//				ed0002List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("ED0005"))
//			{
//				ed0005List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("ED0011"))
//			{
//				ed0011List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0001"))
//			{
//				vi0001List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0002"))
//			{
//				vi0002List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0003"))
//			{
//				vi0003List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0004"))
//			{
//				vi0004List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0005"))
//			{
//				vi0005List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0006"))
//			{
//				vi0006List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0007"))
//			{
//				vi0007List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0008"))
//			{
//				vi0008List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0009"))
//			{
//				vi0009List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0010"))
//			{
//				vi0010List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0013"))
//			{
//				vi0013List.add(mDocTemplate);
//			}
//			else if(mDocTemplate.getMForm().getFormId().equals("VI0015"))
//			{
//				vi0015List.add(mDocTemplate);
//			}
//		}
//		
//		EPaymentConstant.DOC_TEMPLATE_CI0002.addAll(ci0002List);
//		EPaymentConstant.DOC_TEMPLATE_CI0004.addAll(ci0004List);
//		EPaymentConstant.DOC_TEMPLATE_DE0001.addAll(de0001List);
//		EPaymentConstant.DOC_TEMPLATE_DE0008.addAll(de0008List);
//		EPaymentConstant.DOC_TEMPLATE_DE0009.addAll(de0009List);
//		EPaymentConstant.DOC_TEMPLATE_DE0021.addAll(de0021List);
//		EPaymentConstant.DOC_TEMPLATE_DE0023.addAll(de0023List);
//		EPaymentConstant.DOC_TEMPLATE_ED0002.addAll(ed0002List);
//		EPaymentConstant.DOC_TEMPLATE_ED0005.addAll(ed0005List);
//		EPaymentConstant.DOC_TEMPLATE_ED0011.addAll(ed0011List);
//		EPaymentConstant.DOC_TEMPLATE_VI0001.addAll(vi0001List);
//		EPaymentConstant.DOC_TEMPLATE_VI0002.addAll(vi0002List);
//		EPaymentConstant.DOC_TEMPLATE_VI0003.addAll(vi0003List);
//		EPaymentConstant.DOC_TEMPLATE_VI0004.addAll(vi0004List);
//		EPaymentConstant.DOC_TEMPLATE_VI0005.addAll(vi0005List);
//		EPaymentConstant.DOC_TEMPLATE_VI0006.addAll(vi0006List);
//		EPaymentConstant.DOC_TEMPLATE_VI0007.addAll(vi0007List);
//		EPaymentConstant.DOC_TEMPLATE_VI0008.addAll(vi0008List);
//		EPaymentConstant.DOC_TEMPLATE_VI0009.addAll(vi0009List);
//		EPaymentConstant.DOC_TEMPLATE_VI0010.addAll(vi0010List);
//		EPaymentConstant.DOC_TEMPLATE_VI0013.addAll(vi0013List);
//		EPaymentConstant.DOC_TEMPLATE_VI0015.addAll(vi0015List);
//	}
//	
//	public void setMForm(){
//		List list = hazelcastInstance.getList(EPaymentConstant.M_FORM);
//		
//		set(list, MForm.class, "order");
//		
//		List<MForm> viList = new ArrayList<MForm>();
//		EPaymentConstant.M_FORM_VI_LIST = new ArrayList<MForm>();
//		
//		List<MForm> ciList = new ArrayList<MForm>();
//		EPaymentConstant.M_FORM_CI_LIST = new ArrayList<MForm>();
//		
//		List<MForm> edList = new ArrayList<MForm>();
//		EPaymentConstant.M_FORM_ED_LIST = new ArrayList<MForm>();
//		
//		List<MForm> deList = new ArrayList<MForm>();
//		EPaymentConstant.M_FORM_DE_LIST = new ArrayList<MForm>();
//		
//		List<MForm> viAllList = new ArrayList<MForm>();
//		EPaymentConstant.M_FORM_VI_ALL_LIST = new ArrayList<MForm>();
//		
//		List<MForm> ciAllList = new ArrayList<MForm>();
//		EPaymentConstant.M_FORM_CI_ALL_LIST = new ArrayList<MForm>();
//		
//		List<MForm> edAllList = new ArrayList<MForm>();
//		EPaymentConstant.M_FORM_ED_ALL_LIST = new ArrayList<MForm>();
//		
//		List<MForm> deAllList = new ArrayList<MForm>();
//		EPaymentConstant.M_FORM_DE_ALL_LIST = new ArrayList<MForm>();
//		
//		List<MForm> viChildList = new ArrayList<MForm>();
//		List<MForm> ciChildList = new ArrayList<MForm>();
//		List<MForm> edChildList = new ArrayList<MForm>();
//		List<MForm> deChildList = new ArrayList<MForm>();
//		for(Object o : list)
//		{
//			MForm mform = (MForm)o;
//			if(null==mform.getMForm())
//			{
//				if(mform.getMModule().getModuleId().equals("VI"))
//				{
//					viList.add(mform);
//				}
//				else if(mform.getMModule().getModuleId().equals("CI"))
//				{
//					ciList.add(mform);
//				}
//				else if(mform.getMModule().getModuleId().equals("ED"))
//				{
//					edList.add(mform);
//				}
//				else if(mform.getMModule().getModuleId().equals("DE"))
//				{
//					deList.add(mform);
//				}
//			}
//			else
//			{
//				if(mform.getMModule().getModuleId().equals("VI"))
//				{
//					viChildList.add(mform);
//				}
//				else if(mform.getMModule().getModuleId().equals("CI"))
//				{
//					ciChildList.add(mform);
//				}
//				else if(mform.getMModule().getModuleId().equals("ED"))
//				{
//					edChildList.add(mform);
//				}
//				else if(mform.getMModule().getModuleId().equals("DE"))
//				{
//					deChildList.add(mform);
//				}
//			}
//		}
//		
//		EPaymentConstant.M_FORM_VI_LIST.addAll(viList);
//		EPaymentConstant.M_FORM_CI_LIST.addAll(ciList);
//		EPaymentConstant.M_FORM_ED_LIST.addAll(edList);
//		EPaymentConstant.M_FORM_DE_LIST.addAll(deList);
//		
//		viAllList.addAll(viList);
//		ciAllList.addAll(ciList);
//		edAllList.addAll(edList);
//		deAllList.addAll(deList);
//		
//		setChild(viAllList, viChildList);
//		setChild(ciAllList, ciChildList);
//		setChild(edAllList, edChildList);
//		setChild(deAllList, deChildList);
//		
//		EPaymentConstant.M_FORM_VI_ALL_LIST.addAll(viAllList);
//		EPaymentConstant.M_FORM_CI_ALL_LIST.addAll(ciAllList);
//		EPaymentConstant.M_FORM_ED_ALL_LIST.addAll(edAllList);
//		EPaymentConstant.M_FORM_DE_ALL_LIST.addAll(deAllList);
//	}
//	
//	private void setChild(List<MForm> parentList, List<MForm> childList)
//	{
//		for(MForm parent : parentList)
//		{
//			List<MForm> childs = new ArrayList<MForm>();
//			parent.setChilds(childs);
//			for(MForm child : childList)
//			{
//				if(parent.getFormId().equals(child.getMForm().getFormId()))
//				{
//					parent.getChilds().add(child);
//				}
//			}
//		}
//	}
//	
//	public void setMFormStatus(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_FORM_STATUS), MFormStatus.class);
//	}
//	
//	public void setMLawsuitCaseDecided(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_LAWSUIT_CASE_DECIDED), MLawsuitCaseDecided.class);
//	}
//	
//	public void setMLawsuitCompromiseResult(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_LAWSUIT_COMPROMISE_RESULT), MLawsuitCompromiseResult.class);
//	}
//	
//	public void setMLawsuitPersonDecided(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_LAWSUIT_PERSON_DECIDED), MLawsuitPersonDecided.class);
//	}
//	
//	public void setMMessageTemplate(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_MESSAGE_TEMPLATE), MMessageTemplate.class);
//	}
//	
//	public void setMNewsStatus(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_NEWS_STATUS), MNewsStatus.class);
//	}
//	
	public void setMParam(){
		set(hazelcastInstance.getList(EPaymentConstant.M_PARAM), MParam.class);
		try
		{
			List<MParam> list = hazelcastInstance.getList(EPaymentConstant.M_PARAM);
			for(MParam param : list)
			{
				if(param.getName().equals("DOWNLOAD_SESSION_EXPIRY"))
				{
					EPaymentConstant.DOWNLOAD_SESSION_EXPIRY = Integer.parseInt(param.getValue());
					break;
				}
			}
		}
		catch(Exception e)
		{
			logger.error(e);
		}
	}
//	
//	public void setMFlowchart(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_FLOWCHART), MFlowchart.class);
//	}
//	
//	public void setMViolationAccompliceType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATE_ACCOMPLICE_TYPE), MViolationAccompliceType.class);
//	}
//	
//	public void setMViolationAppointmentOrgType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATE_APPOINTMENT_ORG_TYPE), MViolationAppointmentOrgType.class);
//	}
//	
//	public void setMViolationCaseType(){
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_VIOLATE_CASE_TYPE), MViolationCaseType.class);
//	}
//	
//	public void setMViolationCaseTypeAllStatus(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATE_CASE_TYPE_ALL_STATUS), MViolationCaseType.class);
//	}
//	
//	public void setMViolationCloseCaseReason(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATE_CLOSE_CASE_REASON), MViolationCloseCaseReason.class);
//	}
//	
//	public void setMViolationInquiryResult(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATION_INQUIRY_RESULT), MViolationInquiryResult.class);
//	}
//	
//	public void setMViolationOrderingType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATION_ORDERING_TYPE), MViolationOrderingType.class);
//	}
//	
//	public void setMViolationPrescriptionYear(){
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_VIOLATION_PRESCRIPTION_YEAR), MViolationPrescriptionYear.class);
//	}
//	
//	public void setMViolationPrescriptionYearAllStatus(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATION_PRESCRIPTION_YEAR_ALL_STATUS), MViolationPrescriptionYear.class);
//	}
//	
//	public void setMViolationPreinvestigateInquiry(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATE_PRE_INVESTIGATE_INQUIRY), MViolationPreinvestigateInquiry.class);
//	}
//	
//	public void setMViolationAppealResult(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_VIOLATE_APPEAL_RESULT), MViolationAppealResult.class);
//	}
//	
//	public void setMCivilAbttLitigantType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CIVIL_ABTT_LITIGANTTYPE), MCivilAbttLitigantType.class);
//	}
//	
//	public void setMCivilArbMeditationResult(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CIVIL_ARB_MEDITATION_RESULT), MCivilArbitratorMeditationResult.class);
//	}
//	
//	public void setMCivilCaseType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CIVIL_CASE_TYPE), MCivilCaseType.class);
//	}
//	
//	public void setMCivilCloseCaseReason(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CIVIL_CLOSE_CASE_REASON), MCivilCloseCaseReason.class);
//	}
//	
//	public void setMCivilCompensationType(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CIVIL_COMPENSATION_TYPE), MCivilCompensationType.class);
//	}
	
//	public void setMCivilMeditationResult(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CIVIL_MEDITATION_RESULT), MCivilMeditationResult.class);
//	}
//	
//	public void setMCivilPrescriptionYear(){
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_CIVIL_PRESCRIPTION_YEAR), MCivilPrescriptionYear.class);
//	}
//	
//	public void setMCivilPrescriptionYearAllStatus(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_CIVIL_PRESCRIPTION_YEAR_ALL_STATUS), MCivilPrescriptionYear.class);
//	}
//	
//	public void setMCountry() {
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_COUNTRY), MCountry.class);
//	}
//	
//	public void setMCountryAllStatus() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_COUNTRY_ALL_STATUS), MCountry.class);
//	}
//	
//	public void setMEducationCaseType() {
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_CASE_TYPE), MEducationCaseType.class);
//	}
//	
//	public void setMEducationCaseTypeAllStatus() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_CASE_TYPE_ALL_STATUS), MEducationCaseType.class);
//	}
//	
//	public void setMEducationCloseCaseReason() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_CLOSE_CASE_REASON), MEducationCloseCaseReason.class);
//	}
//	
//	public void setMEducationContractType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_CONTRACT_TYPE), MEducationContractType.class);
//	}
//	
//	public void setMEducationExtensionDecisionResult() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_EXTENSTION_DECISION_RESULT), MEducationExtensionDecisionResult.class);
//	}
//	
//	public void setMEducationLeaveType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_LEAVE_TYPE), MEducationLeaveType.class);
//	}
//	
//	public void setMEducationLevel() {
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_LEVEL), MEducationLevel.class);
//	}
//	
//	public void setMEducationLevelAllStatus() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_LEVEL_ALL_STATUS), MEducationLevel.class);
//	}
//	
//	public void setMEducationLiabilityType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCAITON_LIABILITY_TYPE), MEducationLiabilityType.class);
//	}
//	
//	public void setMEducationPersonType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_PERSONAL_TYPE), MEducationPersonType.class);
//	}
//	
//	public void setMEducationRelationship() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_RELATIONSHIP), MEducationRelationship.class);
//	}
//	
//	public void setMEducationReplaceTerm() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_REPLACE_TERM), MEducationReplaceTerm.class);
//	}
//	
//	public void setMEducationReplaceType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_REPLACE_TYPE), MEducationReplaceType.class);
//	}
//	
//	public void setMEducationScholarshipType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_SCHOLARSHIP_TYPE), MEducationScholarshipType.class);
//	}
//	
//	public void setMEducationType() {
//		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_TYPE), MEducationType.class);
//	}
//	
//	public void setMEducationTypeAllStatus() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_TYPE_ALL_STATUS), MEducationType.class);
//	}
//	
//	public void setMEducationExpenseType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EDUCATION_EXPENSE_TYPE), MEducationExpenseType.class);
//	}
//	
	public void setMStatus() {
		set(hazelcastInstance.getList(EPaymentConstant.M_STATUS), MStatus.class);
	}
	
	public void setMUserRole() {
		set(hazelcastInstance.getList(EPaymentConstant.M_USER_ROLE), MUserRole.class);
	}
	
	public void setMUserAcessPermission() {
		set(hazelcastInstance.getList(EPaymentConstant.M_USER_ACCESS_PERMISSION), MUserAccessPermission.class);
	}
//	
//	public void SetMCaseReportResult() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_CASE_REPORT_RESULT), MCaseReportResult.class);
//	}
//	
//	public void SetMEmailTemplate() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_EMAIL_TEMPLATE), MEmailTemplate.class);
//	}
//	
//	public void SetMDebtAssetTrackingResult() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_ASSET_TRACKING_RESULT), MDebtAssetTrackingResult.class);
//	}
//	
//	public void SetMDebtBankruptCaseDecided() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_BANKRUPT_CASE_DECIDED), MDebtBankruptCaseDecided.class);
//	}
//	
//	public void SetMDebtBankruptCompromiseResult() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_BANKRUPT_COMPROMISE_RESULT), MDebtBankruptCompromiseResult.class);
//	}
//	
//	public void SetMDebtBankruptReceivingOrder() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_BANKRUPT_RECEIVING_ORDER), MDebtBankruptReceivingOrder.class);
//	}
//	
//	public void SetMDebtCloseCaseReason() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_CLOSE_CASE_REASON), MDebtCloseCaseReason.class);
//	}
//	
//	public void SetMDebtDecisionResult() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_DECISION_RESULT), MDebtDecisionResult.class);
//	}
	
//	public void SetMDebtInstallmentPeriod() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_INSTALLMENT_PERIOD), MDebtInstallmentPeriod.class);
//	}
//	
//	public void SetMDebtInstallmentPlan() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_INSTALLMENT_PLAN), MDebtInstallmentPlan.class);
//	}	
//	
//	public void SetMDebtPaymentType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_PAYMENT_TYPE), MDebtPaymentType.class);
//	}
//	
//	public void SetMDebtStatus() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_STATUS), MDebtStatus.class);
//	}
//	
//	public void SetMDebtType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_TYPE), MDebtType.class);
//	}
//	
//	public void SetMDebtWriteOffDecisionResult() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_WRITE_OFF_DECISION_RESULT), MDebtWriteOffDecisionResult.class);
//	}
//	
//	public void SetMDebtWriteOffReason() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_WRITE_OFF_REASON), MDebtWriteOffReason.class);
//	}
//	
//	public void SetMNotificationType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_NOTIFICATION_TYPE), MNotificationType.class);
//	}
//
	public void setMLogAction() {
		set(hazelcastInstance.getList(EPaymentConstant.M_LOG_ACTION), MLogAction.class);
	}
	
//	public void SetMDebtAmountType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_AMOUNT_TYPE), MDebtAmountType.class);
//	}
//	
//	public void SetMDebtCostType() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_DEBT_COST_TYPE), MDebtCostType.class);
//	}
//	
//	public void setMTaskStatus() {
//		set(hazelcastInstance.getList(EPaymentConstant.M_TASK_STATUS), MTaskStatus.class);
//	}
//	
//	public void setMPermissionRole(){
//		set(hazelcastInstance.getList(EPaymentConstant.M_PERMISSION_ROLE), MPermissionRole.class);
//	}
	
	public void setMDistrict(){
		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_DISTRICT), MDistrict.class);
	}
	
	public void setMDistrictAllStatus(){
		set(hazelcastInstance.getList(EPaymentConstant.M_DISTRICT_ALL_STATUS), MDistrict.class);
	}
	
	public void setMSubDistrict(){
		setDataActive(hazelcastInstance.getList(EPaymentConstant.M_SUB_DISTRICT), MSubdistrict.class);
	}
	
	public void setMSubDistrictAllStatus(){
		set(hazelcastInstance.getList(EPaymentConstant.M_SUB_DISTRICT_ALL_STATUS), MSubdistrict.class);
	}
	
	public void setMReport(){
		set(hazelcastInstance.getList(EPaymentConstant.M_REPORT), MReport.class);
	}
	
}
