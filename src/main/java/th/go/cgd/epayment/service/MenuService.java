package th.go.cgd.epayment.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.Menu;
//import th.go.cgd.epayment.entity.MenuPermission;
import th.go.cgd.epayment.entity.PermissionGroup;
import th.go.cgd.epayment.entity.PermissionItem;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.MenuItem;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.MenuRepository;

@Service
@Transactional
public class MenuService 
{
	@Autowired
	private MenuRepository menuRepository;
	
	private static final Logger logger = Logger.getLogger(MenuService.class);
	
	public List<MenuItem> menus = null;
	
//	public void create(UserSession userSession)
//	{
//		User user = userSession.getUser();
//		
//		if(null==user)
//		{
//			return;
//		}
//		
//		logger.info("Generating menu for user : " + user.getUserName());
//		
//		List<Menu> menuList = setMenuPerm(user);
//		Collections.sort(menuList, new MenuComparator());
//		
//		userSession.setUserMenus(menuList);
//
//		menus = new ArrayList<MenuItem>();
//		
//		for(Menu menu : menuList)
//		{
//			MenuItem menuItem = null;
//			
//			if(null!=menu.getStatus() && EPaymentConstant.STATUS_ACTIVE.equals(menu.getStatus()))
//			{
//                menuItem = locateMenuItem(menus, menu.getMenuId());
//			}
//			
//			if ((null == menuItem) && (null != menu) 
//					&& (null!=menu.getStatus() && EPaymentConstant.STATUS_ACTIVE.equals(menu.getStatus())))
//            {
//				Menu parentMenu = menu.getMenu();
//				
//                menuItem = new MenuItem(menu);
//
//                if (null == parentMenu)
//                {
//                	if(!contains(menuItem.getMenuId()))
//                	{
//                		menus.add(menuItem);
//                	}
//                }
//                else
//                {
//                    while (null != parentMenu)
//                    {
//                        MenuItem parentMenuItem = locateMenuItem(menus, parentMenu.getMenuId());
//
//                        if (null == parentMenuItem)
//                        {
//                        	parentMenuItem = new MenuItem(parentMenu);
//                        }
//
//                        if (!parentMenuItem.contains(menuItem.getMenuId()))
//                        {
//                        	parentMenuItem.addChild(menuItem);
//                        }
//                        
//                        if ((null == (parentMenu = parentMenu.getMenu()))
//                                && !menus.contains(parentMenuItem))
//                        {
//                            menus.add(parentMenuItem);
//                        }
//
//                        menuItem = parentMenuItem;
//                    }
//                }
//            }
//		}
//		
//		Collections.sort(menus, new MenuItemComparator());
//		userSession.setMenus(menus);
//	}
	
	private MenuItem locateMenuItem(List<MenuItem> menus, int id)
    {
        MenuItem item = null;
        for (MenuItem menu : menus)
        {
            if (id == menu.getMenuId())
            {
                item = menu;
                break;
            }
            else if (null != (item = menu.locateChild(id)))
            {
                break;
            }
        }
        return item;
    }
	
	public boolean contains(int id)
    {
        if (null != menus)
        {
            for (MenuItem item : menus)
            {
                if ((item.getMenuId() == id) || item.contains(id))
                {
                    return true;
                }
            }
        }
        return false;
    }
	
	public static class MenuComparator implements Comparator<Menu> 
	{
        public int compare(Menu o1, Menu o2) 
        {
            return ((Short) o1.getMenuId()).compareTo(o2.getMenuId());
        }
    }
	
	public static class MenuItemComparator implements Comparator<MenuItem> 
	{
        public int compare(MenuItem o1, MenuItem o2) 
        {
            return ((Short) o1.getOrderNo()).compareTo(o2.getOrderNo());
        }
    }
	
//	private List<Menu> setMenuPerm(User user)
//	{
//		List<Menu> menuList = new ArrayList<Menu>();
//		List<PermissionGroup> permGroupList = user.getPermissionGroups();
//		for(PermissionGroup group : permGroupList)
//		{
//			setPermList(group.getPermissionItems(), menuList);
//		}
//		setPermList(user.getPermissionItems(), menuList);
//		return menuList;
//	}
	
//	private void setPermList(List<PermissionItem> permList, List<Menu> menuList)
//	{
//		for(PermissionItem perm : permList)
//		{
//			for(MenuPermission menu : perm.getMenuPermissions())
//			{
//				if(!menuList.contains(menu.getMenu()))
//				{
//					menuList.add(menu.getMenu());
//				}
//			}
//		}
//	}
}
