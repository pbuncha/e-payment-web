package th.go.cgd.epayment.service;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.Organization;
import th.go.cgd.epayment.entity.UserInfo;
import th.go.cgd.epayment.repository.UserInfoRepository;

@Service
@Transactional
public class UserInfoService {
	
	@Autowired
	private UserInfoRepository userInfoRepository;
	
	@Autowired
	private UserService userService;
	
	public UserInfo findOne(int userId) {
		return userInfoRepository.findOne(userId);
	}
	
	public List<UserInfo> findByOrganizationAndUserInfoIsNull(Organization organization) {
		List<UserInfo> userInfos = new ArrayList<UserInfo>();
//		List<UserInfo> userInfoList = userInfoRepository.findByOrganizationAndUserInfoIsNull(organization);
//		for (UserInfo userInfo : userInfoList) {
//			if (!userInfo.getUser().getMUserStatus().getStatus().equals(userService.DELETE)) {
//				userInfos.add(userInfo);
//			}
//		}
		return userInfos;
	}
	
//	public List<UserInfo> findByUserInfo(UserInfo userInfo) {
//		return userInfoRepository.findByUserInfo(userInfo);
//	}
	
	public void save(UserInfo userInfo) {
		userInfoRepository.save(userInfo);
	}
	
}
