package th.go.cgd.epayment.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.component.PasswordConfigModel;
import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.controller.common.EpaymentApiController;
import th.go.cgd.epayment.entity.Address;
import th.go.cgd.epayment.entity.MReport;
import th.go.cgd.epayment.entity.MReportGroup;
import th.go.cgd.epayment.entity.MTitleName;
import th.go.cgd.epayment.entity.MUserAccessPermission;
import th.go.cgd.epayment.entity.MUserRole;
import th.go.cgd.epayment.entity.MUserStatus;
import th.go.cgd.epayment.entity.Organization;
import th.go.cgd.epayment.entity.PermissionGroup;
import th.go.cgd.epayment.entity.PermissionItem;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserAccessPermission;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.entity.UserInfo;
import th.go.cgd.epayment.model.UserRootTreeModel;
import th.go.cgd.epayment.model.UserRootTreeSearchModel;
import th.go.cgd.epayment.model.UserRootTreeStateModel;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.model.email.NotiNewUserModel;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.repository.UserCenterRepository;
import th.go.cgd.epayment.repository.AddressRepository;
import th.go.cgd.epayment.service.admin.OrganizationService;
import th.go.cgd.epayment.service.admin.PermissionGroupService;
import th.go.cgd.epayment.util.CatalogUtil;

@Service
@Transactional
public class UserService {
    private static final Logger logger = Logger.getLogger(UserService.class);
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private UserCenterRepository userCenterRepository;

	@Autowired
	private AddressRepository userAddressRepository;
	
	@Autowired
	private MenuService menuService;

	@Autowired
	private CatalogUtil catalogUtil;

	@Autowired
	private OrganizationService organizationService;

	@Autowired
	private PermissionGroupService permissionGroupService;

	@Autowired
	private IdentificationCardService identificationCardService;

	@Autowired
	private UserInfoService userInfoService;

	private Pattern charCasePattern;

	private Pattern numberPattern;

	private Pattern matchCharPattern;

	private PasswordConfigModel passwordConfigModel;

	public String ACTIVE = "A";
	public String DELETE = "D";

	private static final String TYPE_ORG = "organization";
	private static final String TYPE_USER = "user";

	private static final Integer NOTI_NEW_TO_USER = 1;
	private static final Integer NOTI_NEW_TO_OTHER = 2;

	@Autowired
	UserService(PasswordConfigModel passwordConfigModel) {
		this.passwordConfigModel = passwordConfigModel;
		charCasePattern = Pattern.compile(passwordConfigModel.charCase);
		numberPattern = Pattern.compile(passwordConfigModel.number);
		matchCharPattern = Pattern.compile(passwordConfigModel.matchChar);
	}

	public User findByUserName(String userName) {
		List<User> list = userRepository.findByUserName(userName);
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}

	public User get(int userId) {
		return userRepository.getOne(userId);
	}

	public User findOne(int userId) throws Exception {
		return userRepository.findOne(userId);
	}
	
	public UserCenter findOneUserCenter(String citizenID) throws Exception {
	    if(null != citizenID){
			List<UserCenter> list = userCenterRepository.findByCitizenNo(citizenID.trim());
			if(list.size() > 0){
				return list.get(0);
			}else {
				return null;
			}
		}else{
	    	return null;
		}

	}
	
	public User findOneUserByCitizen(String citizenID)  {
	    if(null != citizenID){
			List<UserCenter> list = userCenterRepository.findByCitizenNo(citizenID.trim());
			if(list.size() > 0){
				return list.get(0).getUserList().get(0);
			}else {
				return null;
			}
		}else{
	    	return null;
		}

	}

	
	public UserCenter findOneUserCenter(Integer userCenterID) throws Exception {
		return userCenterRepository.findOne(userCenterID);
	}

	public User save(User user) {
		System.out.println("In UserService");
		User userTb =userRepository.save(user);
		
		return userTb;
	}
	
	public UserCenter saveUserCenter(UserCenter userCenter) {
		System.out.println("In UserService save userCenter");
		return userCenterRepository.save(userCenter);
	}
	public Address saveUserAddress(Address userAddress) {
		System.out.println("In UserService save userCenter");
		Address a = userAddressRepository.save(userAddress);
		return a;
	}

	public User save(User user, UserSession userSession, Integer notiNewUser, String notiNewUserEmail,
			HttpServletRequest request, HttpServletResponse response, Locale locale) throws Exception {

		checkAdminRole(user, userSession);

		NotiNewUserModel notiNewUserModel = new NotiNewUserModel();
		notiNewUserModel.setUserName(user.getUserName());
		notiNewUserModel.setPassword(user.getPassword());

		UserAccessPermission userAccessPermission = null;
		UserInfo userInfo = null;
		User userForCheck = null;
		if (user.getUserId() == 0) {
//			user.setCreateDate(Calendar.getInstance().getTime());
//			user.setUserByCreateBy(userSession.getUser());
			user.setPassword(BCrypt.hashpw(user.getPassword(), BCrypt.gensalt()));
			userAccessPermission = new UserAccessPermission();
//			userAccessPermission.setUser(user);
			userInfo = new UserInfo();
//			userInfo.setUser(user);
		} else {
			User userObj = userRepository.findOne(user.getUserId());
			userForCheck = userObj;
			user.setLastLogin(userObj.getLastLogin());
			user.setFailedLoginAttempt(userObj.getFailedLoginAttempt());
//			user.setCreateDate(userObj.getCreateDate());
//			user.setUserByCreateBy(userObj.getUserByCreateBy());
//			user.setPassword(userObj.getPassword());
//			userAccessPermission = userObj.getUserAccessPermission();
//			userInfo = userObj.getUserInfo();
		}

//		if (!identificationCardService.checkIdentificationCardNo(user.getUserInfo().getCitizenNo())
//				|| !checkUsername(user.getUserName(), userForCheck)) {
//			throw new Exception();
//		}

//		userAccessPermission.setMUserAccessPermission(
//				(MUserAccessPermission) catalogUtil.getObject(EPaymentConstant.M_USER_ACCESS_PERMISSION,
//						user.getUserAccessPermission().getMUserAccessPermission().getAccessPermissionId()));

//		if (user.getUserAccessPermission().getMUserAccessPermission().getAccessPermissionId() == 3) {
//			List<Organization> organizationList = new ArrayList<Organization>();
//			for (Organization orgazanition : user.getUserAccessPermission().getOrganizations()) {
//				organizationList.add(orgazanition);
//			}
//			userAccessPermission.setOrganizations(organizationList);
//		} else {
//			userAccessPermission.setOrganizations(null);
//		}
//
//		userInfo.setMTitleName((MTitleName) catalogUtil.getObject(EPaymentConstant.M_TITLE_NAME,
//				user.getUserInfo().getMTitleName().getTitleId()));
//		userInfo.setOrganization(organizationService.findOne(user.getUserInfo().getOrganization().getOrgId()));
//		userInfo.setUserInfo(user.getUserInfo().getUserInfo());
//		userInfo.setTitleOther(user.getUserInfo().getTitleOther());
//		userInfo.setFirstName(user.getUserInfo().getFirstName());
//		userInfo.setLastName(user.getUserInfo().getLastName());
//		userInfo.setPhoneNo(user.getUserInfo().getPhoneNo());
//		userInfo.setMobileNo(user.getUserInfo().getMobileNo());
//		userInfo.setEmail(user.getUserName());
//		userInfo.setPosition(user.getUserInfo().getPosition());
//		userInfo.setCitizenNo(user.getUserInfo().getCitizenNo());
//
//		List<PermissionGroup> permissionGroupList = new ArrayList<PermissionGroup>();
//		if (user.getPermissionGroups() != null) {
//			for (PermissionGroup permissionGroup : user.getPermissionGroups()) {
//				permissionGroupList.add(permissionGroupService.findOne(permissionGroup.getPermissionGroupId()));
//			}
//		}
//
//		user.setPermissionGroups(permissionGroupList);
//		user.setUserInfo(userInfo);
//		user.setUserAccessPermission(userAccessPermission);
//		user.setUpdateDate(Calendar.getInstance().getTime());
//		user.setUserByUpdateBy(userSession.getUser());
		User userSave = userRepository.saveAndFlush(user);

		// set fullName for Email
//		notiNewUserModel.setFullName(userSave.getFullName());

		if (notiNewUser != null && (notiNewUser.equals(NOTI_NEW_TO_USER) || notiNewUser.equals(NOTI_NEW_TO_OTHER))) {
			if (notiNewUser.equals(NOTI_NEW_TO_USER)) {
//				notiNewUserModel.setSendTo(userSave.getUserInfo().getEmail());
			} else if (notiNewUser.equals(NOTI_NEW_TO_OTHER)) {
				notiNewUserModel.setSendTo(notiNewUserEmail);
			}
			// mailService.sendEmailNotiNewUser(request, response, userSave,
			// notiNewUserModel, locale);
		}
		return userSave;
	}

	private void checkAdminRole(User user, UserSession userSession) throws Exception {

		if (!userSession.getHasAdminRole()) {
			List<MUserRole> userRoles = new ArrayList<MUserRole>();
			userRoles.add((MUserRole) catalogUtil.getObject(EPaymentConstant.M_USER_ROLE, (short) 1));
//			user.setUserRoles(userRoles);

//			UserAccessPermission userAccessPermission = new UserAccessPermission();
//			userAccessPermission.setMUserAccessPermission(
//					(MUserAccessPermission) catalogUtil.getObject(EPaymentConstant.M_USER_ACCESS_PERMISSION, 1));
//			user.setUserAccessPermission(userAccessPermission);
		}

	}

	public void updateLoginTime(User user) {
		userRepository.save(user);
	}

	// Set user Session
	public void setUserSession(User user, HttpServletRequest request) throws Exception {
		UserSession userSession = new UserSession(user);
		request.getSession().setAttribute(EPaymentConstant.USER_SESSION, userSession);

		setMenus(userSession, request);
		setPermissiosIds(userSession);
		setUserReport(userSession);
		setOrganizations(userSession);
		setAdminRole(userSession);
	}

	private void setMenus(UserSession userSession, HttpServletRequest request) {
		if (null != userSession.getMenus()) {
			request.getSession().setAttribute(EPaymentConstant.MENU, userSession.getMenus());
		} else {
//			menuService.create(userSession);
			request.getSession().setAttribute(EPaymentConstant.USER_SESSION, userSession);

			if (null != userSession.getMenus()) {
				request.getSession().setAttribute(EPaymentConstant.MENU, userSession.getMenus());
			}
		}
	}

	private void setPermissiosIds(UserSession userSession) {
		List<Short> perms = userSession.getPerms();
		User user = userSession.getUser();

//		for (PermissionItem item : user.getPermissionItems()) {
//			if (!perms.contains(item.getPermissionItemId())) {
//				perms.add(item.getPermissionItemId());
//			}
//
//		}

//		for (PermissionGroup group : user.getPermissionGroups()) {
//			for (PermissionItem item : group.getPermissionItems()) {
//				if (!perms.contains(item.getPermissionItemId())) {
//					perms.add(item.getPermissionItemId());
//				}
//			}
//		}

		userSession.setPerms(perms);
	}

	private void setUserReport(UserSession userSession) {
		List<Short> perms = userSession.getPerms();
		List<MReport> reports = new ArrayList<MReport>();

		User user = userSession.getUser();

//		for (PermissionItem item : user.getPermissionItems()) {
//			if (null != item.getPermission() && null != item.getPermission().getReport()
//					&& item.getPermission().getReport().size() > 0 && perms.contains(item.getPermissionItemId())
//					&& !reports.contains(item.getPermission().getReport())) {
//				MReport report = item.getPermission().getReport().get(0);
//				report.setReportName(item.getPermission().getPermissionName());
//				reports.add(report);
//			}
//		}

//		for (PermissionGroup group : user.getPermissionGroups()) {
//			for (PermissionItem item : group.getPermissionItems()) {
//				if (null != item.getPermission() && null != item.getPermission().getReport()
//						&& item.getPermission().getReport().size() > 0 && perms.contains(item.getPermissionItemId())
//						&& !reports.contains(item.getPermission().getReport())) {
//					MReport report = item.getPermission().getReport().get(0);
//					report.setReportName(item.getPermission().getPermissionName());
//					reports.add(report);
//				}
//			}
//		}

//		List<MReportGroup> reportGroups = new ArrayList<MReportGroup>();
//		for (MReport report : reports) {
//			MReportGroup reportGroup = report.getReportGroup();
//			if (!reportGroups.contains(reportGroup)) {
//				reportGroup.getReports().clear();
//				reportGroups.add(reportGroup);
//			}
//			reportGroup.getReports().add(report);
//		}

//		Collections.sort(reportGroups, new ReportGroupComparator());
//
//		for (MReportGroup g : reportGroups) {
//			System.out.println("Group : " + g.getReportGroupName());
//			for (MReport r : g.getReports()) {
//				System.out.println("|---" + r.getReportName() + "(" + r.getReportId() + ")");
//			}
//		}

//		userSession.setUserReportGroups(reportGroups);
	}

	public static class ReportGroupComparator implements Comparator<MReportGroup> {
		public int compare(MReportGroup o1, MReportGroup o2) {
			return new Integer(o1.getReportGroupId()).compareTo(o2.getReportGroupId());
		}
	}

	private void setOrganizations(UserSession userSession) throws Exception {
		List<Integer> orgs = userSession.getOrgs();
		User user = userSession.getUser();

//		int accessPermissionId = user.getUserAccessPermission().getMUserAccessPermission().getAccessPermissionId();
//
//		if (accessPermissionId == 1) {
//			orgs.add(user.getUserInfo().getOrganization().getOrgId());
//		} else if (accessPermissionId == 3) {
//			orgs.add(user.getUserInfo().getOrganization().getOrgId());
//			for (Organization org : user.getUserAccessPermission().getOrganizations()) {
//				if (org.getOrgId() != user.getUserInfo().getOrganization().getOrgId()) {
//					orgs.add(org.getOrgId());
//				}
//			}
//		}

//		userSession.setUserOrg(accessPermissionId);
//		userSession.setOrgs(orgs);
	}

	private void setAdminRole(UserSession userSession) {

		boolean hasAdminRole = false;

//		for (MUserRole userRole : userSession.getUser().getUserRoles()) {
//			if (userRole.getRoleId() == 3) {
//				hasAdminRole = true;
//				break;
//			}
//		}

		userSession.setHasAdminRole(hasAdminRole);
	}

	public boolean checkUsername(String userName, User user) {
		User userObj = null;
		if (user != null) {
//			userObj = userRepository.findByUserNameAndUserIdNot(userName, user.getUserId());
		} else {
			List<User> list = userRepository.findByUserName(userName);
			if(list.size() > 0){
				userObj = list.get(0);
			}else {
				userObj = null;
			}
		}
		return (userObj == null);
	}

	public UserRootTreeModel getRootTree(UserSession userSession, Integer orgId) throws Exception {

		Organization organization;

		UserRootTreeModel userRootTreeModel = new UserRootTreeModel();
		UserRootTreeStateModel userRootTreeStateModel = new UserRootTreeStateModel();
		userRootTreeStateModel.setOpened(true);
		userRootTreeStateModel.setSelected(true);
		userRootTreeModel.setRootTree(true);

		if (orgId != null) {
			organization = organizationService.findOne((int) orgId);
		} else {
			User user = userSession.getUser();
//			organization = user.getUserInfo().getOrganization();
		}
//
//		userRootTreeModel.setText(organization.getOrgName());
//		userRootTreeModel.setOrgId(organization.getOrgId());
//		userRootTreeModel.setChildren(userInfoService.findByOrganizationAndUserInfoIsNull(organization).size() > 0);
		userRootTreeModel.setState(userRootTreeStateModel);
		userRootTreeModel.setType(TYPE_ORG);
		return userRootTreeModel;
	}

	public List<UserRootTreeModel> getChildrenTree(Integer orgId, Integer userId) throws Exception {
		List<UserRootTreeModel> userRootTreeModelList = new ArrayList<UserRootTreeModel>();
		List<UserInfo> userInfoList = new ArrayList<UserInfo>();
		if (orgId != null) {
			userInfoList = userInfoService
					.findByOrganizationAndUserInfoIsNull(organizationService.findOne((int) orgId));
		} else if (userId != null) {
//			userInfoList = userInfoService.findByUserInfo(userInfoService.findOne((int) userId));
		}

		for (UserInfo userInfo : userInfoList) {
//			if (!userInfo.getUser().getMUserStatus().getStatus().equals(DELETE)) {
//				UserRootTreeStateModel userRootTreeStateModel = new UserRootTreeStateModel();
//				userRootTreeStateModel.setOpened(false);
//				userRootTreeStateModel.setSelected(false);
//				UserRootTreeModel userRootTreeModel = new UserRootTreeModel();
//
//				userRootTreeModel.setText(userInfo.getUser().getFullName());
//				userRootTreeModel.setRootTree(false);
//				userRootTreeModel.setChildren(checkUserInfos(userInfo.getUserInfos()));
//				userRootTreeModel.setUserId(userInfo.getUserId());
//				userRootTreeModel.setState(userRootTreeStateModel);
//				userRootTreeModel.setType(TYPE_USER);
//				userRootTreeModelList.add(userRootTreeModel);
//			}
		}

		return userRootTreeModelList;
	}

	private boolean checkUserInfos(List<UserInfo> userInfoList) {
		List<UserInfo> userInfoDeleteList = new ArrayList<UserInfo>();
		boolean haveChildren = userInfoList.size() > 0;
		for (UserInfo userInfo : userInfoList) {
//			if (userInfo.getUser().getMUserStatus().getStatus().equals(DELETE)) {
//				userInfoDeleteList.add(userInfo);
//			}
		}

		if (userInfoList.size() == userInfoDeleteList.size()) {
			haveChildren = false;
		}

		return haveChildren;
	}

	public List<UserRootTreeSearchModel> getUserRootTreeSearchModel(int userId) throws Exception {

		// Create List<UserRootTreeSearchModel> by get all user from target
		// search and recursive to root

		List<UserRootTreeSearchModel> userRootTreeSearchModelList = new ArrayList<UserRootTreeSearchModel>();

		List<UserInfo> allUserInfo = new ArrayList<UserInfo>();

		UserInfo userTarget = userInfoService.findOne((int) userId);

		List<UserInfo> userTargetFamily = getListOfUserTarget(userTarget);

		UserRootTreeSearchModel rootOfSearch = createRootOfSearch(userTarget);

//		List<UserInfo> childrenOfRoot = userInfoService
//				.findByOrganizationAndUserInfoIsNull(userTarget.getOrganization());

//		for (UserInfo uI : childrenOfRoot) {
//			allUserInfo.add(uI);
//		}

//		for (UserInfo userInfo : userTargetFamily) {
//			if (userInfo.getUserInfos().size() != 0
//					&& userInfo.getUserId() != userTargetFamily.get(userTargetFamily.size() - 1).getUserId()) {
//				for (UserInfo uI : userInfo.getUserInfos()) {
//					if (!uI.getUser().getMUserStatus().getStatus().equals(DELETE)) {
//						allUserInfo.add(uI);
//					}
//				}
//			}
//		}

		userRootTreeSearchModelList.add(rootOfSearch);

		for (UserInfo ui : allUserInfo) {
			userRootTreeSearchModelList.add(createChildrenOfRoot(ui, userTargetFamily));
		}
		return userRootTreeSearchModelList;
	}

	private List<UserInfo> getListOfUserTarget(UserInfo userTarget) {
		List<UserInfo> userTargetFamily = new ArrayList<UserInfo>();
		userTargetFamily.add(userTarget);

//		while (userTarget.getUserInfo() != null) {
//			userTarget = userTarget.getUserInfo();
//			userTargetFamily.add(userTarget);
//		}

		Collections.reverse(userTargetFamily);

		return userTargetFamily;
	}

	private UserRootTreeSearchModel createRootOfSearch(UserInfo userTarget) {
		UserRootTreeSearchModel rootOfSearch = new UserRootTreeSearchModel();
		UserRootTreeStateModel stateOfSearch = new UserRootTreeStateModel();
		stateOfSearch.setOpened(true);
		stateOfSearch.setSelected(false);
		rootOfSearch.setId("root");
		rootOfSearch.setParent("#");
		rootOfSearch.setRootTree(true);
		rootOfSearch.setState(stateOfSearch);
//		rootOfSearch.setText(userTarget.getOrganization().getOrgName());
//		rootOfSearch.setOrgId(userTarget.getOrganization().getOrgId());
		rootOfSearch.setType(TYPE_ORG);
		return rootOfSearch;
	}

	private UserRootTreeSearchModel createChildrenOfRoot(UserInfo userInfo, List<UserInfo> userInfoList) {
		UserRootTreeSearchModel userRootTreeSearchModel = new UserRootTreeSearchModel();

		UserRootTreeStateModel stateOfSearch = new UserRootTreeStateModel();
		stateOfSearch.setOpened(userInfoList.contains(userInfo)
				&& userInfo.getUserId() != userInfoList.get(userInfoList.size() - 1).getUserId());
		stateOfSearch.setSelected(userInfo.getUserId() == userInfoList.get(userInfoList.size() - 1).getUserId());

//		if (userInfo.getUserInfo() == null) {
//			userRootTreeSearchModel.setParent("root");
//		} else {
//			userRootTreeSearchModel.setParent(Integer.toString(userInfo.getUserInfo().getUserId()));
//		}

		userRootTreeSearchModel.setId(Integer.toString(userInfo.getUserId()));
		userRootTreeSearchModel.setState(stateOfSearch);
//		if ((!userInfoList.contains(userInfo) && userInfo.getUserInfos().size() > 0
//				&& checkUserInfos(userInfo.getUserInfos()))
//				|| (userInfo.getUserId() == userInfoList.get(userInfoList.size() - 1).getUserId()
//						&& userInfo.getUserInfos().size() > 0 && checkUserInfos(userInfo.getUserInfos()))) {
//			userRootTreeSearchModel.setChildren(true);
//		}

//		userRootTreeSearchModel.setText(userInfo.getUser().getFullName());
		userRootTreeSearchModel.setRootTree(false);
		userRootTreeSearchModel.setType(TYPE_USER);
		userRootTreeSearchModel.setUserId(userInfo.getUserId());

		return userRootTreeSearchModel;

	}

//	public void moveInTree(int selfId, Integer userId, Integer orgId) {
//		UserInfo userInfo = userInfoService.findOne(selfId);
//		if (userId != null) {
//			userInfo.setUserInfo(userInfoService.findOne((int) userId));
//		} else {
//			userInfo.setUserInfo(null);
//		}
//		userInfoService.save(userInfo);
//	}

	public void moveOrganization(User user, int orgId) throws Exception {
		UserInfo userInfo = userInfoService.findOne(user.getUserId());
//		if (userInfo.getUserInfos().size() == 0) {
//			userInfo.setOrganization(organizationService.findOne(orgId));
//			userInfo.setUserInfo(null);
//			userInfoService.save(userInfo);
//		}
	}

	public boolean forceChangePassword(User user, UserSession userSession, String newPassword) throws Exception {
		boolean checkPassword = checkPassword(newPassword);
		if (checkPassword) {
			User userObj = userRepository.findOne(user.getUserId());
			userObj.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
//			userObj.setUserByUpdateBy(userSession.getUser());
//			userObj.setUpdateDate(Calendar.getInstance().getTime());
			userRepository.save(userObj);
		}
		return checkPassword;
	}

	public boolean checkPassword(String password) {
		boolean cc = charCasePattern.matcher(password).find();
		boolean n = numberPattern.matcher(password).find();
		boolean mc = matchCharPattern.matcher(password).find();
		return password.length() >= passwordConfigModel.minLength && cc && n && mc
				&& password.length() <= passwordConfigModel.maxLength;
	}

//	public boolean delete(User user, UserSession userSession) throws Exception {
//		User userObj = userRepository.findOne(user.getUserId());
//		boolean canDelete = userObj.getUserInfo().getUserInfos().size() == 0;
//		if (canDelete) {
//			userObj.getUserInfo().setUserInfo(null);
//			userObj.setMUserStatus((MUserStatus) catalogUtil.getObject(EPaymentConstant.M_USER_STATUS, DELETE));
//			userObj.setUserByUpdateBy(userSession.getUser());
//			userObj.setUpdateDate(Calendar.getInstance().getTime());
//			userRepository.save(userObj);
//		}
//		return canDelete;
//	}

	public String getPassword() {
		String password = "";
		do {
			password = generatePassword();
		} while (!checkPassword(password));
		return password;
	}

	private String generatePassword() {
		String allowedChars = passwordConfigModel.allowChar;
		Random random = new Random();
		int length = random.nextInt(passwordConfigModel.minLength - passwordConfigModel.minLength + 1)
				+ passwordConfigModel.minLength;
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < length; i++) {
			sb.append(allowedChars.charAt(random.nextInt(allowedChars.length())));
		}
		return sb.toString();
	}

//	public void recover(int userId, UserSession userSession) throws Exception {
//		User user = userRepository.findOne(userId);
//		user.setMUserStatus((MUserStatus) catalogUtil.getObject(EPaymentConstant.M_USER_STATUS, ACTIVE));
//		user.setUpdateDate(Calendar.getInstance().getTime());
//		user.setUserByUpdateBy(userSession.getUser());
//		userRepository.save(user);
//	}

//	public void updateProfile(User user, UserSession userSession) throws Exception {
//
//		User userObj = userRepository.findOne(userSession.getUser().getUserId());
//		UserInfo userInfoObj = userObj.getUserInfo();
//		userInfoObj.setMTitleName((MTitleName) catalogUtil.getObject(EPaymentConstant.M_TITLE_NAME,
//				user.getUserInfo().getMTitleName().getTitleId()));
//		if (user.getUserInfo().getMTitleName().getTitleId() == 99) {
//			userInfoObj.setTitleOther(user.getUserInfo().getTitleOther());
//		}
//		userInfoObj.setFirstName(user.getUserInfo().getFirstName());
//		userInfoObj.setLastName(user.getUserInfo().getLastName());
//		userInfoObj.setPhoneNo(user.getUserInfo().getPhoneNo());
//		userInfoObj.setMobileNo(user.getUserInfo().getMobileNo());
//		userInfoObj.setPosition(user.getUserInfo().getPosition());
//		userObj.setUserInfo(userInfoObj);
//		userObj.setUpdateDate(Calendar.getInstance().getTime());
//		userRepository.save(userObj);
//	}
	
//	public boolean changePassword(String oldPassword, String newPassword, UserSession userSession) throws Exception {
//		User user = userRepository.findOne(userSession.getUser().getUserId());
//		boolean checkPw = BCrypt.checkpw(oldPassword, user.getPassword());
//		if (checkPw) {
//			user.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));
//			user.setUpdateDate(Calendar.getInstance().getTime());
//			user.setUserByUpdateBy(user);
//			userRepository.save(user);
//		}
//		return checkPw;
//		
//	}
	public User findByUserNameByStutas(String userName) {
		List<User> list = userRepository.findByUserNameByStutas(userName);
		if(list.size() > 0){
			return list.get(0);
		}else{
			return null;
		}
	}
	public void updateUserStatus(User user) {
		System.out.println("updateUserStatus");
		Date date = new Date();
		user.setStatus('1');
		user.setUpdatedDate(date);
		user.setUpdatedBy(user);
		user = userRepository.save(user);
		   logger.info(">>>>>>>> username <<<<<<<<<<"+    user.getStatus());
	}
}