package th.go.cgd.epayment.service.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import th.go.cgd.epayment.entity.MPermissiongroupProgram;
import th.go.cgd.epayment.entity.MStatus;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.repository.MPermissionGroupProgramRepository;
import th.go.cgd.epayment.repository.MStatusRepository;
import th.go.cgd.epayment.repository.UserRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class MPermissionGroupProgramService {

    @Autowired
    private MPermissionGroupProgramRepository permissionGroupProgramRepository;

    @Autowired
    private MStatusRepository statusRepository;

    @Autowired
    private UserRepository userRepository;

    public List<Integer> findActiveProgramIdByPermissionGroupId(int id){

        return permissionGroupProgramRepository.findByPermissionGroupIdActiveAscAsInt(id);
    }

    public boolean update(int id,List<Integer> list,int userId){

        User user = userRepository.findOne(userId);

        List<MPermissiongroupProgram> in = permissionGroupProgramRepository.findByPermissionGroupIdAsc(id);


        MStatus active = statusRepository.getOne("1");
        MStatus nonActive = statusRepository.getOne("2");

        ArrayList<Integer> tempList = new ArrayList<Integer>();
        for (int i = 0; i < in.size(); i++){
            if(list.contains(in.get(i).getProgramId())){
                in.get(i).setStatus(active);
            }else{
                in.get(i).setStatus(nonActive);
            }
            tempList.add(in.get(i).getProgramId());
        }

        for(int i = 0; i < list.size(); i++){
            if(tempList.contains(list.get(i))){
                list.remove(i);
                i--;
            }
        }

        for (int i = 0 ; i < list.size(); i++){
            MPermissiongroupProgram map = new MPermissiongroupProgram();
            map.setStatus(active);
            map.setProgramId(list.get(i));
            map.setMPermissionGroupId(id);
            map.setCreateBy(user);
            map.setCreateDate(new Date());
            map.setUpdateBy(user);
            map.setUpdateDate(new Date());
            in.add(map);
        }

        boolean flag = false;
        try{
            permissionGroupProgramRepository.save(in);
            flag = true;
        }catch (Exception e){
            System.out.println(e.getMessage());
            flag = false;
        }
        return flag;
    }

}
