package th.go.cgd.epayment.service.admin;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import th.go.cgd.epayment.entity.MPermissionGroup;
import th.go.cgd.epayment.entity.MPermissiongroupProgram;
import th.go.cgd.epayment.entity.PermissionGroup;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.MPermissionGroupProgramRepository;
import th.go.cgd.epayment.repository.MPermissionGroupRepository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
@Transactional
public class MPermissionGroupService {


    @Autowired
    private EntityManager entityManager;

    @Autowired
    private MPermissionGroupRepository permissionGroupRepository;

    @Autowired
    private MPermissionGroupProgramRepository permissionGroupProgramRepository;

    public List<MPermissionGroup> findAll() throws Exception {
        List<MPermissionGroup> list = permissionGroupRepository.findAll();
        if (null == list){
            return new ArrayList<MPermissionGroup>();
        }else{
            return permissionGroupRepository.findAll();
        }
    }

    public List<MPermissionGroup> search(String code,String name,String desc,String status) throws Exception {

    	System.out.println("code:"+code);
    	System.out.println("name:"+name);
    	System.out.println("desc:"+desc);
    	System.out.println("status:"+status);
    	
        StringBuilder str = new StringBuilder("SELECT ");
        str.append(" m FROM MPermissionGroup m");

        if(null != StringUtils.trimToNull(code) || null != StringUtils.trimToNull(name) || null != StringUtils.trimToNull(desc) || null != StringUtils.trimToNull(status)){
            str.append(" WHERE ");
        }
        if(null != StringUtils.trimToNull(code) || null != StringUtils.trimToNull(name) || null != StringUtils.trimToNull(desc)){
            str.append("(");
            if(null != StringUtils.trimToNull(code)){
                str.append(" m.mPermissionGroupCode LIKE '%"+code+"%' ");
            }
            if(null != StringUtils.trimToNull(name)){
                if(null != StringUtils.trimToNull(code)){
                    str.append(" OR ");
                }
                str.append(" m.mPermissionGroupName LIKE '%"+name+"%' ");
            }
            if(null != StringUtils.trimToNull(desc)){
                if(null != StringUtils.trimToNull(code) || null != StringUtils.trimToNull(name)){
                    str.append(" OR ");
                }
                str.append(" m.description LIKE '%"+desc+"%') ");
            }
            str.append(")");
        }
        if(null != StringUtils.trimToNull(status)){
            if(null != StringUtils.trimToNull(code) || null != StringUtils.trimToNull(name) || null != StringUtils.trimToNull(desc)){
                str.append(" AND ");
            }
            str.append(" m.status.status = "+status);
        }


        List<MPermissionGroup> list = entityManager.createQuery(str.toString(),MPermissionGroup.class).getResultList();
        return list;
    }

    public MPermissionGroup findOne(int id) throws Exception {
        return permissionGroupRepository.findOne(id);
    }

    public MPermissionGroup findByPermissionGroupName(String permissionGroupName) {
        return permissionGroupRepository.findByPermissionGroupName(permissionGroupName);
    }

    public MPermissionGroup findByPermissionGroupnameAndPermissionGroupIdNot(String permissionGroupName, int permissionGroupId) {
        return permissionGroupRepository.findByPermissionGroupNameAndPermissionGroupIdNot(permissionGroupName, permissionGroupId);
    }

    public MPermissionGroup save(MPermissionGroup permissionGroup) throws Exception {
        if (null == permissionGroup.getMPermissionGroupId()) {
//			permissionGroup.setUserByCreateBy(userSession.getUser());
            permissionGroup.setCreateDate(Calendar.getInstance().getTime());
        } else {
            MPermissionGroup permissionGroupOld = permissionGroupRepository.findOne(permissionGroup.getMPermissionGroupId());
//			permissionGroup.setUserByCreateBy(permissionGroupOld.getUserByCreateBy());
            permissionGroup.setCreateDate(permissionGroupOld.getCreateDate());
        }
//		permissionGroup.setUserByUpdateBy(userSession.getUser());
        permissionGroup.setUpdateDate(Calendar.getInstance().getTime());

        System.out.println(permissionGroup);

        return permissionGroupRepository.save(permissionGroup);
    }

    public boolean delete(int id) throws Exception {
        boolean canDelete = (permissionGroupRepository.checkPermissionGroupCanDelete(id) == 0);
        if (canDelete) {
            List<MPermissiongroupProgram> list = permissionGroupProgramRepository.findByPermissionGroupIdAsc(id);
            permissionGroupProgramRepository.delete(list);
            permissionGroupRepository.delete(permissionGroupRepository.findOne(id));
        }

        return canDelete;
    }
    
	public List<DropdownBean> getPermissionGroupAll(){
	    List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	    List<MPermissionGroup> permissionGroupList = permissionGroupRepository.findAll();
	    for(MPermissionGroup permissionGroupTb : permissionGroupList){
		  DropdownBean dropdownBean = new DropdownBean();
	     	  dropdownBean.setId(permissionGroupTb.getMPermissionGroupId().toString());
	     	  dropdownBean.setDesc((permissionGroupTb.getMPermissionGroupCode() != null ? permissionGroupTb.getMPermissionGroupCode() : "-")+" "+(permissionGroupTb.getMPermissionGroupName() != null ? permissionGroupTb.getMPermissionGroupName() : "-"));
	     	  dropdownBeanList.add(dropdownBean);
	    }
	    return dropdownBeanList;
	}
	
	public  List<MPermissionGroup> getPermissionGroupById(List<Integer> ids){
	    List<MPermissionGroup> permissionGroupList = permissionGroupRepository.findAll(ids);
	    return permissionGroupList;
	}
}

