package th.go.cgd.epayment.service.admin;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.Organization;
import th.go.cgd.epayment.model.OrganizationDataModel;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.OrganizationRepository;
import th.go.cgd.epayment.repository.SimpleDataTableRepository;
import th.go.cgd.epayment.util.SqlWhereCauseBuilder;

@Service
@Transactional
public class OrganizationService {
	@Autowired
	private OrganizationRepository organizationRepository;
	
	@Autowired
	private SimpleDataTableRepository dataTableRepository;
	
	public List<Organization> findAll() throws Exception{
		return organizationRepository.findAll();
	}
	
	public Organization findOne(int id) throws Exception{
		return organizationRepository.findOne(id);
	}
	
//	public Organization findByOrgName(String orgName) {
//		return organizationRepository.findByOrgName(orgName);
//	}
	
//	public Organization findByOrgNameAndOrgIdNot(String orgName, int orgId) {
//		return organizationRepository.findByOrgNameAndOrgIdNot(orgName, orgId);
//	}
	
	public void save(Organization organization, UserSession userSession) throws Exception {
		if (organization.getOrgId() == 0) {
			organization.setCreateDate(Calendar.getInstance().getTime());
//			organization.setUserByCreateBy(userSession.getUser());
		} else {
			Organization orgObj = organizationRepository.findOne(organization.getOrgId());
			organization.setCreateDate(orgObj.getCreateDate());
//			organization.setUserByCreateBy(orgObj.getUserByCreateBy());
		}
		organization.setUpdateDate(Calendar.getInstance().getTime());
//		organization.setUserByUpdateBy(userSession.getUser());
		
//		if (organization.getMProvince() == null || organization.getMProvince().getProvinceId() == -1) {
//			organization.setMProvince(null);
//		}
		
//		if (organization.getOrganization() == null || organization.getOrganization().getOrgId() <= 0) {
//			organization.setOrganization(null);
//		}
		
		organizationRepository.save(organization);
	}
	
	public void delete(List<Organization> organizationList) throws Exception {
		organizationRepository.delete(organizationList);
	}
	
	public boolean move(Organization organization, List<Organization> organizationList) throws Exception {
		boolean canMove = false;
		List<Integer> orgIdList = new ArrayList<Integer>();
		if (organization == null) {
			canMove = true;
		} else {
			for (Organization org : organizationList) {
				orgIdList.add(org.getOrgId());
			}
			canMove = checkOrganizationParent(organization, orgIdList);
		}
		
		
		if (canMove) {			
			for (Organization org : organizationList) {
//				org.setOrganization(organization);
				organizationRepository.saveAndFlush(org);
			}
		}
		
		return canMove;
	}
	
	private boolean checkOrganizationParent(Organization organization ,List<Integer> orgIdList) {
		boolean canMove = true;
		List<Integer> orgIdParentList = new ArrayList<Integer>();
		orgIdParentList.add(organization.getOrgId());
		
//		while (organization.getOrganization() != null) {
//			organization = organization.getOrganization();
//			orgIdParentList.add(organization.getOrgId());
//		}
		
		for (Integer i : orgIdList) {
			if (orgIdParentList.contains(i)) {
				canMove = false;
				break;
			}
		}
		
		return canMove;
	}
	
	public OrganizationDataModel findOrganizationById(Integer orgId)
	{
		SqlWhereCauseBuilder builder = SqlWhereCauseBuilder.getInstance(false);
		String sql = dataTableRepository.getNamedQuery("findOrganizationById");
		builder.append("ORG.ORG_ID = :orgId", "orgId", orgId);
		
		List<OrganizationDataModel> org = dataTableRepository.getList(builder.appendTo(sql), builder.getParameterMap(), OrganizationDataModel.class);
		
		if(null==org || org.size()==0)
		{
			return null;
		}
		return org.get(0);
	}
}
