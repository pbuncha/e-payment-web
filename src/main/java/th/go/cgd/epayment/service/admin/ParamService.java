package th.go.cgd.epayment.service.admin;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.MParam;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.MParamRepository;

@Service
@Transactional
public class ParamService {

	@Autowired
	private MParamRepository mParamRepository;
	
	public List<MParam> findAll() throws Exception{
		return mParamRepository.findAll();
	}
	
	public MParam findByName(String name) {
		return mParamRepository.findByName(name);
	}
	
	public void save(MParam mParam, UserSession userSession) throws Exception {
		
		mParamRepository.save(mParam);
	}
	
}
