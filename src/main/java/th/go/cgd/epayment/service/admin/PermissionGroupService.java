package th.go.cgd.epayment.service.admin;

import java.util.Calendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.PermissionGroup;
import th.go.cgd.epayment.model.UserSession;
import th.go.cgd.epayment.repository.PermissionGroupRepository;

@Service
@Transactional
public class PermissionGroupService {
	
	
	@Autowired
	private PermissionGroupRepository permissionGroupRepository;
	
	public List<PermissionGroup> findAll() throws Exception {
		return permissionGroupRepository.findAll();
	}
	
	public PermissionGroup findOne(short id) throws Exception {
		return permissionGroupRepository.findOne(id);
	}
	
	public PermissionGroup findByPermissionGroupName(String permissionGroupName) {
		return permissionGroupRepository.findByPermissionGroupName(permissionGroupName);
	}
	
	public PermissionGroup findByPermissionGroupnameAndPermissionGroupIdNot(String permissionGroupName, short permissionGroupId) {
		return permissionGroupRepository.findByPermissionGroupNameAndPermissionGroupIdNot(permissionGroupName, permissionGroupId);
	}
	
	public void save(PermissionGroup permissionGroup, UserSession userSession) throws Exception {
		if (permissionGroup.getPermissionGroupId() == 0) {
//			permissionGroup.setUserByCreateBy(userSession.getUser());
			permissionGroup.setCreateDate(Calendar.getInstance().getTime());
		} else {
			PermissionGroup permissionGroupOld = permissionGroupRepository.findOne(permissionGroup.getPermissionGroupId()); 
//			permissionGroup.setUserByCreateBy(permissionGroupOld.getUserByCreateBy());
			permissionGroup.setCreateDate(permissionGroupOld.getCreateDate());
		}
//		permissionGroup.setUserByUpdateBy(userSession.getUser());
		permissionGroup.setUpdateDate(Calendar.getInstance().getTime());
		permissionGroupRepository.save(permissionGroup);		
	}
	
	public boolean delete(short id) throws Exception {
		boolean canDelete = (permissionGroupRepository.checkPermissionGroupCanDelete(id) == 0);
		if (canDelete) {
			permissionGroupRepository.delete(permissionGroupRepository.findOne(id));
		}
		
		return canDelete;
	}
}
