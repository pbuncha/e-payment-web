package th.go.cgd.epayment.service.admin;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.Permission;
import th.go.cgd.epayment.repository.PermissionRepository;

@Service
@Transactional
public class PermissionService {

	@Autowired
	private PermissionRepository permissionRepository;
	
	public List<Permission> findAll() {
		return permissionRepository.findAll();
	}
	
	public List<Permission> findAllByOrderByPermissionIdAsc() {
		return permissionRepository.findAllByOrderByPermissionIdAsc();
	}
	
}
