package th.go.cgd.epayment.service.admin;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.transform.AliasToBeanResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.MPermissionGroup;
import th.go.cgd.epayment.entity.MPermissiongroupProgram;
import th.go.cgd.epayment.entity.MProgram;
import th.go.cgd.epayment.entity.MStatus;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.ButtonAccessBean;
import th.go.cgd.epayment.model.form.ProgramModel;
import th.go.cgd.epayment.repository.MPermissionGroupProgramRepository;
import th.go.cgd.epayment.repository.MPermissionGroupRepository;
import th.go.cgd.epayment.repository.MProgramRepository;
import th.go.cgd.epayment.repository.MStatusRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProgramService {

	private Logger log = Logger.getLogger(ProgramService.class);
	@Autowired
	private MProgramRepository programRepository;

	@Autowired
	private EntityManager entityManager;
	
	@Autowired
	private MPermissionGroupRepository permissionGroupRepository;
	
	@Autowired
	private MPermissionGroupProgramRepository permissionGroupProgramRepository;

	@Autowired
	private MStatusRepository statusRepository;
	
	public List<MProgram> findAll() {
		return programRepository.findAll();
	}

	public MProgram findOne(int id) {
		return programRepository.findOne(id);
	}

	public List<MProgram> findAllByOrderByProgramIdAsc() {
		return programRepository.findAllByOrderByProgramIdAsc();
	}


	public MProgram findByPermissionGroupName(String programName) {
		return programRepository.findByProgramName(programName);
	}

	public MProgram findByPermissionGroupnameAndPermissionGroupIdNot(String programName, int programId) {
		return programRepository.findByProgramNameAndProgramIdNot(programName, programId);
	}


	@SuppressWarnings("unchecked")
	public List<ProgramModel> search(String code,String name,String desc,String status, String groupId ) throws Exception {

		StringBuilder SQL = new StringBuilder( 
				" SELECT T1.PROGRAM_ID \"programId\","+
						"     T1.PROGRAM_CODE \"programCode\","+
						"     T1.PROGRAM_NAME \"programName\","+
						"     T1.DESCRIPTION \"description\","+
						"     T2.STATUS_NAME \"statusName\","+
						"     T3.CANVIEW \"isView\","+
						"     T3.CANEDIT \"isEdit\","+
						"     T3.CANEXCEL \"isExcel\","+
						"     T3.CANPDF \"isPdf\","+
						"	  CAST(T3.M_PERMISSION_GROUP_ID AS VARCHAR(20)) \"permissionGroupId\","+
						" 	  CASE WHEN T3.M_PERMISSIONGROUP_PROGRAM_ID IS NULL THEN 'N' ELSE 'Y' END AS \"isSelected\" "+
						" FROM PAYDB.M_PROGRAM T1 "+
						" INNER JOIN PAYDB.M_STATUS T2 "+
						"     ON T1.STATUS = T2.STATUS "+
						" LEFT JOIN PAYDB.M_PERMISSIONGROUP_PROGRAM T3 "+
						"     ON T1.PROGRAM_ID = T3.PROGRAM_ID AND T3.M_PERMISSION_GROUP_ID = " + groupId );

		if(null != StringUtils.trimToNull(code) || null != StringUtils.trimToNull(name) || null != StringUtils.trimToNull(desc) || null != StringUtils.trimToNull(status)){
			SQL.append(" WHERE ");
		}
		if(null != StringUtils.trimToNull(code) || null != StringUtils.trimToNull(name) || null != StringUtils.trimToNull(desc)){
			SQL.append("(");
			if(null != StringUtils.trimToNull(code)){
				SQL.append(" T1.PROGRAM_CODE LIKE '%"+code+"%' ");
			}
			if(null != StringUtils.trimToNull(name)){
				if(null != StringUtils.trimToNull(code)){
					SQL.append(" OR ");
				}
				SQL.append(" T1.PROGRAM_NAME LIKE '%"+name+"%' ");
			}
			if(null != StringUtils.trimToNull(desc)){
				if(null != StringUtils.trimToNull(code) || null != StringUtils.trimToNull(name)){
					SQL.append(" OR ");
				}
				SQL.append(" T1.DESCRIPTION LIKE '%"+desc+"%') ");
			}
			SQL.append(")");
		}
		if(null != StringUtils.trimToNull(status)){
			if(null != StringUtils.trimToNull(code) || null != StringUtils.trimToNull(name) || null != StringUtils.trimToNull(desc)){
				SQL.append(" AND ");
			}
			SQL.append(" T2.STATUS = "+status);
		}

		List<ProgramModel> results = ((Session)this.entityManager.getDelegate())
				.createSQLQuery(SQL.toString())
				.setResultTransformer(new AliasToBeanResultTransformer(ProgramModel.class))
				.list();
		return   results;
	}

	public void save(MProgram program) throws Exception {
		System.out.println("Program iD : "+program.getProgramId());
		if (null == program.getProgramId() || program.getProgramId() == 0) {
			//			permissionGroup.setUserByCreateBy(userSession.getUser());
			program.setCreateDate(Calendar.getInstance().getTime());
		} else {
			MProgram programOld = programRepository.findOne(program.getProgramId());
			//			permissionGroup.setUserByCreateBy(permissionGroupOld.getUserByCreateBy());
			program.setCreateDate(programOld.getCreateDate());
		}
		//		permissionGroup.setUserByUpdateBy(userSession.getUser());
		program.setUpdateDate(Calendar.getInstance().getTime());
		programRepository.save(program);
	}

	public boolean delete(int id) throws Exception {
		boolean canDelete = (programRepository.checkProgramCanDelete(id) == 0);
		if (canDelete) {
			programRepository.delete(programRepository.findOne(id));
		}

		return canDelete;
	}

	public void savePermissionGroupProgram(Integer permissionGroupId, User createby, List<ProgramModel> programMapping) {
		final Date now = new Date();
		final String ActiveStatusId = EPaymentConstant.STATUS_ID[1];	// index of 1 is active
		
		List<MPermissiongroupProgram>  permissiongroupProgramList = new ArrayList<>();
		MPermissionGroup permissionGroup = permissionGroupRepository.getOne(permissionGroupId);
		MStatus statusActive = statusRepository.getOne(ActiveStatusId);
		
		
		for(ProgramModel mapping : programMapping){
			
			if( mapping.getIsSelected() != 'Y')
				continue;
			
			MPermissiongroupProgram permissionGroupProgram = new MPermissiongroupProgram();
			permissionGroupProgram.setMPermissionGroupId(permissionGroup.getMPermissionGroupId());
			permissionGroupProgram.setProgramId(mapping.getProgramId());
			permissionGroupProgram.setCanedit(mapping.getIsEdit());
			permissionGroupProgram.setCanexcel(mapping.getIsExcel());
			permissionGroupProgram.setCanpdf(mapping.getIsPdf());
			permissionGroupProgram.setCanview(mapping.getIsView());
			permissionGroupProgram.setUpdateDate(now);
			permissionGroupProgram.setUpdateBy(createby);
			permissionGroupProgram.setCreateBy(createby);
			permissionGroupProgram.setCreateDate(now);
			permissionGroupProgram.setStatus(statusActive);
			
			permissiongroupProgramList.add(permissionGroupProgram);
		}
		
		
		/**
		 * Clear all old data from permission groupId.
		 */
		permissionGroupProgramRepository.deleteByPermissionGroupId(permissionGroupId);
		
		/**
		 * Insert new mapping from selected on screen.
		 */
		permissionGroupProgramRepository.save(permissiongroupProgramList);
		
	}
	public ButtonAccessBean getPermissionButtonAccess(String programCode, User userId ){
		
		
		final String SQL = 
				" SELECT " +
				"     T1.PROGRAM_ID," +
				"     T2.PROGRAM_CODE," +
				"     CASE SUM(CASE CANVIEW WHEN 'Y' THEN 1 ELSE 0 END ) WHEN 0 THEN 'N' ELSE 'Y' END CANVIEW," +
				"     CASE SUM(CASE CANEDIT  WHEN 'Y' THEN 1 ELSE 0 END) WHEN 0 THEN 'N' ELSE 'Y' END  CANEDIT," +
				"     CASE SUM(CASE CANEXCEL  WHEN 'Y' THEN 1 ELSE 0 END) WHEN 0 THEN 'N' ELSE 'Y' END   CANEXCEL," +
				"     CASE SUM(CASE CANPDF  WHEN 'Y' THEN 1 ELSE 0 END ) WHEN 0 THEN 'N' ELSE 'Y' END  CANPDF  " +
				" FROM PAYDB.M_PERMISSIONGROUP_PROGRAM T1" +
				" INNER JOIN PAYDB.M_PROGRAM T2 ON T1.PROGRAM_ID = T2.PROGRAM_ID  AND T2.PROGRAM_CODE = :programCode " +
				" WHERE T1.M_PERMISSION_GROUP_ID IN :permissionGroupId" +
				" GROUP BY T1.PROGRAM_ID, T2.PROGRAM_CODE" ;

		ButtonAccessBean buttonAccessBean = new ButtonAccessBean();
		List<Integer> permissionGroupId =  userId.getMUserPermissiongroupList().stream()
											.map( obj -> obj.getMPermissionGroupId().getMPermissionGroupId())
											.collect(Collectors.toList());
		try{
			Object[] result =(Object[]) entityManager.createNativeQuery(SQL)
								.setParameter("programCode", programCode)
								.setParameter("permissionGroupId", permissionGroupId)
								.getSingleResult();
			
			if (result != null) {
				System.out.println(result[0] + " " + result[1] + " " + result[2] + " " + result[3] + " " + result[4] + " " + result[5]);
				if (EPaymentConstant.YES.equalsIgnoreCase( String.valueOf(result[5]) )) {
					buttonAccessBean.setPdf(true);
				}
				if (EPaymentConstant.YES.equalsIgnoreCase( String.valueOf(result[4]) )) {
					buttonAccessBean.setExcel(true);
				}
				if (EPaymentConstant.YES.equalsIgnoreCase( String.valueOf(result[3]) )) {
					buttonAccessBean.setEdit(true);
				}
				if (EPaymentConstant.YES.equalsIgnoreCase( String.valueOf(result[2]) )) {
					buttonAccessBean.setView(true);
				}
			}
		}catch( NoResultException ex){
			log.warn("No entity found for query ProgramPermissionMapping" );
		}
		return buttonAccessBean;
	}
}






