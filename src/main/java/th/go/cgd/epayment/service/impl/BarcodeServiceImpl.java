package th.go.cgd.epayment.service.impl;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Base64;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;

import javax.imageio.ImageIO;


import org.apache.log4j.Logger;
import org.apache.commons.codec.digest.DigestUtils;
import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;
import org.krysalis.barcode4j.tools.UnitConv;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.model.ImageBean;
import th.go.cgd.epayment.service.interfaces.IBarcodeService;

/**
 * @author torgan.p 18 พ.ย. 2560 13:30:19
 *
 */
@Service
public class BarcodeServiceImpl implements IBarcodeService {
	private static final Logger logger = Logger.getLogger(BarcodeServiceImpl.class);

	@Override
	public void generateBarCode(String barcode) {
		logger.info("##### generateBarcode #####");
		try {
			String hashedFileName = getSha1HexFileName();
			String fileType = ".gif";

			// Create the barcode bean
			Code128Bean bean = new Code128Bean();
			final int dpi = 150;
			// Configure the barcode generator
			bean.setModuleWidth(UnitConv.in2mm(1.5f / dpi)); // makes the narrow
			bean.getBarWidth(1);
			bean.setBarHeight(10);
			bean.doQuietZone(false);
			// Open output file
			String classPath = this.getClass().getClassLoader().getResource("").getPath();
			File folder = new File(classPath + "/barcode");
			if (!folder.exists()) {
				folder.mkdir();
			}
			logger.info("classPath : " + classPath);
			logger.info("FileName : " + hashedFileName);
			File outputFile = new File(folder.getAbsolutePath() + "/" + hashedFileName + fileType);
			OutputStream out = new FileOutputStream(outputFile);
			try {
				// Set up the canvas provider for monochrome JPEG output
				BitmapCanvasProvider canvas = new BitmapCanvasProvider(out, "image/gif", dpi,
						BufferedImage.TYPE_BYTE_BINARY, false, 0);
				// char CR = (char) 0x0D;
				// bean.generateBarcode(canvas,
				// "|099400015951001"+CR+"6006017998345410"+CR+"60063036"+CR+"80000");
				bean.generateBarcode(canvas, barcode);
				// Signal end of generation
				canvas.finish();
			} finally {
				out.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void generateQRCode(String barcode) {
		System.out.println("##### generateQRCode #####");
		String hashedFileName = getSha1HexFileName();
		int size = 250;
		String fileType = ".gif";
		String classPath = this.getClass().getClassLoader().getResource("").getPath();
		File folder = new File(classPath + "/barcode");
		if (!folder.exists()) {
			folder.mkdir();
		}

		logger.info("classPath : " + classPath);
		logger.info("FileName : " + hashedFileName);
		File outputFile = new File(folder.getAbsolutePath() + "/" + hashedFileName + fileType);

		try {

			Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
			hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");

			// Now with zxing version 3.2.1 you could change border size (white
			// border size to just 1)
			hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
			hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

			QRCodeWriter qrCodeWriter = new QRCodeWriter();
			BitMatrix byteMatrix = qrCodeWriter.encode(barcode, BarcodeFormat.QR_CODE, size, size, hintMap);
			int CrunchifyWidth = byteMatrix.getWidth();
			BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth, BufferedImage.TYPE_INT_RGB);
			image.createGraphics();

			Graphics2D graphics = (Graphics2D) image.getGraphics();
			graphics.setColor(Color.WHITE);
			graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
			graphics.setColor(Color.BLACK);

			for (int i = 0; i < CrunchifyWidth; i++) {
				for (int j = 0; j < CrunchifyWidth; j++) {
					if (byteMatrix.get(i, j)) {
						graphics.fillRect(i, j, 1, 1);
					}
				}
			}
			ImageIO.write(image, fileType, outputFile);
		} catch (WriterException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("\n\nYou have successfully created QR Code.");
	}

	public ImageBean generateBarcodeTobyte(String barcode) {
		ImageBean imageBean = null;
		ByteArrayOutputStream bao = null;
		try {
			imageBean = new ImageBean();
			String hashedFileName = DigestUtils.sha1Hex("" + (Math.random() * 1000000 + 1) + (new Date()).toString());
			// Create the barcode bean
			Code128Bean bean = new Code128Bean();
			bao = new ByteArrayOutputStream();
			final int dpi = 150;
			// Configure the barcode generator
			bean.setModuleWidth(UnitConv.in2mm(1.5f / dpi)); // makes the narrow
			bean.getBarWidth(1);
			bean.setBarHeight(10);
			bean.doQuietZone(false);
			try {
				// Set up the canvas provider for monochrome JPEG output
				BitmapCanvasProvider canvas = new BitmapCanvasProvider(bao, "image/gif", dpi,
						BufferedImage.TYPE_BYTE_BINARY, false, 0);
				// char CR = (char) 0x0D;
				// bean.generateBarcode(canvas,
				// "|099400015951001"+CR+"6006017998345410"+CR+"60063036"+CR+"80000");
				System.out.println("barcode : "+barcode);
				bean.generateBarcode(canvas, barcode);
				// Signal end of generation
				canvas.finish();

				String base64Encoded = Base64.getEncoder().encodeToString(bao.toByteArray());
				imageBean.setUrl(base64Encoded);
				imageBean.setName(hashedFileName);
			} finally {
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return imageBean;
	}
	
	

	@Override
  public ImageBean getDataQRCodeTobyte(String barcode) {
	  System.out.println("##### generateQRCode #####");
      String hashedFileName = getSha1HexFileName();
      int size = 250;
      ImageBean imageBean = null;
      ByteArrayOutputStream bao = null;

      try {

          Map<EncodeHintType, Object> hintMap = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
          hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
          String fileType = "gif";
          bao = new ByteArrayOutputStream();
          // Now with zxing version 3.2.1 you could change border size (white
          // border size to just 1)
          hintMap.put(EncodeHintType.MARGIN, 1); /* default = 4 */
          hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

          QRCodeWriter qrCodeWriter = new QRCodeWriter();
          BitMatrix byteMatrix = qrCodeWriter.encode(barcode, BarcodeFormat.QR_CODE, size, size, hintMap);
          int CrunchifyWidth = byteMatrix.getWidth();
          BufferedImage image = new BufferedImage(CrunchifyWidth, CrunchifyWidth, BufferedImage.TYPE_INT_RGB);
          String base64Encoded = "";
          image.createGraphics();
          Graphics2D graphics = (Graphics2D) image.getGraphics();
          graphics.setColor(Color.WHITE);
          graphics.fillRect(0, 0, CrunchifyWidth, CrunchifyWidth);
          graphics.setColor(Color.BLACK);

          for (int i = 0; i < CrunchifyWidth; i++) {
              for (int j = 0; j < CrunchifyWidth; j++) {
                  if (byteMatrix.get(i, j)) {
                      graphics.fillRect(i, j, 1, 1);
                  }
              }
          }
          try {
               ImageIO.write(image, fileType, bao);
               base64Encoded = Base64.getEncoder().encodeToString(bao.toByteArray());
            } catch(IOException e) {
                // This *shouldn't* happen with a ByteArrayOutputStream, but if it
                // somehow does happen, then we don't want to just ignore it
                throw new RuntimeException(e);
            }

          imageBean = new ImageBean();
          imageBean.setUrl(base64Encoded);
          imageBean.setName(hashedFileName);
          
      } catch (WriterException e) {
          e.printStackTrace();
      }
      System.out.println("\n\nYou have successfully created QR Code.");
    return imageBean;
  }

  public String getSha1HexFileName() {
		String hashedFileName = DigestUtils.sha1Hex("" + (Math.random() * 1000000 + 1) + (new Date()).toString());
		return hashedFileName;
	}

	@Override
	public String getDataBarCode(Invoice invoice) {

		char CR = (char) 0x0D;
		
		DecimalFormat df = new DecimalFormat("####.00");
		String strTotalAmount = df.format(new BigDecimal(invoice.getTotalAmount()!=null ? invoice.getTotalAmount()+"":"0"));
		strTotalAmount = strTotalAmount.replace(".", "").replace("\u00A0", "");
//		System.out.println(strTotalAmount.replace(".", "").replace("\u00A0", ""));
		
		StringBuffer str = new StringBuffer();
		str.append(EPaymentConstant.BRACODE_DATA.DATA_INVOICE);
		str.append(CR);
		str.append(invoice.getRef1());
		str.append(CR);
		str.append(invoice.getRef2());
		str.append(CR);
		str.append(strTotalAmount);
		return str.toString();
	}

	@Override
	public String getDataQRCode(Invoice invoice) {
	    
		DecimalFormat df = new DecimalFormat("####.00");
		String strTotalAmount = df.format(new BigDecimal(invoice.getTotalAmount()!=null ? invoice.getTotalAmount()+"":"0"));
		strTotalAmount = strTotalAmount.replace(".", "").replace("\u00A0", "");
	    
		char CR = (char) 0x0D;
		StringBuffer str = new StringBuffer();
		str.append(EPaymentConstant.BRACODE_DATA.DATA_INVOICE);
		str.append(CR);
		str.append(invoice.getRef1());
		str.append(CR);
		str.append(invoice.getRef2());
		str.append(CR);
		str.append(strTotalAmount);
		return str.toString();
	}
	
	@Override
	public String getDataQRCode(String ref1,String ref2,BigDecimal totalAmount) {
	    
		DecimalFormat df = new DecimalFormat("####.00");
		String strTotalAmount = df.format(new BigDecimal(totalAmount!=null ? totalAmount+"":"0"));
		strTotalAmount = strTotalAmount.replace(".", "").replace("\u00A0", "");
	    
		char CR = (char) 0x0D;
		StringBuffer str = new StringBuffer();
		str.append(EPaymentConstant.BRACODE_DATA.DATA_INVOICE);
		str.append(CR);
		str.append(ref1);
		str.append(CR);
		str.append(ref2);
		str.append(CR);
		str.append(strTotalAmount);
		return str.toString();
	}
	@Override
	public String getDataQRCode(String ref1,String ref2,String totalAmount) {
	    
		DecimalFormat df = new DecimalFormat("####.00");
		String strTotalAmount = df.format(new BigDecimal(totalAmount!=null && !totalAmount.equals("") ? totalAmount+"":"0"));
		strTotalAmount = strTotalAmount.replace(".", "").replace("\u00A0", "");
	    
		char CR = (char) 0x0D;
		StringBuffer str = new StringBuffer();
		str.append(EPaymentConstant.BRACODE_DATA.DATA_INVOICE);
		str.append(CR);
		str.append(ref1);
		str.append(CR);
		str.append(ref2);
		str.append(CR);
		str.append(strTotalAmount);
		return str.toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * th.go.cgd.epayment.service.interfaces.IBarcodeService#generateBarCode(
	 * java.lang.String)
	 */

}
