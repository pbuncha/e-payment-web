/**
 * 
 */
package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.BatchFileReadResult;
import th.go.cgd.epayment.model.BatchFileReadResultBean;
import th.go.cgd.epayment.repository.BatchFileReadResultRepository;
import th.go.cgd.epayment.service.interfaces.IBatchFileReadResultService;

@Service
@Transactional
public class BatchFileReadResultServiceImpl implements IBatchFileReadResultService {

	private static final Logger log = Logger.getLogger(BatchFileReadResultServiceImpl.class);
	
	@Autowired
	private BatchFileReadResultRepository repository;
	
	@Override
	public List<BatchFileReadResultBean> getAllBatchFileReadResult() {
				
		List<BatchFileReadResultBean> objBeanList = new ArrayList<BatchFileReadResultBean>();
		
		for (BatchFileReadResult objDB : repository.findAll()) {
			
			BatchFileReadResultBean objBean = new BatchFileReadResultBean();
			
			objBean.setBatchFileReadResultId(objDB.getBatchFileReadResultId());

			if (!StringUtils.isEmpty(objDB.getBankId()))
			{
				objBean.setBankId(objDB.getBankId().getBankId());
				objBean.setBankNameTH(objDB.getBankId().getBankNameTh());
			}
			
			objBean.setBatchAllAmount(objDB.getBatchAllAmount());
			objBean.setBatchAllRow(objDB.getBatchAllRow());
			objBean.setBatchCreateDate(objDB.getBatchCreateDate());
			objBean.setBatchFileName(objDB.getBatchFileName());
			objBean.setBatchTypeId(objDB.getBatchTypeId());
			objBean.setCreateBy(objDB.getCreateBy());
			objBean.setCreateDate(objDB.getCreateDate());
			objBean.setStatus(objDB.getStatus());

			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}
	
	@Override
	public List<BatchFileReadResultBean> getBatchFileReadResultByCriteria(BatchFileReadResultBean inputObj) {
		
		List<BatchFileReadResultBean> q =  getAllBatchFileReadResult();

		if (!StringUtils.isEmpty(inputObj.getBankId()))
		{
			q = q.stream()
					.filter(a -> a.getBankId() == inputObj.getBankId())
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getBatchTypeId()))
		{
			q = q.stream()
					.filter(a -> a.getBatchTypeId() == inputObj.getBatchTypeId())
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getBatchCreateDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getBatchCreateDate() != null)
					.filter(a -> a.getBatchCreateDate().getTime() >= inputObj.getBatchCreateDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getBatchCreateDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getBatchCreateDate().getTime() <= inputObj.getBatchCreateDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	
	
	
}