/**
 * 
 */
package th.go.cgd.epayment.service.impl;

import java.util.Map;

import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.report.service.AbstractJasperReport;

/**
 * @author wichuda.k Nov 30, 2017
 *
 */
@Service
@Transactional
public class BillReportServiceImpl extends AbstractJasperReport {

	private static final Logger log = Logger.getLogger(BillReportServiceImpl.class);
	
	public BillReportServiceImpl(@Value("${report.jrxml.bill.path}") String jrxmlName) {
		super(jrxmlName);
	}

	@Override
	protected Map<String, Object> generateParameter(Object parameter) {
		// TODO Auto-generated method stub
		return null;
	}
}