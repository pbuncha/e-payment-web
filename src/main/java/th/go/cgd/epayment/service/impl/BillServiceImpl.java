package th.go.cgd.epayment.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.Address;
import th.go.cgd.epayment.entity.Billing;
import th.go.cgd.epayment.entity.Catalog;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.InvoiceItem;
import th.go.cgd.epayment.entity.MAmphur;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MPaymentChannel;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MTambon;
import th.go.cgd.epayment.entity.MTitleName;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.BillingBean;
import th.go.cgd.epayment.model.CreatedInvoiceBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.InvoiceCodeBean;
import th.go.cgd.epayment.model.InvoiceItemBean;
import th.go.cgd.epayment.model.SaveInvoiceBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.repository.AddressRepository;
import th.go.cgd.epayment.repository.BillingRepository;
import th.go.cgd.epayment.repository.CatalogRepository;
import th.go.cgd.epayment.repository.InvoiceRepository;
import th.go.cgd.epayment.repository.MAmphurRepository;
import th.go.cgd.epayment.repository.MPaymentChannelRepository;
import th.go.cgd.epayment.repository.MProvinceRepository;
import th.go.cgd.epayment.repository.MTambonRepository;
import th.go.cgd.epayment.repository.MTitleNameRepository;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.service.interfaces.IBillService;
import th.go.cgd.epayment.service.interfaces.IGenerateCodeService;
import th.go.cgd.epayment.util.CalendarHelper;

/**
 * @author torgan.p 28 พ.ย. 2560 00:08:06
 *
 */
@Service
@Transactional
public class BillServiceImpl implements IBillService{
	private static final Logger log = Logger.getLogger(InvoiceServiceImpl.class);
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private InvoiceRepository invoiceRepository;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private MPaymentChannelRepository mPaymentChannelRepository;
	@Autowired
	private BillingRepository billingRepository;
	@Autowired
	private MTitleNameRepository titleNameRepository;
	@Autowired
	private MProvinceRepository mProvinceRepository;
	@Autowired
	private MAmphurRepository mAmphurRepository;
	@Autowired
	private MTambonRepository mTambonRepository;
	@Autowired
	private AddressRepository addressRepository;
	@Autowired
	private CatalogRepository catalogRepository;
	@Autowired
	private IGenerateCodeService generateCodeService;


	@Override
	public List<BillingBean> getBillByUser(SearchBean searchBean) {

		//	    List<Billing> billingList = billingRepository.getBillingByInvoidUser(userId);

		//	        sql.append(" select distinct f ");
		//	        sql.append(" from Lg f, IN(f.requestDocId) g , IN(g.loaneeId) h, IN(h.loaneeMappingList) i");
		//	        sql.append(" WHERE (1=1)")
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");

		StringBuffer queryStr = new StringBuffer("select distinct b ");
		queryStr.append(" from Billing  b ");
		queryStr.append(" ,IN(b.invoiceId)  i ");
		queryStr.append(" ,IN(i.invoiceItemList)  it ");
		queryStr.append(" ,IN(it.catalogId) AS c ");
		queryStr.append(" LEFT JOIN c.catalogStructureList cs ");
		queryStr.append(" LEFT JOIN cs.catalogStructureItemList csi ");
		queryStr.append(" WHERE (1=1)");
		//	        AND TO_CHAR(i.startDate,'yyyyMMdd')
		if (searchBean.getStartDate() != null)
			queryStr.append(" AND TO_CHAR(i.startDate,'yyyyMMdd') = :startDate");
		if (searchBean.getEndDate() != null)
			queryStr.append(" AND TO_CHAR(i.endDate,'yyyyMMdd') = :endDate");
		if (searchBean.getDepartmentId() != null)
			queryStr.append(" AND csi.departmentId.departmentId = :departmentId");
		if (searchBean.getCatalogTypeId() != null)
			queryStr.append(" AND c.catalogTypeId.catalogTypeId = :catalogTypeId");
		if (searchBean.getCatalogId() != null)
			queryStr.append(" AND c.catalogId = :catalogId");
		if (searchBean.getCatalogCode() != null)
			queryStr.append(" AND c.catalogCode = :catalogCode");
		if (searchBean.getCatalogName() != null)
			queryStr.append(" AND c.catalogName = :catalogName");
		if (searchBean.getUserId() != null)
			queryStr.append(" AND i.userId.userId = :userId");
		if(!(searchBean.getInvoice().getInvoiceNo()== null))
			queryStr.append(" AND i.invoiceNo LIKE '%"+searchBean.getInvoice().getInvoiceNo()+"%' ");
		if(!(searchBean.getInvoice().getRef1()== null))
			queryStr.append(" AND i.ref1 LIKE '%"+searchBean.getInvoice().getRef1()+"%' ");
		if(!(searchBean.getInvoice().getCitizenNo()== null))
			queryStr.append(" AND i.citizenNo LIKE '%"+searchBean.getInvoice().getCitizenNo()+"%' ");
		if(!(searchBean.getInvoice().getName()== null))
			queryStr.append(" AND (i.firstName LIKE '%"+searchBean.getInvoice().getName()+"%' OR i.middleName LIKE '%"+searchBean.getInvoice().getName()+"%' OR i.lastName LIKE '%"+searchBean.getInvoice().getName()+"%') ");
		if (searchBean.getBilling().getBillingNo() != null)
			queryStr.append(" AND b.billingNo like :billingNo");
		if (searchBean.getStatusUse() != null)
			queryStr.append(" AND b.status = :status");
		if (searchBean.getBillStartDate() != null)
			queryStr.append(" AND TO_CHAR(b.createdDate,'yyyyMMdd') >= '"+dt1.format(searchBean.getBillStartDate())+"'");
		if (searchBean.getBillEndDate() != null)
			queryStr.append(" AND TO_CHAR(b.createdDate,'yyyyMMdd') <= '"+dt1.format(searchBean.getBillEndDate())+"'");
		
		TypedQuery<Billing> query = entityManager.createQuery(queryStr.toString(), Billing.class);
		//
		if (searchBean.getStartDate() != null)
			query.setParameter("startDate", dt1.format(searchBean.getStartDate()));
		if (searchBean.getEndDate() != null)
			query.setParameter("endDate", dt1.format(searchBean.getEndDate()));
		if (searchBean.getDepartmentId() != null)
			query.setParameter("departmentId", searchBean.getDepartmentId());
		if (searchBean.getCatalogTypeId() != null)
			query.setParameter("catalogTypeId", searchBean.getCatalogTypeId());
		if (searchBean.getCatalogId() != null)
			query.setParameter("catalogId", searchBean.getCatalogId());
		if (searchBean.getUserId() != null)
			query.setParameter("userId", searchBean.getUserId());
		if (searchBean.getBilling().getBillingNo() != null)
			query.setParameter("billingNo", searchBean.getBilling().getBillingNo());
		if (searchBean.getCatalogCode() != null)
			query.setParameter("catalogCode", searchBean.getCatalogCode());
		if (searchBean.getCatalogName() != null)
			query.setParameter("catalogName", searchBean.getCatalogName());
		if (searchBean.getStatusUse() != null)
			query.setParameter("status", searchBean.getStatusUse().charAt(0));
		
		List<Billing> billingList = query.getResultList();
		BillingBean bean = null;
		List<BillingBean> billings = new ArrayList<BillingBean>();


		for (Billing billing : billingList) {
			Invoice invoiceTb = billing.getInvoiceId();
			bean = new BillingBean();
			bean.setBillingId(billing.getBillingId());
			bean.setBillingNo(billing.getBillingNo());
			bean.setInvoiceNo(invoiceTb.getInvoiceNo());
			bean.setPrintingCopyDate(billing.getPrintingCopyDate());
			bean.setPrintingDate(billing.getPrintingDate());
			bean.setCreatedDate(billing.getCreatedDate());
			bean.setCreatedDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(billing.getCreatedDate())));
			bean.setTotalAmount(invoiceTb.getTotalAmount());
			bean.setInvoiceId(invoiceTb.getInvoiceId());
			bean.setStatus(String.valueOf(billing.getStatus()));

			InvoiceBean tmp = new InvoiceBean();
			tmp.setId(invoiceTb.getInvoiceId());
			tmp.setInvoiceNo(invoiceTb.getInvoiceNo());
			tmp.setRef1(invoiceTb.getRef1());
			String name = invoiceTb.getFirstName();
			if (!invoiceTb.getLastName().isEmpty()) {
				name += " " + invoiceTb.getLastName();
			}
			if (!(invoiceTb.getTitleId() == null)) {
				name = invoiceTb.getTitleId().getTitleName() + name;
			}
			tmp.setName(name);
			tmp.setCitizenNo(invoiceTb.getCitizenNo());
			tmp.setTotalAmount(invoiceTb.getTotalAmount());
			tmp.setStartDate(invoiceTb.getStartDate());
			tmp.setEndDate(invoiceTb.getEndDate());
			tmp.setStartDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(invoiceTb.getStartDate())));
			tmp.setEndDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(invoiceTb.getEndDate())));
			tmp.setStatus(String.valueOf(invoiceTb.getStatus()));
			bean.setInvoice(tmp);
			billings.add(bean);
		}


		return billings;
	}

	@Override
	public boolean saveBill(Map<String, Integer> obj) {
		Integer paymentchanelId = obj.get("chanel").intValue();
		Integer invoiceId = obj.get("invoiceId").intValue();
		boolean result = false;
		Billing billing = new Billing();
		    Date nowDate = new Date();
			InvoiceCodeBean invoice = new InvoiceCodeBean();
			invoice = generateCodeService.generateInvoiceCode(nowDate);
		try {
			User userTb = userRepository.findOne(1);
			Invoice invoiceTb = invoiceRepository.findOne(invoiceId);
			MPaymentChannel mPaymentChannelTb =  mPaymentChannelRepository.findOne(paymentchanelId);
			invoiceTb.setPaymentDate(new Date());
			invoiceTb.setPaymentChannelId(mPaymentChannelTb);
			//Save Billing

			String costCenterCode = "";
					
			if(invoiceTb.getInvoiceItemList().size()>0){
				Catalog cat = invoiceTb.getInvoiceItemList().get(0).getCatalogId();
				if(cat!=null){
					
					if(cat.getCatalogStructureList().size()>0){
						if(cat.getCatalogStructureList().get(0).getCatalogStructureItemList().size()>0){
							MCostCenter cost = cat.getCatalogStructureList().get(0).getCatalogStructureItemList().get(0).getCostCenterId();
							if(cost!=null){
								costCenterCode = cost.getCostCenterCode();
							}
						}
					}
				}
			}			
			
			SimpleDateFormat dt1 = new SimpleDateFormat("MM");
			int year = new Date().getYear()+543;
			
			String yearStr = String.valueOf(year).substring(1, 3);
			String monthStr = dt1.format(new Date());
			
			String billType = "1"; //hardCode
			
//			if(invoiceTb.getPaymentChannelId().getPaymentChannelId()==2){
//				billType = "2";
//			}else{
//				billType = "1";
//			}
			
			String billingNo = billType+"-"+costCenterCode+yearStr+monthStr+generateCodeService.getGenerateCode("BILLING_CODE");
			
			log.info("---------- costCenterCode : "+costCenterCode);
			log.info("---------- yearStr : "+yearStr);
			log.info("---------- monthStr : "+monthStr);
			log.info("---------- billType : "+billType);
			log.info("---------- getGenerateCode : "+generateCodeService.getGenerateCode("BILLING_CODE"));
			log.info("---------- billingNo : "+billingNo);
			
			billing.setBillingNo(billingNo);
			billing.setCreatedBy(userTb);
			billing.setUpdatedBy(userTb);
			billing.setInvoiceId(invoiceTb);
			billing.setSeqNo(0);
			billing.setStatus('0');
			billing.setCreatedDate(new Date());
			billingRepository.save(billing);
			//Save Invoice
			invoiceRepository.saveAndFlush(invoiceTb);
			result = true;
			return result;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public Integer saveFromPayment(SaveInvoiceBean saveInvoiceBean,User user) {
		Invoice invoiceTb = new Invoice();
		Address addressTb = new Address();

		try {
		    Date nowDate = new Date();
			InvoiceCodeBean invoice = new InvoiceCodeBean();
			invoice = generateCodeService.generateInvoiceCode(nowDate);

//			User userTb = userRepository.findOne(1);
			CreatedInvoiceBean beanInvoice = saveInvoiceBean.getInvoice();
			List<InvoiceItemBean> itemBeans = saveInvoiceBean.getInvoiceItemList();
			invoiceTb.setInvoiceNo(invoice.getInvoiceCode());
			invoiceTb.setRef1(invoice.getRef1());
			invoiceTb.setRef2(invoice.getRef2());
			// invoiceTb.setInvoiceId(300);
			invoiceTb.setInvoiceType(EPaymentConstant.INVOICE_STATUS.CONFIRM_PAY.charAt(0));
			invoiceTb.setUserId(user);
    
			invoiceTb.setUserTypeId(String.valueOf(user.getUserTypeId()).charAt(0));
			invoiceTb.setPersonTypeId(user.getUserTypeId());
			invoiceTb.setCitizenNo(beanInvoice.getCitizenid());
			invoiceTb.setFirstName(beanInvoice.getFirstName());
			// invoiceTb.setMiddleName();
			invoiceTb.setLastName(beanInvoice.getLastName());
			invoiceTb.setTelephone(beanInvoice.getTelephone());
			invoiceTb.setEmail(beanInvoice.getEmail());
			MTitleName titleNameTb = titleNameRepository.findOne(1);
			invoiceTb.setTitleId(titleNameTb);
			
			// Address
			MProvince province = mProvinceRepository.findOne(beanInvoice.getProvinceId());
			addressTb.setProvinceId(province);
			MAmphur amphur = mAmphurRepository.findOne(beanInvoice.getDistrictId());
			addressTb.setAmphurId(amphur);
			MTambon tambon = mTambonRepository.findOne(beanInvoice.getSubdistrictId());
			addressTb.setTambonId(tambon);
			addressTb.setBuildingName(beanInvoice.getBuildingName());
			addressTb.setCreatedDate(nowDate);
			addressTb.setMoo(beanInvoice.getMoo());
			addressTb.setNo(beanInvoice.getNo());
			addressTb.setRoad(beanInvoice.getRoad());
			addressTb.setSoi(beanInvoice.getSoi());
			addressTb.setVillage(beanInvoice.getVillage());
			addressTb.setCreatedBy(user);
			addressTb.setPostcode(beanInvoice.getPostcode());
			invoiceTb.setAddressIdBilling(addressRepository.save(addressTb));

			BigDecimal sumTotalAmount = BigDecimal.ZERO;
			sumTotalAmount = itemBeans.stream().map(InvoiceItemBean::getAmount).reduce(BigDecimal.ZERO,
					BigDecimal::add);

			invoiceTb.setTotalAmount(sumTotalAmount);
			invoiceTb.setStartDate(nowDate);
			invoiceTb.setEndDate(nowDate);
			invoiceTb.setStatus(EPaymentConstant.INVOICE_STATUS.WAIT_PAY.charAt(0));

			// payment
			// invoiceTb.setPaymentChannelId(paymentChannelId);
			// invoiceTb.setPaymentTypeId(paymentTypeId);
			// invoiceTb.setPaymentDate(paymentDate);

			invoiceTb.setCreatedDate(nowDate);
			invoiceTb.setCreatedBy(user);
			invoiceTb.setUpdatedDate(nowDate);
			invoiceTb.setUpdatedBy(user);
			invoiceTb.setPaymentDate(nowDate);

			List<InvoiceItem> invoiceItemList = new ArrayList<InvoiceItem>();
			for (InvoiceItemBean invoiceItemBean : itemBeans) {
				InvoiceItem invoiceItem = new InvoiceItem();
				invoiceItem.setInvoiceId(invoiceTb);

				if (!"".equals(invoiceItemBean.getCatalogId())) {
					Catalog catalogTb = catalogRepository.findOne(Integer.valueOf(invoiceItemBean.getCatalogId()));
					invoiceItem.setCatalogId(catalogTb);
				}

				invoiceItem.setAmount(invoiceItemBean.getAmount());

				invoiceItem.setCreatedDate(nowDate);
				invoiceItem.setCreatedBy(user);
				invoiceItem.setUpdatedDate(nowDate);
				invoiceItem.setUpdatedBy(user);
				invoiceItem.setInvoiceItemName(invoiceItemBean.getCatalogName());
				invoiceItemList.add(invoiceItem);
			}
			invoiceTb.setInvoiceItemList(invoiceItemList);

			Integer invoiceId = invoiceRepository.save(invoiceTb).getInvoiceId().intValue();
			return invoiceId;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.getStackTrace();
			return null;
		}
	}
	@Override
	public Billing getbillingById(Integer id){
	    return billingRepository.findOne(id);
	}
	@Override
	public int updateSeqBilling(Billing billing){
	    int result = 0;
	    Date date = new Date();
	    try{
		    billing.setUpdatedDate(date);
        	    billing = billingRepository.save(billing);
	    }catch(Exception e){
		e.printStackTrace();
	    }
	    return result;
	}
	
}
