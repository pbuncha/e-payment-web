package th.go.cgd.epayment.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserFavorite;
import th.go.cgd.epayment.model.InvoiceItemBean;
import th.go.cgd.epayment.repository.IUserFavoriteRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.service.interfaces.ICatalogService;
import th.go.cgd.epayment.service.interfaces.IUserFavoriteService;

@Transactional
@Service
public class CatalogServiceImpl implements ICatalogService {
	@PersistenceContext
	private EntityManager entityManager;

	@Autowired
	private MDepartmentRepository mDepartmentRepository;

	@Autowired
	private IUserFavoriteRepository iUserFavoriteRepository;

	@Override
	public List<InvoiceItemBean> getCatalogList(InvoiceItemBean itemBean,User  user) {
		
//		StringBuffer queryStr = new StringBuffer(
//				"SELECT c.CATALOG_NAME, c.CATALOG_ID, c.CATALOG_CODE, csi.DEPARTMENT_ID, cct.CONDITION_TEXT, c.Amount");
//		queryStr.append(" FROM PAYDB.CATALOG C ");
//		queryStr.append(" INNER JOIN PAYDB.M_CATALOG_TYPE mc ON c.CATALOG_TYPE_ID = mc.CATALOG_TYPE_ID ");
//		queryStr.append(" LEFT JOIN PAYDB.CATALOG_STRUCTURE cs ON cs.CATALOG_ID = C.CATALOG_ID ");
//		queryStr.append(
//				" LEFT JOIN PAYDB.CATALOG_STRUCTURE_ITEM csi ON csi.CATALOG_STRUCTURE_ID = cs.CATALOG_STRUCTURE_ID  ");
//		queryStr.append(" LEFT JOIN PAYDB.CATALOG_COST_CENTER cc ON cc.CATALOG_ID = c.CATALOG_ID ");
//		queryStr.append(" LEFT JOIN PAYDB.CATALOG_CONDITION_TEXT CCT ON CCT.CATALOG_ID = C.CATALOG_ID ");
//		queryStr.append(" WHERE (1=1) ");
	    List<UserFavorite> favorites = new ArrayList<UserFavorite>();
		StringBuffer queryStr = new StringBuffer();
	queryStr.append(" SELECT c.CATALOG_NAME AS NAME ");
	queryStr.append(" , c.CATALOG_ID AS ID ");
	queryStr.append(" , c.CATALOG_CODE AS CODE ");
	queryStr.append(" , csi.DEPARTMENT_ID AS DEP_ID ");
	queryStr.append(" , cct.CONDITION_TEXT AS CONDI_TEXT ");
	queryStr.append(" , c.Amount AS AMOUNT ");
	queryStr.append(" FROM PAYDB.CATALOG C ");
	queryStr.append(" INNER JOIN PAYDB.M_CATALOG_TYPE mc ON c.CATALOG_TYPE_ID =mc.CATALOG_TYPE_ID ");
	queryStr.append(" LEFT JOIN PAYDB.CATALOG_STRUCTURE cs ON cs.CATALOG_ID = C.CATALOG_ID ");
	queryStr.append(" LEFT JOIN PAYDB.CATALOG_STRUCTURE_ITEM csi ON csi.CATALOG_STRUCTURE_ID = cs.CATALOG_STRUCTURE_ID ");
	queryStr.append(" LEFT JOIN PAYDB.CATALOG_CONDITION_TEXT CCT ON CCT.CATALOG_ID = C.CATALOG_ID ");
	queryStr.append(" WHERE c.IS_SHOW = 'Y' ");
	// WHERE criteria
			if (!(itemBean.getDepartmentId() == null)) {
				queryStr.append(" AND csi.DEPARTMENT_ID = '" + itemBean.getDepartmentId() + "' ");
			}
//			if (!(itemBean.getCatalogId() == null)) {
			if (!(itemBean.getCatalogTypeId() == null)) {
				queryStr.append(" AND mc.CATALOG_TYPE_ID = '" + itemBean.getCatalogTypeId() + "' ");
//				queryStr.append(" AND cs.CATALOG_ID = '" + itemBean.getCatalogId() + "' ");
			}
//			if (!(itemBean.getItemName() == null)) {
			if (!(itemBean.getCatalogId() == null)) {
				queryStr.append(" AND cs.CATALOG_ID = '" + itemBean.getCatalogId()  + "' ");
//				queryStr.append(" AND cs.CATALOG_NAME LIKE '%" + itemBean.getCatalogName() + "%' ");
			}
			
	queryStr.append(" UNION ");
	queryStr.append(" SELECT c.CATALOG_NAME AS NAME ");
	queryStr.append(" ,c.CATALOG_ID AS ID ");
	queryStr.append(" , c.CATALOG_CODE AS CODE ");
	queryStr.append(" ,null AS DEP_ID ");
	queryStr.append(" , '' AS  CONDI_TEXT ");
	queryStr.append(" , c.AMOUNT AS AMOUNT ");
	queryStr.append(" FROM PAYDB.INVOICE_ITEM it ");
	queryStr.append(" INNER JOIN PAYDB.INVOICE i on it.INVOICE_ID = i.INVOICE_ID ");
	queryStr.append(" INNER JOIN PAYDB.CATALOG c ON it.CATALOG_ID = c.CATALOG_ID ");
	queryStr.append(" WHERE (1=1) ");
	if (user != null) {
		queryStr.append(" AND i.USER_ID = '" + user.getUserId() + "' ");
		 favorites = iUserFavoriteRepository.getUserFavoriteByUser(user.getUserId());
	}else{
	    	queryStr.append(" AND i.USER_ID = 0");
	}

		
		Query query = this.entityManager.createNativeQuery(queryStr.toString());
		try {
			List<InvoiceItemBean> invoiceItemBeans = new ArrayList<InvoiceItemBean>();
			List<Object[]> objects = query.getResultList();
			
			for (Object[] obj : objects) {
				InvoiceItemBean bean = new InvoiceItemBean();
				int catalogId = Integer.parseInt(obj[1].toString());
				bean.setCatalogName(obj[0] != null ? obj[0].toString() : "-");
				bean.setCatalogId(catalogId);
				bean.setCatalogCode(obj[2].toString());
				if (obj[3] != null) {
					MDepartment department = mDepartmentRepository.getOne(Integer.parseInt(obj[3].toString()));
					bean.setDepartmentId(Integer.parseInt(obj[3].toString()));
					bean.setDepartmentName(department.getDepartmentName());
				}
				bean.setConditionText(obj[4] != null ? obj[4].toString() : "-");
				bean.setAmount(new BigDecimal(obj[5] != null ? obj[5].toString() : "0"));
				bean.setFavorite(false);
				
				checkFavoriteLoop:
				for (UserFavorite fav : favorites) {
					if (fav.getCatalogId().getCatalogId() == catalogId) {
						bean.setFavorite(true);
						break checkFavoriteLoop;
					}
				}
				invoiceItemBeans.add(bean);

			}
			return invoiceItemBeans;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

}
