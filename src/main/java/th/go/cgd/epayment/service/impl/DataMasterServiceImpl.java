package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.service.interfaces.IDataMasterService;;

@Service
@Transactional
public class DataMasterServiceImpl implements IDataMasterService {
	private static final Logger logger = Logger.getLogger(DataMasterServiceImpl.class);

    @PersistenceContext
    private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<DropdownBean> getOrganizationType() {
		StringBuffer queryStr = new StringBuffer(" SELECT * FROM PAYDB.M_ORGANIZATION_TYPE WHERE 1 = 1 ");
 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();

       Query query = entityManager.createNativeQuery(queryStr.toString());
       
       List<Object[]> objectList = query.getResultList();
       for(Object[] obj:objectList){
     	  DropdownBean dropdownBean = new DropdownBean();
     	  dropdownBean.setId(obj[0].toString());
     	  dropdownBean.setDesc(obj[4].toString());
     	  dropdownBean.setDesc2(obj[4].toString());
     	  dropdownBeanList.add(dropdownBean);
       }
 	return dropdownBeanList;
	}

	@Override
	public List<DropdownBean> getOrganizationSubType(Integer orgTypeId) {
		StringBuffer queryStr = new StringBuffer(" SELECT ORGANIZATION_SUB_TYPE_ID,ORGANIZATION_SUB_TYPE_NAME FROM PAYDB.M_ORGANIZATION_SUB_TYPE WHERE 1 = 1 ");
	 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	 	   if(orgTypeId!=null){
	 	       queryStr.append(" AND ORGANIZATION_TYPE_ID = " + orgTypeId);
	 	   }

	       Query query = entityManager.createNativeQuery(queryStr.toString());
	       
	       List<Object[]> objectList = query.getResultList();
	       for(Object[] obj:objectList){
	     	  DropdownBean dropdownBean = new DropdownBean();
	     	  if(!"4".equals(obj[0].toString())){
	     	  dropdownBean.setId(obj[0].toString());
	     	  dropdownBean.setDesc(obj[1].toString());
	     	  dropdownBean.setDesc2(obj[1].toString());
	     	  dropdownBeanList.add(dropdownBean);
	     	  }
	       }
	 	return dropdownBeanList;
	}
	
	@Override
	public List<DropdownBean> getDepartment(Integer orgSubTypeId) {
		StringBuffer queryStr = new StringBuffer(" SELECT ");
		queryStr.append(" DEPARTMENT_ID ");
		queryStr.append(" ,DEPARTMENT_CODE ");
		queryStr.append(" ,DEPARTMENT_NAME ");
		queryStr.append(" FROM PAYDB.M_DEPARTMENT ");
		queryStr.append(" WHERE 1 = 1 ");
	 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	 	   if(orgSubTypeId!=null){
	 	       queryStr.append(" AND ORGANIZATION_SUB_TYPE_ID = " + orgSubTypeId);
	 	   }

	       Query query = entityManager.createNativeQuery(queryStr.toString());
	       
	       List<Object[]> objectList = query.getResultList();
	       for(Object[] obj:objectList){
	     	  DropdownBean dropdownBean = new DropdownBean();
	     	  dropdownBean.setId(obj[0].toString());
	     	  dropdownBean.setDesc((obj[1] != null ? obj[1].toString()  : "-")+" "+(obj[2] != null ? obj[2].toString()  : "-"));
	     	  dropdownBeanList.add(dropdownBean);
	       }
	 	return dropdownBeanList;
	}

//	@Override
//	public List<DropdownBean> getDepartment(Integer orgSubTypeId) {
//		StringBuffer queryStr = new StringBuffer(" SELECT * FROM PAYDB.M_DEPARTMENT WHERE 1 = 1 ");
//	 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
//	 	   if(orgSubTypeId!=null){
//            queryStr.append(" AND ORGANIZATION_SUB_TYPE_ID = " + orgSubTypeId);
//       }
//
//	       Query query = entityManager.createNativeQuery(queryStr.toString());
//	       
//	       List<Object[]> objectList = query.getResultList();
//	       for(Object[] obj:objectList){
//	     	  DropdownBean dropdownBean = new DropdownBean();
//	     	  dropdownBean.setId(obj[0].toString());
//
//			   if(null != obj[3] && null != obj[4]){
//				   dropdownBean.setDesc(obj[3].toString());
//				   dropdownBean.setDesc2(obj[4].toString());
//				   dropdownBean.setDesc3(obj[3].toString() + " - " + obj[4].toString());
//			   }else{
//				   if( null != obj[3]){
//					   dropdownBean.setDesc(obj[3].toString());
//					   dropdownBean.setDesc3(obj[3].toString() + " - " + "un title");
//				   }
//				   if( null != obj[4]){
//					   dropdownBean.setDesc2(obj[4].toString());
//					   dropdownBean.setDesc3("Unknown" + " - " + obj[4].toString());
//				   }
//			   }
//
//	     	  dropdownBeanList.add(dropdownBean);
//	       }
//	 	return dropdownBeanList;
//	}

	@Override
	public List<DropdownBean> getBusinessArea() {
		StringBuffer queryStr = new StringBuffer(" SELECT * FROM PAYDB.M_BUSINESS_AREA WHERE 1 = 1 ");
	 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();

	       Query query = entityManager.createNativeQuery(queryStr.toString());
	       
	       List<Object[]> objectList = query.getResultList();
	       for(Object[] obj:objectList){
	     	  DropdownBean dropdownBean = new DropdownBean();
	     	  dropdownBean.setId(obj[0].toString());
	     	  dropdownBean.setDesc(obj[1].toString());
	     	  dropdownBean.setDesc2(obj[2].toString());
	     	  dropdownBean.setDesc3(obj[1].toString() + " - " + obj[2].toString());
	     	  dropdownBeanList.add(dropdownBean);
	       }
	 	return dropdownBeanList;
	}

	@Override
	public List<DropdownBean> getBusinessAreaByDepartment(Integer departmentId) {
		StringBuffer queryStr = new StringBuffer(" SELECT DISTINCT(MB.BUSINESS_AREA_ID) ");
		queryStr.append(" ,MB.BUSINESS_AREA_CODE ");
		queryStr.append(" ,MB.BUSINESS_AREA_NAME ");
		queryStr.append(" FROM PAYDB.M_COST_CENTER MC ");
		queryStr.append(" JOIN PAYDB.M_BUSINESS_AREA MB ");
		queryStr.append(" ON MC.BUSINESS_AREA_ID = MB.BUSINESS_AREA_ID ");
		queryStr.append(" WHERE 1 = 1 ");
		
		if(null != departmentId){
			queryStr.append(" AND MC.DEPARTMENT_ID = "+departmentId);
		}
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();

		Query query = entityManager.createNativeQuery(queryStr.toString());

		List<Object[]> objectList = query.getResultList();
		for(Object[] obj:objectList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj[0].toString());
			dropdownBean.setDesc((obj[1] != null ? obj[1].toString()  : "-")+" "+(obj[2] != null ? obj[2].toString()  : "-"));
//			dropdownBean.setDesc2(obj[2].toString());
//			dropdownBean.setDesc3(obj[1].toString() + " - " + obj[2].toString());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}
	@Override
	public List<DropdownBean> getCostCenter(Integer deptId, Integer bizAreaId) {
		StringBuffer queryStr = new StringBuffer(" SELECT CT.COST_CENTER_ID ");
		queryStr.append(" ,CT.COST_CENTER_CODE ");
		queryStr.append(" ,CT.COST_CENTER_NAME ");
		queryStr.append(" FROM PAYDB.M_COST_CENTER CT");
		queryStr.append(" WHERE 1 = 1 ");
		
		
	 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	 	   if(deptId != null){
	 		   queryStr.append(" AND CT.DEPARTMENT_ID = " + deptId);
	 	   }
	 	  if(bizAreaId != null){
	 		   queryStr.append(" AND CT.BUSINESS_AREA_ID = " + bizAreaId);
	 	   }


	       Query query = entityManager.createNativeQuery(queryStr.toString());
	       
	       List<Object[]> objectList = query.getResultList();
	       for(Object[] obj:objectList){
	     	  DropdownBean dropdownBean = new DropdownBean();
	     	  dropdownBean.setId(obj[0].toString());
	     	 dropdownBean.setDesc((obj[1] != null ? obj[1].toString()  : "-")+" "+(obj[2] != null ? obj[2].toString()  : "-"));
//	     	  dropdownBean.setDesc2(obj[3].toString());
//	     	  dropdownBean.setDesc3(obj[2].toString() + " - " + obj[3].toString());
	     	  dropdownBeanList.add(dropdownBean);
	       }
	 	return dropdownBeanList;
	}

	@Override
	public List<DropdownBean> getProvince() {
		StringBuffer queryStr = new StringBuffer(" SELECT PROVINCE_ID, PROVINCE_NAME_TH, PROVINCE_NAME_EN FROM PAYDB.M_PROVINCE WHERE 1 = 1 ");
	 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();

	       Query query = entityManager.createNativeQuery(queryStr.toString());
	       
	       List<Object[]> objectList = query.getResultList();
	       for(Object[] obj:objectList){
	     	  DropdownBean dropdownBean = new DropdownBean();
	     	  dropdownBean.setId(obj[0].toString());
	     	  dropdownBean.setDesc(obj[1].toString());
	     	  dropdownBean.setDesc2(obj[2] != null ? obj[2].toString() : "-");
	     	  //dropdownBean.setDesc3(obj[1].toString() + " - " + obj[2].toString());
	     	  dropdownBeanList.add(dropdownBean);
	       }
	 	return dropdownBeanList;
	}

	@Override
	public List<DropdownBean> getAmphur(Integer provinceId) {
		StringBuffer queryStr = new StringBuffer(" SELECT AMPHUR_ID, AMPHUR_NAME_TH, AMPHUR_NAME_EN FROM PAYDB.M_AMPHUR WHERE 1 = 1 ");
	 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	 	   if(provinceId != null){
	 		   queryStr.append(" AND PROVINCE_ID = " + provinceId);
	 	   }

	       Query query = entityManager.createNativeQuery(queryStr.toString());
	       
	       List<Object[]> objectList = query.getResultList();
	       for(Object[] obj:objectList){
	     	  DropdownBean dropdownBean = new DropdownBean();
	     	  dropdownBean.setId(obj[0].toString());
	     	  dropdownBean.setDesc(obj[1].toString());
		  dropdownBean.setDesc2(obj[2] != null ? obj[2].toString() : "-");
	     	  //dropdownBean.setDesc3(obj[1].toString() + " - " + obj[2].toString());
	     	  dropdownBeanList.add(dropdownBean);
	       }
	 	return dropdownBeanList;
	}

	@Override
	public List<DropdownBean> getTambon(Integer amphurId) {
		StringBuffer queryStr = new StringBuffer(" SELECT TAMBON_ID, TAMBON_NAME_TH, TAMBON_NAME_EN FROM PAYDB.M_TAMBON WHERE 1 = 1 ");
	 	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	 	   if(amphurId != null){
	 		   queryStr.append(" AND AMPHUR_ID = " + amphurId);
	 	   }

	       Query query = entityManager.createNativeQuery(queryStr.toString());
	       
	       List<Object[]> objectList = query.getResultList();
	       for(Object[] obj:objectList){
	     	  DropdownBean dropdownBean = new DropdownBean();
	     	  dropdownBean.setId(obj[0].toString());
	     	  dropdownBean.setDesc(obj[1].toString());
		  dropdownBean.setDesc2(obj[2] != null ? obj[2].toString() : "-");
	     	  //dropdownBean.setDesc3(obj[1].toString() + " - " + obj[2].toString());
	     	  dropdownBeanList.add(dropdownBean);
	       }
	 	return dropdownBeanList;
	}
	
	
}
