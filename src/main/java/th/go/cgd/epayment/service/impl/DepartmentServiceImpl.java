package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.service.interfaces.IDepartmentService;

/**
 * @author torgan.p 18 พ.ย. 2560 22:53:01
 *
 */
@Service
@Transactional
public class DepartmentServiceImpl implements IDepartmentService{
    
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
	private MDepartmentRepository departmentRepository;
    public List<DropdownBean> getDepartmentAll(){
	    TypedQuery<MDepartment> departmentItera = this.entityManager.createNamedQuery("MDepartment.findByStatus",MDepartment.class);
	    departmentItera.setParameter("status", String.valueOf(EPaymentConstant.STATUS_IS_DELETE));
	    
	    List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	    try{
		for(Iterator<MDepartment> iterator = departmentItera.getResultList().iterator(); iterator.hasNext();){
		     DropdownBean dropdownBean = new DropdownBean();
		     MDepartment departmentTb = (MDepartment)iterator.next();
		     dropdownBean.setId(departmentTb.getDepartmentId()+"");
		     dropdownBean.setDesc(departmentTb.getDepartmentCode());
		     dropdownBean.setDesc2(departmentTb.getDepartmentName());
		     dropdownBeanList.add(dropdownBean);
		 }
		     
	    }catch(Exception ex){
		ex.printStackTrace();
	    }
	return dropdownBeanList;
    }

	@Override
	public List<DropdownBean> getMinistryAll() {
	    TypedQuery<MDepartment> departmentItera = this.entityManager.createNamedQuery("MDepartment.findByStatus",MDepartment.class);
	    departmentItera.setParameter("status", String.valueOf(EPaymentConstant.STATUS_IS_DELETE));
	    
	    List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	    try{
		for(Iterator<MDepartment> iterator = departmentItera.getResultList().iterator(); iterator.hasNext();){
		     DropdownBean dropdownBean = new DropdownBean();
		     MDepartment departmentTb = (MDepartment)iterator.next();
//		     dropdownBean.setId(departmentTb.get()+"");
		     dropdownBean.setDesc(departmentTb.getDepartmentCode());
		     dropdownBean.setDesc2(departmentTb.getDepartmentName());
		     dropdownBeanList.add(dropdownBean);
		 }
		     
	    }catch(Exception ex){
		ex.printStackTrace();
	    }
		return null;
	}

}
