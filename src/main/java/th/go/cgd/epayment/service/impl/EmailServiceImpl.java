package th.go.cgd.epayment.service.impl;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.ParsePosition;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.model.AttachmentBean;
import th.go.cgd.epayment.model.EmailBean;
import th.go.cgd.epayment.service.interfaces.IEmailServiceImpl;

/**
 * @author torgan.p 14 ธ.ค. 2560 17:10:13
 *
 */
@Service
@Transactional
public class EmailServiceImpl implements IEmailServiceImpl{
    
	@Autowired
	private JavaMailSender javaMailSender;
	
	public static ResourceBundle bundle = ResourceBundle.getBundle("TemplateEmail");
	
	public boolean sendMail(EmailBean emailBean) throws MessagingException  {
	   
	    MimeMessage message = javaMailSender.createMimeMessage();
	    message.setHeader("Content-type", "text/html; charset=TIS-620");
	    try{
	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
	    helper.setFrom(bundle.getString("mail.From"), bundle.getString("mail.from.name"));
	    helper.setTo(emailBean.getTo()); 
	    helper.setSubject(emailBean.getSubject());
	    StringBuilder mailBody = new StringBuilder();
	    mailBody.append(emailBean.getText());
	    helper.setText(mailBody.toString(), true);
	     List<AttachmentBean> attachments = emailBean.getAttachments();
	     for (AttachmentBean attachment : attachments) {
        	    DataSource source = new ByteArrayDataSource( attachment.getData(), attachment.getMimeType());
        	    helper.addAttachment(attachment.getFilename(), source);
	     }
	    javaMailSender.send(message);
	    }catch(MessagingException ex){
		ex.printStackTrace();
	    }catch(UnsupportedEncodingException ex){
	  	ex.printStackTrace();
	    }
	    
	    return true;
	}
	
	private InputStreamSource convertBase64ToByte(String base64) {
		byte[] imgBytes = Base64.decodeBase64(base64);
		return new ByteArrayResource(imgBytes);
	} 
	private static String encodeFileToBase64Binary(File file) throws IOException {
	    byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
	    return new String(encoded, StandardCharsets.UTF_8);
	}
	

	
	 public String  getTemplateMail(Object[] metaData,String messageKey){
//	        System.out.println("READ PROPERTIS EFILE");

	        String message ="";
	        Enumeration<String> enumeration = bundle.getKeys(); // unsorted enumeration
	        List list= Collections.list(enumeration); // create list from enumeration
	        Collections.sort(list);
	        enumeration = Collections.enumeration(list);
	        StringBuffer content = new StringBuffer();
	        while (enumeration.hasMoreElements()) {
	            String key = enumeration.nextElement();
	            if(messageKey.equals(key.substring(0,key.length()-1))){
         	            String value = bundle.getString(key);
         	            content.append(value);
	            }
	        }
	        MessageFormat mf = new MessageFormat(content.toString());
	        message = mf.format(metaData);
	        System.out.println("message : " +  message);

	      return  message;
	    }
	
//	public boolean sendMail(EmailBean emailBean) throws UnsupportedEncodingException, MessagingException {
//	    MimeMessage message = javaMailSender.createMimeMessage();
//	    MimeMessageHelper helper = new MimeMessageHelper(message, true);
//	    helper.setFrom("no-reply@bbss.co.th", "กรมบัญชีกลาง");
//	    helper.setTo("torgan.p@motiftech.com"); 
//	    helper.setSubject("ลงทะเบียน การชำระเงินอิเล็กทรอนิกส์");
//	    StringBuilder mailBody = new StringBuilder();
//	    mailBody.append("<span>เรียนคุณ ต่อการณ์ ภูอิ่นอ้อย</span><br/><br/>");
//	    mailBody.append("&nbsp;&nbsp;&nbsp;&nbsp;<span>ยินดีต้อนรับสู่สมาชิก กรมบัญชีกลาง ผู้ให้บริการ การชำระเงินอิเล็กทรอนิกส์ แบบครบวงจร  </span><br/>");
//	    mailBody.append("<span>บัญชีผู้ใช้งานของท่านคือ </span><br/>");
//	    mailBody.append("<span>username : 1234567890123</span><br/>");
//	    mailBody.append("<span>password : p@ssw0rd</span><br/><br/>");
//	    mailBody.append("<span>ขอบคุณที่ใช้บริการ </span><br/>");
//	    helper.setText(mailBody.toString(), true);
//	    
//	    javaMailSender.send(message);
//	    
//	    return false;
//	}

}
