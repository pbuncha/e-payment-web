package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.entity.Example;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.ExampleBean;
import th.go.cgd.epayment.repository.ExampleRepository;
import th.go.cgd.epayment.service.interfaces.IExampleService;

/**
 * @author torgan.p 10 ต.ค. 2560 23:16:14
 *
 */
@Service
@Transactional
public class ExampleServiceImpl implements IExampleService{
	private static final Logger logger = Logger.getLogger(ExampleServiceImpl.class);
	@Autowired
	private ExampleRepository exampleRepository;
	@Autowired
	private SessionFactory sessionFactory;
	    @PersistenceContext
	    private EntityManager entityManager;

	public void saveExample(ExampleBean exampleBean){
		Example exampleTb = new Example();
		try{
			exampleTb.setId(exampleBean.getId());
			exampleTb.setFname(exampleBean.getFname());
			exampleTb.setLname(exampleBean.getLname());
			exampleRepository.save(exampleTb);

		}catch(Exception e){
			System.out.println(e.getMessage());
			e.getStackTrace();
		}
	}

	public ExampleBean findExampleByName(String fname){
		ExampleBean exampleBean = null;
		try{
			Example  exampleTb = exampleRepository.findExampleByName(fname);
			if(exampleTb!=null){
				exampleBean = new ExampleBean();
				exampleBean.setId(exampleTb.getId());
				exampleBean.setFname(exampleTb.getFname());
				exampleBean.setLname(exampleTb.getLname());
			}


		}catch(Exception e){
			System.out.println(e.getMessage());
			e.getStackTrace();
		}
		return exampleBean;
	}

	public List<ExampleBean> findAllExampleByName(){
		List<ExampleBean> exampleBeanList = new ArrayList<ExampleBean>();
		try{
			List<Example>  exampleTbList = (List<Example>) exampleRepository.findAll();
			for(Example exampleTb : exampleTbList){
				ExampleBean exampleBean = new ExampleBean();
				exampleBean.setId(exampleTb.getId());
				exampleBean.setFname(exampleTb.getFname());
				exampleBean.setLname(exampleTb.getLname());
				exampleBeanList.add(exampleBean);
			}


		}catch(Exception e){
			System.out.println(e.getMessage());
			e.getStackTrace();
		}
		return exampleBeanList;
	}
	public void updateExample(ExampleBean exampleBean){

		try{
			Example exampleTb = exampleRepository.findOne(exampleBean.getId());
			//           	exampleTb.setId(1);
			exampleTb.setFname(exampleBean.getFname());
			exampleTb.setLname(exampleBean.getLname());
			exampleRepository.save(exampleTb);
		}catch(Exception e){
			System.out.println(e.getMessage());
			e.getStackTrace();
		}
	}
	@Override
	public ExampleBean getData(Integer id) {
		try {

			Example example = exampleRepository.findOne(id);
			ExampleBean exampleBean = new ExampleBean();
			exampleBean.setFname(example.getFname());
			exampleBean.setLname(example.getLname());
			exampleBean.setId(example.getId());

			return exampleBean;
		} catch (Exception ex) {
			logger.error("Cannot findOne by id: " + id, ex);
		}
		return null;
	}

	@Override
	public Boolean save(ExampleBean exampleBean) {

		if (exampleBean == null)
			return false;

		try {

			Example example = new Example();
			example.setFname(exampleBean.getFname());
			example.setLname(exampleBean.getLname());
			example.setId(exampleBean.getId());
			exampleRepository.save(example);
		} catch (Exception ex) {
			logger.error("Cannot save data " + exampleBean, ex);
			return false;
		}
		return true;
	}

	@Override
	public List<ExampleBean> getAll() {

		List<ExampleBean> result = null;
		Iterator<Example> it = exampleRepository.findAll().iterator();

		if (it.hasNext()) {
			result = new ArrayList<ExampleBean>();
		} else {
			return result;
		}

		while (it.hasNext()) {
			Example example = it.next();
			result.add(new ExampleBean(example.getId(), example.getFname(), example.getLname()));
		}

		return result;
	}

	@Override
	public Boolean saveAll(Collection<ExampleBean> exampleBean) {
		if (exampleBean == null) {
			return false;
		}
		try {
			exampleBean.stream().forEach(item -> {
				save(item);
			});
		} catch (Exception ex) {
			logger.error(ex);
		}
		return true;
	}

	@Override
	public Boolean delete(Integer id) {
		try {
			exampleRepository.delete(id);
		} catch (Exception ex) {
			return false;
		}
		return true;
	}

	@Override
	public ExampleBean update(ExampleBean exampleBean) {
		Example example;
		if( (example = exampleRepository.findOne(exampleBean.getId())) != null ){
			example.setFname( exampleBean.getFname() );
			example.setLname( exampleBean.getLname() );
//			sessionFactory.getCurrentSession().update(example);
			exampleRepository.save(example);
			return exampleBean;
		}else{
			return null;
		}

	}
	
	    public @ResponseBody List<DropdownBean> findExampleByIdNativeQuery(Long exampleId) {
	    	   StringBuffer queryStr = new StringBuffer(" SELECT * FROM PAYDB.EXAMPLE WHERE 1 = 1 ");
	    	   List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	          if(exampleId!=null){
	               queryStr.append(" AND ID = " + exampleId);
	          }
	          Query query = entityManager.createNativeQuery(queryStr.toString());
	          
	          List<Object[]> objectList = query.getResultList();
	          for(Object[] obj:objectList){
	        	  DropdownBean dropdownBean = new DropdownBean();
	        	  dropdownBean.setId(obj[0].toString());
	        	  dropdownBean.setDesc(obj[1].toString());
	        	  dropdownBean.setDesc2(obj[2].toString());
	        	  dropdownBeanList.add(dropdownBean);
	          }
	    	return dropdownBeanList;
	    }
	    public List<ExampleBean> findAllExample(){
		List<ExampleBean> exampleBeanList = new ArrayList<ExampleBean>();
		try{
			List<Example>  exampleTbList = (List<Example>) exampleRepository.findAll();
			for(Example exampleTb : exampleTbList){
				ExampleBean exampleBean = new ExampleBean();
				exampleBean.setId(exampleTb.getId());
				exampleBean.setFname(exampleTb.getFname());
				exampleBean.setLname(exampleTb.getLname());
				exampleBeanList.add(exampleBean);
			}


		}catch(Exception e){
			System.out.println(e.getMessage());
			e.getStackTrace();
		}
		return exampleBeanList;
	}
	    
	    public Page<ExampleBean> findAll(){
		List<ExampleBean> exampleBeanList = new ArrayList<ExampleBean>();
	    	List<Example> exampleList= exampleRepository.findAll();
	    	for(Example exampleTb : exampleList){
			ExampleBean exampleBean = new ExampleBean();
			exampleBean.setId(exampleTb.getId());
			exampleBean.setFname(exampleTb.getFname());
			exampleBean.setLname(exampleTb.getLname());
			exampleBeanList.add(exampleBean);
		}
	    	
	    	 Page<ExampleBean> entityPage = new PageImpl<ExampleBean>(exampleBeanList, new PageRequest(1, 10), exampleBeanList.size());
	    	 return entityPage;
	    }
}
