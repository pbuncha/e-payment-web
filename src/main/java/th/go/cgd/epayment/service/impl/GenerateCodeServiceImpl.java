package th.go.cgd.epayment.service.impl;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.ibm.icu.util.Calendar;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.Billing;
import th.go.cgd.epayment.entity.GenerateCode;
import th.go.cgd.epayment.model.InvoiceCodeBean;
import th.go.cgd.epayment.repository.GenerateCodeRepository;
import th.go.cgd.epayment.service.interfaces.IBillService;
import th.go.cgd.epayment.service.interfaces.IGenerateCodeService;
import th.go.cgd.epayment.util.CalendarHelper;
import th.go.cgd.epayment.util.GenerateInvoiceUti;

@Component
@Transactional(isolation = Isolation.READ_UNCOMMITTED)
public class GenerateCodeServiceImpl implements IGenerateCodeService {

	@Autowired
	public GenerateCodeRepository generateCodeRepository;
	@Autowired
	public IBillService billService;
	@Override
	public String getGenerateCode(String generateKeyTable) {

		Date now = new Date();
		Long generateCodeValue = null;
		GenerateCode generateCodeTb;
		try {
			String year = String.format("%tY", now);
			try {
				generateCodeTb = generateCodeRepository.findByGenerateKey(generateKeyTable, year).get(0);
				generateCodeValue = generateCodeTb.getGenerateValue().longValue() + 1L;
				generateCodeTb.setGenerateValue(generateCodeTb.getGenerateValue().add(BigInteger.ONE));
				generateCodeTb.setUpdateDate(now);
			} catch (NullPointerException | IndexOutOfBoundsException e) {
				generateCodeTb = new GenerateCode();
				generateCodeTb.setYears(year);
				generateCodeTb.setGenerateKey(generateKeyTable);
				generateCodeTb.setGenerateValue(BigInteger.ONE);
				generateCodeTb.setCreateDate(now);
				generateCodeTb.setUpdateDate(now);
				generateCodeTb.setCreateBy("System");
				generateCodeTb.setUpdateBy("System");
				generateCodeValue = BigInteger.ONE.longValue();
				generateCodeTb.setIsDelete('N');
			}

			generateCodeRepository.saveAndFlush(generateCodeTb);

		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}

		return String.format("%08d", generateCodeValue);
	}

	@Override
	public InvoiceCodeBean generateInvoiceCode( Date invoiceDate ){
		try {
			String running = getGenerateCode("BILL_NUMBER");
			GenerateInvoiceUti generateInvoiceUti = new GenerateInvoiceUti(invoiceDate, running);

			return new InvoiceCodeBean(generateInvoiceUti.getRef1(), generateInvoiceUti.getRef2(), generateInvoiceUti.getRef1().substring(0, 14) );
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}


	}

	@Override
	public String generateCatalogCode(){

		String running = getGenerateCode("CATALOG_CODE");
		return running.substring(3, 8);

	}

	public  String getAlphabet(){
		StringBuffer resultbuf = new StringBuffer();
		String result ="";
		Random r = new Random();
		String alphabet = "ABCDEFGRHIJKLMNOPQRSTUVWXYZ0123456789";
		for (int i = 0; i < 3; i++) {
			resultbuf.append(alphabet.charAt(r.nextInt(alphabet.length())));
		} 
		result = resultbuf.toString().toUpperCase();
		return result;
	}
	//	TypePrint P=print ,D=download
	@Override
	public  String generateBillCode(String typePrint,Integer billId){
		String billCode = "";
		String sequenceValue = "";
		DecimalFormat formatNumber = new DecimalFormat("00");
		StringBuffer resultBuff = new StringBuffer();
		resultBuff.append(EPaymentConstant.BILL_GENERATE.BILL_PRINT_TYPE);
		resultBuff.append(typePrint);
		try{
        		Billing billingTb = billService.getbillingById(billId);
        
        		if(billingTb.getStatus()==EPaymentConstant.BILL_STATUS.WAIT_PAY.charAt(0)){
        			resultBuff.append(getAlphabet());
        			resultBuff.append(EPaymentConstant.BILL_GENERATE.BILL_PRINT_PROTOTYPE);
        			billingTb.setSeqNo(0);
        			billingTb.setGenerateNo(resultBuff.toString());
        			billService.updateSeqBilling(billingTb);
        		}else{
        			int seqNo = billingTb.getSeqNo();
//        			String generateStr =  billingTb.getGenerateNo();
        			
        			
        			sequenceValue = String.valueOf(seqNo);
        			sequenceValue =  formatNumber.format(Integer.valueOf(sequenceValue)+ 1);
        			resultBuff.append(getAlphabet());
        			resultBuff.append(sequenceValue);

        			seqNo = (seqNo+1);
        			billingTb.setSeqNo(seqNo);
//        			generateStr = generateStr.substring(0, generateStr.length()-2);
//        			resultBuff.append(generateStr);
        			System.out.println("generateStr : "+resultBuff);
        			billingTb.setGenerateNo(resultBuff.toString());
        			billService.updateSeqBilling(billingTb);
        		}
                }catch(Exception e){
                    e.printStackTrace();
                }
		billCode = resultBuff.toString();
		System.out.println("Code : "+billCode);
		return billCode;
	}


	public  String generateInvoiceLoaderCode( Date nowDate){
		
		String running = getGenerateCode("INVOICE_LOADER_CODE");

		return CalendarHelper.getShortYear(nowDate)+CalendarHelper.getMonth(nowDate)+running.substring(4, 8);
	}

}
