/**
 * 
 */
package th.go.cgd.epayment.service.impl;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.entity.Catalog;
import th.go.cgd.epayment.model.CatalogBean;
import th.go.cgd.epayment.model.InvoiceBillingBean;
import th.go.cgd.epayment.service.interfaces.IInvoiceBillingService;

/**
 * @author wichuda.k Nov 12, 2017
 *
 */
@Service
@Transactional
public class InvoiceBillingServiceImpl implements IInvoiceBillingService {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<InvoiceBillingBean> getInvoiceBillingList(InvoiceBillingBean itemBean) {
						
		StringBuffer queryStr = new StringBuffer();

		queryStr.append(" select  ");
		queryStr.append(" INVOICE.REF_1, ");
		queryStr.append(" INVOICE.USER_ID, ");
		queryStr.append(" INVOICE.FIRST_NAME, ");
		queryStr.append(" INVOICE.LAST_NAME, ");
		queryStr.append(" INVOICE.CITIZEN_NO, ");
		queryStr.append(" INVOICE.PAYMENT_CHANNEL_ID,  ");
		queryStr.append(" M_PAYMENT_CHANNEL.PAYMENT_CHANNEL_NAME,  ");
		queryStr.append(" BILLING.CREATED_DATE, ");
		queryStr.append(" CATALOG_STRUCTURE_ITEM.COST_CENTER_ID, ");
		queryStr.append(" M_COST_CENTER.COST_CENTER_NAME, ");
		queryStr.append(" CATALOG.CATALOG_CODE, ");
		queryStr.append(" CATALOG.CATALOG_NAME, ");
		queryStr.append(" INVOICE_ITEM.AMOUNT, ");
		queryStr.append(" BILLING.BILLING_NO, ");
		queryStr.append(" INVOICE.STATUS ");
		queryStr.append(" from PAYDB.INVOICE INVOICE, PAYDB.BILLING BILLING, PAYDB.INVOICE_ITEM INVOICE_ITEM, PAYDB.CATALOG CATALOG, PAYDB.CATALOG_STRUCTURE CATALOG_STRUCTURE, PAYDB.CATALOG_STRUCTURE_ITEM CATALOG_STRUCTURE_ITEM, PAYDB.M_PAYMENT_CHANNEL M_PAYMENT_CHANNEL, PAYDB.M_COST_CENTER M_COST_CENTER ");
		queryStr.append(" WHERE INVOICE.INVOICE_ID = BILLING.INVOICE_ID ");
		queryStr.append(" and INVOICE_ITEM.INVOICE_ID = INVOICE.INVOICE_ID  ");
		queryStr.append(" AND INVOICE_ITEM.CATALOG_ID = CATALOG.CATALOG_ID ");
		queryStr.append(" AND CATALOG.CATALOG_ID = CATALOG_STRUCTURE.CATALOG_ID ");
		queryStr.append(" AND CATALOG_STRUCTURE.CATALOG_STRUCTURE_ID = CATALOG_STRUCTURE_ITEM.CATALOG_STRUCTURE_ID ");
		queryStr.append(" AND INVOICE.PAYMENT_CHANNEL_ID = M_PAYMENT_CHANNEL.PAYMENT_CHANNEL_ID ");
		queryStr.append(" AND CATALOG_STRUCTURE_ITEM.COST_CENTER_ID = M_COST_CENTER.COST_CENTER_ID ");

		if (itemBean.getRef1() != null) {
			queryStr.append(" AND INVOICE.REF_1 like '%" + itemBean.getRef1() + "%' ");
		}
		if (itemBean.getFirstName() != null) {
			queryStr.append(" AND INVOICE.FIRST_NAME like '%" + itemBean.getFirstName() + "%' ");
		}
		if (itemBean.getCostCenterId() != null) {
			queryStr.append(" AND CATALOG_STRUCTURE_ITEM.COST_CENTER_ID = " + itemBean.getCostCenterId() + " ");
		}
		if (itemBean.getStatus() != null && !"".equals(itemBean.getStatus())) {
			queryStr.append(" AND INVOICE.STATUS = '" + itemBean.getStatus() + "' ");
		}
		
		List<InvoiceBillingBean> invoiceItemBeans = new ArrayList<InvoiceBillingBean>();
		
		try {
			Query query = this.entityManager.createNativeQuery(queryStr.toString());
			List<Object[]> objects = query.getResultList();
			for (Object[] obj : objects) {
				InvoiceBillingBean bean = new InvoiceBillingBean();
				
				bean.setRef1(obj[0].toString());
				bean.setUserId(Integer.parseInt(obj[1].toString()));
				bean.setFirstName(obj[2].toString());
				bean.setLastName(obj[3].toString());
				bean.setCitizenNo(obj[4].toString());
				bean.setPaymentChannelId(obj[5] == null ? null : Integer.parseInt(obj[5].toString()));
				bean.setPaymentChannelName(obj[6].toString());
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd", new Locale("TH", "th"));
				bean.setCreatedDate(formatter.parse(obj[7].toString()));
				
				bean.setCostCenterId(Integer.parseInt(obj[8].toString()));
				bean.setCostCenterCode(obj[9].toString());
				bean.setCatalogCode(obj[10].toString());
				bean.setCatalogName(obj[11].toString());
				
				bean.setAmount(new BigDecimal(obj[12].toString()));
				
				bean.setBillingNo(obj[13].toString());
				bean.setStatus(obj[14].toString());
				
				invoiceItemBeans.add(bean);
			}
			
			
			if (itemBean.getCreatedDateTo() != null)
			{
				invoiceItemBeans = invoiceItemBeans.stream()
						.filter(a -> a.getCreatedDate() != null)
						.filter(a -> a.getCreatedDate().getTime() <= itemBean.getCreatedDateTo().getTime())
						.collect(Collectors.toList());						
			}
			if (itemBean.getCreatedDateFrom() != null)
			{
				invoiceItemBeans = invoiceItemBeans.stream()
						.filter(a -> a.getCreatedDate() != null)
						.filter(a -> a.getCreatedDate().getTime() >= itemBean.getCreatedDateFrom().getTime() )
						.collect(Collectors.toList());						
			}			
			
			return invoiceItemBeans;
		} catch (Exception e) {
			e.printStackTrace();
			return invoiceItemBeans;
		}
		
	}

	
	public Page<InvoiceBillingBean> search(){
		
		StringBuffer queryStr = new StringBuffer();

		queryStr.append(" select  ");
		queryStr.append(" INVOICE.REF_1, ");
		queryStr.append(" INVOICE.USER_ID, ");
		queryStr.append(" INVOICE.FIRST_NAME, ");
		queryStr.append(" INVOICE.LAST_NAME, ");
		queryStr.append(" INVOICE.CITIZEN_NO, ");
		queryStr.append(" INVOICE.PAYMENT_CHANNEL_ID,  ");
		queryStr.append(" BILLING.CREATED_DATE, ");
		queryStr.append(" CATALOG_STRUCTURE_ITEM.COST_CENTER_ID, ");
		queryStr.append(" CATALOG.CATALOG_CODE, ");
		queryStr.append(" CATALOG.CATALOG_NAME, ");
		queryStr.append(" INVOICE_ITEM.AMOUNT, ");
		queryStr.append(" BILLING.BILLING_NO, ");
		queryStr.append(" INVOICE.STATUS ");
		queryStr.append(" from PAYDB.INVOICE INVOICE, PAYDB.BILLING BILLING, PAYDB.INVOICE_ITEM INVOICE_ITEM, PAYDB.CATALOG CATALOG, PAYDB.CATALOG_STRUCTURE CATALOG_STRUCTURE, PAYDB.CATALOG_STRUCTURE_ITEM CATALOG_STRUCTURE_ITEM ");
		queryStr.append(" WHERE INVOICE.INVOICE_ID = BILLING.INVOICE_ID ");
		queryStr.append(" and INVOICE_ITEM.INVOICE_ID = INVOICE.INVOICE_ID  ");
		queryStr.append(" AND INVOICE_ITEM.CATALOG_ID = CATALOG.CATALOG_ID ");
		queryStr.append(" AND CATALOG.CATALOG_ID = CATALOG_STRUCTURE.CATALOG_ID ");
		queryStr.append(" AND CATALOG_STRUCTURE.CATALOG_STRUCTURE_ID = CATALOG_STRUCTURE_ITEM.CATALOG_STRUCTURE_ID ");

//		if (!(itemBean.getDepartmentId() == null)) {
//			queryStr.append(" AND csi.DEPARTMENT_ID = '" + itemBean.getDepartmentId() + "' ");
//		}
			
		List<InvoiceBillingBean> invoiceItemBeans = new ArrayList<InvoiceBillingBean>();
		
		try {
			Query query = this.entityManager.createNativeQuery(queryStr.toString());
			List<Object[]> objects = query.getResultList();
			for (Object[] obj : objects) {
				InvoiceBillingBean bean = new InvoiceBillingBean();
				
				bean.setRef1(obj[0].toString());
				bean.setUserId(Integer.parseInt(obj[1].toString()));
				bean.setFirstName(obj[2].toString());
				bean.setLastName(obj[3].toString());
				bean.setCitizenNo(obj[4].toString());
				bean.setPaymentChannelId(obj[5] == null ? null : Integer.parseInt(obj[5].toString()));
				
				SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd", new Locale("TH", "th"));
				bean.setCreatedDate(formatter.parse(obj[6].toString()));
				
				bean.setCostCenterId(Integer.parseInt(obj[7].toString()));
				bean.setCatalogCode(obj[8].toString());
				bean.setCatalogName(obj[9].toString());
				
//				bean.setAmount(BigDecimal.valueOf(Integer.parseInt(obj[10].toString())));
				
				bean.setBillingNo(obj[11].toString());
				bean.setStatus(obj[12].toString());
				
				invoiceItemBeans.add(bean);			
				
			}
						
			List<InvoiceBillingBean> list = invoiceItemBeans;
			List<InvoiceBillingBean> InvoiceBillingList= new InvoiceBillingServiceImpl().getInvoiceBillingList(new InvoiceBillingBean());

			for(InvoiceBillingBean q : InvoiceBillingList){
				
				InvoiceBillingBean x = new InvoiceBillingBean();
				
				x.setRef1(q.getRef1());
				x.setUserId(q.getUserId());
				x.setFirstName(q.getFirstName());
				x.setLastName(q.getLastName());
				
				list.add(x);
			}
			Page<InvoiceBillingBean> entityPage = new PageImpl<InvoiceBillingBean>(list, new PageRequest(1, 10), list.size());
			return entityPage;	
			
			
			//return invoiceItemBeans;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	
/*	public Page<InvoiceBillingBean> search(){
		List<InvoiceBillingBean> list = new ArrayList<InvoiceBillingBean>();
		List<InvoiceBillingBean> InvoiceBillingList= new InvoiceBillingServiceImpl().getInvoiceBillingList(new InvoiceBillingBean());

		for(InvoiceBillingBean q : InvoiceBillingList){
			
			InvoiceBillingBean x = new InvoiceBillingBean();
			
			x.setRef1(x.getRef1());
			x.setUserId(x.getUserId());
			x.setFirstName(x.getFirstName());
			x.setLastName(x.getLastName());
			
			list.add(x);
		}
		Page<InvoiceBillingBean> entityPage = new PageImpl<InvoiceBillingBean>(list, new PageRequest(1, 10), list.size());
		return entityPage;
	}*/

		

}
