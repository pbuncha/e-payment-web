package th.go.cgd.epayment.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.entity.InvoiceItem;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserFavorite;
import th.go.cgd.epayment.model.InvoiceItemBean;
import th.go.cgd.epayment.repository.DepartmentRepository;
import th.go.cgd.epayment.repository.InvoiceItemRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.service.interfaces.IInvoiceItemService;
import th.go.cgd.epayment.util.CalendarHelper;

@Service
@Transactional
public class InvoiceItemServiceImpl implements IInvoiceItemService {
	private static final Logger logger = Logger.getLogger(InvoiceItemServiceImpl.class);

	@Autowired
	private InvoiceItemRepository invoiceItemRepository;
	@Autowired
	private MDepartmentRepository mDepartmentRepository;
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<InvoiceItemBean> getInvoiceList(User  user) {
//		List<InvoiceItem> invoiceItems = invoiceItemRepository.getInvoiceItemByUser(userId);
		List<InvoiceItemBean> invoiceItemBeans = new ArrayList<InvoiceItemBean>();
		    StringBuffer queryStr = new StringBuffer();
			queryStr.append(" SELECT distinct c.CATALOG_CODE ");
			queryStr.append(" ,c.CATALOG_NAME ");
			queryStr.append(" ,i.INVOICE_NO AS INVOICE_NO ");
			queryStr.append(" ,i.REF_1 ");
			queryStr.append(" , c.Amount AS AMOUNT  ");
			queryStr.append(" , dep.DEPARTMENT_NAME AS DEPARTMENT_NAME ");
			queryStr.append(" ,c.END_DATE  ");
			queryStr.append(" FROM PAYDB.CATALOG c ");
			queryStr.append(" INNER JOIN PAYDB.INVOICE_ITEM it ON c.CATALOG_ID = it.CATALOG_ID ");
			queryStr.append(" INNER JOIN PAYDB.INVOICE i ON i.INVOICE_ID = it.INVOICE_ID ");
			queryStr.append(" LEFT JOIN PAYDB.CATALOG_STRUCTURE cs ON cs.CATALOG_ID = C.CATALOG_ID ");
			queryStr.append(" LEFT JOIN PAYDB.CATALOG_STRUCTURE_ITEM csi ON csi.CATALOG_STRUCTURE_ID = cs.CATALOG_STRUCTURE_ID ");
			queryStr.append(" LEFT JOIN PAYDB.CATALOG_CONDITION_TEXT CCT ON CCT.CATALOG_ID = C.CATALOG_ID ");
			queryStr.append(" LEFT JOIN  PAYDB.M_DEPARTMENT dep ON csi.DEPARTMENT_ID = dep.DEPARTMENT_ID ");
			queryStr.append(" WHERE c.IS_SHOW = 'Y' AND i.USER_ID = '1' ");
			if (user != null) {
				queryStr.append(" AND i.USER_ID = '" + user.getUserId() + "' ");
			}else{
			    	queryStr.append(" AND i.USER_ID = 0");
			}
			Query query = this.entityManager.createNativeQuery(queryStr.toString());
		
			try {
				
				List<Object[]> objects = query.getResultList();

				for (Object[] obj : objects) {
					InvoiceItemBean bean = new InvoiceItemBean();
					bean.setCatalogCode(obj[0] != null ? obj[0].toString() : "-");
					bean.setCatalogName(obj[1] != null ? obj[1].toString() : "-");
					bean.setInvoiceNo(obj[2] != null ? obj[2].toString() : "-");
					bean.setRef1(obj[3] != null ? obj[3].toString() : "-");
					bean.setTotalAmount(new BigDecimal(obj[4] != null ? obj[4].toString() : BigDecimal.ZERO.toString()).setScale(2));
					bean.setDepartmentName(obj[5] != null ? obj[5].toString() : "-");
					bean.setEndDateStr(obj[6] != null ? CalendarHelper.formatDateTH(CalendarHelper.getDateTime(obj[6])) : "-");
					
					invoiceItemBeans.add(bean);

				}
				
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		
//		if (invoiceItems.size() != 0) {
//			for (InvoiceItem item : invoiceItems) {
//				InvoiceItemBean bean = new InvoiceItemBean();
//				bean.setInvoiceId(item.getInvoiceId().getInvoiceId());
////				bean.setTotalAmount((BigDecimal.valueOf(item.getCatalogId().getAmount()).movePointLeft(2)));
//				bean.setTotalAmount((BigDecimal.valueOf(item.getCatalogId().getAmount()).setScale(2)));
//				bean.setInvoiceNo(item.getInvoiceId().getInvoiceNo());
//				bean.setRef1(item.getInvoiceId().getRef1());
//				bean.setCatalogCode(item.getCatalogId().getCatalogCode());
//				bean.setCatalogName(item.getCatalogId().getCatalogName());
//				
//				String mDepartment = item.getCatalogId().getCatalogStructureList().get(0).getCatalogStructureItemList().get(0).getDepartmentId().getDepartmentName();
//				bean.setDepartmentName(mDepartment != null ? mDepartment : null);
//				bean.setEndDate(item.getInvoiceId().getEndDate());
//				invoiceItemBeans.add(bean);
//
//			}
//		}
		return invoiceItemBeans;
	}
	
//	@Override
	public List<InvoiceItemBean> getInvoiceItem(User  user) {
	    List<InvoiceItemBean> invoiceItemBeans = new ArrayList<InvoiceItemBean>();
	    StringBuffer queryStr = new StringBuffer();
		queryStr.append(" SELECT c.CATALOG_NAME AS NAME ");
		queryStr.append(" , c.CATALOG_ID AS ID ");
		queryStr.append(" , c.CATALOG_CODE AS CODE ");
		queryStr.append(" , csi.DEPARTMENT_ID AS DEP_ID ");
		queryStr.append(" , cct.CONDITION_TEXT AS CONDI_TEXT ");
		queryStr.append(" , c.Amount AS AMOUNT ");
		queryStr.append(" FROM PAYDB.CATALOG C ");
		queryStr.append(" INNER JOIN PAYDB.M_CATALOG_TYPE mc ON c.CATALOG_TYPE_ID =mc.CATALOG_TYPE_ID ");
		queryStr.append(" LEFT JOIN PAYDB.CATALOG_STRUCTURE cs ON cs.CATALOG_ID = C.CATALOG_ID ");
		queryStr.append(" LEFT JOIN PAYDB.CATALOG_STRUCTURE_ITEM csi ON csi.CATALOG_STRUCTURE_ID = cs.CATALOG_STRUCTURE_ID ");
		queryStr.append(" LEFT JOIN PAYDB.CATALOG_CONDITION_TEXT CCT ON CCT.CATALOG_ID = C.CATALOG_ID ");
		queryStr.append(" WHERE c.IS_SHOW = 'Y' ");
		queryStr.append(" UNION ");
		queryStr.append(" SELECT c.CATALOG_NAME AS NAME ");
		queryStr.append(" ,c.CATALOG_ID AS ID ");
		queryStr.append(" , c.CATALOG_CODE AS CODE ");
		queryStr.append(" ,null AS DEP_ID ");
		queryStr.append(" , '' AS  CONDI_TEXT ");
		queryStr.append(" , c.AMOUNT AS AMOUNT ");
		queryStr.append(" FROM PAYDB.INVOICE_ITEM it ");
		queryStr.append(" INNER JOIN PAYDB.INVOICE i on it.INVOICE_ID = i.INVOICE_ID ");
		queryStr.append(" INNER JOIN PAYDB.CATALOG c ON it.CATALOG_ID = c.CATALOG_ID ");
		queryStr.append(" WHERE (1=1) ");
		if (user != null) {
			queryStr.append(" AND i.USER_ID = '" + user.getUserId() + "' ");
		}else{
		    	queryStr.append(" AND i.USER_ID = 0");
		}
		Query query = this.entityManager.createNativeQuery(queryStr.toString());
		
		try {
			
			List<Object[]> objects = query.getResultList();

			for (Object[] obj : objects) {
				InvoiceItemBean bean = new InvoiceItemBean();
				int catalogId = Integer.parseInt(obj[1].toString());
				bean.setCatalogName(obj[0] != null ? obj[0].toString() : "-");
				bean.setCatalogId(catalogId);
				bean.setCatalogCode(obj[2].toString());
				if (obj[3] != null) {
					MDepartment department = mDepartmentRepository.getOne(Integer.parseInt(obj[3].toString()));
					bean.setDepartmentId(Integer.parseInt(obj[3].toString()));
					bean.setDepartmentName(department.getDepartmentName());
				}
				bean.setConditionText(obj[4] != null ? obj[4].toString() : "-");
				bean.setAmount(new BigDecimal(obj[5] != null ? obj[5].toString() : "0"));
				bean.setFavorite(false);
				invoiceItemBeans.add(bean);

			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return invoiceItemBeans;
	}

}
