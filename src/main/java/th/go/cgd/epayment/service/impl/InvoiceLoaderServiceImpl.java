/**
 * 
 */
package th.go.cgd.epayment.service.impl;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;


import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;


import th.go.cgd.epayment.entity.Catalog;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.InvoiceItem;
import th.go.cgd.epayment.entity.InvoiceLoader;
import th.go.cgd.epayment.entity.MAmphur;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MTambon;
import th.go.cgd.epayment.entity.MTitleName;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.AddressBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.InvoiceItemBean;
import th.go.cgd.epayment.model.InvoiceLoaderBean;
import th.go.cgd.epayment.repository.CatalogRepository;
import th.go.cgd.epayment.repository.InvoiceLoaderRepository;
import th.go.cgd.epayment.repository.InvoiceRepository;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.service.interfaces.IGenerateCodeService;
import th.go.cgd.epayment.service.interfaces.IInvoiceLoaderService;
import th.go.cgd.epayment.service.interfaces.IInvoiceService;

/**
 * @author wichuda.k Jan 4, 2018
 *
 */
@Service
@Transactional
public class InvoiceLoaderServiceImpl  implements IInvoiceLoaderService {

	private static final Logger log = Logger.getLogger(InvoiceLoaderServiceImpl.class);
	private @PersistenceContext EntityManager entityManager;
	private @Value("${import.output.path}") String rootPath;
	private @Autowired InvoiceLoaderRepository invoiceLoaderRepository;
	private @Autowired InvoiceServiceImpl invoiceServiceImpl;
	public static final String UTF8_BOM = "\uFEFF";
	private @Autowired IGenerateCodeService generateCodeService;
	private @Autowired MCostCenterRepository costCenterRepository;
	private @Autowired UserRepository userRepository;

	public void importInvoice(MultipartFile file) throws Exception{
		//		 BufferedReader br = null;
		String line = "";
		String cvsSplitBy = "\\|";
		byte[] bytes = file.getBytes();
		Date now = new Date();
		User userTb = userRepository.findOne(1);
		boolean firstLine = true;
		BufferedReader br = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(bytes),StandardCharsets.UTF_8));
		String code = generateCodeService.generateInvoiceLoaderCode(now);
		String deptCode = "";
		if(userTb.getUserOrganizationList2()!=null && userTb.getUserOrganizationList2().size()>0){
			deptCode = userTb.getUserOrganizationList2().get(0).getDepartmentId().getDepartmentCode();
			if(deptCode.length()<5)
				deptCode = "0"+deptCode;
		}
		code = deptCode+code;
		while ((line = br.readLine()) != null) {
			// use | as separator
			if (firstLine) {
				line = removeUTF8BOM(line);
				firstLine = false;
			}

			String[] data = line.split(cvsSplitBy);
			InvoiceLoader invoiceLoader = dataToEntity(data,now);
			invoiceLoader.setFileName(file.getOriginalFilename());
			invoiceLoader.setInvoiceLoaderCode(code);
			invoiceLoaderRepository.save(invoiceLoader);
		}
	}

	public List<InvoiceLoaderBean> getInvoiceLoaderByCriteria(InvoiceLoaderBean bean) throws ParseException{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");	
		StringBuffer queryStr = new StringBuffer(" SELECT i.INVOICE_LOADER_CODE, i.FILE_NAME ");
		queryStr.append(" ,TO_CHAR(ADD_YEARS(i.COMFIRM_DATE,543),'yyyyMMdd') ,TO_CHAR(ADD_YEARS(i.CANCEL_DATE,543),'yyyyMMdd'), COUNT(INVOICE_LOADER_ID) as TOTAL ");
		queryStr.append(" from PAYDB.INVOICE_LOADER i ");
		queryStr.append(" WHERE (1=1) ");
		//WHERE criteria
		if((bean.getCode()!= null)){
			queryStr.append(" AND i.INVOICE_LOADER_CODE LIKE '%"+bean.getCode()+"%' ");
		}
		if((bean.getFileName()!= null)){
			queryStr.append(" AND i.FILE_NAME LIKE '%"+bean.getFileName()+"%' ");
		}
		if((bean.getConfirmDateFrom()!= null)){
			queryStr.append(" AND TO_CHAR(i.COMFIRM_DATE,'yyyyMMdd') >= '"+formatter.format(bean.getConfirmDateFrom())+"' ");
		}
		if((bean.getConfirmDateTo()!= null)){
			queryStr.append(" AND TO_CHAR(i.COMFIRM_DATE,'yyyyMMdd') <= '"+formatter.format(bean.getConfirmDateTo())+"' ");
		}

		queryStr.append(" GROUP BY i.FILE_NAME ,i.INVOICE_LOADER_CODE ,i.COMFIRM_DATE ,i.CANCEL_DATE ,i.CREATED_DATE ");
		queryStr.append(" ORDER BY i.CREATED_DATE DESC");

		Query query = this.entityManager.createNativeQuery(queryStr.toString());
		List<Object[]> result = query.getResultList();
		List<InvoiceLoaderBean> loaderBeans = new ArrayList<InvoiceLoaderBean>();
		for(Object[] obj : result){
			InvoiceLoaderBean tmp = new InvoiceLoaderBean();
			tmp.setCode(obj[0]!=null?obj[0].toString():null);
			tmp.setFileName(obj[1]!=null?obj[1].toString():null);					
			tmp.setConfirmDate(obj[2]!=null?formatter.parse(obj[2].toString()):null);
			tmp.setCancelDate(obj[3]!=null?formatter.parse(obj[3].toString()):null);
			tmp.setTotal(obj[4]!=null?BigDecimal.valueOf(Long.valueOf(obj[4].toString())):null);
			loaderBeans.add(tmp);
		}

		return loaderBeans;		
	}

	private static String removeUTF8BOM(String s) {
		if (s.startsWith(UTF8_BOM)) {
			s = s.substring(1);
		}
		return s;
	}

	private Boolean parseDate(String date){
		String[] checkDate = date.split("/");
		if(Integer.valueOf(checkDate[0]) >31|| Integer.valueOf(checkDate[1])>12){
			return false;
		}
		return true;

	}

	private Boolean validateMaster(String tumbon ,String amphur ,String province){
		TypedQuery<MTambon> tumbonlist = this.entityManager.createNamedQuery("MTambon.findByTambonNameTh", MTambon.class);
		tumbonlist.setParameter("tambonNameTh", tumbon);
		if(tumbonlist.getSingleResult() == null){
			return false;
		}
		TypedQuery<MAmphur> amphurlist = this.entityManager.createNamedQuery("MAmphur.findByAmphurNameTh", MAmphur.class);
		amphurlist.setParameter("amphurNameTh", amphur);
		if(amphurlist.getSingleResult() == null){
			return false;
		}
		TypedQuery<MProvince> provincelist = this.entityManager.createNamedQuery("MProvince.findByProvinceNameTh", MProvince.class);
		provincelist.setParameter("provinceNameTh", province);
		if(provincelist.getSingleResult() == null){
			return false;
		}
		return true;
	}

	private InvoiceLoader dataToEntity(String[] data,Date nowDate) throws Exception {
		InvoiceLoader invoiceLoader = new InvoiceLoader(); 
		SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy",new Locale( "th" , "TH" ));

		TypedQuery<Catalog> list = this.entityManager.createNamedQuery("Catalog.findByCatalogCode", Catalog.class);
		list.setParameter("catalogCode", data[2]);
		if(list.getSingleResult() == null)
			throw new Exception("error wrong catalogCode ");
		//1-5
		invoiceLoader.setRunningNo(!StringUtils.isEmpty(data[0])?Integer.valueOf(data[0].trim()):null);
		invoiceLoader.setCostCenterCode(data[1]);
		invoiceLoader.setCatalogId(data[2]);
		invoiceLoader.setInvoiceItemDesc(data[3]);

		if(!parseDate(data[4]) && !parseDate(data[5])){
			throw new ParseException("error", 0);
		}

		invoiceLoader.setInvoiceStartDate(!StringUtils.isEmpty(data[4])?dt1.parse(data[4].trim()):null);
		//6-10
		invoiceLoader.setInvoiceEndDate(!StringUtils.isEmpty(data[5])?dt1.parse(data[5].trim()):null);	
		invoiceLoader.setInvoiceItemAmount(!StringUtils.isEmpty(data[6])?new BigDecimal(data[6].trim().replace(",", "")):BigDecimal.valueOf(0));
		invoiceLoader.setInvoiceItemRef1(data[7]);
		invoiceLoader.setInvoiceItemRef2(data[8]);
		invoiceLoader.setInvoiceItemRef3(data[9]);
		//11-15
		invoiceLoader.setCitizenNo(data[10]);				
		invoiceLoader.setInvoiceTitle(data[11]);
		invoiceLoader.setInvoiceFirstName(data[12]);
		invoiceLoader.setInvoiceMiddleName(data[13]);
		invoiceLoader.setInvoiceLastName(data[14]);
		//16-20
		invoiceLoader.setInvoiceBusinessNo1(data[15]);				
		invoiceLoader.setInvoiceBusinessName1(data[16]);
		invoiceLoader.setInvoiceBusinessNo2(data[17]);
		invoiceLoader.setInvoiceBusinessName2(data[18]);
		invoiceLoader.setAddressNo(data[19]);
		//21-25
		invoiceLoader.setAddressBuildingName(data[20]);				
		invoiceLoader.setAddressMoo(data[21]);
		invoiceLoader.setAddressLane(data[22]);
		invoiceLoader.setAddressSoi(data[23]);
		invoiceLoader.setAddressRoad(data[24]);

		if(!validateMaster(data[26], data[25], data[27])){
			throw new Exception("error Master");
		}
		//26-30
		invoiceLoader.setAddressAmphur(data[25]);
		invoiceLoader.setAddressTambon(data[26]);
		invoiceLoader.setAddressProvince(data[27]);
		invoiceLoader.setAddressPostcode(data[28]);
		invoiceLoader.setUserMobile(data[29]);

		//31-37
		invoiceLoader.setCanSendMobile(!StringUtils.isEmpty(data[30])?data[30].trim().charAt(0):null);
		invoiceLoader.setUserEmail(data[31]);
		invoiceLoader.setCanSendEmail(!StringUtils.isEmpty(data[32])?data[32].trim().charAt(0):null);
		invoiceLoader.setGovOtherDetail1(data[33]);
		invoiceLoader.setGovOtherDetail2(data[34]);
		invoiceLoader.setInvoiceCondition(data[35]);
		invoiceLoader.setGroupBy(data[36]);

		invoiceLoader.setCreatedDate(nowDate);
		invoiceLoader.setLoadDate(nowDate);

		return invoiceLoader;

	}

	public void confirm(String code) {
		try{
			StringBuffer queryStr = new StringBuffer(" SELECT i.INVOICE_LOADER_CODE, i.GROUP_BY ");
			queryStr.append(" from PAYDB.INVOICE_LOADER i ");
			queryStr.append(" WHERE (1=1) ");
			queryStr.append(" AND i.INVOICE_LOADER_CODE = '"+code+"' ");
			queryStr.append(" GROUP BY i.INVOICE_LOADER_CODE, i.GROUP_BY ");
			Query query = this.entityManager.createNativeQuery(queryStr.toString());

			List<Object[]> result = query.getResultList();
			List<InvoiceLoaderBean> loaderBeans = new ArrayList<InvoiceLoaderBean>();
			List<InvoiceBean> beans = new ArrayList<InvoiceBean>();
			Date now = new Date();

			for(Object[] obj : result){
				InvoiceLoaderBean tmp = new InvoiceLoaderBean();
				InvoiceBean bean = new InvoiceBean();
				List<InvoiceItemBean> items = new ArrayList<InvoiceItemBean>();
				boolean firstLine = true;

				tmp.setCode(obj[0]!=null?obj[0].toString():null);
				tmp.setGroupby(obj[1]!=null?obj[1].toString():null);			

				List<InvoiceLoader> loaders = invoiceLoaderRepository.findBy(tmp.getCode(), tmp.getGroupby());
				for(InvoiceLoader loader : loaders ){
					if(firstLine){
						bean = loaderToInvoiceBean(loader);
						firstLine = false;
					}		
					items.add(loaderToInvoiceItemBean(loader));
					loader.setComfirmDate(now);
					invoiceLoaderRepository.save(loader);
				}
				bean.setItems(items);
				beans.add(bean);
			}
			for(InvoiceBean invoiceBean: beans){
				invoiceServiceImpl.save(invoiceBean);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	private InvoiceItemBean loaderToInvoiceItemBean(InvoiceLoader loader) throws Exception{
		InvoiceItemBean bean = new InvoiceItemBean();
		TypedQuery<Catalog> list = this.entityManager.createNamedQuery("Catalog.findByCatalogCode", Catalog.class);
		list.setParameter("catalogCode", loader.getCatalogId());
		List<Catalog> cat = null;
		cat = list.getResultList();
		bean.setCatalogId(cat.get(0).getCatalogId());
		bean.setItemName(loader.getInvoiceItemDesc());
		bean.setAmount(loader.getInvoiceItemAmount());
		return bean;
	}

	private InvoiceBean loaderToInvoiceBean(InvoiceLoader loader) throws NullPointerException{
		InvoiceBean invoiceBean = new InvoiceBean();
		MCostCenter costcenter = costCenterRepository.findMCostCenterByCode(loader.getCostCenterCode());		
		invoiceBean.setCostCenterId(costcenter.getCostCenterId());

		invoiceBean.setCitizenNo(loader.getCitizenNo());

		TypedQuery<MTitleName> list = this.entityManager.createNamedQuery("MTitleName.findByTitleName", MTitleName.class);
		list.setParameter("titleName", loader.getInvoiceTitle());
		MTitleName title = list.getSingleResult();
		invoiceBean.setTitleId(title.getTitleId());

		if(!loader.getInvoiceFirstName().isEmpty() || !loader.getInvoiceFirstName().equals("")){
			invoiceBean.setFirstName(loader.getInvoiceFirstName());
			invoiceBean.setMidName(loader.getInvoiceMiddleName());
			invoiceBean.setLastName(loader.getInvoiceLastName());
			invoiceBean.setPersTypeId(1);
			invoiceBean.setCitizenNo(loader.getCitizenNo());
		}else if(!loader.getInvoiceBusinessName1().isEmpty() || !loader.getInvoiceBusinessName1().equals("")){
			invoiceBean.setCompName(loader.getInvoiceBusinessName1());
			invoiceBean.setPersTypeId(2);
			invoiceBean.setCitizenNo(loader.getInvoiceBusinessNo1());
		}else if(!loader.getInvoiceBusinessName2().isEmpty() || !loader.getInvoiceBusinessName2().equals("")){
			invoiceBean.setGroupName(loader.getInvoiceBusinessName2());
			invoiceBean.setPersTypeId(3);
			invoiceBean.setCitizenNo(loader.getInvoiceBusinessNo2());
		}
		invoiceBean.setMobile(loader.getUserMobile());
		invoiceBean.setEmail(loader.getUserEmail());
		invoiceBean.setStartDate(loader.getInvoiceStartDate());
		invoiceBean.setEndDate(loader.getInvoiceEndDate());
		invoiceBean.setTotalAmount(loader.getInvoiceItemAmount());
		invoiceBean.setAddrHome(loaderToAddrBean(loader));
		invoiceBean.setHomeIsBill(true);
		return invoiceBean;
	}

	private AddressBean loaderToAddrBean(InvoiceLoader loader) throws NullPointerException{
		AddressBean home = new AddressBean();
		home.setNo(loader.getAddressNo());
		home.setMoo(loader.getAddressMoo());
		home.setLane(loader.getAddressLane());
		home.setRoad(loader.getAddressRoad());
		home.setSoi(loader.getAddressSoi());
		home.setVillage(loader.getAddressBuildingName());
		TypedQuery<MTambon> tumbonlist = this.entityManager.createNamedQuery("MTambon.findByTambonNameTh", MTambon.class);
		tumbonlist.setParameter("tambonNameTh", loader.getAddressTambon());
		home.setTambonId(tumbonlist.getSingleResult().getTambonId());
		TypedQuery<MAmphur> amphurlist = this.entityManager.createNamedQuery("MAmphur.findByAmphurNameTh", MAmphur.class);
		amphurlist.setParameter("amphurNameTh", loader.getAddressAmphur());
		home.setAmphurId(amphurlist.getSingleResult().getAmphurId());
		TypedQuery<MProvince> provincelist = this.entityManager.createNamedQuery("MProvince.findByProvinceNameTh", MProvince.class);
		provincelist.setParameter("provinceNameTh", loader.getAddressProvince());
		home.setProvinceId(provincelist.getSingleResult().getProvinceId());
		home.setPosCode(loader.getAddressPostcode());
		return home;
	}

	public void cancel(String code) {
		TypedQuery<InvoiceLoader> list = this.entityManager.createNamedQuery("InvoiceLoader.findByInvoiceLoaderCode", InvoiceLoader.class);
		list.setParameter("invoiceLoaderCode", code);
		List<InvoiceLoader> loaders = null;
		try {
			loaders = list.getResultList();
			for(InvoiceLoader loader : loaders ){
				loader.setCancelDate(new Date());
			}
			invoiceLoaderRepository.save(loaders);
		} catch (Exception ex) {
			log.error("cancel invoice Loader : "+ex.getMessage());
			ex.printStackTrace();
		}
	}

	public void delete(String code){
		TypedQuery<InvoiceLoader> list = this.entityManager.createNamedQuery("InvoiceLoader.findByInvoiceLoaderCode", InvoiceLoader.class);
		list.setParameter("invoiceLoaderCode", code);
		List<InvoiceLoader> loaders = null;
		try {
			loaders = list.getResultList();
			invoiceLoaderRepository.delete(loaders);
		} catch (Exception ex) {
			log.error("delete invoice Loader : "+ex.getMessage());
			ex.printStackTrace();
		}
	}
}
