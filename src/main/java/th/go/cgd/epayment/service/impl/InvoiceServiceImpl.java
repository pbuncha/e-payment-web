/**
 * 
 */
package th.go.cgd.epayment.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import net.sf.jasperreports.engine.JRException;
import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.Address;
import th.go.cgd.epayment.entity.Catalog;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.InvoiceItem;
import th.go.cgd.epayment.entity.MTitleName;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.AddressBean;
import th.go.cgd.epayment.model.AttachmentBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.EmailBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.InvoiceCodeBean;
import th.go.cgd.epayment.model.InvoiceItemBean;
import th.go.cgd.epayment.report.service.AbstractJasperReport.ReportType;
import th.go.cgd.epayment.report.service.ReportUtilService;
import th.go.cgd.epayment.repository.AddressRepository;
import th.go.cgd.epayment.repository.CatalogRepository;
import th.go.cgd.epayment.repository.InvoiceItemRepository;
import th.go.cgd.epayment.repository.InvoiceRepository;
import th.go.cgd.epayment.repository.MAmphurRepository;
import th.go.cgd.epayment.repository.MProvinceRepository;
import th.go.cgd.epayment.repository.MTambonRepository;
import th.go.cgd.epayment.repository.MTitleNameRepository;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.service.interfaces.IBarcodeService;
import th.go.cgd.epayment.service.interfaces.IEmailServiceImpl;
import th.go.cgd.epayment.service.interfaces.IGenerateCodeService;
import th.go.cgd.epayment.service.interfaces.IInvoiceService;

/**
 * @author wichuda.k Nov 12, 2017
 *
 */
@Service
@Transactional
public class InvoiceServiceImpl implements IInvoiceService {
	private static final Logger log = Logger.getLogger(InvoiceServiceImpl.class);

	private @PersistenceContext EntityManager entityManager;
	private @Autowired InvoiceRepository invoiceRepository;
	private @Autowired InvoiceItemRepository invoiceItemRepository;
	private @Autowired UserRepository userRepository;
	private @Autowired MTitleNameRepository titleNameRepository;
	private @Autowired CatalogRepository catalogRepository;
	private @Autowired MProvinceRepository mProvinceRepository;
	private @Autowired MAmphurRepository mAmphurRepository;
	private @Autowired MTambonRepository mTambonRepository;
	private @Autowired IGenerateCodeService generateCodeService;
	private @Autowired MessageResource messageResource;
	private @Autowired IEmailServiceImpl emailService;
	private @Autowired ReportServiceImpl reportService;
	private @Autowired IBarcodeService barcodeService ;
	private @Autowired AddressRepository addressRepository;
	public static ResourceBundle bundle = ResourceBundle.getBundle("TemplateEmail");
	public InvoiceBean save(InvoiceBean invoiceBean) {
		Invoice invoiceTb = new Invoice();
		User userTb = userRepository.findOne(1);
		Date nowDate = new Date();
		InvoiceCodeBean invoice = new InvoiceCodeBean();
		try {
			if(invoiceBean.getId()!= null){	
				invoiceTb = invoiceRepository.findOne(invoiceBean.getId());
				if(invoiceBean.getHomeIsBill()){
					invoiceTb.setInvoiceAddressIdBilling(null);
				}
			}else{
				invoice = generateCodeService.generateInvoiceCode(nowDate);
				invoiceTb.setInvoiceNo(invoice.getInvoiceCode());
				invoiceTb.setRef1(invoice.getRef1());
				invoiceTb.setRef2(invoice.getRef2());
				invoiceTb.setCreatedDate(nowDate);
				invoiceTb.setCreatedBy(userTb);
			}

			invoiceTb.setInvoiceType('1');
			invoiceTb.setUserId(userTb);
			invoiceTb.setUserTypeId('1');
			invoiceTb.setCitizenNo(invoiceBean.getCitizenNo());
			invoiceTb.setPersonTypeId(invoiceBean.getPersTypeId());
			if(invoiceBean.getPersTypeId() == 1){
				if (!"".equals(invoiceBean.getTitleId())) {
					MTitleName titleNameTb = titleNameRepository.findOne(Integer.valueOf(invoiceBean.getTitleId()));
					invoiceTb.setTitleId(titleNameTb);
				}
				invoiceTb.setFirstName(invoiceBean.getFirstName());
				invoiceTb.setMiddleName(invoiceBean.getMidName());
				invoiceTb.setLastName(invoiceBean.getLastName());
			}else if(invoiceBean.getPersTypeId() == 2){
				if (!"".equals(4)) {
					MTitleName titleNameTb = titleNameRepository.findOne(Integer.valueOf(4));
					invoiceTb.setTitleId(titleNameTb);
				}
				invoiceTb.setFirstName(invoiceBean.getCompName());
				invoiceTb.setLastName(" ");
			}else if(invoiceBean.getPersTypeId() == 3){
				if (!"".equals(4)) {
					MTitleName titleNameTb = titleNameRepository.findOne(Integer.valueOf(4));
					invoiceTb.setTitleId(titleNameTb);
				}
				invoiceTb.setFirstName(invoiceBean.getGroupName());
				invoiceTb.setLastName(" ");
			}
			invoiceTb.setTelephone(invoiceBean.getMobile());
			invoiceTb.setEmail(invoiceBean.getEmail());


			//address
			log.info("getHomeIsBill "+invoiceBean.getHomeIsBill());
			if(invoiceBean.getAddrHome()!=null){
				Address addrHome = new Address();
				addrHome = beanToEntity(invoiceBean.getAddrHome(),userTb);
				addrHome.setIsBilling(invoiceBean.getHomeIsBill()?'1':'0');
				addrHome.setIsRegistration('1');
				invoiceTb.setAddressIdBilling(addrHome);
			}

			//address
			if(!invoiceBean.getHomeIsBill()){
				if(invoiceBean.getAddrBilling()!=null){
					log.info("setInvoiceAddressIdBilling");
					Address addrBilling = new Address();
					addrBilling = beanToEntity(invoiceBean.getAddrBilling(),userTb);
					addrBilling.setIsBilling('1');
					addrBilling.setIsRegistration('0');
					invoiceTb.setInvoiceAddressIdBilling(addrBilling);
				}
			}
			invoiceTb.setTotalAmount(invoiceBean.getTotalAmount());
			invoiceTb.setStartDate(invoiceBean.getStartDate());
			invoiceTb.setEndDate(invoiceBean.getEndDate());
			invoiceTb.setStatus('0');


			invoiceTb.setUpdatedDate(nowDate);
			invoiceTb.setUpdatedBy(userTb);
			invoiceTb.setPaymentDate(nowDate);

			List<InvoiceItem> invoiceItemList = new ArrayList<InvoiceItem>();
			for (InvoiceItemBean invoiceItemBean : invoiceBean.getItems()) {
				InvoiceItem invoiceItem = new InvoiceItem();
				invoiceItem.setInvoiceId(invoiceTb);
				if(invoiceItemBean.getId()!= null){
					invoiceItem = invoiceItemRepository.findOne(invoiceItemBean.getId());
				}else{
					invoiceItem.setCreatedDate(nowDate);
					invoiceItem.setCreatedBy(userTb);
				}
				if (!"".equals(invoiceItemBean.getCatalogId())) {
					Catalog catalogTb = catalogRepository.findOne(Integer.valueOf(invoiceItemBean.getCatalogId()));
					invoiceItem.setCatalogId(catalogTb);
				}

				invoiceItem.setAmount(invoiceItemBean.getAmount());			
				invoiceItem.setUpdatedDate(nowDate);
				invoiceItem.setUpdatedBy(userTb);
				invoiceItem.setInvoiceItemName(invoiceItemBean.getItemName());
				invoiceItemList.add(invoiceItem);
			}
			if(invoiceBean.getDeleteItem()!= null){
				log.info("invoice no "+invoiceTb.getInvoiceNo()+" : delete "+invoiceBean.getDeleteItem().size()+" invoice item .");
				for(InvoiceItemBean invoiceItemBean : invoiceBean.getDeleteItem()){
					if(invoiceItemBean.getId() != null){
						invoiceItemRepository.delete(invoiceItemBean.getId());
					}
				}
			}
			invoiceTb.setInvoiceItemList(invoiceItemList);


			invoiceRepository.save(invoiceTb);
			EmailBean mail = new EmailBean();
			try{
				mail.setTo(new String[]{invoiceTb.getEmail()});
				String fullName = (invoiceBean.getFirstName()!= null ? invoiceTb.getFirstName() : "")+" "+(invoiceTb.getLastName() != null ? invoiceTb.getLastName() : "");
				Object[] objArray = {fullName,invoiceTb.getInvoiceNo(), invoiceTb.getRef1(),invoiceTb.getRef2()};
				String  content = emailService.getTemplateMail(objArray,"invoice");
				mail.setText(content);			
				List<AttachmentBean> attachments = new ArrayList<AttachmentBean>();
				AttachmentBean attachmentBean = new AttachmentBean();
				attachmentBean.setData(generateInvoiceRpt(invoiceTb));
				attachmentBean.setFilename("invoice.pdf");
				attachmentBean.setMimeType(EPaymentConstant.CONTENT_TYPE_PDF);
				attachments.add(attachmentBean);
				mail.setAttachments(attachments);
				mail.setSubject(bundle.getString("mail.subject.invoice"));
				emailService.sendMail(mail);
			}catch(Exception e){
				log.info(e.getMessage());
			}

			return invoiceBean;
		} catch (Exception e) {
			log.error(e);
			e.printStackTrace();
			return null;
		}
	}

	public byte[] generateInvoiceRpt(Invoice invoiceTb) throws JRException{
		HashMap<String,Object> params = new HashMap<>();
		params.put("invoiceId", invoiceTb.getInvoiceId());
		params.put("barcode", barcodeService.getDataBarCode(invoiceTb));
		params.put("qrcode",barcodeService.getDataQRCode(invoiceTb));
		params.put("REPORT_DIR",getClass().getClassLoader().getResource("jasper").getPath());
		return reportService.genarateReport(params, ReportType.PDF);
	}

	@Transactional(readOnly = true)
	public List<InvoiceBean> getInvoiceByCriteria(InvoiceBean invoiceBean){
		List<Integer> invoices = null;

		StringBuffer queryStr = new StringBuffer("SELECT i.invoiceId ");
		queryStr.append(" from Invoice i ");
		queryStr.append(" LEFT JOIN i.invoiceItemList as it ");
		queryStr.append(" LEFT JOIN it.catalogId as c ");
		queryStr.append(" LEFT JOIN c.catalogStructureList cs ");
		queryStr.append(" LEFT JOIN cs.catalogStructureItemList csi ");
		queryStr.append(" LEFT JOIN cs.catalogCostCenterList cc ");
		queryStr.append(" WHERE (1=1) ");
		//WHERE criteria
		if(!(invoiceBean.getDepartmentId()== null)){
			queryStr.append(" AND csi.departmentId = '"+invoiceBean.getDepartmentId()+"' ");
		}
		if(!(invoiceBean.getCostCenterPayId()== null)){
			queryStr.append(" AND csi.costCenterId = '"+invoiceBean.getCostCenterPayId()+"' ");
		}
		if(!(invoiceBean.getCostCenterId()== null)){
			queryStr.append(" AND cc.costCenterId = '"+invoiceBean.getCostCenterId()+"' ");
		}
		if(!(invoiceBean.getStatus()== null)){
			queryStr.append(" AND i.status = '"+invoiceBean.getStatus()+"' ");
		}
		if(!(invoiceBean.getCatalogId()== null)){
			queryStr.append(" AND it.catalogId = '"+invoiceBean.getCatalogId()+"' ");
		}
		if(!(invoiceBean.getItemName()== null)){
			queryStr.append(" AND it.invoiceItemId LIKE '%"+invoiceBean.getItemName()+"%' ");
		}
		if(!(invoiceBean.getInvoiceNo()== null)){
			queryStr.append(" AND i.invoiceNo LIKE '%"+invoiceBean.getInvoiceNo()+"%' ");
		}
		if(!(invoiceBean.getRef1()== null)){
			queryStr.append(" AND i.ref1 LIKE '%"+invoiceBean.getRef1()+"%' ");
		}
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
		if(!(invoiceBean.getStartDate()== null)){
			queryStr.append(" AND TO_CHAR(i.startDate,'yyyyMMdd') >= '"+dt1.format(invoiceBean.getStartDate())+"' ");
		}
		if(!(invoiceBean.getEndDate()== null)){
			queryStr.append(" AND TO_CHAR(i.endDate,'yyyyMMdd') <= '"+dt1.format(invoiceBean.getEndDate())+"' ");
		}
		if(!(invoiceBean.getCitizenNo()== null)){
			queryStr.append(" AND i.citizenNo LIKE '%"+invoiceBean.getCitizenNo()+"%' ");
		}
		if(!(invoiceBean.getName()== null)){
			queryStr.append(" AND (i.firstName LIKE '%"+invoiceBean.getName()+"%' OR i.middleName LIKE '%"+invoiceBean.getName()+"%' OR i.lastName LIKE '%"+invoiceBean.getName()+"%') ");
		}

		queryStr.append(" GROUP BY i.invoiceId ");

		TypedQuery<Integer> query = this.entityManager.createQuery(queryStr.toString(),Integer.class);

		try {
			List<InvoiceBean> invoiceBeans = new ArrayList<InvoiceBean>();
			invoices = query.getResultList();
			for(Integer invoiceId:invoices){
				Invoice invoice = invoiceRepository.findOne(invoiceId);

				InvoiceBean tmp = new InvoiceBean();
				tmp.setId(invoice.getInvoiceId());
				tmp.setInvoiceNo(invoice.getInvoiceNo());
				tmp.setRef1(invoice.getRef1());
				String name = invoice.getFirstName();
				if (!invoice.getLastName().isEmpty()) {
					name += " " + invoice.getLastName();
				}
				if (!(invoice.getTitleId() == null)) {
					name = invoice.getTitleId().getTitleName() + name;
				}
				tmp.setName(name);
				tmp.setCitizenNo(invoice.getCitizenNo());
				tmp.setTotalAmount(invoice.getTotalAmount());
				if(invoice.getStartDate()!=null){
					Date parseStartDate = new Date(invoice.getStartDate().getYear()+543,invoice.getStartDate().getMonth(),invoice.getStartDate().getDate());
					tmp.setStartDate(parseStartDate);
				}
				if(invoice.getEndDate()!=null){
					Date parseEndDate = new Date(invoice.getEndDate().getYear()+543,invoice.getEndDate().getMonth(),invoice.getEndDate().getDate());
					tmp.setEndDate(parseEndDate);
				}
				tmp.setStatus(String.valueOf(invoice.getStatus()));
				invoiceBeans.add(tmp);
			}
			return invoiceBeans;
		} catch (Exception ex) {
			ex.printStackTrace();
			log.error(ex.getMessage());

			return null;
		}
	}

	public InvoiceBean getInvoidByUser(Integer userId) {
		Invoice invoiceTb = invoiceRepository.findOne(userId);
		InvoiceBean invoiceBean = null;
		if (invoiceTb != null) {
			invoiceBean = new InvoiceBean();
			List<InvoiceItemBean> itemsList = new ArrayList<InvoiceItemBean>();
			invoiceBean.setId(invoiceTb.getInvoiceId());
			invoiceBean.setInvoiceNo(invoiceTb.getInvoiceNo());
			invoiceBean.setTotalAmount(invoiceTb.getTotalAmount());
			for (InvoiceItem invoiceItem : invoiceTb.getInvoiceItemList()) {
				Catalog catalogTb = invoiceItem.getCatalogId();
				InvoiceItemBean invoiceItemBean = new InvoiceItemBean();
				invoiceItemBean.setId(invoiceItem.getInvoiceItemId());
				invoiceItemBean.setAmount(invoiceItem.getAmount());
				invoiceItemBean.setCatalogId(catalogTb.getCatalogId());
				invoiceItemBean.setCatalogName(catalogTb.getCatalogName());
				invoiceItemBean.setBreakDate(catalogTb.getBreakDate());
				invoiceItemBean.setBreakReason(catalogTb.getBreakReason());

				itemsList.add(invoiceItemBean);
			}
			invoiceBean.setItems(itemsList);
		}

		return invoiceBean;
	}

	public Invoice getInvoidByNo(String invoidNo) {
		TypedQuery<Invoice> invoiceList = this.entityManager.createNamedQuery("Invoice.findByInvoiceNo", Invoice.class);
		invoiceList.setParameter("invoiceNo", invoidNo);
		Invoice invoiceTb = null;
		try {
			invoiceTb = invoiceList.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return invoiceTb;

	}

	public Invoice getInvoidById(Integer invoidId) {
		return invoiceRepository.getOne(invoidId);
	}

	public void cancelInvoice(Integer id){
		Invoice invoice = invoiceRepository.getOne(id);
		invoice.setStatus('3');
		invoiceRepository.save(invoice);
	}

	public void cancelInvoice(List<Integer> id){
		List<Invoice> list = new ArrayList<>();
		for(int i=0; i<id.size(); i++){
			Invoice invoice = invoiceRepository.getOne(id.get(i));
			invoice.setStatus('3');		
			list.add(invoice);
		}
		invoiceRepository.save(list);
	}

	public  List<DropdownBean> getCatalogList(Integer catalogTypeId) {
		log.debug("getCatalogList by catalogTypeId");
		List<Catalog> list = catalogRepository.getCatalogBy(EPaymentConstant.CATALOG_STATUS.ACTIVE.charAt(0), catalogTypeId);
		List<DropdownBean> result = new ArrayList<DropdownBean>();
		for(int i =0 ; i < list.size(); i++){
			Catalog obj =  list.get(i);
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getCatalogId().toString());
			dropdownBean.setDesc(obj.getCatalogName().toString());
			dropdownBean.setDesc2(String.valueOf(obj.getAmount()));
			result.add(dropdownBean);			
		}
		return result;
	}

	public List<DropdownBean> getinvoiceItem() {
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<InvoiceItem> invoiceItemTbList = invoiceRepository.getInvoiceItem();

		for (InvoiceItem obj : invoiceItemTbList) {
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getInvoiceItemId().toString());
			dropdownBean.setDesc(obj.getInvoiceItemName() + "");
			dropdownBeanList.add(dropdownBean);
		}

		return dropdownBeanList;
	}

	public byte[] exportExcel( List<InvoiceBean> invoiceBeans){
		String sheetName = "sheet 1";
		Object[][] data = new Object[invoiceBeans.size()+1][8];

		data[0][0] = messageResource.getMessage("invoice.header.invoiceNo");
		data[0][1] = messageResource.getMessage("invoice.header.ref1");
		data[0][2] = messageResource.getMessage("invoice.header.name");
		data[0][3] = messageResource.getMessage("invoice.header.citizenNo");
		data[0][4] = messageResource.getMessage("invoice.header.totalAmount");
		data[0][5] = messageResource.getMessage("invoice.header.startDate");
		data[0][6] = messageResource.getMessage("invoice.header.endDate");
		data[0][7] = messageResource.getMessage("invoice.header.status");

		for(int i = 0 ;i<invoiceBeans.size();i++){
			data[i+1][0] = invoiceBeans.get(i).getInvoiceNo();
			data[i+1][1] = invoiceBeans.get(i).getRef1();
			data[i+1][2] = invoiceBeans.get(i).getName();
			data[i+1][3] = invoiceBeans.get(i).getCitizenNo();
			data[i+1][4] = invoiceBeans.get(i).getTotalAmount();
			data[i+1][5] = invoiceBeans.get(i).getStartDate();
			data[i+1][6] = invoiceBeans.get(i).getEndDate();
			data[i+1][7] = getStatusName(invoiceBeans.get(i).getStatus());
		}

		byte[] output = ReportUtilService.genarateCustomExcelReport(data, sheetName);
		return output;

	}

	private String getStatusName(String status){
		switch ( status )
		{
		case EPaymentConstant.INVOICE_STATUS.WAIT_PAY
		:     return  messageResource.getMessage("invoice.status.waitpay");
		case EPaymentConstant.INVOICE_STATUS.CONFIRM_PAY
		:     return messageResource.getMessage("invoice.status.confirmpay");
		case EPaymentConstant.INVOICE_STATUS.CANCEL
		:     return messageResource.getMessage("invoice.status.cancel");
		case EPaymentConstant.INVOICE_STATUS.PAYMENT_ONLINE
		:     return messageResource.getMessage("invoice.status.payment.online");
		case EPaymentConstant.INVOICE_STATUS.PAYMENT_FAIL
		:     return messageResource.getMessage("invoice.status.payment.fail");
		default
		: return "สถานะไม่ถูกต้อง";

		}
	}

	private AddressBean entityToBean(Address addr){
		AddressBean bean = new AddressBean();
		bean.setId(addr.getAddressId());
		bean.setNo(addr.getNo());
		bean.setMoo(addr.getMoo());
		bean.setRoad(addr.getRoad());
		bean.setSoi(addr.getSoi());
		bean.setVillage(addr.getVillage());
		bean.setTambonId(addr.getTambonId().getTambonId());
		bean.setAmphurId(addr.getAmphurId().getAmphurId());
		bean.setProvinceId(addr.getProvinceId().getProvinceId());
		bean.setPosCode(addr.getPostcode());
		return bean;
	}

	private Address beanToEntity(AddressBean bean, User user){
		Address addr = new Address();
		Date now = new Date();

		if(bean.getId()!= null){	
			addr = addressRepository.findOne(bean.getId());
		}else{
			addr.setCreatedBy(user);
			addr.setCreatedDate(now);
		}
		addr.setNo(bean.getNo());
		addr.setVillage(bean.getVillage());
		addr.setMoo(bean.getMoo());
		addr.setSoi(bean.getSoi());
		addr.setRoad(bean.getRoad());
		addr.setLane(bean.getLane());
		addr.setProvinceId(mProvinceRepository.getOne(bean.getProvinceId()));
		addr.setAmphurId(mAmphurRepository.getOne(bean.getAmphurId()));
		addr.setTambonId(mTambonRepository.getOne(bean.getTambonId()));
		addr.setPostcode(bean.getPosCode());
		addr.setUpdatedDate(now);
		addr.setUpdatedBy(user);
		return addr;
	}

	private InvoiceBean entityToBean(Invoice invoice){
		InvoiceBean bean = new InvoiceBean();
		bean.setId(invoice.getInvoiceId());
		bean.setPersTypeId(1);
		bean.setCitizenNo(invoice.getCitizenNo());
		bean.setTitleId(invoice.getTitleId().getTitleId());
		bean.setPersTypeId(invoice.getPersonTypeId());
		if(invoice.getPersonTypeId().equals(1)){
			bean.setFirstName(invoice.getFirstName());
			bean.setMidName(invoice.getMiddleName());
			bean.setLastName(invoice.getLastName());
		}else if(invoice.getPersonTypeId().equals(2)){
			bean.setCompName(invoice.getFirstName());
		}else if(invoice.getPersonTypeId().equals(3)){
			bean.setGroupName(invoice.getFirstName());
		}
		bean.setMobile(invoice.getTelephone());
		bean.setEmail(invoice.getEmail());
		bean.setStartDate(invoice.getStartDate());
		bean.setEndDate(invoice.getEndDate());
		bean.setTotalAmount(invoice.getTotalAmount());
		AddressBean home = new AddressBean();
		home = entityToBean(invoice.getAddressIdBilling());
		bean.setAddrHome(home);
		bean.setHomeIsBill(true);
		AddressBean billing = new AddressBean();
		if(invoice.getInvoiceAddressIdBilling()!=null){
			bean.setHomeIsBill(false);				
			billing = entityToBean(invoice.getInvoiceAddressIdBilling());				
		}
		bean.setAddrBilling(billing);

		return bean;
	}

	public InvoiceBean getInvoice(Integer id){
		Invoice invoice = invoiceRepository.findOne(id);
		InvoiceBean bean = new InvoiceBean();
		if(invoice != null){
			bean = entityToBean(invoice);
			List<InvoiceItemBean> invoiceItemBeans = new ArrayList<InvoiceItemBean>();
			for(InvoiceItem invoiceItem : invoice.getInvoiceItemList()){
				InvoiceItemBean invoiceItemBean = new InvoiceItemBean();
				invoiceItemBean.setId(invoiceItem.getInvoiceItemId());
				invoiceItemBean.setItemName(invoiceItem.getInvoiceItemName());
				invoiceItemBean.setAmount(invoiceItem.getAmount());

				Catalog catalog = invoiceItem.getCatalogId();
				invoiceItemBean.setCatalogId(catalog.getCatalogId());
				invoiceItemBean.setCatalogName(catalog.getCatalogName());
				invoiceItemBean.setmCatalogId(catalog.getCatalogTypeId().getCatalogTypeId());

				invoiceItemBeans.add(invoiceItemBean);
			}
			bean.setItems(invoiceItemBeans);

		}
		return bean;
	}

	public List<DropdownBean> getCatalogList(){
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<Catalog> invoiceItemTbList = catalogRepository.findAll();

		for (Catalog obj : invoiceItemTbList) {
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getCatalogId().toString());
			dropdownBean.setDesc(obj.getCatalogName());
			dropdownBeanList.add(dropdownBean);
		}

		return dropdownBeanList;
	}

	public InvoiceBean getInvoiceBy(String citizenNo){
		log.debug("getinvoice by citizen no");
		InvoiceBean bean = new InvoiceBean();
		TypedQuery<Invoice> invoiceList = this.entityManager.createNamedQuery("Invoice.findByCitizenNo", Invoice.class);
		invoiceList.setParameter("citizenNo", citizenNo);
		List<Invoice> invoiceTb = null;
		try {

			invoiceTb = invoiceList.getResultList();
			if(invoiceTb.size()>0){
				bean = entityToBean(invoiceTb.get(0));
				return bean;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}


}
