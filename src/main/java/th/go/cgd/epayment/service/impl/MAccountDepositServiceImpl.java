package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.MAccountDeposit;
import th.go.cgd.epayment.entity.MMoneySource;
import th.go.cgd.epayment.model.master.AccountDepositBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.repository.MAccountDepositRepository;
import th.go.cgd.epayment.service.interfaces.IMAccountDepositService;

@Service
@Transactional
public class MAccountDepositServiceImpl implements IMAccountDepositService {

	@Autowired
	private MAccountDepositRepository repository;
	
	@Override
	public void createMAccountDeposit(AccountDepositBean objBean) {

		MAccountDeposit objDB = new MAccountDeposit();

		objDB.setAccountCode(objBean.getAccountCode());
		objDB.setAccountName(objBean.getAccountName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setOwner(objBean.getOwner());
		objDB.setRemark(objBean.getRemark());
		objDB.setStatus(objBean.getStatus());
		objDB.setCreatedBy(1);
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<AccountDepositBean> getAllMAccountDeposits() {
				
		List<AccountDepositBean> objBeanList = new ArrayList<AccountDepositBean>();
		
		for (MAccountDeposit objDB : repository.findAll()) {
			
			AccountDepositBean objBean = new AccountDepositBean();

			objBean.setAccountDepositId(objDB.getAccountDepositId());
			objBean.setAccountCode(objDB.getAccountCode());
			objBean.setAccountName(objDB.getAccountName());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setOwner(objDB.getOwner());
			objBean.setRemark(objDB.getRemark());
			objBean.setStatus(objDB.getStatus());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMAccountDeposit(AccountDepositBean objBean) {

		MAccountDeposit objDB = repository.findOne(objBean.getAccountDepositId());
		
		objDB.setAccountCode(objBean.getAccountCode());
		objDB.setAccountName(objBean.getAccountName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setOwner(objBean.getOwner());
		objDB.setRemark(objBean.getRemark());
		objDB.setStatus(objBean.getStatus());
//		objDB.setCreatedBy(1);
//		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(1);
		objDB.setUpdatedDate(new Date());
		
		repository.save(objDB);
	}

	@Override
	public void deleteMAccountDeposit(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public AccountDepositBean getMAccountDepositById(Integer id) {
		
		AccountDepositBean objBean = new AccountDepositBean();
		
		MAccountDeposit objDB = repository.findOne(id);
		
		objBean.setAccountDepositId(objDB.getAccountDepositId());
		objBean.setAccountCode(objDB.getAccountCode());
		objBean.setAccountName(objDB.getAccountName());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setOwner(objDB.getOwner());
		objBean.setRemark(objDB.getRemark());
		objBean.setStatus(objDB.getStatus());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy());
		objBean.setUpdatedDate(objBean.getUpdatedDate());
		
		return objBean;
	}
	
	@Override
	public List<AccountDepositBean> getMAccountDepositByCriteria(AccountDepositBean inputObj) {
		
		List<AccountDepositBean> q =  getAllMAccountDeposits();

		if (!StringUtils.isEmpty(inputObj.getAccountCode()))
		{
			q = q.stream()
					.filter(a -> a.getAccountCode() != null)
					.filter(a -> a.getAccountCode().contains(inputObj.getAccountCode()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getAccountName()))
		{
			q = q.stream()
					.filter(a -> a.getAccountName() != null)
					.filter(a -> a.getAccountName().contains(inputObj.getAccountName()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getOwner()))
		{
			q = q.stream()
					.filter(a -> a.getOwner() != null)
					.filter(a -> a.getOwner().contains(inputObj.getOwner()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getRemark()))
		{
			q = q.stream()
					.filter(a -> a.getRemark() != null)
					.filter(a -> a.getRemark().contains(inputObj.getRemark()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate() != null)
					.filter(a -> a.getStartDate().getTime() >= inputObj.getStartDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= inputObj.getStartDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= inputObj.getEndDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= inputObj.getEndDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		MAccountDeposit q = repository.findOne(id);		
		if (q != null)
		{
			q.setStatus("3");
			
			repository.save(q);
		}		
	}
}
