package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.MBusinessArea;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MMoneySource;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.master.BusinessAreaBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.repository.MBusinessAreaRepository;
import th.go.cgd.epayment.service.interfaces.IMBusinessAreaService;

@Service
@Transactional
public class MBusinessAreaServiceImpl implements IMBusinessAreaService {

	@Autowired
	private MBusinessAreaRepository repository;
	
	@Override
	public void createMBusinessArea(BusinessAreaBean objBean) {

		MBusinessArea objDB = new MBusinessArea();

		objDB.setBusinessAreaCode(objBean.getBusinessAreaCode());
		objDB.setBusinessAreaName(objBean.getBusinessAreaName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setProvinceId(objBean.getProvinceId() == null ? null : new MProvince(objBean.getProvinceId()));
		objDB.setCreatedBy(new User(1));
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<BusinessAreaBean> getAllMBusinessAreas() {
				
		List<BusinessAreaBean> objBeanList = new ArrayList<BusinessAreaBean>();
		
		for (MBusinessArea objDB : repository.findAll()) {
			
			BusinessAreaBean objBean = new BusinessAreaBean();

			objBean.setBusinessAreaId(objDB.getBusinessAreaId());
			objBean.setBusinessAreaCode(objDB.getBusinessAreaCode());
			objBean.setBusinessAreaName(objDB.getBusinessAreaName());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setStatus(objDB.getStatus());
			objBean.setProvinceId(objDB.getProvinceId() == null ? null : objDB.getProvinceId().getProvinceId());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMBusinessArea(BusinessAreaBean objBean) {

		MBusinessArea objDB = repository.findOne(objBean.getBusinessAreaId());
		
		objDB.setBusinessAreaCode(objBean.getBusinessAreaCode());
		objDB.setBusinessAreaName(objBean.getBusinessAreaName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setProvinceId(objBean.getProvinceId() == null ? null : new MProvince(objBean.getProvinceId()));
//		objDB.setCreatedBy(new User(1));
//		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(new User(1));
		objDB.setUpdatedDate(new Date());
		
		repository.save(objDB);
	}

	@Override
	public void deleteMBusinessArea(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public BusinessAreaBean getMBusinessAreaById(Integer id) {
		
		BusinessAreaBean objBean = new BusinessAreaBean();
		
		MBusinessArea objDB = repository.findOne(id);
		
		objBean.setBusinessAreaId(objDB.getBusinessAreaId());
		objBean.setBusinessAreaCode(objDB.getBusinessAreaCode());
		objBean.setBusinessAreaName(objDB.getBusinessAreaName());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setStatus(objDB.getStatus());
		objBean.setProvinceId(objDB.getProvinceId() == null ? null : objDB.getProvinceId().getProvinceId());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
		objBean.setUpdatedDate(objBean.getUpdatedDate());

		
		return objBean;
	}
	
	@Override
	public List<BusinessAreaBean> getMBusinessAreaByCriteria(BusinessAreaBean inputObj) {
		
		List<BusinessAreaBean> q =  getAllMBusinessAreas();

		if (!StringUtils.isEmpty(inputObj.getBusinessAreaCode()))
		{
			q = q.stream()
					.filter(a -> a.getBusinessAreaCode() != null)
					.filter(a -> a.getBusinessAreaCode().contains(inputObj.getBusinessAreaCode()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getBusinessAreaName()))
		{
			q = q.stream()
					.filter(a -> a.getBusinessAreaName() != null)
					.filter(a -> a.getBusinessAreaName().contains(inputObj.getBusinessAreaName()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate() != null)
					.filter(a -> a.getStartDate().getTime() >= inputObj.getStartDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= inputObj.getStartDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= inputObj.getEndDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= inputObj.getEndDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		MBusinessArea q = repository.findOne(id);		
		if (q != null)
		{
			q.setStatus("3");
			
			repository.save(q);
		}		
	}
	@Override
	public MBusinessArea findBusinessAreaById(Integer id) {
	    return repository.findOne(id);
	}
}
