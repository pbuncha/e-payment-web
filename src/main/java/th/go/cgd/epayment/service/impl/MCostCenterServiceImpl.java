package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.MBusinessArea;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MDisbursementUnit;
import th.go.cgd.epayment.entity.MGl;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.master.CostCenterBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.service.interfaces.IMCostCenterService;

@Service
@Transactional
public class MCostCenterServiceImpl implements IMCostCenterService {

	@Autowired
	private MCostCenterRepository repository;
	
	@Override
	public void createMCostCenter(CostCenterBean objBean) {

		MCostCenter objDB = new MCostCenter();

		objDB.setCostCenterCode(objBean.getCostCenterCode());
		objDB.setCostCenterName(objBean.getCostCenterName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setBusinessAreaId(objBean.getBusinessAreaId() == null ? null : new MBusinessArea(objBean.getBusinessAreaId()));
		objDB.setDepartmentId(objBean.getDepartmentId() == null ? null : new MDepartment(objBean.getDepartmentId()));
		objDB.setDisbursementUnitId(objBean.getDisbursementUnitId() == null ? null : new MDisbursementUnit(objBean.getDisbursementUnitId()));		
		objDB.setCreatedBy(new User(1));
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<CostCenterBean> getAllMCostCenters() {
				
		List<CostCenterBean> objBeanList = new ArrayList<CostCenterBean>();
		
		for (MCostCenter objDB : repository.findAll()) {
			
			CostCenterBean objBean = new CostCenterBean();

			objBean.setCostCenterId(objDB.getCostCenterId());
			objBean.setCostCenterCode(objDB.getCostCenterCode());
			objBean.setCostCenterName(objDB.getCostCenterName());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setStatus(objDB.getStatus());
			objBean.setBusinessAreaId(objDB.getBusinessAreaId() == null ? null : objDB.getBusinessAreaId().getBusinessAreaId());
			objBean.setDepartmentId(objDB.getDepartmentId() == null ? null : objDB.getDepartmentId().getDepartmentId());
			objBean.setDisbursementUnitId(objDB.getDisbursementUnitId() == null ? null : objDB.getDisbursementUnitId().getDisbursementUnitId());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMCostCenter(CostCenterBean objBean) {

		MCostCenter objDB = repository.findOne(objBean.getCostCenterId());
		
		objDB.setCostCenterCode(objBean.getCostCenterCode());
		objDB.setCostCenterName(objBean.getCostCenterName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setBusinessAreaId(objBean.getBusinessAreaId() == null ? null : new MBusinessArea(objBean.getBusinessAreaId()));
		objDB.setDepartmentId(objBean.getDepartmentId() == null ? null : new MDepartment(objBean.getDepartmentId()));
		objDB.setDisbursementUnitId(objBean.getDisbursementUnitId() == null ? null : new MDisbursementUnit(objBean.getDisbursementUnitId()));		
//		objDB.setCreatedBy(new User(1));
//		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(new User(1));
		objDB.setUpdatedDate(new Date());

		
		repository.save(objDB);
	}

	@Override
	public void deleteMCostCenter(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public CostCenterBean getMCostCenterById(Integer id) {
		
		CostCenterBean objBean = new CostCenterBean();
		
		MCostCenter objDB = repository.findOne(id);
		
		objBean.setCostCenterId(objDB.getCostCenterId());
		objBean.setCostCenterCode(objDB.getCostCenterCode());
		objBean.setCostCenterName(objDB.getCostCenterName());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setStatus(objDB.getStatus());
		objBean.setBusinessAreaId(objDB.getBusinessAreaId() == null ? null : objDB.getBusinessAreaId().getBusinessAreaId());
		objBean.setDepartmentId(objDB.getDepartmentId() == null ? null : objDB.getDepartmentId().getDepartmentId());
		objBean.setDisbursementUnitId(objDB.getDisbursementUnitId() == null ? null : objDB.getDisbursementUnitId().getDisbursementUnitId());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
		objBean.setUpdatedDate(objBean.getUpdatedDate());
		
		return objBean;
	}

	@Override
	public List<CostCenterBean> getMCostCenterByCriteria(CostCenterBean inputObj) {
		
		List<CostCenterBean> q =  getAllMCostCenters();

		if (!StringUtils.isEmpty(inputObj.getCostCenterCode() ))
		{
			q = q.stream()
					.filter(a -> a.getCostCenterCode() != null)
					.filter(a -> a.getCostCenterCode().contains(inputObj.getCostCenterCode()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getCostCenterName()))
		{
			q = q.stream()
					.filter(a -> a.getCostCenterName() != null)
					.filter(a -> a.getCostCenterName().contains(inputObj.getCostCenterName()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate() != null)
					.filter(a -> a.getStartDate().getTime() >= inputObj.getStartDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= inputObj.getStartDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= inputObj.getEndDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= inputObj.getEndDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		MCostCenter q = repository.findOne(id);		
		if (q != null)
		{
			q.setStatus("3");
			
			repository.save(q);
		}		
	}
	
	@Override
	public MCostCenter findMCostCenterById(Integer id) {
	    return repository.findOne(id);
	}
	
	@Override
	public List<DropdownBean> getCostCenterByDisbursementUnitId(Integer disbursementUnitId) {

		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MCostCenter> mCostCenterList= repository.getCostCenterByDisbursementUnitId(disbursementUnitId);
		for(MCostCenter mCostCenterTb : mCostCenterList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(mCostCenterTb.getCostCenterId().toString());
			dropdownBean.setDesc(mCostCenterTb.getCostCenterCode());
			dropdownBean.setDesc2(mCostCenterTb.getCostCenterName());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;		
	}

	
	
}
