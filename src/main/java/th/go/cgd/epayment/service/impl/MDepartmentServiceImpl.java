package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.Address;
import th.go.cgd.epayment.entity.MAmphur;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MMoneySource;
import th.go.cgd.epayment.entity.MOrganizationSubType;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MTambon;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.AddressBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.master.DepartmentBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.service.interfaces.IMDepartmentService;

@Service
@Transactional
public class MDepartmentServiceImpl implements IMDepartmentService {

	@Autowired
	private MDepartmentRepository repository;
	
	@Override
	public void createMDepartment(DepartmentBean objBean) {

		MDepartment objDB = new MDepartment();
//	
		objDB.setDepartmentCode(objBean.getDepartmentCode());
		objDB.setDepartmentName(objBean.getDepartmentName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setOrganizationSubTypeId(objBean.getOrganizationSubTypeId() == null ? null : new MOrganizationSubType(objBean.getOrganizationSubTypeId()));
		objDB.setParentDepartmentId(objBean.getParentDepartmentId());
		objDB.setCreatedBy(new User(1));
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<DepartmentBean> getAllMDepartments() {
				
		List<DepartmentBean> objBeanList = new ArrayList<DepartmentBean>();
		
		for (MDepartment objDB : repository.findAll()) {
			
			DepartmentBean objBean = new DepartmentBean();

			objBean.setDepartmentId(objDB.getDepartmentId());
			objBean.setDepartmentCode(objDB.getDepartmentCode());
			objBean.setDepartmentName(objDB.getDepartmentName());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setStatus(objDB.getStatus());
			objBean.setOrganizationSubTypeId(objDB.getOrganizationSubTypeId() == null ? null : objDB.getOrganizationSubTypeId().getOrganizationSubTypeId());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
			
			objBean.setOrganizationSubTypeName(objDB.getOrganizationSubTypeId() == null ? null : objDB.getOrganizationSubTypeId().getOrganizationSubTypeName());

			objBean.setParentDepartmentId(objDB.getParentDepartmentId());
			
			if (objDB.getParentDepartmentId() != null)
			{
				MDepartment m = findDepartmentById(objDB.getParentDepartmentId());
				if (m != null)
				{
					objBean.setParentDepartmentName(m.getDepartmentName());
				}
			}

			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMDepartment(DepartmentBean objBean) {

		MDepartment objDB = repository.findOne(objBean.getDepartmentId());
		
		objDB.setDepartmentCode(objBean.getDepartmentCode());
		objDB.setDepartmentName(objBean.getDepartmentName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setOrganizationSubTypeId(objBean.getOrganizationSubTypeId() == null ? null : new MOrganizationSubType(objBean.getOrganizationSubTypeId()));
		objDB.setParentDepartmentId(objBean.getParentDepartmentId());
//		objDB.setCreatedBy(new User(1));
//		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(new User(1));
		objDB.setUpdatedDate(new Date());

		
		repository.save(objDB);
	}

	@Override
	public void deleteMDepartment(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public DepartmentBean getMDepartmentById(Integer id) {
		
		DepartmentBean objBean = new DepartmentBean();
		
		MDepartment objDB = repository.findOne(id);
		
		objBean.setDepartmentId(objDB.getDepartmentId());
		objBean.setDepartmentCode(objDB.getDepartmentCode());
		objBean.setDepartmentName(objDB.getDepartmentName());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setStatus(objDB.getStatus());
		objBean.setOrganizationSubTypeId(objDB.getOrganizationSubTypeId() == null ? null : objDB.getOrganizationSubTypeId().getOrganizationSubTypeId());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
		objBean.setUpdatedDate(objBean.getUpdatedDate());

		objBean.setOrganizationSubTypeName(objDB.getOrganizationSubTypeId() == null ? null : objDB.getOrganizationSubTypeId().getOrganizationSubTypeName());

		objBean.setParentDepartmentId(objDB.getParentDepartmentId());	
		
		if (objDB.getParentDepartmentId() != null)
		{
			MDepartment m = findDepartmentById(objDB.getParentDepartmentId());
			if (m != null)
			{
				objBean.setParentDepartmentName(m.getDepartmentName());
			}
		}

		return objBean;
	}

	@Override
	public List<DepartmentBean> getMDepartmentByCriteria(DepartmentBean inputObj) {
		
		List<DepartmentBean> q =  getAllMDepartments();

		if (!StringUtils.isEmpty(inputObj.getDepartmentCode()))
		{
			q = q.stream()
					.filter(a -> a.getDepartmentCode() != null)
					.filter(a -> a.getDepartmentCode().contains(inputObj.getDepartmentCode()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getDepartmentName()))
		{
			q = q.stream()
					.filter(a -> a.getDepartmentName() != null)
					.filter(a -> a.getDepartmentName().contains(inputObj.getDepartmentName()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate() != null)
					.filter(a -> a.getStartDate().getTime() >= inputObj.getStartDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= inputObj.getStartDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= inputObj.getEndDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= inputObj.getEndDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		MDepartment q = repository.findOne(id);		
		if (q != null)
		{
			q.setStatus("3");
			
			repository.save(q);
		}		
	}
	
	@Override
	public AddressBean getAdressBydept(Integer deptId) {

	    MDepartment departmentTb = repository.findOne(deptId);
	    AddressBean addressBean = null;
	    Address  addressTb = departmentTb.getAddressId();
	    if(addressTb!=null){
		 addressBean = new AddressBean();
		 addressBean.setNo(addressTb.getNo());
		 addressBean.setMoo(addressTb.getMoo());
		 addressBean.setRoomNo("");
		 addressBean.setSoi(addressTb.getSoi());
		 addressBean.setRoad(addressTb.getRoad());
		 MProvince provinceTb = addressTb.getProvinceId();
		 if(provinceTb != null){
		     addressBean.setProvinceName(provinceTb.getProvinceNameTh() != null ? provinceTb.getProvinceNameTh() :"");
		 }
		 MAmphur amphurTb = addressTb.getAmphurId();
		 if(amphurTb != null){
		     addressBean.setAmphurName(amphurTb.getAmphurNameTh() != null ? amphurTb.getAmphurNameTh() : "");
		 }
		 MTambon tambon = addressTb.getTambonId();
		 if(tambon != null){
		     addressBean.setTambonName(tambon.getTambonNameTh() != null ? tambon.getTambonNameTh()  : "");
		 }
		 addressBean.setPosCode(addressTb.getPostcode());
	    }
	 	return addressBean;
	}
	
	@Override
	public MDepartment findDepartmentById(Integer id) {
	    return repository.findOne(id);
	}
	
	@Override
    public List<DropdownBean> getMinistryAll() {
	    
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<DepartmentBean> q =  getAllMDepartments()
				.stream()
				.filter(a -> a.getOrganizationSubTypeId() == 4)
				.collect(Collectors.toList());
		
	    for(Iterator<DepartmentBean> iterator = q.iterator(); iterator.hasNext();){
		     DropdownBean dropdownBean = new DropdownBean();
		     DepartmentBean departmentTb = iterator.next();
		     dropdownBean.setId(departmentTb.getDepartmentId()+"");
		     dropdownBean.setDesc(departmentTb.getDepartmentCode());
		     dropdownBean.setDesc2(departmentTb.getDepartmentName());
		     dropdownBeanList.add(dropdownBean);
		 }

	    return dropdownBeanList;
	}
	
	@Override
    public List<DropdownBean> getDepartmentAll() {
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<DepartmentBean> q =  getAllMDepartments()
				.stream()
				.filter(a -> a.getOrganizationSubTypeId() == 5)
				.collect(Collectors.toList());
		
	    for(Iterator<DepartmentBean> iterator = q.iterator(); iterator.hasNext();){
		     DropdownBean dropdownBean = new DropdownBean();
		     DepartmentBean departmentTb = iterator.next();
		     dropdownBean.setId(departmentTb.getDepartmentId()+"");
		     dropdownBean.setDesc(departmentTb.getDepartmentCode());
		     dropdownBean.setDesc2(departmentTb.getDepartmentName());
		     dropdownBeanList.add(dropdownBean);
		 }

	    return dropdownBeanList;
	}
	
	@Override
    public List<DropdownBean> getDepartmentByMinistryId(Integer id) {
		
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<DepartmentBean> q =  getAllMDepartments()
				.stream()
				.filter(a -> a.getParentDepartmentId() == id)
				.collect(Collectors.toList());
		
	    for(Iterator<DepartmentBean> iterator = q.iterator(); iterator.hasNext();){
		     DropdownBean dropdownBean = new DropdownBean();
		     DepartmentBean departmentTb = iterator.next();
		     dropdownBean.setId(departmentTb.getDepartmentId()+"");
		     dropdownBean.setDesc(departmentTb.getDepartmentCode());
		     dropdownBean.setDesc2(departmentTb.getDepartmentName());
		     dropdownBeanList.add(dropdownBean);
		 }

	    return dropdownBeanList;
	}
	
	
	
}
