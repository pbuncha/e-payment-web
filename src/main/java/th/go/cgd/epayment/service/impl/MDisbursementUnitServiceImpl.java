package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDisbursementUnit;
import th.go.cgd.epayment.entity.MMoneySource;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.master.DisbursementUnitBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.repository.MDisbursementUnitRepository;
import th.go.cgd.epayment.service.interfaces.IMDisbursementUnitService;

@Service
@Transactional
public class MDisbursementUnitServiceImpl implements IMDisbursementUnitService {

	@Autowired
	private MDisbursementUnitRepository repository;
	
	
	@Override
	public List<DropdownBean> getDisbursementUnit()
	{
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MDisbursementUnit> mDisbursementUnitList= repository.findAll();
		for(MDisbursementUnit mDisbursementUnitTb : mDisbursementUnitList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(mDisbursementUnitTb.getDisbursementUnitId().toString());
			dropdownBean.setDesc(mDisbursementUnitTb.getDisbursementUnitDesc());
			dropdownBean.setDesc2(mDisbursementUnitTb.getDisbursementUnitLongDesc());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;		
	}
	
	@Override
	public List<DropdownBean> getDisbursementUnitByDepartmentId(Integer departmentId)
	{
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MDisbursementUnit> mDisbursementUnitList= repository.getDisbursementUnitAllByDepartmentId(departmentId);
		for(MDisbursementUnit mDisbursementUnitTb : mDisbursementUnitList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(mDisbursementUnitTb.getDisbursementUnitId().toString());
			dropdownBean.setDesc(mDisbursementUnitTb.getDisbursementUnitDesc());
			dropdownBean.setDesc2(mDisbursementUnitTb.getDisbursementUnitLongDesc());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;		
	}
	
	@Override
	public void createMDisbursementUnit(DisbursementUnitBean objBean) {

		MDisbursementUnit objDB = new MDisbursementUnit();

		objDB.setDisbursementUnitCode(objBean.getDisbursementUnitCode());
		objDB.setDisbursementUnitDesc(objBean.getDisbursementUnitDesc());
		objDB.setDisbursementUnitLongDesc(objBean.getDisbursementUnitLongDesc());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setCreatedBy(new User(1));
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<DisbursementUnitBean> getAllMDisbursementUnits() {
				
		List<DisbursementUnitBean> objBeanList = new ArrayList<DisbursementUnitBean>();
		
		for (MDisbursementUnit objDB : repository.findAll()) {
			
			DisbursementUnitBean objBean = new DisbursementUnitBean();

			objBean.setDisbursementUnitId(objDB.getDisbursementUnitId());
			objBean.setDisbursementUnitCode(objDB.getDisbursementUnitCode());
			objBean.setDisbursementUnitDesc(objDB.getDisbursementUnitDesc());
			objBean.setDisbursementUnitLongDesc(objDB.getDisbursementUnitLongDesc());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setStatus(objDB.getStatus());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMDisbursementUnit(DisbursementUnitBean objBean) {

		MDisbursementUnit objDB = repository.findOne(objBean.getDisbursementUnitId());
		
		objDB.setDisbursementUnitCode(objBean.getDisbursementUnitCode());
		objDB.setDisbursementUnitDesc(objBean.getDisbursementUnitDesc());
		objDB.setDisbursementUnitLongDesc(objBean.getDisbursementUnitLongDesc());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
//		objDB.setCreatedBy(new User(1));
//		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(new User(1));
		objDB.setUpdatedDate(new Date());
		
		repository.save(objDB);
	}

	@Override
	public void deleteMDisbursementUnit(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public DisbursementUnitBean getMDisbursementUnitById(Integer id) {
		
		DisbursementUnitBean objBean = new DisbursementUnitBean();
		
		MDisbursementUnit objDB = repository.findOne(id);
		
		objBean.setDisbursementUnitId(objDB.getDisbursementUnitId());
		objBean.setDisbursementUnitCode(objDB.getDisbursementUnitCode());
		objBean.setDisbursementUnitDesc(objDB.getDisbursementUnitDesc());
		objBean.setDisbursementUnitLongDesc(objDB.getDisbursementUnitLongDesc());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setStatus(objDB.getStatus());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
		objBean.setUpdatedDate(objBean.getUpdatedDate());
		
		return objBean;
	}
	
	@Override
	public List<DisbursementUnitBean> getMDisbursementUnitByCriteria(DisbursementUnitBean inputObj) {
		
		List<DisbursementUnitBean> q =  getAllMDisbursementUnits();

		if (!StringUtils.isEmpty(inputObj.getDisbursementUnitCode()))
		{
			q = q.stream()
					.filter(a -> a.getDisbursementUnitCode() != null)
					.filter(a -> a.getDisbursementUnitCode().contains(inputObj.getDisbursementUnitCode()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getDisbursementUnitDesc()))
		{
			q = q.stream()
					.filter(a -> a.getDisbursementUnitDesc() != null)
					.filter(a -> a.getDisbursementUnitDesc().contains(inputObj.getDisbursementUnitDesc()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getDisbursementUnitLongDesc()))
		{
			q = q.stream()
					.filter(a -> a.getDisbursementUnitLongDesc() != null)
					.filter(a -> a.getDisbursementUnitLongDesc().contains(inputObj.getDisbursementUnitLongDesc()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate() != null)
					.filter(a -> a.getStartDate().getTime() >= inputObj.getStartDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= inputObj.getStartDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= inputObj.getEndDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= inputObj.getEndDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		MDisbursementUnit q = repository.findOne(id);		
		if (q != null)
		{
			q.setStatus("3");
			
			repository.save(q);
		}		
	}
}
