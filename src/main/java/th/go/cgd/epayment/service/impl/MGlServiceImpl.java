package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.MGl;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.master.GlBean;
import th.go.cgd.epayment.model.master.StandardRevernueBean;
import th.go.cgd.epayment.repository.MGlRepository;
import th.go.cgd.epayment.service.interfaces.IMGlService;

@Service
@Transactional
public class MGlServiceImpl implements IMGlService {

	@Autowired
	private MGlRepository repository;
	
	@Override
	public void createMGl(GlBean objBean) {

		MGl objDB = new MGl();

		objDB.setGlCode(objBean.getGlCode());
		objDB.setGlName(objBean.getGlName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setStandardRevernueId(objBean.getStandardRevernueId());
		objDB.setCreatedBy(new User(1));
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<GlBean> getAllMGls() {
				
		List<GlBean> objBeanList = new ArrayList<GlBean>();
		
		for (MGl objDB : repository.findAll()) {
			
			GlBean objBean = new GlBean();

			objBean.setGlId(objDB.getGlId());
			objBean.setGlCode(objDB.getGlCode());
			objBean.setGlName(objDB.getGlName());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setStatus(objDB.getStatus());
			objBean.setStandardRevernueId(objDB.getStandardRevernueId());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMGl(GlBean objBean) {

		MGl objDB = repository.findOne(objBean.getGlId());
		
		objDB.setGlCode(objBean.getGlCode());
		objDB.setGlName(objBean.getGlName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setStandardRevernueId(objBean.getStandardRevernueId());
		//objDB.setCreatedBy(new User(1));
		//objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(new User(1));
		objDB.setUpdatedDate(new Date());
		
		repository.save(objDB);
	}

	@Override
	public void deleteMGl(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public GlBean getMGlById(Integer id) {
		
		GlBean objBean = new GlBean();
		
		MGl objDB = repository.findOne(id);
		
		objBean.setGlId(objDB.getGlId());
		objBean.setGlCode(objDB.getGlCode());
		objBean.setGlName(objDB.getGlName());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setStatus(objDB.getStatus());
		objBean.setStandardRevernueId(objDB.getStandardRevernueId());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
		objBean.setUpdatedDate(objBean.getUpdatedDate());
		
		return objBean;
	}

	@Override
	public List<GlBean> getMGlsByCriteria(GlBean inputObj) {
		
		List<GlBean> q =  getAllMGls();

		if (!StringUtils.isEmpty(inputObj.getGlCode()))
		{
			q = q.stream()
					.filter(a -> a.getGlCode() != null)
					.filter(a -> a.getGlCode().contains(inputObj.getGlCode()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getGlName()))
		{
			q = q.stream()
					.filter(a -> a.getGlName() != null)
					.filter(a -> a.getGlName().contains(inputObj.getGlName()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate() != null)
					.filter(a -> a.getStartDate().getTime() >= inputObj.getStartDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= inputObj.getStartDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= inputObj.getEndDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= inputObj.getEndDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		MGl q = repository.findOne(id);		
		if (q != null)
		{
			q.setStatus("3");
			
			repository.save(q);
		}		
	}
	
}
