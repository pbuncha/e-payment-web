package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MMoneySource;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.model.master.StandardRevernueBean;
import th.go.cgd.epayment.repository.MMoneySourceRepository;
import th.go.cgd.epayment.service.interfaces.IMMoneySourceService;

@Service
@Transactional
public class MMoneySourceServiceImpl implements IMMoneySourceService {

	@Autowired
	private MMoneySourceRepository repository;
	
	@Override
	public void createMMoneySource(MoneySourceBean objBean) {

		MMoneySource objDB = new MMoneySource();

		objDB.setMoneySourceCode(objBean.getMoneySourceCode());
		objDB.setMoneySourceName(objBean.getMoneySourceName());
		objDB.setMoneySourceType(objBean.getMoneySourceType());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setCreatedBy(1);
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<MoneySourceBean> getAllMMoneySources() {
				
		List<MoneySourceBean> objBeanList = new ArrayList<MoneySourceBean>();
		
		for (MMoneySource objDB : repository.findAll()) {
			
			MoneySourceBean objBean = new MoneySourceBean();

			objBean.setMoneySourceId(objDB.getMoneySourceId());
			objBean.setMoneySourceCode(objDB.getMoneySourceCode());
			objBean.setMoneySourceName(objDB.getMoneySourceName());
			objBean.setMoneySourceType(objDB.getMoneySourceType());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setStatus(objDB.getStatus());
			objBean.setCreatedBy(objDB.getCreatedBy());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMMoneySource(MoneySourceBean objBean) {

		MMoneySource objDB = repository.findOne(objBean.getMoneySourceId());
		
		objDB.setMoneySourceCode(objBean.getMoneySourceCode());
		objDB.setMoneySourceName(objBean.getMoneySourceName());
		objDB.setMoneySourceType(objBean.getMoneySourceType());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		//objDB.setCreatedBy(1);
		//objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(1);
		objDB.setUpdatedDate(new Date());
		
		repository.save(objDB);
	}

	@Override
	public void deleteMMoneySource(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public MoneySourceBean getMMoneySourceById(Integer id) {
		
		MoneySourceBean objBean = new MoneySourceBean();
		
		MMoneySource objDB = repository.findOne(id);
		
		objBean.setMoneySourceId(objDB.getMoneySourceId());
		objBean.setMoneySourceCode(objDB.getMoneySourceCode());
		objBean.setMoneySourceName(objDB.getMoneySourceName());
		objBean.setMoneySourceType(objDB.getMoneySourceType());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setStatus(objDB.getStatus());
		objBean.setCreatedBy(objDB.getCreatedBy());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy());
		objBean.setUpdatedDate(objBean.getUpdatedDate());
		
		return objBean;
	}

	@Override
	public List<MoneySourceBean> getMMoneySourceByCriteria(MoneySourceBean inputObj) {
		
		List<MoneySourceBean> q =  getAllMMoneySources();

		if (!StringUtils.isEmpty(inputObj.getMoneySourceCode() ))
		{
			q = q.stream()
					.filter(a -> a.getMoneySourceCode() != null)
					.filter(a -> a.getMoneySourceCode().contains(inputObj.getMoneySourceCode()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getMoneySourceName()))
		{
			q = q.stream()
					.filter(a -> a.getMoneySourceName() != null)
					.filter(a -> a.getMoneySourceName().contains(inputObj.getMoneySourceName()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate() != null)
					.filter(a -> a.getStartDate().getTime() >= inputObj.getStartDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= inputObj.getStartDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= inputObj.getEndDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= inputObj.getEndDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		MMoneySource q = repository.findOne(id);		
		if (q != null)
		{
			q.setStatus("3");
			
			repository.save(q);
		}		
	}
	
}
