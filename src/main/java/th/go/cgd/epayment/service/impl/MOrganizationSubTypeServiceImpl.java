package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.entity.MOrganizationSubType;
import th.go.cgd.epayment.entity.MOrganizationType;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.master.OrganizationSubTypeBean;
import th.go.cgd.epayment.repository.MOrganizationSubTypeRepository;
import th.go.cgd.epayment.service.interfaces.IMOrganizationSubTypeService;

@Service
@Transactional
public class MOrganizationSubTypeServiceImpl implements IMOrganizationSubTypeService {

	@Autowired
	private MOrganizationSubTypeRepository repository;
	
	@Override
	public void createMOrganizationSubType(OrganizationSubTypeBean objBean) {

		MOrganizationSubType objDB = new MOrganizationSubType();

		objDB.setOrganizationSubTypeName(objBean.getOrganizationSubTypeName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setOrganizationTypeId(objBean.getOrganizationTypeId() == null ? null : new MOrganizationType(objBean.getOrganizationTypeId()));
		objDB.setCreatedBy(new User(1));
		objDB.setCreatedDate(new Date());
//		objDB.setUpdatedBy(new User(1));
//		objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<OrganizationSubTypeBean> getAllMOrganizationSubTypes() {
				
		List<OrganizationSubTypeBean> objBeanList = new ArrayList<OrganizationSubTypeBean>();
		
		for (MOrganizationSubType objDB : repository.findAll()) {
			
			OrganizationSubTypeBean objBean = new OrganizationSubTypeBean();

			objBean.setOrganizationSubTypeId(objDB.getOrganizationSubTypeId());
			objBean.setOrganizationSubTypeName(objDB.getOrganizationSubTypeName());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());			
			objBean.setStatus(objDB.getStatus());
			objBean.setOrganizationTypeId(objDB.getOrganizationTypeId() == null ? null : objDB.getOrganizationTypeId().getOrganizationTypeId());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
			
			objBean.setOrganizationTypeName(objDB.getOrganizationTypeId() == null ? null : objDB.getOrganizationTypeId().getOrganizationTypeName());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMOrganizationSubType(OrganizationSubTypeBean objBean) {

		MOrganizationSubType objDB = repository.findOne(objBean.getOrganizationSubTypeId());
		
		objDB.setOrganizationSubTypeName(objBean.getOrganizationSubTypeName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setOrganizationTypeId(objBean.getOrganizationTypeId() == null ? null : new MOrganizationType(objBean.getOrganizationTypeId()));
//		objDB.setCreatedBy(new User(1));
//		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(new User(1));
		objDB.setUpdatedDate(new Date());

		
		repository.save(objDB);
	}

	@Override
	public void deleteMOrganizationSubType(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public OrganizationSubTypeBean getMOrganizationSubTypeById(Integer id) {
		
		OrganizationSubTypeBean objBean = new OrganizationSubTypeBean();
		
		MOrganizationSubType objDB = repository.findOne(id);
		
		objBean.setOrganizationSubTypeId(objDB.getOrganizationSubTypeId());
		objBean.setOrganizationSubTypeName(objDB.getOrganizationSubTypeName());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());			
		objBean.setStatus(objDB.getStatus());
		objBean.setOrganizationTypeId(objDB.getOrganizationTypeId() == null ? null : objDB.getOrganizationTypeId().getOrganizationTypeId());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
		objBean.setUpdatedDate(objBean.getUpdatedDate());

		objBean.setOrganizationTypeName(objDB.getOrganizationTypeId() == null ? null : objDB.getOrganizationTypeId().getOrganizationTypeName());

		return objBean;
	}
	
	@Override
	public MOrganizationSubType findMOrganizationSubTypeById(Integer id) {
	    return repository.findOne(id);
	}
	
}
