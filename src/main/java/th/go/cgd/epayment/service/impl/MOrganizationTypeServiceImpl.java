package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.entity.MOrganizationType;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.master.OrganizationTypeBean;
import th.go.cgd.epayment.repository.MOrganizationTypeRepository;
import th.go.cgd.epayment.service.interfaces.IMOrganizationTypeService;

@Service
@Transactional
public class MOrganizationTypeServiceImpl implements IMOrganizationTypeService {

	@Autowired
	private MOrganizationTypeRepository repository;
	
	@Override
	public void createMOrganizationType(OrganizationTypeBean objBean) {

		MOrganizationType objDB = new MOrganizationType();

		objDB.setOrganizationTypeName(objBean.getOrganizationTypeName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setCreatedBy(new User(1));
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<OrganizationTypeBean> getAllMOrganizationTypes() {
				
		List<OrganizationTypeBean> objBeanList = new ArrayList<OrganizationTypeBean>();
		
		for (MOrganizationType objDB : repository.findAll()) {
			
			OrganizationTypeBean objBean = new OrganizationTypeBean();

			objBean.setOrganizationTypeId(objDB.getOrganizationTypeId());
			objBean.setOrganizationTypeName(objDB.getOrganizationTypeName());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setStatus(objDB.getStatus());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMOrganizationType(OrganizationTypeBean objBean) {

		MOrganizationType objDB = repository.findOne(objBean.getOrganizationTypeId());
		
		objDB.setOrganizationTypeName(objBean.getOrganizationTypeName());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		//objDB.setCreatedBy(new User(1));
		//objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(new User(1));
		objDB.setUpdatedDate(new Date());
		
		repository.save(objDB);
	}

	@Override
	public void deleteMOrganizationType(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public OrganizationTypeBean getMOrganizationTypeById(Integer id) {
		
		OrganizationTypeBean objBean = new OrganizationTypeBean();
		
		MOrganizationType objDB = repository.findOne(id);
		
		objBean.setOrganizationTypeId(objDB.getOrganizationTypeId());
		objBean.setOrganizationTypeName(objDB.getOrganizationTypeName());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setStatus(objDB.getStatus());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
		objBean.setUpdatedDate(objBean.getUpdatedDate());
		
		return objBean;
	}
	
	@Override
	public MOrganizationType findMOrganizationTypeById(Integer id) {
	    return repository.findOne(id);
	}
	

	
	
}
