package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.entity.MAmphur;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MTambon;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.master.ProvinceBean;
import th.go.cgd.epayment.repository.MAmphurRepository;
import th.go.cgd.epayment.repository.MProvinceRepository;
import th.go.cgd.epayment.repository.MTambonRepository;
import th.go.cgd.epayment.service.interfaces.IMProvinceService;

@Service
@Transactional
public class MProvinceServiceImpl implements IMProvinceService {

	@Autowired
	private MProvinceRepository mProvinceRepository;
	@Autowired
	private MAmphurRepository mAmphurRepository;
	@Autowired
	private MTambonRepository mTambonRepository;
	
	@Override
	public void createMProvince(ProvinceBean objBean) {

		MProvince objDB = new MProvince();

		objDB.setProvinceNameEn(objBean.getProvinceNameEn());
		objDB.setProvinceNameTh(objBean.getProvinceNameTh());
		objDB.setCreatedBy(1);
		objDB.setCreatedDate(new Date());
		//objDB.setUpdatedBy(new User(1));
		//objDB.setUpdatedDate(new Date());
				
		mProvinceRepository.save(objDB);
	}

	@Override
	public List<ProvinceBean> getAllMProvinces() {
				
		List<ProvinceBean> objBeanList = new ArrayList<ProvinceBean>();
		
		for (MProvince objDB : mProvinceRepository.findAll()) {
			
			ProvinceBean objBean = new ProvinceBean();

			objBean.setProvinceId(objDB.getProvinceId());
			objBean.setProvinceNameEn(objDB.getProvinceNameEn());
			objBean.setProvinceNameTh(objDB.getProvinceNameTh());
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy());
			objBean.setUpdatedDate(objBean.getUpdatedDate());
									
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMProvince(ProvinceBean objBean) {

		MProvince objDB = mProvinceRepository.findOne(objBean.getProvinceId());
		
		objDB.setProvinceNameEn(objBean.getProvinceNameEn());
		objDB.setProvinceNameTh(objBean.getProvinceNameTh());
//		objDB.setCreatedBy(1);
//		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(1);
		objDB.setUpdatedDate(new Date());

		
		mProvinceRepository.save(objDB);
	}

	@Override
	public void deleteMProvince(Integer id) {
		
	    mProvinceRepository.delete(id);		
	}

	@Override
	public ProvinceBean getMProvinceById(Integer id) {
		
		ProvinceBean objBean = new ProvinceBean();
		
		MProvince objDB = mProvinceRepository.findOne(id);
		
		objBean.setProvinceId(objDB.getProvinceId());
		objBean.setProvinceNameEn(objDB.getProvinceNameEn());
		objBean.setProvinceNameTh(objDB.getProvinceNameTh());
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy());
		objBean.setUpdatedDate(objBean.getUpdatedDate());

		
		return objBean;
	}
	public MProvince getProvinceById(Integer id){
	    return mProvinceRepository.findOne(id);
	}
	public MAmphur getAmphurById(Integer id){
	    return mAmphurRepository.findOne(id);
	}
	public MTambon getSubDistrictById(Integer id){
	    return mTambonRepository.findOne(id);
	}
}
