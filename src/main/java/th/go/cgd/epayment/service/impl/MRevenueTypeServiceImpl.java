package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.MRevenueType;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.service.interfaces.IMRevenueTypeService;

@Service @Transactional
public class MRevenueTypeServiceImpl implements IMRevenueTypeService {

    @PersistenceContext
    private EntityManager entityManager;
    
	@Override 
    public List<DropdownBean> getStatusActive(){
	    TypedQuery<MRevenueType> revenueTypeList = this.entityManager.createNamedQuery("MRevenueType.findByStatus",MRevenueType.class);
	    revenueTypeList.setParameter("status", '1');
	    
	    List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
	    try{
		for(Iterator<MRevenueType> iterator = revenueTypeList.getResultList().iterator(); iterator.hasNext();){
		     DropdownBean dropdownBean = new DropdownBean();
		     MRevenueType revenueType = (MRevenueType)iterator.next();
		     dropdownBean.setId(revenueType.getRevenueTypeId().toString());
		     dropdownBean.setDesc(revenueType.getRevenueTypeName());
		     dropdownBean.setDesc2(revenueType.getRevenueTypeNameEn());
		     dropdownBeanList.add(dropdownBean);
		 }
		     
	    }catch(Exception ex){
		ex.printStackTrace();
	    }
	return dropdownBeanList;
    }


}
