package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.MMoneySource;
import th.go.cgd.epayment.entity.MStandardRevernue;
import th.go.cgd.epayment.entity.MStandardRevernueGl;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.model.master.StandardRevernueGlBean;
import th.go.cgd.epayment.repository.MStandardRevernueGlRepository;
import th.go.cgd.epayment.service.interfaces.IMStandardRevernueGlService;

@Service
@Transactional
public class MStandardRevernueGlServiceImpl implements IMStandardRevernueGlService {

	@Autowired
	private MStandardRevernueGlRepository repository;
	
	@Override
	public void createMStandardRevernueGl(StandardRevernueGlBean objBean) {

		MStandardRevernueGl objDB = new MStandardRevernueGl();

		objDB.setStandardRevernueId(new MStandardRevernue(objBean.getStandardRevernueId()));
		objDB.setOrder(objBean.getOrder());
		objDB.setGlStartId(objBean.getGlStartId());
		objDB.setGlFinishId(objBean.getGlFinishId());
		objDB.setStatus(objBean.getStatus());
		objDB.setCreatedBy(1);
		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(objBean.getUpdatedBy());
		objDB.setUpdatedDate(objBean.getUpdatedDate());
		
		repository.save(objDB);
	}

	@Override
	public List<StandardRevernueGlBean> getAllMStandardRevernueGls() {
				
		List<StandardRevernueGlBean> objBeanList = new ArrayList<StandardRevernueGlBean>();
		
		for (MStandardRevernueGl objDB : repository.findAll()) {
			
			StandardRevernueGlBean objBean = new StandardRevernueGlBean();
			
			objBean.setStandardRevernueGlId(objDB.getStandardRevernueGlId());			
			objBean.setStandardRevernueId(objDB.getStandardRevernueId() == null ? null : objDB.getStandardRevernueId().getStandardRevernueId());
			objBean.setOrder(objDB.getOrder());
			objBean.setGlStartId(objDB.getGlStartId());
			objBean.setGlFinishId(objDB.getGlFinishId());
			objBean.setStatus(objDB.getStatus());
			objBean.setCreatedBy(objDB.getCreatedBy());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy());
			objBean.setUpdatedDate(objDB.getUpdatedDate());

			objBean.setStandardRevernueName(objDB.getStandardRevernueId() == null ? null : objDB.getStandardRevernueId().getStandardRevernueName());
			
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}

	@Override
	public void updateMStandardRevernueGl(StandardRevernueGlBean objBean) {

		MStandardRevernueGl objDB = repository.findOne(objBean.getStandardRevernueGlId());
		
		objDB.setStandardRevernueId(new MStandardRevernue(objBean.getStandardRevernueId()));
		objDB.setOrder(objBean.getOrder());
		objDB.setGlStartId(objBean.getGlStartId());
		objDB.setGlFinishId(objBean.getGlFinishId());
		objDB.setStatus(objBean.getStatus());
		objDB.setCreatedBy(objBean.getCreatedBy());
		objDB.setCreatedDate(objBean.getCreatedDate());
		objDB.setUpdatedBy(objBean.getUpdatedBy());
		objDB.setUpdatedDate(objBean.getUpdatedDate());
		
		repository.save(objDB);
	}

	@Override
	public void deleteMStandardRevernueGl(Integer id) {
		
		repository.delete(id);		
	}

	@Override
	public StandardRevernueGlBean getMStandardRevernueGlById(Integer id) {
		
		StandardRevernueGlBean objBean = new StandardRevernueGlBean();
		
		MStandardRevernueGl objDB = repository.findOne(id);
		
		objBean.setStandardRevernueGlId(objDB.getStandardRevernueGlId());			
		objBean.setStandardRevernueId(objDB.getStandardRevernueId() == null ? null : objDB.getStandardRevernueId().getStandardRevernueId());
		objBean.setOrder(objDB.getOrder());
		objBean.setGlStartId(objDB.getGlStartId());
		objBean.setGlFinishId(objDB.getGlFinishId());
		objBean.setStatus(objDB.getStatus());
		objBean.setCreatedBy(objDB.getCreatedBy());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy());
		objBean.setUpdatedDate(objDB.getUpdatedDate());

		objBean.setStandardRevernueName(objDB.getStandardRevernueId() == null ? null : objDB.getStandardRevernueId().getStandardRevernueName());
		
		return objBean;
	}

	@Override
	public List<StandardRevernueGlBean> getMStandardRevernueGlByCriteria(StandardRevernueGlBean inputObj) {
		
		List<StandardRevernueGlBean> q =  getAllMStandardRevernueGls();

		if (!StringUtils.isEmpty(inputObj.getStandardRevernueName()))
		{
			q = q.stream()
					.filter(a -> a.getStandardRevernueName() != null)
					.filter(a -> a.getStandardRevernueName().contains(inputObj.getStandardRevernueName()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getOrder()))
		{
			q = q.stream()
					.filter(a -> a.getOrder() != null)
					.filter(a -> a.getOrder().equals(inputObj.getOrder()))
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getGlStartId()))
		{
			q = q.stream()
					.filter(a -> a.getGlStartId() != null)
					.filter(a -> a.getGlStartId().equals(inputObj.getGlStartId()))
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getGlFinishId()))
		{
			q = q.stream()
					.filter(a -> a.getGlFinishId() != null)
					.filter(a -> a.getGlFinishId().equals(inputObj.getGlFinishId()))
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		MStandardRevernueGl q = repository.findOne(id);		
		if (q != null)
		{
			q.setStatus("3");
			
			repository.save(q);
		}		
	}
}
