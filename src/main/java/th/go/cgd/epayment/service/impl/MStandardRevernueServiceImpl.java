package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.entity.MMoneySource;
import th.go.cgd.epayment.entity.MStandardRevernue;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.master.StandardRevernueBean;
import th.go.cgd.epayment.repository.MStandardRevernueRepository;
import th.go.cgd.epayment.service.interfaces.IMStandardRevernueService;

@Service
@Transactional
public class MStandardRevernueServiceImpl implements IMStandardRevernueService {

	@Autowired
	private MStandardRevernueRepository repository;
	
	public String TEST()
	{
		System.out.println("service TEST 1");
		
		return "TEST from service.";
	}
	
	
	@Override
	public void createMStandardRevernue(StandardRevernueBean objBean) {

		MStandardRevernue objDB = new MStandardRevernue();

		objDB.setStandardRevernueCode(objBean.getStandardRevernueCode());
		objDB.setStandardRevernueName(objBean.getStandardRevernueName());
		objDB.setStandardRevernueDesc(objBean.getStandardRevernueDesc());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setMoneySourceId(objBean.getMoneySourceId() == null ? null : new MMoneySource(objBean.getMoneySourceId()));		
		objDB.setCreatedBy(new User(1));
		objDB.setCreatedDate(new Date());
//		objDB.setUpdatedBy(new User(1));
//		objDB.setUpdatedDate(new Date());
				
		repository.save(objDB);
	}

	@Override
	public List<StandardRevernueBean> getAllMStandardRevernues() {
				
		System.out.println("test2");
		List<StandardRevernueBean> objBeanList = new ArrayList<StandardRevernueBean>();
		
		System.out.println("test3");
		for (MStandardRevernue objDB : repository.findAll()) {
			
			System.out.println("test4");
			StandardRevernueBean objBean = new StandardRevernueBean();

			objBean.setStandardRevernueId(objDB.getStandardRevernueId());
			objBean.setStandardRevernueName(objDB.getStandardRevernueName());
			objBean.setStandardRevernueCode(objDB.getStandardRevernueCode());
			objBean.setStandardRevernueDesc(objDB.getStandardRevernueDesc());
			objBean.setStartDate(objDB.getStartDate());
			objBean.setEndDate(objDB.getEndDate());
			objBean.setStatus(objDB.getStatus());
			objBean.setMoneySourceId(objDB.getMoneySourceId() == null ? null : objDB.getMoneySourceId().getMoneySourceId());		
			objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
			objBean.setCreatedDate(objDB.getCreatedDate());
			objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
			objBean.setUpdatedDate(objDB.getUpdatedDate());
			
			objBean.setMoneySourceName(objDB.getMoneySourceId() == null ? null : objDB.getMoneySourceId().getMoneySourceName());
						
			objBeanList.add(objBean);
		}
		
		return objBeanList;
	}



	
	@Override
	public void updateMStandardRevernue(StandardRevernueBean objBean) {

		MStandardRevernue objDB = repository.findOne(objBean.getStandardRevernueId());
		
		objDB.setStandardRevernueCode(objBean.getStandardRevernueCode());
		objDB.setStandardRevernueName(objBean.getStandardRevernueName());
		objDB.setStandardRevernueDesc(objBean.getStandardRevernueDesc());
		objDB.setStartDate(objBean.getStartDate());
		objDB.setEndDate(objBean.getEndDate());
		objDB.setStatus(objBean.getStatus());
		objDB.setMoneySourceId(objBean.getMoneySourceId() == null ? null : new MMoneySource(objBean.getMoneySourceId()));		
//		objDB.setCreatedBy(new User(1));
//		objDB.setCreatedDate(new Date());
		objDB.setUpdatedBy(new User(1));
		objDB.setUpdatedDate(new Date());
		
		repository.save(objDB);
	}

	@Override
	public void deleteMStandardRevernue(Integer id) {
		
		repository.delete(id);		
	}
	
	@Override
	public StandardRevernueBean getMStandardRevernueById(Integer id) {
		
		StandardRevernueBean objBean = new StandardRevernueBean();
		
		MStandardRevernue objDB = repository.findOne(id);
		
		objBean.setStandardRevernueId(objDB.getStandardRevernueId());
		objBean.setStandardRevernueCode(objDB.getStandardRevernueCode());
		objBean.setStandardRevernueName(objDB.getStandardRevernueName());
		objBean.setStandardRevernueDesc(objDB.getStandardRevernueDesc());
		objBean.setStartDate(objDB.getStartDate());
		objBean.setEndDate(objDB.getEndDate());
		objBean.setStatus(objDB.getStatus());
		objBean.setMoneySourceId(objDB.getMoneySourceId() == null ? null : objDB.getMoneySourceId().getMoneySourceId());		
		objBean.setCreatedBy(objDB.getCreatedBy() == null ? null : objDB.getCreatedBy().getUserId());
		objBean.setCreatedDate(objDB.getCreatedDate());
		objBean.setUpdatedBy(objDB.getUpdatedBy() == null ? null : objDB.getUpdatedBy().getUserId());
		objBean.setUpdatedDate(objDB.getUpdatedDate());
		
		objBean.setMoneySourceName(objDB.getMoneySourceId() == null ? null : objDB.getMoneySourceId().getMoneySourceName());

		return objBean;
	}
	
	
	@Override
	public List<StandardRevernueBean> getMStandardRevernuesByCriteria(StandardRevernueBean inputObj) {
		
		List<StandardRevernueBean> q =  getAllMStandardRevernues();

		if (!StringUtils.isEmpty(inputObj.getStandardRevernueCode()))
		{
			q = q.stream()
					.filter(a -> a.getStandardRevernueCode() != null)
					.filter(a -> a.getStandardRevernueCode().contains(inputObj.getStandardRevernueCode()))
					.collect(Collectors.toList());			
		}
		if (!StringUtils.isEmpty(inputObj.getStandardRevernueName()))
		{
			q = q.stream()
					.filter(a -> a.getStandardRevernueName() != null)
					.filter(a -> a.getStandardRevernueName().contains(inputObj.getStandardRevernueName()))
					.collect(Collectors.toList());
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate() != null)
					.filter(a -> a.getStartDate().getTime() >= inputObj.getStartDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStartDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getStartDate().getTime() <= inputObj.getStartDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateFrom()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() >= inputObj.getEndDateFrom().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getEndDateTo()))
		{
			q = q.stream()
					.filter(a -> a.getEndDate() != null)
					.filter(a -> a.getEndDate().getTime() <= inputObj.getEndDateTo().getTime())
					.collect(Collectors.toList());						
		}
		if (!StringUtils.isEmpty(inputObj.getStatus()))
		{
			q = q.stream()
					.filter(a -> a.getStatus() != null)
					.filter(a -> inputObj.getStatus().equals(a.getStatus()))
					.collect(Collectors.toList());
		}
		
		return q;		
	}	

	@Override
	public void updateStatusCancel(Integer id) {
		
		repository.updateStatusCancel(id);
	}
	
	
}
