package th.go.cgd.epayment.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.Catalog;
import th.go.cgd.epayment.entity.CatalogConditionText;
import th.go.cgd.epayment.entity.CatalogCostCenter;
import th.go.cgd.epayment.entity.CatalogStructure;
import th.go.cgd.epayment.entity.CatalogStructureItem;
import th.go.cgd.epayment.entity.MAccountDeposit;
import th.go.cgd.epayment.entity.MCatalogType;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MGl;
import th.go.cgd.epayment.entity.MLevyType;
import th.go.cgd.epayment.entity.MMoneySource;
import th.go.cgd.epayment.entity.MRevenueType;
import th.go.cgd.epayment.entity.MStandardRevernueGl;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.CatalogBean;
import th.go.cgd.epayment.model.CatalogConditionTextBean;
import th.go.cgd.epayment.model.CatalogCostCenterBean;
import th.go.cgd.epayment.model.CatalogStructureItemBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.model.master.AccountDepositBean;
import th.go.cgd.epayment.model.master.CostCenterBean;
import th.go.cgd.epayment.model.master.DepartmentBean;
import th.go.cgd.epayment.model.master.GlBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.model.master.StandardRevernueBean;
import th.go.cgd.epayment.report.service.ReportUtilService;
import th.go.cgd.epayment.repository.CatalogConditionTextRepository;
import th.go.cgd.epayment.repository.CatalogCostCenterRepository;
import th.go.cgd.epayment.repository.CatalogRepository;
import th.go.cgd.epayment.repository.CatalogStructureItemRepository;
import th.go.cgd.epayment.repository.CatalogStructureRepository;
import th.go.cgd.epayment.repository.MAccountDepositRepository;
import th.go.cgd.epayment.repository.MCatalogTypeRepository;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MGlRepository;
import th.go.cgd.epayment.repository.MLevyTypeRepository;
import th.go.cgd.epayment.repository.MMoneySourceRepository;
import th.go.cgd.epayment.repository.MRevenueTypeRepository;
import th.go.cgd.epayment.repository.MStandardRevernueGlRepository;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.service.interfaces.IGenerateCodeService;
import th.go.cgd.epayment.service.interfaces.IManageCatalogService;
import th.go.cgd.epayment.util.CalendarHelper;

@Service
@Transactional
public class ManageCatalogServiceImpl implements IManageCatalogService{
	private static final Logger logger = Logger.getLogger(ManageCatalogServiceImpl.class);

	@Autowired
	private MCatalogTypeRepository mCatalogTypeRepository;
	@Autowired
	private MRevenueTypeRepository mRevenueTypeRepository;
	@Autowired
	private MLevyTypeRepository mLevyTypeRepository;
	@Autowired
	private MCostCenterRepository mCostCenterRepository;
	@Autowired
	private MDepartmentRepository mDepartmentRepository;
	@Autowired
	private MGlRepository mGlRepository;
	@Autowired
	private MAccountDepositRepository mAccountDepositRepository;

	@Autowired
	private CatalogRepository catalogRepository;
	@Autowired
	private CatalogStructureRepository catalogStructureRepository;
	@Autowired
	private CatalogStructureItemRepository catalogStructureItemRepository;
	@Autowired
	private CatalogConditionTextRepository catalogConditionTextRepository;
	@Autowired
	private CatalogCostCenterRepository catalogCostCenterRepository;
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private IGenerateCodeService generateCodeService;
	
	@Autowired
	private MStandardRevernueGlRepository mStandardRevernueGlRepository;
	@Autowired
	private MMoneySourceRepository mMoneySourceRepository;
	@Autowired
	private MessageResource messageResource;

	public Page<CatalogBean> search(SearchBean catalogBean){
		logger.info("search");
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
		List<CatalogBean> catalogBeanList = new ArrayList<CatalogBean>();


		StringBuffer strQueryWhere = new StringBuffer(" ");
		StringBuffer queryStr = new StringBuffer("select distinct c ");

		strQueryWhere.append(" from Catalog  c ");
		strQueryWhere.append(" LEFT JOIN c.catalogStructureList cs ");
		strQueryWhere.append(" LEFT JOIN cs.catalogStructureItemList csi ");

		strQueryWhere.append(" WHERE (1=1) AND c.status <> '"+EPaymentConstant.NO.charAt(0)+"' ");

		if (catalogBean.getDateStartForm() != null)
			strQueryWhere.append(" AND TO_CHAR(c.startDate,'yyyyMMdd') >= :startForm");
		if (catalogBean.getDateStartTo() != null)
			strQueryWhere.append(" AND TO_CHAR(c.startDate,'yyyyMMdd') <= :startTo");
		if (catalogBean.getDateEndFrom() != null)
			strQueryWhere.append(" AND TO_CHAR(c.endDate,'yyyyMMdd') >= :endFrom");
		if (catalogBean.getDateEndTo() != null)
			strQueryWhere.append(" AND TO_CHAR(c.endDate,'yyyyMMdd') <= :endTo");
		if (catalogBean.getStatusUse() != null)
			strQueryWhere.append(" AND c.status= :statusUse");
		if (catalogBean.getStatusApprove() != null)
			strQueryWhere.append(" AND c.status= :statusApprove");
		if (catalogBean.getPending() != null)
			strQueryWhere.append(" AND c.status= :pending");
		if (catalogBean.getRevenueTypeId()!= null)
			strQueryWhere.append(" AND c.revenueTypeId.revenueTypeId= :revenueTypeId");
		if (catalogBean.getCatalogTypeId()!= null)
			strQueryWhere.append(" AND c.catalogTypeId.catalogTypeId= :catalogTypeId");
		if (catalogBean.getCatalogName()!= null)
			strQueryWhere.append(" AND c.catalogName LIKE :catalogName ");
		if (catalogBean.getDepartmentId() != null)
			strQueryWhere.append(" AND csi.departmentId.departmentId = :departmentId");
		TypedQuery<Catalog> query = entityManager.createQuery(queryStr.toString()+strQueryWhere.toString(), Catalog.class);

		if (catalogBean.getDateStartForm() != null)
			query.setParameter("startForm", dt1.format(catalogBean.getDateStartForm()));
		if (catalogBean.getDateStartTo() != null)
			query.setParameter("startTo", dt1.format(catalogBean.getDateStartTo()));
		if (catalogBean.getDateEndFrom() != null)
			query.setParameter("endFrom", dt1.format(catalogBean.getDateEndFrom()));
		if (catalogBean.getDateEndTo() != null)
			query.setParameter("endTo", dt1.format(catalogBean.getDateEndTo()));
		if (catalogBean.getStatusUse()!= null)
			query.setParameter("statusUse", catalogBean.getStatusUse().charAt(0));
		if (catalogBean.getStatusApprove() != null)
			query.setParameter("statusApprove", catalogBean.getStatusApprove().charAt(0));
		if (catalogBean.getPending() != null)
		    	query.setParameter("pending",catalogBean.getPending().charAt(0));
		if (catalogBean.getRevenueTypeId()!= null)
			query.setParameter("revenueTypeId", catalogBean.getRevenueTypeId());
		if (catalogBean.getCatalogTypeId() != null)
			query.setParameter("catalogTypeId", catalogBean.getCatalogTypeId());
		if (catalogBean.getCatalogName() != null)
			query.setParameter("catalogName", "%" + catalogBean.getCatalogName() + "%");
		if (catalogBean.getDepartmentId() != null)
			query.setParameter("departmentId", catalogBean.getDepartmentId());

		List<Catalog> catalogList = query.getResultList();

		for(Catalog catalogTb : catalogList){
			CatalogBean catalogBeanTemp = new CatalogBean();
			catalogBeanTemp.setId(catalogTb.getCatalogId().toString());
			catalogBeanTemp.setCatalogCode(catalogTb.getCatalogCode());
			catalogBeanTemp.setCatalogName(catalogTb.getCatalogName());
			catalogBeanTemp.setRevenueTypeName(catalogTb.getRevenueTypeId().getRevenueTypeName());
			catalogBeanTemp.setVersion(catalogTb.getVersion());
			if(null!=catalogTb.getCatalogTypeId()){
				MCatalogType catalogTypeTb =  catalogTb.getCatalogTypeId();
				catalogBeanTemp.setCatalogTypeId(catalogTypeTb.getCatalogTypeId()+"");
				catalogBeanTemp.setCatalogTypeName(catalogTypeTb.getCatalogTypeName());
			}
			if(null!=catalogTb.getStartDate()){
				catalogBeanTemp.setStartDate(catalogTb.getStartDate());
				catalogBeanTemp.setStartDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(catalogTb.getStartDate())));
			}
			if(null!=catalogTb.getEndDate()){
				catalogBeanTemp.setEndDate(catalogTb.getEndDate());
				catalogBeanTemp.setEndDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(catalogTb.getEndDate())));
			}
			
			catalogBeanTemp.setStatusUse(catalogTb.getStatus()+"");
			catalogBeanTemp.setStatusApprove(catalogTb.getStatus()+"");
			catalogBeanTemp.setStatusUpdate(catalogTb.getStatus()+"");
			catalogBeanTemp.setStatus(catalogTb.getStatus()+"");
			catalogBeanList.add(catalogBeanTemp);
		}
		logger.info("catalogBeanList"+catalogBeanList.size());
		Page<CatalogBean> entityPage = new PageImpl<CatalogBean>(catalogBeanList, new PageRequest(1, 10), catalogBeanList.size());
		return entityPage;
	}

	public @ResponseBody List<DropdownBean> getMCatalogType() {
		logger.info("getMCatalogType");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MCatalogType> mCatalogTypeList= mCatalogTypeRepository.findAll();
		for(MCatalogType mCatalogTypeTb : mCatalogTypeList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(mCatalogTypeTb.getCatalogTypeId().toString());
			dropdownBean.setDesc(mCatalogTypeTb.getCatalogTypeName());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}

	public @ResponseBody List<DropdownBean> getMRevenueType() {
		logger.info("getMRevenueType");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MRevenueType> mRevenueTypeList= mRevenueTypeRepository.findAll();
		for(MRevenueType mRevenueTypeTb : mRevenueTypeList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(mRevenueTypeTb.getRevenueTypeId().toString());
			dropdownBean.setDesc(mRevenueTypeTb.getRevenueTypeName());
			dropdownBean.setDesc3(mRevenueTypeTb.getRevenueTypeNameEn());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}

	public @ResponseBody List<DropdownBean> getMLevyType() {
		logger.info("getMLevyType");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MLevyType> mLevyTypeList= mLevyTypeRepository.findAll();
		logger.info("getMLevyType size : "+mLevyTypeList.size());
		for(MLevyType mLevyTypeTb : mLevyTypeList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(mLevyTypeTb.getLevyTypeId().toString());
			dropdownBean.setDesc(mLevyTypeTb.getLevyTypeName());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}

	public @ResponseBody List<DropdownBean> getCostCenter() {
		logger.info("getCostCenter");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MCostCenter> mCostCenterList= mCostCenterRepository.findAll();
		logger.info("getCostCenter size : "+mCostCenterList.size());
		for(MCostCenter mCostCenterTb : mCostCenterList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(mCostCenterTb.getCostCenterId().toString());
			dropdownBean.setDesc(mCostCenterTb.getCostCenterCode()+" - "+mCostCenterTb.getCostCenterName());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}

	public @ResponseBody CostCenterBean getCostCenterByCode(String costCenterCode) {
		logger.info("getCostCenterByCode : "+costCenterCode);
		CostCenterBean costCenterBean = new CostCenterBean();
		if(costCenterCode!=null){
			MCostCenter mCostCenterTb = mCostCenterRepository.findMCostCenterByCode(costCenterCode);
			if(mCostCenterTb!=null){
				costCenterBean.setCostCenterId(mCostCenterTb.getCostCenterId());
				costCenterBean.setCostCenterName(mCostCenterTb.getCostCenterName());
				if(mCostCenterTb.getDepartmentId()!=null){
					costCenterBean.setDepartmentId(mCostCenterTb.getDepartmentId().getDepartmentId());
					costCenterBean.setDepartmentCode(mCostCenterTb.getDepartmentId().getDepartmentCode());
					costCenterBean.setDepartmentName(mCostCenterTb.getDepartmentId().getDepartmentName());
				}
				
			}
		}
		return costCenterBean;
	}
	
	public @ResponseBody DepartmentBean getDepartmentByCode(String departmentCode) {
		logger.info("getDepartmentByCode : "+departmentCode);
		DepartmentBean departmentBean = new DepartmentBean();
		if(departmentCode!=null){
			MDepartment mDepartmentTb = mDepartmentRepository.findMDepartmentByCode(departmentCode);
			if(mDepartmentTb!=null){
				departmentBean.setDepartmentId(mDepartmentTb.getDepartmentId());
				departmentBean.setDepartmentName(mDepartmentTb.getDepartmentName());
			}
		}
		return departmentBean;
	}
	
	public @ResponseBody DepartmentBean getDepartmentById(String departmentId) {
		logger.info("getDepartmentById : "+departmentId);
		DepartmentBean departmentBean = new DepartmentBean();
		if(departmentId!=null){
			MDepartment mDepartmentTb = mDepartmentRepository.findOne(Integer.valueOf(departmentId));
			if(mDepartmentTb!=null){
				departmentBean.setDepartmentId(mDepartmentTb.getDepartmentId());
				departmentBean.setDepartmentCode(mDepartmentTb.getDepartmentCode());
				departmentBean.setDepartmentName(mDepartmentTb.getDepartmentName());
			}
		}
		return departmentBean;
	}

	public @ResponseBody GlBean getGlByCode(String glCode) {
		logger.info("getGlByCode : "+glCode);
		GlBean glBean = new GlBean();
		if(glCode!=null){
			MGl mGlTb = mGlRepository.findMGlByCode(glCode);
			if(mGlTb!=null){
				glBean.setGlId(mGlTb.getGlId());
				glBean.setGlName(mGlTb.getGlName());
			}
		}
		return glBean;
	}

	public Boolean save(CatalogBean catalogBean,Integer userId) {
		logger.info("save");
		//save CATALOG
		Catalog catalogTb = saveCatalog(catalogBean, userId);

		//save CATALOG_STRUCTURE
		CatalogStructure catalogStructureTb = saveCatalogStructure(catalogTb, userId);

		//save CATALOG_STRUCTURE_ITEM
		saveCatalogStructureItem(catalogStructureTb,catalogBean.getStructureItem(),userId);

		//save CATALOG_CONDITION_TEXT
		saveCatalogConditionText(catalogBean.getConditionText(), catalogTb,userId);

		//save CATALOG_COST_CENTER
		saveCatalogCostCenter(catalogBean.getCostCenter(), catalogTb,userId);

		return true;
	}

	private Catalog saveCatalog(CatalogBean catalogBean,Integer userId) {
		logger.info("saveCatalog");
		if(catalogBean == null) return null;

		Catalog catalogTb = new Catalog();
		if(catalogBean.getId()!=null){
			logger.info("editCatalog");
			catalogTb = catalogRepository.findOne(Integer.valueOf(catalogBean.getId()));
		}
		try {

			User userTb = userRepository.findOne(userId);
			MDepartment mDepartmentTb = new MDepartment();
			
			if(userTb.getUserOrganizationList2()!=null){
				if(userTb.getUserOrganizationList2().get(0).getDepartmentId()!=null){
					mDepartmentTb = mDepartmentRepository.findOne(userTb.getUserOrganizationList2().get(0).getDepartmentId().getDepartmentId());
				}
			}


			MCatalogType mCatalogTypeTb = new MCatalogType();
			mCatalogTypeTb = mCatalogTypeRepository.findOne(Integer.valueOf(catalogBean.getCatalogTypeId()));

			MRevenueType mRevenueTypeTb = new MRevenueType();
			mRevenueTypeTb = mRevenueTypeRepository.findOne(Integer.valueOf(catalogBean.getRevenueTypeId()));
		
			
			
			if("1".equals(catalogBean.getIsCopy()) || "1".equals(catalogBean.getIsEdit())){
				logger.info("copy or edit catalogCode");
				catalogTb.setCatalogCode(catalogBean.getCatalogCode());
				catalogTb.setVersion(catalogBean.getVersion());
			}else{
				logger.info("generate new catalogCode");
				catalogTb.setCatalogCode(mDepartmentTb.getDepartmentCode()+generateCodeService.generateCatalogCode());
				catalogTb.setVersion("1");
			}
			catalogTb.setCatalogName(catalogBean.getCatalogName());
			catalogTb.setCatalogTypeId(mCatalogTypeTb);
			catalogTb.setRevenueTypeId(mRevenueTypeTb);
			System.out.println("catalogBean.getAmount() : "+catalogBean.getAmount());
			if(catalogBean.getAmount()!=null && catalogBean.getAmount()!=""){
				catalogTb.setAmount(Integer.valueOf(catalogBean.getAmount().replace(".", "")));
			}else if(catalogBean.getAmount()==""){
				catalogTb.setAmount(0);
			}
			if(catalogBean.getIsShow()!=null){
				catalogTb.setIsShow(catalogBean.getIsShow().charAt(0));
			}
			
			if(catalogBean.getStartDate()!=null){
			    catalogTb.setStartDate(catalogBean.getStartDate());
			}
			if(catalogBean.getEndDate()!=null){
				catalogTb.setEndDate(catalogBean.getEndDate());
			}
			if(catalogBean.getBreakDate()!=null){
				catalogTb.setBreakDate(catalogBean.getBreakDate());
			}
			if(catalogBean.getBreakReason()!=null){
				catalogTb.setBreakReason(catalogBean.getBreakReason());
			}

			if(catalogBean.getIsBreak()!=null){
				catalogTb.setStatus('2');
			}else{
				catalogTb.setStatus(catalogBean.getStatus().charAt(0));
			}
			

			catalogTb.setCreatedDate(new Date());
			catalogTb.setCreatedBy(userTb);
			catalogTb.setUpdatedDate(new Date());
			catalogTb.setUpdatedBy(userTb);

			catalogRepository.save(catalogTb);

		}catch (Exception ex) {
			logger.error("Cannot save data " + catalogBean, ex);
			return null;
		}
		return catalogTb;
	}

	private CatalogStructure saveCatalogStructure(Catalog catalogTb,Integer userId) {
		logger.info("saveCatalogStructure");
		if(catalogTb == null) return null;
		//EDIT
		CatalogStructure catalogStructureTb = catalogStructureRepository.findCatalogStructureByCatId(catalogTb.getCatalogId());
		if(catalogStructureTb==null){
			catalogStructureTb = new CatalogStructure();
		}else{
			logger.info("editCatalogStructure");
		}

		try {
			User userTb = new User();
			userTb = userRepository.findOne(userId);

			catalogStructureTb.setCatalogId(catalogTb);
			catalogStructureTb.setStatus('1');

			catalogStructureTb.setCreatedDate(new Date());
			catalogStructureTb.setCreatedBy(userTb);
			catalogStructureTb.setUpdatedDate(new Date());
			catalogStructureTb.setUpdatedBy(userTb);

			catalogStructureRepository.save(catalogStructureTb);

		}catch (Exception ex) {
			logger.error("Cannot save data " + catalogTb, ex);
			return null;
		}
		return catalogStructureTb;
	}

	private Boolean saveCatalogStructureItem(CatalogStructure catalogStructureTb,List<CatalogStructureItemBean> structureItemBeanList,Integer userId) {
		logger.info("saveCatalogStructureItem");
		if(catalogStructureTb == null) return false;

		try {
			User userTb = new User();
			userTb = userRepository.findOne(userId);
	
			MDepartment mDepartmentTb = new MDepartment();
			
			if(userTb.getUserOrganizationList2()!=null){
				if(userTb.getUserOrganizationList2().get(0).getDepartmentId()!=null){
					mDepartmentTb = mDepartmentRepository.findOne(userTb.getUserOrganizationList2().get(0).getDepartmentId().getDepartmentId());
				}
			}

			
			logger.info("structureItemBeanList.size() : "+structureItemBeanList.size());

			if(structureItemBeanList.size()>0){
				for(int i=0;i<structureItemBeanList.size();i++){
					
					CatalogStructureItem catalogStructureItemTb = new CatalogStructureItem();
					//EDIT
					if(catalogStructureTb.getCatalogStructureItemList()!=null){
						
						logger.info("editCatalogStructureItem : "+catalogStructureTb.getCatalogStructureItemList().size());
						
						if(i<catalogStructureTb.getCatalogStructureItemList().size()){
							catalogStructureItemTb = catalogStructureTb.getCatalogStructureItemList().get(i);
						}
					}

					MRevenueType mRevenueTypeTb = new MRevenueType();
					mRevenueTypeTb = mRevenueTypeRepository.findOne(Integer.valueOf(structureItemBeanList.get(i).getRevenueTypeId()));

					MLevyType mLevyTypeTb = new MLevyType();
					mLevyTypeTb = mLevyTypeRepository.findOne(Integer.valueOf(structureItemBeanList.get(i).getLevyTypeId()));

					if(structureItemBeanList.get(i).getCostCenterId()!=null){
						MCostCenter mCostCenterTb = mCostCenterRepository.findOne(Integer.valueOf(structureItemBeanList.get(i).getCostCenterId()));
						catalogStructureItemTb.setCostCenterId(mCostCenterTb);
					}else{
						MCostCenter mCostCenterTb = mCostCenterRepository.findOne(61);
						catalogStructureItemTb.setCostCenterId(mCostCenterTb);

					}
					
					if(structureItemBeanList.get(i).getDepartmentId()!=null){
						mDepartmentTb = mDepartmentRepository.findOne(Integer.valueOf(structureItemBeanList.get(i).getDepartmentId()));
						catalogStructureItemTb.setDepartmentId(mDepartmentTb);
					}else{
						catalogStructureItemTb.setDepartmentId(mDepartmentTb);
					}
				
					if(structureItemBeanList.get(i).getGlId()!=null){
						//MGl mGlTb = mGlRepository.findOne(Integer.valueOf(structureItemBeanList.get(i).getGlId()));
						catalogStructureItemTb.setGlId(Integer.valueOf(structureItemBeanList.get(i).getGlId()));
					}
					System.out.println("structureItemBeanList.get(i).getAccountDepositId() : "+structureItemBeanList.get(i).getAccountDepositId());


					if(structureItemBeanList.get(i).getAccountDepositId()!=null){
						MAccountDeposit mAccountDepositTb =  mAccountDepositRepository.findOne(Integer.valueOf(structureItemBeanList.get(i).getAccountDepositId()));
						catalogStructureItemTb.setAccountDepositId(mAccountDepositTb);
					}else{
						MAccountDeposit mAccountDepositTb =  mAccountDepositRepository.findOne(1);
						catalogStructureItemTb.setAccountDepositId(mAccountDepositTb);
					}
					
					System.out.println("structureItemBeanList.get(i).getCostCenterId() : "+structureItemBeanList.get(i).getCostCenterId());
					System.out.println("structureItemBeanList.get(i).getDepartmentId() : "+structureItemBeanList.get(i).getDepartmentId());
					System.out.println("structureItemBeanList.get(i).getGlId() : "+structureItemBeanList.get(i).getGlId());
					System.out.println("structureItemBeanList.get(i).getAccountDepositId() : "+structureItemBeanList.get(i).getAccountDepositId());

					catalogStructureItemTb.setCatalogStructureId(catalogStructureTb);
					catalogStructureItemTb.setRevenueTypeId(mRevenueTypeTb);
					catalogStructureItemTb.setLevyTypeId(mLevyTypeTb);
					catalogStructureItemTb.setPercent(Integer.valueOf(structureItemBeanList.get(i).getPercent()));


					catalogStructureItemTb.setCreatedDate(new Date());
					catalogStructureItemTb.setCreatedBy(userTb);
					catalogStructureItemTb.setUpdatedDate(new Date());
					catalogStructureItemTb.setUpdatedBy(userTb);

					catalogStructureItemRepository.save(catalogStructureItemTb);
				}
			}

		}catch (Exception ex) {
			logger.error("Cannot save data " + catalogStructureTb, ex);
			return false;
		}
		return true;
	}

	private Boolean saveCatalogConditionText(List<CatalogConditionTextBean> conditionText,Catalog catalogTb,Integer userId) {
		logger.info("saveCatalogConditionText");

		if(catalogTb == null || conditionText.size() == 0) return false;

		try {
			User userTb = new User();
			userTb = userRepository.findOne(1);
			logger.info("conditionText.size() : "+conditionText.size());
			for(int i=0; i<conditionText.size(); i++){

				CatalogConditionText catalogConditionTextTb = new CatalogConditionText();
				
				//EDIT
				if(catalogTb.getCatalogConditionTextList()!=null){
					
					logger.info("editCatalogConditionText : "+catalogTb.getCatalogConditionTextList().size());
					
					if(i<catalogTb.getCatalogConditionTextList().size()){
						catalogConditionTextTb = catalogTb.getCatalogConditionTextList().get(i);
					}
				}

				catalogConditionTextTb.setCatalogId(catalogTb);
				catalogConditionTextTb.setConditionText(conditionText.get(i).getConditionText());
				catalogConditionTextTb.setDisplayNo(i+1);

				catalogConditionTextTb.setCreatedDate(new Date());
				catalogConditionTextTb.setCreatedBy(userTb);
				catalogConditionTextTb.setUpdatedDate(new Date());
				catalogConditionTextTb.setUpdatedBy(userTb);

				catalogConditionTextRepository.save(catalogConditionTextTb);
			}

		}catch (Exception ex) {
			logger.error("Cannot save data " + catalogTb, ex);
			return false;
		}
		return true;
	}

	private Boolean saveCatalogCostCenter(List<CatalogCostCenterBean> costCenterBeanList,Catalog catalogTb,Integer userId) {
		logger.info("saveCatalogCostCenter");
		if(catalogTb == null || costCenterBeanList.size() == 0) return false;

		try {
			User userTb = new User();
			userTb = userRepository.findOne(userId);
			logger.info("costCenterBeanList.size() : "+costCenterBeanList.size());
			for(int i=0; i<costCenterBeanList.size(); i++){

				CatalogCostCenter catalogCostCenterTb = new CatalogCostCenter();
				
				//EDIT
				if(catalogTb.getCatalogCostCenterList()!=null){
					
					logger.info("editCatalogCostCenter : "+catalogTb.getCatalogCostCenterList().size());
					
					if(i<catalogTb.getCatalogCostCenterList().size()){
						catalogCostCenterTb = catalogTb.getCatalogCostCenterList().get(i);
					}
				}

				MCostCenter mCostCenterTb = new MCostCenter();
				mCostCenterTb = mCostCenterRepository.findOne(Integer.valueOf(costCenterBeanList.get(i).getCostCenterId()));

				catalogCostCenterTb.setCatalogId(catalogTb);
				catalogCostCenterTb.setCostCenterId(mCostCenterTb);

				catalogCostCenterTb.setCreatedDate(new Date());
				catalogCostCenterTb.setCreatedBy(userTb);
				catalogCostCenterTb.setUpdatedDate(new Date());
				catalogCostCenterTb.setUpdatedBy(userTb);

				catalogCostCenterRepository.save(catalogCostCenterTb);
			}

		}catch (Exception ex) {
			logger.error("Cannot save data " , ex);
			return false;
		}
		return true;
	}

	public List<DropdownBean> getCatalogByCatType(Integer id){
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		MCatalogType catalogTypeTb= catalogRepository.getCatalogByCatType(id);
		//		MCatalogType catalogTypeTb= catalogRepository.findOne(id);
		if(catalogTypeTb!=null){
			for(Catalog catalogTb : catalogTypeTb.getCatalogList()){
				DropdownBean dropdownBean = new DropdownBean();
				dropdownBean.setId(catalogTb.getCatalogId()+"");
				dropdownBean.setDesc(catalogTb.getCatalogName());
				dropdownBeanList.add(dropdownBean);
			}
		}

		return dropdownBeanList;
	}

	public boolean removeCatalog(List<CatalogBean> catalogBean)  throws Exception {
		Collection<Catalog> catalogList = new ArrayList<Catalog>();
		for(CatalogBean item : catalogBean){
			Catalog catalog;
			if (null != (catalog = catalogRepository.findOne(Integer.parseInt(item.getId())))) {
				catalog.setStatus(EPaymentConstant.NO.charAt(0));
				catalogList.add(catalog);
			} else {
				throw new Exception();
			}
		}
		catalogRepository.save(catalogList);

		return true;
	}

	public boolean removeCatalogById(Integer id)  throws Exception {
		Collection<Catalog> catalogList = new ArrayList<Catalog>();

		Catalog catalog;
		if (null != (catalog = catalogRepository.findOne(id))) {
			catalog.setStatus(EPaymentConstant.NO.charAt(0));
			catalogRepository.save(catalogList);
		} else {
			throw new Exception();
		}



		return true;
	}

	public List<DropdownBean> getCatalogAllByStatus(){
		logger.info("catalogListAll");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		StringBuffer queryStr = new StringBuffer();
		queryStr.append(" SELECT distinct (CATALOG_CODE)"); 
		queryStr.append(" , MAX(c.VERSION) ");
		queryStr.append(" FROM PAYDB.CATALOG c ");
		queryStr.append(" WHERE c.STATUS <> 'N' ");
		queryStr.append(" Group by c.CATALOG_CODE ");
		Query query = this.entityManager.createNativeQuery(queryStr.toString());
		List<Object[]> objects = query.getResultList();
		for (Object[] obj : objects) {
		    logger.info("Code : "+obj[0].toString() +" V :"+obj[1].toString());
		    List<Catalog>  catalogTbList = catalogRepository.getCatalogAllByStatus(obj[0].toString(),obj[1].toString());
			if(catalogTbList.size()!=0){
        			DropdownBean dropdownBean = new DropdownBean();
        			dropdownBean.setId(catalogTbList.get(0).getCatalogId().toString());
        			dropdownBean.setDesc(catalogTbList.get(0).getCatalogName());
        			dropdownBean.setDesc2(catalogTbList.get(0).getCatalogCode());
        			dropdownBeanList.add(dropdownBean);
			}
		}
		
//		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
//		List<Catalog> catalogList= catalogRepository.getCatalogAllByStatus('N');
//		for(Catalog mCatalogTypeTb : catalogList){
//			DropdownBean dropdownBean = new DropdownBean();
//			dropdownBean.setId(mCatalogTypeTb.getCatalogId().toString());
//			dropdownBean.setDesc(mCatalogTypeTb.getCatalogName());
//			dropdownBean.setDesc2(mCatalogTypeTb.getCatalogCode());
//			dropdownBeanList.add(dropdownBean);
//		}
		return dropdownBeanList;
	}
	public byte[] exportExcel( List<CatalogBean> catalogBean){
		logger.info("exportExcel");
		String sheetName = "sheet 1";
		Object[][] data = new Object[catalogBean.size()+1][10];
		
		data[0][0] = messageResource.getMessage("catalog.header.catalogCode");
		data[0][1] = messageResource.getMessage("catalog.header.catalogName");
		data[0][2] = messageResource.getMessage("catalog.header.version");
		data[0][3] = messageResource.getMessage("catalog.header.revenueTypeName");
		data[0][4] = messageResource.getMessage("catalog.header.catalogTypeName");
		data[0][5] = messageResource.getMessage("catalog.header.status1");
		data[0][6] = messageResource.getMessage("catalog.header.status2");
		data[0][7] = messageResource.getMessage("catalog.header.startDate");
		data[0][8] = messageResource.getMessage("catalog.header.endDate");
		data[0][9] = messageResource.getMessage("catalog.header.status3");

		for(int i = 0 ;i<catalogBean.size();i++){
			data[i+1][0] = catalogBean.get(i).getCatalogCode();
			data[i+1][1] = catalogBean.get(i).getCatalogName();
			data[i+1][2] = catalogBean.get(i).getVersion();
			data[i+1][3] = catalogBean.get(i).getRevenueTypeName();
			data[i+1][4] = catalogBean.get(i).getCatalogTypeName();
			
			if(catalogBean.get(i).getStatus().equals("0")){
				data[i+1][5] = getStatusName("0"); //saveTemp
				data[i+1][6] = getStatusName("3"); //waitForApprove
			}else if(catalogBean.get(i).getStatus().equals("1")){
				data[i+1][5] = getStatusName("1"); //using
				data[i+1][6] = getStatusName("4"); //approve
			}else{
				data[i+1][5] = getStatusName("5"); //break
				data[i+1][6] = getStatusName("2"); //notApprove
			}
			
			data[i+1][7] = catalogBean.get(i).getStartDateStr();
			data[i+1][8] = catalogBean.get(i).getEndDateStr();
			if(catalogBean.get(i).getStatus().equals("2")){
				data[i+1][9] = getStatusName("6"); //waitForUpdate
			}else{
				data[i+1][9] = "-";
			}
			

		}

		byte[] output = ReportUtilService.genarateCustomExcelReport(data, sheetName);
		return output;

	}
	
	public CatalogBean getCatalog(Integer id){
		logger.info("getCatalog id : "+id);
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
		Catalog catalog = catalogRepository.findOne(id);
		CatalogBean bean = new CatalogBean();
		if(catalog != null){
			bean.setIsEdit("1");
			bean.setId(catalog.getCatalogId().toString());
			bean.setCatalogCode(catalog.getCatalogCode());
			bean.setCatalogName(catalog.getCatalogName());
			bean.setCatalogTypeId(catalog.getCatalogTypeId().getCatalogTypeId().toString());
			bean.setRevenueTypeId(catalog.getRevenueTypeId().getRevenueTypeId().toString());
			
			if(catalog.getAmount()!=0){
				bean.setAmount(String.valueOf(catalog.getAmount()));
			}
			bean.setIsShow(String.valueOf(catalog.getIsShow()));

			if(catalog.getEndDate()!=null){
				bean.setEndDate(catalog.getEndDate());
			}
			if(catalog.getStartDate()!=null){
				bean.setStartDate(catalog.getStartDate());
			}
			
			if(catalog.getBreakDate()!=null){
				bean.setIsBreak("1");
				bean.setBreakDate(catalog.getBreakDate());
				bean.setBreakReason(catalog.getBreakReason());
			}
			
			bean.setStatus(String.valueOf(catalog.getStatus()));
		
			bean.setVersion(catalog.getVersion());
			
			List<CatalogConditionTextBean> conditionText = new ArrayList<CatalogConditionTextBean>();
			logger.info("catalog.getCatalogConditionTextList() : "+catalog.getCatalogConditionTextList().size());
			for(CatalogConditionText catalogConditionText : catalog.getCatalogConditionTextList()){
				CatalogConditionTextBean catConTextBean = new CatalogConditionTextBean();
				catConTextBean.setId(catalogConditionText.getCatalogConditionTextId().toString());
				catConTextBean.setCatalogId(catalogConditionText.getCatalogId().getCatalogId().toString());
				catConTextBean.setDisplayNo(String.valueOf(catalogConditionText.getDisplayNo()));
				catConTextBean.setConditionText(catalogConditionText.getConditionText());
				conditionText.add(catConTextBean);
			}
			bean.setConditionText(conditionText);
			
			List<CatalogCostCenterBean> costCenter = new ArrayList<CatalogCostCenterBean>();
			List<DropdownBean> costCenterList = new ArrayList<DropdownBean>();
			for(CatalogCostCenter catalogCostCenter : catalog.getCatalogCostCenterList()){
				CatalogCostCenterBean catCostBean = new CatalogCostCenterBean();
				DropdownBean dropdownBean = new DropdownBean();
				catCostBean.setCatalogId(String.valueOf(catalogCostCenter.getCatalogId().getCatalogId()));
				
				
				catCostBean.setCostCenterId(String.valueOf(catalogCostCenter.getCostCenterId().getCostCenterId()));
				catCostBean.setId(String.valueOf(catalogCostCenter.getCatalogCostCenterId()));
				
				dropdownBean.setId(String.valueOf(catalogCostCenter.getCostCenterId().getCostCenterId()));
				dropdownBean.setDesc(catalogCostCenter.getCostCenterId().getCostCenterCode() +" - "+catalogCostCenter.getCostCenterId().getCostCenterName());
				costCenter.add(catCostBean);
				costCenterList.add(dropdownBean);
			}
			bean.setCostCenter(costCenter);
			bean.setCostCenterList(costCenterList);


			CatalogStructure catalogStructureTb = catalogStructureRepository.findCatalogStructureByCatId(catalog.getCatalogId());
			if(catalogStructureTb!=null){
				
				List<CatalogStructureItemBean> structureItem = new ArrayList<CatalogStructureItemBean>();
				if(catalogStructureTb.getCatalogStructureItemList()!=null){
					for(CatalogStructureItem catalogStructureItem : catalogStructureTb.getCatalogStructureItemList()){
						CatalogStructureItemBean catStructureItemBean = new CatalogStructureItemBean();
						catStructureItemBean.setId(String.valueOf(catalogStructureItem.getCatalogStructureItemId()));
						catStructureItemBean.setRevenueTypeId(String.valueOf(catalogStructureItem.getRevenueTypeId().getRevenueTypeId()));
						catStructureItemBean.setLevyTypeId(String.valueOf(catalogStructureItem.getLevyTypeId().getLevyTypeId()));
						catStructureItemBean.setPercent(String.valueOf(catalogStructureItem.getPercent()));
						
						if(catalogStructureItem.getCostCenterId()!=null && catalogStructureItem.getLevyTypeId().getLevyTypeId()==3){
							catStructureItemBean.setCostCenterId(String.valueOf(catalogStructureItem.getCostCenterId().getCostCenterId()));
							catStructureItemBean.setCostCenterCode(catalogStructureItem.getCostCenterId().getCostCenterCode());
							catStructureItemBean.setCostCenterName(catalogStructureItem.getCostCenterId().getCostCenterName());
							catStructureItemBean.setBudgetCode(catalogStructureItem.getCostCenterId().getCostCenterCode().substring(0, 5));
						}
						
						if(catalogStructureItem.getDepartmentId()!=null){
							catStructureItemBean.setDepartmentId(String.valueOf(catalogStructureItem.getDepartmentId().getDepartmentId()));
							catStructureItemBean.setDepartmentCode(catalogStructureItem.getDepartmentId().getDepartmentCode());
							catStructureItemBean.setDepartmentName(catalogStructureItem.getDepartmentId().getDepartmentName());
						}
						if(catalogStructureItem.getGlId()!=0){
							MGl mGlTb =  mGlRepository.findOne(catalogStructureItem.getGlId());
							if(mGlTb!=null){
								catStructureItemBean.setGlId(String.valueOf(mGlTb.getGlId()));
								catStructureItemBean.setGlCode(mGlTb.getGlCode());
								catStructureItemBean.setGlName(mGlTb.getGlName());
								
								MMoneySource mMoneySourceTb = mMoneySourceRepository.findMMoneySourceByGlCode(Long.valueOf(mGlTb.getGlCode()));
								if(mMoneySourceTb!=null){
									catStructureItemBean.setMoneySourceId(String.valueOf(mMoneySourceTb.getMoneySourceId()));
									catStructureItemBean.setMoneySourceCode(mMoneySourceTb.getMoneySourceCode());
									catStructureItemBean.setMoneySourceName(mMoneySourceTb.getMoneySourceName());			
								}
								
								MStandardRevernueGl mStandardRevernueGl = mStandardRevernueGlRepository.findMStandardRevernueGlByGlCode(Long.valueOf(mGlTb.getGlCode()));
								if(mStandardRevernueGl!=null){
									catStructureItemBean.setStandardRevernueId(String.valueOf(mStandardRevernueGl.getStandardRevernueId().getStandardRevernueId()));
									catStructureItemBean.setStandardRevernueCode(mStandardRevernueGl.getStandardRevernueId().getStandardRevernueCode());
									catStructureItemBean.setStandardRevernueName(mStandardRevernueGl.getStandardRevernueId().getStandardRevernueName());				
								}
							}
							
						}
						if(catalogStructureItem.getAccountDepositId()!=null && catalogStructureItem.getRevenueTypeId().getRevenueTypeId() !=1){
							catStructureItemBean.setAccountDepositId(String.valueOf(catalogStructureItem.getAccountDepositId().getAccountDepositId()));
							catStructureItemBean.setAccountDepositCode(catalogStructureItem.getAccountDepositId().getAccountCode());
							catStructureItemBean.setAccountDepositOwner(catalogStructureItem.getAccountDepositId().getOwner());
							catStructureItemBean.setAccountDepositName(catalogStructureItem.getAccountDepositId().getAccountName());
						}
						structureItem.add(catStructureItemBean);
					}
					bean.setStructureItem(structureItem);
				}
			}	
			
		}
		return bean;
	}
	
	public @ResponseBody AccountDepositBean getAccountDepositByCode(String accountDepositCode) {
		logger.info("getAccountDepositByCode : "+accountDepositCode);
		AccountDepositBean accountDepositBean = new AccountDepositBean();
		if(accountDepositCode!=null){
			
			
			User userAu = null;
			Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if ((principal != null) && (principal instanceof User)) {
				userAu = (User) principal;
			} 		
			User user = userRepository.findOne(userAu.getUserId());
			String DeptCode = null;
			if(user.getUserOrganizationList2()!=null){
				if(user.getUserOrganizationList2().get(0).getDepartmentId()!=null){
					DeptCode = user.getUserOrganizationList2().get(0).getDepartmentId().getDepartmentCode();
				}
			}
			
			System.out.println("-----DeptCode------ "+ DeptCode);
			
			MAccountDeposit mAccountDepositTb = mAccountDepositRepository.findMAccountDepositByCode(accountDepositCode,DeptCode);
			if(mAccountDepositTb!=null){
				accountDepositBean.setAccountDepositId(mAccountDepositTb.getAccountDepositId());
				accountDepositBean.setOwner(mAccountDepositTb.getOwner());
				accountDepositBean.setAccountName(mAccountDepositTb.getAccountName());
			}
		}
		return accountDepositBean;
	}
	
	public @ResponseBody StandardRevernueBean getStandardRevernueByGlCode(String glCode) {
		logger.info("getStandardRevernueByGlCode : "+glCode);
		StandardRevernueBean standardRevernueBean = new StandardRevernueBean();
		if(glCode!=null){
			MStandardRevernueGl mStandardRevernueGl = mStandardRevernueGlRepository.findMStandardRevernueGlByGlCode(Long.valueOf(glCode));
			if(mStandardRevernueGl.getStandardRevernueId()!=null){
				standardRevernueBean.setStandardRevernueId(mStandardRevernueGl.getStandardRevernueId().getStandardRevernueId());
				standardRevernueBean.setStandardRevernueCode(mStandardRevernueGl.getStandardRevernueId().getStandardRevernueCode());
				standardRevernueBean.setStandardRevernueName(mStandardRevernueGl.getStandardRevernueId().getStandardRevernueName());				
			}
		}
		return standardRevernueBean;
	}
	
	public @ResponseBody MoneySourceBean getMMoneySourceByGlCode(String glCode) {
		logger.info("getMMoneySourceByGlCode : "+glCode);
		MoneySourceBean moneySourceBean = new MoneySourceBean();
		if(glCode!=null){
			MMoneySource mMoneySourceTb = mMoneySourceRepository.findMMoneySourceByGlCode(Long.valueOf(glCode));
			if(mMoneySourceTb!=null){
				moneySourceBean.setMoneySourceId(mMoneySourceTb.getMoneySourceId());
				moneySourceBean.setMoneySourceCode(mMoneySourceTb.getMoneySourceCode());
				moneySourceBean.setMoneySourceName(mMoneySourceTb.getMoneySourceName());			
			}
		}
		return moneySourceBean;
	}
	@SuppressWarnings("unchecked")
	public Boolean copyCatalog(DropdownBean catalogCopy,Integer userId) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
		
		logger.info("copyCatalog Id : "+catalogCopy.getId());
		Catalog catalogTb = catalogRepository.findOne(Integer.valueOf(catalogCopy.getId()));
		
		if(catalogTb!=null){
			logger.info("getCatalog Id : "+catalogTb.getCatalogId());
			CatalogBean catalogBean = getCatalog(catalogTb.getCatalogId());
			catalogBean.setId(null);
			catalogBean.setIsCopy("1");
			int version = Integer.valueOf(catalogBean.getVersion());
			version = version+1;
			catalogBean.setVersion(String.valueOf(version));
			for(int i=0;i<catalogBean.getConditionText().size();i++){
				catalogBean.getConditionText().get(i).setId(null);
				
			}
			for(int i=0;i<catalogBean.getCostCenterList().size();i++){
				catalogBean.getCostCenterList().get(i).setId(null);
			}
			for(int i=0;i<catalogBean.getStructureItem().size();i++){
				catalogBean.getStructureItem().get(i).setId(null);
			}
			
			save(catalogBean, userId);
			
		}
	    return true;
	}
	
	private String getStatusName(String status){
		switch ( status )
		{
		case "0"
		:     return messageResource.getMessage("catalog.status.saveTemp");
		case "1"
		:     return messageResource.getMessage("catalog.status.using");
		case "2"
		:     return messageResource.getMessage("catalog.status.notApprove");
		case "3"
		:     return messageResource.getMessage("catalog.status.waitForApprove");
		case "4"
		:     return messageResource.getMessage("catalog.status.approved");
		case "5"
		:     return messageResource.getMessage("catalog.status.break");
		case "6"
		:     return messageResource.getMessage("catalog.status.waitForUpdate");
		default
		: return "สถานะไม่ถูกต้อง";

		}
	}
}
