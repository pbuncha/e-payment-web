/**
 * 
 */
package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.entity.Bank;
import th.go.cgd.epayment.entity.MBusinessArea;
import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MPersonType;
import th.go.cgd.epayment.entity.MStatus;
import th.go.cgd.epayment.entity.MTitleName;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.repository.BankRepository;
import th.go.cgd.epayment.repository.MBusinessAreaRepository;
import th.go.cgd.epayment.repository.MCostCenterRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.repository.MPersonTypeRepository;
import th.go.cgd.epayment.repository.MStatusRepository;
import th.go.cgd.epayment.repository.MTitleNameRepository;
import th.go.cgd.epayment.service.interfaces.IMasterService;
/**
 * @author wichuda.k Nov 11, 2017
 *
 */

@Service
@Transactional
public class MasterServiceImpl implements IMasterService{
	private static final Logger logger = Logger.getLogger(MasterServiceImpl.class);
	@Autowired
	private MTitleNameRepository titleNameRepository;
	@Autowired
	private MPersonTypeRepository personTypeRepository;
	@Autowired
	private MCostCenterRepository costCenterRepository;
	@Autowired
	private MDepartmentRepository departmentRepository;
	@PersistenceContext
	private EntityManager entityManager;
	MTitleName mTitleName;
	
	@Autowired
	private BankRepository bankRepository;
	@Autowired
	private MStatusRepository statusRepository;
	
	@Autowired
	private MBusinessAreaRepository mBusinessAreaRepository;
	
	public List<DropdownBean> getTitleList() {
		logger.debug("getTitleList");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MTitleName> objectList = (List<MTitleName>) titleNameRepository.findAll();
		for(MTitleName obj:objectList){
			if(obj.getTitleId()!= 4){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getTitleId().toString());
			dropdownBean.setDesc(obj.getTitleName().toString());
			dropdownBean.setDesc2(obj.getTitleNameEn().toString());
			dropdownBeanList.add(dropdownBean);
			}
		}
		return dropdownBeanList;
	}

	public List<DropdownBean> getPersonTypeList() {
		logger.debug("getPersonTypeList");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MPersonType> objectList = (List<MPersonType>) personTypeRepository.findAll();
		for(MPersonType obj:objectList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getPersonTypeId().toString());
			dropdownBean.setDesc(obj.getPersonTypeName().toString());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}
	
	
	public List<DropdownBean> getMCostCenterList() {
		logger.debug("getMCostCenterList");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MCostCenter> objectList = (List<MCostCenter>) costCenterRepository.findAll();
		for(MCostCenter obj:objectList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getCostCenterId().toString());
			dropdownBean.setDesc(obj.getCostCenterName().toString());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}
	
	public List<DropdownBean> getMDepartmentList() {
		logger.debug("getMDepartmentList");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MDepartment> objectList = (List<MDepartment>) departmentRepository.findAll();
		for(MDepartment obj:objectList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getDepartmentId().toString());
			dropdownBean.setDesc(obj.getDepartmentName().toString());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}
	
	public List<DropdownBean> getBank() {
		logger.debug("getBank");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<Bank> objectList =  bankRepository.findAll();
		for(Bank obj:objectList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getBankId()+"");
			dropdownBean.setDesc(obj.getBankNameTh());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}
	@Override
	public List<DropdownBean> getStatusUse(String [] statusId) {
		logger.debug("getMstatus");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MStatus> objectList =  statusRepository.getMStatusById(statusId);
		for(MStatus obj:objectList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getStatus()+"");
			dropdownBean.setDesc(obj.getStatusName());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}
	
	public MTitleName getMTitleNameById(Integer id) {
	    return titleNameRepository.findOne(id);
	}
	
	public List<DropdownBean> getMBusinessAreaList() {
		logger.debug("getMBusinessAreaList");
		List<DropdownBean> dropdownBeanList = new ArrayList<DropdownBean>();
		List<MBusinessArea> objectList = (List<MBusinessArea>) mBusinessAreaRepository.findAll();
		for(MBusinessArea obj:objectList){
			DropdownBean dropdownBean = new DropdownBean();
			dropdownBean.setId(obj.getBusinessAreaId().toString());
			dropdownBean.setDesc(obj.getBusinessAreaName().toString());
			dropdownBeanList.add(dropdownBean);
		}
		return dropdownBeanList;
	}
}