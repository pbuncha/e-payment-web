package th.go.cgd.epayment.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.entity.MAmphur;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MTambon;
import th.go.cgd.epayment.model.ProvinceDropdownBean;
import th.go.cgd.epayment.repository.MAmphurRepository;
import th.go.cgd.epayment.repository.MProvinceRepository;
import th.go.cgd.epayment.repository.MTambonRepository;
import th.go.cgd.epayment.service.interfaces.IProvinceDropdownService;

@Transactional
@Service
public class ProvinceDropdownServiceImpl implements IProvinceDropdownService {

	@Autowired
	private MProvinceRepository mProvinceRepository;
	@Autowired
	private MAmphurRepository mAmphurRepository;
	@Autowired
	private MTambonRepository mTambonRepository;

	@Override
	public List<ProvinceDropdownBean> getAllprovince() {
		List<MProvince> mProvinces = mProvinceRepository.findAll();
		List<ProvinceDropdownBean> dropdownBeans = new ArrayList<ProvinceDropdownBean>();
		for (MProvince obj : mProvinces) {
			ProvinceDropdownBean bean = new ProvinceDropdownBean();
			bean.setId(obj.getProvinceId());
			bean.setNameEN(obj.getProvinceNameEn());
			bean.setNameTH(obj.getProvinceNameTh());
			dropdownBeans.add(bean);
		}

		return dropdownBeans;
	}

	@Override
	public List<ProvinceDropdownBean> getDistrictByProvince(Integer provinceId) {
		List<MAmphur> mAmphurs = mAmphurRepository.getAmphurByProvince(provinceId);
		List<ProvinceDropdownBean> dropdownBeans = new ArrayList<ProvinceDropdownBean>();
		for (MAmphur obj : mAmphurs) {
			ProvinceDropdownBean bean = new ProvinceDropdownBean();
			bean.setId(obj.getAmphurId());
			bean.setNameEN(obj.getAmphurNameEn());
			bean.setNameTH(obj.getAmphurNameTh());
			dropdownBeans.add(bean);
		}
		return dropdownBeans;
	}

	@Override
	public List<ProvinceDropdownBean> getSubDistrictByDistrict(Integer DistrictId) {
		List<MTambon> mTambons = mTambonRepository.getTambonByAmphur(DistrictId);
		List<ProvinceDropdownBean> dropdownBeans = new ArrayList<ProvinceDropdownBean>();
		for (MTambon obj : mTambons) {
			ProvinceDropdownBean bean = new ProvinceDropdownBean();
			bean.setId(obj.getTambonId());
			bean.setNameEN(obj.getTambonNameEn());
			bean.setNameTH(obj.getTambonNameTh());
			dropdownBeans.add(bean);
		}
		return dropdownBeans;
	}
	


}
