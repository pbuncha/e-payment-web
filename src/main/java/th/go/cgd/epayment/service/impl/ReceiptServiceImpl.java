package th.go.cgd.epayment.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.go.cgd.epayment.component.MessageResource;
import th.go.cgd.epayment.entity.Billing;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.InvoiceItem;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.BillingBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.InvoiceItemBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.report.service.ReportUtilService;
import th.go.cgd.epayment.repository.BillingRepository;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.service.interfaces.IGenerateCodeService;
import th.go.cgd.epayment.service.interfaces.IReceiptService;
import th.go.cgd.epayment.util.CalendarHelper;

@Service
@Transactional
public class ReceiptServiceImpl implements IReceiptService{
	private static final Logger logger = Logger.getLogger(ReceiptServiceImpl.class);

	@Autowired
	private MessageResource messageResource;
	@Autowired
	private BillingRepository billingRepository;
	@Autowired
	private IGenerateCodeService generateCodeService;
	@Autowired
	private UserRepository userRepository;

	public byte[] exportExcel( List<BillingBean> billingBean){
		logger.info("exportExcel");
		String sheetName = "sheet 1";
		Object[][] data = new Object[billingBean.size()+1][8];

		data[0][0] = messageResource.getMessage("receipt.header.createdDate");
		data[0][1] = messageResource.getMessage("receipt.header.billingNo");
		data[0][2] = messageResource.getMessage("receipt.header.invoiceNo");
		data[0][3] = messageResource.getMessage("receipt.header.invoice.ref1");
		data[0][4] = messageResource.getMessage("receipt.header.invoice.name");
		data[0][5] = messageResource.getMessage("receipt.header.invoice.citizenNo");
		data[0][6] = messageResource.getMessage("receipt.header.invoice.totalAmount");
		data[0][7] = messageResource.getMessage("receipt.header.status");

		for(int i = 0 ;i<billingBean.size();i++){
			data[i+1][0] = billingBean.get(i).getCreatedDate();
			data[i+1][1] = billingBean.get(i).getBillingNo();
			data[i+1][2] = billingBean.get(i).getInvoiceNo();
			data[i+1][3] = billingBean.get(i).getInvoice().getRef1();
			data[i+1][4] = billingBean.get(i).getInvoice().getName();
			data[i+1][5] = billingBean.get(i).getInvoice().getCitizenNo();
			data[i+1][6] = billingBean.get(i).getInvoice().getTotalAmount();
			data[i+1][7] = getStatusName(billingBean.get(i).getStatus());
		}

		byte[] output = ReportUtilService.genarateCustomExcelReport(data, sheetName);
		return output;

	}

	@Override
	public BillingBean getBillById(Integer billingId) {
		//sirikarn.s
		BillingBean bean = null;
		Billing billing = billingRepository.findOne(billingId);
		if(billing!=null){

			Invoice invoiceTb = billing.getInvoiceId();
			bean = new BillingBean();
			bean.setBillingId(billing.getBillingId());
			bean.setBillingNo(billing.getBillingNo());
			bean.setInvoiceNo(invoiceTb.getInvoiceNo());
			bean.setPrintingCopyDate(billing.getPrintingCopyDate());
			bean.setPrintingDate(billing.getPrintingDate());
			bean.setCreatedDate(billing.getCreatedDate());
			bean.setCreatedDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(billing.getCreatedDate())));
			bean.setTotalAmount(invoiceTb.getTotalAmount());
			bean.setInvoiceId(invoiceTb.getInvoiceId());
			bean.setStatus(String.valueOf(billing.getStatus()));
			bean.setStatusName(getStatusName(String.valueOf(billing.getStatus())));
			if(billing.getPrintingDate()!=null){
				bean.setPrintingDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(billing.getPrintingDate())));
			}
			if(billing.getPrintingCopyDate()!=null){
				bean.setPrintingCopyDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(billing.getPrintingCopyDate())));
			}

			InvoiceBean tmp = new InvoiceBean();
			tmp.setId(invoiceTb.getInvoiceId());
			tmp.setInvoiceNo(invoiceTb.getInvoiceNo());
			tmp.setRef1(invoiceTb.getRef1());

			
			String name = invoiceTb.getFirstName();
			if (!invoiceTb.getLastName().isEmpty()) {
				name += " " + invoiceTb.getLastName();
			}
			
			if(invoiceTb.getPersonTypeId()==1){
				//1 is personName
				if (!(invoiceTb.getTitleId() == null)) {
					name = invoiceTb.getTitleId().getTitleName() + name;
				}
				tmp.setName(name);
			}else{
				//else is compName
				tmp.setCompName(name);
			}

			tmp.setCitizenNo(invoiceTb.getCitizenNo());
			tmp.setTotalAmount(invoiceTb.getTotalAmount());
			tmp.setStartDate(invoiceTb.getStartDate());
			tmp.setEndDate(invoiceTb.getEndDate());
			tmp.setStartDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(invoiceTb.getStartDate())));
			tmp.setEndDateStr(CalendarHelper.formatDateTH(CalendarHelper.getDateTime(invoiceTb.getEndDate())));
			tmp.setStatus(String.valueOf(invoiceTb.getStatus()));

			List<InvoiceItem> invoiceItemList = invoiceTb.getInvoiceItemList();
			List<InvoiceItemBean> invoiceItemBeanList = new ArrayList<InvoiceItemBean>();
			
			//sort list by invoiceItemId
			//invoiceItemList.sort(Comparator.comparing(InvoiceItem::getInvoiceItemId));

			for (InvoiceItem item : invoiceItemList) {
				InvoiceItemBean itemBean = new InvoiceItemBean();
				itemBean.setCatalogName(item.getCatalogId().getCatalogName());
				itemBean.setAmount(BigDecimal.valueOf(item.getCatalogId().getAmount()));
				invoiceItemBeanList.add(itemBean);
			}

			tmp.setItems(invoiceItemBeanList);
			bean.setInvoice(tmp);
		}

		return bean;
	}

	private String getStatusName(String status){
		switch ( status )
		{
		case "0"
		:     return messageResource.getMessage("receipt.status.wait");
		case "1"
		:     return messageResource.getMessage("receipt.status.original");
		case "2"
		:     return messageResource.getMessage("receipt.status.copy");
		case "3"
		:     return messageResource.getMessage("receipt.status.cancel");
		default
		: return "สถานะไม่ถูกต้อง";

		}
	}

	public SearchBean getDefaultData(){

		User userAu = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ((principal != null) && (principal instanceof User)) {
			userAu = (User) principal;
		} 		
		User user = userRepository.findOne(userAu.getUserId());
		SearchBean bean = new SearchBean();
		if(user.getUserOrganizationList2()!=null){
			if(user.getUserOrganizationList2().get(0).getDepartmentId()!=null){
				bean.setDepartmentId(user.getUserOrganizationList2().get(0).getDepartmentId().getDepartmentId());
			}
		}

		return bean;
	}
	
	public String getGenCodeBilling(String printType,Integer billingId){
		
		logger.info("getGenCodeBilling for Type : "+printType);
		generateCodeService.generateBillCode(printType, billingId);
		
		Billing bill = billingRepository.findOne(billingId);
		
		String genCode = bill.getGenerateNo();
		
		if(printType.equals("P")){ //update status only when print
			updateStatusPrint(billingId);
		}
		return genCode;
	}
	
	private void updateStatusPrint(Integer billingId){
		

		User userAu = null;
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if ((principal != null) && (principal instanceof User)) {
			userAu = (User) principal;
		} 		
		User user = userRepository.findOne(userAu.getUserId());
		
		Billing bill = billingRepository.findOne(billingId);
		if(bill.getStatus()=='0'){
			bill.setStatus('1');
			bill.setPrintingDate(new Date());
			bill.setPrintedBy(user.getUserCenterId().getFirstNameTh()+" "+user.getUserCenterId().getLastNameTh());
			logger.info("printed by : "+bill.getPrintedBy());
			logger.info("printed Date : "+bill.getPrintingDate());
		}
		else if(bill.getStatus()=='1' || bill.getStatus()=='2'){
			bill.setStatus('2');
			bill.setPrintingCopyDate(new Date());
			bill.setPrintedCopyBy(user.getUserCenterId().getFirstNameTh()+" "+user.getUserCenterId().getLastNameTh());
			logger.info("printed Copy by : "+bill.getPrintedCopyBy());
			logger.info("printed Copy Date : "+bill.getPrintingCopyDate());
		}

		billingRepository.save(bill);
	}
}
