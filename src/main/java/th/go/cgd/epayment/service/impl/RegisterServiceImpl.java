package th.go.cgd.epayment.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.MUserStatus;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.repository.MUserStatusRepository;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.service.interfaces.IRegisterService;

@Service
public class RegisterServiceImpl implements IRegisterService {
	private static final Logger logger = Logger.getLogger(ExampleServiceImpl.class);
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private SessionFactory sessionFactory;
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private MUserStatusRepository mUserStatusRepository;
	    
	@Override
	public Boolean save(UserBean userBean) {
		// TODO Auto-generated method stub
		if(userBean == null) return false;
		
		User user = new User();
		user.setUserName(userBean.getUsername());
		user.setPassword(userBean.getPassword());
	
		//MUserStatus userStatusTb = 	mUserStatusRepository.getOne("A");
		user.setStatus('A');
		user.setUserId(4);
		
		
		
		
		return null;
	}

}
