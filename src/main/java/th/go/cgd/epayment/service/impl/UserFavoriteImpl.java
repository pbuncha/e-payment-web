package th.go.cgd.epayment.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.Catalog;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.InvoiceItem;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserFavorite;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.InvoiceItemBean;
import th.go.cgd.epayment.repository.CatalogRepository;
import th.go.cgd.epayment.repository.IUserFavoriteRepository;
import th.go.cgd.epayment.repository.MDepartmentRepository;
import th.go.cgd.epayment.service.interfaces.IUserFavoriteService;

@Service
@Transactional
public class UserFavoriteImpl implements IUserFavoriteService {

	@Autowired
	private IUserFavoriteRepository iUserFavoriteRepository;
	@Autowired
	private CatalogRepository ctalogRepository;
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private MDepartmentRepository mDepartmentRepository;

	@Override
	public boolean save(List<Integer> listId) {
		List<UserFavorite> favorites = new ArrayList<UserFavorite>();
		boolean result = false;
		User user = new User();
		user.setUserId(1);
		iUserFavoriteRepository.deleteAllFavorite(user.getUserId());
		for (Integer obj : listId) {
			Catalog ctalogTb = ctalogRepository.findOne(obj.intValue());

			UserFavorite favorite = new UserFavorite();
			favorite.setCatalogId(ctalogTb);
			favorite.setCreatedBy(user);
			favorite.setCreatedDate(new Date());
			favorite.setCreatedBy(user);
			favorite.setUserId(user);
			favorites.add(favorite);
		}
		iUserFavoriteRepository.save(favorites);
		result = true;
		return result;
	}

	@Override
	public List<Integer> getFavoriteByUser(Integer userId) {
		List<Integer> mapList = new ArrayList<Integer>();

		List<UserFavorite> favorites = iUserFavoriteRepository.getUserFavoriteByUser(userId);
		Map<String, Integer> map = new HashMap<>();
		for (UserFavorite favorite : favorites) {
			mapList.add(favorite.getCatalogId().getCatalogId());
		}
		return mapList;
	}

	@Override
	public List<InvoiceItemBean> getFavoriteList(List<Integer> listId) {
		StringBuffer queryStr = new StringBuffer(
				"SELECT c.CATALOG_NAME, c.CATALOG_ID, c.CATALOG_CODE, csi.DEPARTMENT_ID, cct.CONDITION_TEXT, c.Amount");
		queryStr.append(" FROM PAYDB.CATALOG C ");
		queryStr.append(" LEFT JOIN PAYDB.CATALOG_STRUCTURE cs ON cs.CATALOG_ID = C.CATALOG_ID ");
		queryStr.append(
				" LEFT JOIN PAYDB.CATALOG_STRUCTURE_ITEM csi ON csi.CATALOG_STRUCTURE_ID = cs.CATALOG_STRUCTURE_ID  ");
		queryStr.append(" LEFT JOIN PAYDB.CATALOG_COST_CENTER cc ON cc.CATALOG_ID = c.CATALOG_ID ");
		queryStr.append(" LEFT JOIN PAYDB.CATALOG_CONDITION_TEXT CCT ON CCT.CATALOG_ID = C.CATALOG_ID ");
		queryStr.append(" WHERE (1=1) ");
		// WHERE criteria

		queryStr.append(" AND cs.CATALOG_ID IN (" + listId.toString().replace("[", "").replace("]", "") + ") ");
		Query query = this.entityManager.createNativeQuery(queryStr.toString());
		try {
			List<InvoiceItemBean> invoiceItemBeans = new ArrayList<InvoiceItemBean>();
			List<Object[]> objects = query.getResultList();
			for (Object[] obj : objects) {
				InvoiceItemBean bean = new InvoiceItemBean();
				int catalogId = Integer.parseInt(obj[1].toString());
				bean.setCatalogName(obj[0].toString());
				bean.setCatalogId(catalogId);
				bean.setCatalogCode(obj[2].toString());
				if (obj[3] != null) {
					MDepartment department = mDepartmentRepository.getOne(Integer.parseInt(obj[3].toString()));
					bean.setDepartmentId(Integer.parseInt(obj[3].toString()));
					bean.setDepartmentName(department.getDepartmentName());
				}
				bean.setConditionText(obj[4] != null ? obj[4].toString() : null);
				bean.setAmount(new BigDecimal(obj[5].toString()));
				bean.setFavorite(false);
				invoiceItemBeans.add(bean);

			}
			return invoiceItemBeans;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
