package th.go.cgd.epayment.service.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.entity.MPermissionGroup;
import th.go.cgd.epayment.entity.MUserPermissiongroup;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.entity.UserOrganization;
import th.go.cgd.epayment.model.MPermissionGroupBean;
import th.go.cgd.epayment.model.MUserPermissiongroupBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.model.UserPermissionGroupBean;
import th.go.cgd.epayment.repository.MPermissionGroupRepository;
import th.go.cgd.epayment.repository.MUserPermissionGroupRepository;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.service.interfaces.IUserInfoService;

@Transactional
@Service
public class UserInfoServiceImpl implements IUserInfoService {
	private static final Logger logger = Logger.getLogger(ManageCatalogServiceImpl.class);
	@PersistenceContext
	private EntityManager entityManager;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private MUserPermissionGroupRepository mUserPermissionGroupRepository;
	@Autowired
	private MPermissionGroupRepository mPermissionGroupRepository;

	public List<UserBean> search(SearchBean userBean){
		logger.info("search");
		SimpleDateFormat dt1 = new SimpleDateFormat("yyyyMMdd");
		List<UserBean> userBeanList = new ArrayList<UserBean>();


		StringBuffer strQueryWhere = new StringBuffer(" ");
		StringBuffer queryStr = new StringBuffer("select distinct up ");

		strQueryWhere.append(" from MUserPermissiongroup up ");
		strQueryWhere.append(" LEFT JOIN up.userId u ");
		strQueryWhere.append(" LEFT JOIN u.userCenterId uc ");
		strQueryWhere.append(" LEFT JOIN u.userOrganizationList2 uo ");
		strQueryWhere.append(" LEFT JOIN uo.departmentId d ");
		strQueryWhere.append(" LEFT JOIN d.mCostCenterList c ");
		strQueryWhere.append(" LEFT JOIN c.businessAreaId b ");

		//Pop revise 04/Feb/2018
		//strQueryWhere.append(" WHERE (1=1) AND u.status <> '"+EPaymentConstant.NO.charAt(0)+"' ");
		//strQueryWhere.append(" AND up.requestType is not null ");
		strQueryWhere.append(" WHERE (1=1) AND u.status = 1 ");
		strQueryWhere.append(" AND up.requestType is not null ");
		//End revise

		if (userBean.getUserType() != null)
			strQueryWhere.append(" AND u.userTypeId= :userTypeId");
		if (userBean.getStatusUse() != null)
			strQueryWhere.append(" AND up.status= :statusUse");
		if (userBean.getStatusApprove() != null)
			strQueryWhere.append(" AND up.status= :statusApprove");
		if (userBean.getCitizenid() != null)
			strQueryWhere.append(" AND uc.citizenNo= :citizenNo");
		if (userBean.getName()!= null)
			strQueryWhere.append(" AND uc.firstNameTh LIKE :name OR cs.lastNameTh LIKE :name");
		if (userBean.getDepartmentId() != null)
			strQueryWhere.append(" AND d.departmentId= :departmentId");
		if (userBean.getCostCenterId()!= null)
			strQueryWhere.append(" AND c.costCenterId= :costCenterId");
		if (userBean.getBusinessAreaId()!= null)
			strQueryWhere.append(" AND b.businessAreaId= :businessAreaId");


		TypedQuery<MUserPermissiongroup> query = entityManager.createQuery(queryStr.toString()+strQueryWhere.toString(), MUserPermissiongroup.class);

		if (userBean.getUserType() != null)
			query.setParameter("userTypeId", Integer.valueOf(userBean.getUserType()));
		if (userBean.getStatusUse()!= null)
			query.setParameter("statusUse", userBean.getStatusUse());
		if (userBean.getStatusApprove() != null)
			query.setParameter("statusApprove", userBean.getStatusApprove());
		if (userBean.getCitizenid() != null)
			query.setParameter("citizenNo", userBean.getCitizenid());
		if (userBean.getName() != null)
			query.setParameter("name", userBean.getName());
		if (userBean.getDepartmentId() != null)
			query.setParameter("departmentId", userBean.getDepartmentId());
		if (userBean.getCostCenterId() != null)
			query.setParameter("costCenterId", userBean.getCostCenterId());
		if (userBean.getBusinessAreaId() != null)
			query.setParameter("businessAreaId", userBean.getBusinessAreaId());

		List<MUserPermissiongroup> userPerList = query.getResultList();
		logger.info("userPerList.size() : "+userPerList.size());
		for(MUserPermissiongroup userPerTb : userPerList){
			UserBean userBeanTemp = new UserBean();
			userBeanTemp.setUserId(userPerTb.getUserId().getUserId());
			userBeanTemp.setUserPermissionGroupId(userPerTb.getMUserPermissiongroupId());
			userBeanTemp.setCitizenid(userPerTb.getUserId().getUserCenterId().getCitizenNo());
			userBeanTemp.setUserType(userPerTb.getUserId().getUserTypeId());
			userBeanTemp.setStatus(userPerTb.getStatus());

			String name = "";
			if(userPerTb.getUserId().getUserCenterId().getTitleId()!=null){
				name += userPerTb.getUserId().getUserCenterId().getTitleId().getTitleName();
			}
			if(userPerTb.getUserId().getUserCenterId().getFirstNameTh()!=null){
				name += userPerTb.getUserId().getUserCenterId().getFirstNameTh();
			}
			if(userPerTb.getUserId().getUserCenterId().getLastNameTh()!=null){
				name += " "+userPerTb.getUserId().getUserCenterId().getLastNameTh();
			}
			if(userPerTb.getUserId().getUserOrganizationList2().size()>0){
				if(userPerTb.getUserId().getUserOrganizationList2().get(0).getDepartmentId()!=null){
					userBeanTemp.setDepartmentName(userPerTb.getUserId().getUserOrganizationList2().get(0).getDepartmentId().getDepartmentName());
				}

			}
			
			userBeanTemp.setRequestType(userPerTb.getRequestType());
			userBeanTemp.setAttachFile(userPerTb.getAttachFile());
			userBeanTemp.setName(name);
			userBeanList.add(userBeanTemp);
		}
		logger.info("userBeanList.size() : "+userBeanList.size());
		return userBeanList;
	}
	
	public void updateStatusUserPermission(List<UserBean> userBeanList,Integer userId){
		logger.info("updateStatusUserPermission");

		User userLoginTb = new User();
		userLoginTb = userRepository.findOne(userId);

		for(int i=0;i<userBeanList.size();i++){
			logger.info("updateStatusUserPermission ID : "+userBeanList.get(i).getUserPermissionGroupId());
			MUserPermissiongroup userPerTb = mUserPermissionGroupRepository.findOne(userBeanList.get(i).getUserPermissionGroupId());

			if(userBeanList.get(i).getRequestType().equals("1")){
				//request to open
				if(userBeanList.get(i).getStatus().equals("1")){
					//approve
					userPerTb.setStatus("1");
				}else{
					//reject
					//userPerTb.setStatus("0");
				}
				
				
			}else if(userBeanList.get(i).getRequestType().equals("0")){
				//request to close
				if(userBeanList.get(i).getStatus().equals("1")){
					//approve
					userPerTb.setStatus("2");
				}else{
					//reject
					//userPerTb.setStatus("1");
				}
				
			}
			
			userPerTb.setRequestType(null);
			userPerTb.setComment(userBeanList.get(i).getApproveComment());
			userPerTb.setUpdateBy(userLoginTb.getUserId());
			userPerTb.setUpdateDate(new Date());
			mUserPermissionGroupRepository.save(userPerTb);
		}		
	}

	public void update(List<UserBean> userBeanList,Integer userId){
		logger.info("updateStatusUser");

		User userLoginTb = new User();
		userLoginTb = userRepository.findOne(userId);

		for(int i=0;i<userBeanList.size();i++){
			User userTb = userRepository.findOne(userBeanList.get(i).getUserId());
			
			//Pop revise 4 Feb 2018
			userTb.setIsApproveByAdmin(userBeanList.get(i).getStatus().charAt(0));
	
			userTb.setStatus(userBeanList.get(i).getStatus().charAt(0));
			userTb.setApproveComment(userBeanList.get(i).getApproveComment());
			userTb.setUpdatedBy(userLoginTb);
			userTb.setUpdatedDate(new Date());
			userRepository.save(userTb);
		}		
	}

	public List<UserPermissionGroupBean> search(Integer userId){
		User user = userRepository.findOne(userId);
		List<UserPermissionGroupBean> groupList = new ArrayList<UserPermissionGroupBean>();

		for(MUserPermissiongroup userPermissiongroup :user.getMUserPermissiongroupList()){
			UserPermissionGroupBean groupBean = new UserPermissionGroupBean();
			groupBean.setPermisName(userPermissiongroup.getMPermissionGroupId().getMPermissionGroupName());
			groupBean.setSendDate(userPermissiongroup.getCreateDate());
			groupBean.setSendStatus("");			
			groupBean.setAppvStatus(userPermissiongroup.getRequestType());
			groupBean.setStartDate(user.getStartDate());			
			groupBean.setEndDate(user.getEndDate());
			groupBean.setStatus(userPermissiongroup.getStatus());
			groupBean.setAttachFile(userPermissiongroup.getAttachFile());
			groupBean.setPermisId(userPermissiongroup.getMUserPermissiongroupId());
			//			groupBean.setAttachFile(attachFile);
			groupList.add(groupBean);
		}
		return groupList;
	}

	public void update(UserBean bean,Integer userId){
		logger.info("update user open/close status");

		User userLoginTb = new User();
		userLoginTb = userRepository.findOne(userId);
		User userTb = userRepository.findOne(bean.getUserId());
		userTb.setStatus(bean.getStatus().charAt(0));

		//Pop revise 4 Feb 2018
		userTb.setIsApproveByAdmin(bean.getStatus().charAt(0));
		
		userTb.setStartDate(bean.getStartDate());
		userTb.setEndDate(bean.getEndDate());
		userTb.setApproveComment(bean.getApproveComment());
		userTb.setUpdatedBy(userLoginTb);
		userTb.setUpdatedDate(new Date());
		List<MUserPermissiongroup> groups = new ArrayList<MUserPermissiongroup>();
		for(UserPermissionGroupBean bean2 :bean.getUserPermissionGroup()){
			MUserPermissiongroup group = new MUserPermissiongroup();
			group = mUserPermissionGroupRepository.findOne(bean2.getPermisId());
			group.setStatus(bean2.getStatus());
			groups.add(group);
			
		}
		userTb.setMUserPermissiongroupList(groups);
		userRepository.save(userTb);
	}

	
	public List<MPermissionGroupBean> getMPermissionGroup(Integer userId) {
//		System.out.println("userId:" + userId);			
		
		List<MPermissionGroupBean> objBeanList = new ArrayList<MPermissionGroupBean>();

		for (MPermissionGroup objDB : mPermissionGroupRepository.findAll()) {

			MPermissionGroupBean objBean = new MPermissionGroupBean();

			objBean.setmPermissionGroupId(objDB.getMPermissionGroupId());
			objBean.setmPermissionGroupCode(objDB.getMPermissionGroupCode());
			objBean.setmPermissionGroupName(objDB.getMPermissionGroupName());
			objBean.setDescription(objDB.getDescription());			
			objBean.setCreateBy(objDB.getCreateBy() == null ? null : objDB.getCreateBy().getUserId());
			objBean.setCreateDate(objDB.getCreateDate());
			objBean.setUpdateBy(objDB.getUpdateBy() == null ? null : objDB.getUpdateBy().getUserId());
			objBean.setUpdateDate(objBean.getUpdateDate());

			objBeanList.add(objBean);
		}

		List<MUserPermissiongroupBean> userPermissionGroup = getAllMUserPermissiongroupByUserId(userId);
		for (MPermissionGroupBean x : objBeanList){
						
			MUserPermissiongroupBean objFound = userPermissionGroup.stream().filter(a -> a.getmPermissionGroupId() == x.getmPermissionGroupId()).findFirst().orElse(null);
			if (objFound != null)
			{
				x.setStatus(objFound.getStatus());
				
				if (!StringUtils.isEmpty(objFound.getRequestType()))
				{
					x.setStatus("0");
				}				
			}
		}
				
		return objBeanList;

	}

	public UserBean getUserById(Integer userId) 
	{
		User user = userRepository.findOne(userId);
		if (user != null)
		{
			UserBean userBean = new UserBean();
			
			userBean.setCitizenid(user.getUserCenterId().getCitizenNo());
			
			userBean.setFnameTH(user.getUserCenterId().getFirstNameTh());
			userBean.setLnameTH(user.getUserCenterId().getLastNameTh());
					
			UserOrganization org = user.getUserOrganizationList2().stream().findFirst().orElse(null);
			if (org != null)
			{
				userBean.setDepartmentName(org.getDepartmentId().getDepartmentName());				
				userBean.setBusinessAreaName(org.getBusinessAreaId().getBusinessAreaName());
				userBean.setCostCenterName(org.getCostCenterId().getCostCenterName());
			}
			
			return userBean;
		}
		return null;
	}

	@Override
	public void createUserPermissiongroup(MUserPermissiongroupBean objBean) {
		
		MUserPermissiongroup objDB = new MUserPermissiongroup();

		objDB.setUserId(objBean.getUserId() == null? null: new User(objBean.getUserId()));
		objDB.setMPermissionGroupId(objBean.getmPermissionGroupId() == null? null : new MPermissionGroup(objBean.getmPermissionGroupId()));
		objDB.setAttachFile(objBean.getAttachFile());
		objDB.setRequestType(objBean.getRequestType());
		//objDB.setStatus(objBean.getStatus());
		objDB.setCreateBy(objBean.getUserId());
		objDB.setCreateDate(new Date());
		objDB.setUpdateBy(objBean.getUserId());
		objDB.setUpdateDate(new Date());
						
		mUserPermissionGroupRepository.save(objDB);
	}
	
	@Override
	public List<MUserPermissiongroupBean> getAllMUserPermissiongroupByUserId(Integer userId) {
		List<MUserPermissiongroupBean> groupList = new ArrayList<MUserPermissiongroupBean>();

		for(MUserPermissiongroup db : mUserPermissionGroupRepository.findByUserIdAsc(userId)){
						
			MUserPermissiongroupBean b = new MUserPermissiongroupBean();
			
			b.setUserId(db.getUserId().getUserId());
			b.setmPermissionGroupId(db.getMPermissionGroupId().getMPermissionGroupId());
			b.setRequestType(db.getRequestType());
			b.setStatus(db.getStatus());
			b.setAttachFile(db.getAttachFile());
			b.setCreateBy(db.getCreateBy());
			b.setCreateDate(db.getCreateDate());
			db.setUpdateBy(db.getUpdateBy());
			db.setUpdateDate(db.getUpdateDate());

			groupList.add(b);
		}
		return groupList;		
	}
	
	
	@Override
	public void updateUserPermissiongroup(MUserPermissiongroupBean objBean)
	{				
		MUserPermissiongroup q = mUserPermissionGroupRepository.findAll().stream().filter(a -> a.getUserId().getUserId() == objBean.getUserId() && a.getMPermissionGroupId().getMPermissionGroupId() == objBean.getmPermissionGroupId()).findFirst().orElse(null);
		if (q != null)
		{
			MUserPermissiongroup objDB = mUserPermissionGroupRepository.findOne(q.getMUserPermissiongroupId());
			if (objDB != null)
			{
				objDB.setAttachFile(objBean.getAttachFile());
				objDB.setRequestType(objBean.getRequestType());
				//objDB.setStatus(objBean.getStatus());
				objDB.setUpdateBy(objBean.getUserId());
				objDB.setUpdateDate(new Date());
								
				mUserPermissionGroupRepository.save(objDB);				
			}			
		}
		else 
		{
			createUserPermissiongroup(objBean);
		}
	}

	public byte[] getFile(String path){
		Path pathFile = Paths.get(path);
		byte[] data = null;
		try {
			data = Files.readAllBytes(pathFile);
			logger.info("open file : "+pathFile +" success ; ");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("open file : "+pathFile +" error ; "+e.getMessage());
			e.printStackTrace();
		}
		return data;
	}
}
