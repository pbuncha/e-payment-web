package th.go.cgd.epayment.service.impl;

import javax.transaction.Transactional;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.Address;
import th.go.cgd.epayment.entity.MAmphur;
import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MTambon;
import th.go.cgd.epayment.entity.UserOrganization;
import th.go.cgd.epayment.model.AddressBean;
import th.go.cgd.epayment.repository.UserOrganizationRepository;
import th.go.cgd.epayment.service.interfaces.IUserOrganizationService;

/**
 * @author torgan.p 26 ธ.ค. 2560 15:23:07
 *
 */
@Service
@Transactional
public class UserOrganizationServiceImpl implements IUserOrganizationService{
    private static final Logger logger = Logger.getLogger(UserOrganizationServiceImpl.class);
    
    @Autowired
    private UserOrganizationRepository userOrganizationRepository;
    
    	@Override
	public UserOrganization saveUserOrganization(UserOrganization userOrganization) {
	   return  userOrganizationRepository.save(userOrganization);

	}
}
