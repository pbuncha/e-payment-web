package th.go.cgd.epayment.service.impl;

import javax.transaction.Transactional;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.MUserPermissiongroup;
import th.go.cgd.epayment.entity.UserOrganization;
import th.go.cgd.epayment.repository.MUserPermissionGroupRepository;
import th.go.cgd.epayment.repository.UserOrganizationRepository;
import th.go.cgd.epayment.service.interfaces.IUserPermissiongroupService;

/**
 * @author torgan.p 26 ธ.ค. 2560 16:22:42
 *
 */
@Service
@Transactional
public class UserPermissiongroupServiceImpl implements IUserPermissiongroupService {

    
    private static final Logger logger = Logger.getLogger(UserPermissiongroupServiceImpl.class);
    
    @Autowired
    private MUserPermissionGroupRepository userPermissionGroupRepository;
    
    	@Override
	public MUserPermissiongroup saveUserPermissiongroup(MUserPermissiongroup userPermissiongroupTb) {
	    MUserPermissiongroup userPermissiongroupTemp = userPermissionGroupRepository.save(userPermissiongroupTb);
	 return userPermissiongroupTemp;
	}
}
