package th.go.cgd.epayment.service.interfaces;

import java.math.BigDecimal;

import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.model.ImageBean;

/**
 * @author torgan.p 18 พ.ย. 2560 13:57:15
 *
 */
public interface IBarcodeService {
    public void generateBarCode(String barcode);
    public ImageBean generateBarcodeTobyte (String barcode);
    public ImageBean getDataQRCodeTobyte (String barcode);
    public void generateQRCode(String barcode);
    public String getDataQRCode(Invoice invoice);
    public String getDataBarCode(Invoice invoice);
    public String getDataQRCode(String ref1,String ref2,BigDecimal totalAmount) ;
    public String getDataQRCode(String ref1,String ref2,String totalAmount);
}
