package th.go.cgd.epayment.service.interfaces;

/**
 * @author torgan.p 14 ต.ค. 2560 18:00:05
 *
 */
public interface IBaseUrlService {
    public String getBaseUrl() ;

}
