package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.BatchFileReadResultBean;
import th.go.cgd.epayment.model.master.BusinessAreaBean;

public interface IBatchFileReadResultService {

	public List<BatchFileReadResultBean> getAllBatchFileReadResult();

	public List<BatchFileReadResultBean> getBatchFileReadResultByCriteria(BatchFileReadResultBean obj);
	
}

