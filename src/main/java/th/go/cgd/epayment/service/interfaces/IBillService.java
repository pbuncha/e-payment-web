package th.go.cgd.epayment.service.interfaces;

import java.util.List;
import java.util.Map;

import th.go.cgd.epayment.entity.Billing;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.BillingBean;
import th.go.cgd.epayment.model.SaveInvoiceBean;
import th.go.cgd.epayment.model.SearchBean;

/**
 * @author torgan.p 28 พ.ย. 2560 00:08:35
 *
 */
public interface IBillService{
    public boolean saveBill(Map<String, Integer> obj);
    public Integer saveFromPayment(SaveInvoiceBean saveInvoiceBean,User  user);
    public List<BillingBean> getBillByUser(SearchBean searchBean) ;
    public Billing getbillingById(Integer id);
    public int updateSeqBilling(Billing billing);

}
