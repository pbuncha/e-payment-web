package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.InvoiceItemBean;

public interface ICatalogService {
	public List<InvoiceItemBean> getCatalogList(InvoiceItemBean itemBean,User  user);
}
