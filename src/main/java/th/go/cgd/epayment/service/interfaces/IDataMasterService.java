package th.go.cgd.epayment.service.interfaces;

import java.util.Collection;
import java.util.List;

import th.go.cgd.epayment.model.DropdownBean;

public interface IDataMasterService {
	public List<DropdownBean> getOrganizationType();
	public List<DropdownBean> getOrganizationSubType(Integer orgTypeId);
	public List<DropdownBean> getDepartment(Integer orgSubTypeId);
	public List<DropdownBean> getBusinessArea();
	public List<DropdownBean> getBusinessAreaByDepartment(Integer departmentId);
	public List<DropdownBean> getCostCenter(Integer deptId, Integer bizAreaId);
	public List<DropdownBean> getProvince();
	public List<DropdownBean> getAmphur(Integer provinceId);
	public List<DropdownBean> getTambon(Integer amphurId);
}
