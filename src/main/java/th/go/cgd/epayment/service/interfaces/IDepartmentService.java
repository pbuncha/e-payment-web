package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.DropdownBean;

/**
 * @author torgan.p 18 พ.ย. 2560 22:53:19
 *
 */
public interface IDepartmentService {
    public List<DropdownBean> getDepartmentAll();
    public List<DropdownBean> getMinistryAll();
}
