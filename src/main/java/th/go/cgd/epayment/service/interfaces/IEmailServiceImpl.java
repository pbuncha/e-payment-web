package th.go.cgd.epayment.service.interfaces;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import javax.mail.MessagingException;

import th.go.cgd.epayment.model.EmailBean;

/**
 * @author torgan.p 14 ธ.ค. 2560 20:43:26
 *
 */
public interface IEmailServiceImpl {

    public boolean sendMail(EmailBean emailBean) throws UnsupportedEncodingException, MessagingException ;
//    public String  getTemplateMail(String fullName,String username,String password);
    public String  getTemplateMail(Object[] metaData,String messageKey);
}
