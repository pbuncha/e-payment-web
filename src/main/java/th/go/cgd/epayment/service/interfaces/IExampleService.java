package th.go.cgd.epayment.service.interfaces;

import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.ExampleBean;

public interface IExampleService {
	
	public ExampleBean getData(Integer id);
	public Boolean save(ExampleBean exampleBean);
	public Boolean saveAll( Collection<ExampleBean> exampleBean);
	public List<ExampleBean> getAll();
	public Boolean delete(Integer id);
	public ExampleBean update( ExampleBean exampleBean);
	public List<DropdownBean> findExampleByIdNativeQuery(Long exampleId);
	public List<ExampleBean> findAllExample();
	public Page<ExampleBean> findAll();

}
