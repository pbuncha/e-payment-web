package th.go.cgd.epayment.service.interfaces;

import java.util.Date;

import th.go.cgd.epayment.model.InvoiceCodeBean;

public interface IGenerateCodeService {


	public String getGenerateCode(String generateKeyTable);

	InvoiceCodeBean generateInvoiceCode(Date invoiceDate);

	public String generateCatalogCode();
	public  String generateBillCode(String typePrint,Integer billId);

	public  String generateInvoiceLoaderCode(Date newDate);
}