package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import org.springframework.data.domain.Page;

import th.go.cgd.epayment.model.InvoiceBillingBean;

public interface IInvoiceBillingService {
	public List<InvoiceBillingBean> getInvoiceBillingList(InvoiceBillingBean itemBean);
	public Page<InvoiceBillingBean> search();
}
