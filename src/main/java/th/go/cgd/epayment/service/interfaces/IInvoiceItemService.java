package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.model.InvoiceItemBean;

public interface IInvoiceItemService {
    public List<InvoiceItemBean> getInvoiceList(User  user);
}
