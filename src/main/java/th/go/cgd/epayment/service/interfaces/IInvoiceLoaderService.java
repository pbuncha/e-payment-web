package th.go.cgd.epayment.service.interfaces;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import th.go.cgd.epayment.model.InvoiceLoaderBean;

public interface IInvoiceLoaderService {

	public void importInvoice(MultipartFile file) throws Exception;
	public List<InvoiceLoaderBean> getInvoiceLoaderByCriteria(InvoiceLoaderBean invoiceLoaderBean)throws ParseException;
	public void confirm(String code);
	public void cancel(String code);
	public void delete(String code);
}
