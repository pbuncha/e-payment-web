/**
 * 
 */
package th.go.cgd.epayment.service.interfaces;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import net.sf.jasperreports.engine.JRException;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.model.BillingBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.SaveInvoiceBean;

/**
 * @author wichuda.k Nov 12, 2017
 *
 */
public interface IInvoiceService {
	public InvoiceBean save(InvoiceBean invoiceBean);
	public InvoiceBean getInvoidByUser(Integer userId);
	public Invoice getInvoidByNo(String invoidNo);
	public Invoice getInvoidById(Integer invoidId);
	public List<InvoiceBean> getInvoiceByCriteria(InvoiceBean invoiceBean);
	public List<DropdownBean> getCatalogList(Integer catalogTypeId);
	public List<DropdownBean> getinvoiceItem();
	public void cancelInvoice(Integer id);
	public byte[] exportExcel( List<InvoiceBean> invoiceBeans);
	public InvoiceBean getInvoice(Integer id);
	public List<DropdownBean> getCatalogList();
	public InvoiceBean getInvoiceBy(String citizenNo);
	public void cancelInvoice(List<Integer> id);
	public byte[] generateInvoiceRpt(Invoice invoiceTb) throws JRException;
}
