package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.master.AccountDepositBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;

public interface IMAccountDepositService {

	public List<AccountDepositBean> getAllMAccountDeposits();
	public void createMAccountDeposit(AccountDepositBean obj);
	public void updateMAccountDeposit(AccountDepositBean obj);
	public void deleteMAccountDeposit(Integer id);
	public AccountDepositBean getMAccountDepositById(Integer id);

	public List<AccountDepositBean> getMAccountDepositByCriteria(AccountDepositBean obj);
	public void updateStatusCancel(Integer id);

}
