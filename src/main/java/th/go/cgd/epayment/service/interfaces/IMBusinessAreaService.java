package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.MBusinessArea;
import th.go.cgd.epayment.model.master.BusinessAreaBean;

public interface IMBusinessAreaService {

	public List<BusinessAreaBean> getAllMBusinessAreas();
	public void createMBusinessArea(BusinessAreaBean obj);
	public void updateMBusinessArea(BusinessAreaBean obj);
	public void deleteMBusinessArea(Integer id);
	public BusinessAreaBean getMBusinessAreaById(Integer id);

	public List<BusinessAreaBean> getMBusinessAreaByCriteria(BusinessAreaBean obj);
	public void updateStatusCancel(Integer id);
	public MBusinessArea findBusinessAreaById(Integer id);

}
