package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.MCostCenter;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.master.CostCenterBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;

public interface IMCostCenterService {

	public List<CostCenterBean> getAllMCostCenters();
	public void createMCostCenter(CostCenterBean obj);
	public void updateMCostCenter(CostCenterBean obj);
	public void deleteMCostCenter(Integer id);
	public CostCenterBean getMCostCenterById(Integer id);

	public List<CostCenterBean> getMCostCenterByCriteria(CostCenterBean obj);
	public void updateStatusCancel(Integer id);
	public MCostCenter findMCostCenterById(Integer id);
	
	public List<DropdownBean> getCostCenterByDisbursementUnitId(Integer disbursementUnitId);

}
