package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.MDepartment;
import th.go.cgd.epayment.model.AddressBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.master.DepartmentBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;

public interface IMDepartmentService {

	public List<DepartmentBean> getAllMDepartments();
	public void createMDepartment(DepartmentBean obj);
	public void updateMDepartment(DepartmentBean obj);
	public void deleteMDepartment(Integer id);
	public DepartmentBean getMDepartmentById(Integer id);

	public List<DepartmentBean> getMDepartmentByCriteria(DepartmentBean obj);
	public void updateStatusCancel(Integer id);
	public AddressBean getAdressBydept(Integer deptId);
	public MDepartment findDepartmentById(Integer id);

    public List<DropdownBean> getMinistryAll();
    public List<DropdownBean> getDepartmentAll();
    public List<DropdownBean> getDepartmentByMinistryId(Integer id);

}
