package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.master.DisbursementUnitBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;

public interface IMDisbursementUnitService {

	public List<DropdownBean> getDisbursementUnit();
	public List<DisbursementUnitBean> getAllMDisbursementUnits();
	public void createMDisbursementUnit(DisbursementUnitBean obj);
	public void updateMDisbursementUnit(DisbursementUnitBean obj);
	public void deleteMDisbursementUnit(Integer id);
	public DisbursementUnitBean getMDisbursementUnitById(Integer id);

	public List<DisbursementUnitBean> getMDisbursementUnitByCriteria(DisbursementUnitBean obj);
	public void updateStatusCancel(Integer id);
	
	public List<DropdownBean> getDisbursementUnitByDepartmentId(Integer departmentId);

}
