package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.master.GlBean;
import th.go.cgd.epayment.model.master.StandardRevernueBean;

public interface IMGlService {

	public List<GlBean> getAllMGls();
	public void createMGl(GlBean obj);
	public void updateMGl(GlBean obj);
	public void deleteMGl(Integer id);
	public GlBean getMGlById(Integer id);

	public List<GlBean> getMGlsByCriteria(GlBean obj);
	public void updateStatusCancel(Integer id);

}
