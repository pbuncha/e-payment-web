package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.model.master.StandardRevernueBean;

public interface IMMoneySourceService {

	public List<MoneySourceBean> getAllMMoneySources();
	public void createMMoneySource(MoneySourceBean obj);
	public void updateMMoneySource(MoneySourceBean obj);
	public void deleteMMoneySource(Integer id);
	public MoneySourceBean getMMoneySourceById(Integer id);

	public List<MoneySourceBean> getMMoneySourceByCriteria(MoneySourceBean obj);
	public void updateStatusCancel(Integer id);
	
	
}
