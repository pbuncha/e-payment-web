package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.MOrganizationSubType;
import th.go.cgd.epayment.model.master.OrganizationSubTypeBean;

public interface IMOrganizationSubTypeService {

	public List<OrganizationSubTypeBean> getAllMOrganizationSubTypes();
	public void createMOrganizationSubType(OrganizationSubTypeBean obj);
	public void updateMOrganizationSubType(OrganizationSubTypeBean obj);
	public void deleteMOrganizationSubType(Integer id);

	public OrganizationSubTypeBean getMOrganizationSubTypeById(Integer id);
	public MOrganizationSubType findMOrganizationSubTypeById(Integer id);

}
