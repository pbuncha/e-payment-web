package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.MOrganizationType;
import th.go.cgd.epayment.model.master.OrganizationTypeBean;

public interface IMOrganizationTypeService {

	public List<OrganizationTypeBean> getAllMOrganizationTypes();
	public void createMOrganizationType(OrganizationTypeBean obj);
	public void updateMOrganizationType(OrganizationTypeBean obj);
	public void deleteMOrganizationType(Integer id);

	public OrganizationTypeBean getMOrganizationTypeById(Integer id);
	
	public MOrganizationType findMOrganizationTypeById(Integer id);

}
