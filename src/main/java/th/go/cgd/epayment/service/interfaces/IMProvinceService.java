package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.MAmphur;
import th.go.cgd.epayment.entity.MProvince;
import th.go.cgd.epayment.entity.MTambon;
import th.go.cgd.epayment.model.master.ProvinceBean;

public interface IMProvinceService {

	public List<ProvinceBean> getAllMProvinces();
	public void createMProvince(ProvinceBean obj);
	public void updateMProvince(ProvinceBean obj);
	public void deleteMProvince(Integer id);

	public ProvinceBean getMProvinceById(Integer id);
	public MProvince getProvinceById(Integer id);
	public MAmphur getAmphurById(Integer id);
	public MTambon getSubDistrictById(Integer id);

}
