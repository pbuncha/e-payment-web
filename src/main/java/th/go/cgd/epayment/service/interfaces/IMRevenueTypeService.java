package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.DropdownBean;

public interface IMRevenueTypeService {
	public List<DropdownBean> getStatusActive();
}
