package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.MStandardRevernueGl;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.model.master.StandardRevernueGlBean;

public interface IMStandardRevernueGlService {

	public List<StandardRevernueGlBean> getAllMStandardRevernueGls();
	public void createMStandardRevernueGl(StandardRevernueGlBean obj);
	public void updateMStandardRevernueGl(StandardRevernueGlBean obj);
	public void deleteMStandardRevernueGl(Integer id);
	public StandardRevernueGlBean getMStandardRevernueGlById(Integer id);

	public List<StandardRevernueGlBean> getMStandardRevernueGlByCriteria(StandardRevernueGlBean obj);
	public void updateStatusCancel(Integer id);
}
