package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.master.StandardRevernueBean;

public interface IMStandardRevernueService {

	public List<StandardRevernueBean> getAllMStandardRevernues();
	public void createMStandardRevernue(StandardRevernueBean obj);
	public void updateMStandardRevernue(StandardRevernueBean obj);
	public void deleteMStandardRevernue(Integer id);
	public StandardRevernueBean getMStandardRevernueById(Integer id);

	public List<StandardRevernueBean> getMStandardRevernuesByCriteria(StandardRevernueBean obj);
	public void updateStatusCancel(Integer id);

}
