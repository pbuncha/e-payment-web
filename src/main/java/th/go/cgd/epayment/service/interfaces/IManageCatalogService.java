package th.go.cgd.epayment.service.interfaces;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.ResponseBody;

import th.go.cgd.epayment.model.CatalogBean;
import th.go.cgd.epayment.model.DropdownBean;
import th.go.cgd.epayment.model.InvoiceBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.model.master.AccountDepositBean;
import th.go.cgd.epayment.model.master.CostCenterBean;
import th.go.cgd.epayment.model.master.DepartmentBean;
import th.go.cgd.epayment.model.master.GlBean;
import th.go.cgd.epayment.model.master.MoneySourceBean;
import th.go.cgd.epayment.model.master.StandardRevernueBean;

public interface IManageCatalogService {

    public Page<CatalogBean> search(SearchBean catalogBean);
	public List<DropdownBean> getMCatalogType();
	public List<DropdownBean> getMRevenueType();
	public List<DropdownBean> getMLevyType();
	public List<DropdownBean> getCostCenter();
	public CostCenterBean getCostCenterByCode(String costCenterCode);
	public DepartmentBean getDepartmentByCode(String departmentCode);
	public DepartmentBean getDepartmentById(String departmentId);
	public GlBean getGlByCode(String glCode);
	public Boolean save(CatalogBean catalogBean,Integer userId);
	public List<DropdownBean> getCatalogByCatType(Integer id);
	public boolean removeCatalog(List<CatalogBean> catalogBean) throws Exception ;
	public List<DropdownBean>  getCatalogAllByStatus();
	public boolean removeCatalogById(Integer id)  throws Exception;
	public byte[] exportExcel( List<CatalogBean> catalogBean);
	
	public CatalogBean getCatalog(Integer id);
	public AccountDepositBean getAccountDepositByCode(String accountDepositCode);
	public StandardRevernueBean getStandardRevernueByGlCode(String glCode);
	public MoneySourceBean getMMoneySourceByGlCode(String glCode);
	public Boolean copyCatalog(DropdownBean catalogCopy,Integer userId) throws IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException;
}
