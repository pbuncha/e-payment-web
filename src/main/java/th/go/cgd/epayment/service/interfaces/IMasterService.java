/**
 * 
 */
package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.entity.MTitleName;
import th.go.cgd.epayment.model.DropdownBean;

/**
 * @author wichuda.k Nov 11, 2017
 *
 */
public interface IMasterService {
	public List<DropdownBean> getTitleList() ;
	public List<DropdownBean> getPersonTypeList();
	public List<DropdownBean> getMCostCenterList();
	public List<DropdownBean> getMDepartmentList();
	public List<DropdownBean> getBank();
	public List<DropdownBean> getStatusUse(String [] statusId);
	public MTitleName getMTitleNameById(Integer id);
	public List<DropdownBean> getMBusinessAreaList();
}
