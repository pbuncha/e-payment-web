package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.ProvinceDropdownBean;

public interface IProvinceDropdownService {
	List<ProvinceDropdownBean> getAllprovince();
	List<ProvinceDropdownBean> getDistrictByProvince(Integer provinceId);
	List<ProvinceDropdownBean> getSubDistrictByDistrict(Integer DistrictId);
}
