package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.BillingBean;
import th.go.cgd.epayment.model.SearchBean;

public interface IReceiptService {

	public byte[] exportExcel( List<BillingBean> billingBean);
	public BillingBean getBillById(Integer billingId);
	public SearchBean getDefaultData();
	public String getGenCodeBilling(String printType,Integer billingId);
	
}
