package th.go.cgd.epayment.service.interfaces;

import th.go.cgd.epayment.model.UserBean;;

public interface IRegisterService {
	
	public Boolean save(UserBean userBean);

}
