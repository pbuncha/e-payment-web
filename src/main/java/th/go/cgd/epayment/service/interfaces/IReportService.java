/**
 * 
 */
package th.go.cgd.epayment.service.interfaces;

import java.util.Map;

import net.sf.jasperreports.engine.JRException;

/**
 * @author wichuda.k Nov 30, 2017
 *
 */
public interface IReportService {
	public byte[] getPDF(Map<String, Object> jasperParameter,String jrxmlName) throws JRException;
}
