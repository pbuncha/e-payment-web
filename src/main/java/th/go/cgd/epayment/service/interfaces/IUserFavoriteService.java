package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.InvoiceItemBean;

public interface IUserFavoriteService {
	public boolean save(List<Integer> listId);
	public List<Integer> getFavoriteByUser(Integer userId);
	public List<InvoiceItemBean> getFavoriteList(List<Integer> listId);
	
}
