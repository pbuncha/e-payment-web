package th.go.cgd.epayment.service.interfaces;

import java.util.List;

import th.go.cgd.epayment.model.MPermissionGroupBean;
import th.go.cgd.epayment.model.MUserPermissiongroupBean;
import th.go.cgd.epayment.model.SearchBean;
import th.go.cgd.epayment.model.UserBean;
import th.go.cgd.epayment.model.UserPermissionGroupBean;

public interface IUserInfoService {
	public List<UserBean> search(SearchBean userBean);
	public void update(List<UserBean> userBeanList,Integer userId);
	public void updateStatusUserPermission(List<UserBean> userBeanList,Integer userId);
	public List<UserPermissionGroupBean> search(Integer userId);
	public void update(UserBean bean,Integer userId);
	
	public List<MPermissionGroupBean> getMPermissionGroup(Integer userId);
	public UserBean getUserById(Integer userId);
	public void createUserPermissiongroup(MUserPermissiongroupBean objBean);
	public void updateUserPermissiongroup(MUserPermissiongroupBean objBean);
	public List<MUserPermissiongroupBean> getAllMUserPermissiongroupByUserId(Integer userId);
	public byte[] getFile(String path);
	
}
