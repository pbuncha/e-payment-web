package th.go.cgd.epayment.service.interfaces;

import th.go.cgd.epayment.entity.UserOrganization;

/**
 * @author torgan.p 26 ธ.ค. 2560 15:23:33
 *
 */
public interface IUserOrganizationService {
    public UserOrganization saveUserOrganization(UserOrganization userOrganization);
}
