package th.go.cgd.epayment.service.interfaces;

import th.go.cgd.epayment.entity.MUserPermissiongroup;

/**
 * @author torgan.p 26 ธ.ค. 2560 16:23:00
 *
 */
public interface IUserPermissiongroupService {
    public MUserPermissiongroup saveUserPermissiongroup(MUserPermissiongroup userPermissiongroupTb);

}
