package th.go.cgd.epayment.util;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.ResolverStyle;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.apache.log4j.Logger;

import th.go.cgd.epayment.controller.common.LoginController;



public class CalendarHelper {
	private static final Locale THAI_LOCALE = new Locale("th", "TH");
	private static final Logger logger = Logger.getLogger(LoginController.class);
	private static final String DATETIME_PATTERN = "dd/MM/yyyy HH:mm";
	private static final String LONG_DATE_PATTERN = "dd MMM yyyy";
	private static final String LONG_DATETIME_PATTERN = "dd MMM yyyy HH:mm";
	private static final String FULL_DATE_PATTERN = "dd MMMMM yyyy";
	private static final String SORT_DATE_PATTERN = "yyyy/MM/dd";
	private static final String DB_DATE_PATTERN = "yyyy-MM-dd";
	private static final String COMMENT_DATE_PATTERN = "dd-MM-yyyy";
	private static final String YEAR_ONLY_PATTERN = "yyyy";
	private static final String SHORT_YEAR_ONLY_PATTERN = "yy";
	private static final String MONTH_ONLY_PATTERN = "MM";
	private static final String FULL_DATETIMESTAMP_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";
	private static final String FULL_DATETIMESTAMP_PATTERN2 = "yyyy-MM-dd HH:mm:ss.SSS";
	private static final String SHORT_DATE_PATTERN_TH = "dd/MM/yyyy";
	private static final String SHORT_DATE_PATTERN_TH2 = "ddMMyyyy";
	public static final SimpleDateFormat DATE_FORMAT_TH = new SimpleDateFormat(SHORT_DATE_PATTERN_TH, THAI_LOCALE);

	public static final SimpleDateFormat YY = new SimpleDateFormat("yy",new Locale("EN","en"));
	
	
	
	    public static String getYearFormatYY(){
	        String result ="";
	        Date date = new Date();
	        try{
	            result =   YY.format(date);
	        }catch(Exception e){
	            e.printStackTrace();
	        }
	        return result;
	    }
	    
	    public static Integer getAge(Date birthDate){
		  Calendar calendarset = new GregorianCalendar();
		  logger.info("birthDate : "+birthDate);
		  Integer age = 0;
		  try{
        		  calendarset.setTime(birthDate);
        		  int year       = calendarset.get(Calendar.YEAR);
        		  int month      = calendarset.get(Calendar.MONTH); 
        		  int date3      = calendarset.get(Calendar.DATE); 
        		  LocalDate today = LocalDate.now();
        		  LocalDate birthday = LocalDate.of(year, month, date3);
        		  Period p = Period.between(birthday, today);
        		  age = p.getYears();
		  }catch(Exception e){
		      e.printStackTrace();
		  }
		 logger.info("Age : "+age);
		 return age;
	    }
	    public static Integer getAgeInput(Date birthDate){
		  Calendar calendarset = new GregorianCalendar();
		  logger.info("birthDate : "+birthDate);
		  Integer age = 0;
		  try{
        		  calendarset.setTime(birthDate);
        		  int year       = calendarset.get(Calendar.YEAR);
        		  int month      = calendarset.get(Calendar.MONTH)+1; 
        		  int date3      = calendarset.get(Calendar.DATE); 
        		  LocalDate today = LocalDate.now();
        		  LocalDate birthday = LocalDate.of(year, month, date3);
        		  Period p = Period.between(birthday, today);
        		  age = p.getYears();
		  }catch(Exception e){
		      e.printStackTrace();
		  }
		 logger.info("Age : "+age);
		    

		 return age;
	    }
	    public static String formatDateTH(Date d) {
//		System.out.println("Round : "+date2); //Tue Aug 31 10:20:56 SGT 1982
		SimpleDateFormat sdf = new SimpleDateFormat(SHORT_DATE_PATTERN_TH,THAI_LOCALE);
		String date = sdf.format(d);
		 System.out.println("date Str : "+date);
		return date;
	    }
	    
	    public static String formatDateTH2(Date d) {
//		System.out.println("Round : "+date2); //Tue Aug 31 10:20:56 SGT 1982
		SimpleDateFormat sdf = new SimpleDateFormat(SHORT_DATE_PATTERN_TH2,THAI_LOCALE);
		String date = sdf.format(d);
		 System.out.println("date Str : "+date);
		return date;
	    }
	    
	    public static String getYear(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat(YEAR_ONLY_PATTERN,THAI_LOCALE);
		String date = sdf.format(d);
		 System.out.println("date Str : "+date);
		return date;
	    }
	    
	    public static String getShortYear(Date d) {
			SimpleDateFormat sdf = new SimpleDateFormat(SHORT_YEAR_ONLY_PATTERN,THAI_LOCALE);
			String date = sdf.format(d);
			 System.out.println("date Str : "+date);
			return date;
		    }
	    
	    public static String getMonth(Date d) {
		SimpleDateFormat sdf = new SimpleDateFormat(MONTH_ONLY_PATTERN,THAI_LOCALE);
		String date = sdf.format(d);
		 System.out.println("date Str : "+date);
		return date;
	    }
	    
	    public static java.util.Date getDateTime(Object theDateObj) {
	        java.sql.Time w_TempDate;
	        java.util.Date theDateDate = null;
	        if (theDateObj == null) {
	            w_TempDate = null;
	        } else {
	            theDateDate = (java.util.Date) theDateObj;
//	            System.out.print(theDateDate);
	        }

	        return theDateDate;

	    }
	   


}
