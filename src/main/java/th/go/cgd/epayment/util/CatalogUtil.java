package th.go.cgd.epayment.util;

import java.lang.reflect.Method;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;

import th.go.cgd.epayment.constant.EPaymentConstant;
import th.go.cgd.epayment.service.MasterService;

@Service
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CatalogUtil {
	
//	@Autowired
//	private HazelcastInstance hazelcastInstance;
	
//	@Autowired
//	public CatalogUtil(MasterService masterService){
//		loadCatalog(masterService);
//	}
	
	public void loadCatalog(MasterService masterService) {
//		masterService.setMCasePermission();
//		masterService.setMCourt();
//		masterService.setMCourtType();
//		masterService.setMCurrency();
//		masterService.setMDocumentTemplate();
//		masterService.setMExpiryType();
//		masterService.setMForm();
//		masterService.setMFormStatus();
//		masterService.setMLawsuitCaseDecided();
//		masterService.setMLawsuitCompromiseResult();
//		masterService.setMLawsuitPersonDecided();
//		masterService.setMLitigantType();
//		masterService.setMMaritalStatus();
//		masterService.setMMessageTemplate();
//		masterService.setMModule();
//		masterService.setMMonth();
//		masterService.setMOrgType();
//		masterService.setMProvince();
//		masterService.setMPersonType();
//		masterService.setMRelationship();
//		masterService.setMSendCaseType();
//		masterService.setMTitleName();
//		masterService.setMUserStatus();
//		masterService.setMNewsStatus();
//		masterService.setMParam();
//		masterService.setMCountry();
//		masterService.setMStatus();
//		masterService.setMFlowchart();
//		masterService.setMUserRole();
//		masterService.setMUserAcessPermission();
//		masterService.SetMCaseReportResult();
//		masterService.SetMEmailTemplate();
//		masterService.SetMNotificationType();
//		masterService.setMLogAction();
//		masterService.setMTaskStatus();
//		masterService.setMPermissionRole();
//		masterService.setMDistrict();
//		masterService.setMSubDistrict();
//		masterService.setMReport();
		
//		masterService.setMViolationAccompliceType();
//		masterService.setMViolationAppealResult();
//		masterService.setMViolationAppointmentOrgType();
//		masterService.setMViolationCaseType();
//		masterService.setMViolationCloseCaseReason();
//		masterService.setMViolationInquiryResult();
//		masterService.setMViolationOrderingType();
//		masterService.setMViolationPreinvestigateInquiry();
//		masterService.setMViolationPrescriptionYear();
		
//		masterService.setMCivilAbttLitigantType();
//		masterService.setMCivilArbMeditationResult();
//		masterService.setMCivilCaseType();
//		masterService.setMCivilCloseCaseReason();
//		masterService.setMCivilCompensationType();
//		masterService.setMCivilMeditationResult();
//		masterService.setMCivilPrescriptionYear();
		
//		masterService.setMEducationCaseType();
//		masterService.setMEducationCloseCaseReason();
//		masterService.setMEducationContractType();
//		masterService.setMEducationExtensionDecisionResult();
//		masterService.setMEducationLeaveType();
//		masterService.setMEducationLevel();
//		masterService.setMEducationLiabilityType();
//		masterService.setMEducationPersonType();
//		masterService.setMEducationRelationship();
//		masterService.setMEducationReplaceTerm();
//		masterService.setMEducationReplaceType();
//		masterService.setMEducationScholarshipType();
//		masterService.setMEducationType();
//		masterService.setMEducationExpenseType();
		
//		masterService.SetMDebtAssetTrackingResult();
//		masterService.SetMDebtBankruptCaseDecided();
//		masterService.SetMDebtBankruptCompromiseResult();
//		masterService.SetMDebtBankruptReceivingOrder();
//		masterService.SetMDebtCloseCaseReason();
//		masterService.SetMDebtDecisionResult();
//		masterService.SetMDebtInstallmentPeriod();
//		masterService.SetMDebtInstallmentPlan();
//		masterService.SetMDebtPaymentType();
//		masterService.SetMDebtStatus();
//		masterService.SetMDebtType();
//		masterService.SetMDebtWriteOffDecisionResult();
//		masterService.SetMDebtWriteOffReason();
//		masterService.SetMDebtCostType();
//		masterService.SetMDebtAmountType();
		
		//Master All Status
//		masterService.setMProvinceAllStatus();
//		masterService.setMDistrictAllStatus();
//		masterService.setMSubDistrictAllStatus();
//		masterService.setMCurrencyAllStatus();
//		masterService.setMRelationshipAllStatus();
//		masterService.setMViolationCaseTypeAllStatus();
//		masterService.setMViolationPrescriptionYearAllStatus();
//		masterService.setMCivilPrescriptionYearAllStatus();
//		masterService.setMEducationCaseTypeAllStatus();
//		masterService.setMEducationLevelAllStatus();
//		masterService.setMEducationTypeAllStatus();
//		masterService.setMCountryAllStatus();
	}
	
	public List<Object> get(String key){
//		return hazelcastInstance.getList(key);
	    return null;
	}
	
	public <T> T getObject(String key, short id) 
	{
		List<Object> mUtilityInputTypeList = this.get(key);
		for(Object object : mUtilityInputTypeList){
			Method[] methods = object.getClass().getMethods();
			for (Method method: methods) {
				if (method.getAnnotation(javax.persistence.Id.class) != null) {
					try {
						Number idNumberType = (Number) method.invoke(object);
						if( idNumberType.shortValue() == id ){
							@SuppressWarnings("unchecked")
							T t = (T) object;
							return t;
						}
					} catch(Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		
		if(key.indexOf("AllStatus")==-1)
		{
			return getObject(key+"AllStatus", id);
		}
		
		return null;
	}
	
	public <T> T getObject(String key, String id)
	{
		List<Object> mUtilityInputTypeList = this.get(key);
		for(Object object : mUtilityInputTypeList){
			Method[] methods = object.getClass().getMethods();
			for (Method method: methods) {
				if (method.getAnnotation(javax.persistence.Id.class) != null) {
					try {
						String idNumberType = (String) method.invoke(object);
						if( idNumberType.equals(id) ){
							@SuppressWarnings("unchecked")
							T t = (T) object;
							return t;
						}
					} catch(Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		
		if(key.indexOf("AllStatus")==-1)
		{
			return getObject(key+"AllStatus", id);
		}
		
		return null;
	}
	
	public <T> T getObject(String key, int id)
	{
		List mUtilityInputTypeList = this.get(key);
		for(Object object : mUtilityInputTypeList){
			Method[] methods = object.getClass().getMethods();
			for (Method method: methods) {
				try {
					if (method.getAnnotation(javax.persistence.Id.class) != null) {
						Number idNumberType = (Number) method.invoke(object);
						if( idNumberType.intValue() == id ){
							return (T) object;
						}
					}
				} catch(Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		
		if(key.indexOf("AllStatus")==-1)
		{
			return getObject(key+"AllStatus", id);
		}
		
		return null;
	}
	
//	public <T> T getFlowchartObject(String key, String formId, String status) throws Exception
//	{
//		List mUtilityInputTypeList = this.get(key);
//		for(Object object : mUtilityInputTypeList){
//			Method[] methods = object.getClass().getMethods();
//			for (Method method: methods) {
//				if (method.getAnnotation(javax.persistence.EmbeddedId.class) != null) {
//					MFlowchartId mFlowchartId = (MFlowchartId) method.invoke(object);
//					if(null!=mFlowchartId && mFlowchartId.getFormId().equals(formId) && mFlowchartId.getStatus().equals(status)){
//						return (T) object;
//					}
//				}
//			}
//		}
//		return null;
//	}
	
	/**
	 * 
	 * @param key the static field's name in CivilConstant class
	 * @return the Catalog list from hazelcast
	 * @throws Exception
	 */
	public List<Object> getWithConstantName(String key) throws Exception {
		return get((String) EPaymentConstant.class.getField(key).get(null));
	}
	
	public <T> void reload(MasterService masterService, String key, Class<T> clazz)
	{
		masterService.reload(key, clazz);
		if(EPaymentConstant.M_PARAM.equals(key))
		{
			masterService.setMParam();
		}
	}
	
	public <T> T save(MasterService masterService, String key, Class<T> clazz, T o)
	{
		T saveObject = masterService.save(o);
		masterService.reload(key, clazz);
		return saveObject;
	}
	
	public <T> void delete(MasterService masterService, String key, Class<T> clazz, Short id)
	{
		masterService.delete(clazz, id);
		masterService.reload(key, clazz);
	}
}
