package th.go.cgd.epayment.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

public class ClientMachineUtil {
	
	public static String getClientMachineName(HttpServletRequest request) {
		
		String computerName = null;
	    String remoteAddress = request.getRemoteAddr();
	    
	    try {
	        InetAddress inetAddress = InetAddress.getByName(remoteAddress);
	        computerName = inetAddress.getHostName();
	        if (computerName.equalsIgnoreCase("localhost")) {
	            computerName = java.net.InetAddress.getLocalHost().getCanonicalHostName();
	        } 
	    } catch (UnknownHostException e) {

	    }
		
		return computerName;
	}
	
}
