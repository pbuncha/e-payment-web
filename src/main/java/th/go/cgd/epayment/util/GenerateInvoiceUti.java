package th.go.cgd.epayment.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class GenerateInvoiceUti {
	
	private static SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd", new Locale("TH", "th"));
	private static int[] CONSTANCE = { 3, 9, 7, 9, 5 };
	private static int PAY_PERIOD = 30;
	private int pivot = 0;
	private char[] ref1;
	private String ref2;
	private String checker;

	

	public GenerateInvoiceUti(Date invoiceDate, String running) throws ParseException {
		this.ref1 = (formatter.format(invoiceDate) + running + "10").toCharArray();
		generate();
	}


	private void generate() throws ParseException {
		String createDate = new String(Arrays.copyOfRange(this.ref1, 0, 6));

		Calendar cal = Calendar.getInstance();
		cal.setTime(formatter.parse(createDate));
		cal.add(Calendar.DAY_OF_YEAR, PAY_PERIOD );

		String expireDate = formatter.format(cal.getTime());
		char[] ref2 = new char[expireDate.length()];

		for (int i = 0; i < expireDate.length(); i++) {
			ref2[i] = expireDate.charAt(i);
		}

		int sumRef1 = calculate(this.ref1);
		int sumRef2 = calculate(ref2);

		this.checker = String.format("%02d", (sumRef1 + sumRef2) % 100);
		this.ref2 =new String(ref2)+checker;
	}

	private int calculate(final char[] data) {
		int sum = 0;
		for (int i = 0; i < data.length; i++, pivot++) {
			int index = (pivot * 5 + pivot) % 5, value = Integer.parseInt(String.valueOf(data[i]));
			sum += (CONSTANCE[index] * value);
		}

		return sum;
	}

	public String getChecker() {
		return checker;
	}

	public void setChecker(String checker) {
		this.checker = checker;
	}
	
	public String getRef2() {
		return ref2;
	}

	public void setRef2(String ref2) {
		this.ref2 = ref2;
	}

	public String getRef1() {
		return new String(ref1);
	}





//	public static void main(String[] args) throws ParseException {
//		GenerateInvoiceUti generateInvoiceUti = new GenerateInvoiceUti(new Date(), "00000001");
//		System.out.println("6012050000000110");
//		System.out.println("6012050000000110".substring(0, 14));
//	}
}
