package th.go.cgd.epayment.util;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.ScrollableResults;
import org.hibernate.internal.AbstractScrollableResults;

import th.go.cgd.epayment.util.datatable.IndexOperation;

public class SqlUtil 
{
	public static String[] getColumnNamesFromSql(String sql) {
		sql = sql.toUpperCase();
		sql = removeAllSubQuery(sql);
		List<String> res = new ArrayList<String>();
		int lastPos = sql.indexOf("SELECT") + 6;
		outerLoop:
		while(lastPos < sql.length()) {
			IndexOperation<GetColumnNamesFromSqlEnum> op = new IndexOperation<GetColumnNamesFromSqlEnum>(sql.length(), GetColumnNamesFromSqlEnum.CANT_FIND);
			op.setOp(sql.indexOf(',', lastPos), GetColumnNamesFromSqlEnum.COMMA);
			op.setOp(sql.indexOf("FROM", lastPos), GetColumnNamesFromSqlEnum.FROM);
			String col = sql.substring(lastPos, op.idx);
			switch(op.op) {
			case CANT_FIND:
				throw new RuntimeException("Can't find FROM in sql: " + sql);
			case COMMA:
				lastPos = op.idx + 1;
				res.add(col);
				break;
			case FROM:
				lastPos = op.idx + 4;
				res.add(col);
				break outerLoop;
			}
		}
		
		for (int i = 0; i < res.size(); ++i) {
			String col = res.get(i).trim();
			int idx;
			if ((idx = col.indexOf(" AS ")) >= 0) {
				col = col.substring(idx + 4).trim();
			} else if ((idx = col.indexOf(' ')) >= 0) {
				col = col.substring(idx + 1).trim();
			} else if ((idx = col.indexOf('.')) >= 0) {
				col = col.substring(idx + 1).trim();
			}
			res.set(i, col);
		}
		
		return res.toArray(new String[res.size()]);
	}
	
	private enum GetColumnNamesFromSqlEnum{
		COMMA, CANT_FIND, FROM;
	}
	
	private static String removeAllSubQuery(String sql) {
		char[] charArray = sql.toCharArray();
		StringBuilder sb = new StringBuilder();
		int lastPos = 0;
		while(lastPos < sql.length()) {
			IndexOperation<RemoveSubQueryEnum> op = new IndexOperation<RemoveSubQueryEnum>(sql.length(), RemoveSubQueryEnum.PROCEED);
			op.setOp(sql.indexOf('(', lastPos), RemoveSubQueryEnum.OPEN_PARENTHESIS);
			sb.append(charArray, lastPos, op.idx - lastPos);
			lastPos = op.idx + 1;
			switch(op.op) {
			case OPEN_PARENTHESIS:
				if (true) {
					int parenthesisCount = 1;
					while(parenthesisCount > 0) {
						IndexOperation<RemoveParenthesisEnum> paOp = new IndexOperation<RemoveParenthesisEnum>(sql.length(), RemoveParenthesisEnum.CANT_FIND);
						paOp.setOp(sql.indexOf('(', lastPos), RemoveParenthesisEnum.OPEN);
						paOp.setOp(sql.indexOf(')', lastPos), RemoveParenthesisEnum.CLOSE);
						lastPos = paOp.idx + 1;
						switch(paOp.op) {
						case CANT_FIND:
							throw new RuntimeException("Can't find Close parenthesis for sql: " + sql);
						case CLOSE:
							--parenthesisCount;
							break;
						case OPEN:
							++parenthesisCount;
						}
					}
				}
				break;
			case PROCEED:
				break;
			}
		}
		return sb.toString();
	}
	
	private enum RemoveSubQueryEnum{
		OPEN_PARENTHESIS, PROCEED;
	}
	
	private enum RemoveParenthesisEnum{
		CANT_FIND, OPEN, CLOSE;
	}

	public static String[] getColumnNamesResultSetFromHackishScrollAbleResult(ScrollableResults scroll) {
		try {
			Method method = AbstractScrollableResults.class.getDeclaredMethod("getResultSet");
			method.setAccessible(true);
			ResultSet resultSet = (ResultSet) method.invoke(scroll);
			ResultSetMetaData metaData = resultSet.getMetaData();
			String[] colName = new String[metaData.getColumnCount()];
			for (int i = 0; i < metaData.getColumnCount(); i++) {
				colName[i] = metaData.getColumnLabel(i + 1); // sql column index by 1
			}
			return colName;
		}catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
