package th.go.cgd.epayment.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Query;

/**
 * Build SQL's where cause.
 * 
 * @author Thanadee
 *
 */
public class SqlWhereCauseBuilder {

	private StringBuilder sBuilder = new StringBuilder();

	private boolean isFirstCause;

	private Map<String, Object> paramMap = new HashMap<String, Object>();

	private SqlWhereCauseBuilder() {
	}
	
	/**
	 * Important, it's not safe to use more than 1 instance at a time.
	 * 
	 * @return
	 */
	public static SqlWhereCauseBuilder getInstance(boolean hasWhereCause) {
		SqlWhereCauseBuilder builder = threadLocal.get();
		builder.init(hasWhereCause);
		return builder;
	}

	private void init(boolean hasWhereCause) {
		sBuilder.setLength(0);
		paramMap.clear();
		if (hasWhereCause) {
			isFirstCause = false;
		} else {
			isFirstCause = true;
		}
	}
	
	public SqlWhereCauseBuilder append(String sqlCause) {
		if (isFirstCause) {
			sBuilder.append(" WHERE ");
		} else {
			sBuilder.append(" AND ");
		}
		sBuilder.append(sqlCause);
		isFirstCause = false;
		return this;
	}

	public SqlWhereCauseBuilder append(String sqlCause, String paramName,
			Object param) {
		if (param == null) {
			return this;
		} else if (param instanceof Collection<?>) {
			if (((Collection<?>) param).isEmpty()) {
				// collection is empty if statement is IN statement will add and 1 != 1 to statement
				if (sqlCause.toUpperCase().indexOf(" NOT IN ") < 0){
					sqlCause = "1 != 1";
					param = null;
				} else {
					return this;
				}
			}
		}
		if (isFirstCause) {
			sBuilder.append(" WHERE ");
		} else {
			sBuilder.append(" AND ");
		}
		sBuilder.append(sqlCause);
		if (param != null) {
			paramMap.put(paramName, param);
		}
		isFirstCause = false;
		return this;
	}
	
	public StringBuilder appendTo(StringBuilder builder) {
		builder.append(sBuilder);
		return builder;
	}

	public String appendTo(String builder) {
		return builder + sBuilder.toString();
	}

	public Query setParameter(Query query) {
		for (String key : paramMap.keySet()) {
			Object value = paramMap.get(key);
			if (value instanceof Collection<?>) {
				query.setParameterList(key, (Collection<?>) value);
			} else {
				query.setParameter(key, paramMap.get(key));
			}
		}
		return query;
	}
	
	public Map<String,Object> getParameterMap(){
		return paramMap;
	}

	private static MyThreadLocal threadLocal = new MyThreadLocal();

	private static class MyThreadLocal extends
			ThreadLocal<SqlWhereCauseBuilder> {
		@Override
		protected SqlWhereCauseBuilder initialValue() {
			return new SqlWhereCauseBuilder();
		}
	}
}
