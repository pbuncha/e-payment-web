package th.go.cgd.epayment.util;

import org.springframework.stereotype.Component;

@Component
public class ThymeleafUtil {
	public String makeOptionalEL(String el) {
		return el.replace(".", "?.");
	}
	public boolean checkNull(Object obj) {
		if(obj == null) return true;
		if(obj instanceof String){
			String str = (String) obj;
			return str.trim().equals("") || str.trim().equals("0") || str.trim().equals("-1");
		}else if(obj instanceof Integer){
			int val = (int) obj;
			return val <= 0;
		}else if(obj instanceof Short){
			short val = (short) obj;
			return val <= 0;
		}else if(obj instanceof Long){
			long val = (Long) obj;
			return val <= 0;
		}
		return false;
	}
}
