package th.go.cgd.epayment.util.datatable;

public class IndexOperation <T extends Enum<T>> 
{
	public T op;
	public int idx = 0;
	
	public IndexOperation(int idx2, T defaultOp) {
		idx = idx2;
		op = defaultOp;
	}
	
	public void setOp(int idx2, T op2) {
		if (idx2 != -1)
			if (idx2 < idx){
				op = op2;
				idx = idx2;
			}
	}
}