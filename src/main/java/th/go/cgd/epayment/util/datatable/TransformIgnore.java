package th.go.cgd.epayment.util.datatable;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * For configure the AnnotationResultTransformers.
 * If annotation is not present will honor javax's column , didn't need all target to be filled and didn't need all source to be present
 * @author Thanadee
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TransformIgnore 
{
	/**
	 * will ignore these select statement's column
	 * @return
	 */
	String[] value() default {};
	/**
	 * determine if this class will be scanned for javax.persistant.Column
	 * @return
	 */
	boolean willHonorColumn() default false;
	/**
	 * if true will throw exception if some select query didn't match any receiver
	 * @return
	 */
	boolean needAllSource() default true;
	/**
	 * if true will throw exception if some receiver didn't filled.
	 * @return
	 */
	boolean needAllTarget() default false;
}
