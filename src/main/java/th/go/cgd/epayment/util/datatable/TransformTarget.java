package th.go.cgd.epayment.util.datatable;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD})
public @interface TransformTarget 
{
	/**
	 * select query's names to be use for this receiver.
	 * @return
	 */
	String[] value() default {};
}