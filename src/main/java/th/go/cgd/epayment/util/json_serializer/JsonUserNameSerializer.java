package th.go.cgd.epayment.util.json_serializer;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.repository.UserRepository;

@Component
public class JsonUserNameSerializer extends JsonSerializer<Integer>{

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public void serialize(Integer id, JsonGenerator gen, SerializerProvider provider)
			throws IOException, JsonProcessingException {
		if (id == null) {
			gen.writeString("");
			return;
		}
		User user = userRepository.findOne(id);
		if (user == null) {
			gen.writeString("");
			return;
		} else {
//			gen.writeString(user.getFullName());
			return;
		}
	}

}
