package th.go.cgd.epayment.ws.endpoint;

import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.xml.xsd.XsdSchema;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


public abstract class AbstactSoapEndpoint{

	
	@Autowired @Qualifier("billpaymentSchema")
	protected XsdSchema billPaymentSchema;
	
	
	protected <T> void validateXml( JAXBElement<T> request ) throws SAXException, JAXBException{
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = sf.newSchema(billPaymentSchema.getSource());

		// create JAXB context
		JAXBContext context = JAXBContext.newInstance(request.getValue().getClass());

//		Marshaller marshaller = context.createMarshaller();
//		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
//		marshaller.setSchema(schema);
//
//		marshaller.marshal(request, System.out);
		JAXBSource source = new JAXBSource(context, request);
		Validator validator = schema.newValidator();
		validator.setErrorHandler(new CustomValidationErrorHandler());
		try {
			validator.validate(source);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	private static class CustomValidationErrorHandler   implements ErrorHandler{

		@Override
	    public void warning(SAXParseException exception) throws SAXException {
	        System.out.println("WARNING Occured");
	        exception.printStackTrace();
	    }
		@Override
	    public void error(SAXParseException exception) throws SAXException {
	        System.out.println("ERROR Occured");
	        exception.printStackTrace();
	    }
		@Override
	    public void fatalError(SAXParseException exception) throws SAXException {
	        System.out.println("FATAL ERROR Occured");
	        exception.printStackTrace();
	    }
	}
}
