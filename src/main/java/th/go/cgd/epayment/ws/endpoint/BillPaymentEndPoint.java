package th.go.cgd.epayment.ws.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.xml.sax.SAXException;

import th.go.cgd.epayment.config.SoapConfig;
import th.go.cgd.epayment.ws.entity.InputBillpaymentManage;
import th.go.cgd.epayment.ws.entity.OutputBillpaymentManage;
import th.go.cgd.epayment.ws.service.interfaces.IBillPaymentWebService;

@Endpoint
public class BillPaymentEndPoint extends AbstactSoapEndpoint {

	@Autowired
	private IBillPaymentWebService billPaymentWebService;

	@PayloadRoot(namespace = SoapConfig.BILLPAYMENT_NAMESPACE, localPart = "CreateBillPaymentRequest")
	@ResponsePayload
	public JAXBElement<OutputBillpaymentManage> requestCreateBillPaymentService(
			@RequestPayload JAXBElement<InputBillpaymentManage> request) {
		try {
			super.validateXml(request);
			billPaymentWebService.createInvoice(request.getValue());
		} catch (SAXException | JAXBException er) {
			er.printStackTrace();
		}
		OutputBillpaymentManage response = new OutputBillpaymentManage();

		return new JAXBElement<OutputBillpaymentManage>(request.getName(), OutputBillpaymentManage.class, response);
	}

}
