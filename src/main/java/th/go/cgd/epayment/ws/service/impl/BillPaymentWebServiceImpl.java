package th.go.cgd.epayment.ws.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.go.cgd.epayment.entity.Address;
import th.go.cgd.epayment.entity.Catalog;
import th.go.cgd.epayment.entity.Invoice;
import th.go.cgd.epayment.entity.InvoiceItem;
import th.go.cgd.epayment.entity.User;
import th.go.cgd.epayment.repository.CatalogRepository;
import th.go.cgd.epayment.repository.InvoiceItemRepository;
import th.go.cgd.epayment.repository.MAmphurRepository;
import th.go.cgd.epayment.repository.MProvinceRepository;
import th.go.cgd.epayment.repository.MTambonRepository;
import th.go.cgd.epayment.repository.UserRepository;
import th.go.cgd.epayment.ws.entity.DataListBillpaymentManage;
import th.go.cgd.epayment.ws.entity.InputBillpaymentManage;
import th.go.cgd.epayment.ws.service.interfaces.IBillPaymentWebService;

@Service
@Transactional
public class BillPaymentWebServiceImpl implements IBillPaymentWebService {

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CatalogRepository catalogRepository;
	@Autowired
	private MAmphurRepository amphurRepository;
	@Autowired
	private MTambonRepository tambonRepository;
	@Autowired
	private MProvinceRepository provinceRepository;
	@Autowired
	private InvoiceItemRepository invoiceItemRepository;
	
	@Override
	public void createInvoice(InputBillpaymentManage inputBillpaymentManage) {
		
		final Date now = new Date();
		List<InvoiceItem> invoiceListList = new LinkedList<InvoiceItem>();
		User createBy = userRepository.getMockupCreateUser();
		
		for (DataListBillpaymentManage billpaymentManage : inputBillpaymentManage.getBillPaymentData()) {
			/**
			 * Create New Address From WebService.
			 */
			Address address = createAddressEntity(billpaymentManage, now, createBy);
			
			/**
			 * Create New Invoice From WebService.
			 */
			Invoice invoice = createInvoiceEntity(billpaymentManage, now, createBy);
			invoice.setAddressIdBilling(address);
			invoice.setInvoiceAddressIdBilling(address);
			
			/**
			 * Create New InvoiceItem From WebService.
			 */
			InvoiceItem invoiceItem = createInvoiceItem(billpaymentManage, now, createBy);
			invoiceItem.setInvoiceId(invoice);
			
			invoiceListList.add(invoiceItem);
		}
		
		invoiceItemRepository.save(invoiceListList);
		
		
	}
	
	private final Invoice createInvoiceEntity(DataListBillpaymentManage billpaymentManage, Date createDate, User createBy ){
		Invoice invoice = new Invoice();
		invoice.setCreatedDate(billpaymentManage.getInvoiceCreateDate());
		invoice.setStartDate(billpaymentManage.getInvoiceStartDate());
		invoice.setEndDate(billpaymentManage.getInvoiceEndDate());
		invoice.setRef1(billpaymentManage.getRefNo1());
		invoice.setRef2(billpaymentManage.getRefNo2());
		invoice.setRef3(billpaymentManage.getRefNo3());
		invoice.setCitizenNo(billpaymentManage.getCitizenNo() + billpaymentManage.getBusinessNo() + billpaymentManage.getTaxID());
//		invoice.setTitleId( billpaymentManage.getTitleName() );
		invoice.setFirstName(billpaymentManage.getFirstName() );
		invoice.setMiddleName(billpaymentManage.getMiddleName());
		invoice.setLastName(billpaymentManage.getLaseName());
		invoice.setTelephone(billpaymentManage.getMobileNo());
		invoice.setCanSendEmail(StringUtils.isNotBlank(billpaymentManage.getCanSendToEmail())?billpaymentManage.getCanSendToEmail().charAt(0):null);
		invoice.setCanSendMobile(StringUtils.isNotBlank(billpaymentManage.getCanSendToMobile())?billpaymentManage.getCanSendToMobile().charAt(0):null);
		invoice.setEmail(billpaymentManage.getEmail());
		invoice.setCreatedDate(createDate);
		invoice.setUpdatedDate(createDate);
		invoice.setCreatedBy(createBy);
		invoice.setUpdatedBy(createBy);
		return invoice;
	}
	
	private final Address createAddressEntity(DataListBillpaymentManage billpaymentManage, Date createDate, User createBy ){
		Address address = new Address();
		address.setNo(billpaymentManage.getHouseNo());
		address.setBuildingName(billpaymentManage.getBuildingName());
		address.setMoo(billpaymentManage.getMoo());
		address.setSoi(billpaymentManage.getSoi());
		address.setRoad(billpaymentManage.getRoad());
		address.setAmphurId(amphurRepository.getOne( Integer.valueOf(billpaymentManage.getAmphurCode())));
		address.setTambonId(tambonRepository.getOne( Integer.valueOf(billpaymentManage.getTambonCode())));
		address.setProvinceId(provinceRepository.getOne( Integer.valueOf(billpaymentManage.getProvinceCode())));
		address.setPostcode(billpaymentManage.getPostcode());
		address.setCreatedDate(createDate);
		address.setUpdatedDate(createDate);
		address.setCreatedBy(createBy);
		return address;
	}
	
	private final InvoiceItem createInvoiceItem(DataListBillpaymentManage billpaymentManage, Date createDate, User createBy ){
		Catalog catalog = catalogRepository.findLatestVersoinByCode(billpaymentManage.getCatalogCode());
		InvoiceItem invoiceItem = new InvoiceItem();
		invoiceItem.setAmount( BigDecimal.valueOf(billpaymentManage.getAmount()) );
		invoiceItem.setCatalogId(catalog);
		invoiceItem.setInvoiceItemName( catalog.getCatalogName() );
		invoiceItem.setCreatedDate(createDate);
		invoiceItem.setUpdatedDate(createDate);
		invoiceItem.setCreatedBy(createBy);
		invoiceItem.setUpdatedBy(createBy);
		return invoiceItem;
	}

}
