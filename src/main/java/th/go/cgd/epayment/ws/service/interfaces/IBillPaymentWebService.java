package th.go.cgd.epayment.ws.service.interfaces;

import th.go.cgd.epayment.ws.entity.InputBillpaymentManage;

public interface IBillPaymentWebService {
	void createInvoice(InputBillpaymentManage inputBillpaymentManage);
}
