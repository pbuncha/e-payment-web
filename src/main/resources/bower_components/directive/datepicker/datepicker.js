

var app = angular.module('datepickerApp', []);

app.directive('datepickerDir', ['$q', '$compile','$http' ,function($q, $compile, $http) {

    return {
        restrict: 'EA',
        scope: 	{
        	dataModel : "=ngModel",
        	formatDate : "@",
        	checkValid : "=",
        	disableDate: "=",
        	min : "=min",
        	onChange: "&ngChange"
        },
        require: '?ngModel',
        link: function( scope, element, attrs, ctrRoot){
        	var isFirst = true;
        	scope.dt = new Date(scope.dataModel);
        	scope.$watch( 'dataModel' ,function(n,o){
        		if( !isFirst && angular.isUndefined(attrs['onChange'])){
        			scope.onChange();
        		}
        		isFirst = false;
        	});
        },
        controller: function($rootScope,$scope,$q) {
        	//$scope.dt = $scope.dataModel;
      	  $scope.today = function() {
  		    $scope.dt = new Date();
  		  };
  		 // $scope.today();

  		  $scope.clear = function() {
  		    $scope.dt = null;
  		  };

  		  $scope.inlineOptions = {
  		    customClass: getDayClass,
  		    minDate: new Date(),
  		    showWeeks: true
  		  };

  		  $scope.dateOptions = {
//  		    dateDisabled: disabled,
  		    formatYear: 'yy',
//  		    maxDate: new Date(2017, 0, 7),
  		    minDate: new Date(2017, 0, 5),
  		  showButtonBar: false,
  		showWeeks:false,
  		    startingDay: 1
  		  };

  		  // Disable weekend selection
  		  function disabled(data) {
  			  console.log(data);
  		    var date = data.date,
  		      mode = data.mode;
  		    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
  		  }

  		  $scope.toggleMin = function() {
  		    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
  		    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
  		  };

  		  $scope.toggleMin();

  		  $scope.open1 = function() {
  		    $scope.popup1.opened = true;
  		  };

  		  $scope.open2 = function() {
  		    $scope.popup2.opened = true;
  		  };

  		  $scope.setDate = function(year, month, day) {
  		    $scope.dt = new Date(year, month, day);
  		  };

//  		  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'shortDate'];
//  		  $scope.format = $scope.formats[2];
  		 $scope.format = $scope.formatDate;
  		  $scope.altInputFormats = ['M!/d!/yyyy'];

  		  $scope.popup1 = {
  		    opened: false
  		  };

  		  $scope.popup2 = {
  		    opened: false
  		  };

  		  var tomorrow = new Date();
  		  tomorrow.setDate(tomorrow.getDate() + 1);
  		  var afterTomorrow = new Date();
  		  afterTomorrow.setDate(tomorrow.getDate() + 1);
  		  $scope.events = [
  		    {
  		      date: tomorrow,
  		      status: 'full'
  		    },
  		    {
  		      date: afterTomorrow,
  		      status: 'partially'
  		    }
  		  ];

  		  function getDayClass(data) {
  		    var date = data.date,
  		      mode = data.mode;
  		  
  		    if (mode === 'day') {
  		      var dayToCheck = new Date(date).setHours(0,0,0,0);

  		      for (var i = 0; i < $scope.events.length; i++) {
  		        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

  		        if (dayToCheck === currentDay) {
  		          return $scope.events[i].status;
  		        }
  		      }
  		    }

  		    return '';
  		  }
        	
        	
        },

//        template:
//        			'<div ng-class={"has-error":checkValid}>'+
//        			' <p class="input-group">'+
//        			'<input type="text" readonly="readonly" style="background-color:white;"  class="form-control" ng-init="dt" uib-datepicker-popup="{{format}}" ng-model="dataModel" is-open="popup1.opened" datepicker-options="dateOptions"  close-text="Close" alt-input-formats="altInputFormats" />'+
//        			'<span class="input-group-btn">'+
//        			'<button type="button" ng-class={"btn-danger":checkValid,"btn-default":!checkValid} class="btn"  ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>'+
//        			'</span>'+
//        			' </p>' +
//        			'<div>'
        			
//        "disabledatepicger":true
        			  template:
        					'<div  ng-class={"has-error":checkValid}>'+
        					' <p class="input-group">'+
        					'<input type="text" max-date="today" readonly="readonly" style="background-color:white;" ng-disabled=disableDate  class="form-control input-sm datedisable" ng-init="dt" uib-datepicker-popup="{{format}}" ng-model="dataModel" is-open="popup1.opened" datepicker-options="dateOptions"  close-text="Close" alt-input-formats="altInputFormats" />'+
        					'<span  class="input-group-btn" >'+
        					'<button type="button"   ng-disabled=disableDate style="padding: 2px 12px;" ng-class={"btn-danger":checkValid,"btn-default":!checkValid} class="btn input-sm"  ng-click="open1()"><i class="glyphicon glyphicon-calendar"  ></i></button>'+
        					'</span>'+
        					' </p>' +
        					'<div>'+
        					'<div style="color: #a94442" ng-show=checkValid>'+
        					'<span ng-bind=validTxt></span></div>'


    };
}]);

