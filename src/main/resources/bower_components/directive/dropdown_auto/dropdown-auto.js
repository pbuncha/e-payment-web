var app = angular.module('dropdownApp', [ 'ui.select' ]);

app.filter('propsFilter', function() {
	return function(items, props) {
		var out = [];

		if (angular.isArray(items)) {
			var keys = Object.keys(props);

			items
					.forEach(function(item) {
						var itemMatches = false;

						for (var i = 0; i < keys.length; i++) {
							var prop = keys[i];
							var text = props[prop].toLowerCase();
							if (item[prop].toString().toLowerCase().indexOf(
									text) !== -1) {
								itemMatches = true;
								break;
							}
						}

						if (itemMatches) {
							out.push(item);
						}
					});
		} else {
			// Let the output be the input untouched
			out = items;
		}

		return out;
	};
});

app
		.directive(
				'dropdownAuto',
				[
						'$q',
						'$compile',
						'$http',
						function($q, $compile, $http) {

							return {
								restrict : 'AE',
								transclude : true,
								scope : {
									dataModel : "=ngModel",
									listValue : "=",
									placeholder : "@",
									groupBy : "@",
									require : "=ngRequired",
									disabled : "=ngDisabled",
									change : "&ngChange",
									init : "=ngInit",
									trash : "=",
									order : "="

								},
								require : '?ngModel',
								link : function(scope, element, attrs, ctrRoot,
										$select) {
									//console.log('text : ' + scope.groupBy);
									 if(scope.init){
										 scope.dataModel = scope.init;
									 }
									 
									 console.log(scope.trash);

									scope.$watch("listValue", function(
											newValue, oldValue) {
										scope.dataList = eval(scope.listValue);
									});
									
									

								},

								// templateUrl:
								// 'templates/campaign_management/home.html',
								template : '<ui-select ng-required="require" trash="trash" on-select="change()" ng-disabled="disabled" sortable="true" ng-model="$parent.dataModel" style="margin-bottom:4px"  theme="bootstrap"  reset-search-input="'
										+ "'true'"
										+ '">'
										+ '<ui-select-match placeholder={{placeholder}}>{{$select.selected.desc}}</ui-select-match>'
										+ '<ui-select-choices ng-animate-children="false" group-by=$parent.groupBy repeat="data in dataList  | filter:$select.search | orderBy:order | limitTo: 100">'
										+ '<div ng-bind-html="data.desc | highlight: $select.search"></div>'
										+ ' </ui-select-choices>'
										+ '</ui-select>'

							};
						} ]);