'use strict';

var input = angular.module('inputCustom', ['ui.bootstrap']);

input.directive('fileBrowser',['$parse', '$window', function ($parse, $window) {
	return {
		restrict: 'EA',
		require: 'ngModel',
		replace: true,
		scope: { ngModel: "=",
				  total: "@",
		},
		template:	'<div style="position: relative;" class="upload">\
						<div class="input-group-nolabel input-group input-group">\
						<span class="btn btn-default input-group-addon"> Choose File...\
						<input type="file" name="file"  class="upload-file" id="file" size="10" accept=""/></span>\
						<input type="text" class="form-control" id="include-product-info" placeholder="{{fileName}}" readonly="readonly" />\
						</div>\
						<label class="control-label" >File Upload : <span ng-bind="fileName"></span>&nbsp;&nbsp;<a href="" ng-click="removeFile()"><i class="fa fa-trash"></i></a></label>\
					</div>',
			controller: function( $scope ){

				/* DEFINED VARIABLE */
				$scope.ngModel = {};
				$scope.file = undefined;
				$scope.fileName = "no file";

				var reader = new FileReader();
				reader.onload  = function(e){

					$scope.$apply(function(){

						/* get FileReader */
						var base64 = _arrayBufferToBase64(e.target.result);

						/* Add element to file object*/
						var obj ={};
						obj.name = $scope.file.name;
						obj.size = $scope.file.size;
						obj.type = $scope.file.type;
						obj.base64 = base64;
						$scope.ngModel = obj;
						$scope.fileName = $scope.file.name;

						/* Clear temporary file */
						$scope.fileBrowser_Name = undefined;
						$scope.file = undefined

					});
				};


				$scope.$addFile = function( ){

					console.log($scope.file);
				//	if( ($scope.ngModel.length < $scope.total) && ($scope.file != undefined) ){

						reader.readAsArrayBuffer( $scope.file );
				//	}   
				}

				$scope.removeFile = function(){
					
					$scope.ngModel = {};
					$scope.file = undefined;
					$scope.fileName = "no file";

					/* Add element to file object*/
					var obj ={};


					/* Clear temporary file */
					$scope.fileBrowser_Name = undefined;
					$scope.file = undefined
//					$scope.ngModel.splice($index,1);
//					console.log($scope.ngModel);
////					$scope.ngModel = {};
////					$scope.file = undefined;
////					$scope.fileName = "no file";
//					
//					var obj ={};
//
//					/* Clear temporary file */
//					$scope.fileBrowser_Name = undefined;
//					$scope.file = undefined
//					$scope.fileName = "no file";
//					$scope.ngModel = {};
//					console.log($scope.ngModel);
				}

				/* This function is a converter ArrayByte to String Base64*/
				function _arrayBufferToBase64( buffer ) {
					var binary = '';
					var bytes = new Uint8Array( buffer );
					var len = bytes.byteLength;
					for (var i = 0; i < len; i++) {
						binary += String.fromCharCode( bytes[ i ] );
					}
					return $window.btoa( binary );
				}
				/* End */

			},

			link: function (scope, element, attrs, ngModel) {

				// Default accept all file type.
				scope.accepType = attrs.accept || "*";

				// Default a max size of file is 1024 Kb.
				var maxSize = 1024*1024 ;
				// if attribute maxSize is valid and is not NaN
				if( attrs.maxSize !== undefined && attrs.maxSize >= 0 && (maxSize = parseInt(attrs.maxSize)) != NaN ){
					maxSize = parseInt(attrs.maxSize)*1024; // Convert to bytes.
				}

				// Default select button
				scope.title = attrs.title || "Choose File";

				// file input element to access even
				var fileInputEle = element.find('input');

				// on change even
				fileInputEle.bind('change', function(event){
					scope.$apply(function () {
						event.preventDefault();

						scope.file = event.target.files[0];

						// The selected file is less than maxSize.
						if( scope.file != undefined && scope.file.size <= maxSize ){
							
							// Set viewValue
							scope.fileBrowser_Name = scope.file.name;

							/* Clear file input to empty.*/
							fileInputEle.val(null);
							
							//addFile On change
							scope.$addFile();
							
							
						}else{
							// The selected file is mor than maxSize. Alert message
							alert("File size is mor than "+ (maxSize/1024) + " Kb.");
							scope.file = undefined;
						}
					});
				});
			}
	};
}]);

(function(){
	'use strict';
	var injection = [];
	angular.module('ui.template', injection);
	angular.module('ui.template')
	.directive('labelRequire',[function(){
		var self = {};
		self.template = '<span>*</span>';
		self.style = {	 "color":"red"
			,"font-size": "large"
				,"margin-right":"3px"
					,"vertical-align":"text-top"
		};
		return {
			restrict: 'C',
			link: function( scope, element, attrs ){
				element.prepend(function(){
					return angular.element(self.template).css(self.style);
				});
			}
		}
	}]);
}(angular));
