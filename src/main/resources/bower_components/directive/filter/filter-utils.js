(function() {

	'use strict';
	angular.module('filter.util', []);

	angular.module('filter.util')
	.directive('numberFormat', ['$filter', '$parse', function($filter, $parse) {
		  const decimals = 2;
		  return {
		    require: 'ngModel',
		    link: function(scope, element, attrs, ngModelController) {

		      var elem = angular.element(element);


		      if (angular.isDefined(scope[attrs['ngModel']])) {
		        ngModelController.$formatters.push(function(value) {
		          return toDecimal(value);
		        });
		      } else {
		        $parse(attrs['ngModel']).assign(scope, toDecimal(0));
		      }

		      scope.$watch(attrs.ngModel, function(newValue, oldValue) {
		        var arr = String(newValue).split("");
		        if (arr.length === 0) return;
		        else if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.')) return;
		        else if (arr.length === 2 && newValue === '-.') return;
		        else if (isNaN(newValue)) {
		          scope[attrs.ngModel] = oldValue;
		        }
		      });

		      element.bind('focus', function() {
		        elem.val(scope[attrs.ngModel]);
		      });

		      element.bind('blur', function() {
		        var value = toDecimal(scope[attrs.ngModel]);
		        elem.val(value);
		      });


		      function toDecimal(val) {
		        var value = "0.00";
		        if (!isNaN(val)) {
		          value = val;
		        }
		        return $filter('number')(value, decimals);
		      }
		      
		      /**
		       * Number format style,Text-right
		       */
		      elem.css('text-align', 'right');
		    }
		  }
		}]);

})(angular);