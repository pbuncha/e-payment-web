var myApp = angular.module('modalApp', ['ui.bootstrap','ngAnimate']);

myApp.service('messageDialog', ['$uibModal', function($uibModal) {
	return {
		
		modalOpen: function(scope, content){
		 return $uibModal.open({
		      templateUrl: '/cmt-web/public/lib/assets/directive/modal/modal-confirm.html',
		      scope: scope,
		      controller: function($scope) {
			        $scope.btnYes = function(){
			        	content.yes();
			        	$scope.modalInstance.close('Yes Button Clicked');
			        };
			        $scope.btnNo = function(){
			        	content.no();
			        	$scope.modalInstance.close('Yes Button Clicked');
			        };
			        $scope.title = content.title;
			        $scope.content = content.content;
			      }
		 });
		}
		,
	
		modalAlert: function(content){
			var modalInstance = $uibModal.open({
		      templateUrl: '/cmt-web/public/lib/assets/directive/modal/modal-alert.html',
		      controller: function($scope) {
			        $scope.content = content;
			        $scope.btnNo = function(){
//			        	content.no();
			        	modalInstance.close('Alert close');
			        };
			        $scope.title = "Alert";
			      }
		 });
			return modalInstance;
		}
		,
		modalNoti: function(content){
			var modalInstance = $uibModal.open({
		      templateUrl: '/cmt-web/public/lib/assets/directive/modal/modal-notification.html',
		      controller: function($scope) {
			        $scope.content = content;
			        $scope.btnNo = function(){
//			        	content.no();
			        	modalInstance.close('Alert close');
			        };
			        $scope.title = "Notification";
			      }
		 });
			return modalInstance;
		}
	}		
}]);

myApp.directive('divDialog', function (messageDialog) {
    return {
        restrict: 'AE',
        template: '<ng-transclude></ng-transclude><div ng-click="openModal()" style="position:absolute;top:0;left:0;right:0;bottom:0;"></div>',
        transclude : true,
        scope: {
            mbClass : '@',
            mbTitle : '@',
            mbMessage : '@',
            mbBtnName : '@',
            mbYes : '&',
            mbNo : '&'
        },
        link: function(scope,element, attributes) {
        	element.css('position','relative');
            scope.modalContent = { 
					title : typeof scope.mbTitle !== 'undefined'?scope.mbTitle:'Are you sure',
					content : typeof scope.mbMessage !== 'undefined'?scope.mbMessage:'Are you sure',
					yes : function(){
						  scope.mbYes();
						  console.log('Yes Button Clicked');
						},
					no : function(){
						  scope.mbNo();
						  console.log('No Button Clicked');
						}
					}
			scope.openModal = function () {
				scope.modalInstance = messageDialog.modalOpen(scope,scope.modalContent);
			}
        
        }
    };
});
				
