

var app = angular.module('modalApp', ['ui.bootstrap','ngAnimate']);

	
app.service('messageDialog', ['$uibModal', function($uibModal) {
	return {
		
		openMenuModal: function(){
		 $uibModal.open({
		      animation: true,
		      ariaLabelledBy: 'modal-title-bottom',
		      ariaDescribedBy: 'modal-body-bottom',
		      templateUrl: '../public/lib/assets/directive/modal/redemtion-popup.html',
		      size: 'sm',
		      controller: function($scope) {
		        $scope.name = 'bottom';  
		      }
		 });
		}
	}
    
		
}]);


