(function(angular) {
	'use strict';
	angular.module('modalProductApp', ['ui.bootstrap','ngAnimate','ngResource','ngSanitize','ngRoute']);
	
	angular.module('modalApp')
	.service('$selectProduct', ['$uibModal', '$http', function($uibModal, $http) {
		return function(numOfSelect, target, title){
			var urlBase = "../campaignController";
			return $uibModal.open({
				animation : true,
				size: "lg",
				templateUrl: '../public/lib/assets/directive/modal/select-product.html',
				controller: function( $uibModalInstance ) {
					var list = [];
					var self = this;
					
					self.records = [], self.title = title, self.currentPage = 1;
                	self.assignValue = function(value){
                		if(list.length >= numOfSelect){
                			list[0].select = false;
                			list.splice(0,1);
                		}
                		value.select = true;
                		list.push(value);
                	}
                	
                	self.currentPage = 1;
                	var serachStr = " ";
                	self.pageChanged = function( ) {
                		
                	    $http.post(urlBase+'/'+target+"/"+self.currentPage, serachStr)
                        .then(function (response) {
                        	self.totalElements = response.data.totalElements;
                        	self.currentPage = response.data.number;
                        	self.maxSize = response.data.size;
                        	self.records = response.data.dataList;
                        	self.currentNoOfRecord = self.currentPage*self.maxSize-self.maxSize+1;
                        	self.showingTo = self.currentPage*self.maxSize-self.maxSize+self.records.length;
                        }, function (error) {
                            var status01 = 'Unable to load customer data: ' + error.message;
                        });
                	};
                	
                	self.submitSearch = function(){
                		console.log(self.nameOrCode);
                		serachStr = " "+self.nameOrCode+" ";
                		self.currentPage = 1;
                		self.pageChanged();
                	}
                	self.choose = function(){
                		$uibModalInstance.close(list);
                	}
                	
                	self.pageChanged();
				},
				controllerAs: '$ctrl'
			});
		}
	}]);

})(window.angular);