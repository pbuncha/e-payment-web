var myApp = angular.module('input.multiple',['ui.bootstrap']);

myApp.directive('multiInput', function ($timeout) {
    return {
        restrict: 'AE',
        template: '<div class="col-sm-12" style="background-color: white;-webkit-appearance: none;display: block;border: 1px solid #ccc;border-radius: 4px;padding: 3px 6px;display: flex;flex-wrap: wrap;">'+
        				'<div class="btn btn-default btn-xs" ng-show="focus" ng-repeat="data in miModel track by data.id">'+
        					'<div><span ng-bind="data.value"></span>'+
        						'<span class="close ui-select-match-close" ng-mousedown="close($event,data)"">&nbsp;×</span>'+
        					'</div>'+
        				'</div>'+
        				'<input ng-focus="onFocus()" ng-keydown="keyPress($event)" ng-blur="onBlur()" ng-model="myInput" placeholder="{{miModel.length > 0?'+"''"+':miHint}}" type="text" style="flex:1;background-color: transparent !important;border: none;outline: none;height: 23.33px;min-width: 10px;padding-top:4px;padding-bottom:1px;"/>'+
        			'</div>'

        	,
        transclude : true,
        scope: {
            miModel : '=ngModel',
            miHint : '@placeholder'
        },
        link: function(scope ,$element) {
        	if(typeof scope.miModel === 'undefined'){
        		scope.miModel = [];
        	}
        	
        	scope.myInput = '';
        	scope.focus = false;
        	
        	scope.onBlur = function(){
        		scope.focus = false;
        		var temp = '';
        		for(var i=0; i < scope.miModel.length ; i++){
        			temp = temp+scope.miModel[i].value;
        			if(i != scope.miModel.length-1){
        				temp = temp+',';
        			}
        		}
        		scope.myInput=temp;
        		
        	}
        	
        	scope.onFocus = function(){
        		scope.focus = true;
        		scope.myInput='';
        	}
        	
        	scope.close = function(event,data) {
        	    console.log("click");
        	    
        	    var index = scope.miModel.indexOf(data);
          		scope.miModel.splice(index,1);
          		console.log(index);
          		console.log(scope.miModel);
          		event.preventDefault();
        	};
        	
        	var txtTemp = '';
        	var count = 0;
        	var lineCount = 0;
        	scope.keyPress = function(event){
        		if (event.keyCode === 8) {
        			if(scope.myInput.substring(scope.myInput.length-1) == '-'){
        				console.log(scope.myInput.substring(scope.myInput.length-1));
        				lineCount = 0;
        			}
        			if(scope.myInput.length == 0){
        				
        				if(count > 0){
        					scope.miModel.splice(scope.miModel.length-1,1);
        					count = 0;
        				}else{
        					count++;
        				}
        				
        			}
        			

        	    }
        		if (event.keyCode === 13) {
        			if(scope.myInput != ''){
        				putToModel(scope.myInput.substring(0,scope.myInput.length));
        			}
        			

        	    }
        		if (event.keyCode === 32) {
        			event.preventDefault();
        			if(scope.myInput != ''){
        				putToModel(scope.myInput.substring(0,scope.myInput.length));
        			}

        	    }
        	}
        	
        	scope.$watch("myInput",function(newValue,oldValue){
        		count = 0;
        		if(typeof newValue != 'undefined'){
        			
        			
        			if ( (( !isNaN(newValue.substring(newValue.length-1)) && angular.isNumber(+newValue.substring(newValue.length-1))) || (newValue.substring(newValue.length-1) == '-' && lineCount == 0))) {
        				//console.log(lineCount);
        				console.log('Yes');
            			if(newValue.substring(newValue.length-1) == '-'){
            				//console.log('-');
            				
            				if(scope.myInput.length <= 1){
            					console.log('No');
                		    	if(oldValue.length < newValue.length){
                		    		scope.myInput = oldValue.replace(/[\s]/g, '');
                    			}
            				}else{
            					lineCount++;
            				}
            			}
            			
            			
            			
        		    }else{
        		    	console.log('No');
        		    	if(oldValue.length < newValue.length){
        		    		scope.myInput = oldValue.replace(/[\s]/g, '');
            			}
        		    	
        		    }
        			if(newValue.substring(newValue.length-1) == ','){
        				putToModel(scope.myInput.substring(0,scope.myInput.length));
        				
        			}
        			
        		}
				
				
				
			});
        	
        	var putToModel = function(data){
        		
        		if(data.substring(data.length-1) == '-'){
        			console.log('end -');
        		}else{
        			var temp = data.replace(/[\s]/g, '').split('-');
            		if(parseInt(temp[0], 10) >= parseInt(temp[1], 10)){
            			console.log('Greater');
            			console.log(parseInt(temp[0], 10));
            			console.log(parseInt(temp[1], 10));
            		}else{
            			for(var i = 0; i < temp.length; i++){
                			//console.log(temp[i].trim());
                			temp[i] = temp[i].trim();
                		}
            			
            			console.log('Lesser');
            			console.log(parseInt(temp[0], 10));
            			console.log(parseInt(temp[1], 10));
                		if(temp.length > 1){
                			console.log(temp.length);
                			scope.miModel.push({id:scope.miModel.length,value:temp[0]+'-'+temp[1]});
                		}else{
                			console.log(temp.length);
                			scope.miModel.push({id:scope.miModel.length,value:temp[0]});
                		}

        				scope.myInput = '';
        				lineCount = 0;
            		}
        		}
        		
        		
        	}
        	
        }
    };
});


myApp.directive('multiTag', function ($timeout) {
    return {
        restrict: 'AE',
        template: '<div class="col-sm-12" ng-click="btnClick()" style="height:34px;background-color: white;-webkit-appearance: none;display: block;border: 1px solid #ccc;border-radius: 4px;padding: 3px 6px;display: flex;flex-wrap: wrap;">'+
						'<div class="btn btn-default btn-xs" style="height:22px;margin-top:3px;" ng-show="miModel.length > 0?showTag:false" ng-repeat="(key,data) in miModel">'+
							'<div><span ng-bind="data.value"></span>'+
								'<span class="close ui-select-match-close" ng-click="closeClick(data)">&nbsp;×</span>'+
							'</div>'+
						'</div>'+
						'<input ng-model="myInput" ng-keydown="keyPress($event)" ng-focus="onFocus()" sync-focus="inputElement" ng-blur="onBlur()" ng-show="miModel.length > 0?!showTag:true" placeholder="{{miModel.length > 0?'+"''"+':miHint}}" type="text" style="flex:1;background-color: transparent !important;border: none;outline: none;height: 23px;min-width: 10px;"/>'+
					'</div>'

        	,
        transclude : true,
        scope: {
            miModel : '=ngModel',
            miHint : '@placeholder'
        },
        link: function(scope ,$element) {
        	if(typeof scope.miModel === 'undefined'){
        		scope.miModel = [];
        	}
        	scope.myInput = '';
        	scope.showTag = true;
        	
        	var txtTemp = '';
        	var count = 0;
        	var lineCount = 0;
        	var colonCount = 0;
        	
        	scope.onFocus = function(){
        		var temp = '';
        		for(var i = 0; i < scope.miModel.length; i++){
        			temp = temp+scope.miModel[i].value;
        			if(i != scope.miModel.length-1){
        				temp = temp+',';
        			}
        		}
        		scope.myInput = temp;
        	}
        	
        	scope.onBlur = function(event){
    			scope.miModel.length = 0;
        		scope.showTag = true;
        		var temp = scope.myInput;
        		var array = temp.split(',');
        		console.log(array);
        		
        		for(var i = 0; i < array.length; i++){
        			array[i] = array[i].trim();
        			var value = array[i].split('-');
        			if(array[i].length > 1 && array[i].substring(array[i].length-1) == '-'){
        				array[i] = array[i].substring(0,array[i].length-1);
        			}
        			
        			if(array[i] != ''){
        				if(value.length > 1){
        					if(parseInt(value[0],10) > parseInt(value[1],10)){
        						scope.miModel.push({id:scope.miModel.length,value:value[1]+'-'+value[0]});
        					}else{
        						scope.miModel.push({id:scope.miModel.length,value:value[0]+'-'+value[1]});
        					}
            			}else{
            				scope.miModel.push({id:scope.miModel.length,value:array[i]});
            			}
        				
        			}
        			
        		}
        		scope.myInput = '';
        		
        	}
    	    
    	    var close = false;
    	    scope.closeClick = function(data){
    	    	//alert('Close click');
    	    	close = true;
    	    	var index = scope.miModel.indexOf(data);
          		scope.miModel.splice(index,1);
    	    }
    	    
    	    scope.btnClick = function(){
    	    	if(close){
    	    		close=false;
    	    	}else{
    	    		//alert('Button click');
    	    		scope.showTag = false;
    	    		//console.log(scope.inputElement);
    	    		$timeout(function () {
    	    			scope.inputElement.focus();
                    });
    	    	}
    	    }
    	    
    	    
    	    scope.keyPress = function(event){
        		if (event.keyCode === 8) {
        			if(scope.myInput.substring(scope.myInput.length-1) == '-'){
        				console.log(scope.myInput.substring(scope.myInput.length-1));
        				lineCount = 0;
        			}
        			if(scope.myInput.length == 0){
        				
        				if(count > 0){
        					scope.miModel.splice(scope.miModel.length-1,1);
        					count = 0;
        				}else{
        					count++;
        				}
        				
        			}
        			

        	    }
        		if (event.keyCode === 13) {
        			
        			

        	    }
        		if (event.keyCode === 32) {
        			event.preventDefault();

        	    }
        	}
    	    
    	    
    	    scope.$watch("myInput",function(newValue,oldValue){
        		count = 0;
        		if(typeof newValue != 'undefined'){
        			
        			
        			if ( (( !isNaN(newValue.substring(newValue.length-1)) && angular.isNumber(+newValue.substring(newValue.length-1))) || (newValue.substring(newValue.length-1) == '-' && lineCount == 0)) || newValue.substring(newValue.length-1) == ',' ) {
        				//console.log(lineCount);
        				console.log('Yes');
            			if(newValue.substring(newValue.length-1) == '-'){
            				//console.log('-');
            				
            				if(scope.myInput.length <= 1){
            					console.log('No');
                		    	if(oldValue.length < newValue.length){
                		    		scope.myInput = oldValue.replace(/[\s]/g, '');
                    			}
            				}else{
            					lineCount++;
            				}
            			}
            			
            			
            			
        		    }else{
        		    	
        		    	console.log('No');
        		    	if(oldValue.length < newValue.length && newValue.substring(newValue.length-1) != '.'){
        		    		scope.myInput = oldValue.replace(/[\s]/g, '');
            			}else{
            				console.log(".");
            			}
        		    	
        		    }
        			if(newValue.substring(newValue.length-1) == ','){
        				lineCount = 0;
        			}
        			
        		}
				
				
				
			});
        	
        }// end of link
    };
});

myApp.directive('syncFocus', function ($timeout) {
    return {
        restrict: 'AE',
        scope: {
            sfData : '=syncFocus'
        },
        link: function(scope ,element) {
        	scope.sfData = element[0];
        }
    };
});

