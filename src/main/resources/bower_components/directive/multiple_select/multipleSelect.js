var app = angular.module('multipleSelectApp', [ 'ui.select' ]);

app
		.directive(
				'multipleSelect',
				[
						'$q',
						'$compile',
						'$http',
						function($q, $compile, $http) {

							return {
								restrict : 'EA',
								scope : {
									dataModel : "=ngModel",
									placeholder : "@",
									listValue : "=",
									groupBy : "@",
									prefix : "@",
									callFunction : "=",
									disabled : "=ngDisabled",
									order : "="
								},
								transclude : true,
								require : '?ngModel',
								link : function(scope, element, attrs, ctrRoot) {
									
									if(!scope.dataModel){
										scope.dataModel = [];
									}
									scope.$watch('listValue', function(n) {
										scope.dataList = eval(n);
									});
									if(typeof scope.callFunction !== 'undefined'){
										scope.inFunction = function(value) {
											
											var rvalue = '';										
											for (var i = 0; i < value.length; i++) {
												if (i == value.length - 1) {
													rvalue += '' + value[i].id;
												} else {
													rvalue += '' + value[i].id + ',';
												}
											}										
											//console.log(rvalue);
											scope.callFunction(rvalue);
										}
									}

								},

								template : '<ui-select ng-change="inFunction($parent.dataModel)" multiple="" ng-init="$parent.dataModel"  ng-model="$parent.dataModel" theme="bootstrap" ng-disabled="disabled" sortable="true" close-on-select="false" reset-search-input="true" >'
										+ '<ui-select-match placeholder={{placeholder}} >{{(prefix?$item[prefix]:"") + " " +$item.desc}}</ui-select-match>'
										+ '<ui-select-choices group-by=groupBy repeat="data in dataList | filter:$select.search | orderBy:order">'
										+ '{{data.desc}}'
										+ '</ui-select-choices>'
										+ '</ui-select>'

							};
						} ]);
