var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';
		  
	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-cost-center');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-cost-center/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-cost-center',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-cost-center',obj);
	    };

	    return dataFactory;
});

app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $location,$route,$http) {
	
	$scope.obj ={};

	loadBusinessAreaList();
	loadDisburementUnitList();
	loadDepartmentList();

	var id = $location.absUrl().split('/')[6];
	dataFactory.findOne(id)
	.then(function (response) {
		$scope.obj =response;
		if(response.startDate!=null){
			$scope.obj.startDate =  new Date(response.startDate);
		}
		if(response.endDate!=null){
			$scope.obj.endDate =  new Date(response.endDate);
		}		
	}, function (error) {
		$scope.status = 'Unable to load data: ' + error.message;
	});
	
	
	
	$scope.create = function () {		
		dataFactory.create($scope.obj)
		$window.location.href = ctx + '/master2/cost-center-view';	
	};
	
	$scope.update = function () {		
		dataFactory.update($scope.obj)
		$window.location.href = ctx + '/master2/cost-center-view';	
	};		
	
	$scope.cancel = function() {
		$window.location.href = ctx + '/master2/cost-center-view';	
	};

	function loadBusinessAreaList() {
		var url  = ctx+'/master2/findall-business-area';		
		$http.get(url).then(function(response) {			
			console.log(response);	
			$scope.businessAreaList = response.data;
		}, function (error) {
			$scope.status = 'Unable to load data: ' + error.message;
		});		
	}	

	function loadDepartmentList() {
		var url  = ctx+'/master2/findall-department';		
		$http.get(url).then(function(response) {			
			console.log(response);	
			$scope.departmentList = response.data;
		}, function (error) {
			$scope.status = 'Unable to load data: ' + error.message;
		});		
	}	

	function loadDisburementUnitList() {
		var url  = ctx+'/master2/findall-disbursement-unit';		
		$http.get(url).then(function(response) {			
			console.log(response);	
			$scope.disburementUnitList = response.data;
		}, function (error) {
			$scope.status = 'Unable to load data: ' + error.message;
		});		
	}	
	
	/*	$scope.createMDisbursementUnit = function () {
	
	console.log("mDisbursementUnitBean=>"+$scope.mDisbursementUnit)
	dataFactory.createMDisbursementUnit($scope.mDisbursementUnit)
	$window.location.href = ctx + '/master2/view-cost-center';	
};

$scope.updateMDisbursementUnit = function () {
	
	console.log("mDisbursementUnitBean=>"+$scope.mDisbursementUnit)
	dataFactory.updateMDisbursementUnit($scope.mDisbursementUnit)
	$window.location.href = ctx + '/master2/view-cost-center';	
}*/;	

});


