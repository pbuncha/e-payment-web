var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';

	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-cost-center');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-cost-center/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-cost-center',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-cost-center',obj);
	    };
	    dataFactory.search = function (code) {
	        return httpProvider().post(urlBase+'/search-cost-center',code);
	    };
	    dataFactory.updateStatusCancel = function (id) {
	        return httpProvider().get(urlBase+'/cancel-cost-center/' + id);
	    };

	    return dataFactory;
});


app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $http) {
	
	var $ctrl = this;

	$scope.criteria ={};
	$scope.data2List ={};

	doSearch();
	
	$scope.doSearch = doSearch;	
		
	function doSearch() {
		console.log($scope);
		dataFactory.search($scope.criteria).then(function (response) { 
			console.log(response);
			$scope.data2List = response;
			$scope.totalItems = $scope.data2List.length;
			$scope.currentPage = 1;
			$scope.itemsPerPage = 10;
			$scope.maxSize = 5;
		}, function (error) {
			$scope.status = 'Unable to load customer data: ' + error.message;
		});
	};	

	$scope.updateStatusCancel = function( id ){
		dataFactory.updateStatusCancel(id).then(function(){
			doSearch();
		});
	}

	$scope.clearData = function() {
		$scope.criteria ={};
	};
		
	$scope.goCreateNew = function () {
		$window.location.href = ctx + '/master2/cost-center-create';	
	};

});


