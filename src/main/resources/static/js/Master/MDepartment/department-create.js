var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';

	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-department');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-department/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-department',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-department',obj);
	    };

	    return dataFactory;
});

app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $location,$route,$http) {
		
	$scope.obj ={};
	
	loadOrganizationSubTypeList();
	loadParentDepartmentList();
	
	$scope.create = function () {
		dataFactory.create($scope.obj);
		$window.location.href = ctx + '/master2/department-view';	
	};
	
	$scope.update = function () {		
		dataFactory.update($scope.obj);
		$window.location.href = ctx + '/master2/department-view';	
	};	
	
	$scope.cancel = function() {
		$window.location.href = ctx + '/master2/department-view';	
	};

	function loadOrganizationSubTypeList() {
		var url  = ctx+'/master2/findall-organization-sub-type';		
		$http.get(url).then(function(response) {			
			console.log(response);	
			$scope.organizationSubTypeList = response.data;
		}, function (error) {
			$scope.status = 'Unable to load data: ' + error.message;
		});		
	}	

	function loadParentDepartmentList() {
		var url  = ctx+'/master2/findall-department';		
		$http.get(url).then(function(response) {			
			console.log(response);	
			$scope.parentDepartmentList = response.data;
		}, function (error) {
			$scope.status = 'Unable to load data: ' + error.message;
		});		
	}	
	
});


