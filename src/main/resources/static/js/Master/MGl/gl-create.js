var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';

	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-gl');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-gl/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-gl',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-gl',obj);
	    };

	    return dataFactory;
});

app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $location,$route,$http) {
	
	$scope.obj ={};
		
	loadStandardRevenueList();
	
	$scope.create = function () {		
		dataFactory.create($scope.obj)
		$window.location.href = ctx + '/master2/gl-view';	
	};
	
	$scope.update = function () {		
		dataFactory.update($scope.obj)
		$window.location.href = ctx + '/master2/gl-view';	
	};
		
	$scope.cancel = function() {
		$window.location.href = ctx + '/master2/gl-view';	
	};

	function loadStandardRevenueList() {
		var url  = ctx+'/master2/findall-standard-revernue';		
		$http.get(url).then(function(response) {			
			console.log(response);	
			$scope.standardRevernueList = response.data;
		}, function (error) {
			$scope.status = 'Unable to load data: ' + error.message;
		});		
	}	
});


