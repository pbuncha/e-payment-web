var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';
		  
	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-organization-type');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-organization-type/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-organization-type',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-organization-type',obj);
	    };

	    return dataFactory;
});

app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $location,$route,$http) {
	
	$scope.obj ={};

	var id = $location.absUrl().split('/')[6];
	dataFactory.findOne(id)
	.then(function (response) {
		$scope.obj =response;
	}, function (error) {
		$scope.status = 'Unable to load data: ' + error.message;
	});
	
	$scope.create = function () {		
		dataFactory.create($scope.obj)
		$window.location.href = ctx + '/master2/organization-type-view';	
	};
	
	$scope.update = function () {		
		dataFactory.update($scope.obj)
		$window.location.href = ctx + '/master2/organization-type-view';	
	};		
	
	$scope.cancel = function() {
		$window.location.href = ctx + '/master2/organization-type-view';	
	};

});


