var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';

	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-organization-type');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-organization-type/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-organization-type',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-organization-type',obj);
	    };
	    dataFactory.search = function (code) {
	        return httpProvider().get(urlBase+'/search-organization-type',code);
	    };

	    return dataFactory;
});


app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $http) {
	
	$scope.data2List ={};

	doSearch();
	
/*	dataFactory.findAll()
	.then(function (response) {
		$scope.data2List = response;
		console.log(response);
		//$scope.totalItems = $scope.data2List.length;
		$scope.totalItems = response.totalPages;
		$scope.currentPage = 1;
		$scope.itemsPerPage = 5;
		$scope.maxSize = 5;
		
	}, function (error) {
		$scope.status = 'Unable to load data: ' + error.message;
	});
*/

	$scope.doSearch = doSearch;	
	
	function doSearch() {
		var url  = ctx+'/master2/search-organization-type';
		var config = {
				params: { 
					'name' : $scope.name, 
//					'startDateFrom' : new Date($scope.startDateFrom).getTime(), 
//					'startDateTo' : new Date($scope.startDateTo).getTime()
					}
			}
		
		if ($scope.startDateFrom != null)
			config.params["startDateFrom"] = new Date($scope.startDateFrom).getTime();
		if ($scope.startDateTo != null)
			config.params["startDateTo"] = new Date($scope.startDateTo).getTime();
		if ($scope.endDateFrom != null)
			config.params["endDateFrom"] = new Date($scope.endDateFrom).getTime();
		if ($scope.endDateTo != null)
			config.params["endDateTo"] = new Date($scope.endDateTo).getTime();
		
		console.log(config);
		
		$http.get(url, config).then(function(response) {
			
			console.log(response);
			
			$scope.data2List = response.data;
			$scope.totalItems = response.data.length;
			//$scope.totalItems = response.data.totalPages;
			$scope.currentPage = 1;
			$scope.itemsPerPage = 10;
			$scope.maxSize = 5;
		}, function (error) {
			$scope.status = 'Unable to load data: ' + error.message;
		});		
	}
		
	$scope.clearData = function() {
		$scope.criteria ={};
	};

	$scope.goCreateNew = function () {
		$window.location.href = ctx + '/master2/organization-type-create';	
	};

});


