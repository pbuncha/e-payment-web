var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';

	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-province');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-province/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-province',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-province',obj);
	    };

	    return dataFactory;
});

app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $location,$route,$http) {
	
	$scope.obj ={};
			
	$scope.create = function () {		
		dataFactory.create($scope.obj)
		$window.location.href = ctx + '/master2/province-view';	
	};
	
	$scope.update = function () {		
		dataFactory.update($scope.obj)
		$window.location.href = ctx + '/master2/province-view';	
	};

	$scope.cancel = function() {
		$window.location.href = ctx + '/master2/province-view';	
	};

});


