var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';
		  
	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-standard-revernue');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-standard-revernue/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-standard-revernue',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-standard-revernue',obj);
	    };

	    return dataFactory;
});

app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $location,$route,$http) {
	
	$scope.obj ={};

	loadMoneySourceList();
	
	var id = $location.absUrl().split('/')[6];
	dataFactory.findOne(id)
	.then(function (response) {
		$scope.obj =response;
		
		if(response.startDate!=null){
			$scope.obj.startDate =  new Date(response.startDate);
		}
		if(response.endDate!=null){
			$scope.obj.endDate =  new Date(response.endDate);
		}		
		
	}, function (error) {
		$scope.status = 'Unable to load data: ' + error.message;
	});
			
	$scope.create = function () {		
		dataFactory.create($scope.obj)
		$window.location.href = ctx + '/master2/standard-revernue-view';	
	};
	
	$scope.update = function () {		
		dataFactory.update($scope.obj)
		$window.location.href = ctx + '/master2/standard-revernue-view';	
	};		
	
	$scope.cancel = function() {
		$window.location.href = ctx + '/master2/standard-revernue-view';	
	};

	function loadMoneySourceList() {
		var url  = ctx+'/master2/findall-money-source';		
		$http.get(url).then(function(response) {			
			console.log(response);	
			$scope.moneySourceList = response.data;
		}, function (error) {
			$scope.status = 'Unable to load data: ' + error.message;
		});	
	}	
	
});


