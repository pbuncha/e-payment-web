var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/master2';

	    var dataFactory = {};

	    dataFactory.findAll = function () {
	        return httpProvider().get(urlBase+'/findall-standard-revernue-gl');
	    };
	    dataFactory.findOne = function (id) {
	        return httpProvider().get(urlBase+'/findone-standard-revernue-gl/' + id);
	    };
	    dataFactory.create = function (obj) {
	        return  httpProvider().post(urlBase+'/create-standard-revernue-gl',obj);
	    };
	    dataFactory.update = function (obj) {
	        return  httpProvider().put(urlBase+'/update-standard-revernue-gl',obj);
	    };
	    dataFactory.search = function (obj) {
	        return httpProvider().post(urlBase+'/search-standard-revernue-gl',obj);
	    };
	    dataFactory.updateStatusCancel = function (id) {
	        return httpProvider().get(urlBase+'/cancel-standard-revernue-gl/' + id);
	    };

	    return dataFactory;
});


app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $http) {
	
	var $ctrl = this;

	$scope.criteria ={};
	$scope.data2List ={};

	doSearch();
	
	$scope.doSearch = doSearch;	
		
	function doSearch() {
		console.log($scope);
		dataFactory.search($scope.criteria).then(function (response) { 
			console.log(response);
			$scope.data2List = response;
			$scope.totalItems = $scope.data2List.length;
			$scope.currentPage = 1;
			$scope.itemsPerPage = 10;
			$scope.maxSize = 5;
		}, function (error) {
			$scope.status = 'Unable to load customer data: ' + error.message;
		});
	};	

	$scope.updateStatusCancel = function( id ){
		dataFactory.updateStatusCancel(id).then(function(){
			doSearch();
		});
	}
		
	$scope.clearData = function() {
		$scope.criteria ={};
	};

	$scope.goCreateNew = function () {
		$window.location.href = ctx + '/master2/standard-revernue-gl-create';	
	};

});


