var app = angular.module("mapPermProgramApp", ['httpProvider','ui.bootstrap','ngRoute','ngResource','ngMaterial','ngSanitize','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/admin/program';
	  var urlPermBase = ctx+'/admin/permission-group';
	  var urlUBase = ctx+'/admin/user-permission-group';
	    var dataFactory = {};

			dataFactory.getStatusList = function () {
                return  httpProvider().post(urlUBase+'/getAllStatus');
            };

            dataFactory.getProgramList = function (data) {
				return  httpProvider().post(urlBase+'/search',data);
			};
			dataFactory.getPermission = function (id) {
               return  httpProvider().get(urlPermBase+'/getPermission?id='+id);
            };

            dataFactory.save = function (permissionGroupId, data) {
                return  httpProvider().post(urlBase+'/save/'+ permissionGroupId,data);
            };

            dataFactory.getPermissionList = function (data) {
                return  httpProvider().post(urlPermBase+'/searchPermissionGroup',data);
            };
            
			dataFactory.delPerm = function (id) {
                return  httpProvider().post(urlBase+'/deletePermissionGroup/'+id);
            };


	    return dataFactory;
});

app.controller("mapPermProgramCtrl",function($scope,$rootScope,$log,$location, $mdDialog,dataFactory,$window){
	$scope.permissionList = [];
	$scope.data = {};
	$scope.search = {};

	dataFactory.getStatusList().then(function(response){
        $log.log(response);
        $scope.data.statusList = response;
    }).catch(function(response) {
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });

    dataFactory.getPermissionList({}).then(function(response){
        $log.log(response);
        $scope.data.permList = response;
    }).catch(function(response) {
        alert('Can not get Permission list.');
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });


    var defaultValue = {};

    $scope.getPerm = function(id){
        dataFactory.getPermission(id).then(function(response){
            $log.log(response);
            $scope.data.id = response.mPermissionGroupId;
            $scope.data.code = response.mPermissionGroupCode;
            $scope.data.name = response.mPermissionGroupName;
            $scope.data.status = response.status;
            $scope.data.desc = response.description;
            $scope.data.check = response.checkPermissionAll;
            
            /*
             * Clear mapping result table
             */
            $scope.data.programList = [];
            $scope.totalItems = 0;
            
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }


    $scope.clearData = function(){
        $scope.data.name2 = undefined;
        $scope.data.code2 = undefined;
        $scope.data.desc2 = undefined;
        $scope.data.status2 = undefined;
    }

	$scope.searchProgram = function(){
		if($scope.data.id == undefined )
			return;
		if(	$scope.data.programList.length > 0 )
			return;
			
			
        var data = {
	        permissionGroupId : $scope.data.id
        };

		/**
		 *  Default search all to client 
		 */
        dataFactory.getProgramList(data).then(function(response){
            $log.log(response);
            $scope.data.programList = response;
            $scope.totalItems = response.length;
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
	
	}
	


    $scope.save = function(permissionGroupId, data){
        dataFactory.save(permissionGroupId, data ).then(function(response){
          
            if(response){
                alert("Save data success");
                $window.location.reload();
            }else{
                alert("Save data failure");
                $window.location.reload();
            }
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }

    $scope.checkAll = function(all){
    	if($scope.data.programList==undefined || $scope.data.programList.length ==0){
    		return;
    	}
        var begin = (($scope.currentPage - 1) * $scope.perPages);
        var end = begin + $scope.perPages;
		console.log(begin);
		console.log(end);
        for(var i= begin; i<$scope.data.programList.length && i<end; i++ ){
        	$scope.data.programList[i].isSelected = all;
        }
    }

	$scope.perPages = 10;
	$scope.totalItems = 0;
	$scope.currentPage = 1;
	$scope.all = 'N';
	$scope.values = [10,15,20,25];
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}