var app = angular.module("permissionGroupApp", ['httpProvider','ui.bootstrap','ngRoute','ngResource','ngMaterial','ngSanitize','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/admin/program';
	  var urlPermBase = ctx+'/admin/permission-group';
	  var urlUBase = ctx+'/admin/user-permission-group';
	    var dataFactory = {};

			dataFactory.getStatusList = function () {
                return  httpProvider().post(urlUBase+'/getAllStatus');
            };

            dataFactory.getProgramList = function (data) {
				return  httpProvider().post(urlBase+'/search',data);
			};
			dataFactory.getPermission = function (id) {
               return  httpProvider().get(urlPermBase+'/getPermission?id='+id);
            };

            dataFactory.save = function (data) {
                return  httpProvider().post(urlPermBase+'/save',data);
            };

	    return dataFactory;
});

app.controller("permissionGroupCtrl",function($scope,$rootScope,$log,$location, $mdDialog,dataFactory){
	$log.log('permissionGroupCtrl');
	$scope.permissionList = [];
	$scope.data = {};
	$scope.search = {};

	dataFactory.getStatusList().then(function(response){
        $log.log(response);
        $scope.data.statusList = response;
    }).catch(function(response) {
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });

    $scope.edit = getParameterByName('id');
    console.log($scope.edit);

    var defaultValue = {};

    if($scope.edit){
        dataFactory.getPermission($scope.edit).then(function(response){
            $log.log(response);
            $scope.data.id = response.mPermissionGroupId;
            $scope.data.code = response.mPermissionGroupCode;
            $scope.data.name = response.mPermissionGroupName;
            $scope.data.status = response.status;
            $scope.data.desc = response.description;
            $scope.data.check = response.checkPermissionAll;

            defaultValue.code = response.mPermissionGroupCode;
            defaultValue.name = response.mPermissionGroupName;
            defaultValue.status = response.status;
            defaultValue.desc = response.description;
            defaultValue.check = response.checkPermissionAll;
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }




    $scope.clearData1 = function(){
        if(!$scope.edit){
            $scope.data.name = null;
            $scope.data.code = null;
            $scope.data.desc = null;
            $scope.data.status = null;
        }else{
            $scope.data.name = defaultValue.name;
            $scope.data.code = defaultValue.code;
            $scope.data.desc = defaultValue.desc;
            $scope.data.status = defaultValue.status;
        }
    }

    $scope.clearData = function(){
        $scope.data.name2 = null;
        $scope.data.code2 = null;
        $scope.data.desc2 = null;
        $scope.data.status2 = null;
    }

	$scope.searchProgram = function(){
    	        $scope.search.userId = $scope.data.userLogin;
    	        var data = {}
    	        data.programName = $scope.data.name2;
    	        data.programCode = $scope.data.code2;
    	        data.description = $scope.data.desc2;
    	        if($scope.data.status2){
    	            data.status = $scope.data.status2.status;
    	        }
    	        console.log(data);

                dataFactory.getProgramList(data).then(function(response){
                    $log.log(response);
                    $scope.data.programList = response;
                    $scope.totalItems = response.length;
                    $scope.figureOut();
                }).catch(function(response) {
                    console.error('data error', response);
                })
                .finally(function() {
                    console.log("finally finished");
                });

    	}
//	$scope.searchBilling();
//
	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.totalItems = 0;
	$scope.currentPage = 1;
//
	$scope.values = [10,15,20,25];

	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.data.programList.slice(begin, end);
	}


	$scope.pageChanged = function() {
		$scope.figureOut();
	  };

    $scope.save = function(){
        var data = {};
        data.mPermissionGroupId = $scope.data.id;
        data.mPermissionGroupCode = $scope.data.code;
        data.mPermissionGroupName = $scope.data.name;
        data.description = $scope.data.desc;
        data.status = $scope.data.status;
        data.checkPermissionAll = $scope.data.check;


        dataFactory.save(data).then(function(response){
            $log.log(response);
            window.location = ctx+'/admin/permission-group';
//            if(response){
//                alert("Save data success");
//                window.location = ctx+'/admin/permission-group';
//            }else{
//                alert("Save data failure");
//                window.reload();
//            }
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }



    var findIndex = function(id,list){
        for(var i = 0; i < list.length; i++){
            if(id == list[i].programId){
                return i;
            }
        }
        return -1;
    }

    $scope.toggleSelect = function(id){
        var idx = $scope.data.check.indexOf(id);

        // Is currently selected
        if (idx > -1) {
          $scope.data.check.splice(idx, 1);
        }

        // Is newly selected
        else {
          $scope.data.check.push(id);
        }
	}

	$scope.isCheckAll = function(){
        var flag = true;
        for(var i = 0 ; i < $scope.sliceFavoriteList.length ; i++){
            if($scope.data.check.indexOf($scope.sliceFavoriteList[i].programId) < 0){
                flag = false;
            }
        }
        return flag;
    }

    $scope.checkAll = function(all){
        for(var i = 0; i < $scope.sliceFavoriteList.length ; i++){
            if(all){
                $scope.data.check.push($scope.sliceFavoriteList[i].programId)
            }else{
                var idx = $scope.data.check.indexOf($scope.sliceFavoriteList[i].programId);
                console.log(idx);
                $scope.data.check.splice(idx,1)
            }

        }

    }


})

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}