var app = angular.module("permissionGroupListApp", ['httpProvider','ui.bootstrap','ngRoute','ngResource','ngMaterial','ngSanitize','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/admin/permission-group';
	  var urlPermBase = ctx+'/admin/user-permission-group';
	    var dataFactory = {};

			dataFactory.getStatusList = function () {
                return  httpProvider().post(urlPermBase+'/getAllStatus');
            };

            dataFactory.getPermissionList = function (data) {
				return  httpProvider().post(urlBase+'/searchPermissionGroup',data);
			};

			dataFactory.delPerm = function (id) {
                return  httpProvider().post(urlBase+'/deletePermissionGroup/'+id);
            };

	    return dataFactory;
});

app.controller("permissionGroupListCtrl",function($scope,$rootScope,$log,$location, $mdDialog,dataFactory){
	$log.log('permissionGroupListCtrl');
	$scope.permissionList = [];
	$scope.data = {};
	$scope.search = {};

	dataFactory.getStatusList().then(function(response){
        $log.log(response);
        $scope.data.statusList = response;
    }).catch(function(response) {
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });




    $scope.clearData = function(){
        $scope.data.name = null;
        $scope.data.code = null;
        $scope.data.desc = null;
        $scope.data.status = null;
    }

	$scope.searchProgram = function(){
    	        $scope.search.userId = $scope.data.userLogin;
    	        var data = {}
    	        data.mPermissionGroupid = $scope.data.id;
    	        data.permissionName = $scope.data.name;
    	        data.permissionCode = $scope.data.code;
    	        data.desc = $scope.data.desc;
    	        if($scope.data.status){
    	            data.status = $scope.data.status;
    	        }

                $log.log(data);
                dataFactory.getPermissionList(data).then(function(response){
                    $log.log($scope);
                    $log.log(response);
                    $scope.data.programList = response;
                    $scope.totalItems = response.length;
                    $scope.figureOut();
                }).catch(function(response) {
                    console.error('data error', response);
                })
                .finally(function() {
                    console.log("finally finished");
                });

    	}
//	$scope.searchBilling();
//
	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.totalItems = 0;
	$scope.currentPage = 1;
//
	$scope.values = [10,15,20,25];

	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.data.programList.slice(begin, end);
	}


	$scope.pageChanged = function() {
		$scope.figureOut();
	  };



    $scope.delPerm = function(id){
    	 $log.log(id);
        dataFactory.delPerm(id).then(function(response){
            $log.log(response);
            if(response){
            	
           	 successDialog = $mdDialog.alert({
			        title: 'จัดการกลุ่มสิทธิ์',
			        textContent: 'Delete Success',
			        ok: 'ตกลง'
			      });

			      $mdDialog
			        .show( successDialog )
			        .finally(function() {
			        	successDialog = undefined;
		                $scope.sliceFavoriteList.splice(findIndex(id,$scope.sliceFavoriteList),1);
		                $scope.data.programList.slice(findIndex(id,$scope.sliceFavoriteList),1);
			        });
            	
//                alert("Delete success");
//                $scope.sliceFavoriteList.splice(findIndex(id,$scope.sliceFavoriteList),1);
//                $scope.data.programList.slice(findIndex(id,$scope.sliceFavoriteList),1);
            }else{
            	
           	 successDialog = $mdDialog.alert({
			        title: 'จัดการกลุ่มสิทธิ์',
			        textContent: 'Delete Failure',
			        ok: 'ตกลง'
			      });

			      $mdDialog
			        .show( successDialog )
			        .finally(function() {
			        	successDialog = undefined;
//			        	 $window.location.reload();
			        });
//                alert("Delete failure");
            }
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }

    var findIndex = function(id,list){
        for(var i = 0; i < list.length; i++){
            if(id == list[i].mPermissionGroupId){
                return i;
            }
        }
        return -1;
    }



})