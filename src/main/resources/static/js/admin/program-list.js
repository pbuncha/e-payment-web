var app = angular.module("programApp", ['httpProvider','ui.bootstrap','ngRoute','ngResource','ngMaterial','ngSanitize','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/admin/program';
	  var urlPermBase = ctx+'/admin/user-permission-group';
	    var dataFactory = {};

			dataFactory.getStatusList = function () {
                return  httpProvider().post(urlPermBase+'/getAllStatus');
            };

            dataFactory.getProgramList = function (data) {
				return  httpProvider().post(urlBase+'/search',data);
			};

//			dataFactory.delProgram = function (id) {
//                return  httpProvider().post(urlBase+'/delete',id);
//            };
            
			dataFactory.delProgram = function (id) {
                return  httpProvider().post(urlBase+'/deleteProgram/'+id);
            };

	    return dataFactory;
});

app.controller("programCtrl",function($scope,$rootScope,$log,$location, $mdDialog,dataFactory){
	$log.log('programCtrl');
	$scope.permissionList = [];
	$scope.data = {};
	$scope.search = {};

	dataFactory.getStatusList().then(function(response){
        $log.log(response);
        $scope.data.statusList = response;
    }).catch(function(response) {
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });




    $scope.clearData = function(){
        $scope.data.name = null;
        $scope.data.code = null;
        $scope.data.desc = null;
        $scope.data.status = null;
    }

	$scope.searchProgram = function(){
    	        $scope.search.userId = $scope.data.userLogin;
    	        var data = {}
    	        data.programName = $scope.data.name;
    	        data.programCode = $scope.data.code;
    	        data.description = $scope.data.desc;
    	        if($scope.data.status){
    	            data.status = $scope.data.status;
    	        }

                dataFactory.getProgramList(data).then(function(response){
                    $log.log(response);
                    $scope.data.programList = response;
                    $scope.totalItems = response.length;
                    $scope.figureOut();
                }).catch(function(response) {
                    console.error('data error', response);
                })
                .finally(function() {
                    console.log("finally finished");
                });

    	}
	$scope.searchProgram();
	
//	$scope.searchBilling();
//
	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.totalItems = 0;
	$scope.currentPage = 1;
//
	$scope.values = [10,15,20,25];

	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.data.programList.slice(begin, end);
	}


	$scope.pageChanged = function() {
		$scope.figureOut();
	  };

    $scope.updatePerms = function(){
        dataFactory.updateUserPermList($scope.search.userId,$scope.data.check).then(function(response){
            $log.log(response);
            if(response){
                alert("Update success");
            }else{
                alert("Update failure");
            }
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }

    $scope.delPerm = function(id){
        $log.log(id);
        dataFactory.delProgram(id).then(function(response){
            $log.log(response);
            if(response){
            	
           	 successDialog = $mdDialog.alert({
			        title: 'บริหารจัดการโปรแกรม',
			        textContent: 'Delete Success',
			        ok: 'ตกลง'
			      });

			      $mdDialog
			        .show( successDialog )
			        .finally(function() {
			        	successDialog = undefined;
		                $scope.sliceFavoriteList.splice(findIndex(id,$scope.sliceFavoriteList),1);
		                $scope.data.programList.slice(findIndex(id,$scope.sliceFavoriteList),1);
			        });
         	
	

            }else{
               
                
           	 successDialog = $mdDialog.alert({
			        title: 'บริหารจัดการโปรแกรม',
			        textContent: 'Delete failure',
			        ok: 'ตกลง'
			      });

			      $mdDialog
			        .show( successDialog )
			        .finally(function() {
			        	successDialog = undefined;
			        	 console.error('data error', response);
			        });
            }
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }

    var findIndex = function(id,list){
        for(var i = 0; i < list.length; i++){
            if(id == list[i].mPermissionGroupId){
                return i;
            }
        }
        return -1;
    }



})