var app = angular.module("programSaveApp", ['httpProvider','ui.bootstrap','ngRoute','ngResource','ngMaterial','ngSanitize','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/admin/program';
	  var urlPermBase = ctx+'/admin/user-permission-group';
	    var dataFactory = {};

            dataFactory.getProgram = function (id) {
                return  httpProvider().get(urlBase+'/getProgram?id='+id);
            };

            dataFactory.save = function (data) {
                return  httpProvider().post(urlBase+'/save',data);
            };

			dataFactory.getStatusList = function () {
                return  httpProvider().post(urlPermBase+'/getAllStatus');
            };


	    return dataFactory;
});

app.controller("programSaveCtrl",function($scope,$rootScope,$log,$location, $mdDialog,dataFactory){
	$log.log('programSaveCtrl');
	$scope.data = {};
	$scope.search = {};
    $scope.edit = getParameterByName('id');
    console.log($scope.edit);

	dataFactory.getStatusList().then(function(response){
        $log.log(response);
        $scope.data.statusList = response;
    }).catch(function(response) {
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });

    var defaultValue = {};

    if($scope.edit){
        dataFactory.getProgram($scope.edit).then(function(response){
            $log.log(response);
            $scope.data.id = response.programId;
            $scope.data.code = response.programCode;
            $scope.data.name = response.programName;
            $scope.data.status = response.status;
            $scope.data.desc = response.description;
            defaultValue.code = response.programCode;
            defaultValue.name = response.programName;
            defaultValue.status = response.status;
            defaultValue.desc = response.description;
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }





    $scope.clearData = function(){
        if(!$scope.edit){
            $scope.data.name = null;
            $scope.data.code = null;
            $scope.data.desc = null;
            $scope.data.status = null;
        }else{
            $scope.data.name = defaultValue.name;
            $scope.data.code = defaultValue.code;
            $scope.data.desc = defaultValue.desc;
            $scope.data.status = defaultValue.status;
        }
    }



    $scope.save = function(){
        var data = {};
        data.programId = $scope.data.id;
        data.programCode = $scope.data.code;
        data.programName = $scope.data.name;
        data.description = $scope.data.desc;
        data.status = $scope.data.status;
        console.log(data);
        dataFactory.save(data).then(function(response){
            $log.log(response);

            window.location = ctx+'/admin/program'

//            if(response){
//                alert("Save data success");
//                window.location = ctx+'/admin/program'
//            }else{
//                alert("Save data failure");
//            }
            
        }).catch(function(response) {
            console.error('data error', response);
//             alert("Save data failure");
        })
        .finally(function() {
            console.log("finally finished");
        });
    }




})


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}