var app = angular.module("master2App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp','ngMaterial']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/admin';

	    var dataFactory = {};

	    dataFactory.search = function (obj) {
	        return httpProvider().post(urlBase+'/search-user-permission-group',obj);
	    };
	    dataFactory.create = function (obj) {
	        return httpProvider().post(urlBase+'/create-user-permission-group',obj);
	    };	    
	    dataFactory.getUserLoginDetails = function (id) {
	        return httpProvider().get(urlBase+'/getUserLoginDetails/' + id);
	    };
	    
	    return dataFactory;
});
app.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;

          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
 }]);

app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl){
       var fd = new FormData();
       fd.append('file', file);

       return $http.post(uploadUrl, fd, {
          transformRequest: angular.identity,
          headers: {'Content-Type': undefined}
       });

    }}]);

app.controller("master2Ctrl", function ($log, $scope, dataFactory, $window, $http, $uibModal,fileUpload,$mdDialog) {
	
	if(loginUserId != null && loginUserId != ""){
		console.log("loginUserId : ",loginUserId);
		$scope.loginUserId = loginUserId;
	}	
	
	var $ctrl = this;

	$scope.criteria ={};
	$scope.data2List ={};
	$scope.selectItemAdd = [];
	$scope.selectItemRemove = [];
	$scope.list = []; 
//	$scope.AttachFile = "Test.pdf";

	getUserInfo();
	doSearch();
		
	$scope.doSearch = doSearch;	
	$scope.getUserInfo = getUserInfo;	
	$scope.save = save;	

	function doSearch() {
		console.log($scope);
		dataFactory.search($scope.criteria).then(function (response) { 
			console.log(response);
			$scope.data2List = response;
			$scope.totalItems = $scope.data2List.length;
			$scope.currentPage = 1;
			$scope.itemsPerPage = 10;
			$scope.maxSize = 5;
		}, function (error) {
			$scope.status = 'Unable to load customer data: ' + error.message;
		});
	};	

	
	function getUserInfo() {
		console.log("getUserInfo");
		console.log($scope);
		dataFactory.getUserLoginDetails($scope.loginUserId).then(function (response) { 
			console.log(response);
			$scope.data = response;
		}, function (error) {
			$scope.status = 'Unable to load customer data: ' + error.message;
		});
	};	

	$scope.clearData = function() {
		$scope.criteria ={};
		$scope.selectItemAdd = [];
		$scope.selectItemRemove = [];
		
		getUserInfo();
		doSearch();
	};
		
	$scope.reloadPage = function () {
		$window.location.href = ctx + '/admin/request-user-permission-group';	
	};

	$scope.selectToAdd = function( item ){
				
		var index = $scope.isExistItemRemove(item.mPermissionGroupId);
		if(index != -1 ){
			$scope.selectItemRemove.splice(index,1);
		}		

		var index = $scope.isExistItemAdd(item.mPermissionGroupId);
		if(index == -1 ){
			$scope.selectItemAdd.push(item);
		}		
				
		console.log("selectItemAdd");
		console.log($scope.selectItemAdd);
		console.log("selectItemRemove");
		console.log($scope.selectItemRemove);
	}

	$scope.selectToRemove = function( item ){

		var index = $scope.isExistItemAdd(item.mPermissionGroupId);
		if(index != -1 ){
			$scope.selectItemAdd.splice(index,1);
		}		

		var index = $scope.isExistItemRemove(item.mPermissionGroupId);
		if(index == -1 ){
			$scope.selectItemRemove.push(item);
		}		

		console.log("selectItemAdd");
		console.log($scope.selectItemAdd);
		console.log("selectItemRemove");
		console.log($scope.selectItemRemove);
	}

	$scope.isExistItemAdd = function( id ){
		for( var i =0 ; i<$scope.selectItemAdd.length; i++ ){
			if( $scope.selectItemAdd[i].mPermissionGroupId == id ){
				return i;
			}
		}
		return -1;
	}	

	$scope.isExistItemRemove = function( id ){
		for( var i =0 ; i<$scope.selectItemRemove.length; i++ ){
			if( $scope.selectItemRemove[i].mPermissionGroupId == id ){
				return i;
			}
		}
		return -1;
	}	
	
	function save() {		
		console.log("save");
		
		$scope.list = []; 
		 var file = $scope.data.ufile;
		   if(file){
	            var type = ['pdf', 'docx', 'dox']
	           if(type.includes(file.name.split('.')[1])){

	                var urlBase = ctx+'/admin';
	                var size = file.size;
	                size = size / 1048576;
	             
	                if(size>2){
	                	  showAlert('ขนาดเกิน 2 MB');
	                	  return;
	                }
	           
	               fileUpload.uploadFileToUrl(file, urlBase+'/upload')
	               .then(function(response) {
	                   console.log(response)
	                   $scope.AttachFile = response.data.path;

	               }).catch(function(error){
	                      showAlert(error)
	                      $scope.data.uploadErr = error.data;
	              }).finally(function() {
	                    console.log("finally finished");
	                });
	           }else{
	            showAlert('ประเภทของไฟล์ไม่ถูกต้อง');
	            $scope.data.uploadErr = 'File type not support.';
	           }
	       }else{
	        showAlert('กรุณาเลือกไฟล์');
	       }
		for( var i =0 ; i<$scope.selectItemAdd.length; i++ ){
			$scope.list.push({ 
				userId: $scope.loginUserId, 
				mPermissionGroupId: $scope.selectItemAdd[i].mPermissionGroupId, 
				requestType: 1,
				status: 0,
				attachFile: $scope.AttachFile
			});
		}
		for( var i =0 ; i<$scope.selectItemRemove.length; i++ ){
			$scope.list.push({ 
				userId: $scope.loginUserId, 
				mPermissionGroupId: $scope.selectItemRemove[i].mPermissionGroupId, 
				requestType: 0,
				status: 0,
				attachFile: $scope.AttachFile
			});
		}
				  		   
//		for( var i =0 ; i<$scope.list.length; i++ ){
//				dataFactory.create($scope.list[i]).then(function (response) {
//					console.log(response);
//				}).catch(function(response) {
//					console.error('data error', response);
//				})
//				.finally(function() {
//					console.log("finally finished post.");
//				});
//		}

		
		dataFactory.create($scope.list).then(function (response) {
			console.log(response);
			$scope.reloadPage();
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished post.");
		});
		
		console.log("cleardata done.");
	};
	
    $scope.$watch( 'data.ufile', function(file,o){
  	  console.log('file : ', file);
  	
  	  if(file != undefined){
  		  console.log('file : ', file.name);
  		   var typeFile = file.name.split('.');
  	 	   var type = ['pdf', 'docx', 'dox']
  	 	      if(!type.includes(typeFile[1])){
  	 	    	  showAlert("ประเภทของไฟล์ไม่ถูกต้อง");
  	 	    	  return true;
  	 	      }else{
  	 	    	$scope.fileNane = file.name;
  	 	      }
  	  }


  });
	
    $scope.checkTypeFile = function (file) {

 	   var typeFile = file.split('.');
 	   var type = ['pdf', 'docx', 'dox']
 	      if(!type.includes(typeFile[1])){
 	    	  showAlert("ประเภทของไฟล์ไม่ถูกต้อง");
 	    	  $('#file' ).val('');
 	    	 $scope.data.ufile = "";
 	    	 $scope.data.ufile ={};
 	    	  return true;
 	      }
 	
 }

	function showAlert (textContent) {
		$mdDialog.show(
				$mdDialog.alert()
    		        .clickOutsideToClose(true)
    		        .title('ข้อผิดพลาด')
    		        .textContent(textContent)
    		        .ok('OK')
    		    );
    }
});


