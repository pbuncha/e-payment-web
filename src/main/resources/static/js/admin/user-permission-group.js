var app = angular.module("userPermissionGroupApp", ['httpProvider','ui.bootstrap','ngRoute','ngResource','ngMaterial','ngSanitize','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/admin/user-permission-group';
	  var permissionBase = ctx+'/admin/permission-group'
	    var dataFactory = {};
			dataFactory.searchUserCenter = function (data) {
		        return  httpProvider().post(urlBase+'/getUserByCitizen',data);
		    };
		    dataFactory.searchUserLogin = function (data) {
                return  httpProvider().post(urlBase+'/getUserByLogin',data);
            };
			dataFactory.getPermList = function (data) {
				return  httpProvider().post(permissionBase+'/searchPermissionGroup',data);
			};
			dataFactory.getStatusList = function () {
                return  httpProvider().post(urlBase+'/getAllStatus');
            };
            dataFactory.getUserPermList = function (data) {
                return  httpProvider().post(urlBase+'/getUserPermissionGroup',data);
            };

            dataFactory.updateUserPermList = function (id,data) {
                return  httpProvider().post(urlBase+'/updateUserPermissionGroup/'+id,data);
            };

            dataFactory.delPerm = function (data) {
                return  httpProvider().post(permissionBase+'/delete/',data);
            };


	    return dataFactory;
});

app.controller("userPermissionGroupCtrl",function($scope,$rootScope,$log,$location, $mdDialog,dataFactory,$window){
	$log.log('userPermissionGroupCtrl');
	$scope.permissionList = [];
	$scope.data = {};
	$scope.search = {};
	$scope.data.user={};
	
	dataFactory.getStatusList().then(function(response){
        $log.log(response);
        $scope.data.statusList = response;
    }).catch(function(response) {
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });

	$scope.searchUser = function(){
	    if($scope.data.citizen){
            $log.log("data tor : ",$scope.data.citizen);
            dataFactory.searchUserLogin($scope.data.citizen).then(function(response){
                $log.log("User : ",response);
                $scope.data.user = {};
                $scope.data.user.id = response.userId;
                $scope.data.user.name = response.fnameTH;
                $scope.data.user.lname = response.lnameTH;
                console.log("response.perms : ", response.perms);
                $scope.data.check = response.perms;
                console.log(response);
                $scope.clearData();
            }).catch(function(response) {
                alert('User not exist.');
                console.error('data error', response);
            })
            .finally(function() {
                console.log("finally finished");
            });
	    }
	}





	$scope.toggleSelect = function(id){
        var idx = $scope.data.check.indexOf(id);

        // Is currently selected
        if (idx > -1) {
          $scope.data.check.splice(idx, 1);
        }

        // Is newly selected
        else {
          $scope.data.check.push(id);
        }
	}

    $scope.clearData = function(){
        $scope.data.name2 = null;
        $scope.data.status2 = null;
        $scope.data.citizen = null;
        $scope.data.user.name = null;
    }

	$scope.searchPermissionGroup = function(){
	        console.log($scope.data.user.id);
    	    if($scope.data.user.id){
    	        $scope.search.userId = $scope.data.user.id;
    	        var data = {}
    	        data.permissionName = $scope.data.name;
    	        data.permissionCode = $scope.data.code;
    	        data.desc = $scope.data.desc;
    	        if($scope.data.status){
    	            data.status = $scope.data.status.status;
    	        }

                dataFactory.getPermList(data).then(function(response){
                    $log.log(response);
                    $scope.data.permissionList = response;
                    $scope.totalItems = response.length;
                    $scope.figureOut();
                }).catch(function(response) {
                    console.error('data error', response);
                })
                .finally(function() {
                    console.log("finally finished");
                });
                console.log($scope.data.userLogin);

    	    }
    	}
//	$scope.searchBilling();
//
	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.totalItems = 0;
	$scope.currentPage = 1;
//
	$scope.values = [10,15,20,25];

	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.data.permissionList.slice(begin, end);
	}
//	$scope.all = false;
	 $scope.data.check = [];
	$scope.checkAll = function(all){
		 console.log(" XXXXXXXXXXXX CheckAll data : ", $scope.data.check);
		 console.log("CheckAll : ",all);
        for(var i = 0; i < $scope.sliceFavoriteList.length ; i++){
            if(all){
            	   console.log("CheckAll data : ",$scope.sliceFavoriteList[i].mPermissionGroupId);
                $scope.data.check.push($scope.sliceFavoriteList[i].mPermissionGroupId)
            }else{
//                var idx = $scope.data.check.indexOf($scope.sliceFavoriteList[i].mPermissionGroupId);
//                console.log(idx);
//                $scope.data.check.splice(idx,1);
            	   console.log("CheckAll data : ", $scope.data.check);
                $scope.data.check = [];
                console.log("CheckAll : ", $scope.data.check);
            }

        }
        
        console.log("CheckAll : ",all);

	}

    $scope.isCheckAll = function(){
        var flag = true;
        console.log("isCheckAll data.check1 : ",$scope.data.check);
        console.log("sliceFavoriteList.check1 : ",$scope.sliceFavoriteList.length);
        console.log("$scope.all : ",$scope.all);
        if( $scope.data.check.length == 0 || $scope.sliceFavoriteList.length ==0){
        	flag = false;
        }
//        if(!$scope.data.check){
//            $scope.data.check = [];
//        }
       
//        console.log("sliceFavoriteList.length1 : ",$scope.sliceFavoriteList.length);
//        for(var i = 0 ; i < $scope.sliceFavoriteList.length ; i++){
//            if($scope.data.check.indexOf($scope.sliceFavoriteList[i].mPermissionGroupId) < 0){
//                flag = false;
//            }
//        }
        console.log("flag : ",flag);
        return flag;
    }

	$scope.pageChanged = function() {
		$scope.figureOut();
	  };
	  
	
		
    $scope.updatePerms = function(){
   	 var successDialog;
        dataFactory.updateUserPermList($scope.search.userId,$scope.data.check).then(function(response){
            $log.log(response);
            if(response){
           
			 successDialog = $mdDialog.alert({
			        title: 'จัดการสิทธิ์ผู้ใช้งาน',
			        textContent: 'Update Success',
			        ok: 'ตกลง'
			      });

			      $mdDialog
			        .show( successDialog )
			        .finally(function() {
			        	successDialog = undefined;
			        	 $window.location.reload();
			        });
//                alert("Update success");
//                $window.location.reload();
            }else{
            	
   			 successDialog = $mdDialog.alert({
			        title: 'จัดการสิทธิ์ผู้ใช้งาน',
			        textContent: 'Update Failure',
			        ok: 'ตกลง'
			      });

			      $mdDialog
			        .show( successDialog )
			        .finally(function() {
			        	successDialog = undefined;
			        	 $window.location.reload();
			        });
//                alert("Update failure");
//                $window.location.reload();
            }
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }

    $scope.delPerm = function(id){
        dataFactory.delPerm(id).then(function(response){
            $log.log(response);
            if(response){
                alert("Delete success");
                $scope.sliceFavoriteList.splice(findIndex(id,$scope.sliceFavoriteList),1);
                $scope.data.permissionList.slice(findIndex(id,$scope.sliceFavoriteList),1);
            }else{
                alert("Delete failure");
            }
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
    }

    var findIndex = function(id,list){
        for(var i = 0; i < list.length; i++){
            if(id == list[i].mPermissionGroupId){
                return i;
            }
        }
        return -1;
    }



})