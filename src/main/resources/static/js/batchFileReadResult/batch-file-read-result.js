var app = angular.module("ctl10000SearchApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	var urlBase = ctx+'/batchFileReadResult';
	var dataFactory = {};
	
    dataFactory.getMCostCenterList = function () {
        return  httpProvider().get(urlBase+'/getMCostCenterList');
    };

    dataFactory.getdeptList = function () {
		return  httpProvider().get(ctx+'/manageInvoice/getdeptList');
	};

    dataFactory.getBank = function () {
        return  httpProvider().get(urlBase+'/getBank');
    };

    dataFactory.getBatchFileReadResultByCriteria = function (data) {
        return  httpProvider().post(urlBase+'/search-batch-file-read-result', data);
    };
    
	return dataFactory;
});

app.controller("ctl10000SearchCtrl", function ($log,$scope,dataFactory) {
	
	dataFactory.getBank().then(function (response) {
		console.log(response);
		$scope.bankList = response;
	});
	
	$scope.data2List ={};
	$scope.criteria ={};
	
	$scope.search = function() {
		
		$log.log($scope);		
		
		dataFactory.getBatchFileReadResultByCriteria($scope.criteria).then(function (response) {
//		dataFactory.getAllBatchFileReadResult().then(function (response) {
			
			$log.log("start loading 1");
			$log.log(response);
			$log.log("start loading 2");
			
			$scope.data2List = response;
			$log.log(response.totalPages);

			$scope.totalItems = $scope.data2List.length;
			$scope.figureOut();

			$log.log("start loading 9");
												
		}, function (error) {
			$scope.status = 'Unable to load customer data: ' + error.message;
		});
	};	
	$scope.search();
	
	// Paging
	$scope.sliceData2List = [];
	$scope.perPages = 10;
	$scope.totalItems = $scope.data2List.length;
	$scope.currentPage = 1;
	
	$scope.values = [10,15,20,25];
	
	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceData2List = $scope.data2List.slice(begin, end);
	}
	
	$scope.pageChanged = function() {
		$scope.figureOut();
	};
	
	
});