$(document).ready(function() {
	
//	initCaseReport();
	
});

function initCaseReport() {
	$.ajax({
		  type: "GET",
		  url: ctx + 'casereport',
		  skipLoading: true,
		  success: function(response) {
			  $("#caseReportSection").html(response);
			  initFunction();
		  }
	});
}

function initFunction() {
	$('#case-report-save-btn').on('click', function() {
		save();
	});
	$('#editCaseReport').on('click', function() {
		$(this).hide();
		showHideEdit();
	});
	
	showHideEdit();
	
}

function showHideEdit() {
	if ($('#editCaseReport').is(':visible')) {
		$('#caseReportForm input[type=radio]').prop('disabled', true);
		$('#case-report-save-section').hide();
		$('#case-report-time-save').show();
	} else {
		$('#caseReportForm input[type=radio]').prop('disabled', false);
		$('#case-report-save-section').show();
		$('#case-report-time-save').hide();
	}
}

function save() {
	if ($('#caseReportForm').valid()) {
		$.ajax({
			type: "POST",
			url: ctx + 'casereport/save',
			data: $('#caseReportForm').serialize(),
			success: function(response) {
				if (response) {
					initCaseReport();
				}
			}
		});
	}
}