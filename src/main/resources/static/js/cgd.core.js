/**
 * 
 */

(function($) {
	$.ajaxSetup({
		cache: false,
		//timeout: 15000,
		statusCode: {
			400: function() {
				$('#error400').show();
			},
			404: function() {
				//showError("404 - File not found.");
			},
			440 :function() {
				console.log("redirect");
			},
			500:function(){
				//messageError();
			}
			
		},
		beforeSend: function(jqXHR, setting) {
			//hideError();	
//			console.log("beforeSend");
			
			if (!setting.skipLoading || setting.skipLoading == undefined) {
				showLoading();
			}
			
			$('#error400').hide();
			
			var success = setting.success;
			
			setting.success = function(data, textStatus, jqXHR) {;
				if (redirectLogin(jqXHR)) {
					return;
				}
				if (success) {
					success.apply(this,arguments);
				}
			};
			
			var error = setting.error;
			
			setting.error = function(jqXHR, textStatus, errText) {
				if (redirectLogin(jqXHR)) {
					return;
				}
				if (error) {
					error.apply(this,arguments);
				}
			};
			function redirectLogin(obj){
				if (obj.responseText && obj.responseText.indexOf('class="login"') >= 0) {
				   window.location.reload();
				   return true;
				}
				return false;
			}
		},
		complete: function() {
			hideLoading();
			//messageError();		
		},
		error: function (request, status, error) {
		    //alert(request.responseText);
			console.log(status);
		    if (status == "timeout"){
		    	showError("Request timeout.");
			}else if(status==500){
				if(request.responseText){
					try{
						showError(decodeURI(request.responseText.replace(/\+/g, '%20')));
					}catch(e){
						showError(error);
					}
					
				}else{
					showError(error);
				}
				
			}else if(request.status==500){
				messageError();
			}
			else{
				console.log("error status "+status);
//				console.log("error message "+request.responseText);
				console.log("error error "+error);
				if(request.responseText){
					try{
						showError(decodeURI(request.responseText.replace(/\+/g, '%20')));
					}catch(e){
						showError(error);
					}
					
				}else{
					showError(error);
				}
			}
		}
	});
})(jQuery);

function showError(msg) {
	var errorBox=$("#errorBox");
	if(errorBox==null || errorBox==undefined || errorBox.size()==0){
		errorBox=$('<div id="errorBox"></div>');
	}
	errorBox.empty();
	errorBox.html(msg);
}

function showLoading() {
	$("#loadingBox").loading().show();
}

function hideLoading() {
	$("#loadingBox").hide();
}

function goBack() {
	window.history.back();
}