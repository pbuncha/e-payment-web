(function($) {
	var loading = function() {
		if ($(this).is(":empty")) {
//			console.log("..empty..");
			$(this).hide();
			var lightbox = '<div id="cgdLightbox">';
			lightbox += '<div class="loading"></div>';
			lightbox += 'กรุณารอสักครู่';
			lightbox += '</div>';
			lightbox += '<div id="cgdUnderlay"></div>';
			$(this).html(lightbox);
		}
		
		$(this).hide();
		
		var res = {
			scope: this,
			show: function() {
				this.scope.show();
			},
			hide: function() {
				this.scope.hide();
			}
		};
		return res;
	};
	
	$.fn.loading = loading;
})(jQuery);