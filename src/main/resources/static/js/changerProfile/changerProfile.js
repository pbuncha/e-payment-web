/**
 * 
 */
var app = angular.module("changerProfileApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp','ngMaterial','BotDetectCaptcha']);


app.config(function(captchaSettingsProvider,$compileProvider) {

	  captchaSettingsProvider.setSettings({
	    captchaEndpoint: ctx+'/botdetectcaptcha'
	  });
	  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|data):/);
	});

app.factory("dataFactory", function (httpProvider) {
	var urlBase = ctx+'/changerProfile';


	var dataFactory = {};

	dataFactory.getUserProfile = function (id) {
        return httpProvider().get(urlBase+'/getUserProfile');
    };
    dataFactory.getPermissionGroupLits = function () {
		return  httpProvider().post(urlBase+'/getUserPermissionGroup');
	};
	
	dataFactory.getOrganizationType = function () {
        return httpProvider().get(urlBase+'/getOrganizationType');
    };
    dataFactory.getOrganizationSubType = function (id) {
        return httpProvider().get(urlBase+'/getOrganizationSubType/'+id);
    };
	
	dataFactory.getDepartment = function (id) {
        return httpProvider().get(urlBase+'/getDepartment/'+id);
    };
	dataFactory.getBusinessArea = function (id) {
        return httpProvider().get(urlBase+'/getBusinessArea/'+id);
    };
	
	dataFactory.getCostCenter = function (id1,id2) {
        return httpProvider().get(urlBase+'/getCostCenter/'+id1+'/'+id2);
    };
    dataFactory.getProvince = function () {
        return httpProvider().get(urlBase+'/getProvince/');
    };
	
	dataFactory.getAmphur = function (id) {
        return httpProvider().get(urlBase+'/getAmphur/'+id);
    };
	
	dataFactory.getTambon = function (id) {
        return httpProvider().get(urlBase+'/getTambon/'+id);
    };
    dataFactory.updateProfile = function (userBean,reCaptcha) {
	    return  httpProvider().post(urlBase+'/updateProfile/'+reCaptcha,userBean);
	};
	return dataFactory;
});



app.controller("changerProfileCtrl", function ($scope, dataFactory,$http,$mdDialog,Captcha) {
	$scope.userBean = {};

	
	
	dataFactory.getProvince().then(function (response) {
		console.log('In getProvince');
		$scope.dataProvince = response;
	}).catch(function(response) {
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally finished");
	});


	dataFactory.getOrganizationType().then(function (response) {
		console.log('In getOrganizationType');
		console.log(response);
		$scope.data1List = response;
		//$scope.orgType = $scope.data1List[0];
	}).catch(function(response) {
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally finished");
	});
	
	
	
	dataFactory.getUserProfile().then(function (response) {
	    $scope.userBean = response;
	    $scope.getAmphur($scope.userBean.provinceId);
	    $scope.getTambon($scope.userBean.amphurId);
	    $scope.getOrganizationSubType($scope.userBean.orgType.id)
	    $scope.getDepartment($scope.userBean.orgSubType.id)
	    $scope.getBusinessArea($scope.userBean.dept.id)
	    $scope.getCostCenter($scope.userBean.dept.id,$scope.userBean.bizArea.id)
	    $scope.getAmphurGovAddre($scope.userBean.govAddreProvinceId);
	    $scope.getTambonGovAddre($scope.userBean.govAddreAmphurId);
	    console.log('userBean ' , $scope.userBean);
	}).catch(function(response) {
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally getUserProfile");
	});
		
		
		$scope.getAmphur = function (id) {
			if(undefined==id){
				id = 0;
			}
			dataFactory.getAmphur(id).then(function (response) {
				$scope.dataAmphurList = response;
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
		};
		
		$scope.getTambon = function (id) {
			if(undefined==id){
				id = 0;
			}
			dataFactory.getTambon(id).then(function (response) {
				$scope.dataTambon = response;
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
		};
		
		$scope.getAmphurGovAddre = function (id) {
			console.log(id);
//			$scope.userBean.provinceId = id;
			if(undefined==id){
				id = 0;
			}
			dataFactory.getAmphur(id).then(function (response) {
				console.log(response);
				$scope.dataAmphurGov = response;
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
		};
		
		$scope.getTambonGovAddre = function (id) {
			console.log(id);
			if(undefined==id){
				id = 0;
			}
			dataFactory.getTambon(id).then(function (response) {
				console.log(response);
				$scope.dataTambonGov = response;
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
		};
		
		
		$scope.getBusinessArea = function(id){
			if(undefined == id){
				id = 0;
			}
		    dataFactory.getBusinessArea(id).then(function (response) {
	    		console.log('In getBusinessArea');
	    		console.log(response);
	    		$scope.dataBizArea = response;
	    		//$scope.orgType = $scope.data1List[0];
	    	}).catch(function(response) {
	    		console.error('data error', response);
	    	})
	    	.finally(function() {
	    		console.log("finally finished");
	    	});
		    

		}
		

        dataFactory.getPermissionGroupLits().then(function(response){
            $scope.permissionGroupList = response;
        }).catch(function(response) {
            console.error('data error', response);
        })
        .finally(function() {
            console.log("finally finished");
        });
        
        $scope.getOrganizationSubType = function (id) {
        	alert("");
    		if(undefined == id){
    			id = 0;
    		}
    		
    		dataFactory.getOrganizationSubType(id).then(function (response) {
    			console.log("id " , id);
    			console.log("getOrganizationSubType" , response);
    			$scope.dataOrgSub = response;
    		}).catch(function(response) {
    			console.error('data error', response);
    		})
    		.finally(function() {
    			console.log("finally finished");
    		});
    	};

    	$scope.getDepartment = function (id) {
    		console.log(id);
    		if(undefined == id){
    			id = 0;
    		}
    		dataFactory.getDepartment(id).then(function (response) {
    			console.log(response);
    			$scope.dataDept = response;
    		}).catch(function(response) {
    			console.error('data error', response);
    		})
    		.finally(function() {
    			console.log("finally finished");
    		});

    		
    	};
    	
    	$scope.getBusinessArea = function(id){
    		if(undefined == id){
    			id = 0;
    		}
    	    dataFactory.getBusinessArea(id).then(function (response) {
        		console.log('In getBusinessArea');
        		console.log(response);
        		$scope.dataBizArea = response;
        		//$scope.orgType = $scope.data1List[0];
        	}).catch(function(response) {
        		console.error('data error', response);
        	})
        	.finally(function() {
        		console.log("finally finished");
        	});
    	    

    	}

    	
    	$scope.getOrganizationSubType = function (id) {
    		console.log(id);
    		if(undefined == id){
    			id = 0;
    		}
    		dataFactory.getOrganizationSubType(id).then(function (response) {
    			console.log(response);
    			$scope.dataOrgSub = response;
    		}).catch(function(response) {
    			console.error('data error', response);
    		})
    		.finally(function() {
    			console.log("finally finished");
    		});
    	};
    	
    	$scope.getCostCenter = function (depId,bizId) {
    		if(undefined == depId){
    			depId = 0;
    		}
    		if(undefined == bizId){
    			bizId = 0;
    		}
    		console.log(depId);
    		console.log(bizId);

    		dataFactory.getCostCenter(depId,bizId).then(function (response) {
    			console.log(response);
    			$scope.dataCostCenter = response;
    		}).catch(function(response) {
    			console.error('data error', response);
    		})
    		.finally(function() {
    			console.log("finally finished");
    		});
    	};	
    	
    	
    	$scope.updateUserGoverment = function () {
//    	function updateUserGoverment(){

            var captcha = new Captcha();

            var grecaptcha = captcha.captchaId;

            var captchaCode = $scope.userBean.captcha;

            var postData = {
              captchaId: grecaptcha,
              captchaCode: captchaCode
            };

            console.log(postData);

    	
    	
    		 if($scope.userBean.email == undefined || $scope.userBean.email==""){
                 showAlert('email invalid format');
                 return;
             }


    		dataFactory.updateProfile($scope.userBean,grecaptcha).then(function (response) {
    			console.log(response);
    			if(response == 'success'){
    				 var successDialog;
    				 successDialog = $mdDialog.alert({
    				        title: 'ระบบ',
    				        textContent: 'เปลี่ยนแปลงข้อมูลสำเร็จ',
    				        ok: 'ตกลง'
    				      });

    				      $mdDialog
    				        .show( successDialog )
    				        .finally(function() {
    				        	successDialog = undefined;
    				        	  window.location = ctx;
    				        });
    			  
    			}else{
    			    showAlert(response);
    			}

    		}).catch(function(response) {
    			console.log(response);
    			showAlert(response)
    		})
    		.finally(function() {
    			console.log("finally finished");
    		});
    	};
    	
    	$scope.clearData = function () {
    		$scope.userBean.email  = "";
    		$scope.userBean.mobile  = "";
    		$scope.userBean.no  = "";
    		$scope.userBean.soi  = "";
    		$scope.userBean.moo  = "";
    		$scope.userBean.roomNo  = "";
    		$scope.userBean.road  = "";
    		$scope.userBean.postCode  = "";
    		$scope.userBean.provinceId = undefined;
    		$scope.dataAmphurList = [];
    		$scope.dataTambon = [];
    		$scope.userBean.orgType = undefined;
    		$scope.dataOrgSub = [];
    		$scope.dataDept = [];
    		$scope.dataBizArea = [];
    		$scope.dataCostCenter = [];
    		
    		$scope.userBean.govAddreNo  = "";
    		$scope.userBean.govAddreMoo  = "";
    		$scope.userBean.govAddreRoomNo  = "";
    		$scope.userBean.govAddreSoi  = "";
    		$scope.userBean.govAddreRoad  = "";
    		$scope.userBean.govAddrePosCode  = "";
    		
    		$scope.userBean.govAddreProvinceId = undefined;
    		$scope.dataAmphurGov = [];
    		$scope.dataTambonGov = [];
    		
    	};
    	
    	function showAlert (textContent) {
        	$mdDialog.show(
        		      $mdDialog.alert()
        		        .clickOutsideToClose(true)
        		        .title('ข้อผิดพลาด')
        		        .textContent(textContent)
        		        .ok('OK')
        		    );
        }
});

function checkPatternNumber(field,patten,message) {
	
	var field_input = document.querySelector("#"+field);
	
	var $field_data = $('form input[name="'+field); //change form to id or containment selector
//	$field_data.val().replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
	$( '#'+field ).val($field_data.val().replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1'))
	var re = patten;
	if ($field_data.val() == '' || !re.test($field_data.val())){
		field_input.setCustomValidity(message);
		  return false;

	}else{
		field_input.setCustomValidity("");
	}
}

function checkEmail() {
	var email_input = document.querySelector("#email");
	
	var $email = $('form input[name="email'); //change form to id or containment selector
	var re = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
	if ($email.val() == '' || !re.test($email.val()))
	{
		email_input.setCustomValidity("โปรดป้อนอีเมลที่ถูกต้อง");
	    return false;
	}else{
		email_input.setCustomValidity("");
	}


}