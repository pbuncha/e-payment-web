/*<![CDATA[*/
$(document).ready(function() {
		
//		----------------- paging -----------------
		
		var form = $('#civilArbitratorFormSearch');
		var psersonSearch = 'psersonSearch';
		var searchResult = 'searchResult';
		var dataList = $('#dataList');
		var isDocumentReady = true;
		
		init();
		search();
		
		function init() 
		{
			var params = WS.session.get(psersonSearch);
			if(params) 
			{
				form.deserialize(params);
				search();
			} 
			else 
			{
				dataList.DataTable(PagingUtil.getStaticOption());
				isDocumentReady = false;
			}
		}
		
		function doRenderCheckbox(data, type, row) {
			var chkbox = document.createElement('input');
			chkbox.setAttribute('type', 'checkbox');
			chkbox.setAttribute('class', 'chkDelete');
			chkbox.setAttribute('name', 'personId');
			chkbox.setAttribute('value', data);
			return chkbox.outerHTML;
		}
		
		function getColumns()
		{
			return [
			          {data: "personId", sortable: false, className: "dt-body-center", render: doRenderCheckbox},
			          {data: "personName", className: "dt-body-center"},
			          {data: "personTypeName", className: "dt-body-center"},
			       ];
		};

		function search()
		{
			var params = form.serialize();
			
			var dataTableOption = PagingUtil.getDefaultOption();
			
			$.extend(dataTableOption, {
				order: [[1, "asc"]],
				ajaxSource: ctx+"civil/searchCivilArbitratorPerson?"+params,
				columns: getColumns(),
				createdRow: function (row, data, index) {
					/* getCreateRows(row, data, index); */
		        },
		        stateSave: true,
				stateSaveCallback: function(settings, data) {
					WS.session.setJSON(searchResult, data);
					
					// change state to false
					isDocumentReady = false;
				},
				stateLoadCallback: function(settings) {
					if(isDocumentReady) {
						return WS.session.getJSON(searchResult);
					}
					return false;
				}
			});
			
			WS.session.set(psersonSearch, params);
			
			dataList.DataTable(dataTableOption);
		};
		
//		$("#chkAll").click(function() {
//			$(':checkbox.chkDelete').prop('checked', this.checked);
//			$('#chkDeleteMsgLb').removeClass('show');
//		});
//		
//		$(":checkbox.chkDelete").click(function() {
//			$('#chkDeleteMsgLb').removeClass('show');
//		});
//		
//		$('#confirmSelect').on('click', function() {
//			var selected = $('.chkDelete:checked');
//			if (selected.length == 0) {
//				alert("กรุณาเลือกคู่กรณีอย่างน้อย 1 รายการ");
//				return;
//			}
//
//			// submit
//			var addPersonAction = $('#add-person-action').val();
//			$('#civilArbitratorFormSearch').attr('action', addPersonAction);
//			$('#civilArbitratorFormSearch').unbind('submit').submit();
//
//		});

	});
/*]]>*/