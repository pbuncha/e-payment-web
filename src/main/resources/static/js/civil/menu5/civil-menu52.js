/*<![CDATA[*/
$(document).ready(function() {
	calendar('receiveDatePicker');
	calendar('writDatePicker');
	calendar('expiryDatePicker');
	
	/*$('#receiveDatePicker').timepicker();
	$('#writDatePicker').timepicker();
	$('#expiryDatePicker').timepicker();*/
	
	$(".department-placeholder").select2({
	  placeholder: "โปรดระบุ",
	  allowClear: true,
	  theme: "classic"
	});
	
	$("#cancelBtn").click(function() {
		WS.page.change(ctx+"civil/99999/12345/5/2/add");
	});
	
	$('#delete').on("show.bs.modal", function (e) {
		var writId = $(e.relatedTarget).data('id')
		console.log(writId);
		$('#writId').val(writId);
		$('#cmrDel').text(writId);

	});
	
	$('#deleteCivilMenu52Confirm').on('click',function() {
				var writId = $('#writId').val();
				var url = ctx
						+ "civil/99999/12345/5/2/delete/"+writId;
				window.location.href = url;
	});
	
	calExpiryDate();
		
	$('#receiveDate').on('change',function(evt){
		calExpiryDate();
		
	});

	
	$('#expireDay').on('change',function(evt){
		calExpiryDate();
		
	});
	
	function calExpiryDate(){
		var receiveDate = $('#receiveDate').val();
		var expireDay = +$('#expireDay').val();
		var sumDate = 0;
		
		if(receiveDate != ""){
			sumDate = getDateGregorianFromThaiDate(receiveDate).getDate()+expireDay;			
			var expiryDate = new Date(getDateGregorianFromThaiDate(receiveDate).getFullYear(), getDateGregorianFromThaiDate(receiveDate).getMonth(), sumDate);
			console.log(getThaiDateFromGregorianDate(expiryDate));
			$('#expiryDate').text(getThaiDateFromGregorianDate(expiryDate));			
		}else{			
			$('#expiryDate').text('-')
		}
		


		
		

	}
	
	/*function shwAndHideForm(){
		var writId = $('#writId').val();
		
		if(writId == 0){
			$('#hideForm').hide();
			$('#showForm').on('click',function(){
				$('#hideForm').show();
			});
		}else{
			$('#hideForm').show();
		}
	}
	shwAndHideForm();*/
	
	console.log(1);
	
	$.fn.extend({
	    filedrop: function (options) {
	        var defaults = {
	            callback : null
	        }
	        options =  $.extend(defaults, options)
	        return this.each(function() {
	            var files = []
	            var $this = $(this)

	            // Stop default browser actions
	            $this.bind('dragover dragleave', function(event) {
	                event.stopPropagation()
	                event.preventDefault()
	            })

	            // Catch drop event
	            $this.bind('drop', function(event) {
	                // Stop default browser actions
	                event.stopPropagation()
	                event.preventDefault()

	                // Get all files that are dropped
	                files = event.originalEvent.target.files || event.originalEvent.dataTransfer.files
	                
//	                // Convert uploaded file to data URL and pass trought callback
	                if(options.callback) {
	                    var reader = new FileReader()
	                    reader.onload = function(event) {
	                        options.callback(event.target.result)
	                    }
	                    reader.readAsDataURL(files[0])
	                }
	                return false
	            })
	        })
	    }
	});
	
	
//	$(".upload").attr('disabled', true);
//	var dropElement = Dropzone.options.uploadForm = {
//	  maxFilesize: 10,
//      uploadMultiple: true,
//      addRemoveLinks: true,
//      createImageThumbnails: true,
//	  acceptedFiles: ".pdf,.xls,.xlsx,.doc,.docx,.one,.pub,.pptx,.vsdx,.accdb,.mdb,.oft,.mpp,.olm,.vsd,.ppt,.png,.jpg,.jpeg,.gif,.bmp,.svg",
//	  init: function () {
//	    this.on("complete", function (file) {
//	      if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
//	    	  $(".upload").attr('disabled', false);
//	      }
//	    }),
//	    this.on("processing", function (file) {
//	    	$(".upload").attr('disabled', true);
//	    });
//	  }
//	};
//	console.log(dropElement);
//	
//	$(".upload-modal").on('shown.bs.modal', function() {
//		dropElement
//	});
	
	
//	var elementFileSelector = '<div class="fileupload fileupload-new" data-provides="fileupload">'+
//								    '<span class="btn btn-warning btn-file"><span class="fileupload-new">เลือกไฟล์</span>'+
//								    '<span class="fileupload-exists">เปลี่ยนไฟล์</span>'+         
//								    '	<input type="file" name="files"/>'+
//								    '</span>'+
//								    '<span class="fileupload-preview"></span>'+
//								    '<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>'+
//							  	'</div>';
//	
//	function addFileUploadElement()
//	{
//		$('#upload-file-form').append(elementFileSelector);
//	}
//	
//	$('#add-file-chosen').on('click', function() {
//		addFileUploadElement();
////		$('#upload-file-form').append('<input type="file" name="files" accept="*"/>');
//	});
//	
//	$('#upload-file-btn').on('click', function() {
//		upload('upload-file-form', 'messageBox', 'message', 'uploadModal');
//	});
//	
//	$("#uploadModal").on('shown.bs.modal', function() {
////		var hiddenCSRF = $('#upload-file-form input[type="hidden"]').val();
////		$('#upload-file-form').empty();
////		$('#upload-file-form').append('<input type="hidden" name="_csrf" value='+hiddenCSRF+'>');
////		$('#upload-file-form').append('<input type="file" name="files" accept="*"/>');
////		$('#upload-file-form input[type="file"]').val('');
////		$('#messageBox').attr('style','display:none');
//		
//		var hiddenCSRF = $('#upload-file-form input[type="hidden"]').val();
//		$('#upload-file-form').empty();
//		$('#upload-file-form').append('<input type="hidden" name="_csrf" value='+hiddenCSRF+'>');
//		addFileUploadElement();
//		$('#messageBox').attr('style','display:none');
//	});
//	
//	function upload(formSelectorName, msgSelectorName, msgName, modalSelectorName)
//	{
//		var msgSelector = undefined;
//		var msg = undefined;
//		var modalSelector = undefined;
//		if(undefined!=msgSelectorName && null!=msgSelectorName)
//		{
//			msgSelector = $('#'+msgSelectorName);
//		}
//		if(undefined!=msgName && null!=msgName)
//		{
//			msg = $('#'+msgName);
//		}
//		if(undefined!=modalSelectorName && null!=modalSelectorName)
//		{
//			modalSelector = $('#'+modalSelectorName);
//		}
//		
//		var form = $('#'+formSelectorName);
//		
//		$.ajax({
//			url: form.attr('action'),
//			type: form.attr('method'),
//			data: new FormData(form[0]),
//			enctype: 'multipart/form-data',
//			processData: false,
//			contentType: false,
//			cache: false,
//			success: function (result) {
////				if(null==result[0].fileId)
////				{
////					msg.text(result[0].message.message);
////					msgSelector.attr('style','display:block');
////				}
////				else
////				{
//					modalSelector.modal('hide');
////				}
//			}
//		});
//	};
});
/*]]>*/