$(document).ready(function(){
	var myMarkup = '[data-mark-up="menu-55"]';
	$(document).on('change.menu-55', myMarkup+' [name="decision.MDecided.decidedId"]', function(e){
		switch($(this).val()) {
		case "4":
			$(this).closest(myMarkup).find('[name="decision.totalDamageAmount"]').closest('.form-group').showForm();
			$(this).closest(myMarkup).find('[data-litigant-damage]').hideForm();
			break;
		default:
			$(this).closest(myMarkup).find('[name="decision.totalDamageAmount"]').closest('.form-group').hideForm();
			$(this).closest(myMarkup).find('[data-litigant-damage]').showForm();
		}
		switch($(this).val()) {
		case "5":
		case "4":
			$(this).closest(myMarkup).find('[data-litigant-decided-damage]').showForm();
			break;
		default:
			$(this).closest(myMarkup).find('[data-litigant-decided-damage]').hideForm();
		}
	});
	
	$(document).on('change.menu-55', myMarkup+' [data-litigant-decided]', function(e){
		switch($(e.target).val()) {
		case "1":
			$(this).closest('[data-litigant-decided-damage]').find('[data-litigant-damage]').parent().showForm();
			break;
		default:
			$(this).closest('[data-litigant-decided-damage]').find('[data-litigant-damage]').parent().hideForm();
		}
	});
	
	$(document).on('newElement', myMarkup, function(e){
		if (e.target === this) {
			$('[name="decision.MDecided.decidedId"]', this).trigger('change.menu-55');			
		}
	});
});