
$(document).ready(function() {
	init();
	
	//view page
	var mode  = $('#mode').val();
	if (mode == "editResult") {
		$('#appealResultDiv').show();
	} else {
		$('#appealResultDiv').hide();
	}
});

function init(){
//	$('#appealFormDiv').hide();
	$('#appealResultDiv').hide();
	calendar('requestDatePicker');
}

function cancel(){
	var appealId  = $('#appealId').val();
	if (appealId > 0) {
		var lawsuitId = $('#lawsuitId').val();
		var url = ctx + "civil/"+lawsuitId+"/5/6/view/"+appealId;
		window.location.href = url;
	} else {
		document.getElementById("civilLawsuitAppealForm").reset();
	}
}

function addNewAppeal(){
	var lawsuitId = $('#lawsuitId').val();
	var url = ctx + "civil/"+lawsuitId+"/5/6/add";
	window.location.href = url;
}

function edit(){
	var appealId  = $('#appealId').val();
	var lawsuitId = $('#lawsuitId').val();
	var url = ctx + "civil/"+lawsuitId+"/5/6/edit/"+appealId;
	window.location.href = url;
}

function edit_summary(appealId){
	var lawsuitId = $('#lawsuitId').val();
	var url = ctx + "civil/"+lawsuitId+"/5/6/edit/"+appealId;
	window.location.href = url;
}

function showDeleteModal(appealId){
	$('#selectId').val(appealId);
	$("#deleteMenu56ViewAll").modal("show");
}

function confirmDelete_summary(){
	var appealId  = $('#selectId').val();
	var lawsuitId = $('#lawsuitId').val();
	var url = ctx + "civil/"+lawsuitId+"/5/6/summary/delete/"+appealId;
	window.location.href = url;
}

function confirmDelete(){
	var appealId  = $('#appealId').val();
	var lawsuitId = $('#lawsuitId').val();
	var url = ctx + "civil/"+lawsuitId+"/5/6/view/delete/"+appealId;
	window.location.href = url;
}

function addAppealResultDiv(){
	$('#appealResultDiv').show();
	$('#appealResult-btn').hide();
}