$(document).ready(function() {
	
	calendar('closeCaseDate');
	
	reasonOther();
	
	$('#closeCaseReason').change(function() {
		reasonOther();
	});
	
	function reasonOther() {
		var element = $('#closeCaseReason');
		if (element.val() == 99) {
			$('#reasonOther').show();
		} else {
			$('#reasonOther').hide();
		}
	}

});