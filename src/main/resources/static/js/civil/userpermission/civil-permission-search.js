/*<![CDATA[*/
$(document).ready(function() {
	$('#user').select2Ajax({ajax:{url:ctx+'civil/select2/civilPermissionUser'}});
});
$(document).ready(function() {
		
//		----------------- paging -----------------
		
		var form = $('#civilPermissionFormSearch');
		var userSearch = 'userSearch';
		var searchResult = 'searchResult';
		var userList = $('#userList');
		var isDocumentReady = true;
		
		init();
		
		function init() 
		{
			var params = WS.session.get(userSearch);
			if(params) 
			{
				form.deserialize(params);
				search();
			} 
			else 
			{
				userList.DataTable(PagingUtil.getStaticOption());
				isDocumentReady = false;
			}
		}
		
		function doRenderCheckbox(data, type, row) {
			var chkbox = document.createElement('input');
			chkbox.setAttribute('type', 'checkbox');
			chkbox.setAttribute('class', 'chkDelete');
			chkbox.setAttribute('name', 'userId');
			chkbox.setAttribute('value', data);
			return chkbox.outerHTML;
		}
		
		function getColumns()
		{
			return [
			          {data: "userId", sortable: false, render: function(data, type, row, meta) {
			        	  var temp = $('<div>')
			        	  $('<input type="checkbox" name="userList['+meta.row+'].userId">').appendTo(temp).val(data);
			        	  return temp.html();
			           },className: "dt-body-center"},
			          {data: "fullName", className: "dt-body-left text-nowrap"},
			          {data: "orgName", className: "dt-body-center"},
			          {data: "position", className: "dt-body-center"},
			          {data: "parentName", className: "dt-body-left text-nowrap"},
			          	{
			        	  data: null,
					         sortable:false,
					         className: "text-nowrap",
					         render: function (data, type, full, meta){
					        	 var sp = full.casePermssionName.split(', ');
					        	 var ret ='';
					        	 $.each(full.casePermssionId.split(', '), function(idx, data){
					        		 ret += '<label class="radio-inline hidden"><input type="radio" name="casePermissionList['+meta.row+'].casePermissionId" value="'+data+'">'+'<span>'+sp[idx]+'</span></label>';
					        	 });
					        	 return ret;
					         }
				         }
			       ];
		};

		function search()
		{
			var params = form.serialize();
			
			var dataTableOption = PagingUtil.getDefaultOption();
			
			$.extend(dataTableOption, {
				order: [[1, "asc"]],
				ajaxSource: ctx+"civil/civilSearchUserRoleTwo?"+params,
				columns: getColumns(),
				createdRow: function (row, data, index) {
					/* getCreateRows(row, data, index); */
		        },
		        stateSave: true,
				stateSaveCallback: function(settings, data) {
					WS.session.setJSON(searchResult, data);
					
					// change state to false
					isDocumentReady = false;
				},
				stateLoadCallback: function(settings) {
					if(isDocumentReady) {
						return WS.session.getJSON(searchResult);
					}
					return false;
				}
			});
			
			WS.session.set(userSearch, params);
			
			userList.DataTable(dataTableOption);
		};
		
		$('#searchBtn').on('click', function() {
			search();
		});

	});
/*]]>*/