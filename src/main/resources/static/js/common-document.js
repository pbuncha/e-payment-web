var commonDoc = {
		init: function(idOrClass, name) {
			if (idOrClass.match(/^([#])(.+)/)) {
				var id = idOrClass;
				$.post(
						ctx + 'commonDoc/init',
						{documentSessionName: name, elementId: id},
						function(response) {
						  if ($(id).parents('form').length == 0) {								  
							  $(id).wrap($('<form>').attr('data-toggle', 'validator'));
						  }
						  $(id).html(response);
						}
				);
			} else if (idOrClass.match(/^([.])(.+)/)) {
				var className = idOrClass;
				var id = className.replace(".", "");
				$(className).each(function(index, element) {
					var elementId = id + "_" + index;
					$(element).attr("id", elementId);
					$.post(
							ctx + 'commonDoc/initList',
							{documentSessionName: name, elementId: elementId, index: index},
							function(response) {
							  if ($(element).parents('form').length == 0) {								  
								  $(element).wrap($('<form>'));
							  }
							  $(element).html(response);
							}
					);
				})
			}
		},
		attachModalUp: function(id) {
			$(id).modal('show');
			$(id).find('input[name=docTitle]').attr('required', true);
			$(id).find('input[name=docDate]').attr('required', true);
		},
		viewInfo: function(id, index, name, indexList) {
			var data;
			if (indexList == 'null') {
				data = {documentSessionName: name, elementId: id, documentIndex: index}; 
			} else {
				data = {documentSessionName: name, elementId: id, documentIndex: index, commonDocListIndex: indexList};
			}
			$.post(
					ctx + 'commonDoc/viewDocument',
					data,
					function(response) {
						$(id).find('.view-doc-modal').remove();
						$(id).find('div.modal-doc').append(response);
						$(id).find('.view-doc-modal').modal('show');
					}
			);
		},
		editDoc: function(id, index, name, indexList) {
			var data;
			if (indexList == 'null') {
				data = {documentSessionName: name, elementId: id, documentIndex: index}; 
			} else {
				data = {documentSessionName: name, elementId: id, documentIndex: index, commonDocListIndex: indexList};
			}
			$.post(
					ctx + 'commonDoc/editDocument',
					data,
					function(response) {
						$(id).find('div.edit-doc-modal').remove();
						$(id).find('div.delete-file-moal').remove();
						$(id).find('div.modal-doc').append(response);
						$(id).find('.edit-doc-modal').modal('show');
						$(id).find('div.edit-doc-modal input[name=docTitle]').attr('required', true);
						$(id).find('div.edit-doc-modal input[name=docDate]').attr('required', true);
					}
			);
		},
		deleteDoc: function(id, index, name, indexList, docTitle) {
			
			var data;
			if (indexList == 'null') {
				data = {
					commonDocumentObjIndex: index,
					documentSessionName: name, 
					elementId: id 
				};
			} else {
				data = {
						commonDocumentObjIndex: index,
						documentSessionName: name, 
						elementId: id,
						commonDocListIndex: indexList
				};
			}
			
			$.post(
					ctx + 'commonDoc/deleteDocumentConfirm',
					data,
					function (response) {
						$(id + '>div.table-doc').html(response);
					}
	        );
		},
		deleteFile: function(id, fileId, fileName , type) {
			var mainForm = $(id).find('div.edit-doc-modal');
			var li;
			if (type == 'commonDocumentFile') {
				var input = $(mainForm).find('li.common-file>input[name=documentFileFromCommon][value=' + fileId + ']');
				li = $(input).closest('li.common-file');
			} else if (type == 'tempFile') {
				var input = $(mainForm).find('li.temp-file>input[name=documentFileFromTemp][value=' + fileId + ']');
				li = $(input).closest('li.temp-file');
			}
			$(li).remove();
		}
};

(function(){
	var isAjaxUploadSupported = (function(){
		if (window.FormData === undefined) return false;
		if (window.XMLHttpRequest === undefined) return false;
		var xhr = new XMLHttpRequest();
		return 'upload'in xhr && 'onprogress'in xhr;
	})();
	var uploadUrl = ctx+'views/common/upload/uploadTemporaryFilePart';
	
	var uploadProgress = false;
	
	$(document).on('hidden.bs.modal','.common-document-modal', function(e){
		$(this).find('input').prop('disabled',true);
	});
	
	$(document).on('show.bs.modal','.common-document-modal', function(e){
		$(this).find('input').prop('disabled',false);
	});
	
	$(document).on('newElement','.common-document-file-upload',function(e){
		if (this != e.target) {
			return;
		}
		if ($(this).data('fileUpload') !== undefined) {
			return;
		} 
		$(this).data('fileUpload', true);
		var multipart_params = {};
		multipart_params[csrfParameterName]=csrfToken;
		$(".common-document-file-upload").plupload({
			runtimes : "html5,html4",
			url: uploadUrl,
			max_file_count: 20,
			chunk_size: "1024kb",
			filters: {
				max_file_size: "100mb",
				mime_types: [
					{title:"Image",extensions:"png,jpg"},
					{title:"Document", extensions:"pdf,doc,docx,xls,xlsx,ppt,pptx"},
					{title:"Archive", extensions: "zip,rar,7zip"}
				]
			},
			rename: false,
			sortable: false,
			dragdrop: true,
			views: {
				list: true,
				active: 'list'
			},
			multipart_params : multipart_params,
			init: {
				BeforeUpload: function(up, file) {
	                uploadProgress = true;
	            },
				UploadComplete: function(up, files) {
					uploadProgress = false;
            	},
            	FilesRemoved: function(up, files, e) {
                    plupload.each(files, function(file) {
                        $(document).find('input[type=hidden][fileid=' + file.id + ']').remove();
                    });
                },
                FilesAdded: function(up, files) {
                    up.start();
                }
			}
		});
	});
	
	$(document).on('uploaded',function(e,data){
		var input = $('<input>').attr({type: 'hidden', name: 'documentFile', value: data.result.response, fileid: data.file.id});
		$(e.target).append(input);
	});
	
	$(document).on('click','.plupload_button.plupload_start', function(e){
		$(this).closest('.common-document-file-upload').plupload('start');
	});
	
	$(document).on('click', '.edit-doc-upload-ok', function(ev) {
		var $this = $(this);
		var id = $this.attr('data-elementId');
		var divForm = $('#' + id + ' div.edit-doc-modal');
		$(divForm).find('input[name=docTitle]').valid();
		$(divForm).find('input[name=docDate]').valid();
		var formFile = $(divForm).find('input[name=documentFile]');
		var formFileCommon = $(divForm).find('input[name=documentFileFromCommon]');
		var formFileTemp = $(divForm).find('input[name=documentFileFromTemp]');
		if (!uploadProgress && $(divForm).find('input[name=docTitle]').valid() && $(divForm).find('input[name=docDate]').valid() && (formFile.length > 0 || formFileCommon.length > 0 || formFileTemp.length > 0)) {
			$.post(
					ctx + 'commonDoc/editDocumentConfirm',
					$(divForm).find('input').serialize(),
	            	function (response) {
		                $(divForm).modal('hide');
		                $('#' + id + '>div.table-doc').html(response);
		                $(divForm).find('input[name=docTitle]').attr('required', false);
		        		$(divForm).find('input[name=docDate]').attr('required', false);
					}
	        );
		} else {
			$(divForm).find('.msg-empty-file').attr('class', 'row form-group msg-empty-file');			
		}
	});
	
	$(document).on('click', '.attach-new-upload-ok', function(ev){
		var $this = $(this);
		var id = $this.attr('data-elementId');
		var divForm = $('#' + id + ' div.attach-new-doc-modal');
		$(divForm).find('input[name=docTitle]').valid();
		$(divForm).find('input[name=docDate]').valid();
		var divFormFile = $(divForm).find('input[name=documentFile]');
		if (!uploadProgress && $(divForm).find('input[name=docTitle]').valid() && $(divForm).find('input[name=docDate]').valid()  && divFormFile.length > 0) {
	        $.post(
	        		ctx + 'commonDoc/attachNewDocument',
	        		$(divForm).find('input').serialize(),
	        		function (response) {
		            	$(divForm).find('input[name=docTitle]').val($(divForm).find('input[name=docTitle]').attr('value'));
		            	$(divForm).find('input[name=docNo]').val($(divForm).find('input[name=docNo]').attr('value'));
		            	$(divForm).find('input[name=docDate]').val($(divForm).find('input[name=docDate]').attr('value'));
		            	$(divForm).find('input[name=roomNo]').val($(divForm).find('input[name=roomNo]').attr('value'));
		            	$(divForm).find('input[name=cabinetNo]').val($(divForm).find('input[name=cabinetNo]').attr('value'));
		            	$(divForm).find('input[name=shelfNo]').val($(divForm).find('input[name=shelfNo]').attr('value'));
		            	$(divForm).find('input[name=docComment]').val($(divForm).find('input[name=docComment]').attr('value'));
		            	$(divForm).find('input[name=docTitle]').attr('required', false);
		            	$(divForm).find('input[name=docDate]').attr('required', false);
		                $(divForm).closest('.attach-new-doc-modal').modal('hide');
		                var uploader = $(".common-document-file-upload").plupload('getUploader').splice();
		        		$(divForm).find('.plupload_filelist_content', uploader).empty();
		        		$(divForm).find('input[name=documentFile]').remove();
		        		$(divForm).find('.msg-empty-file').attr('class', 'row form-group msg-empty-file hidden');
		                var elementId = $this.attr('data-elementId');
		                $('#' + elementId + '>div.table-doc').html(response);
	        		}
	        );
		} else {
			$(divForm).find('.msg-empty-file').attr('class', 'row form-group msg-empty-file');
		}
	});
	
	$(document).on('click', '.delete-doc-modal-button', function(ev) {
		var $this = $(this);
		var id = $this.attr('data-elementId');
		var divForm = $('#' + id + ' div.delete-doc-modal');
		$.post(
				ctx + 'commonDoc/deleteDocumentConfirm',
				$(divForm).find('input').serialize(),
				function (response) {
					$(divForm).modal('hide');
					$('#' + id + '>div.table-doc').html(response);
				}
        );
	});
	
	$(document).on('complete',function(e){
		var queueLen = $('.plupload_filelist_content').children().length;
		if(queueLen>0)
		{
			$('.documentAttach').val(queueLen);
		}
	});
	
})();