cmnFn = {};

cmnFn.urlParam = function(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

cmnFn.errorDialog = function(msg) {
	showDialog("เกิดข้อผิดพลาด", msg);
};

cmnFn.confirmDialog = function() {
	
};

cmnFn.successDialog = function() {
	
};

function showDialog(title, content) {
	$("#messageModal .modal-title").text(title);
	$("#messageModal .modal-body p").text(content);
	$('#messageModal').modal() ;
}

cmnFn.showLoadingImage = function () {
	$('.circles-loader').show();
	$('.overlay').show();
};

cmnFn.hideLoadingImage = function () {
	$('.circles-loader').hide();
	$('.overlay').hide();
};

cmnFn.goBack = function() {
	history.go(-1);
	//window.history.back();
}

cmnFn.showMessageBox = function(success, msg) {
	if(undefined===success) {
		$('#headerMsg').addClass('hidden');
		return;
	}
	
	var headerMsg = $('#headerMsg').attr('class', 'col-md-12');
	var p = headerMsg.children().attr('class', success?'bg-success text-success panel-round':'bg-danger text-danger panel-round');
	$(p[0].children[0]).attr('class', success?'hidden glyphicon glyphicon-exclamation-sign':'glyphicon glyphicon-exclamation-sign');
	$(p[0].children[1]).attr('class', success?'glyphicon glyphicon-ok-circle':'hidden glyphicon glyphicon-ok-circle');
	$(p[0].children[2]).text(msg);
	
	$('body,html').animate({
		scrollTop: 0
	}, 400);
};

//WiseSoft jQuery Function
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

(function ($) {
	var config = {
		'.chosen-select'           : {},
		'.chosen-select-deselect'  : {allow_single_deselect:true},
	    '.chosen-select-no-single' : {disable_search_threshold:10},
	    '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
	    '.chosen-select-width'     : {width:"95%"}
    }
	
    for (var selector in config) {
    	if ($(selector).length > 0) {
    		$(selector).chosen(config[selector]);
    	}
    }
	
	$.ajaxSetup({
		error: function(xhr, exception, errorThrown) {
			cmnFn.errorDialog(xhr.responseText);
		}
	});
	
	if(undefined!=$.fn.dataTable)
	{
		$.fn.dataTable.ext.errMode = 'none';
	}

	$(document).ajaxSend(function(e, jqXHR){
		cmnFn.showLoadingImage();
	});
	
	$(document).ajaxComplete(function(e, jqXHR){
		cmnFn.hideLoadingImage();
	});
})(jQuery);