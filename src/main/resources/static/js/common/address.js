/**
 * wichuda.k
 */

angular.module('addressApp', [])
.directive('myAddress', function() {
	return {
		restrict: 'E',
		scope: {
			addr: '=addr',
			dis: '=disabled',
			isSubmit: '=isSubmit'
		},
		controller: function($scope,$q,httpProvider) {
			$scope.provinceList = [];
			$scope.districtList = [];
			$scope.subdistrictList = [];
			var urlBase = ctx+'/manageInvoice';
			console.log($scope.addr);

			$scope.loadSubdistrict = function(id){
				if(id){
					httpProvider().get(urlBase+'/getSubDistrict/'+id).then(function(response){
						$scope.subdistrictList = response;
						if(angular.equals($scope.addr.tambonId,null)|| angular.equals($scope.addr.tambonId,{})|| angular.equals($scope.addr.tambonId,undefined)){
							$scope.addr.tambonId = response[0].id;
						}
					});
				}
			}
			$scope.loadDistrict = function(id){
				if(id){
					httpProvider().get(urlBase+'/getDistrict/'+id).then(function(response){
						$scope.districtList = response;
						if(angular.equals($scope.addr.amphurId,null)|| angular.equals($scope.addr.amphurId,{})|| angular.equals($scope.addr.amphurId,undefined)){
							$scope.addr.amphurId = response[0].id;							
						}
						$scope.loadSubdistrict($scope.addr.amphurId);
					});
				}

			}
			$scope.loadProvince = function(){
				if($scope.provinceList != []){
					httpProvider().get(urlBase+'/getProvince').then(function(response){
//						console.log(response);
						$scope.provinceList = response;
						if(angular.equals($scope.addr,null)|| angular.equals($scope.addr,{})){
							$scope.addr.provinceId = response[0].id;
						}
						$scope.loadDistrict($scope.addr.provinceId);
					});
				}
			}
			
			$scope.$watch(function () { return $scope.addr.provinceId; }, function (newData, oldData) {
				if(!angular.equals(newData,oldData)){
					$scope.loadDistrict($scope.addr.provinceId);
				}
			});
			
			$scope.$watch(function () { return $scope.addr.amphurId; }, function (newData, oldData) {
				if(!angular.equals(newData,oldData)){
					$scope.loadSubdistrict($scope.addr.amphurId);
				}
			});
		},
		templateUrl: ctx +'/templates/views/common/address.html',
//		templateUrl: '../js/common/address.html'
	};
});