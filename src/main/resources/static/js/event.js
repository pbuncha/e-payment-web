$(document).ready(function() {
	// Button Add Hover
	$('.btn-group-add .btn-add').hover(
		function() { // in
			var $span = $(this).find('span');
			$({deg: 0}).animate({deg: 90}, {
		        duration: 120,
		        step: function(now) {
		        	$span.css({
		                transform: 'rotate(' + now + 'deg)'
		            });
		        },
		        complete: function() {
		        	$span.removeClass('glyphicon-plus');
		        	$span.addClass('glyphicon-pencil');
		        	
		        	$({deg: -30}).animate({deg: 0}, {
				        duration: 20,
				        step: function(now) {
				        	$span.css({
				                transform: 'rotate(' + now + 'deg)'
				            });
				        }
				    });
		        }
		    });

		}, function() { // out
			var $span = $(this).find('span');
			
			$span.removeClass('glyphicon-pencil');
        	$span.addClass('glyphicon-plus');
			
			$({deg: 90}).animate({deg: 0}, {
		        duration: 100,
		        step: function(now) {
		            // in the step-callback (that is fired each step of the animation),
		            // you can use the `now` paramter which contains the current
		            // animation-position (`0` up to `angle`)
		        	$span.css({
		                transform: 'rotate(' + now + 'deg)'
		            });
		        }
		    });
			
		}
	);

	// Tooltips
    $("body").tooltip({ selector: '[data-tooltip=tooltip]' });
    
    // Button Command for Box
    $('.box-header .btn-close').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().fadeOut();
    });
    $('.box-header .btn-minimize').click(function (e) {
        e.preventDefault();
        console.log('click');
        var $target = $(this).parent().parent().next('.box-content');
        if ($target.is(':visible')) $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        else                       $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        $target.slideToggle();
    });
    
    // Button Command for Panel
    $('.panel-heading .btn-close').click(function (e) {
        e.preventDefault();
        $(this).parent().parent().parent().fadeOut();
    });
    $('body').on('click','.panel-heading .btn-minimize', function (e) {
        e.preventDefault();
        console.log('click');
        var $target = $(this).parent().parent().next('.panel-collapse');
        if ($target.is(':visible')) $('i', $(this)).removeClass('glyphicon-chevron-up').addClass('glyphicon-chevron-down');
        else                       $('i', $(this)).removeClass('glyphicon-chevron-down').addClass('glyphicon-chevron-up');
        $target.slideToggle();
    });
    
    // event back button for back page
    if($('form .btn .glyphicon-floppy-disk').length > 0) {
    	detectBackButtonEvent();
    }
    
    // enter to tab event
//    $('body').on('keydown', 'input, select', function(e) {
//        var self = $(this)
//          , form = self.parents('form:eq(0)')
//          , focusable
//          , next
//          ;
//        if (e.keyCode == 13) {
//            focusable = form.find('input,select,button,textarea').filter(':visible').not('.tt-hint');
//            next = focusable.eq(focusable.index(this)+1);
//            if (next.length) {
//                next.focus();
//            } else {
//                form.submit();
//            }
//            return false;
//        }
//    });
    
    // disable autocomplete in calendar input
    $('div.input-group.date > input').attr('autocomplete', 'off');
});
// alert and stop the input file event chain when upload exceed 3MB
$(document).on('change', '.btn-file :file, .file-event:file', function(e){
  var input = e.target;
  if (!input.files) { // ignore, if file is undefined: Can't check for anything file related.
    return;
  }
  var numFiles = input.files ? input.files.length : 1;
  for (var i = 0; i < numFiles; ++i) {
    if (input.files[i].size > 3145728) {
      var jq = $(input).wrap('<form>');
      jq.closest('form')[0].reset();
      jq.unwrap();
      e.stopImmediatePropagation();
      e.preventDefault(0);
      alert("ขนาดของไฟล์ใหญ่เกิน 3 MB");
      return;
    }
    var fileExtension = $(input).val().substring().replace(/\\/g, '/').replace(/.*\//, '');
    var index = fileExtension.lastIndexOf('.');
    if (index >= 0) {
      if (['.bmp','.jpg','.jpeg','.pdf','.png','.xls','.xlsx','.doc','.docx','txt','.ppt','.pptx'].indexOf(fileExtension.substring(index).toLowerCase()) < 0) {
        var jq = $(input).wrap('<form>');
        jq.closest('form')[0].reset();
        jq.unwrap();
        e.stopImmediatePropagation();
        e.preventDefault(0);
        alert("ชนิดของไฟล์ไม่ถูกต้อง ระบบรองรับไฟล์ '.bmp','.jpeg','.jpg','.pdf','.png','.xls','.xlsx','.doc','.docx','txt','.ppt','.pptx' เท่านั้น");
        return;
      }
    }
  }
});
// input file
$(document).on('change', '.btn-file :file, .file-event:file', function() {
  var input = $(this),
      numFiles = input.get(0).files ? input.get(0).files.length : 1,
      label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
  input.trigger('fileselect', [numFiles, label]);
});
$(document).on('fileselect', 'input', function(event,numFiles, label){
  $(this).closest('.input-group').find('input[data-file-name]').val(label);
});