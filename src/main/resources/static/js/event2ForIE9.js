$(document).ready(function(){
	function triggerNewElement(element){
		if ($(element).data('__isThisElementNeverTriggerBefore__') === undefined) {
			$(element).data('__isThisElementNeverTriggerBefore__', true);
			$(element).trigger('newElement');			
		}
	}
	$('body')[0].addBehavior("foo.htc");
	$('body')[0].attachEvent("onreadystatechange", function(e){		
		triggerNewElement(e.target);
	});
	$('*').each(function(idx,ele){
		triggerNewElement(ele);
	});
});