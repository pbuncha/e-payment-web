

$(document).ready(function() {

		$('#saveBtn').on('click', function() {
			var svFname = $("#svFname").val();
			save(svFname);
		});
		$('#searchBtn').on('click', function() {
			var sFname = $("#sFname").val();
			search(sFname);
		});
		$('#searchAllBtn').on('click', function() {
//			var sFname = $("#sFname").val();
			searchAll();
		});
		$('#updateBtn').on('click', function() {
//			var sFname = $("#sFname").val();
			update();
		});
		
		
		
		function search(sFname){
			$.ajax({
				url :"../exampleController/findExampleByName",
				data : {
					sFname:sFname
				},
				type : "GET",
				dataType : "json",
				success : function(result) {
					$('#exampleResult').html(result.fname+" "+result.lname);
				},
				error : function(error) {
				console.log(error);
				}
			});
		}
		function searchAll(sFname){
			$.ajax({
				url :"../exampleController/findAllExampleByName",
				data : {
				},
				type : "GET",
				dataType : "json",
				success : function(resultList) {

					var valElem= $('#exampleResult2').val();
					var label = "";
					for ( var i = 0; i < resultList.length; i++) {
						label += " "+resultList[i].fname+" "+resultList[i].lname+"<br/>";
						console.log(label);
					}
					$('#exampleResult2').html(label);		

				},
				error : function(error) {
					console.log(error);
				}
			});
		}
		
		
		function save(svFname){
			var exampleBean = {
					id: $("#svId").val(),
					fname: $("#svFname").val(),
					lname: $("#svLname").val(),
			
				};
			$.ajax({
				url :"../exampleController/saveExample",
				data : JSON.stringify(exampleBean),
				type : "POST",
				contentType : 'application/json; charset=utf-8',
				dataType : "json",
				success : function(result) {
					console.log(result);
				},
				error : function(error) {
				console.log(error);
				}
			});
		}
		
		function update(){
			
			var exampleBean = {
					id: $("#upId").val(),
					fname: $("#upFname").val(),
					lname: $("#upLname").val(),
			
				};
			


			$.ajax({
				url :"../exampleController/updateExample",
				data : JSON.stringify(exampleBean),
				type : "POST",
				contentType : 'application/json; charset=utf-8',
				dataType : "json",
				success : function(result) {
					console.log(result);
				},
				error : function(error) {
				console.log(error);
				}
			});
		}
	
		

	});
/*]]>*/