var app = angular.module("exampleApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);


app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/exampleController';
	
	  
	    var dataFactory = {};

	    dataFactory.saveExample = function (exampleBean) {
	        return  httpProvider().post(urlBase+'/saveExample',exampleBean);
	    };
	    dataFactory.updateExample = function (exampleBean) {
	        return httpProvider().put(urlBase+'/updateExample',exampleBean);
	    };

	    dataFactory.findAllExampleByName = function () {
	        return httpProvider().get(urlBase+'/findAllExampleByName');
	    };
	    
	    dataFactory.deleteExample = function (id) {
	        return httpProvider().get(urlBase+'/deleteExample/'+id);
	    };
	    
	    
	    
	    
	    dataFactory.postExample = function () {
	        return  httpProvider().post(urlBase+'/postExample');
	    };
	    dataFactory.getExample = function () {
	        return  httpProvider().get(urlBase+'/getExample');
	    };
	    dataFactory.angucomplete = function () {
	        return  httpProvider().get(urlBase+'/angucomplete');
	    };
	    dataFactory.pagination = function () {
	        return  httpProvider().post(urlBase+'/pagination');
	    };

	    return dataFactory;
});

app.controller("exampleCtrl", function ($log,$scope, dataFactory) {
	$scope.exampleBean = {};
	
	dataFactory.postExample().then(function (response) {
			console.log(response);
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
	});
	
	
	dataFactory.getExample().then(function (response) {
			console.log(response);
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
	});
	
	$log.log('Example 1 - text auto complete');
	dataFactory.angucomplete().then(function (response) {
		console.log(response);
		$scope.data1List = response;
	}).catch(function(response) {
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally finished");
	});
	
	
	
	$log.log('Example 2 - pagination');

	$scope.data3List ={};
	dataFactory.pagination()
	.then(function (response) {
		$scope.data3List =response.content;
		$log.log(response);
		$log.log(response.totalPages);


		$scope.maxSize = response.size;
		$scope.bigTotalItems = response.totalPages;
		$scope.bigCurrentPage = 1;
	}, function (error) {
		$scope.status = 'Unable to load customer data: ' + error.message;
	});

	 	
	$scope.findAllExampleByName = function () {
		dataFactory.findAllExampleByName().then(function (response) {
			console.log(response);
			$scope.data4List = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};

	$scope.saveExample = function () {
		console.log($scope.exampleBean);
		dataFactory.saveExample($scope.exampleBean).then(function (response) {
			console.log(response);
			$scope.data4List = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	
	$scope.updateExample = function () {
		console.log($scope.exampleBean1);
		dataFactory.updateExample($scope.exampleBean1).then(function (response) {
			console.log(response);
			$scope.data4List = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	
	$scope.deleteExample = function () {
		console.log($scope.id);
		dataFactory.deleteExample($scope.id).then(function (response) {
			console.log(response);
			$scope.data4List = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	 	

    
});