(function(){
	
	/**
	 * Start Example for configure rule for any select element
	 */
	
	$.validator.addMethod('selectInvalidOption',function(value, element, params){
		return $('[data-rule-option="invalid"]:selected',element).length == 0;
	});
	
	$(document).on('configRule.validator',function(e){
		$(e.target).find('select').addBack('select').each(function(idx,ele){
			$(ele).rules('add',{
				selectInvalidOption: true
			});
		});
	});
	
	/**
	 * End Example for configure rule for any select element
	 */
	
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation=number] input').addBack('[data-validation=number] input').each(function(idx,ele){
			$(ele).rules('add',{
				number : true
			});
		});
	});
	
	$.validator.addMethod('notMoreThanCurrentDate',function(value, element, params){
		 if(jQuery.trim($(element).val()).length==0)
		 {
			 return true;
		 }
		 var thaiDate = getDateGregorianFromThaiDate($(element).val());
		 if(thaiDate == null) return true;
		 return thaiDate.getTime()<= new Date().getTime();
	}, jQuery.validator.format("วันที่ระบุต้องไม่เกินวันที่ปัจจุบัน"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=notMoreThanCurrentDate] input').addBack('[data-validation-rules=notMoreThanCurrentDate] input').each(function(idx,ele){
			$(ele).rules('add',{
				notMoreThanCurrentDate: true
			});
		});
	});
	
	$.validator.addMethod('lessThanCurrentDate',function(value, element, params){
		 if(jQuery.trim($(element).val()).length==0)
		 {
			 return true;
		 }
		 var thaiDate = getDateGregorianFromThaiDate($(element).val());
		 if(thaiDate == null) return true;
		 return thaiDate.getTime() < getDateGregorianFromThaiDate(getThaiDateFromGregorianDate(new Date())).getTime();
	}, jQuery.validator.format("วันที่ระบุต้องน้อยกว่าวันที่ปัจจุบัน"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=lessThanCurrentDate] input').addBack('[data-validation-rules=lessThanCurrentDate] input').each(function(idx,ele){
			$(ele).rules('add',{
				lessThanCurrentDate: true
			});
		});
	});
	
	$.validator.addMethod('moreThanZero',function(value, element, params){
		if(jQuery.trim($(element).val()).length==0)
		{
			return true;
		}
		return +$(element).val().replace(',','') > 0;
	}, jQuery.validator.format("ต้องระบุค่ามากกว่าศูนย์"));
		$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=moreThanZero] input').addBack('[data-validation-rules=moreThanZero] input').each(function(idx,ele){
			$(ele).rules('add',{
				moreThanZero: true
			});
		});
	});
		
	$.validator.addMethod('moreThanMillion',function(value, element, params){
		if(jQuery.trim($(element).val()).length==0)
		{
			return true;
		}
		return +$(element).val().replace(',','') >= 1000000;
	}, jQuery.validator.format("ต้องระบุทุนทรัพย์ที่ฟ้องไม่น้อยกว่า 1,000,000 บาท"));
		$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=moreThanMillion] input').addBack('[data-validation-rules=moreThanMillion] input').each(function(idx,ele){
			$(ele).rules('add',{
				moreThanMillion: true
			});
		});
	});

	$.validator.addMethod('checkSSN',function(value, element, params){
		if(jQuery.trim($(element).val()).length==0)
		{
			return true;
		}
		value = value.replace("-", "");
		if(value.length != 13) return false;
		for(var i=0, sum=0; i < 12; i++) sum += parseFloat(value.charAt(i))*(13-i); 
		if((11-sum%11)%10!=parseFloat(value.charAt(12))) return false; 
		return true;
	});

	$(document).on('configRule.validator',function(e){
		$(e.target).find('input[data-validator=SSN],[data-validator=SSN] input').addBack('input[data-validator=SSN],[data-validator=SSN] input').each(function(idx,ele){
			$(ele).rules('add',{
				checkSSN: true,
				messages: {
					checkSSN: "เลขบัตรประจำตัวประชาชนไม่ถูกต้อง"	
				}
			});
		});
	});

	$.validator.addMethod('validDateStart',function(value, element, params){
		 var notMoreThanCurrentDate = false;
		 var val = $(element).val();
		 if(''==val || undefined==val)
		 {
			 return true;
		 }
		 else
		 {
			 if(getDateGregorianFromThaiDate(val).getTime()<=new Date().getTime())
			 {
				 notMoreThanCurrentDate = true; 
			 }
			 else
			 {
				 return notMoreThanCurrentDate;
			 }
		 }
		 
		 var dataTargetVal = $('#'+$(element).closest('div[data-target]').attr('data-target')+' input').val();
		 if(''==dataTargetVal || undefined==dataTargetVal)
		 {
			 return getDateGregorianFromThaiDate(val).getTime()<=new Date().getTime();
		 }
		 return getDateGregorianFromThaiDate(val).getTime()<=getDateGregorianFromThaiDate(dataTargetVal).getTime();
	}, jQuery.validator.format("วันที่เริ่มต้นต้องไม่เกินวันที่ปัจจุบันและวันที่สิ้นสุด"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validDateStart] input').addBack('[data-validation-rules=validDateStart] input').each(function(idx,ele){
			$(ele).rules('add',{
				validDateStart: [true]
			});
		});
	});
	
	$.validator.addMethod('validDateEnd',function(value, element, params){
		 var notMoreThanCurrentDate = false;
		 var val = $(element).val();
		 if(''==val || undefined==val)
		 {
			 return true;
		 }
		 else
		 {
			 if(getDateGregorianFromThaiDate(val).getTime()<=new Date().getTime())
			 {
				 notMoreThanCurrentDate = true; 
			 }
			 else
			 {
				 return notMoreThanCurrentDate;
			 }
		 }
		
		var dataTargetVal = $('#'+$(element).closest('div[data-target]').attr('data-target')+' input').val();
		if(''==dataTargetVal || undefined==dataTargetVal)
		{
			return true;
		}
			
		return getDateGregorianFromThaiDate(val).getTime()>=getDateGregorianFromThaiDate(dataTargetVal).getTime();
	}, jQuery.validator.format("วันที่สิ้นสุุดต้องไม่เกินวันที่ปัจจุบันและไม่น้อยกว่าวันที่เริ่มต้น"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validDateEnd] input').addBack('[data-validation-rules=validDateEnd] input').each(function(idx,ele){
			$(ele).rules('add',{
				validDateEnd: [true]
			});
		});
	});
	
	$.validator.addMethod('validDateStartIgnoreCurrenDate',function(value, element, params){
		 var val = $(element).val();
		 
		 if(''==val || undefined==val)
		 {
			 return true;
		 }
		 var dataTargetVal = $('#'+$(element).closest('div[data-target]').attr('data-target')+' input').val();
		 if(''==dataTargetVal || undefined==dataTargetVal)
		 {
			 return true;
		 }
		 return getDateGregorianFromThaiDate(val).getTime()<=getDateGregorianFromThaiDate(dataTargetVal).getTime();
	}, jQuery.validator.format("วันที่เริ่มต้นต้องไม่เกินวันที่สิ้นสุุด"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validDateStartIgnoreCurrenDate] input').addBack('[data-validation-rules=validDateStartIgnoreCurrenDate] input').each(function(idx,ele){
			$(ele).rules('add',{
				validDateStartIgnoreCurrenDate: [true]
			});
		});
	});
	
	$.validator.addMethod('validDateEndIgnoreCurrenDate',function(value, element, params){
		 var val = $(element).val();
		 if(''==val || undefined==val)
		 {
			 return true;
		 }
		var dataTargetVal = $('#'+$(element).closest('div[data-target]').attr('data-target')+' input').val();
		if(''==dataTargetVal || undefined==dataTargetVal)
		{
			return true;
		}
		return getDateGregorianFromThaiDate(val).getTime()>=getDateGregorianFromThaiDate(dataTargetVal).getTime();
	}, jQuery.validator.format("วันที่สิ้นสุุดต้องไม่น้อยกว่าวันที่เริ่มต้น"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validDateEndIgnoreCurrenDate] input').addBack('[data-validation-rules=validDateEndIgnoreCurrenDate] input').each(function(idx,ele){
			$(ele).rules('add',{
				validDateEndIgnoreCurrenDate: [true]
			});
		});
	});
	
	$.validator.addMethod('notMoreThanBirthdateAndCurrentDate',function(value, element, params){
		 var notMoreThanBirthdateAndCurrentDate = false;
		 var val = $(element).val();
		 if(''==val || undefined==val)
		 {
			 return true;
		 }
		 else
		 {
			 if(getDateGregorianFromThaiDate(val).getTime()<=new Date().getTime())
			 {
				 notMoreThanBirthdateAndCurrentDate = true; 
			 }
			 else
			 {
				 return notMoreThanBirthdateAndCurrentDate;
			 }
		 }
		
		var dataTargetVal = $('#'+$(element).closest('div[data-target]').attr('data-target')+' input').val();
		if(''==dataTargetVal || undefined==dataTargetVal)
		{
			return true;
		}
			
		return getDateGregorianFromThaiDate(val).getTime()>=getDateGregorianFromThaiDate(dataTargetVal).getTime();
	}, jQuery.validator.format("วันที่เสียชีวิตต้องไม่มากกว่าวันที่ปัจจุบันและไม่น้อยกว่าวันที่เกิด"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=notMoreThanBirthdateAndCurrentDate] input').addBack('[data-validation-rules=notMoreThanBirthdateAndCurrentDate] input').each(function(idx,ele){
			$(ele).rules('add',{
				notMoreThanBirthdateAndCurrentDate: [true]
			});
		});
	});
	
	$.validator.addMethod('notMoreThanDeathDateAndCurrentDate',function(value, element, params){
		 var notMoreThanDeathDateAndCurrentDate = false;
		 var val = $(element).val();
		 if(''==val || undefined==val)
		 {
			 return true;
		 }
		 else
		 {
			 if(getDateGregorianFromThaiDate(val).getTime()<=new Date().getTime())
			 {
				 notMoreThanDeathDateAndCurrentDate = true; 
			 }
			 else
			 {
				 return notMoreThanDeathDateAndCurrentDate;
			 }
		 }
		
		var dataTargetVal = $('#'+$(element).closest('div[data-target]').attr('data-target')+' input').val();
		if(''==dataTargetVal || undefined==dataTargetVal)
		{
			return true;
		}
			
		return getDateGregorianFromThaiDate(val).getTime()>=getDateGregorianFromThaiDate(dataTargetVal).getTime();
	}, jQuery.validator.format("วันที่รู้ถึงความตายต้องไม่มากกว่าวันที่ปัจจุบันและไม่น้อยกว่าวันที่เสียชีวิต"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=notMoreThanDeathDateAndCurrentDate] input').addBack('[data-validation-rules=notMoreThanDeathDateAndCurrentDate] input').each(function(idx,ele){
			$(ele).rules('add',{
				notMoreThanDeathDateAndCurrentDate: [true]
			});
		});
	});
	
	$.validator.addMethod('notMoreThanSevenHour',function(value, element, params){
		if(jQuery.trim($(element).val()).length==0)
		{
			return true;
		}
		return $(element).val() <= 7;
	}, jQuery.validator.format("ต้องระบุค่าไม่เกิน 7 ชั่วโมง"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=notMoreThanSevenHour] input').addBack('[data-validation-rules=notMoreThanSevenHour] input').each(function(idx,ele){
			$(ele).rules('add',{
				notMoreThanSevenHour: true
			});
		});
	});
	
	$(document).on('click.validation focus.validation', 'form[data-toggle="validator"]:not([novalidate])', function(e){
		initValidation(this);
	});
	
	$.validator.addMethod('validatePostalCode',function(value, element, params){
		var val = jQuery.trim($(element).val());
		 if(''==val || undefined==val || val.length==0 || val.length==5)
		 {
			 return true;
		 }

		return false;
	}, jQuery.validator.format("ต้องระบุค่าให้ครบ 5 หลัก"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validatePostalCode] input').addBack('[data-validation-rules=validatePostalCode] input').each(function(idx,ele){
			$(ele).rules('add',{
				validatePostalCode: true
			});
		});
	});
	
	$.validator.addMethod('notLessThanCurrentDate',function(value, element, params){
		 if(jQuery.trim($(element).val()).length==0)
		 {
			 return true;
		 }
		 var thaiDate = getDateGregorianFromThaiDate($(element).val());
		 if(thaiDate == null) return true;
		 var today = new Date();
		 today.setHours(0,0,0,0);
		 return thaiDate.getTime() >= today.getTime();
	}, jQuery.validator.format("วันที่ระบุต้องไม่น้อยกว่าวันที่ปัจจุบัน"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=notLessThanCurrentDate] input').addBack('[data-validation-rules=notLessThanCurrentDate] input').each(function(idx,ele){
			$(ele).rules('add',{
				notLessThanCurrentDate: true
			});
		});
	});
	
	$.validator.addMethod('validDateStartDebtAssetAuction',function(value, element, params){
		 var notMoreThanCurrentDate = false;
		 var val = $(element).val();
		 if(''==val || undefined==val)
		 {
			 return true;
		 }
		 else
		 {
			 if(getDateGregorianFromThaiDate(val).getTime()<=new Date().getTime())
			 {
				 notMoreThanCurrentDate = true; 
			 }
			 else
			 {
				 return notMoreThanCurrentDate;
			 }
		 }
		 
		 var dataTargetVal = $('#'+$(element).closest('div[data-target]').attr('data-target')+' input').val();
		 if(''==dataTargetVal || undefined==dataTargetVal)
		 {
			 return getDateGregorianFromThaiDate(val).getTime()<=new Date().getTime();
		 }
		 return getDateGregorianFromThaiDate(val).getTime()<=getDateGregorianFromThaiDate(dataTargetVal).getTime();
	}, jQuery.validator.format("วันที่ขายต้องไม่มากกว่าวันที่ได้รับเงินหรือไม่เกินวันปัจจุบัน"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validDateStartDebtAssetAuction] input').addBack('[data-validation-rules=validDateStartDebtAssetAuction] input').each(function(idx,ele){
			$(ele).rules('add',{
				validDateStartDebtAssetAuction: [true]
			});
		});
	});
	
	$.validator.addMethod('validDateEndDebtAssetAuction',function(value, element, params){
		 var notMoreThanCurrentDate = false;
		 var val = $(element).val();
		
		/* if(''==val || undefined==val)
		 {
			 return true;
		 }
		 else
		 {
			 if(getDateGregorianFromThaiDate(val).getTime()<=new Date().getTime())
			 {
				 notMoreThanCurrentDate = true; 
			 }
			 else
			 {
				 return notMoreThanCurrentDate;
			 }
		 }*/
		
		var dataTargetVal = $('#'+$(element).closest('div[data-target]').attr('data-target')+' input').val();
		if(''==dataTargetVal || undefined==dataTargetVal)
		{
			return true;
		}
			
		return getDateGregorianFromThaiDate(val).getTime()>=getDateGregorianFromThaiDate(dataTargetVal).getTime();
	}, jQuery.validator.format("วันที่ได้รับเงินต้องไม่น้อยกว่าวันที่ขาย"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validDateEndDebtAssetAuction] input').addBack('[data-validation-rules=validDateEndDebtAssetAuction] input').each(function(idx,ele){
			$(ele).rules('add',{
				validDateEndDebtAssetAuction: [true]
			});
		});
	});
	
	$.validator.addMethod('validStartYear',function(currentVal, element, params){
		var dataTargetVal = $('#'+($(element).closest('div[data-target]').attr('data-target'))+' select').val();
		return currentVal<=dataTargetVal;
	}, jQuery.validator.format("ปีเริ่มต้นต้องไม่มากกว่าปีสิ้นสุด"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validStartYear] select').addBack('[data-validation-rules=validStartYear] select').each(function(idx,ele){
			$(ele).rules('add',{
				validStartYear: [true]
			});
		});
	});
	
	$.validator.addMethod('validEndYear',function(currentVal, element, params){
		var dataTargetVal = $('#'+($(element).closest('div[data-target]').attr('data-target'))+' select').val();
		return currentVal>=dataTargetVal;
	}, jQuery.validator.format("ปีสิ้นสุดไม่น้อยกว่าปีเริ่มต้น"));
	$(document).on('configRule.validator',function(e){
		$(e.target).find('[data-validation-rules=validEndYear] select').addBack('[data-validation-rules=validEndYear] select').each(function(idx,ele){
			$(ele).rules('add',{
				validEndYear: [true]
			});
		});
	});
	
})();

function initValidation(formNativeElement, callback) {
	var form = $(formNativeElement);
	if (form.data('validator')) return;
//	console.log('init jqueryformvalidator for ', formNativeElement);
	var validator = form.validate({
		submitHandler:function(nativeFormElement,event){
			if (validator.settings.debug){
				return;
			}
			if(callback){
				callback(nativeFormElement);return;
			}
			form.find('input[type=submit],button:not(.btn-report),a').attr('disabled','disabled');
			$(nativeFormElement).trigger('submit.form-plugin');
		},
		invalidHandler:function(event,validator){
			var nativeFormElement = this;
			var totalError = validator.numberOfInvalids();
			if (totalError !== 0) {
				
			}
		},
		ignore:':disabled',
		onsubmit:true,
		focusInvalid:true,
		errorClass: 'help-block',
		errorElement: 'span',
		errorPlacement: function(error, element) {
			error.data('validatorCreated',true);
			// this element has been replace with select2 the placement will be after the select2.
			if (element.next().is('span.select2')) {
				element = element.next();
			}
			// this element has been replace with CKEDITOR the placement will be after the cke.
			if (element.next().is('div.cke')) {
				element = element.next();
			}
			// this element was in bootstrap's input-group the placement will be after the input-group.
			if (element.parent().is('.input-group')){
				element = element.parent();
			}
			element.after(error);
		},
		success: function(label, nativeInput){
		},
		highlight: function(nativeInput, errorClass, validClass) {
			$(nativeInput).closest('.form-group').addClass('has-error');
		},
		unhighlight: function(nativeInput, errorClass, validClass) {
			if ($(nativeInput).is('.select2-search__field')) return;// select2 append input.
			$(nativeInput).closest('.form-group').removeClass('has-error');
			$(nativeInput).closest('.form-group').find('help-block').filter(function(idx,ele){
				return !$(ele).data('validatorCreated');
			}).remove();
		},
		ignoreTitle: false
	});
	validator.focusInvalid = function() {
	    if ( validator.settings.focusInvalid ) {
	    	if (validator.errorList.length > 0) {
	            var firstInvalidElement = $(validator.errorList[0].element);
	            firstInvalidElement.scrollToMe(true);
	            firstInvalidElement.promise().done(function(){
	            	firstInvalidElement.trigger('focus');		            			            	
	            });
	    	}
	    }
	}
	form.data('validator',validator);
	form.on('click', 'input[type=submit],button:not([type=button]):not([type=reset])', function(e){
		e.preventDefault();
//		console.log(e.delegateTarget);
		if (form.data('hiddenButtonSubmit')) {
			form.data('hiddenButtonSubmit').remove();
		}
		if ($(this).attr('name')) {
			var button = $('<input type="hidden"/>');
			button.attr('name',$(this).attr('name'));
			button.val($(this).attr('value'));
			button.appendTo(form);
			form.data('hiddenButtonSubmit', button);
		}
		$(e.delegateTarget).trigger('submit.validate');
	});
	form.on('keypress', ':input:not(textarea):not([type=submit])', function(e){
		if (e.keyCode == 13) {
			e.preventDefault();
		}
	});
	form.trigger('configRule.validator');
	form.on('newElement', 'input,select,textarea,div[data-validation]', function(e){
		if (e.target !== this) {return;}
		$(e.target).validate(validator.settings);
		$(e.target).trigger('configRule.validator');
		$(e.target).removeAttr('novalidate');
	});
}


//init error
$(document).on('newElement', '[data-error-group]', function(e){
	if (e.target !== this) {return;}
	$(this).removeAttr('data-error-group').parent('form-group').removeClass('has-error').addClass('has-error');
});

$.fn.hideForm = function() {
	this.addClass('hidden').attr('data-hide-form','');
	$('input:not(:disabled),select:not(:disabled)', this).attr('data-disabled-by-hide-form','').prop('disabled',true);
}

$.fn.showForm = function() {
	this.removeClass('hidden').removeAttr('data-hide-form');
	$('[data-disabled-by-hide-form]',this).filter(function(idx,ele){
		return $(ele).closest('[data-hide-form]').length === 0
	}).removeAttr('data-disabled-by-hide-form').prop('disabled',false);
}

$(document).on('expand.scrollToMe', '.tab-pane:not(.active)', function(e, promiseArray){
	if (e.isDefaultPrevented()) return;
	// auto select tab that requestFocus
	var defer = $.Deferred();
	promiseArray.push(defer.promise());
	var id = $(this).attr('id');
	$('[data-toggle="tab"][href="#'+id+'"]').one('shown.bs.tab', function(e){
		defer.resolve("tab shown");
	}).trigger('click');
});
$(document).on('expand.scrollToMe', '.panel-collapse:hidden', function(e, promiseArray){
	if (e.isDefaultPrevented()) return;
	// auto expand the collapsed panel
	promiseArray.push($(this).promise());
	$(this).prev('.panel-heading').find('.btn-minimize .glyphicon-chevron-down').trigger('click');
});

$.fn.scrollToMe = function(animated) {
	var _this = this;
	var promiseArray = [];
	var defer = $.Deferred();
	defer.promise(_this);
	_this.trigger('expand.scrollToMe',[promiseArray]);
	if(this.closest('.modal').length > 0) return;
	$.when.apply($, promiseArray).done(function(){
		var offsetTop = _this.offset().top - $(window).height() / 2 + _this.height() / 2;
		if (animated) {
			$('html,body').animate({scrollTop: offsetTop}, 500);
		} else {
			$('html,body').scrollTop(offsetTop);
		}
		defer.resolve();
	});
}