$.fn.setText = function (val) {
    this.each(function (index, element) {
        switch (element.tagName.toLowerCase()) {
            case 'input':
                var type = $(element).attr('type');
                if (type === undefined) {
                    type = 'text';
                }
                switch (type.toLowerCase()) {
                    case 'radio':
                    case 'checkbox':
                        if ($(this).val() == val) {
                            $(this).prop('checked', true);
                        } else {
                            $(this).prop('checked', false);
                        }
                        return;
                    default:
                        break;
                }
            case 'select':
            case 'textarea':
                $(element).val(val);
                return;
            default:
                $(element).text(val);
                return;
        }
    });
}
$.fn.getText = function () {
    if (this.length === 0) { return undefined; }
    var element = this[0];
    switch (element.tagName.toLowerCase()) {
        case 'input':
        case 'select':
        case 'textarea':
            return $(element).val();
        default:
            return $(element).text();
    }
};

$.liveFormat = {};
$(document).on('focus', '[data-live-format]', function (e) {
    $.liveFormat[$(e.target).data('liveFormat')].init.call(e.target, e);
});
$(document).on('blur', '[data-live-format]', function (e) {
    $(this).off('.liveFormat');
    $.liveFormat[$(e.target).data('liveFormat')].deinit.call(e.target, e);
});

$.dataFormatFormatter = {};
$.dataFormat = function (context) {
    $('[data-tnd-format]', context).addBack('[data-tnd-format]').each(function (index, formattedElement) {
        $(formattedElement).setText($.dataFormatFormatter[$(formattedElement).data('tndFormat')]($(formattedElement).getText()));
    });
}
$.fn.dataFormat = function () {
    return $.dataFormat(this);
}
$(document).ready(function () {
    $.dataFormat();
});