﻿(function () {
    var notNumberRegExp = /[^\d,.]/g;
    var initText;
    $.liveFormat.money = {
        init: function (e) {
            initText = $(e.target).val();
            $(e.target).on('change.liveFormat', function (e) {
                e.stopImmediatePropagation();
                e.preventDefault();
            });
            $(e.target).on('keypress.liveFormat', function (e) {
                var char = String.fromCharCode(e.which);
                if (char.search(notNumberRegExp) < 0) {
                    if (char === '.' && $(e.target).val().indexOf('.') >= 0){
                        e.stopImmediatePropagation();
                        if ($(e.target).val().indexOf('.') >= 0) {
                          e.preventDefault();
                          e.target.selectionEnd = e.target.selectionStart = $(e.target).val().indexOf('.') + 1;
                        }
                    } else {
                        $(e.target).trigger('textChange');
                    }
                } else {
                    e.stopImmediatePropagation();
                    e.preventDefault();
                }
            });
            $(e.target).on('paste.liveFormat cut.liveFormat', function (e) {
                $(e.target).trigger('textChange');
            });
            $(e.target).on('textChange.liveFormat', function (e) {
                var valueBefore = $(e.target).val();
                
                setTimeout(function () {
                    var oldVal = $(e.target).val();
                    var oldDotIdx = oldVal.indexOf('.');
                    oldDotIdx = oldDotIdx >= 0 ? oldDotIdx : oldVal.length;
                    var selection = e.target.selectionStart;
                    var valueAfter = $(e.target).val();
                    if (valueAfter.search(notNumberRegExp) < 0) {
                        $(e.target).val(getFormattedText(valueAfter.replace(notNumberRegExp, '')));
                    } else {
                        $(e.target).val(valueBefore);
                    }
                    e.target.selectionStart = selection - oldDotIdx + $(e.target).val().indexOf('.');
                    e.target.selectionEnd = e.target.selectionStart;
                }, 0);
            });
        },
        deinit: function (e) {
            var valueAfter = $(e.target).val();
            if (valueAfter.search(notNumberRegExp) < 0) {
                $(e.target).val(getFormattedText(valueAfter.replace(notNumberRegExp, '')));
            }
            if ($(e.target).val() !== initText) {
                $(e.target).trigger('change');
            }
            lastText = undefined;
        }
    }

    $.dataFormatFormatter.money = function (text) {
        return getFormattedText(text);
    };

    function getFormattedText(val) {
        if (val.length === 0) { return val; }
        var text = val.replace(/,/g, '');
        if (isNaN(text)) { return val; }
        var text = (Math.floor((+text) * 100) / 100).toFixed(2);
        for (var i = text.indexOf('.') - 3; i > 0; i -= 3) {
            text = text.slice(0, i) + ',' + text.slice(i);
        }
        return text;
    }
})();