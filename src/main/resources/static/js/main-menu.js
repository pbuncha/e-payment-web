$(document).ready(function() {
	$('.navbar-toggle').click(function (e) {
        e.preventDefault();
        $('.nav-sm').html($('.navbar-collapse').html());
        $('.sidebar-nav').toggleClass('active');
        $(this).toggleClass('active');
    });
	
	var $sidebarNav = $('.sidebar-nav');

    // Hide responsive navbar on clicking outside
    $(document).mouseup(function (e) {
        if (!$sidebarNav.is(e.target) // if the target of the click isn't the container...
            && $sidebarNav.has(e.target).length === 0
            && !$('.navbar-toggle').is(e.target)
            && $('.navbar-toggle').has(e.target).length === 0
            && $sidebarNav.hasClass('active')
            )// ... nor a descendant of the container
        {
            e.stopPropagation();
            $('.navbar-toggle').click();
        }
    });
    
    
    $('.notification-popper').popover({
	    placement: 'bottom',
	    container: 'body',
	    html: true,
	    title: 'แจ้งเตือน',
	    content: function() {
	        return $(this).next('.notification-popper-content').html();
	    },
	    template: '<div class="popover notification-popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title" style="font-size: 18px;"></h3><div class="popover-content"></div></div>'
	});
	
	$(document).on('click', function(e) {
	  $('a.notification-popper[data-toggle="popover"]').each(function() {
	    //the 'is' for buttons that trigger popups
	    //the 'has' for icons within a button that triggers a popup
	    if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
	      $(this).popover('hide').data('bs.popover').inState.click = false // fix for BS 3.3.6
	    }

	  });
	});
    
});