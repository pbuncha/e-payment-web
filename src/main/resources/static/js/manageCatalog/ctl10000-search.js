(function() {
	'use strict';

	var app = angular.module("ctl10000SearchApp", ['httpProvider','ui.bootstrap','angucomplete','ngAnimate','datepickerApp','ngMaterial']);

	app.factory("$confirmDialog", function($uibModal){
		return function(){
			return $uibModal.open({
				animation: true,
				backdrop: 'static',
				size: 'sm',
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				template: "<div class='modal-header'>\
					<h3 class='modal-title' id='modal-title'><b>ต้องการลบข้อมูล?</b></h3>\
					</div>\
					<div class='modal-body text-center' id='modal-body'>\
					<button class='btn btn-primary' type='button' ng-click='ok()'>ตกลง</button>\
					<button class='btn btn-warning' type='button' ng-click='cancel()'>ยกเลิก</button>\
					</div>",
					controller: function($scope, $uibModalInstance){

						$scope.ok = function() {
							return $uibModalInstance.close(true);
						};

						$scope.cancel = function() {
							return  $uibModalInstance.dismiss(false);
						};

					}
			});
		};
	});

	app.factory("dataFactory", function (httpProvider, $q,reportFactory) {
		var urlBase = ctx+'/manageCatalogController';
		var dataFactory = {};

		dataFactory.getMRevenueType = function () {
			return  httpProvider().get(urlBase+'/getMRevenueType');
		};

		dataFactory.getdeptList = function () {
			return  httpProvider().get(ctx+'/manageInvoice/getdeptList');
		};

		dataFactory.pagination = function (criteria) {
			return  httpProvider().post(urlBase+'/pagination',criteria);
		};
		dataFactory.deleteCatalog = function ( itemList ) {
			return  httpProvider().post(urlBase+'/removeCatalog', itemList);
		};
		dataFactory.getStatusUse = function ( ) {
			return  httpProvider().get(urlBase+'/getStatusUse');
		}
		dataFactory.getCatalogAllByStatus = function () {
			return  httpProvider().get(urlBase+'/getCatalogAllByStatus');
		};
		dataFactory.removeCatalogById = function (id) {
			return  httpProvider().get(urlBase+'/removeCatalogById/'+ id);
		};

		dataFactory.exportReport = function (criteria){
			reportFactory.post(urlBase + '/exportExcel', criteria);
		}
		
		dataFactory.editCatalog = function (id){
			window.location.href = urlBase+'/editCatalog/'+id;
		}
		dataFactory.copyCatalog = function (copyCatalog){
			return  httpProvider().post(urlBase+'/copyCatalog', copyCatalog);
		}
		dataFactory.mCatalogType = function (copyCatalog){
			return  httpProvider().get(urlBase+'/getMCatalogType');
		}
		

		return dataFactory;
	});

	app.controller("ctl10000SearchCtrl", function ($log,$scope,dataFactory, $confirmDialog,$uibModal,$mdDialog) {
		$log.log('Master : Department List');
		$scope.selectItem = [];

		$scope.catalogList =[];
		$scope.criteria ={};
		
		dataFactory.mCatalogType().then(function (response) {
			console.log("mCatalogType : "+response);
			$scope.mCatalogList = response;
		},function(error){
			$log.error('data error', response);
		});

		dataFactory.getdeptList().then(function (response) {
			console.log(response);
			$scope.deptList = response;
		},function(error){
			$log.error('data error', response);
		});

		$log.log('getMRevenueType');
		dataFactory.getMRevenueType().then(function (response) {
			console.log(response);
			$scope.mRevenueTypeList = response;
		},function(error){
			$log.error('data error', response);
		});

		dataFactory.getStatusUse().then(function (response) {
			console.log(response);
			$scope.statusUseList = response;
		},function(error){
			$log.error('data error', response);
		});
		$scope.statusApproveList = [
		                            {id: '1', desc: 'อนุมัติ'},
		                            {id: '2', desc: 'ไม่อนุมัติ'}
		                            ];
		
		$scope.editCatalog = function(id){
			dataFactory.editCatalog(id).then(function (response) {
				
			}, function (error) {
				
			});
		}
		
		function showAlert (textContent) {
			$mdDialog.show(
					$mdDialog.alert()
					.clickOutsideToClose(true)
					.title('Error')
					.textContent(textContent)
					.ok('OK')
			);
		}

		$scope.exportExcel = function(dataList){
			if(dataList.length == 0){
				showAlert("กรุณาเลือกรายการ");
			}else{
				dataFactory.exportReport(dataList);
				$scope.selectItem = [];
				$scope.isSelectAll = false;
			}
		}

		$scope.search = function() {
			console.log($scope.pendingType);
			
			if($scope.pendingType){
				$scope.criteria.pending = "2";
			}else{
				$scope.criteria.pending = undefined;
			}
			dataFactory.pagination($scope.criteria).then(function (response) {
				$scope.catalogList = response.content;
				$scope.totalItems = $scope.catalogList.length;
				$scope.figureOut();

			}, function (error) {
				$scope.status = 'Unable to load customer data: ' + error.message;
			});
		};
		$scope.search();
		$scope.sliceCatalogTempList = [];
		$scope.perPages = 10;
		$scope.totalItems = $scope.catalogList.length;
		$scope.currentPage = 1;
		
		$scope.values = [10,15,20,25];
		
		$scope.figureOut = function(){
			var begin = (($scope.currentPage - 1) * $scope.perPages);
			var end = begin + $scope.perPages;
			$scope.from = begin;
			$scope.to = end;
			$scope.sliceCatalogTempList = $scope.catalogList.slice(begin, end);
		}

		$scope.pageChanged = function() {
			$scope.figureOut();
		};
		
		$scope.orderByField = '';
		$scope.reverseSort = false;
		$scope.orderFunction = function(field) {
			$scope.orderByField = field;
			$scope.reverseSort = !$scope.reverseSort;		
		}
		$scope.checkField = function(name) {
			if ($scope.orderByField == name) {
				return true;
			} else {
				return false;
			}
		}

		$scope.deleteStructureItem = function( list ){
			console.log(list);
			$confirmDialog()['result']['then'](function(ok){
				dataFactory.deleteCatalog(list).then(function(){
					$scope.search();
				});
			},function(cancel){
				/* Cancel*/
			});
		}
		$scope.removeCatalogById = function( id,check ){
			console.log(id);
			console.log(check);
			console.log($scope.index);
			if(eval(check)==true){
				$confirmDialog()['result']['then'](function(ok){
					dataFactory.removeCatalogById(id).then(function(){
						$scope.search();
					});
				},function(cancel){
					/* Cancel*/
				});
			}
		}

		$scope.selectToDel = function( item ){
			var index = $scope.isDelete(item.id);
			if(index > -1 ){
				$scope.selectItem.splice(index,1);
			}else{
				$scope.selectItem.push(item);
			}
		}

		$scope.isDelete = function( id ){
			for( var i =0 ; i<$scope.selectItem.length; i++ ){
				if( $scope.selectItem[i].id == id ){
					return i;
				}
			}
			return -1;
		}

		$scope.selectAll = function(isSelectAll){
			console.log(isSelectAll);
			if( isSelectAll ){
				$scope.selectItem = angular.copy($scope.sliceCatalogTempList);
			}else{
				$scope.selectItem = [];
			}

		};

		$scope.clearData = function() {
			$scope.catalogList = [];
			$scope.criteria ={};
			$scope.isSelectAll = false;
			$scope.sliceCatalogTempList = [];
			$scope.perPages = 10;
			$scope.totalItems = $scope.catalogList.length;
			$scope.currentPage = 1;
			$scope.selectItem = [];
		};
		
		$scope.addCatalogModal = function(){
			
			$uibModal.open({
				controller: addCatalogModalController,
				templateUrl: ctx +'/templates/views/manageCatalog/modal-add-catalog.html', 
			})
			.result.then(function(answer) {
				
//				console.log(answer);
			}, function() {
				console.log('closed');
			});
		};

		function addCatalogModalController($scope,$uibModal,$uibModalInstance,$window){
			
			$scope.choseCat='1';
			$scope.$watch('choseCat', function(value) {
			       console.log(value);
			       if(value==1){
			    	   $scope.test(true);
			       }else{
			    	   $scope.test(false);
			       }
			    
			 });
			
			
			dataFactory.getCatalogAllByStatus().then(function (response) {
//				console.log('response ', response);
				$scope.catalogAutoList = response;

			}, function (error) {
				$scope.status = 'Unable to load  data: ' + error.message;
			}); 

			$scope.cancel = function() {
				$uibModalInstance.dismiss('cancel');
			};

			$scope.submit = function() {
				if($scope.choseCat == 1){
					$window.location.href = '../manageCatalogController/create?choseCat='+$scope.choseCat;
				}else{
//					$scope.copyCatalog = {};
					console.log('selectedCatalog : ', $scope.selectedCatalog.originalObject);
					$scope.copyCatalog  = 	$scope.selectedCatalog.originalObject;
					dataFactory.copyCatalog($scope.copyCatalog).then(function (response) {
						console.log('response ', response);
						$window.location.href = ctx+'/manageCatalogController/search';

					}, function (error) {
						$scope.status = 'Unable to load  data: ' + error.message;
					}); 
					
				}
				$uibModalInstance.close();
			};
		}

	});
	
	app.factory('reportFactory',function($http){
		function strToArraybuffer(str, fileType){
			var binary_string =  window.atob(str);
			var bytes = new Uint8Array( binary_string.length );
			for (var i = 0; i < binary_string.length; i++){
				bytes[i] = binary_string.charCodeAt(i);
			}
			var file = new Blob([bytes.buffer], {type: fileType});
			var fileURL = URL.createObjectURL(file);
			window.open(fileURL);
		}

		return {
			get: function(url, params ){
				$http.get(url, params ).then(function(data) {
					strToArraybuffer(data['data']['file'], data['data']['fileType']);
				});
			},
			post: function(url, params ){
				$http.post(url, params ).then(function(data) {
					strToArraybuffer(data['data']['file'], data['data']['fileType']);
				});
			},

		}
	});


})(angular);