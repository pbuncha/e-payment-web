var app = angular.module("ctl20000CreateApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp','dualmultiselect','ngMaterial','filter.util' ]);

app.factory("dataFactory", function (httpProvider) {
	var urlBase = ctx+'/manageCatalogController';
	var dataFactory = {};

	dataFactory.getMCatalogType = function () {
		return  httpProvider().get(urlBase+'/getMCatalogType');
	};

	dataFactory.getMRevenueType = function () {
		return  httpProvider().get(urlBase+'/getMRevenueType');
	};

	dataFactory.getMLevyType = function () {
		return  httpProvider().get(urlBase+'/getMLevyType');
	};

	dataFactory.getCostCenter = function () {
		return  httpProvider().get(urlBase+'/getCostCenter');
	};

	dataFactory.saveCatalog = function (catalog) {
		return  httpProvider().post(urlBase+'/saveCatalog',catalog);
	};

	dataFactory.getCostCenterByCode = function (costCenterCode) {
		return httpProvider().get(urlBase+'/getCostCenterByCode/'+costCenterCode);
	};

	dataFactory.getDepartmentByCode = function (departmentCode) {
		return httpProvider().get(urlBase+'/getDepartmentByCode/'+departmentCode);
	};

	dataFactory.getDepartmentById = function (departmentId) {
		return httpProvider().get(urlBase+'/getDepartmentById/'+departmentId);
	};

	dataFactory.getGlByCode = function (glCode) {
		return httpProvider().get(urlBase+'/getGlByCode/'+glCode);
	};

	dataFactory.getAccountDepositByCode = function (accountDepositCode) {
		return httpProvider().get(urlBase+'/getAccountDepositByCode/'+accountDepositCode);
	};

	dataFactory.getStandardRevernueByGlCode = function (glCode) {
		return httpProvider().get(urlBase+'/getStandardRevernueByGlCode/'+glCode);
	};

	dataFactory.getMMoneySourceByGlCode = function (glCode) {
		return httpProvider().get(urlBase+'/getMMoneySourceByGlCode/'+glCode);
	};

	return dataFactory;
});

app.controller("ctl20000CreateCtrl", function ($log,$scope,dataFactory,$uibModal,$document,$window,$mdDialog) {

	$scope.catalog = {};
	$scope.catalog.conditionText = [];	
	$scope.catalog.costCenter = [];	
	$scope.catalog.structureItem = [];
	$scope.catalog.deleteItem = [];
	$scope.catalog.deleteText = [];
	$scope.regexNumber = /^\d+$/;
	$scope.conditionList = [];
	$scope.costCenterOptions = {
			orderProperty: 'desc',
			items: $scope.costCenterList,
			selectedItems: []
	};	

	$scope.getDesc = function(id,list){
		var text = "";
		if(list==undefined){
			list = [];
		}
		var objArr = angular.fromJson(list).filter(function(item) {
			if (item.id === id) {
				return true;
			}
		});
		if(objArr.length != 0){
			text = objArr[0].desc;
		}
		return  text;
	};

	$log.log('getMCatalogType');
	dataFactory.getMCatalogType().then(function (response) {
		console.log(response);
		$scope.mCatalogTypeList = response;
	}, function (error) {

	});

	$log.log('getMRevenueType');
	dataFactory.getMRevenueType().then(function (response) {
		console.log(response);
		$scope.mRevenueTypeList = response;	
		$scope.mRevenueTypeListPopup = angular.copy($scope.mRevenueTypeList);	
		//hard code
		$scope.mRevenueTypeListPopup.splice(2,1);
	}, function (error) {

	});


	$log.log('getMLevyType');
	dataFactory.getMLevyType().then(function (response) {
		console.log(response);
		$scope.mLevyTypeList = response;
	}, function (error) {

	});
	
	function showAlert (textContent) {
		$mdDialog.show(
				$mdDialog.alert()
				.clickOutsideToClose(true)
				.title('Error')
				.textContent(textContent)
				.ok('OK')
		);
	}


	//EDIT
	if(catalog != null && catalog != ""){
		console.log("catalog : ",catalog);
		$scope.catalog = catalog;
		$scope.catalog.startDate =  new Date(catalog.startDate);

		if(catalog.endDate!=null){
			$scope.catalog.endDate =  new Date(catalog.endDate);
		}
		if(catalog.breakDate!=null){
			$scope.catalog.breakDate =  new Date(catalog.breakDate);
		}

		$scope.conditionList = catalog.conditionText;

		if(catalog.structureItem!=null){
			$scope.catalog.structureItem = catalog.structureItem;
		}else{
			$scope.catalog.structureItem = [];
		}

		if(catalog.deleteItem!=null){
			$scope.catalog.deleteItem = catalog.deleteItem;
		}else{
			$scope.catalog.deleteItem = [];
		}

		if(catalog.deleteText!=null){
			$scope.catalog.deleteText = catalog.deleteText;
		}else{
			$scope.catalog.deleteText = [];
		}

		if(catalog.isBreak == "1"){
			$scope.catalog.isBreak = true;
		}
		if(catalog.amount != null){
			$scope.catalog.isAmount = true;
		}

		dataFactory.getCostCenter().then(function (response) {
			$scope.costCenterList = response;
			$scope.items = [];
			$scope.selectedItems = [];

			var check = false;
			for(var i in $scope.costCenterList){
				var check = false;
				for(var j in catalog.costCenterList){
					if($scope.costCenterList[i].id==catalog.costCenterList[j].id){
						console.log("finally finished 1 ",	$scope.costCenterList[i]);
						$scope.selectedItems.push($scope.costCenterList[i]);
						check = true;
					}
				}
				if (!check) {
					$scope.items.push($scope.costCenterList[i]);
				}

			}



			$scope.costCenterOptions = {
					orderProperty: 'desc',
					items: $scope.items,
					selectedItems: $scope.selectedItems
			};	

		}, function (error) {

		});

	}else{
		$log.log('getCostCenter');
		dataFactory.getCostCenter().then(function (response) {
			console.log("getCostCenter : ",response);	
			$scope.costCenterList = response;
			$scope.costCenterOptions = {
					orderProperty: 'desc',
					items: $scope.costCenterList,
					selectedItems: []
			};	
		}, function (error) {

		});
	}


	$scope.clearAmount = function() {
		if($scope.catalog.isAmount == false){
			$scope.catalog.amount="";
		}
	};

	$scope.alertMessage = "";
	$scope.checkRevenue = function() {
		if($scope.catalog.structureItem != null && $scope.catalog.structureItem !=""){

			if($scope.catalog.revenueTypeId != $scope.catalog.structureItem[0].revenueTypeId && $scope.catalog.revenueTypeId != 3){
				showAlert("กรุณาลบข้อมูลโครงสร้างรายได้ก่อนทำการเปลี่ยนแปลงประเภทการจัดเก็บ");
				//$scope.alertMessage = "กรุณาลบข้อมูลโครงสร้างรายได้ก่อนทำการเปลี่ยนแปลงประเภทการจัดเก็บ"
				$scope.catalog.revenueTypeId = $scope.catalog.structureItem[0].revenueTypeId;

			}else{
				$scope.alertMessage = "";
			}
		}
	};

	$scope.addCondition = function() {
		$scope.condition = {};
		$scope.condition.conditionText = "";
		$scope.conditionList.push($scope.condition);
		$scope.catalog.conditionText = angular.copy($scope.conditionList);
	};


	//	REMOVE A SPECIFIC TABLE ROW
	$scope.removeRow = function(rowId, rowData){
		var rows = eval( rowData );
		// ROW MORE THAN INDEX, SPLICE THIS INDEX TO NEXT INDEX
		if( rows.length >= rowId ){
			rows.splice( rowId, 1 );
		}
	};


	var $ctrl = this;
	$log.log('Modal : addItem');
	$ctrl.open = function (index, size, parentSelector) {
		var data = {};
		if(index !=null){
			data = $scope.catalog.structureItem[index];
		}
		var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
				var modalInstance = $uibModal.open({
					animation: true,
					templateUrl: ctx +'/templates/views/manageCatalog/ctl20001-item.html',
					controller: 'ctl20001ItemCtrl',
					controllerAs: '$ctrl',
					backdrop: 'static',
					keyboard: false,
					size: size,
					appendTo: parentElem,
					resolve: {
						mRevenueTypeListPopup: function () {
							return $scope.mRevenueTypeListPopup;
						},
						revenueTypeId: function () {
							return $scope.catalog.revenueTypeId;
						},
						structureItemList: function () {
							return $scope.catalog.structureItem;
						},
						data : data
					}
				});

				modalInstance.result.then(function (structureItem) {
					if(index !=null){
						$scope.catalog.structureItem[index] = structureItem;
					}else{
						$scope.catalog.structureItem.push(structureItem);
					}
					$log.log($scope.catalog.structureItem);
				}, function () {
					$log.info('Modal dismissed at: ' + new Date());
				});
	};

	$scope.deleteCondition = function(index) {

		$scope.catalog.deleteText.push($scope.catalog.conditionText[index])
		$scope.conditionList.splice(index,1);
		$scope.catalog.conditionText = angular.copy($scope.conditionList);
		$log.log($scope.catalog.conditionText);
	};

	$scope.deleteStructureItem = function(index) {

		$scope.catalog.deleteItem.push($scope.catalog.structureItem[index])
		$scope.catalog.structureItem.splice(index,1);

	};

	$scope.isSubmit = false;	

	$scope.saveCatalog = function(form,status,chk) {
		$scope.isSubmit = true;	
		console.log(form);
		if(form.$valid){
			console.log($scope.catalog);
			$scope.catalog.status = status;
			$scope.catalog.startDate = Date.parse($scope.catalog.startDate);
			$scope.catalog.endDate = Date.parse($scope.catalog.endDate);
			$scope.catalog.breakDate = Date.parse($scope.catalog.breakDate);
			$scope.costCenterOptions.selectedItems
			console.log("costCenterOptions : "+$scope.costCenterOptions.selectedItems);

			$scope.catalog.costCenter = [];
			for(var i in $scope.costCenterOptions.selectedItems){
				$scope.catalog.costCenter.push({costCenterId:angular.copy($scope.costCenterOptions.selectedItems[i].id)});
				console.log($scope.catalog.costCenter[i].costCenterId);
			}

			for(var i in $scope.conditionList){
				$scope.catalog.conditionText = angular.copy($scope.conditionList);

			}
			console.log("conditionText : "+$scope.catalog.conditionText);
			console.log("costCenter : "+$scope.catalog.costCenter);
			var percent=0;
			for(var i in $scope.catalog.structureItem){
				percent += parseInt($scope.catalog.structureItem[i].percent);
			}

			if(percent<100 && chk == 1){
				showAlert("อัตราร้อยละไม่ถึง 100% ไม่สามารถส่งอนุมัติได้");
			}else if(percent>100){
				showAlert("อัตราร้อยละต้องไม่เกิน 100%");
			}else{
				//DO SAVE CATALOG
				dataFactory.saveCatalog($scope.catalog).then(function (response) {
					console.log(response);
					$window.location.href = ctx+'/manageCatalogController/search';
				}, function (error) {
					
				});
			}

		}
	};

	$scope.cancel = function() {
		$window.location.href = ctx+'/manageCatalogController/search';

	};

});

app.controller("ctl20001ItemCtrl", function ($uibModalInstance,mRevenueTypeListPopup,revenueTypeId,structureItemList,data,$log,dataFactory,$timeout) {
	var $ctrl = this;
	console.log("structureItemList.length : "+structureItemList.length);
	console.log(structureItemList);
	$ctrl.structureItem = angular.copy(data);
	$ctrl.mRevenueTypeListPopup = mRevenueTypeListPopup;

	$log.log('getCostCenterByCode');
	$ctrl.getCostCenterByCode = function (costCenterCode) {
		console.log("costCenterCode : "+costCenterCode);
		dataFactory.getCostCenterByCode(costCenterCode).then(function (response) {
			console.log(response);
			$ctrl.structureItem.costCenterId= response.costCenterId;
			$ctrl.structureItem.costCenterName = response.costCenterName;
			$ctrl.structureItem.departmentId= response.departmentId;
			$ctrl.structureItem.departmentCode = response.departmentCode;
			$ctrl.structureItem.departmentName = response.departmentName;
		}, function (error) {
			
		});
	};

	$log.log('getDepartmentByCode');
	$ctrl.getDepartmentByCode = function (departmentCode) {
		console.log("departmentCode : "+departmentCode);
		dataFactory.getDepartmentByCode(departmentCode).then(function (response) {
			console.log(response);
			$ctrl.structureItem.departmentId = response.departmentId;
			$ctrl.structureItem.departmentName = response.departmentName;
		}, function (error) {
			
		});
	};

	$log.log('getGlByCode');
	$ctrl.getGlByCode = function (glCode) {
		console.log("glCode : "+glCode);
		dataFactory.getGlByCode(glCode).then(function (response) {
			console.log(response);
			$ctrl.structureItem.glId = response.glId;
			$ctrl.structureItem.glName = response.glName;
		}, function (error) {
			
		});
	};

	$log.log('getAccountDepositByCode');
	$ctrl.getAccountDepositByCode = function (accountDepositCode) {
		console.log("accountDepositCode : "+accountDepositCode);
		dataFactory.getAccountDepositByCode(accountDepositCode).then(function (response) {
			console.log(response);
			$ctrl.structureItem.accountDepositId = response.accountDepositId;
			$ctrl.structureItem.accountDepositOwner = response.owner;
			if(response.accountName == null){
				$ctrl.structureItem.accountDepositName = "";
			}else{
				$ctrl.structureItem.accountDepositName = response.accountName;
			}

		}, function (error) {
			
		});
	};

	$log.log('getStandardRevernueByGlCode');
	$ctrl.getStandardRevernueByGlCode = function (glCode) {
		console.log("getStandardRevernueByGlCode : "+glCode);
		dataFactory.getStandardRevernueByGlCode(glCode).then(function (response) {
			console.log(response);
			$ctrl.structureItem.standardRevernueCode = response.standardRevernueCode;
			$ctrl.structureItem.standardRevernueName = response.standardRevernueName;
		}, function (error) {
			
		});
	};

	$log.log('getMMoneySourceByGlCode');
	$ctrl.getMMoneySourceByGlCode = function (glCode) {
		console.log("getMMoneySourceByGlCode : "+glCode);
		dataFactory.getMMoneySourceByGlCode(glCode).then(function (response) {
			console.log(response);
			$ctrl.structureItem.moneySourceCode = response.moneySourceCode;
			$ctrl.structureItem.moneySourceName = response.moneySourceName;
		}, function (error) {
			
		});
	};
	

	if(angular.equals(data, {})){
		$ctrl.structureItem = {};		
	}else{

		if($ctrl.structureItem.accountDepositCode != null){
			$ctrl.getAccountDepositByCode($ctrl.structureItem.accountDepositCode);
		}
		if($ctrl.structureItem.glCode != null){
			$ctrl.getStandardRevernueByGlCode($ctrl.structureItem.glCode);
			$ctrl.getMMoneySourceByGlCode($ctrl.structureItem.glCode);
		}

	}

	if(revenueTypeId!=null && revenueTypeId!=3){
		$ctrl.structureItem.revenueTypeId = revenueTypeId;
		$ctrl.locked = true;
	}

	$log.log('Modal - getMLevyType');
	dataFactory.getMLevyType().then(function (response) {
		console.log(response);
		$ctrl.mLevyTypeList = response;
	}, function (error) {
		
	});

	//onKeyUpHandler and onKeyDownHandler
	var timer;
	$ctrl.onKeyUpHandler = function(dataCode,type) {
		console.log("type : "+type);
		if(dataCode!=""){
			if(type==1){
				//getCostCenterByCode
				timer = $timeout($ctrl.getCostCenterByCode(dataCode), 1000); 
			}
			if(type==2){
				//getDepartmentByCode
				timer = $timeout($ctrl.getDepartmentByCode(dataCode), 1000); 
			}
			if(type==3){
				//getGlByCode
				timer = $timeout($ctrl.getGlByCode(dataCode), 1000); 
				//getStandardRevernueByGlCode
				timer = $timeout($ctrl.getStandardRevernueByGlCode(dataCode), 1000); 
				//getMMoneySourceByGlCode
				timer = $timeout($ctrl.getMMoneySourceByGlCode(dataCode), 1000); 
			}
			if(type==4){
				//getGlByCode
				timer = $timeout($ctrl.getAccountDepositByCode(dataCode), 1000); 
			}

		}
	};

	$ctrl.onKeyDownHandler = function() {
		$timeout.cancel(timer);
	};

	//hard code
	$ctrl.autoDepCode = function(levyTypeId) {
		if(levyTypeId==1){	
			dataFactory.getDepartmentById(1).then(function (response) {
				console.log(response);
				$ctrl.structureItem.departmentCode = response.departmentCode;
				$ctrl.onKeyUpHandler($ctrl.structureItem.departmentCode,2);
				$ctrl.structureItem.costCenterId= null;
				$ctrl.structureItem.costCenterCode= '';
				$ctrl.structureItem.costCenterName = '';
			}, function (error) {
				
			});	

		}else{
			$ctrl.structureItem.departmentId = '';
			$ctrl.structureItem.departmentCode = '';
			$ctrl.structureItem.departmentName = '';
		}
	};

	//hard code
	$ctrl.autoBudgetCode = function(costCenterCode) {
		$ctrl.structureItem.budgetCode = costCenterCode.substring(0,5);
	};

	$ctrl.clearData = function() {
		if($ctrl.structureItem.revenueTypeId == 1){
			$ctrl.structureItem.accountDepositCode = "";
			$ctrl.structureItem.accountDepositOwner = "";
			$ctrl.structureItem.accountDepositName = "";
		}

	};
	
	var countPercent = 0;
	if(structureItemList.length>0){
	
		if($ctrl.structureItem.percent==undefined){
			//only new popup
			for(var i = 0; i < structureItemList.length; i++){
				countPercent += parseInt(structureItemList[i].percent);
			}
			
			$ctrl.structureItem.percent = 100-countPercent;
		}
		
		
	}else{
		$ctrl.structureItem.percent = 100;
	}

	$ctrl.ok = function (form) {
		$log.log('Modal - ok');
		$ctrl.isSubmit = true;	
		if(form.$valid){
			$log.log('true');
			var percent = parseInt($ctrl.structureItem.percent);
			if(percent > 100){
				alert("อัตราร้อยละต้องไม่เกิน 100%");
			}else if($ctrl.structureItem.revenueTypeId==2){
				$log.log('structureItem.revenueTypeId==2');
				$ctrl.costCenterList = [];
				if(structureItemList.length>0){
					for(var i = 0; i < structureItemList.length; i++){
						$ctrl.costCenterList.push(structureItemList[i].costCenterCode);

					}
					$ctrl.costCenterList.sort();
					console.log("costCenterList.length : "+$ctrl.costCenterList.length);
					console.log("costCenterList : "+$ctrl.costCenterList);

					var countDupCostCenter = 1;
					if($ctrl.costCenterList.length>2){
						var keepGoing = true;
						for(var i = 0; i < $ctrl.costCenterList.length; i++){
							console.log("-------------- i : "+i);
							console.log("costCenterList i : "+$ctrl.costCenterList[i]);
							console.log("costCenterList i+1 : "+$ctrl.costCenterList[i+1]);

							if($ctrl.costCenterList[i+1] == $ctrl.costCenterList[i]){
								countDupCostCenter++;

								if(countDupCostCenter == 3){
									console.log("countDupCostCenter : "+countDupCostCenter);
									console.log("$ctrl.costCenterList : "+$ctrl.costCenterList[i]);
									console.log("$ctrl.structureItem.costCenterCode : "+$ctrl.structureItem.costCenterCode);
									if($ctrl.costCenterList[i] == $ctrl.structureItem.costCenterCode){
										alert("ไม่สามารถเลือก รหัส-ชื่อศูนย์ต้นทุนเจ้าของรายได้ซ้ำกันเกิน 3 รายการ");
										keepGoing = false;
									}

								}
							}
						}

						if(keepGoing){
							$uibModalInstance.close($ctrl.structureItem);
						}
					}else{
						$uibModalInstance.close($ctrl.structureItem);
					}

				}else{
					$uibModalInstance.close($ctrl.structureItem);
				}

			}else{
				$uibModalInstance.close($ctrl.structureItem);
			}
		}		
	};

	$ctrl.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});	
