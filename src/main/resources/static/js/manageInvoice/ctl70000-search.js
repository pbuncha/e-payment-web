/**
 * 
 */
var app = angular.module("ctl70000SearchApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp','ngMaterial']);

app.factory("dataFactory", function (httpProvider,reportFactory) {
	var urlBase = ctx+'/manageInvoice';
	var urlBase2 = ctx+'/receiptController';
	var dataFactory = {};
	
	dataFactory.getDefaultData = function () {
		return  httpProvider().get(urlBase2+'/getDefaultData');
	};

	dataFactory.pagination = function (bean,page) {
		return  httpProvider().post(urlBase+'/getInvoiceList',bean);
	};
	dataFactory.getMCostCenterList = function () {
		return  httpProvider().get(urlBase+'/getMCostCenterList');
	};

	dataFactory.getdeptList = function () {
		return  httpProvider().get(urlBase+'/getdeptList');
	};
	dataFactory.getInvoiceRpt = function(id){
		return  httpProvider().get(urlBase+'/getInvoiceRpt/'+id);
	};

	dataFactory.cancel = function ( id ) {
		return  httpProvider().get(urlBase+'/cancel/'+id);
	};
	dataFactory.cancelList = function ( ids ) {
		return  httpProvider().post(urlBase+'/cancelList',ids);
	};
	dataFactory.exportReport = function(criteria){
		reportFactory.post(urlBase + '/exportExcel', criteria);
	};
	dataFactory.edit = function(id){
		window.location.href = urlBase+'/edit/'+id;
	};
	dataFactory.getDisbursementUnitList = function (departmentId) {
		return httpProvider().get(urlBase2+'/getDisbursementUnitList/'+departmentId);
	};
	dataFactory.getCostCenterList = function (disbursementUnitId) {
		return httpProvider().get(urlBase2+'/getCostCenterList/'+disbursementUnitId);
	};
	return dataFactory;
});

app.factory("$confirmDialog", function($uibModal){
	return function(){
		return $uibModal.open({
			animation: true,
			backdrop: 'static',
			size: 'sm',
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			template: "<div class='modal-header'>\
				<h3 class='modal-title' id='modal-title'><b>ต้องการลบข้อมูล?</b></h3>\
				</div>\
				<div class='modal-body text-center' id='modal-body'>\
				<button class='btn btn-primary' type='button' ng-click='ok()'>ตกลง</button>\
				<button class='btn btn-warning' type='button' ng-click='cancel()'>ยกเลิก</button>\
				</div>",
				controller: function($scope, $uibModalInstance){

					$scope.ok = function() {
						return $uibModalInstance.close(true);
					};

					$scope.cancel = function() {
						return  $uibModalInstance.dismiss(false);
					};

				}
		});
	};
});

app.factory('reportFactory',function($http){
	function strToArraybuffer(str, fileType,fileName){
		var a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";
		var binary_string =  window.atob(str);
		var bytes = new Uint8Array( binary_string.length );
		for (var i = 0; i < binary_string.length; i++)        {
			bytes[i] = binary_string.charCodeAt(i);
		}
		var file = new Blob([bytes.buffer], {type: fileType});
		var fileURL = URL.createObjectURL(file);

		console.log(fileURL);
//		window.open(fileURL,"_blank");
		a.href = fileURL;
		a.download = fileName;
		a.click();
		window.URL.revokeObjectURL(fileURL);
	}

	return {
		get: function(url, params ){
			$http.get(url, params ).then(function(data) {
				strToArraybuffer(data['data']['file'], data['data']['fileType']);
			});
		},
		post: function(url, params ){
			$http.post(url, params ).then(function(data) {				
				strToArraybuffer(data['data']['file'], data['data']['fileType'],data['data']['fileName']);
			});
		},

	}
});

app.controller("ctl70000Ctrl", function ($log,$scope, dataFactory,$http,$confirmDialog,$mdDialog) {

	var $ctrl = this;

	$scope.invoiceList = [];

	$scope.data3List =[];
	$scope.data ={};

	$scope.sliceData = [];
	$scope.perPages = 10;
	$scope.totalItems = $scope.data3List.length;
	$scope.currentPage = 1;
	$scope.invoiceCheckList = [];
	$scope.values = [10,15,20,25];
	$scope.checkList = [];
	$scope.selectAll = false;

	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
		var end = begin + $scope.perPages;
		$scope.from = begin;
		$scope.to = end;
		$scope.sliceData = $scope.data3List.slice(begin, end);
	}

	$scope.orderByField = '';
	$scope.reverseSort = false;
	$scope.orderFunction = function(field) {
		$scope.orderByField = field;
		$scope.reverseSort = !$scope.reverseSort;		
	}
	$scope.checkField = function(name) {
		if ($scope.orderByField == name) {
			return true;
		} else {
			return false;
		}
	}

	$log.log('getDefaultData');
	
	dataFactory.getDefaultData().then(function (response) {
		console.log(response);
		if(response.departmentId!=null){
			$scope.data.departmentId = response.departmentId+"";
			$scope.getDisbursementUnitList(response.departmentId);
		}
	});
	
	$scope.getDisbursementUnitList = function (departmentId) {
		console.log("departmentId : "+departmentId);
		if(departmentId!=null){
			dataFactory.getDisbursementUnitList(departmentId).then(function (response) {
				console.log(response);
				$scope.disbursementUnitList = response;
			}, function (error) {
				
			});
		}else{
			$scope.disbursementUnitList = [];
		}
		
	};
	
	$scope.getCostCenterList = function (disbursementUnitId) {
		console.log("disbursementUnitId : "+disbursementUnitId);
		if(disbursementUnitId!=null){
			dataFactory.getCostCenterList(disbursementUnitId).then(function (response) {
				console.log(response);
				$scope.costCenterList = response;
			}, function (error) {
				
			});
		}else{
			$scope.costCenterList = [];
		}
		
	};
	
	$log.log('Master : Department List');
	dataFactory.getdeptList().then(function (response) {
		$scope.deptList = response;
	},function(err){

	});

//	$log.log('Master : getMCostCenterList');
//	dataFactory.getMCostCenterList().then(function (response) {
////		console.log(response);
//		$scope.costCenterList = response;
//	},function(err){
//
//	});

	$ctrl.search = function(){
		$log.log('search');
		$scope.invoiceCheckList = [];
//		$scope.currentPage = 1;
		dataFactory.pagination($scope.data).then(function (response) { 
			$scope.data3List = response;
			$scope.totalItems = $scope.data3List.length;
			$scope.figureOut();
//			console.log(response);
		}, function (error) {
			$scope.status = 'Unable to load customer data: ' + error.message;
		});
	};

	$ctrl.clearSearch = function(){
		$scope.data3List =[];
		$scope.data ={};
		$scope.invoiceCheckList = [];
		$scope.sliceData = [];
		$scope.perPages = 10;
		$scope.totalItems = $scope.data3List.length;
		$scope.currentPage = 1;
		$scope.checkList = [];
		$scope.selectAll = false;
	};

	$scope.pageChanged = function() {
		$scope.figureOut();
		$scope.checkList = [];
		$scope.selectAll = false;
		$scope.exportList = [];
//		$scope.invoiceList = [];
	};

	$scope.cancelInvoice = function( id ){
		$confirmDialog()['result']['then'](function(ok){
			dataFactory.cancel(id).then(function(){
				$ctrl.search();
			});
		},function(cancel){
			/* Cancel*/
		});
	}

	$scope.getDesc = function(id,list){
		var objArr = angular.fromJson(list).filter(function(item) {
			if (item.id === id) {		    	
				return true;
			}
		});
		return objArr[0].desc;
	};




	$ctrl.prinvInvoice = function(id){
		$http.get(ctx+'/manageInvoice'+'/getInvoiceNormalRpt/'+id,{responseType: 'arraybuffer'}).then(function (response) {
			console.log(response.data);
			var file = new Blob([response.data], {type: 'application/pdf'});
			var fileURL = URL.createObjectURL(file);
			window.open(fileURL);
			console.log("seccess");
		}, function (error) {
			$scope.status = 'error: ' + error.message;
		});
	}

	$scope.getMultiInvoiceRpt = function(ids){
		if(ids === [] || ids === undefined ){
			return;
		}

		$http.get(ctx+'/manageInvoice'+'/getMultiInvoiceRpt/'+ids.join(),{responseType: 'arraybuffer'})
		.then(function (response) {
			console.log(response.data);
			var file = new Blob([response.data], {type: 'application/pdf'});
			var fileURL = URL.createObjectURL(file);
			window.open(fileURL);
			console.log("seccess");
		}, function (error) {
			$scope.status = 'error: ' + error.message;
		});
	}

	function showAlert (textContent) {
		$mdDialog.show(
				$mdDialog.alert()
				.clickOutsideToClose(true)
				.title('Error')
				.textContent(textContent)
				.ok('OK')
		);
	}

	$scope.exportExcel = function( dataList ){
		if(dataList.length == 0){
			showAlert("ไม่มีข้อมูล");
		}else{
			dataFactory.exportReport(dataList);
		}
	};

	$scope.editPage = function(id){
		dataFactory.edit(id);
	};



	$scope.isInvoiceContains = function( invoiceId ){
		return $scope['invoiceList'].indexOf( invoiceId ) >=0 ;
	}

	$scope.exportList = [];
	$scope.selectInvoice = function( invoice ){
		var index = $scope['invoiceList'].indexOf( invoice.id );
		if( index == -1 ){
			$scope['invoiceList'].push( invoice.id );
			$scope.exportList.push(invoice);
		}else{
			$scope['invoiceList'].splice( index, 1 );

			var index2 = -1;
			$scope.exportList.some(function(obj,j) {
				return obj.id ===  invoice.id ? index2 = j : false;
			});
			$scope.exportList.splice( index2, 1 );
		}
	}

	$scope.setSelectAll = function(list){
		$scope['invoiceList'] = [];
		if($scope.selectAll){
			for(var i =0 ;i<list.length ;i++){
//				$scope.isInvoiceContains(list[i].id);
				$scope.selectInvoice(list[i]);
				var index = -1;
				$scope.sliceData.some(function(obj,j) {
					return obj.id ===  list[i].id ? index = j : false;
				});

				$scope.checkList[index] = true;
			}
		}else{
			$scope['invoiceList'] = [];
			$scope.checkList = [];
		}
	}

	$scope.deleteList = function(checkList){
		console.log(checkList);
		if(checkList.length==0){
			showAlert("กรุณาเลือกข้อมูล");
		}else{
			$confirmDialog()['result']['then'](function(ok){
				dataFactory.cancelList(checkList).then(function(){	
					$ctrl.search();
				});				
			},function(cancel){
				/* Cancel*/
			});
		}
	}
});