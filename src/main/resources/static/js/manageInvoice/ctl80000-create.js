/**
 * 
 */
var app = angular.module("ctl80000App", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp' ,'addressApp','ngMaterial','ngSanitize']);

app.factory("dataFactory", function (httpProvider) {
	var urlBase = ctx+'/manageInvoice';

	var dataFactory = {};

	dataFactory.pagination = function () {
		return  httpProvider().post(urlBase+'/pagination');
	};

	dataFactory.getTitleList = function () {
		return  httpProvider().get(urlBase+'/getTitleList');
	};

	dataFactory.getPersTypeList = function () {
		return  httpProvider().get(urlBase+'/getPersonTypeList');
	};
	dataFactory.save = function (bean) {
		return  httpProvider().post(urlBase+'/saveInvoice',bean);
	};
	dataFactory.getCatalogList = function (mCatalogId) {
		return  httpProvider().get(urlBase+'/getCatalogList/'+mCatalogId);
	};
	dataFactory.getMCatalogType = function () {
		return  httpProvider().get(urlBase+'/getMCatalogType');
	};
	dataFactory.getInvoice = function(id){
		return  httpProvider().get(urlBase+'/getInvoice/'+id);
	};
	dataFactory.searchPage = function(id){
		window.location.href = urlBase+'/search';
	};
	dataFactory.getDetailByCitizenNo = function(no){
		return  httpProvider().get(urlBase+'/getDetailByCitizenNo/'+no);
	};
	return dataFactory;
});

app.factory("$confirmDialog", function($uibModal){
	return function(){
		return $uibModal.open({
			animation: true,
			backdrop: 'static',
			size: 'sm',
			ariaLabelledBy: 'modal-title',
			ariaDescribedBy: 'modal-body',
			template: "<div class='modal-header'>\
				<h3 class='modal-title' id='modal-title'><b>ต้องการยกเลิกรายการ?</b></h3>\
				</div>\
				<div class='modal-body text-center' id='modal-body'>\
				<button class='btn btn-primary btn-style' type='button' ng-click='ok()'>ตกลง</button>\
				<button class='btn btn-warning btn-style' type='button' ng-click='cancel()'>ยกเลิก</button>\
				</div>",
				controller: function($scope, $uibModalInstance){

					$scope.ok = function() {
						return $uibModalInstance.close(true);
					};

					$scope.cancel = function() {
						return  $uibModalInstance.dismiss(false);
					};

				}
		});
	};
});

app.controller("ctl80000Ctrl", function ($log,$scope, dataFactory, $mdDialog, $document,$location,$uibModal,$confirmDialog) {
	var $ctrl = this;
	$log.log('Example 2 - pagination');
	$scope.data3List ={};
	$scope.data ={items:[],
			totalAmount : 0,
			addrHome:{},
			addrBilling:{},
			homeIsBill:false};
	$scope.addrBillDis = false;

	$scope.homeIsBill = function(){
		if($scope.data.homeIsBill){					
			$scope.data.addrBilling = $scope.data.addrHome;
			$scope.addrBillDis = true;
		}else{			
			$scope.data.addrBilling = angular.copy($scope.data.addrHome);
			$scope.addrBillDis = false;
		}
	};
	
	$scope.dis = {	pers:false,
			comp:true,
			group:true
	};
	
	$scope.disToggle = function(){
		console.log("disToggle : "+$scope.data.persTypeId);
		if($scope.data.persTypeId == 1){
			console.log("1 disToggle : "+$scope.data.persTypeId);
			$scope.dis.pers=false;
			$scope.dis.comp=true;
			$scope.dis.group=true;
		}else if($scope.data.persTypeId == 2){
			console.log("2 disToggle : "+$scope.data.persTypeId);
			$scope.dis.pers=true;
			$scope.dis.comp=false;
			$scope.dis.group=true;

		}else if($scope.data.persTypeId == 3){
			console.log("3 disToggle : "+$scope.data.persTypeId);
			$scope.dis.pers=true;
			$scope.dis.comp=true;
			$scope.dis.group=false;
		}else{
			$scope.dis.pers=false;
			$scope.dis.comp=true;
			$scope.dis.group=true;
		};
	};
	
	if(invoice != null && invoice != ""){
		console.log(invoice);
		$scope.data =invoice;
		$scope.data.startDate = new Date($scope.data.startDate);
		if($scope.data.endDate != null)
			$scope.data.endDate = new Date($scope.data.endDate);
		$scope.data.titleId = $scope.data.titleId.toString();
		$scope.data.persTypeId = $scope.data.persTypeId.toString();
		$scope.disToggle();
		$scope.homeIsBill();
	}

	

	$scope.cancel = function(){
//		$scope.data3List ={};
//		$scope.data ={items:[],
//		totalAmount : 0,
//		addrHome:{},
//		addrSend:{}};
//		$scope.dis = {	pers:false,
//		comp:true,
//		group:true
//		};
//		$scope.data.persTypeId = $scope.persTypeList[0].id;
		$confirmDialog()['result']['then'](function(ok){
			dataFactory.searchPage();
		},function(cancel){
			/* Cancel*/
		});
	}

	

	dataFactory.pagination()
	.then(function (response) {
		$scope.data3List =response.content;
		$scope.maxSize = response.size;
		$scope.bigTotalItems = response.totalPages;
		$scope.bigCurrentPage = 1;
	}, function (error) {
		$scope.status = 'Unable to load customer data: ' + error.message;
	});


	$log.log('Master : Title List');
	dataFactory.getTitleList().then(function (response) {
		$scope.titleList = response;
		if($scope.data.titleId==null || $scope.data.titleId =="" || $scope.data.titleId ==undefined){		
			$scope.data.titleId = $scope.titleList[0].id;
		}
//		console.log(">>>"+ $scope.titleList[0].id);
	});

	$log.log('Master : Person Type List');
	dataFactory.getPersTypeList().then(function (response) {
		$scope.persTypeList = response;
		if($scope.data.persTypeId==null || $scope.data.persTypeId =="" || $scope.data.persTypeId ==undefined){		
			$scope.data.persTypeId = $scope.persTypeList[0].id;
		}
	});

	$log.log('Master : getMCatalogList');
	dataFactory.getMCatalogType().then(function (response) {
		$scope.mCatalogList = response;
	});

	$log.log('Modal : addItem');
	$ctrl.open = function (index,size, parentSelector) {
		var data = {};
		if(index !=null){
			data = $scope.data.items[index];
		}else if($scope.data.items.length >=5){
			showAlert("ไม่สามารถเพิ่มรายการรับชำระได้เกิน 5 รายการ");
			return;
		}
		var parentElem = parentSelector ? 
				angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
				var modalInstance = $uibModal.open({
					animation: true, 
					templateUrl: ctx +'/templates/views/manageInvoice/ctl80001-item.html',
					controller: 'ctl80001Ctrl',
					controllerAs: '$ctrl',
					backdrop: 'static',
					keyboard: false,
					size: size,
					appendTo: parentElem,
					resolve: {
						mCatalogList: function () {
							return $scope.mCatalogList;
						},
						data : data
					}
				});

				modalInstance.result.then(function (items) {
					if(index !=null){
						$scope.data.items[index] = items;
					}else{
						$scope.data.items.push(items);
					}
					$scope.data.totalAmount = $scope.getTotal();
				}, function () {
					$log.info('Modal dismissed at: ' + new Date());
				});
	};

	function showAlert (textContent) {
		$mdDialog.show(
				$mdDialog.alert()
				.clickOutsideToClose(true)
				.title('ผิดพลาด')
				.textContent(textContent)
				.ok('OK')
		);
	}
	$scope.isSubmit = false;	
	$scope.save = function (form) {
		$scope.isSubmit = true;	
		console.log(form);
		if(form.$valid){
			if($scope.data.items.length ==0){
				showAlert("กรุณาระบุข้อมูลรายการรับชำระ");
			}else {
				$scope.data.startDate = Date.parse($scope.data.startDate);
				$scope.data.endDate = Date.parse($scope.data.endDate);

				console.log($scope.data);
				dataFactory.save($scope.data).then(function (response) {
					console.log(response);
					$scope.data = response;
					window.location.replace(ctx + '/manageInvoice/success');
				}).catch(function(response) {
					console.error('data error', response);
				})
				.finally(function() {
					console.log("finally finished");
				});
			}
		}
	};

	$scope.data.deleteItem = [];
	$scope.deleteItem = function (index) {
		$scope.data.deleteItem.push($scope.data.items[index]);
		$scope.data.items.splice(index,1);
		$scope.data.totalAmount = $scope.getTotal();
	}

	$scope.getTotal = function(){
		var total = 0;
		for(var i = 0; i < $scope.data.items.length; i++){
			var amount = $scope.data.items[i].amount;
			total = total+amount;	       
		}

		return total;
	}

	$scope.getUserCitizen = function(no){
		dataFactory.getDetailByCitizenNo(no).then(function (response) {
			$scope.data.persTypeId = angular.copy(response.persTypeId.toString());
			$scope.disToggle();
			$scope.data.titleId = angular.copy(response.titleId.toString());
			if(!$scope.dis.pers){
				$scope.data.firstName = angular.copy(response.firstName);
				$scope.data.midName = angular.copy(response.midName);
				$scope.data.lastName = angular.copy(response.lastName);
			}else if(!$scope.dis.comp){
				$scope.data.compName = angular.copy(response.compName);
			}else if(!$scope.dis.group){
				$scope.data.groupName = angular.copy(response.groupName);
			}
			$scope.data.mobile = angular.copy(response.mobile);
			$scope.data.email = angular.copy(response.email);
			$scope.data.addrHome = angular.copy(response.addrHome);
			$scope.data.homeIsBill = angular.copy(response.homeIsBill);
			$scope.data.addrHome.id = null;
			$scope.data.addrBilling = angular.copy(response.addrBilling);
			$scope.data.addrBilling.id = null;
			$scope.homeIsBill();
			
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	}


});

app.controller('ctl80001Ctrl', function ($scope,$uibModalInstance, mCatalogList,data,$location,dataFactory,$timeout,$mdDialog,$filter) {
	var $ctrl = this;
	$ctrl.data = angular.copy(data);
	$ctrl.mCatalogList = mCatalogList;
	$ctrl.catalogList = [];
	$scope.amountDis = true;
	$scope.regexNumber = /^\d+$/;
	if(angular.equals(data, {})){
		$ctrl.data.amount = null;
		$ctrl.data.catalogId = {};	
		$ctrl.data.catalogName = {};
	}else{
		$ctrl.data.mCatalogId = $ctrl.data.mCatalogId.toString();
		$ctrl.data.amount = Number($ctrl.data.amount) ;

	}

	$ctrl.getCatalogList = function(mCatalogId){
		if(mCatalogId != null && mCatalogId != ""){
			dataFactory.getCatalogList(mCatalogId).then(function (response) {	
//				console.log(response);
				$ctrl.catalogList = response;
			});
		}
	}

	$scope.$watch(function () { return $ctrl.catalog; }, function (newData, oldData) {
		if(!angular.equals(newData,oldData)){
//		if($ctrl.data.catalogId == null && $ctrl.data.catalogId == "" && $ctrl.data.catalogId == undefined){
			if($ctrl.catalog != undefined && $ctrl.catalog != null){
				$ctrl.data.catalogId = $ctrl.catalog.originalObject.id;
				$ctrl.data.catalogName = $ctrl.catalog.originalObject.desc;
				$ctrl.data.amount = Number($ctrl.catalog.originalObject.desc2);
//				$ctrl.data.amount = $filter('number')($ctrl.data.amount, 2);
				if($ctrl.data.amount==null || $ctrl.data.amount ==undefined || $ctrl.data.amount ==0 ){
					$scope.amountDis = false;
				}
			}else{
				$ctrl.data.amount = null;
				$ctrl.data.catalogId = {};	
				$ctrl.data.catalogName = {};

			}
		}
	});

	$ctrl.ok = function (form) {
		$ctrl.isSubmit = true;	
		console.log($ctrl.data);

		if(form.$valid){
			$uibModalInstance.close($ctrl.data);
		}
	};

	$ctrl.cancel = function () {
		$uibModalInstance.dismiss('cancel');
	};
});

app.controller('invoiceSuccessCtrl',function($scope,$location,$log){
	$log.log('invoiceSuccessCtrl');

});
