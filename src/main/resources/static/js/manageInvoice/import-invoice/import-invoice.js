(function() {

	var urlBase = ctx + '/manageInvoice';



	angular.module("Import-Invoice", [ 'httpProvider', 'ui.bootstrap','ngResource', 'ngSanitize', 'datepickerApp','bootstrap.fileField','ngAnimate','ngAnimate','ngMaterial']);

	angular.module("Import-Invoice")
	.config(function($sceDelegateProvider,$compileProvider) {

		$sceDelegateProvider.resourceUrlWhitelist(['**']);
		$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|data):/);
	});

	angular.module("Import-Invoice")
	.factory("dataFactory", function(httpProvider, $reportFactory,$http) {
		return {
			getMCatalogType : function() {
				return httpProvider().get(urlBase + '/getMCatalogType')
			},
			getDepartmentAll : function() {
				return httpProvider().get(urlBase + '/getDepartmentAll')
			},
			getMRevenueType : function(bean) {
				return httpProvider().get(urlBase + '/getMRevenueType');
			},
			search: function(criterai){
				return httpProvider().post(urlBase +'/searchImport', criterai);
				
			},
			confirm: function(code){
				return httpProvider().get(urlBase +'/confirmLoader/'+ code);
				
			},
			cancel: function(code){
				return httpProvider().get(urlBase +'/cancelLoader/'+ code);
				
			},
			deleteLoader: function(code){
				return httpProvider().get(urlBase +'/deleteLoader/'+ code);
				
			},
			importFile: function(file){
				var formData = new FormData();
				formData.append('file', file);

				return $http.post(urlBase + '/importFile', formData,{
					// this cancels AngularJS normal serialization of request
					transformRequest: angular.identity,
					// this lets browser set `Content-Type: multipart/form-data` 
					// header and proper data boundary
					headers: {'Content-Type': undefined}
				});
			}

		}
	});

	angular.module("Import-Invoice")
	.factory('$reportFactory',function($http){
		function strToArraybuffer(str, fileType){
			var binary_string =  window.atob(str);
			var bytes = new Uint8Array( binary_string.length );
			for (var i = 0; i < binary_string.length; i++)        {
				bytes[i] = binary_string.charCodeAt(i);
			}
			var file = new Blob([bytes.buffer], {type: fileType});
			var fileURL = URL.createObjectURL(file);
			window.open(fileURL);
		}

		return {
			get: function(url, params ){
				$http.get(url, params ).then(function(data) {
					strToArraybuffer(data['data']['file'], data['data']['fileType']);
				});
			},
			post: function(url, params ){
				$http.post(url, params ).then(function(data) {
					strToArraybuffer(data['data']['file'], data['data']['fileType']);
				});
			},

		}
	});

	angular.module("Import-Invoice")
	.controller("import-invoice-main",
			function($scope, $log, $location, dataFactory, $uibModal, $http) {
		$scope.cri = {};
		$scope.resultList = [];
		$scope.sliceData = [];
		$scope.perPages = 10;
		$scope.totalItems = $scope.resultList.length;
		$scope.currentPage = 1;
		$scope.values = [10,15,20,25];
		
		$scope.figureOut = function(){
			var begin = (($scope.currentPage - 1) * $scope.perPages);
			var end = begin + $scope.perPages;
			console.log(begin+" "+end);
			$scope.from = begin;
			$scope.to = end;
			$scope.sliceData = $scope.resultList.slice(begin, end);
		}
		
		$scope.pageChanged = function() {
			$scope.figureOut();
			$scope.checkList = [];
			$scope.selectAll = false;
			$scope.exportList = [];
//			$scope.invoiceList = [];
		};
//		dataFactory.getMCatalogType().then(function(data){
//			$scope.catalogTypeList = data;
//		});
//		dataFactory.getDepartmentAll().then(function(data){
//			$scope.departmentList = data;
//		});
//		dataFactory.getMRevenueType().then(function(data){
//			$scope.revenueTypeList = data;
//		});

		$scope.add = function (size, parentSelector) {
			var parentElem = parentSelector ? 
					angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
					var modalInstance = $uibModal.open({
						animation: true,  
						templateUrl: ctx +'/templates/views/manageInvoice/import-invoice/import-modal.html',
						controller: 'modalCtrl',
						backdrop: 'static',
						keyboard: false,
						size: size,
						appendTo: parentElem
					});

					modalInstance.result.then(function (items) {
						$scope.search($scope.cri);
					}, function () {
						$log.info('Modal dismissed at: ' + new Date());
						
					});
		};
		
		$scope.clear = function () {
			$scope.cri = {};
			$scope.resultList = [];
			$scope.currentPage = 1;
			$scope.figureOut();
		}
		
		$scope.search = function (bean) {
			dataFactory.search(bean).then(function(data){
				console.log(data);
				$scope.resultList = data;
				$scope.totalItems = $scope.resultList.length;
				$scope.figureOut();
			});
		};
		
		$scope.cancel = function (code) {
			dataFactory.cancel(code).then(function(data){
				$scope.search($scope.cri);
			});
		}
		
		$scope.confirm = function (code) {
			dataFactory.confirm(code).then(function(data){
				$scope.search($scope.cri);
			});
		}
		
		$scope.deleteLoader = function (code) {
			dataFactory.deleteLoader(code).then(function(data){
				$scope.search($scope.cri);
			});
		}
		

	});


	angular.module("Import-Invoice").controller("modalCtrl",
			function($scope,$uibModalInstance,$mdDialog,dataFactory) {	

		$scope.title ="";

		function showAlert (textContent) {
			$mdDialog.show(
					$mdDialog.alert()
					.clickOutsideToClose(true)
					.title('ผิดพลาด')
					.textContent(textContent)
					.ok('OK')
			);
		};

		$scope.fileType = false;
		$scope.error = false;
		$scope.$watch(function () { return $scope.fileUpload; }, function (newData, oldData) {
			if(!angular.equals(newData,oldData)){
				$scope.error = false;
				$scope.title = $scope.fileUpload.name;
				if( (angular.equals($scope.fileUpload.type,"text/plain") || angular.equals($scope.fileUpload.type,"application/vnd.ms-excel"))){
					$scope.fileType = false;
				}else{
					$scope.fileType = true;
				}
			}
		});

		$scope.ok = function (form) {
			$scope.isSubmit = true;	
			console.log(form);
			console.log($scope.fileUpload);
			if(form.$valid ){
				if(!$scope.fileType){
					dataFactory.importFile($scope.fileUpload).then( function(data){
						$scope.error = false;
						$uibModalInstance.close();
					})
					.catch(function(response) {
						$scope.error = true;
						console.error('data error', response.data);
					})
					;
				}
//				
				
			}

		};

		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
	});

})(angular);