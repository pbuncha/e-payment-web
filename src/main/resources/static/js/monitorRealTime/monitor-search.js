var app = angular.module("ctl10000SearchApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	var urlBase = ctx+'/monitorRealTime';
	var dataFactory = {};
	
    dataFactory.getMCostCenterList = function () {
        return  httpProvider().get(urlBase+'/getMCostCenterList');
    };

    dataFactory.getdeptList = function () {
		return  httpProvider().get(ctx+'/manageInvoice/getdeptList');
	};

    dataFactory.pagination = function () {
        return  httpProvider().post(urlBase+'/pagination');
    };

    dataFactory.getInvoiceBillingList = function (data) {
        return  httpProvider().post(urlBase+'/getInvoiceBillingList', data);
    };
    
	return dataFactory;
});

app.controller("ctl10000SearchCtrl", function ($log,$scope,dataFactory) {
	
//	$log.log('Master : Department List');
//	dataFactory.getdeptList().then(function (response) {
//		console.log(response);
//		$scope.deptList = response;
//	}).catch(function(response) {
//		console.error('data error', response);
//	})
//	.finally(function() {
//		console.log("finally finished");
//	});
//	
//	$log.log('getMRevenueType');
//	dataFactory.getMRevenueType().then(function (response) {
//		console.log(response);
//		$scope.mRevenueTypeList = response;
//	}).catch(function(response) {
//		console.error('data error', response);
//	})
//	.finally(function() {
//		console.log("finally finished");
//	});
	

	$log.log('getMCostCenterList');
	dataFactory.getMCostCenterList().then(function (response) {
		console.log(response);
		$scope.mCostCenterList = response;
	}).catch(function(response) {
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally finished");
	});
	
	$scope.data2List ={};
	$scope.criteria ={};
	$scope.search = function() {
		
		$log.log($scope);		
		
		dataFactory.getInvoiceBillingList($scope.criteria).then(function (response) {
//			$scope.data2List = response.content;
			$scope.data2List = response;
			$log.log(response);
			$log.log(response.totalPages);

			
			$scope.totalItems = $scope.data2List.length;
			$scope.figureOut();
			
/*			$scope.maxSize = response.size;
			$scope.bigTotalItems = response.totalPages;
			$scope.bigCurrentPage = 1;*/
									
		}, function (error) {
			$scope.status = 'Unable to load customer data: ' + error.message;
		});
	};	
	$scope.search();
	
	// Paging
	$scope.sliceData2List = [];
	$scope.perPages = 10;
	$scope.totalItems = $scope.data2List.length;
	$scope.currentPage = 1;
	
	$scope.values = [10,15,20,25];
	
	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceData2List = $scope.data2List.slice(begin, end);
	}
	
	$scope.pageChanged = function() {
		$scope.figureOut();
	};
	
	
});