$.fn.offlineTable = function(exOption) {
	var option = PagingUtil.getStaticOption();
	option.columns = [];
	option.order = [];
	this.find('thead>tr>th,thead>tr>td').each(function(idx,ele) {
		if ($(ele).is('.not-sortable')) {
			option.columns.push({sortable:false});
		} else {
			option.columns.push(null);
			option.order.push([idx,'asc']);
		}
	});
	
	option = $.extend(true, option, exOption);
	
	return this.DataTable(option);
}
var PagingUtil = {
	getStaticOption : function() {
		
		return {
			pagingType: "full_numbers",
		    language:  {
		       paginate: {
		           first: "หน้าแรก",
		           last: "หน้าสุดท้าย",
		           next: "ถัดไป",
		           previous: "ย้อนกลับ"
		       },
		       emptyTable: "ไม่พบข้อมูลที่ค้นหา",
		       infoEmpty: "ผลการค้นหาทั้งหมด _TOTAL_ รายการ",
		       info: "ผลการค้นหาทั้งหมด _TOTAL_ รายการ (แสดงรายการที่ _START_ ถึง _END_)",
		       lengthMenu: "แสดงหน้าละ _MENU_ รายการ"
		    },
		    filter: false,
			autoWidth: false,
			sDom: '<"top"ilf>rt<"bottom"p><"clear">'
		};
		
	},
	getAjaxOption : function() {
		
		var option = PagingUtil.getStaticOption();
		
		$.extend(option, {
			serverSide: true,
			destroy: true,
			order: ([[0, "asc"]])
		});
		
		return option;
		
	},
	getDefaultOption : function() {
		
		return {
			pagingType: "full_numbers",
		    language:  {
		       paginate: {
		           first: "หน้าแรก",
		           last: "หน้าสุดท้าย",
		           next: "ถัดไป",
		           previous: "ย้อนกลับ"
		       },
		       emptyTable: "ไม่พบข้อมูลที่ค้นหา",
		       infoEmpty: "ผลการค้นหาทั้งหมด _TOTAL_ รายการ",
		       info: "ผลการค้นหาทั้งหมด _TOTAL_ รายการ (แสดงรายการที่ _START_ ถึง _END_)",
		       lengthMenu: "แสดงหน้าละ _MENU_ รายการ"
		    },
		    filter: false,
			serverSide: true,
			destroy: true,
			order: ([[0, "asc"]]),
			autoWidth: false,
			sDom: '<"top"ilf>rt<"bottom"p><"clear">'
		};
		
	}, getEditElement : function() {
		
		var aEdit = document.createElement('a');
		aEdit.setAttribute('href', '#');
		aEdit.setAttribute('class', 'cmd-icon cmd-edit');
		aEdit.setAttribute('data-tooltip', 'tooltip');
		aEdit.setAttribute('title', 'แก้ไข');
		var spanEdit = document.createElement('span');
		spanEdit.setAttribute('class', 'fa fa-pencil-square-o');
		aEdit.appendChild(spanEdit);
		return aEdit;
		
	}, getEditPasswordElement : function() {
		
		var aEdit = document.createElement('a');
		aEdit.setAttribute('href', '#');
		aEdit.setAttribute('class', 'cmd-icon cmd-edit-pw');
		aEdit.setAttribute('data-tooltip', 'tooltip');
		aEdit.setAttribute('title', 'เปลี่ยนรหัสผ่าน');
		var spanEdit = document.createElement('span');
		spanEdit.setAttribute('class', 'fa fa-key');
		aEdit.appendChild(spanEdit);
		return aEdit;
		
	}, getDeleteElement : function() {
		
		var aRemove = document.createElement('a');
		aRemove.setAttribute('href', '#');
		aRemove.setAttribute('class', 'cmd-icon cmd-del');
		aRemove.setAttribute('data-tooltip', 'tooltip');
		aRemove.setAttribute('title', 'ลบ');
		var spanRemove = document.createElement('span');
		spanRemove.setAttribute('class', 'fa fa-trash-o');
		aRemove.appendChild(spanRemove);
		return aRemove;
		
	}, getPrintElement : function() {
		
		var aPrint = document.createElement('a');
		aPrint.setAttribute('href', '#');
		aPrint.setAttribute('class', 'cmd-icon cmd-print');
		aPrint.setAttribute('data-tooltip', 'tooltip');
		aPrint.setAttribute('title', 'พิมพ์');
		var span = document.createElement('span');
		span.setAttribute('class', 'fa fa-print');
		aPrint.appendChild(span);
		return aPrint;
		
	}
};
