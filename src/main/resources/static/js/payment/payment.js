var app = angular.module("paymentApp", ['httpProvider','ui.bootstrap','ngRoute','ngResource','ngMaterial','ngSanitize']);



app.config(function($routeProvider,$mdAriaProvider,$sceDelegateProvider,$compileProvider) {
	  $routeProvider
	    .when('/paymentSelect', {
	      controller:'paymentSelectCtrl',
	      templateUrl: ctx +'/templates/views/payment/payment-select.html'
	    })
	    .when('/paymentConfirm', {
	    	controller:'paymentConfirmCtrl',
	    	templateUrl: ctx + '/templates/views/payment/payment-confirm.html'
	    })
	    .when('/paymentChanel/:invoiceId', {
	    	controller:'paymentChanelCtrl',
	    	templateUrl: ctx + '/templates/views/payment/payment-chanel.html'
	    })
	    .when('/paymentFavorite', {
	    	controller:'paymentFavoriteCtrl',
	    	templateUrl: ctx + '/templates/views/payment/payment-favorite.html'
	    })
	     .when('/paymentSuccess', {
	    	controller:'paymentSuccessCtrl',
	    	templateUrl: ctx + '/paymentController/paymentSuccess'
	    })
	    .otherwise({
	      redirectTo:'/paymentSelect'
	    });
	  
	  $mdAriaProvider.disableWarnings();
	  $sceDelegateProvider.resourceUrlWhitelist(['**']);
	  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|data):/);
	});

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/paymentController';
	  var urlBase2 = ctx+'/manageInvoice';
	    var dataFactory = {};
	    dataFactory.searchInvoice = function (id) {
	    	var param = {id:id};
	        return  httpProvider().post(urlBase+'/searchInvoice',param);
	    };
	    dataFactory.getInvoiceItemSelected = function (data){
	    	return httpProvider().post(urlBase + '/getInvoiceItemSelected',data);
	    }
	    dataFactory.getBarcode = function(_id){
	    	var param = {id:_id};
	    	return httpProvider().post(urlBase + '/getBarcode',param);
	    }
	    dataFactory.getQRCode = function(_id){
	    	var param = {id:_id};
	    	return httpProvider().post(urlBase + '/getQRCode',param);
	    }
	    dataFactory.getMCostCenterList = function () {
			return  httpProvider().get(urlBase2+'/getMCostCenterList');
		};

		dataFactory.getCatalogList = function () {
			return  httpProvider().get(urlBase2+'/getCatalogList');
		};
		dataFactory.getdeptList = function () {
			return  httpProvider().get(urlBase2+'/getdeptList');
		};
	    dataFactory.getList = function(bean){
	    	return httpProvider().post(urlBase + '/getCatalogList',bean);
	    }
	    dataFactory.saveFavorite = function(bean){
	    	return httpProvider().post(urlBase + '/saveFavorite',bean);
	    }
	    dataFactory.getFavoriteByUser = function(){
	    	return httpProvider().get(urlBase + '/getFavoriteByUser');
	    }
	    dataFactory.getFavoriteData = function(bean){
	    	return httpProvider().post(urlBase + '/getFavoriteData',bean);
	    }
		dataFactory.getCatalogType = function () {
			return  httpProvider().get(urlBase+'/getMCatalogType');
		};
		dataFactory.getCatalogByCatType = function (id) {
			return  httpProvider().get(urlBase+'/getCatalogByCatType/'+id);
		};
		dataFactory.getProvince = function(){
			return  httpProvider().get(urlBase+'/getProvince/');
		};
		dataFactory.getDistrict = function(id){
			return  httpProvider().get(urlBase+'/getDistrict/'+id);
		};
		dataFactory.getSubDistrict = function(id){
			return  httpProvider().get(urlBase+'/getSubDistrict/'+id);
		};
		dataFactory.saveInvoice = function(bean){
			return httpProvider().post(urlBase+'/saveInvoice',bean);
		}
		dataFactory.getBank = function () {
			return  httpProvider().get(urlBase+'/getBank');
		};
		dataFactory.saveBill  = function(bean){
			return httpProvider().post(urlBase+'/saveBill',bean);
		};
		dataFactory.getUserDetails = function(){
			return httpProvider().get(urlBase+'/getUserDetails');
		}
		
	    return dataFactory;
});
/**
 * App Filter Config
 */
// let's make a startFrom filter
app.filter('startFrom', function() {
    return function(input, start) {
    	if (!input || !input.length) { return; }
        start = +start; // parse to int
        return input.slice(start);
    }
});

// Sum amount
app.filter('sumOfAmount', function () {
    return function (data, key) {        
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;        
        var sum = 0;        
        angular.forEach(data,function(value){
            sum = sum + parseInt(value[key], 10);
        });        
        return sum;
    }
});

app.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);

/**
 * End App Filter Config
 */
app.run( function($rootScope, $location) {

    // register listener to watch route changes
    $rootScope.$on( "$routeChangeStart", function(event, next, current) {
    		
    	
    		if (next.$$route.originalPath === '/paymentFavorite'){
    			if($rootScope.favoriteSelectedList.length == 0){
    				$rootScope.tab = 0; 
    				$location.path( "/paymentSelect" );
    			}
    		} else {
    			if ( $rootScope.invoiceItemSelectedList.length == 0 ) {	
    				if(next.$$route.originalPath === '/paymentChanel/:invoiceId'){
    					if(!next.params){
    						$rootScope.tab = 0;
    						$location.path( "/paymentSelect" );
    					}else{
    						$rootScope.tab = 2;
    					}
    				}else{
    					if( next.$$route.originalPath  === '/paymentSuccess'){
    						$rootScope.tab = 3; 
    						$location.path( "/paymentSuccess" );
    		    		}else{
    		    			$rootScope.tab = 0;
    		    			$location.path( "/paymentSelect" );
    		    		}  
    					
    				}
      	        
    			}	
    		}
    		
    	         
    });
 })

app.controller("paymentMainCtrl", function ($scope,$rootScope,$log,$location, $mdDialog,dataFactory,$uibModal) {

	$scope.imagePath = 'img/washedout.png';
	
	$scope.progress = '0%';
	$rootScope.tab = 0;
	
	$scope.headerList = ['ขั้นตอนที่  1 เลือกรายการชำระเงิน','ขั้นตอนที่ 2 ตรวจสอบและยืนยันรายการชำระเงิน','ขั้นตอนที่ 3 เลือกช่องทางชำระเงิน','ผลการชำระ','รายการโปรด'];
	$rootScope.headerStep = $scope.headerList[$rootScope.tab];
	
	$scope.$watch('tab',function(newValue,oldValue){
		$rootScope.headerStep = $scope.headerList[newValue];
	})
	
	// ~ Initial Bean
	$rootScope.invoiceItemSelectedList = [];
	$rootScope.favoriteSelectedList = [];
	$rootScope.invoiceSelectedSet = new Set();
	$rootScope.favoriteSelectedSet = new Set();
	$rootScope.nextClicked = false;
	
	
	// ~ Service
	dataFactory.getFavoriteByUser().then(function(response){
		$log.log(response);
		$rootScope.favoriteSelectedSet = new Set(response);
		$rootScope.favoriteSelectedList = Array.from($rootScope.favoriteSelectedSet);
	}).catch(function(response) {
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally finished");
	});
	
	// tab 2
	$rootScope.confirmPayment = false;
	
	$scope.clicktab = function(tab){
		var beforeTab = $rootScope.tab;
		$rootScope.tab = tab;
		
		if($rootScope.tab == 0){
			$location.path('paymentSelect').replace();
			$rootScope.headerStep = $scope.headerList[$rootScope.tab];
		}else if ($rootScope.tab == 1){
			$location.path('paymentConfirm').replace();
			$rootScope.headerStep = $scope.headerList[$rootScope.tab];
		}else if ($rootScope.tab == 2) {
			$location.path('paymentChanel').replace();
			$rootScope.headerStep = $scope.headerList[$rootScope.tab];
		}else if ($rootScope.tab == 3) {
			$location.path('paymentChanel').replace();
			$rootScope.headerStep = $scope.headerList[$rootScope.tab];
		}else if ($rootScope.tab == 4) {
			if(!$scope.favoriteSelectedList.length == 0){
				$location.path('paymentFavorite').replace();
				$rootScope.headerStep = $scope.headerList[$rootScope.tab];
			}else{
				$rootScope.tab = beforeTab;
			}
			
			
		}
		console.log("headerStep : "  , $rootScope.headerStep);
	}
	
    $scope.next = function(){
    	$rootScope.nextClicked = true;
    	if($rootScope.tab == 0){
    		if(!validationPaymentSelect()){
    			showAlert('Please Select Invoice Item.');
    		}else{
    			
    			$rootScope.nextClicked = false;
    			$scope.clicktab($rootScope.tab + 1);
    		}
    	}
    	if($rootScope.tab == 1 && $rootScope.nextClicked){
    		if(!$rootScope.confirmPayment){
    			showAlert('Please Confirm.');
     			
    		}else{
    			$scope.paymentProcess($scope.isLogin);
//    			if($rootScope.invoiceItemSelectedList.length == 0){
//    				showAlert('Please Select Invoice Item.');
//    				$rootScope.confirmPayment = false;
//    				$scope.clicktab($rootScope.tab - 1);
//    				
//    			}else{
//    				$scope.clicktab($rootScope.tab + 1);
//    			}
    			
    		}
    	}
    }
    
    $scope.previous = function(){
    	$scope.clicktab($rootScope.tab - 1);
    }
    
    function validationPaymentSelect () {
    	return !$rootScope.invoiceItemSelectedList.length == 0;
    }
    
    function showAlert (textContent) {
    	$mdDialog.show(
    		      $mdDialog.alert()
    		        .clickOutsideToClose(true)
    		        .title('Error')
    		        .textContent(textContent)
    		        .ok('OK')
    		    );
    }
    
    
    
    //modal Controller
    $scope.paymentProcess = function(isLogin,ev){
    	
    	if(!isLogin){
    		$uibModal.open({
  		      controller: DialogLoginController,
  		      templateUrl: ctx +'/templates/views/payment/modal-login.html',
  		    })
  		    .result.then(function(answer) {
  		      console.log(answer);
  		    }, function() {
  		    	console.log('closed');
  		    });
    	}else{
    		$uibModal.open({
    		      controller: DialogController,
    		      controllerAs : "vm",
    		      templateUrl: ctx +'/templates/views/payment/modal-register.html',
    		      backdrop: 'static',
    		      keyboard: false,
    		      size:'lg'
    		    })
    		    .result.then(function(response) {
    		     console.log('DialogController return');
    		      console.log(response);
    		      console.log($rootScope.invoiceItemSelectedList);
    		      //~ Create Bean to Save
    		      var saveData = {};
    		      saveData.invoice = angular.copy(response);
    		      saveData.invoiceItemList = angular.copy($rootScope.invoiceItemSelectedList);
    		      dataFactory.saveInvoice(saveData).then(function(response){
    		    	 console.log(response);
    		    	 $rootScope.tab = 2;
    		    	 $location.path('paymentChanel/'+response).replace();
    		    	
    		      });
    		    }, function() {
    		    	console.log('closed');
    		    });
    	}
    }
	
	   function DialogController($scope, $uibModal,$uibModalInstance) {
	        console.log('DialogController');
	        // Init
	        // Regex
	        $scope.regexNumber = /^\d+$/;
	        $scope.regexDigit = /^[a-zA-Z]*$/;	            
	        $scope.isSubmit = false;	
	        
	        $scope.cancel = function() {
	        	  $uibModalInstance.dismiss('cancel');
	          };

	          $scope.submit = function(form,bean) {
	        	  $scope.isSubmit = true;	
	        	  if(form.$valid){
	        		  $uibModalInstance.close(bean);
		          }
	        	  
	          };
	        
	        $scope.userTypeList = [
	        	{
	        		id:1,
	        		value:'ประชาชน'
	        	},
	        	{
	        		id:2,
	        		value:'ภาคธุรกิจ'
	        	}
	        ];
	        
	        $scope.loadProvince = function(){
	        	dataFactory.getProvince().then(function(response){
	        		$scope.provinceList = response;
	        	});
	        }
	        $scope.loadDistrict = function(id){
	        	if(id){
	        		dataFactory.getDistrict(id).then(function(response){
		        		$scope.districtList = response;
		        	});
	        	}
	        	
	        }
	        $scope.loadSubdistrict = function(id){
	        	if(id){
	        		dataFactory.getSubDistrict(id).then(function(response){
		        		$scope.subdistrictList = response;
		        	});
	        	}
	        	
	        }
	        $scope.loadUserdata = function(loaded){
	        	if(loaded){
	        		dataFactory.getUserDetails().then(function(response){
	        			$scope.loadDistrict(response.provinceId);
	        			$scope.loadSubdistrict(response.districtId);
	        			$scope.user = response;
	        		});
	        	}
	        }
	        
	        
	      }
	    function DialogLoginController($scope,$uibModal,$uibModalInstance){
	    	
	    	 
	          $scope.cancel = function() {
	        	  $uibModalInstance.dismiss('cancel');
	          };

	          $scope.submit = function(bean) {
	        	  $uibModalInstance.close(bean);
	          };
	          
	          $scope.registerPopup = function (ev){
	      		console.log('registerPopup');
	      		$uibModalInstance.dismiss('cancel');
	      		$uibModal.open({
	      		      controller: DialogController,
	      		      controllerAs : "vm",
	      		      templateUrl: ctx +'/templates/views/payment/modal-register.html',
	      		      backdrop: 'static',
	      		      keyboard: false,
	      		      size:'lg'
	      		    })
	      		    .result.then(function(response) {
	      		     console.log('DialogController return');
	      		      console.log(response);
	      		      console.log($rootScope.invoiceItemSelectedList);
	      		      //~ Create Bean to Save
	      		      var saveData = {};
	      		      saveData.invoice = angular.copy(response);
	      		      saveData.invoiceItemList = angular.copy($rootScope.invoiceItemSelectedList);
	      		      dataFactory.saveInvoice(saveData).then(function(response){
	      		    	 console.log(response);
	      		    	 $rootScope.tab = 2;
	      		    	 $location.path('paymentChanel/'+response).replace();
	      		    	
	      		      });
	      		    }, function() {
	      		    	console.log('closed');
	      		    });
	      	}
	    }
    
    
    
	 	

    
});

app.controller("paymentSelectCtrl", function ($scope,$rootScope,$log,dataFactory) {


	
	$log.log("paymentSelectCtrl");
	
	$scope.currentPage = 0;
	$scope.pageSize = 15;
	$scope.deptList = [];
	$scope.catalogList = [];
	$scope.data = {};
	$scope.data.departmentId = null;
	$scope.data.catalogId = null;
	$scope.data.catalogName = null;
	$rootScope.tab = 0;
	

		// ~ Search Service
	$log.log('Master : Department List');
		 dataFactory.getdeptList().then(function (response) {
			$scope.deptList = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
		
	

	$log.log('Master : getMCostCenterList');
		 dataFactory.getMCostCenterList().then(function (response) {
			console.log(response);
			$scope.costCenterList = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	
	

//	$log.log('Master : getCatalogList');
//		 dataFactory.getCatalogList().then(function (response) {
//			console.log(response);
//			$scope.catalogList = response;
//		}).catch(function(response) {
//			console.error('data error', response);
//		})
//		.finally(function() {
//			console.log("finally finished");
//		});
		 
		
	 dataFactory.getList($scope.data).then(function (response) {
			$log.log(response);
			$rootScope.invoiceItemList = response;
			$log.log($rootScope.invoiceItemList);
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	 
		dataFactory.getCatalogType().then(function (response) {
			$rootScope.getCatalogTypeList = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
		
		$scope.getCatalogByCatType = function(id){
			console.log(id);
			dataFactory.getCatalogByCatType(id).then(function (response) {
				$rootScope.getCatalogList = response;
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
		}
	 
	 
	$scope.search = function(bean){
		 $log.log("data tor : ",bean);
		dataFactory.getList(bean).then(function (response) {
			$log.log(response);
			$rootScope.invoiceItemList = response;
			$log.log($rootScope.invoiceItemList);
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	}
	
		// ~ End Search Service
		
		
		
		/** *********************************************************************************** */
		/**
		 * ******************************* Method
		 * ***********************************
		 */						
		/** *********************************************************************************** */
		
		$scope.addInvoiceSelect = function(item){
			if(!$rootScope.invoiceSelectedSet.has(item)){
				$rootScope.invoiceSelectedSet.add(item);
				$rootScope.invoiceItemSelectedList = Array.from($rootScope.invoiceSelectedSet);
				console.log($rootScope.invoiceSelectedSet,$scope.invoiceItemSelectedList);
			}
			
		}
		$rootScope.delInvoiceSelect = function(item){
			$rootScope.invoiceSelectedSet.delete(item);
			$rootScope.invoiceItemSelectedList = Array.from($rootScope.invoiceSelectedSet);
			console.log($rootScope.invoiceSelectedSet,$scope.invoiceItemSelectedList);
		}
		$scope.checkInvoiceSelected = function(item){
			return $rootScope.invoiceSelectedSet.has(item);
		}
		$scope.clickCart = function(item){
			if( !$rootScope.invoiceSelectedSet.has(item)){
				$scope.addInvoiceSelect(item);
			}else{
				$scope.delInvoiceSelect(item);
			}
		}
		$scope.addFavoriteSelected = function(item){
			if(!$rootScope.favoriteSelectedSet.has(item.catalogId)){
			$rootScope.favoriteSelectedSet.add(item.catalogId);
			item.favorite = true;
			$rootScope.favoriteSelectedList = Array.from($rootScope.favoriteSelectedSet);
			}
		}
		$scope.delFavoriteSelected = function(item){
			$rootScope.favoriteSelectedSet.delete(item.catalogId);
			item.favorite = false;
			$rootScope.favoriteSelectedList = Array.from($rootScope.favoriteSelectedSet);
			console.log($rootScope.favoriteSelectedSet,$scope.favoriteSelectedList);
		}
            
          
    
});

app.controller('paymentConfirmCtrl', function ($scope,$rootScope,$log,dataFactory){
	$log.log("paymentConfirmCtrl");
	console.log($rootScope.confirmPayment);
	

});

app.controller('paymentChanelCtrl', function ($scope,$scope,$rootScope,$log,dataFactory,$timeout,$mdDialog,$routeParams,$location,$http){
	$log.log("paymentChanelCtrl");
	$log.log("$routeParams",$routeParams.invoiceId);
	
	// ~ Initial
	$scope.toggleCreditTab = false;
	$scope.toggleCounterTab = false;
	$scope.toggleBookTab = false;
	$scope.invoice = {};
	$scope.paymentData = {};
	//~ Mock
	   $scope.creditList = [{
	        id: "1",
	        title: 'visa',
	        url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/16/Former_Visa_%28company%29_logo.svg/80px-Former_Visa_%28company%29_logo.svg.png'
	      },{
	        id: "2",
	        title: 'master card',
	        url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f0/Mastercard_logo.svg/80px-Mastercard_logo.svg.png'
	      }];
	    
	    $scope.counterList = [{
	    	id: "1",
	        title: 'ใบแจ้งการชำระเงิน',
	        url: 'fa fa-money fa-2x'
	      },{
	        id: "2",
	        title: 'QR Code',
	        url: 'fa fa-qrcode fa-2x'
	    },{
	        id: "3",
	        title: 'Barcode',
	        url: 'fa fa-barcode fa-2x'
	    }];
	
	/** *********************************************************************************** */
	/**
	 * ******************************* Service
	 * ***********************************
	 */						
	/** *********************************************************************************** */
	
	// ~ Mock
	    $scope.isLogin = {};
	$scope.isLogin = false;
	$scope.bankList = null;
	
	$scope.loadBanks = function(){
			dataFactory.getBank().then(function (response) {
				console.log(response);
				$scope.bankList = response;
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
	}
	
	dataFactory.searchInvoice($routeParams.invoiceId).then(function(response){
		console.log('searchInvoice',response)
		$scope.invoice = response;
	}).catch(function(response){
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally finished");
	});
	
	/** *********************************************************************************** */
	/**
	 * ******************************* Method
	 * ***********************************
	 */						
	/** *********************************************************************************** */
	$scope.formatStyle = function(isChecked){
		if(isChecked){
			return "{background: 'primary-500'}";
		}else{
			return "{background: 'grey-500'}";
		}
	}
	
	
    function showAlert (textContent) {
    	$mdDialog.show(
    		      $mdDialog.alert()
    		        .clickOutsideToClose(true)
    		        .title('Error')
    		        .textContent(textContent)
    		        .ok('OK')
    		    );
    }
	
    $scope.paymentProcess = function(){
    	if(!$scope.ruleConfirm){
			showAlert('Please Confirm Condition');
		}else{
			if(!$scope.toggleCreditTab && !$scope.toggleCounterTab && !$scope.toggleBookTab){
				showAlert('Please Select Payment Chanel');
			}else{
				if($scope.toggleCreditTab ){
					$scope.paymentData.chanel = 1;
				}else if($scope.toggleCounterTab ){
					$scope.paymentData.chanel = 2;
				}else if($scope.toggleCreditTab){
					$scope.paymentData.chanel = 3;
				}
				$scope.paymentData.invoiceId = $routeParams.invoiceId;
//				$location.path("/paymentSuccess");
				dataFactory.saveBill($scope.paymentData).then(function(response){
					if(response){
						$location.path("/paymentSuccess");
					}
				});
			}
		}
    }
    
	
	$scope.tabClearOther = function(isChecked){
		if($scope.toggleCreditTab === isChecked){
			$scope.toggleCounterTab = false;
			$scope.toggleBookTab = false;
		}
		if($scope.toggleCounterTab === isChecked){
			$scope.toggleBookTab = false;
			$scope.toggleCreditTab = false;
		}
		if($scope.toggleBookTab === isChecked){
			$scope.toggleCreditTab = false;
			$scope.toggleCounterTab = false;
		}
	}
 
    
	//~ Modal
    $scope.imagePopup = function(id,ev){
    	$log.log('imagePopup do : '+id);
    	$log.log('invoiceId do : '+$routeParams.invoiceId);
    	if(id == 1){
    		
//    			dataFactory.getInvoiceRpt(id).then(function (response) {
    			$http.get(ctx+'/manageInvoice'+'/getInvoiceRpt/'+$routeParams.invoiceId,{responseType: 'arraybuffer'}).then(function (response) {
    				console.log(response.data);
    				 var file = new Blob([response.data], {type: 'application/pdf'});
    				 var fileURL = URL.createObjectURL(file);
    				 window.open(fileURL);
    				 console.log("seccess");
    			}, function (error) {
    				$scope.status = 'error: ' + error.message;
    			});

    		
    		
//    		var imageObj = {}
//    		imageObj.name = 'paymebill';
//    		imageObj.url = 'iVBORw0KGgoAAAANSUhEUgAAAfQAAAEcCAYAAAAxwUZlAAAABGdBTUEAALGPC/xhBQAA5Y9JREFUeF7sXQVgFMkSXQLB3Tn0cD843O1wd3d3d3d31+Du7u5OCASJu7vb+1U9O5tNCBDsk4Rubi47Mz0tb+R1VVdXJTp9+jQiIiI0iRIl0nACIP7yvnpMPa6eMzAwiJKPz6tlcB4+r78vCtSWqf9bzcP1qGXr18HnOan1cT4+pl+/fn1qvfrX6J+P3q+Y2qhfhn67+LiKg9oO9bx6Tr+9Kib6dcZUn3qe26lirpaj9kPts/59+BwG0e+jWoaKe/T7pH+/9dusX7c+DjHdx+hl8P7n7mP0Z0z/vqvYq31T753aruj3Rm3L1+5j9Oci+vP6ufsY/X5Efx9UTPWf6S/dR/Wcfv3614aHh2sSJ06se3fU50zNoz5nn7s+pvdY/z7G1F79+qPfG/3nQf+eqLjo33e1b+qzqv+efuk+Rn8/9N+H6G3/3vsYvUz9Z1jtQ3RsPvfe6n+LvnQf9L+B6vOl/07ov4/Rv6fq+x/Te6ffXvV8TPdD/9saUzv1n1P9+xj9/ul/e9TnM6b3Mfo78D33Mfp9if5d/dn3Uf+e6/dbv57o91F9R/SfaX081PP67+7nvp8xvT/6eaOXoc87+m1X2yLKo/99VwoICADdYLlJDOQzIJ8B+QzIZ0A+A9/5DPj6+n4XB8d00XcTemBg4E9rhCxIIiARkAhIBCQCfyICPj4+P63bktB/GpSyIImAREAiIBGQCHwbApLQvw0vmVsiIBGQCEgEJAJxEgFJ6HHytshGSQQkAhIBiYBE4NsQkIT+bXjJ3BIBiYBEQCIgEYiTCEhCj5O3RTZKIiARkAhIBCQC34ZAvCF0WtvHa9zj/fZtt0fmlghIBCQCEoH4iECEttHq3+h9iDxPvMYnid/E9gMpXhD60xtmmNZtL6Z32x+vt2ld92HfyltiUCKTREAiIBGQCMRHBJiAWbhUSFj80W6iN9rve4RYSx5Gu/SXNv4rhFI9og9nDg/nLYzycJm08wMpzhO6v08gxrXeiVm99mNGz/i9zaT2T2y/C09ufPyBWyYvlQhIBCQCEoH/DwL6wpeWyBU6j0bUeoTNJE2NY2pmwhZ/w0MQEeqN8GAHhAd8pO0Fwv2fIyLwLR1zVPIxqfMg4AcEvjhP6I+ufhQkOKMHkXkC2KZ124d1U87/f55FWYtEQCIgEZAIfAcCivpbaMF1/4T4Tf+xNM2bIoELKVuQdygQ5ouIYGtE+D9GuMduhDtNQrhtV4RbVEX4x4IIe5+CNg3C39FmqkEY/32fFOGWjRAe4k6SuiLFf2+K84T+9LpZgiP0DVMvfO/9ktdJBCQCEgGJwC9HQLXX0qrJicAjIkIEkesIntXjLF37XEG4y3JE2HRC2MecCP+gJWsi7AgmbN7XbhH0N+JDItro70ci8vcGCPuQDOFvaN/vunYa/fvV7nGe0B9escaglicxqtM5jOh4ASM7nqeN/0b/re7z+XMi/6hOZzGm8xmM7XwS47ucwMSuxzCl22FM736Qtn0k8e/DTO3Gv5Xt12oCWEKXhP7L30ZZgURAIpBQEIhiLBZVelXmsbWCs27nOzquK4MFc0WdrkjLisqc1eXwf4YIz30Is+9NEncJImOStom0mYx5C9Nu4R8TCQJXSJvJXNnXHePfdJzPh39ISVI6nbeoQdJ+qG6e/Tt6IC6J84TuZ3oQDhtywt0oN7x3ZoX/rkwI3JUBQTvTIXhnGvGXt8Cd6RGwMyN8d2aG144ccN+eG87b/obdlsKw3FgKH9aVg8nKKnixpBbuzGuIC9N74fD4cdg+Yj5WD1iL+b12YVrXwxjf+TRG80Cg0xkxCOABAJN8VOL/ftKXhP69j6q8TiIgEfgRBHTc98XVQoqKWWHJuJF4Tpm3MFJvh4WF06ZKyfw7HKG0H0aSs6Iij2271bzav9pBg2q0FhHqifCgDwj3PoNw962IcFmCcLd1gNceRPheBgIe0Py3GUnotkCIHf21AQKfUt4tJHEXE5J3OKnWVVJnYhebSvJ0TkjmrHa3boLwsEAxf64YziVgCf3tnVdY0m0lFvfehrk9d2N2dyJTItlpXY9gqt7GZDydjs8k6XtOj72Y33OXuGZFv41YN2gltg1bhH2jpuHExJG4OqM3Hi1qincr/4H9xr/hsj0XvHZnRtDeVAjalQTuW/LDbGUl3JnbDkcnDsfGYQuwsO82zOh+AJO7HsUk2iZT3dNp/1uleknoceMjIVshEfjjENAReaRUG2mirRCbYmkdSXa/ByPt8i3VWpzbRaTNgwy/oBCY2nvg+mtbXDG2hYmtO7wCgkUzFUJUFOLqzPfX2i8GAHwNTYTzFhbmR3PgFkTW72gj47VQVxpIULlU0HunAJx7YY/LxvY4/dQSnn4BuPLKmtphjUv094Wli6guLMSD5skTC8k8gkldj8wj1e9JSbonQncarqjZSTpnc3d9K/ivtT2m83FeQv85c+iqOj3y73SSupmQWQKfQAQ9rvMpkszPYHTns7R/AnN67cbGwctwdvJgvF9Rk4g+GSKO0g3anwjWq8vhJBH9kr6bMZ7yju96HKK8WKjrJaF/z2Mqr5EISAR+FAEmi4CgUDx874Bn5s54auYs/orftJk7ecGfCFNIxD9onPX9bdUSrDA+UyRVlsD9g0MwftddaDqsQ+LOG5Gkz3Yk7rYRBk2WQ9NiBYZuvwVPX0XKZfqNHJTE0BJRhaJW9/YPxMOPjlosXPDwgxP8AxVDN0UzoKjeOdVbeAaa/xaL+jTV5+D+B0do6i6CpuVKaBoswchtt0S+iDAPMnpLQtK4gSKR682hC0IX8+qGZDhnrLWEp/qozWIw9YOakT+E0L9dRT6dNAHTSNqf2v0QpnY7hCmkAZjcjaRyIu2Vg5bjzJTBsF9fAjiige/+dHi5uB42DVmGUR3Pktr+xGcld0no3/+qyyslAhKB70NAzAMTYey/8wGamvOQuOUqGLRYCYPmvK1A4mYrYdh8OTJ2XIdxex8QMTKpx1Z9/X1tivkqldCJUEM5FGgozj8xR6o+27D18mvYe/ghMDgUIaHhCAoJg7NXAI49MkPWvkTwRKqnn1kqmgYm4c/NqeuptfsuvyhI2qDZChgQJppa80kKt9JKywI03TKyUiP2QtNxAzRdN9G2GaceW0DTlAYUXTZCQxgeevBBIXTHsYoqXTt3riN0JvJ3KciAbh4NEgIjLdrVdsZ6uuDzeEtCj4VUHWkop0j4LNmz2n0sSfXTiey3DF8A4xW1gOMaBO1JA9OVFWlufh7GkcTPgwF9QztJ6D/z5ZdlSQQkAl9HgEhJK/mN2Xkbmjaroem2GZrutHUjcuLf4i9tnYmw6i9Bm7mnRLGCGLUS79fr+YEcQmLmmngpGG2BRI5BD7DrlhlarboIv8AQpXDtdIC6XludMw8Ni8C0Aw+gqTKHiJX9fDAR01z7J4MSxcqN1eisutc0WgpNTy0WjEfzVVh/3linulemIWienvJn6bBeh1Gjeacx4+BDRTrvQpg1XQYTG1+Eu65A+Fsmc2UOXRjCqcvUrGlpGvVLTAgIazu2nI/BMc0PwCgJ/ZsIPSZJny3l9wp1/Zyee+GyOSewj0LDHyXJfUcmGI2aTNb1rJJXLOglof/A0yovlQhIBL4LASYwNiArM/4gNO3WkoRJJNRpA1J2XI8URFSJWDrtQMdY2uy5BZp6i3H4oZkgPkVSV6X12BjMxSZP1G6I0lWS47XcztPxxMIPI7bfFKQXxhbgPM+tHZoojl0i5/3ZUI7TmgvG0FSeg49O3ooHthgMzJRBCtBp+QVoWjEh00CGyZqJufUqDN92M0rjuJ6PDl5CCtd0p7xt1mDlqReot+AsNO0Jy050jJyfebgtQIixok4XZE4SubCCNyMjOZ/Loi1C80H3QRkm/XwDREnoP0zokSTPlvA8Bx+8Pzki9tLIjDYc0MBlU0Ei9aPCUl4S+nd9j+RFEgGJwA8gwKQUSCpqJnAhhROZpyfV8UdHL3xw8MDJR6Q+ZmLrSoTO52muuvykw9p10ZHW4/rxNNTmxCbGhpCs9eaHY7pGOGqhLcyiMhmW2WP9xZeiCp5D1zfUCyCVu7NXIIJD9SRc7Zw35++8+jKqjjkgrlXJWx0vqOp4F58AMc2g6blVDHDSDN5F5EzYtF+HmtOPRkrOwmAuHEdI6tc0ZUInKZ7U8y8sXVF82G7KvwUpO+zAjWuVADPtenMmcybyD+kR4baRLPD1DA5Fq37dVIYk9J9K6HsxstN5hOwnAzotoat/nTcVwpiOpyWh/8BHSV4qEZAIfB8CTGSuRGJJWcXMUjhJlsXHHVSM30j1yyRz950DEhFZacjojLccvbZoiZSnlPVJKAIefkFw8vKDZ0DQJ0vFWIqOnlgyVaaI2ZL80/Mq3Yf73qM56DFknOZGpO2vmwvn+oOIwEfuuIWMNBBJTcZoBah9QSGhmLTnPvptvIa7bx0EVfL8enKyE3hIRmusYVCSUr9qF9BuBUnnLWnqgQYvyXpsQbc1l0laJy0FDXjSD9xJ1ynDD3VAMIwM3jStKD8NeAwbL4aDhw9SNtuLKmPmwsUsGcLM2UkM/SV1O69Pj3BdJtauK1MCkfL499292F8lCf2nEPpecmBzCnN774HDpsKfkLmQ1I9phNHcdAoyIx3LxP4BlTklAhKBH0NAnWd+YuYEg4ZE6DxXTnO/g7aQOpuV2IJsI+Ds6S+M5YSkTurn7JRPqLu16uw3Nm7ouv4qig3aicxkPJeeJNscNDgoSvs15pzExL33BQnfoSVlVacdRd0FZ1Bz9knsvfVOJy2rFuM7rr5F9WnHUGf+aVQdtx8PaTDB1Mee1hDmDFcqR5kHJ4mdSDmEyLzCmP3CmlxTfzGa0Bx2oV5bUWnSQSRiS/d/p+OGiZ3OOr/r6kvou+qSrl5h+y5U9+HCkj8l95NtB4jEZx96iKFbb+jmw1OS9oK1GbzunQmdByD5xx9S1OvdNiBNsx0koftj6vq6CHNKigizRAAbwb3/C+HOM4jIXbRDGn3/7pFajh+7m1++WhL6NxO6YhjHxm7qevRlAzbg0YLmwuI9umSuU70Toa8auFYS+q98mmXZEgGJwGcR2HjZRJkHZkIn9fEuIlrVuIxJ3crZW1i9i/l1subO2W+HTkJdc/Y5NDwYaLxMSLFCLa9V3WvaEtG1XkPW8/PxjtT3L61coamzQJFoGy9HLyJXISMzoWrntPPxXDRrA4hQUzVaBp/AYIQFmdKcc0rSBtDSOa2XNnUA0IwGBlx3MhpE3HpDjlwoOXn7K3PgZAHP7fXxozLYxzoRsQkNPgqQ7xC1XtW4jffH7Lyj2BFQG1K1Xg1fMribzgZujA0NUJKR9b8XaSBUZzas2k/D7e20BQaNd6L6xLnkn70kNYAENTsNHN6lh4fFRJobcFVkceEmVjvH/wvV6zHdaEnoXyF0dX05/+UlbGzgxsvXNg5egucLicTZAO7w54lcEPoeDd6SFTwbzsk5dPnFlQhIBP6fCKhq4/4kXWvaEJGRipmXW70hpyyh7HlNq5aec+SJYgHPKve2a9BswWlBTCvOvhRGcopl/CaSiJchKZG1IeclaVYYk9HcchYiQ5bmec47PZfB1uN0ruG8k4qkzHPkVN+ZZzRfz2Teg+ejl+PQPSX6ZAR5Ygu3qqfVGigSNVuq3yFVOudj8n/00UnxFqdtcy2a7+ZlZJUnHtbWoawv55SRJHhhDKidLuC2OZEWgi3ZRZupD02WnRf1bb7yWpkjp8FOEpL4eXkcq/l5oOPm44dkDTcgFfXpyIl6gDN902008LbQYN6m7rQufQtcfHgQEtVqXTV7+3/e63hC6Dt/oo/1T53MsMQ9jZeiEVGPI7/v7AOe/cFP6HISi3rvwJ5RM/BgXjs4bswHvz3pEHogKbCfbiqR+eckcpXImexNltYT0ryoR/py/38+37IuicAfj4AiaYaj6HAy/GJ1OpHyXyRlq9IrS6BrzhOhsbTLa6yZ8EkaP/fCWqjQk7bQqqd57p2IeO/td3D1DoALrQHXkDdOTSdFYi9CRmJqKjmE66L8tJUYRf7PWa1PS8v4b66RtJ67PUn0NBhIQ9fxPLgIMWrfB+EOY3XLunggwgOOTGx8RmTbf/N1bZsjCb3y5CNCFT51/32F0LXuU/l3vuF7iYy1zma0AwAhiZPhm7BWJ23Eezvy6kb9P/vUivpG/ae+8/r8xzQ9wZI2N+axuTvGrewMXxv67rvTN98uL2au74cUHcmgrv1WZOi2VUwJ/KiXt5/xoMZ5Qn9+4wMmtN2LyV2OYwKR7dhOp4WfdeHVjYg3+jZGe4z/cj7OP56um0TXTxWW5geE3/bl/TZhA81p7x49EycmjcD1WZ3xfFE1fCAvcPY0D+5tlA1h+5IBB0kCP6RYq3+RvPWM4MBz5qR+99j2F7aPnIVxNDBQXcRKQv8Zj60sQyIgEYgtAkzmnqRCFmpxrRV7UnKMUmrUXpQkss1LqmlNC5K2WWplQicpvtrY/aL4hrQeXajY6bgBSbRnn5PTFUosJTuytMtqdSZuWsrVbOFZXZMa0ty4IE5WYRNJ+pOHOpZ2Pzh5IjGt2RaESlK30TWKB85z1VymVUOA1nGr67R5IGLh4oXEpBFg8rVw9BbSP8+Hh5HhWzANBDIONBJkL5zBaJNqM1B+8mG4+wYoy8QIAycahBi002JAVvzNFyhr7XnQ8NLSTXESw1bsbVdi1x1rIvNQhHnsQIhFfvLhrsGLh0VRZ+wgPPtoh5SdaJDBWohO61CI5va5b5EGeLG9Mz8/X5wndJhtBUhlzRJxyJ6UFIAlE3y2U/CVrfngurkAnIh8HTYWoa2o2HjfhY570HlvyudvlAXBu1KTNG0gSFmQM5Gt2GituPirJWxWn6uSNavJY0vgIh+r3on8eRBgvb4Ido6cLaT8Gex7Xk+tLwn95z/EskSJgEQgZgTUueNXVkRYZEzGHs4EETEJs7qcPZ/x7+4klfNxUmtnI+txXtblQgSYiC3BhVX8OpSZoKi1VeK6R4ZsGiZbHgiQ5L7wxFOtm9QIjNtzL9KLGg0Q3ElS5tSNnMQoRL8B+fvv0A0OhAW6VX1EONH8NB1VpwlWnX0lpgGqaFXqwq86ZQ4lidjW1VcZpNDAwNbVR2cQxwMBTnVmntSq55U16hP3kOtYlsJ5UMNLz8jdrbqEjAc8SXkKocsWmlLYjq2ntiPCqhQZuyXDs3sl0GDMdKprD1I1Xg9Hsu5Pyg5luBwayPTfcE2ZHvgtnvWi3ve4T+gfdiPCKBq5MtnGdou2fOybSDo215Jv9xBSwTtuzI8jYydgIkVoG9/5OJH43hinCSShy0+vREAi8P9FIAKHeR21IHQiIZbCyRhMw5bevJF0zvPihnSs3/or5DedAoVQuvLKRsnH+UkNveP6WzFHznPhTIRrL5GanufC+TxJydcpWEoYqdWZ3A7dJxezbJEuzi2HBRncObj7kWc2GgCQD3ZN46XYRPPWilGe4scuzLolqd37K8vFtKTcgubfeT5/NBmycVJCmip1sFQuDPVobp89yakDDdWavfXS89oBSITQEKTjgQQbBHKbyJBuBnmWG0v+4SfsvotJ+8iDXvPdKDxkMZ7dLUXR1RJRe1pRxDVL5O5PDmRYvd5tPYr13QpjMrhLxPPwXBYNgIxusJZBCe7yu1P8JPTYEO1PzCMM30hDoEr1YVS2+erKODZ+JEVh20pz70qcdZ6H/1o8dUnov/uRl/VLBP4cBFRJd7SQTkmlzIROZDRq512su2iMteRZbeMVE4oWZkvz4uRfXE86XnTimXapluId7SPPN+vNUbdZeE45z2pqOu9GUj0TOqvjn1u4EKFrVetknW5s7YYFXB5b0VP+vwco0rkgZzY+Y0J3moBw8xzCJSqXwXP7uQfvFGr9Raef6/KrZN93Cy01o0ApOqM7vbXjTz44Y+7RJ+Iazj+aA7tw/9mGQLXQ58FMayL52kYo3n0+Xjz5GyHOBlixuwVqTN0prvXw5akKajOTNw0sGtAUxK6bporxIE8bkIbgjY07QoRB3PeHPf1ZT2TcJ/T3JKHv0JPIv4eovyDN68ia1e5kwCZIm63WSUXvQ3HVLdaUw4P5zXFs4mBsGLoM82j+fVLXYzQvfoKI/Cj5dWeVumpo9/UgMJLQf9ajK8uRCEgEvoaAqgZuMvuEMheuXbJm78HrvKMmVT2vEmb/DWQVz2p5kmhTkeGbB0UzU9eEizl5no/W+oDP109ZIsZGbzzPzecNVNIjSZ0jk+Umq3Nh2U5S9a7bpgrZiuhoWgndm1TkpsnpGA8MwgShZ6Glc6wlWHDyhTa/EniF573TcFmkzl9+hs+py8SUQQIbv72jAUgIlRNAc+3piIxVIz1NRxqEdGC/7ftQasgiPLydD+avcmLo2i7I0pkwarUbBr13krFeCF7RQERoGtjCnzQNkw8+QO81V4QnPe57OsKFg8Wo3uy+dj9+9fk4T+jB7/bRnDnFKd+TFqF76WbvM1SkZd54Tlz9y7/197V5OD9fF7QnNfx2Z4D3zmxw3ZELjhTz3GZ9UXxYVRpPF9XD1Zk9cHj8eGweuhQLextRvPNjZO1+TmxM3lO7HRTEze5bvyaFf+m8JPRf/UjL8iUCEgEFAV73rRBs3j5EpqxqJun4L/LVzmSp+GmPVGELUtQ6meGru68i72naQUBiIjAXsnhXl4AtPkHr0huQCp/n10nibUoDBlGj3jKxQuRxTUjvtL57xA5SaWtdp6ai+jlSmr6KPJx8tbO717A3JEz53NB5kytP3ux4nr/rClV9rkjBHKpUSNwkoZtYu+u81XF/mGDXnH+lhSAcs3k5HnuBY4m67SbkHHwGxWjZ8d4jZRFmkxvj10/H3wNP0xr3PTSA0QaqabtaDEqOPKSpioY8dUD9pP4eI//2OQaRIR5L+STtFx+2Rxlo6EVw+51PX5wn9PtXrDGo5SnyxHZaWItPYamYyHV2z30kLe/Gwj5GWNxnh25bQCOrub32YLaIinZQ5J9AhDyWrmeLeCZo9rfO5Y1nKbvrEREmNVLK1l/W9nWJ+1vJXRL673zcZd0SgT8HAZ6GZtW1nTuvvVaWZLGKvP7Mowr5irlofbekHO4s0qPZlP0UvYznnXkgQEZgq06/EIOD+xQzXMydM1kzsbUiw7BttyPL1C4R67KE3KvygIBJnyVczkskfPSBWbT6tcvNmBitOyDCoqpuDn0CG9dxGS1X4I6pg1BtHyCHOOXICv9vItPUpOr3pZjmkQMTcspJpOsTECyOuZNWQcQv77INiZsbIU+vZQh2agX4NCHz9kc0sCBXsYrNHJosprly6osg7yYrYOnsgykHaJkbDwYYA3KSY8xOc9SpC3Ko8x95wxOErh0Y/e6nK84T+tPrZpjYfldUqZjU3KzqVrYD0bbIc99Ktv+P/JLQf/cjL+uXCPwZCKhzupfIWE0YxIm11yswlozAIgk9Kha6IChE7McemSvuUIU0ulGo0DPQnHYSJkhWOWvXmbOl+1Iie7VMtcTZRx9Hkh+r7qmclHQNS+fK3L46mBCjC0HiEYHkLe51IopOdkHM57NWwPC/RWJgkYTanoZU57ko9jkHaOm+/hqS15oHLyJv1Vrd1cdfWOfzUjhOi46/IK91O1Bj5EwcP1UBr550IgZ/r2gHhDaCNRRKi/ttvanzFsdhVXnevxW5mBXW/6ReT0l/X1qSbQC3h/tO6/Mn73+oA1BdLvc7n674Sejf7K7150va30v+ktB/5+Mu65YI/DkIqGFPFx4nlTMHZWEpmeaD992lWONaqTI6GvpR0Lz8g2nZGhO6dpkbS64sLZPqfOFxMnAjS3Uxh05ls1V79HT6KXmEa6hVy7OES1qCXTdMhVo+Mgqalk3ZME6QOp2z701BTlIiPNhRaBGeWbiiIan0a009KrzW8fx5BGkK2Bq/65pLKE1+1o2um+IqDVx4bbowsqNi3clI779JM8m7W0NMWtcbadrPw423XtrAK1G1Edz2haeoTzwtoLXMv/7GFoUH0hw+z9UTBjXIb/zZZ6plPUvxy3CCotTpR3T73U+XJPQYBgfTuu4lJzR7hVe37yXuz10nCf13P/KyfonAn4GAuoyq0fwzMCAVeVKO+V1vIZ6TlKmvWv+E1NlMjcVW2o4/NCcXr6SGZgcytCUn9fPJp5bYfInijtddiGQcQ53KvERe5VQpWTGuoxjijp6KNMsE2WkjrTvfrjeQiB4zXZGYeS5dWLxbNqRgJ3kRHmKnk6DVdqr+55UoceRS3dMPZhQGlg3phJV+qC8QeB2P7vZFsym9qd1Evo22oNFUZR19zNbotLTvPs2XU1+S89p8ssyfdeQxUlEQGt43oIHNaJq3n07TEIlI5S7yEPlzHyWhR3uCAgMVpwMxpRhV7l/zv95dcbE6/UtbV55jj5TcOf+kTnvo2D6sHH8GG6ZfxLKRJzG5M3mpo43Lik7S0yhy2nQq50ukz3VwPjWPJPQ/42MqeykR+N0ICBU20aM9EZ6duy/sabN1IQcsvJb8C3O+Cpez5zNlKZY1XbPk5FMsJwnWyZPV2eHw8g+isryFz3MrmmtmQ7To6QSr7BtrDcrIQnzr1de6NeQxxQQXKmue99cGNwmz70lGckkQ4XmA2kLz5NqI6ko+7YBA5Fec3YSHknGc13EKvzoB8L1CXulCqO2+5NHOhxzPeNNa9eAoKvbo7eWpABvKx32yozXzbqS6tyXM+Ledmw8CaC27K7m7VfD0o3y+cWbuXO1LgpPQZ/bcj5VjT8H0qS1e3rPCizuWeHXH4pPtzUNrLBxyTBDt6BbbsH/VbQSQVWP0xOsq3Rx9sHz0SUzQm8uf2GEXXt23wvUTrzG1y54YSX0yuQc8vO4ent4wp0HBbpFHEvrv/szJ+iUCfwgCWuITEq3W/an4zWSuk6c/xUIldJFLG75UvU5Io4LslbCibPWuEr9QpWut6ll1nonn7LXL2opQiFXFm5oSUvTzSVWFk/TNeQOeI9yyBsUYp3l1IuoIb5pbp3n28CAbRAR9RIT/YyJ8InGnqYhwXkD5Tah8djNLVvNiPTvVyX3nv1pjvZjrVowBI/upBIbR4cZqftYgCBzVv0p/48LceYIl9NEtd8CfnAFcO/IS9lYecLTyxO0zb3DvvGmU7eHl94LQx7YygrmJk8Dj1kkTkspPKEFUuuzFzJ4HsHbyObx5YivOP7j4DmMoPxPzuNZGCKFRKd/MjTMvYUrnT0mdjfmuHnkFD2dfTOqoGPZJQv9DPqaymxKB34xAlBCeqkSrrtf+AhGpvtR1BnLcDz2pWL9ckVdL0r7COE3xjd5jLa3VFqFYidTrL8F+Umfz4EBJXyZ01iuoFvjqfHh4sA3CPQ8SaS+ibT5FPFuGcPfNROZHiNTvIiLESbd0LJzCr4opBbCGIbLtkfYBMd0YdYijP9TR1wREm3PX4hepLfjNN1tbfYKR0Fkyn9hhN6zeu8Dewh2D622Eg7Un7p4zxagW2zG+3a6oG5EtS9mONp4ICgzF8lGnMK6NEZG4ngEd/eb9kc234fB6xTKUBwcskY9rsxPB5G7QjiLxcJrb79AnUjqXf+XQS7jae0tCjxvPu2yFREAi8JMRUB3RtKa5+uaLzqIkR3Vjn+m8rI2s4cuTMZugcd2a91g2QDsIEZboJHELhQHzc7RNlCbU9MpAQd+wL5Y1JZhsCYbQJ3XcjeNbHwp1yDSa157QbiccSDq/T1L1JCJ6MZ9O8+PqvPlkmi8/vP6eGMHN6UduW8kIjs9/bj6cyfng6tviYZk38DBJ6DsRShL62d3PYPHGCS4O3hjdcnuU6yWhJ5j3RHZEIiAR+AwCqu/0zLScTNOOlrNRBDKxRK7dWqQnT2q8FlxxIqO1aI81kgp16y+lE6SuLx0rFK4jcpXQFfL/1vpi3bA4mzGBEPo+Uo0rnoQWDTsuCHxowy0koXvg9SMb7F56A8c23cepHY9xavtj7F1+C2NINc/qnJf3LbF78Q1sn3+V5slPgYl+QtuY46/zoCE4MAwmVOYwKp9V7lcOvcLIZlvhQcYXr6gs/WslocfZ5142TCKQ4BHQV5n/ynleLvudHVm015yrqNk5DCl5V8tPoVld2b+7MK6LW3PNn7v58aWdn2t/vCd0Vonz/DXHx71y2BjzBhzBnbNvsZnmtZ1tvfDo6kfd8jOev2ZJnP8uG3VSYLJq7GkhVc/tewiLhhzH+qnncZlI+uCaOxhPKnh9iZ0J/ephxaXgKFLDs4TOeVn6XzBYUSsdXn9fZxEvCT3BfzNlByUCcRaB/wuh82oztnonuyX2qT7U6A7G7ruP008syfmLQuRs3KZGaIurYKkDHknokXeInKl/X/qRZWssafOcubmJI0Y23SqsEO9dMMUYMlqzs3QncjfFWFKFs8U5E/9UMnbjZWTrtBI9q9mZeNmCfSLNs7O6fv20C6IjFw+8jELofP2+ZTfFOZ5vDyUnBkzoPEBgyX4PnWOV/+w+B8idrFKunEP/vmdCXiURkAj8GAJRLNOpqF9CVlq7sZg0ABx0hZOq+P6VWgIVqR8ZxOir8v8fbf2xuxvz1fFaQmdyZsJm8NmifDoR9eEN9+HnHbn8zJ3WSF47+gpHSXLeueg6tsy6hE0zLgjy5XRiywOc2/0Ez26Zw8OJHBJQCqGRpcljGyozquqdJfSL+56LPKObb49C6CzJs+Hd01sWdDwMkynvJEnov+KZlWVKBCQC34AAf+R9fX11y6v0pVF9IhTkKwzRPk3RyU4/X0znop9Xy44+/x1TvpjIWf96/dbFVN7n6ohO9vo4qEvanJycogx8og+C4rokH28JfQpZmq8iBzCclgznpWaKdTpL3Ey849sawdnOCzYfXMR6dAcrd/h6BYp57zCKNBRCTgI4BdP++5f2uH36rZhrnzfgsBgcsGo+uoEcE3QAuUN8/8peN4euSuicl9X/EygPL5t7RXUOqb9JSujf8OGRWSUCEoGfh4BKUjY2NjA1NYWLC3mIo6RK7kFBQYoDGXI0w1rSL0nw6jlvb29RDl+jWq3ztc7OzlEit4WGhtK3VVnGptbDZfA1fn5+urao5VpZWcHdXYmapraby/T399dbvx4hruVy1bqZwLg9IRTqVL/9jo6OumvVMnlQw23hxG1WywgIUJzlcD59HNR9Lov7o+bhvjs4OOgw+Hl37MdLipeEPqvXAcyhOW9OPNfN0nB08h1Hhm32lh5i2drYVtuFoRwTPavGeeN15Ce2PhLOEaaSKp3V46xS/5yl+0S6dueSG6LOxTSA4PXrPDjQJ3R1QDGFNAcs5R/ZeA8XSKKXy9Z+/EGVJUgEJALfhoBKjLa2tnj27BnevXuH9+/f66Twp0+fClL6+PEj7O3tPyudq0TG0iuXw+UxwTHh8d8nT56ABw38Vx0wMBmbmZmJPMbGxrqyzc3Nxf7bt29haWmp6xCXy+Wrbf7w4QOYSJ8/fy4IXCXXO3fu6Or29PTEixcvRD4mepXQuT9cP5fJBM5lMuHfu3dPR8LW1taiHzxgYBzUa/ka/QHAXbqGsVEHEYzXy5e0FNnVFa9evYoyAPm2u/Nrcsc/QicpmAnagcjajObNJ5Cae4b+2nGtpD6eCJ2XrbFDGV4zziQ+Rd20c+m8bp2dvvj5BAljuk+iuumVtWX2ZXEHjB9YEfHvFmXGROiC1Mkxzc7F18XNZgc2LnId+q95emWpEgGJwGcRUEmKyZZJiYnt0SMWYhRjNSZQloqZiPUl0OgFqmpmYyIwzsdJJT0mUyZPTkzSatl8jKVurtPExERXJBM6S8osFesTPf9Wr+XMN2/eFIT/4MED3XQBk+/r14r7WC6Xyev+/ftg8lclbi7jBl3Lg4m7d++CNQrqYIAHM/pt5zq5/UzO6kBCJXS+hgdAjI+aVO3C1atXYWFhIepWBzBx5TGMd4Q+hYzbnt40gxfF+B3eZOtn140zoTvSsrX3xg7Ys/wmDtAacnXbR25e9628hQUDj9Dyte1wdfAR9+PsrqfCYI4N6Hib1JHiqpMm4PlNc3He9JkdzZ1vUzzFCaO4MLKsV4ziPtEQEOFfP/5aXOdC5UtPcXHlkZftkAj8GQioJMUkytIoS7FM6KrKncmZpVMmP5XoYkJGLYcJm9XbfJ0qobOUzITKBKhK6PybiZIJkSXkFyTRqoMCzsskyxsTqppY6lWlYD726PFjQdKqhM3XM6FzPrWt6jEvLy8hLavH75NUzXn11fBctjqwUNvC7WMNgErKfPwx1avu84CC86h48WCBy2VJn9vF5X/Znez//zmLV4TO6vAV407B6p0LFg49HtWrW7SALRPb76Ylax/w0dgRH187fbJZvXfFGpqDV8h5F45ueqB1+E+uC4moWWXOfzmxBM9W8Sop8zUszb8lw7lD5Ks9psAtnIfn3NmX/KNrZuTLXXENK12//v8fclmjROBPRYAJhyViJiEmXP35YsaEJWAmZVWt/jmc+DyTOBMgk56qBmcSZBU3H2MSV7UCKsEzibJ6Wz3Okju3gQmRj6skzAMFJk+VTLnNXCYTuCqRc52qml5tD6vceZCiGv1xXi7/4cOHOg0A5+X67OzsogwG3NzcdIMRlZhZ8lbbwH+5TdwOLlO/D1ynflviyvMVrwhdqLNJGub57i95dVOlZWFpTj7UdRtdx0TMG5MtW8WLvKSyZ5U8W80vJV/uu5Zcx8G1d7B93hXM639Yt+TtEyM5Ycm+W3ili0ntL4ifzrO0L6OtxZVHXrZDIvBnIKBKoioh6pMtExhLwEzQ6vHPoaJ/nb40q59fX1LVz6/fBpW8o5enlqPfjs+VoZanf43+YCR6H79Wtn4fYiqbz7NkHj2pUnv0a373kxXvCP1H4pOz5fr2eVeF0xkm8JjKYkt1Vruzv3b2z2608DoFZNmODbQ2PbokztL2UTJ8u33m7Re1Bfr1SAn9dz/ysn6JwJ+DwJeIkSVefQv0L6HypXJiOhedvGMic32yj07m+vnV39H/Rh8Y6JNr9DbFNGj53ADiS/V8abASF56qBEnoIha61i+7Or/NDmNWjT+NWxRc5S4Zyu1dQcHq9ea+Ob8uLx+n/cXDj8P4oZXwNrdlziVdfvW6hUOOCqO7qV+JiS4JPS486rINEgGJQHSJ9XPSskQqfiKQ4AidLeCPb3qIRUOPUdCVQ2T8dltYpa8ad5rWpu/ELrI+XzPpnFjOtnTkSSLuA2SVvl+EUt1G0jvHLd+38o6Q3hcOPobdtFSN1enb5l4RS+VYyl876SwdOyB8wk8l8p9Jy+hiqzmQEnr8fFFkqyUCEgGJQFxHIEEROpPluT3PcIfI+sL+58Jf+wdyGjOm1Q6YU0S0+QOPktc4Y+xfdYeipD2hJWUfhJX62glnKW75Szwh47VT25/glNFjnN3zVDiFMXloQ0R+ELfJP/zDKx/w5IYZ3j61FYFYTtI69gv7nmE+OaORhB7XH3XZPomAREAikLARSFiETtLzgdV3MJfCm1p/cMWwRpvJa1sghjTYDBuyah9LErrxA2s8ImK+ftwY8wcdEX9XjDlFf1/RAOAUnCg++obpF8kd7FPhG57V8zwwWDv5nHANe2bnUyJ0O7Hs7cqRVzhPxM+Gc5LQE/aLInsnEZAISATiOgIJitCnEqHzWvJNMy+SRO6MEU23wZvWq18iSZuXjxmRL/fLB1/iMRH6nbNvsHjYCZK0rUgKt8YlCsSyhObMzd86YeOMSzi/9xmWkUr+8qEXQn3PUdis3rtgVp9DePPEDi/vWojjPIcuCT2uP+ayfRIBiYBEIOEjkKAInaVkntOe3fug4ou9O+13VhzF8Jw3u3blc1O70hI1Os+GcHyM/cJPp7w8l85uW/k6YVjHZRGB81/Oy5bv4hzl4SVufE5dSicl9IT/ssgeSgQkAhKBuIxAgiN0QaxEup8QrNbqna3XYyTfbz3+uXqiObiJXpc0iovLr4Nsm0RAIiARiL8IxHlCf0xLxj7nYz22UnFcyseErsZij7+PjWy5REAiIBGQCMQ1BOI8oTtYeYjlZnGJlH+kLaziP22kRCWSSSIgEZAISAQkAj8LgThP6NxRDpE6stk2EeM8Pm9jafncbFoCx/HYZZIISAQkAhIBicDPRCBeEDp32J3CnDrZeMHJNp5u1HYXOy8KghDxM++fLEsiIBGQCEgEJAICgXhD6PJ+SQQkAhIBiYBEQCLweQQkocunQyIgEZAISAQkAgkAAUnoCeAmyi5IBCQCEgGJgEQgThN6SEgI5CYxkM+AfAbkMyCfAfkMRH0GYhq+xGlCl+MtiYBEQCIgEZAISARih4Ak9NjhJHNJBCQCEgGJgEQgTiMgCT1O3x7ZOImAREAiIBGQCMQOAUnoscNJ5pIISAQkAhIBiUCcRkASepy+PbJxEgGJgERAIiARiB0CktBjh5PMJRGQCEgEJAISgTiNgCT0OH17/v+NCw8NR2hY6P+/YlmjREAiIBGQCPwQAvGS0J3NjbF14xqs37ARzyw8fwiAH73Y/dVVrF69F77hP1rS77/+xZrBSGKggSZRIuTvYPT7GyRbIBGQCEgEJAKxRiBeEXp4sA8GtSoPjYZIR28r33wmfgefhjk+Ee3IndgQnRcyAQagS91/UCRvJpwzcYn1TYgrGTtQ21P/1x2P19aCJnE9+MeVhlE7jC/tRN+OLVGpQnk0aN4O89dfhoxzE4dukGyKREAi8NsRiFeE/mRLV0GgBoaJ6W8ylChdFIm0xN7voOn/HcxQ+6fKwCKVBk0mrcDMvrVJujVA0fnX/u9t+RkV3prQU/QnCQ9SKsz6LYOkmPpxdVEf0a6MfxdB3379UOSvbGK//NgDP6PbsgyJgERAIpAgEIhXhL7wXyLP1MMAl4v0QU8DK5IhUyVjcteg5Yh9v+WGeL45hqSqtiBTQdz44Plb2vGzKn13/wTmL1qJPbu3wtk7+GcV+wPlWCBtNI2Mvnbmho3fD5QtL5UISAQkAgkHgXhF6CZHhhJ5p8BfWdNBk6sm7C1O6ST0QScslbsS4QNbe+dfKl36ebrA3sEezm5eiAgNwfXb92BOdYbFGOo8Ai729vAICvriU+NE5dnZ2cLN+0v5wuDp6kr57Mhw7TOTDGTQ5mRrBycnN3j5+cHP3x8OZq/wxjkgsv4IapML12eP6JS9bbaiBeHtnpWe0j0iGK4uLrBzcP58P4L8YW9jDxdXD/hQvb5U/6s7t2D/5a5/+W0K88Pbly/x/v17vH/3DnZmprh+7RqMze3w8f1zeAcqBnzhYWECv5CYb0LCeWNlTyQCEgGJwGcQiFeEjvBQ7F+yAJMnTcIrS3MUVSW3vFUFMdk8OYTEBokEGaXImgdnLQMju+1vqhxPkQIDd94Ux8M8LOhYcqRMmQL1hy1QiCHUHVkzGCJVEg2qzVbycXpyZAsa1vj3k/l7w/TFMNLoSszwhnujRYXcumsaTV4fNV+AE4b0bAMDMkLTlzqzl6qJrRdf6PI63VqO2hVLfFJ3nfajoT9Tv2ZcW6T7jDQ74sxbUd7p1T2Q1VDPBiFZOvSfMk8MRszvbERO6rdoS6Y6ZBFA+dcNR8kCf0WpO0nyVOg3bq1uMBDkZYlezWsKVX10+wbeP+IABNs/QrlcaZA4ZVZMWrld17c+9aqh28CRQKg/MqdNJ+5FyfqdY8bT7RWyp0umTLskTYGFd6xFPo8355BDrTtRFmy/oPRVSfYoTOeSJzFA/XWRmMZcgTwqEZAISATiLwLxi9D1cLa4sFZHHotOPhFnjo0pRUQ0AMEfjopzOQt2gE5o1hI6Hx+4S5njDnW31JVRb9gscSw81APZUyjEVGPaDV2N+we0jCSrJMmRJRNpCfQIbOGJT8ki4P1lymOIm+6hqF82J/1Oirt6Y4zBtXNFlpE4JTJlTBWlzGVXzUX9rzd01h1PkiIt0uoRcqnqk0Qe823K/DdveYuVRZUqVVCtWjVUJCOyChUrYc9TO1gcnhyl/Kzp02v3k+C4pQP+SakSMk1nBISIcie1Lqa7Jk3GTEjGVvDaepZfeC3ybOtVRHesVLlKurrLlSuPMqVL4xWNDM5P5MFQFczp2QCaAhXFdWfmtBbXDT3riPBgJnSFrEvUbxPjG/VuYys6/y98A95q68sHT8p5f9VEaDLnQygckJuuT1axg9719iiibW9dSegx4ioPSgQkAgkDgXhL6LY31ulI5PBLR3E37myOJDUmhsx5C0VaQv8goa/t2xqGOQvi7FMr3SDh/YXFkQSZr/unT4Tnm08k1r2KUAnf25HX/t1yFgLDlON3d0/XXZOibl9x7MXKKtDka4Rbr23EfkR4EP7OmlLJl74wWKM9ojAPGLRkW0z5W6RSe9wwsdW19+y0FtAUqQt/m+swpPP3Sb3fQHtNxQW38XLLYF0Zc+6SWE1pYtMMyNF4PMzclLlqT+OTkfWUn0BHAlBeXzLPxnUnQ9MBk2DvHanmf7yMiFyXLxOGNeZ8BqgyeKEoNzaE7vtoxSd42lHnX2yeHfV41jp690KR0LluSegJ46MleyERkAjEjEDCIPSnCqFHhIfg2pGNpJY1FB/wn0noQf5ETsFOKJQvDzJmyYxs2bLjOXHc3PKqxFoGTHkLaudE8RIl8HfuXHhNQq6HzVvMHtZcRzgqoT9a2VR3LHn2fMhYZYr2DgWjHamHBfnlaCiOhQb4fjI/f3pgUSVPmoJgoX/WP5Gq/ehq7zJLH4lyDnejdqRIg5LFCohr35rd07Vh+Nn3xNa3dPtz7ymEHhigN/eubeH4Eto+lxjNVIz6n1G1cx3bnrpp700gjq+fgXLFS6BEiVJoO2g6Hryz0dk6xEzoPmhcOh/lL4GK1WshhNQtL26exn+VC+rayYTObXh5+SCKJlEMJDWS0OX3TiIgEfgDEUgQhH5QS+jq/atUKOO3E/rQmeJyfZV79anXozwSL4+ShGiYFDtXr0AuUk9nyFsao+qrhF4ErpTb9sFGJNYSXJ0+i5TrrQ9+QuhvDm1A0SyR6ut8jRWVP6cDHaISekzP5bWxWlU4ETpz2sX1a3DrxVsy1PNEcHAwnExvIhctp2OCS5kuLwKEDZ0fFgzvh85dumLOFWsMqv63OJ8kZSbYB5BxmcenhB5T3bMq6RM6sG/JCpiYWcLD24+M00Jxc+9CXX9bjF0Zq9cqKqG31l0ztYHqdyAJ9r/3Esc3zegQjdD5aCgGplYGclEJ3U5K6LG6AzKTREAiEN8RSBCEvvXyG2g11uJ+VCiQ4ZsJvWafSQght6dM6Nm0c+iVxpxAkJ4xuclJInRyIrN0xVRkJOIQy6kSk1SYJAnVpxA6SB3+T1Z1LjwXTPiY2e5PCB14i0KJlTnjNMVGEB1Fpr1tvp3Qoz+IYSFBaFenuCg/RdoM8NcHiDP73EEK7cCj+MDTyuXfSejR6w52eafrb4NR88TpQFdTmiNPLo7P2X9Hd0mHqv/gv87jWQ2hm0MvUqs5/IO0iPg90Q2QNJrWwtnNmiltYiD0EPSLkdAt8Le2n/9tMo7v76tsv0RAIiAR+CwCCYLQmSSKNuii6+T3ELpGQ5bmmQohIMxXZxSnSUTSa7Lk8AhUTetCMbGmBoYkpZcrq5Bx5KYldGpFxMdduuPZSkyB781ZnxC657WlumPLnylqaTX9KKGH+FsgE2kQVKc7MRK62R5d/V0OWv00Qn93cqQoV61bJfSLs/+jgU8HrO9J8/h/VxD13Z5NHukob++LdlHm0MW9SJYG77VL+LbOjpTIV1z8iBUDqn2Z0HPV4yEEZveprssn2lPn93gUlN8fiYBEQCLw/0Ag3hK65cXVyse64kRYXV1FvzPCghCbWb40Ea6yDCxxEkMUKVIYhQsXRpGCeXUfd9XKHX722mOFYO78UVhIG73+iL9SU7nZ68DRUvEEd8jYKcq9CA9XxPar60bHSOh8rnn1wgqxGSRGriyptflS4q52abfX9WW6a+c/iFr+jxL6gyUNqexycLG6GaOE7mt3G8XzZNHVnzpbPoFR0QKR8/DqHHpMD2F0lbt+nr61s6NQ1VGwuawY/f03QpHQX62pR4SeHrmy01QBDYhaF8lHy/U0aDRomTgfERpIywXZ0O8vvHC0Q2m6dtLlD0rR3pESv2G6zMioSuKa4sLKXUl6EjpdmyplUl3//mnRDx/v7xX7H9wV632ZJAISAYlAQkMg3hJ6hPcH/MWEnqgA6lclI68UpYQ6tmOa6JKzvhSt/B6gXbZGK9HRu7Iy71q/EREO/T1uF4qm+bKKAULj5nQsURLcsIykjSgPQLCt0gaxFUYUWna8hlxRJHgNclRupbvc69o8HeHMux+V0I1aaVXu2Rt89nm7OFo7h55amUPXT3aXWSOQEk3+UyTUXN3WRHG04211Xli5Rzee09+fozWKi6kB0ypory0+6pPTk7tWRaoMf6NhRcVob+bxFyIPGyyunj0ODf+rgdp16tHa8zE4ceeN3vXhmNhSsdSv2bCR+Lv+oWKYx8n89GQ91btSf+MVt/SuD0GXVNo5dDpnmDo9Fhx/hMmtSiMpLWlrWrOsuMbGN0bvPwntvZb9kQhIBP5ABOItofO9CvI1w3+1qqNyhXLY9oDXg4VjR6uGqFm7DurU+fy25urLyFtNXtN6tm+GqlUrocdY1X1sIEZ0qo/KlSujz7a7X3wsnhgNRK3atVGh5jBh5a6ffB1MaU12VbEevPO4pVG8sgWankLFytVRq2ZNnLTwjnLdzYV1UaNWHVTsrKwxjykZG/VE1Rp1UK1590+8vRF7YmmHlqhctTK6DF39yeX+Ts9Rn9r8OYxq1aqFg6aen61735gK4tryQzZ/mifUBb1o7Xu1atWx8sS3z1lPHdKV7kVlNOm8KopdAVf09tI2uidVUKVSJcw5cDvSx4BoRTi2tagv7n3VKo3xTK/54zuVQ+UqtXHM9FOL/T/wnZddlghIBBIoAvGa0BPoPZHdkghIBCQCEgGJwDcjIAn9myGTF0gEJAISAYmARCDuISAJPe7dE9kiiYBEQA8BT09PDB06FIMGDRJHBwwYgNBQZVnjvn37cOLECYSEKMaObLDq6+uLYcOGiTzTp0/H8OHDMXLkSDx48AA3b97EkCFDxHne+vTpI67jst3d3bFt2zYcPXpU+HKwtrbG4MGDdXk5j7MzBX6iOjgfXz9ixAisW7cOfhSIiOtRy+U6AgMDsXbtWpw+fVoEF+I2TJw4UdTH7eW8bm5uCKPAQlzmhQsXcOzYMUTQNCDXw33u378/grSBnSZPniyuuXr1qiiD+8l5eOM0cOBA0Teui8u8ceOGaB9v8+YpxqmcxowZAy8vL8ydOzdKm2fPni3O9+jRQ/xlbLgtvHG6f/++6KuVlZXAldvFdat95t8vXrwQ+bl+S0tL3XkVZ7533t7e2LFjh65uxm0Sxefg+8V94PIYaxMTE3F/r1y5IvB6R8GZuH+8qfd79OjRoi9c56VLl8S148aN0/VVbf/UqVPx9u1bHDlyBGPHjoWRkZHIw+e5Xi6Hr+WyOfgV3zeu9/Xr17r7oCs0Dv+QhB6Hb45smkRAIkDOmmxtdQacjAcbNzJZcurbty8mTJig2+ePswtFBeQ8TMp58uSBgYEBkiZNKj7ia9as+cQYVC2TCbxt27bi4+5P0QKfPlVWuehvTJZcB+fjcpOQD4r69evDlaIgJooWZIkJt169epgyZYogV25D5syZRbu5/ZzfwsJClMcEyATLdTO5cz1qvVwOp6xZs4prli1TVoZwnWoetQ+8f42iETLhcV8NDQ1FG9nboppSpUoFe4oAWbRoUdEHsRqHyi1YsKDIwvvqX26LSug8iGnQoAEePXokcOVBjHqt2o7jx4+LvnD9Dx8+jIKdWqajoyM6duyow4vbkCVLFnG/9LE+d+6cGNAsWbJE4MUEq55X73/KlCnh4OAg2rhgwQJxPl26dLq+qoSeM2dOXLx4UQxu0qRJgy5dlGXO3D8emHDQLvX+vaTojnxPFy9ejLNnz0bBOK6/j5LQ4/odku2TCPzhCEhCl4QuCT12HwFJ6LHDSeaSCEgEfhMCktAloUtCj93LlyAIPdTDBtdv34+y1pq7HxbghX69+uCVM70Q4R6499Qydqj8hFxu789j3mYlKMqPJG9HU3QacPCTvn17maF4fWYddr3wgqflc9x6roRm/dZE2rQvp0BndOzcA689eI4zFCbvtOHlvrUimV8ioEVAErokdEnosfscxHtCD3G8jlSagpg4sClSZCmFoLBIxyEOr44h/z8aNJu/E9Nb1kLns7axQ+WHcoVjREaKlZ69/w+VwhefmzaQXKDmgfI6fz6FhwQiKPTzTBtBftKzZkyJmmPW4uX+sTBM1wQDqyZH3XHbPik0guaUgoP1PcvrZwkUoVJbnFCi28WU/J4upzmnLCg6bAsWdMmGOfte/DAOsoA/GwFJ6JLQJaHH7hsQrwg9IjyUDCA8KJSoQtqhZERhemgaurTtiMaNm6Bjj84wdfOHn6cLvP3J6jUiBLNaEblqcuLqRw+6IgKerg4ICFbIj4nQ3tU3moMSPh6MkLDIqCwhQf5wJMtHzwCtT7aIMPj7eMPFyRlBIQr5Bft5wdVLcS1za3krTN+jOK8JJYOLcHWMQQ5fPB2dEORNbaBAMCJRX4ICA+BEhh1efoqhD6cwasP7KyuRq/J8eAdxnkBdORx4JSAgED7eyotudaw/mm5iBzhh8PPxhL2DIwJVgufyKfzq5L/SYPNjB2yd0QsdmzREw8YtUWfwEgUHipDm7+cJBydXuD3ZBcNMuRCq51AtPCyELGo94Ojuj6UzJsCSoA2mNrs5Rm0z4xvo64Yp5J0tReq0uG6tOMwJ8vcRhiu+Kn66XsofEoGvIyAJXRK6JPSvvyecI14R+q3ltVGlkAajNh8XvZtXPRVsgoFWlXKhRNve6NuhKfoeeIz8zVqjYhElAAincjmT4gm9E57vybd5u0HI+PcQOhqB1kkN8DdZdz60VsJyqul8vwKYcfKj2A2wviBcjtZp3EBYO4654QxYbEPKbMXQrUldGJIfdC8aH/zXqwvKaDLAnInwwwG0nrtd10YLP2okt7eTNoY5W84mKQwqCd4fH4ny+4wchQLJNKg75zYd9UCxpBp0HDAQbRs2xR5TbxTOkgSvaLwQ7mWJDKmTwZD8oCdPkRo7Lp1EutQpYeEDDO9YDllylsfIfk2QxDAZHlP4t4v7ZyNthgoYMLg36rftj2YGGjTvNQDJDJNg5JYTol2XxudA9opNMbCeEmd80X2tw3ktICdGZEKmys0xsOk/0KSsS65m/YQf9j6DByI95R972U7knFG/MvIULI7apfIhW6588CcsTA5OoDJToFXHFjDQJMbGm/ZRsJY7EoGvISAJXRK6JPSvvSXK+XhF6L6ObzC8YUq0mmckGr/ov8RwCvUnibMeujRsgOa9x4q55pP7Vgsf66XKFUe+PCWRNVNKFC/biQg0HBuWTiGCqYi3BxehyPTrONRRgy33tNHGtJhdGZYT044phL6pRWLsfWWLOSMn4sn1zUhaZDRJzyQta/MOonreeAXh6sk9qFygIN4yoVMks6YzFLeoi/4zgLUgdHeUzVQB1m/PofPyexhQLhdWPPYm6TgI5iYPYWzjgeB3F5A8ZxFaYPoUzRo3R5kaVTFmKfkrJ8k+NwWMeUGE7mpyAZpctZF32mkMapwXa1+5IxNFJzNxDYC/hxNuXL4gQsnuaJkdE41eYNeUjujRtTUK/1sDDxyDERasDU7ifB5ZWs0UbXxxbCFatm6PqePmYdzAVuh82DLK0+NHy4AC7D+KEKwTWxbCXpoWD9FK8PfmDUeR8edE/sLZkiF735X0yw//GCSCFbW3a/VsWHHzGS0vGoA7y9qj1ogFsXsyZS6JgBYBSeiS0CWhx+5zEK8Inbtke2w4ms1S5n4X1TOAXYA63+uKFIbpSdWsUG3z/Bkwd2ZfFC5ZQoTynLyVJV9OFHc7fQM0a11dBFPZ2lyD7TEQ+oyjSqSvnR3So0rpjCicMS3K9h8BwyxdovgYX9tUA1N3xUf47I6V8J5/WO5Bi7lGujbaCkJ3QoV8FTCnU2ZU5faUK4b5z3wR4PgCSUja1WiaUh47JM2Wn7XwSnq7H8lz0/pROsCE/orfa5pG2DqlIa6c2wRNhb50IAydSOp+5hwAowFtkCJdHqRZZYIzQ3Ji9Iab2oKAIy3SY8UVjkenJnMUbzuTdsLRhtpz1dQUpTIUxuKx/dHkcFRDtjAvKxgm4Qh2VSm4yj84qAjkIpkfG4tCQy+J3yXzpseGRxxQJRgtKP9Ham+PhsWweN1KVGlWEyfOGKHm4Fl6bZA/JQJfR0ASuiR0Sehff084Rzwj9Ai839YHzWYwoUdgZq3EWkL3RVVSUSelud8g7eRvA5IWX5NEGUxOG4plSYRHpJJWkh/yEDlW6zlB7Bm10GBbNEI/O7KQEvVr40tYn2eJPj/MPEPg+uIwkmTtGoXQVzXUI/QO5bHntBGSJ2GC/pdkclKz11AcU5y2DkGPsqmxasMkJB15EmvrFMciInRfu8fQpO2CV46k9vcwRjKV0J0UJwp/FSsjCL1wDg4tmheK3oAOWRxAilr9RH9aEaE/dQrAoQFNUHKUEQ1qAnGsX3aM0hL6iQlKpLGNdxWi9rG5hWS0n6fbItoLQW2t8wzjV4oThaEno1umh+PMxhU4vX2sOK9P6B+PEKEPVwg9F8VgfynMCLSE7hWBHWSsWLj3NBimSo8w2zOoNkjxRiWTRCC2CEhCl4QuCT12b0u8IvRAj/dIZajnuSlpOiJ0ViEHo0FWDbLlH0kGc8DKmuWieXhKirfKNDalQOSmueqVly3FnlEnioF+3yYKWkGvd4nru2x/jUDby8g/5og47/rmNJKk7PYpobspEvrcDnnRdckpXJxcmebIa4BNwl7tHSXKumJFbihb5keNCfuESnxepSJY9swbPnZPkIQIWfWAlL1QKUVCd7uBFHRdy3knaScCq9vzIKMQyfCsgQ9DwexM8NrrEifFa7dg7B7QOkq/J2xSJPTL02qL40/sFBD8nO4jOWkFRuxSIsmdnN5CnH9Fc+6L+lZG20P6knwUaNC5WGIc0uP7j0eJ0PudFZnypiVCFwOnYLSi8sw8Q/Fmz2CkzFOaDPtIc2J9ANUGzI1aoNyTCHwFAUnoktAlocfuMxGvCD12XZK5JAISgYSEgCR0SeiS0GP3RktCjx1OMpdEQCLwmxCQhC4JXRJ67F4+Seixw0nmkghIBH4TApLQJaFLQo/dyycJPXY4yVwSAYnAb0JAErokdEnosXv5JKHHDieZSyIgEfhNCEhCl4QuCT12L58k9NjhJHNJBCQCvwkBSeiS0CWhx+7lk4QeO5xkLomAROA3ISAJXRK6JPTYvXyS0GOHk8wlEZAI/CYEJKFLQpeEHruXTxJ67HCSuSQCEoHfhIAkdEnoktBj9/JJQo8dTjKXREAi8JsQkIQuCV0SeuxevnhF6AcPHsTWrVvlFgMGO3fuxJYtW+QmMfiuZ+DcuXPo2bNn7L4a/+dcktAloUtCj91LF68Incn83r17ePPmjdz0MDAxMUHKlCkRHBwMNzc3uUkMvvkZePnypSR0a2u0bdsWo0ePhr+/P54+fRotJoQG79+/RygFfOJ8BgYGSJIkCerXrw9XV1ckSsQRCSPjMvj6+qJevXqYMmUKbty4gaRJkyJz5sziy8wExfktLCxEeWFhYZg7d66oOzw8XNSjlsXlcMqaNau4ZtmyZWKf61Tz8L76+9q1awgJCcGaNWtgaGgo2liiBEVt1KZUqVLB3t4eRYsWFX3g67jcggULihy8r/7ltkRowz9u27YNDRo0wKNHj5AnTx74+fnprlXrPn78uOgL1//w4cMoeKhlOjo6omPHjjq8uA1ZsmQR3y99/HiQ2b9/fyxZskTgdeWKErCKN97nxN89BwcH0cYFCxaIc+nSpdP1lY/zljNnTly8eBEjRoxAmjRp0KVLF5GH+xcUFIQUKVLo2sPvAt/TxYsX4+xZJWCViknsaPX35Yp3hM5kzjdQbpEY2NnZIXny5L/vKZI1x3sE+L2SErokdEnoFGBKEroyoPver5o62vra9SyhS0L/dDAjCf1rT448/zUEJKFrYC0ldCmhkzQuCV35WsRLQreztYV9PJf4JaF/ja7k+a8hIAldErpUuStTJpLQ4ymhO7k4YXbWVHhp5fRLVfiOTk5wonmjXzVNIAn9a3Qlz38NAUnoktAloUtC1/9OxBsJ3d7BEU52H3Dt5h38Z6iBiaM79i2Zhf5DO+GZxbfPzz85Nh8rDz+B2cOreG3rrEfcjrA2vYdalf5BrZpVsPqu2ZdJ3dEZD46tw7orH/H02kk8eW8Tq0GAJPSv0ZU8/zUEJKFLQpeELgn9txO6i6u7Vvp1hCtZN3t6esLVWZG4HZ2cdftOzi5wI4ncycUNXh/OIQWpVkr/m4UsG9Pg2YFRSKJJjVLl/0XiGovg5uUFD1dnsgR1gLOrG1ycHOHo7Aw3Vxcq1xHubq5wICtRLtPd0wPXprdFyhLV0LzdYMrnBFcPL6UNTi54cHQp+vbth77dmqDT0FXC+tiDr3fg611Fm7itLm7u8PYNwNllnaHJVxlN2o+DM+17uHNeB5oWoHo93OFM10WX9CWhf42u5PmvISAJXRK6JHRJ6L+V0B0dbDGjQxMcv2cCi5f30KttAxQuXAgDZm6Gg4sLbu1egOJFi6DnrD24ZDQP/ZecxfmlvfBPgwHwCInA9Q2jUXPUBrg7vUHHtm0woM7fKD77NCY2r4Ta49bC3d0ZW0a2xcITj3F//3J0HDwbdh8uo1mrEbD29MKBZeNQtWwZlKzSVEjTzkT8DnbW6FmjBHrMPQgvJzNMG9AZRYsUReGMqVC/92qcmtUdpZoPhwVpBQ6vHoPe807CwdoMk7o3RL6/86HD8GVw9fLFhc3zUbZkYZRr2B0frOzg/fEKapSpguNv9DUAijYhRkIPeIuS2VKIOaF/B69DKN2pj1e3KkswDJJiyUVzce+OzOqoXZaRGY8t3emIF4bUyKUs/6jUGa50xOH1ESQ2UB72kfvuABH+aFM6h3Jd0r9w7JUHHQvBkGrKsUy5a8KfKgwP8oPZzS10LAWeBSiPyuCO1aMsTTELBs4s6qk9lgF3PrjC8+11XZ5EeWriHa3aebp9MAy0y0f6rDwjygryMkHNXJlF3kJdj9KRCPj42KF6Jg26n7AReaZXKB6lvm13HbGwV3ntsUToseC0yOdt/xb1iijLU4YuOQM/y/u660o3HiHwU5IPyuZIhFotV4m921tGK/mSZMVhEx/RhmnNlTpTZSgDNwYizAZ1C6QVx/K1nwWGwvHFeV35o3Y8AAKMo7RTk6OBtr5wDC2YE5qczXQt+BU/JKFLQpeELgld/9vyf1e5OznaokeaJNh45RVeXTmIEUuN8PHDKyLPRBhudAZVUxSCpsFE5M2UFPP6dkOBkQexoaWy7nLnlUvKBzRREryxdoK3xwcUpf1LZo7InzMDSckN4RXgjSW1Nei37TLurOiELP/Uhd3bY3RdBdjQuVndO2LXrsM4PKcLNIU7wdPHFc7ONkiXOhk0eTrCw+kFOgyag+1DlQ98s0GbsaxFSvH7ymsnTO1THQV67cW6+qmQp9UoWpNqgja5NPhn0VXMGTYVt01MMe6/bKi35Aa8nzMxarDhJWsJok4LfEro4ZhRjEigxgD4mzJxGOKGYwBq5dWgft8DeL13nCjLyvic+LvxhTf6ZUgFTZNpMN05B5rUWeDp54OSWTXoOXMfWlCe1MMP4sX6TpQ/P5xA5L3oKJF2GHrVy4ecJSrA/uw0xaDE0x2VmcymX4HHsR5akkqHl8qyUQxsVx7/NpmDYFpHyhqPCIuLIs+KRx4Y9ldGaOqNgr/ZU1x+64yQcHcUTaFBxzWP8XzPIjh7+eDIkFrQZKjCdI6MaZKizdTN9DsMgX5cQSDy0xQKlzfwpL2oz580Ni6unjC7vkccP/rSDUf27kJAWCger2hLx8qDxhSoTeeqjTuG0AgqKyQUzh9f4bq5K7XlLGlvNBh3iwctoSiQK5Mop3anlQjzs0HaZBrMOWiMtSObIU3m3HC+tkicv+Hog3r8fI08ht3takNTrAFCXU2RxEADowcO6F1Wg/zVlsHp8mKRn24BXGgQ6urpi3+K5kYKGkxxMjXqpWAoCV3gwIn/qitj+vbtiwkTJuj2eU0348h5eF0zr5fmdc68BtzIyEiszRZ46m1qmdLKXa5DF98wuWxNec++V3L43mVrjkToPdMmwY77H2Byer14STMX74AmJf5Cz1VXcWXbeuxb2R+aEj1xfFpvFBi+D86k8v47oyFK/VMTe26R04BEBnhjQ4Tu+R5l6fo7LgEIcHuMVFlrwJ0JvU4koWf/twFcLU5TPVVhS84eXhxbi9z16qN48YLQ5GqP+8fIsUHKXKRGf4LEf7eEl/UDZEjMH49M2DezMqp3XA4nyzcwTKTBtXcemDuoLgoOP4LVrQyRn5w6VK5cFf1H9oWm1kK4ebiR+t4NZ9b1hUHDlfTbhYgmCC6OsVi2Fh6CvDkyoNX0tUCwncBlt4kTKqbRYNQpO/jYXEFiOnZuP5NJRljQjTvZkyTI/IOwdWFnpM+RR0ikdcvnQ9tug8X1ix87A/Y8OEiCx1ppm+93hyIGKFZpLl5vaEPnqghynPyvBkkabxWPQ+Cr/XQ8eSSht68Mg8SGyJm3ICo13wTflzvofFqYUt7zA7ND81cncZ233V38W4IGZJpceKP3YC3vShJ+3roI93mHZIRtytQpYJg0FRp27EuysZJaZI8kdPXS8flokGXYUkjHnOaULEADPRrgzDpOQvcT5QOfKi0MU6RGs8kbdDWGvlYGUsdtgcerW6HU4P3o2vJf1Om6CiHe5khF506b+uGx0Sxo0maB8bbulL8kyfHAkhpUZo1VaFa3BCp26EcqCx8kNTTAgnPGaJ9Tg8abqdehr5GGyjB6oTgPgf1VcW/mnf0I17fX6PksAvNL06HJLiV0SejSsYw6EJOOZWJm23jtWMbJ0QbtUiSjD2IB7N5LklHS/MhYsAiS0gex1/KrcHKww4S2ZVG21wRMblsDhQbtE3PghXOkRqaWQ+Dw9JQeoX8QhJ4vRVY8Or8RSVLmx/4DR9C6MBH69su4vqg9EmUugFbTZuPvYkXQftx6uNmaYnSnUahbNDURejuSEpzxbw4N2vSmefC/2sDRhMrX5IBB1lJIl1yDhj2Ww+GjMZISEbVp2wSp02dEjYLN8HL/SFLZ5kHTzpOwf/1AIoHFNNfvJAj95OreMKizHD7vjgti2flKmVPX3z6R0MNDidAzouV0IqZAS3HdPpJ4mdCHHbKAr+11QRpn9rJnpUywpGfjVG8i9KIDsWV+dx2h1/43F9p1HyGuX/SYJFSro/Q7KZ4FKQ/Tkt5VoEmXEybE/q/WtKRz1QWhTylHhN6aJWfSJL84GIXQ3z5/Bgta77tjLEmtVO6V05vobzq8o7wXBhOh5+sorgvxt8HhXcuRI7UGLWacEsdeGs2BIV2z9JAxAl1NkJwGRsdee2D7pC50b/IiOEyh9ObZohK61YWNVEci7LfSNpzyvL13DP061BWDCQuru6JfD/2A/d2rQpO1sdIGx2dIn5K8bI3ZBri/Eu3tMnw4iuTOhBx/V8fMs0+Qmo6deO2NJ9Q2JvRXm0hbo/kHTM9LaxKh11+BpnVKEaEPJEWClyD0xRdeC0JvsPIFHXsrCH3HSx4CBKN1HrqmYB9R//gS5MGrWFv0a1qOnu2cWLDhUMxv8U84Gt9U7uzdi1OfPn0wceLEz0roefPmReLEiZEsWbKfLqFzueyJ7Wue4qZOnfpZT3GWlpax9hSXLVu2b/YUx5qJz3mKK1as2Dd7imvYsOEv8RTHXvC+5inu6tWrOs2KSujs9S66p7j06dPr3gh9T3GXLl0SnuLSpk37VU9x7BFPeor7zg/L90roDrQU7PbOMUieoz1s7d8gQ8b02PrYGq1rl8KYXbfhZPsRBQrkR748uZH7L1JdzzwhCL1WmXxYcfktPjw5R/PWf+OdLRnLOXtg54JeyFV8AE7MqIEcufIhd+5cyJcvL6YfuA1vmycoRu4Bd957g7JF8mDy+stw1krLHq9PIEXVvmQg5wbbDzdROH9OTN54BV6u1hheKQNaLjuPdd0bo9OYXXC0eIvCBfJh8blXWDWuHcq32ACb+5uQptlEeHh74orReOQZvJVU92xU54qL28fi72E74PX2KNKkToPj79y/Tugkqy76Nzc0xdvj9ZV1glAfuITgvyJJULX1Ipyex9K0AQ1ImMg0WHj2NTqnSYlkHefhw8F50CTPgOfvX6MgDQD6LjqNjkScmlbLcHwCEYumFEg7jIUNlamL0ccf4cEzY3jfXy72L78yFgOj4ivuI8LHCU9Za0H1H3z2UUj9ixfNgcm799g2glTnlM/RXpGOZx57iV5ZMsCw1RS8ubATOy48wPvXt1CI2tBoyC68Oz5XDELaTlkGk2cPYOvhgZTJDDBuy3VsGtkKSbLlIxV9OBkyfkBNkrw7b7gJc1dFz9+5fHrkLFVH93ROHToExtSG3VNaiLptPK3F3z47H2Nrj3w0qGiLYJqfF9JA0g54+e4lXt6/i/GzZmPp8pUoUyAj8hQqiaMPPyA9TQmMWHUe0zqXQ9osheH/YrO47sjjN6hKf3PPvIhD3WngkLMq3tO8eRLCct8TVwyqnhq5Sw3DnQ2kkaF8JjQSsn3Ggx8apDxXpguOr5qJpUuXYlav/HQ8F5YcuPadb9jXL4vrhK66VeWe6KvcmdBHjhyJgABF98Iqd2cyYGU1O5N+rly5BKHztn37duE2la/n86rbU7VMJtd27dphyJAhwq0puziNntfU1FTU0axZM10Z/PFnNT/XoV8uS0tM9uPGjQO7Y+XzTCacuL28b25uLtykcpkzZszAcBowsjvSt2/f6lyRenvzGwdxLV+jun5Vpxb0NRf8m4mLyXHFihW69hQvXlz3ELC7VBsbG/xN3z79NhcuXFjk0S9P3/UrO/aqWbMm7t+/L1ypskva6PgcPnxY9IX7xPn4vkXHmV2/durUSdc2bgOTMN8vtTz+e+rUKfTq1Qvz588XA7YLFy58QuhcNgs0qutX3udBlppUQs+dO7dw4zp06FDR5/bt24ss3D8umzFR2/ns2TPUqFFDEPrJkyejtP/rb9LvzRGvJfRfta47vpUbs5W7A/7JnkS8AEVG7qBZZsD5/i7tC5EU6+6SDpnS2ZkKofD22p5EVJqbHlNbmSdOUq03+Iib+aVIo7gjTxHhZQsDPb/VyXIVpfnlcAwrRIMIui5zvuIIJ2HZ/fwMXdkagyR46RaKbvVLKscMDLHhjrVow9Ulw3T5ntr44+N1hRR5y0Jzyfw5W1o/6rznPkvA5tpqIbFrNFlx9I0bfybFAEC9tvLaR3A5PlHs7zBlBJTUsZLSTg31YfrRF+LYh/PKXDZvJ1+74e2pFZFtp2PVWq3WXd+5cQnUbKv4036zb4I2X3acs1UGELNrlBHHUmfKDr9grtcLjQqRyp+x6bKELBBoSsE0UtqYvPcZHQlDW64/dU+h5dBPziepjnSqkVy0kz9pNy4TOn+U+aOrDvz1BQCVDPVhUPPzMSYJ9Vr2Mc5ko+7zX3UgwL/5Ov3y+GPP5/XzqwTBhKke59/6bVSP8zE+x3WqxKFqFrhtap1q2zkfb2od+uXo94XzcIoJF75GJWH9vsZUrz42fJ2aR8VE/avfPu6P6v88pj4zxmpS+6yPM5ep4qKPK9cdvTzVJ7yKiX55qn95fQzV/ur3VW0LH+Pr+f7yb/6rn/Tbwvmi37foWPyk1+6nFyMJPZ57ifuslftPf1RkgQkZgbhM6Iw7f2R5U3/rk4Z6XP/+6OdVr1WJUt2PqUzOo16r/tbPH73e6GXwPhORSjj616rHVDLWzxe9j/r70ctS+6HfFj7G5eljod/v6NjE1D41j0rK+uQcvc7oOOhjG1M/o9+j6PfgS/dLLU//fujXr/6O6Xx0jNQ8+m3Ux1odDKl/v1RmXPweSEKXhB4Xn0vZpv8zAnGd0P/PcHx3dSoRsHqaI6ft3r1bVxZHCxs1ahRu3rypO/bixQuMGTMGrKpWE6vnOd/t27fFIZUsWfXO+dV0/fp1UYeaj4/zPDNPQfA5lahY9c52Bk7ksVJfhc5RzNT9d+/eiTzTpk0T0wec2KfH5MmThepe1R7wHDSXP3bsWBHdjBPn54hynE9NrG7nfPv27dO1Y+3ataK9R4/yElNlkCbTz0VAErok9J/7RMnS4iUCktB/zm1jkuK5V15Kx2ngwIGYM2eOCIdaoEABcaxChQpi3pedTHHYUE6NGzcWxGxsbIxKlSqJYzxPbWZmJn7znHqmTJnA4UnVfT7PiUOj8lw8b2zoxqly5cqifJ6r5rl5KysrEbqViZntA5ioM2bMKKR6Tlw2Dxi4XalTpxbHOD/Xz4OD6JH4OIw1h0DlwQaX8+TJExHaumvXrmIqQQ1h2qpVKxFGlcOVrly5UuRn+wMewOhrAH4O+rIUSeiS0OVbIBEQUQzjavjU+HR7mDDPnDmD7t27k2Mqd0F6bNy1bt06EV+d08GDB9G7d29h8Hj+/HmRj0mQCW7AgAE4dEhZzcCSPEu1nNhYj6XevXv3in0mx7t374rfnJ+NA1lCZvLkdOzYMRFLnMtgSZuJlA0EeS6Y1+KzgRlbzavz8TwA+PjxI16/fi1iuHOb2GhOJd1atWqJclWpmvNz4sEBW75z4jpy5Mgh+qJqEk6fPo0jR46I9rJxIA8yateuLQwOpYT+859sSeiS0H/+UyVLjHcISEL/ObeMCfLVq1dC4t21a5eQwNminNXZTJKcWOJt3bq1UKuzZM6EXKZMGSFhN2/eXFjEM5HyPkvXamJyVlXYTI5sLc7pzp076NChg1Bn8zEmVj7GkrBK2Kz6Z4tx/blulsBVwm7UqBFmzpypU6dzuWnSpBHqdNYOsAU4J87PgwFWuav7GTJkEIMBfobYqp2t3zkfkz1L6txvtrxnDcXOnTvBS824nZLQf84zp19KvCJ0Hp3yyI/VV3KLxIDnv7p16wYPWsrFL4rcJAbf+gzweyUl9J/zgeU5cSZ1ThcvXhRL0Vgat7BgF07k6pgkeFZB8/K458+fi2MPHjwQ6nl+j8+dOyeIl8/xdWpilfz+/fvFLs9f8xI6Tizl83ULFy4EfwvUelndzqTJBMxSNyd9ozCV0NnqW1Xzcx5WoXPi7wmr0FetWoXq1avrrudBB2sLVOt+e4prwfkWLVqE7Nmz6wid61RxqFu3rq7//B3fvHmzVLn/nMctSinxitC3bdsmRrz80MgtEgN2b8mqLl5ryWo1uUkMvvUZYJWwJPQf/8Iyga5evRrLly8XZMoky0THqnCWyDkxwbMqmsleXVPOqnW+hgmb10pzYre2+gZ0+oTOEjir5zn169dPqNgfP36sk5xZguZ116zmL1++vMinLv9Srev1JfSiRYsKgvb39wc74+HE1/HyLhaeVLU6H8+XL5/om7ocj9fas5qd+8jrt3nQULFiRaGJUAcQ3Jc9e/aI69iwjtd3q9qDH0ddlqAiEK8InS1HWa0T39aJ/+r2ymhr8oX+UQSkyv1HEVSuV5dDMdmymprJSz3OHuPYFwEbyanHmMRZTc351WtnzSI3wpRv3rx5OhU5579165ZO5c77rCJX8/E+k6VaB59jMmYDN9VZC6vQWRpXiZ2duajW66yh4X0eCPL3hBMTNHvYY5LmuXd1qZnqgIYHBryxZT17pON8rGZX28D1ct9UC362A2CnLmr/fw7ishR9BCShyzl0+UZIBKRRnHwGJAIJAAFJ6JLQE8BjLLvwowhICf1HEZTXSwR+PwJ/BKE7cZzyBEDcn+uDVLn//hcpvrdAEnp8v4Oy/RIBChxJ8QN+Vvq/h0/9hODIwlsc0wsx6uTwHlNnLYaZ46dRyhIKyUtC/1mP8J9bjiT0P/fey54nHAQSDKG7erpj2F8U3nLpCqT/bzhc3JnAHWH/aJswCrlu6f1dUrq9nTXqVS6JXBUqo8fYo99Vxq8eOEhCTzgv5O/qiST034W8rFci8PMQiPeEbvXmLhZvPwAnVzcYX96Eat0Ww92OwnOuX4WjFx/Cw9MJjcr/jV03TXDywB5s2LgdVrRu0tnVGffPHBRek5aMH4MX79/h5JlbtIbbCbcevcXLe1dw8d5r2Jnewahhw9G2RGFU7zMezlavMXbFblg7OMHinTEOn7oGeydnnDu2D5fvv/4thP+zCd32vSlFS1Nii/+cFI5nx3Yj6Be6bg4NDsAbGrStu/ry5zT5DytFEvofdsNldxMkAvGa0J0cbdEhuSHKpkiLI2YBuL5pMP6edg7VyxVDxgypaclFeoxokZbie3fEPaPpSJU9L/LnygxNlbH4eGoRSe6JUbdeQ3TqOAzL+/VAiuS5cPTAXLRefgZbejRA2eH7SX3vBB8/ZzSuXQaFClfGtfl10KVqYszecR8vTqxC0vRtYefmhdqFDNB44ik4/oa5+p9J6Ebjm6B8ux6ETSP4RnvkvbyjH4ndO9GMNCTT7nN401+UAt8jB8Vc7143KzIP2PCLKknYxUpCT9j3V/buz0Ag3hN62zSGWHjwArq3aoJHB8eg4oxzqPFvXlSu2wq5DNLCzPYONMV7wMPLFfb2jnA2v45EmhI4vXsuqvUZji6VCkOTawA8vB1w6uBeXLhnTCTujG096+PfqadwbUEjaOrOwOsjs6HJWRauNIhokTE55h5/gldnVyJp1o6wJdI3MX6ON+/M4pCE7oe+5QshSea8OPdeIWKTi2vF9EOXKYo/6MlrFK9TajIa1BEVhq8k/45+aFl/IkVGB64Z8dpZA7QYOBj/DN0Ezwe7MWwrx++mdapbhqJG647IWmqk2Le6vReFc6RDmqx58cadJXx/TOvbDnWqloBh8526esxvHsIBy2A4XZqHo08c4GpK9yRZKngHhuOl0UzsfW6NzhXyi7beNHOlwNE2aNNqPrjEfmVzY+qeuwjxtoZhkuJwDQ9FhzLZsOCSDZ31w7hNp4BgC9QsUBp3KP566wmb4W12C7ly0prYsq3h6qfEkpYpKgKS0OUTIRGI/wjEa0J3JHLtmT450qdJjE5dOyBzjiyoOP0M3F1ccW7ZcGQpORj+7o+1hO4m5tSdLS4LQv/o6QM3FztMIfLXaP6B0cyxKNe5Khxt7qDp3GPYO6glNOlK47mlObIk02D+xiXQZC8FN2cHDPkrNabtPo8BzSohRZp8sHJyQ58WVTF01+04QugR2FI2M/Zce4RrN2+jVNJMCEEwShFBDr5ogyNDSsMsIBhVBk6P8gTfv7QLxUtwoAV/JE5eQxD6xS0z0GP0XJw8uhpjt16E7YnJyNPvDJG+Lf7tsgBnjp/Crf3TsfS1D3ydLGD8wRoRHq+gSdkSDmdmoEStmshfpTkiQpWoTmIgcHgR1rwNhPOhJlh0zAze9h8wsmN5nH7vgcczGmHgVSfYOHlTTg+UbcO+rC2RNFMLsMb+7OYJKNywB3G8MZIbaPDaIxgHFw5C1qLsXSsI/4xcCZgfQ7IcnWHuH4481SYgIswXHv6h8LU8ieHbr8T/t/YX9OBPIPQrLWtj9Vu/X4CeLFIiEDcQiNeE7uRogw4pk6Hy6M3k19AHPYsnQ8VZZ+Dt74nGWTRE1ImQhEhM8zdJ6N4qoV+i40Vg6uENF5s3yKTJhzzVh8P0zj60at4K7br2g5mdI04uGIREmUth7VM7nJzQnQYBGhRsOgzOjvbonT0zUhX4F11bNcU/+VPD1NYNdQolQ9MpJ+MIoQPHe+QkUk2Pkg07oW+alIKcSxWsjACmxyuLcN/DHWW7DP/kKWxRqQIdC0aSlLXEuYZliuL5g+No0LMnRm86A5tTU5G9+TpBthWHTkW9XBoMmTUfS5974s3q+gLz235hVHc9GkQoaVKL4lHU90+PLcaU8+ZwPdUdIzcocZs/Urk7Xzrh7ZIW6HbdUxwL9bJEgd4z6Zc1DNJVgH8oy+heaFumAJpVqYM325th/317asoLpM7dVlxTdPxK4MMupC00XEj0eapP1fXR7tlhDNl2KW68eXGsFX8CoZ8eWgkbjT+dNrJ9fglzlpIr0p9pNhLH7q9szp+BQLwmdLYe9/Tzh5e3P4bXzYzhWw6g8vzzcHVygLdfoHBXyFugryep2+21ZOtMx3zgSPv2dL03/fZ0d6PzDnB2doGTdsmbo7MrBRnwhBMdd3BygS/5KvZxdxFlePj6wcfTgwzx3BFAvo8dHOzh7UvtcHeOM4Tudn44Biw/QU9xIOomS04UDVSpXBD/NukC95sLke6v/EiTUoNmPSLnnBc1rQ5NodJokicLEXN9IRF3qZYNOQuUwN0JhdBl/VkEvTooVOGzHnrh379To1LfRbB9ZIRFzzxxbXJplG/UG6sXToVByraiznUD2qFwuUIk80em91e2QJMtMZZO74G0VNba4yeEWr9QIZoioP0ld4jQwwLIDuJf/NNzIV1oL/IlmcrStRv+pt8cgsJ8Ww2ceO2Fjtk0SJo5H3JmTIlC/TfB4fQMpMzWFMuWL4ZhZcUvtq/lfWiKVsKwvXf+jDf7G3v5PYRuen4DNAbp8cQnUvvyjdV+Y/YQHNowhyKVtcHSnecQEgsCDva0wsKxA9GqfSdkpem55a8+Xac7ND9NxxgmhbWXOgT9xmbJ7BKBOIJAvCd0dUnY6/tXMGnyNJx5YflbDNN+9dK0L5X/ZaO4EDSij5UHmZiHUfAF/U9v9H1iUQSERP04h1M4yOizzuEhLO/HNoWT/+hPP/ihgUqpwSFfMH0PpRjR2tMREaHk1zq2dcacL5QGZT9YxI81IA5f/T2Efnb6ADG42235/yHC66P+E/Vx5C/+u+6C8VcRHdrsb2hSpEPdUqyx02DHy08J/dWheaQBagobPx6CyiQRiL8IJBhC5/lxJyenP47Mmei/ZuX+8dxaMjj7f0lR8fdl+JNbHltCjwgLxoVDW9C3RyXUazEMpx+bE2wRuH9hN/o3rI6aTceIaR1Ok/s2Q7vVJ5WdMBfs2bwNFr4ReHr+IHp1r4CGnYfCgcd1EWFY1b88hiy7ILKGBnlj7y4jOHkFw4uWoO7cdwih4RFC0+bm7YNDs7qifPfF8KJrnxyajn9rj6Bllko1F/dvxT2yxUCEG6qXLYYTj00pHrcHzC6vRqnWU8U0ULC3CwZS/PAGfcfBNZAmdB6w1ikJHlvY4dCm+WjYoCiGLdmj0wC8ubwNpUuXQr/+Q1Ce+gd4Y8fuY6JNXmbG+OjgD4tru7Fy4QZ4/H/GNn/yoyr7/gUEEhChk2r8NywZiwt1fo3Q5RsgEfgaArEldLfXN8gmpSIGDhuDRmTLYJAuH5yCPZAp7V/o3H8wqhdOD8OkNak6K+T+tzZqZkuFt1y55X4hIR97SlMfmnTo3HsICufNjEwVyAjTzwbJNEWRNz05gPKkSSK3D0jOzqCsg2B1aR00qbLCX9XkhFmRmt8QORInwr6njmiRIQm610+FRU+VZZFN02nQ7ZQzzHa3xX+NqpJBbBsgxEbUzdtZGy+0yJII5RJp0KBUWjLsHIDueQyRNFVBvNveCBlylUH//r1F3hob7sDtCq1uod+GJZuh84AumLLjOg6t6oeUyRNhOq2KyZa+IK6T0H92UiGRz5IGCDJJBH4XApLQE8AgQBL673p9Ek69sSV07vHHfWORWpMKu48ZEYkZ4oGendnr86tgQFM8nqQQuru1pyDvlzxDY39SEN5550jM3m4g4kxRQhhsvji7Rpy/RsJ1oKeZIPS7NsGwvcGEng20ohHDBrRH/+k7EPDyAAxphcOyM6/hbXlPXLfwMS1vpNSC7CmGnHagXxGYXIFWsBRqi/O7RmHTlXNIR/lOmXugbdFCyFmoOIY3yo58PZbh8ZEJMEyWAu5kzKmm7bVpAFBuKl7vmIYi/1RAxbJlkCSxBmM3XKVFIK5YPGscBo5ZAndaScHJ8fUD3Lh0HXJVZMJ5J+JjTyShS0KPj8+tbPNPRuBbCP1IzyRiNQOv/NBo0uCp3kqwj1c3CgMzT1KHB7xgVXYaIaGbX5ynELpjZMPXNae58MyltXYd7wXh3iZ9vcXtfTCk31dJ3BWEnjgNTj6yhNPpKaKME+QRMLlhIiw9w3PovqhLx5a/9kWo61v8m1SDzoetRSU3J5cV+TMXLEOjhOekBdDgoInS2LBgU6Sg/QPvQnF500BB6N56xiIbG1DfKszVNTbE2xYZ06ZAm3lGeL1rDpWbAs/cgzChXmn0PGMLtw9PcOP6XVqJ8ZNvjCxOIvANCMQ7Qjc1NSVrdGe56WHgSEFpkidP/g23XWaVCERF4FsInaVf34AQnJzY64uE7vN8h6KuTpoE/9AAYPDwCqg6TPED4HllDC0RHQRN1rLahrC3Pw0SJ82MunWLY9WGeeizch/srq1D4lRpUITO2VHOCc0KIEmqTGIwcfyJkyD0+vQ7ReoUqFalEBq3HIZMeQsL48crExVCn3jSEeGej/QIPQj182YX57LlI2k9S1pB6Dwnr6ZV/7GEPke3v7iiBrMH1kXDWTtgema+uDZ1utTovuSoyHNpRg1xzFqq3OWr9RsRiFeEvn//ftSpUwe1a9eWmx4G//33H4YOHYpq1aqhRo0acpMYfPMzsH37dvQkXwPflAIdMWP2MjjqGYJ5WL7AkqXLFL/95M1v1qzZuPyG5rdpHnvsmAn46KWuagjAqknjcfC2mbZKd6yfM4vm2K0REuCA6RMn4aM7WZ1HBOHgvFmYc+KJyBfu44g5s2djwaGb2hULwTg0bw4WHnkgzm+bNwZHXynz6TYPjDBjwT6RLyLYGSupfFN3bmw4bi5biEWLF2Oxdlu6bHmUZXCvz87HnGOPdHB4WD7AxCnTYOuttYQnQz79ZHn3EBbMnwdfKaF/0yMkM/9cBOIVoW/dulVYdAfS8iO5RWLgT2vhpYT+c1+MP620b5PQ/zR0ZH8lAvEDgXhH6PzhiQuW5XGpDdIoLn68bHG5lZLQ4/LdkW2TCMQOAUno0igudk+KzJWgEZCEnqBvr+zcH4LAH0jodjC3tktQUr6U0P+Qt/UXdlMS+i8EVxYtEfg/IZAgCN3ZxYU8xNnD0enLvtQdKcypk5MleszfBw9nxyikzpbzcUmN/i1tkYT+f3pbEnA1ktAT8M2VXftjEIj3hM4kPqBLJ1w3dcbR1TNhY6sGYYnqOc7RwRqn923FyeMr0X35EbjrEbojxeMeOWkhnF3jJ6lLQv9j3tdf1lFJ6L8MWlmwROD/hkA8J3RHeNPSmMIZaf1qosQYuvo4/PwokhpHTCNp3MPDQ0jdPrSGZky7f5EyTVJoyOVj7zVn4eXpCS8PDqnqAItTk2kNaVG89/aGp6cXvL28lKhrtL7b08tb7Pt5eZAGwEXse/Fvnnun814+/nCi367kL9rV2UmU50K/vaksN+0AwYnis3vTdZ5UH0d4c3B0pjK4HC/4+rjDycWd2uIqIr59i2Su5pWE/n97XxJsRZLQE+ytlR37gxCI14Ru+fYpiubMjGTJ06LG8EVweHII1atWxbozN2gN6lHkKlIOdvbW6FCvDPpNWQ0vmxfIlyETtt8yxfg21ZC/VF0iWEe0q1oYtYYug8mVbcjIpJ8iM8av3Yd7ZzYib9Z0qFizIXI1HIR9c1ogNXmiSp61IHace4mnJzajGNW35LIJhrWtij7LL8Du/X3UKZFbOJnIX74VTCkk6+ja5WjAoUHGvKVw6aMDDvRtifSFSuC/2tVQtMYY3FvTBSkrDYeLl+tPI3T72zvQsnkL9Bwb6e3q/pYlaNq0Mc7YqWuB7dF90CiKhuGPfp3bomnXBSKymsX9g2jXugVa9ZpHYSi0yecpFp16RG40l6J1y2bou/So8PAV4miCFo0bouvogyLcKic/i49Y8ICdfpDHz0eH0KdXL7HGefSRZz/91fqwbinO2vLa4FAs798XPaieXr37YNaS45GR1SL8sLJHJ7Ro2RKbLr0SbXh6chMaN2mCqftiDqdqfGUHbjsqi4qNlk5Fk+at8FDPbelP78hvLlAS+m++AbJ6icBPQCBeE7rVh9c4fvsFAhweCN/SV1/dRC5DA7RdYATHB4egSZkNdg42aFAiLTLV7IaTCxugYxUNuqw4iRVj20CTPCfemhxGqdzZkalWX5g/u4rHRLgme4fDoEp/rOtTBjlz50aeUuVx4t4bnN25ClaunljfLBuqD12I25unklvKZDjywhqtquZD8zlXcXjsPxQ8oiJM7JzRqaQGzVffxbplu+ESEIjh9XOh4fLraJosFbJlz4h/KrfAHXt3PN7RB0UbTIOL588j9GtrFuKNiw/WjWqBrmfs4HZ2Oop2n0MaDG84aF11Whr1QIcF73F5fjWsufUBT1a1R7OxR/F831yYu3jidM9/0XHaLfGY3R5dCJffBuLwzg1w9fFCn6KJcd0yCH3r5sMtp0Ds6JoXh59YI8z2CUqmNESjTUpoS5NNPVCRMDB9+wbvHTx1j2x4iA9sPcnPZ0QAHl6/hHPnLyJEhMwKx/und3Hq9BmKYsVOwsPx4t5NnDl3Ad7RQq06XV+PlDRw2viEhx3h+GDyAsbGr3Fj3TD81WQ5wsnntpmNO/ne/oDB04/Bm+LZ506rIecgVkiZqyhdE4ESFDzkpgvgYm0Of23ErjcX1oroW+tNgxD8ZCmq9V6IYJeHyGSYUuerO4JCu96/dIUciVBYWm9HvHP8NCznT3g//29FSEL/v0EtK5II/DIE4jWhqypnT8eHyEK+lR+7+mBL6+TosGgvbG7vpaAO2WFjZ493hyYjacaM+LvcKGxuYoDOK48gyIqiRmmS479EBnhwcBRSVu4OH3dlDt39+XZo0tSCm7+3cDFr1DsPqg+apTOk29M9O2oNW4BHO5eQCj8Vbr2xQQsi9KazLuHJvsHQGNaFqaMnFg5thLYbHyvqeWd3zBtSF3m6GMHFzZVU9J4Y2ig/um5/Dntba5hbWX+XdP618KkHFvbB9Efu6JMtDRr1ao8qbQbCQhvfsn6FAhQTC1hYOwV8gkm+DniK/HXaiYft9dVDyJEhMR4R2SHclzQMSRBJWeGYmVKDey5BaNeptcgf8nEv+hpdF7/Nb25B2U2vxW/jLYPIFWdrzF2zn/xcR0Yj97e7gYnHngOuD7Fg53U8PbUYZeZcpiscMWHIYhqIWSA9aUNMXKwwd+Y2vHt4HJrCEz95EVbULIq5gtAjU+eKhWHDdP1mGzT9tokT4YG+MFo9G0W7LKE9PzSigcDcveeQMUd70a/1vYvgCbvtND+DAq2n4N3lTVjzMRT2B1pj8qanoow1tQzg4q94CPMwPkbPjwEmXXqP4OsT0WDyzU/aFp8OSEKPT3dLtlUiEDMC8Z/QaT66WrmWqFWhMp67e2Nty+RIkiIVfWxTodewfjC2tYPpgUlCBb7zow9WMKGvOIIASyZ0DapMvwjTvaORskp0Qq8NFx8PWL8+jp6da6Fa/5k6Qt/eLTtqDicJfdMEGKSsio6dOiMbRWJqOvsKjk+qg9KTjsKDCHs+EfgnhN55pyDuY0OroF6VGii8/jHuLmtEISl7w8NbmdP/1u1zc+gPp/dA4Zqscg9BhpSJ8cKd4je/u4JkubrQsVDUbTNNPBU6Qg95haJ12opjfm72eLRvJDqM3Ql/swcoM/O27gk6Pq0yhu9W9tt0pvCUXJrVYfQ3uiZ+v7+6HmU3KoQeEuAHL29fWNzYiJpLzuvKCHC6gTkXP+j2Q3zdkXIenbc6in/r9kOHTDnovnXGhiuWSp4wfyRLWVMXa1u9cE61wpj3OJLQzYyao+mQg5887RHhobC1tULBQvnw7PVVFC7XCbbvXqNqtuzYbqZG8I7AmMK5YZgqHVKlSIpUmbLB2ccXC8d3R7vOfUkbk5yia0W6/HR6fxLTL70D7s9Do6n34/U3RhJ6vL59nz7vYWEI9PFBSEjkIDph9VD2JiYE4j+hEwHe2rUeizfvhauTIx4eWYSVu87B2fI5Js6YBxt7e1g9v4CZ6/fAw9UJd/fOx7E7L+BuZ4ppMxbinb0TrF5cwOIdR8moTVnK5kQENnn1Lji7OMPezgKL58zA5VdmOqI1vrQDu05fg+m9i5g7fw75lp6DeXNnY9/Nt3h+wQibLj8jyd4F107twoEHH5TraOBx9dROrDn7RKnjw0OMnTAf72yc8eHOMUzbeIzqU4zqvnX7lNAjcHnGIJTvNJpc5HrBkaYJNtRPi+XnzOF0ZTtSVp2D4OdzMMNIIeWTE0qT/2tnmO8bivpD1uPmzjXwDw7B+akd0XL8STzc1hePPBWH3Sv7kr3B+N3wdHMRwSy61ciPd8SHF8aUx/a7H4l4gVdnlqDcqsdiTv3C5avwDAzCvT1DMWi34m+bU4DTFeSsWQoHrpJqPswVjev1QIGJZ0jFvQrJMpSjKYDHSJdKA6OHHJJDS+ipasJf7ykOCQzB5MoFMOeOE0jzTSmIbB6qQAmkSYMMm6uYsv8uTeQ/w7Z7ZggNcEJZipd97+kpZCrUVeQZXCUr1j31we3dC2Cl55P80ZlV2MJiPnXILygUZleNkKxaP21kMKV8L6vzqNqkHQ3MyqBaQ7JFCHDGwOWRgxa9psb5n5LQf+0tmlU0E3LnyYM8efIif4uBMVQWgj6F8qBw0aIo+pUtf44smH/P85MyHJ6dQ892TZAnEws0Svx3deuwTvGFL1PCRiBBEPq3EmBCy/8poYdjR9+uqF2nNgVsqYqGbefTUxyOPo3L4J/GPUX86cE0z31f+00ID/RBg5qVULLaCHHuyvpRqFi+HMp3nCme/hapsmjnt4FZPWuhXo3qqF69OhaffYRwm8coXbIEqrdZK8gu8MMZlK1YE43q1MQ5UxdcWjYF5aisWvWbIUAvnkVEaDC6NGuEM28pkEaEP8bXq45DL5lB3TCgamVcd4jA+SltcNGMAmRzCg9Ci/Y0ENF7H21Pj0KNeg1QtVJj2NJx93trsPXGi8gczg8wZAOp8f2sULlCOZQrVwHrLivBQC6uG4eyZcti6DaKb03p7PqpsNAr3OzuYZyieNycepUpjkZdBn3yJQhwfokeK87C/PR89Ju6iGYm7NBk1vF4+cWQhP6TbxsJxqHBwfD2cMaptUOjkGvmmt1jqCwYPWmKKToRf25/xnUlAI2azszt88VrO9IAW6aEj4Ak9O+QiOPagOBbl62FB3khQ+FI6/cvPeah5qfwd+09Cf9N+MN7KAn95z4AvnbGMKSVLTERcqbPEHrbFEmQ2NAQhp9sBp+UM/26qoeiuO8L239yPlmKFEiVOjVSpUqJpCSpd5CE/nNvcBwtTRL6H0jocfRZlM36jQhIQv+54Ps7v0ZSsquJPaFH0CoMV+E7I/oWanPxk3KOs0pKJD+UiqZeH77lIgJDtOqwiHD4eTrD1VdvPunndlWWFocQkIQuCT0OPY6yKb8LAUnoPxf5QE8bdGjXBp06dUKHFrWjEHLMEvrn65/TqUaU64v2mKnLbLm3RZRzlQYd+6aO+Js/wuGjx3Hq1Kko29GD+/HKXjEWPXPiBE6fjnr+4KHz4EUhMsUtBCShS0KPW0+kbM1vQUAS+i+EPdTi+wnd+SxSRJPAj+qMPcIxPE+OyLLJJ8aas/swoE0dJONraEluyRpNsfbIzSjGnPo9Nd1I/jiila/RJBLH2h5V1AAlc31qZKfRZIP1L4RMFv19CEhCl4T+fU+OvCpBISAJ/RfezoB330foERFoXihnlGtTjT8U2dDwEOTJkSFWhnQ12i6LdQdfbKwuylygtaQvnScdavb71AdErAuUGf9vCMQrQj927Bhu3ryJ27dvy00Pg1u3buHAgQO4d+8e7t69KzeJwXc9A+yeV6ZfgMB3EnqoxeEoZJ0oSTJYePA6FCWFhwTirwzJY0XoTNAnHGPXt5LZNchRg31VKIkJvXqvMbG7WOb6rQjEK0LfunUrpkyZgvnz58tND4OFCxciffr02LZtG9atWyc3icE3PwPnz58X/vZl+gUIfBeh+6Ic+UzQV4eXHXYySuMimNAzptDlMUieGq/tFCdLvk7GaJg76vXFxl75aufeLFfm+5dfso9C6JkKVsP2zSuwcddB2PpHDiq+WqDM8H9FIN4ROqsG49qysd/dnm9dtvZ/fcJkZfECAaly/4W36TsI/fWppVEl79SFIgMlaZvK8QT0CT1JyrRw13P24PxgdZQycrTaERmwKIbuhvs5IGkSAyQtNCLK2VK5aflb2vTIlCFSvb/mArltlinOISAJXc6hx7mHUjbo/4+AJPRfiPk3E7oH/o1mqFa794VPG0hz6Pn/yhgpoadIC2c/JUIgJx/LK2INuirl52i984uEfndLP5H3yFOHKHWpURTFQS8TZElOZWau/wsBk0V/LwKS0CWhf++zI69LQAhIQv+FNzPgfVRPcXVi8hQXWf+Rab2jSucZq35miVgE1taLajS37KFugTqebmkXpZyco7/gltj/o5I3WT+K/PDltLN9McpbAkqAZCW92LMApXtPF7/vrhyOF+Yf0KpcQWTMkgN2fsFweXQE+f6pI/rxfu8IVO8TO8dWv/CuJMiiJaF/A6E7OjnDgyKl/W4Ve/T6pco9Qb6b/9dOSUL/uXAH0Tr0Lh07oEOH9ujSpnEUYk2SKTe6detM5zqgWdtJ0cjaGZmjuYAds1UJehRjsj8BA31pPunf2HriAQ5vn0chpfXn0FPjnMvnA7XsndOZ2pgCF9TYyl+AY2LNQpS3OMVFjEw3zuxCuWp/iVgLJ1tmwebn1jA2d6N4Bw9ReMJJ+Dw5i9JlC8AkyBfVRh5Hs1qto1z/c9H/c0tLMITu6GCLR3duw8H+24ObxIqgKbjKnZPr0KjjAFjZK0FcvnWzd3SCm6MNXr15J8Kyfuv1n8svCf3PfYF/Vs8lof8sJJVyvuQpLuq673/hqVf1iq5FokrnqfJEOR9TKye2LPFVS/cKWumZr781u4nI3+2oupL8I/LSfraSjREejfNDTfdj0MTFePnRjgI9BeD9g93ISCr3VHX6fdKUndPbUQxHiovQJQu2vFWc0oT42+Of2SfE78NrZ2PX9v545xGKZjWbgyMzy/RzEUgQhO7o6AjLWxvpIU2Cg6+t4UT7fEwlwBh/cx4tKUc/r7/PZYh9Jxdc3jQNadJlhLmtUn7UfFGPqWUrkdaUtji/v0VtTIQKFEe7ycS1cKfoalHqjqE9sSH9n0no4SHBcHd3h7e/fhiUn/DQkQvKUCUk2jelcAoDGR79K/NNJcjMsUFAEnpsUIp9Hj9y/Wr4GdevUQm9nI6wI0LMkNkgqmX6mttvY1Xp1Bb5kCyx4hBGf0ucLCVaTloXpYy7SxVC73FEIfS1dSuK/X2Wn0rwQS83fFJmEgMDXLaMnKvnMsI8rdCytCHW7TuEwemT48D7SEIvPUsh9EPLuiNVlkaC9JsQoUedqY9VN2WmryAQ7wndhaIZzejdBfVatsOWE3dh72CGOWP7o17Ttjh6j8KF0n7xvOVx/qkZ7C3foUr6NHhlbY221apj29XXJNHbY/64Ybj+wgqOtpaY0rsDKlaqh3XH78GNwrHavrmCdi2aYtL6w3B9fw8GSVLh9sNbmDy4Mxq17ob7li5w87DGxF6kPmvZEtVz54bl2zuYt/YAPB0/YvOWw1g3eQSuPH0HO2tzmLx+iRE1CmL01iv4eOcAMuVtIkK8Wt4/jnFz1sPT2Roz+vfFqlMP4eIUOy3AzyT00TWVD0KSpMnxwevLsZQ/nluFlKnTIUOGvzD/lKl41I6N6YY0GdIh29+d4aM3Gfd2W1to+u8Sed5fXoqMGTPT4Kgc3rtTeFLb10ibPgMyZ86MfDV6wEfvoW1TPjP6nreluOquyJsrBzJnSk+R3WZqc3ihWom8SJ8mNQZtj4xHfnbZWBSqUE2RNrweImnyVMiSJYvYphyMjOv+M78Omyc2QeEqM0TIWN8H25CMYqqrddaetC1KVTP6lkf6DJmRIVM+7HymKC73jehIGKRHZmrjIyffGJrmh1bJEiPnkMhobr5WL5E+Y1acfef+w12RhP7DEEYpICI8FC4uLl/dnJ09dYZqEaEBIuyy/nVhsR3M0oDZ290Fd66dwaYN67BqzXIcOHMdbt5+nxjChQR4wYk0hL7BygDbw90Nzm7aqIbRYYgIE+W+fn4fFy5ewoOnbyicsF7YRG3+iBB/ONGUpJOTk9iCw7TfDmqXu5+yzC3I34d8yiuCgpent3hXZPq5CMRrQmc1+6BCKVC8aTcsn9KfDDoyw/L9WXTpORLzRnVG4oy54WR1GUX/yoyKbfvB6skF5C1VDPtvnkXJ5IbIVrC4IPS8aTTYSnG3J1RNjXzNh2Hn1mXISaQ259pbVNdkgmHpFihCZUxZOA5JUqTCzeXt0H7oDEzp2xzJ0haDUee8ggSr/dcWRqfv4uEKZQRcsnhZzDj2Al1SGWLDpRdwc/eAj4sZxfvODU3uXjgwbxCy58uO2VdN8W7/GGioHn9HUxgkSoSWSw/AXRuf/WtS+ucI/d29Uxg7bjIOXHouXmob43tYvXg2Nh67q33JgzF7+gQ4kqWK6a0zeOnig2n1q6DriPlITO039/XD1HGT4CCIOYyc15yOEo/88qJxFHv+MOYNVdxHvjw7n/5SX6/fREXab7z6ovZpDUYzWlO76znFNvd4pkgHC3ajeeb0SNV2CnxNb2LKKiOcPH0Q+ZJp0GbhLe11H2GoyS5G8gGe1pi+ajOObV+AJKwuPGuP5eXTQ1OqNY6v4fCU6fEhIAJNK+UX5WfJX1QhdOfztJ8KW89dwvGjB/HMItKUJyLQAXtO8EAgGNeO7cTChYvw3lOJuO5o/hKbVq7CknU74UX9D/JxhNGmFZizfBv0fHuIvI1LpRV1ZirYT3ykgh1eYet2I+zetQFJDQ1Qa/J2BNmZYPPOfQL3keMn49jpM5jYpiRd9x+FnD0krt/8xAI9U9KAquYsGhVYY93WPVDkHGBpL8V7V9LmO7VHfFA8hyEdS4xjbyKVl6Helmg9daOYl/VxsaL+3YrVh1MS+s/9sMrSJAK/A4F4TehO9pbIniIZEqUtBZtAF1SlD95REzu4uzrBx/oBfexSw4TU40cHlkOZDsNwe3Mv8VHcef0NHmwcirR/l6QRsSvyEdnsfuqEAeU1mD1hAJInK4cm9cugyoRD2NjqP0ycNRrJ0hXE47v7oUmaEhZOnnClEe6r8+tgkDIdji8fi7X7DmNo8/JIV2ISjVBtcPbsBdjY2uDJgyfold4Q2+5+wJbpvZA6R2WSSK9CU7gTgsN9MTSfBlMvmMLyxHQkLdMK3m5OcPfygqtL7OfYYyL08Hc7Bcm1bt8GWanP2166o33dYmjeqiVyUVjHfRSH3HhNE2TOVQwFt1hiX8vUmLFXiZlseWUmNI03IuThPPxVpBDytmd3kwHIpCmEmGTBN6fmCVxfHJwi6jQmNlnbiIip1mxRXrDjc/IrnQR2vqHwebqN8qQDy/NXR9JAKFtbJY+/JaaO7k4DCQNst1BozGJHO6TvMi/qexHyGlmorsmXrJE6ZTL020jONryMRf3nbZVwEYuGN4gkdCcmdA3K1WqAniNnwc4zciohzOM+NMVGIuDVNgonWxmtmrBTjQLwojJG/5MOTVt3RM3i2ZC33TS8PLECVeo3RfXC6VFu2KduNBd2K0SE3j8Keb4/MU3Ufds6CO+PLYEmbU6d5LJ72jhUyUuDvg5baaLxA7KlogFBoXqoSmra8YceI9TYiK7NAZabPu6ejDT/DcXuibWQtN1eheBblUezYWtE+SfeRRL60/VN0bJsIqw9bw6H2zuQLFNXoeL8WpKE/jWE5HmJQNxHIF4TuqOjHQamM8SaE0/g6W6OGvRxO05z6Dx/7WlxR0hmprb2ODmmPJLSvE7hWrPQJ1kiLD35CA+3DUfyrHkxe0BDpKWP6KQTr7FqQBVUrEhElLgkujQojfIjj4q575ureqFcl/F4eGINEhGhm9EgwcHBES/PrRWEbm7rAC/7ByhI9afN2RX31zSnunPi4NbxyDPkCHpm5OAG5WDhbiM+wPM3kdOI3F3gFeyLsWU06DJ7FSplJSmvaCN4kfq+tEEi9Fx5Cm4/IKE/X1NK1NVi+G08X9YCuaZHrmNdXYcGEdcs8MaoPVIUqC2e0uPdU2H2wafi99vTRMxN14jf52Z2Q76xZ+lXELIkKyHITj/5f6TBCdWTdeFdIPAlDJMkIQ2DorZP3kUp49Wx+UiSd6QIEOH/bA+dS4nXxDIXh+aBpkBHkSfY8zWmDG4sNAMtVz+gI6HolDElFh4zjqwuPBg1/yHtRoX2dCxQEHr3NUToQcqyoL3vFGX93OGNIwnd/TG27j6P83sWiTzdtlzVlRfmTYO+ihN0++EhHkhHeUxJIj8+qz14/tHXjgYEGZtFtsH2KDQFI91iqifmdSoaldBDbJCRJeqBR0WW0LBQBPir8jbNJ66dgHSpklKbqiPA47nQCNVs3QqpSUORposyYPD390dEhB9ykhHSgJUnMLhubiQpMwjvL8wSfTl144L4O3LJTrj4ahWYEaHolS0V1t+1gfNDIyT7q89nA3Po30dJ6NEebLkrEYiHCMRrQncii/FOyZNg0+UXROhmqEwft5NvtIRuzgZoqfGWCP30hHLQ/F0H7p4OaEVkufzUUzzaMQya5Nlx/eVT5KAP5uILb3FoaTckMkiMB6a26Nu0OCpNOU4qeRvk1QuAkDSFSugOeHNtGwzImYMgdNt7yEb191lzHfa2H7F/13ZcuPVQGMQdm9mS2vIvTNx98PrGJiQhw5fuc4/By9sDEyto8HezaTg1n5aNkITm4fJRqNy7rTr8/YRO817DSxBZ5mwqHsm7CxujzExV/Q2srKUQ+ot17ZC1gkJWZ3uljiT0MyxZGuCEmQ9uLRuCXP33YeUomgOn/r3Um971e31QHCtSZzWCQwIQRvOGoSHhCHB5I46POqJI/POapsbQC9pFLtaXxDmjdxGYXZzW0NYcDttnF/HQmsnYGxXJj3SdzisR7u+GlDQwIMWCSOHBPsidjVTsmnq0/jUcwUF+6JEmJQ085iDk2RZxr594KbLo/JFM6MWFyt3h4iZcMvNEqKe5qLen3hIgJnRe2jNxkzKQOTPpH5HHjH6PKKm1JUhGvrLztIt8tS0PkFZBb197Zl6nv4nQB+nIc9eSgdCkyAhnf6VNtnf2I0Pe4nQ+BGt2nhHHjs5uLeq7vKIT/S0GNzp2b24V+t0CPk73qJ7i8IvwR/euHdCv7yDUKKKh57MojhjNRrsu3dCnq3J9xRrV8MxR1TyEY3yutFh04ia61imBlGmzk5rfGgU0f+F5TFPz2vZLQo+HX2/ZZIlANATiNaGz9Ozq4QlnIk17Wq7m7ulJRnCKIZk9SdCenh7it5OLOzw93EQeN8rjTMZujmR84unhIaR5N7Lq5jKcXFzpGk9xzNXNHW5khc7Xu1M+Pi42+h1pPe+kq4Pr4/pdqGxhGU+GITorelLre3i6wZHm6x1o6Rpbkbs6O1F77OHi7klTBM7UHspDa9ztWbtA5bhq6/7a/Dmfj65yjwgLRpFCxVCqhRJQ4e7i/1B6WiShr21MGolL5ni8nAcakVax8/c9EfmdTI8hebpk2HLPEteWj4RBuszYa7QYuSjvAfNIBe7oCuxgIvL63S+tUJskTD5Wq88MmnsmqTH0A+2nj+KEYnOvRrrr9t55TxKnojoWc8S5yuG6XQhJl+tpWiLSBaX7h2NCelfzZc/TA54fjyIze62i4zk7TBXq7prl/o7SpvdXVkbu56uLj66RUZzDA+2QNXdO9JnIauxwjPw3CzS56oN5b1X/ishHxHhve3doMulJ6B9JFf5XmyivUcNSPNDQtq0sTzN8EG3N1W+lziDJ7DSr3HMhNCIQxbPw3DeHt0yKAUvOkYbBClUKZdKVMeHkC0S8JU0A2Q/oGwjuHlQCyRoZRdYd4iiuOfbBWa89YRiSMwv+qt4WK8b0Ra4s6RAcaiJCcF71/Pz3TxK65AaJQPxHIN4TemwIL6Hn+ZlW7j/7kX6zbQIyFK/1zcUur5AMw4+/+ebr5AXfh4Ak9O/DTV4lEYhLCEhC/w4HMXFtgBCXCT0uPeyyLVJCl8+ARCAhIyAJXRJ6Qn6+Zd9iiYCU0GMJlMwmEYjDCEhCl4Qehx9P2bT/FwKS0P9fSMt6JAK/DgFJ6JLQf93TJUuONwh8K6G7ekcaF3In/Z5uwWNbW8xfSevqo6WIMF9atvf9fsHCAnxp+d6vg9LB2hZBzg/goV2R8Otq+raSeWXHa1te+/DtyfKdiTDu/J7kb/scpg6B8HO2AflqkikeIRCvCH379u0wNjaGjY2N3PQwsLKyQm5yOcspKChIbhKDb34GXr9+jZ49e3710+XnYY/3zj6YcvoeXj1/Hpnf4hiq1KsFjWFOmEbzZBPgeAVPPn4fMXEFZ6Ykg4/WTelXG/iNGRZ0qoa6nXuQ74lksPWJjQueb6zgO7PbPiLvgYmTwPE7GLV7ywqo3WnFZ2sODw5EYPCn7lv5AgsjwqL+LJgemYJU5buJVRS2flGLCgoI+CSIy3d2U172kxGIV4S+Y8eOKEuS9JdM/cm/kyVLhoIFC0ps9Ja1/cnPw/f0fdmyZbEidP7+3FlUWDxrVxyjim89h0zEKaNZeEnCu4/xYZQq1VhIie8PTcEbj2DsXTAcDRvWQrpc3TCr+3+ijO23PiLU3w6Z0yTDa6cQOD47hwW32fNdAPqWL4RUeUvjupk3IhxvIW+mNChStQk56qUU4YbWDWqgVpPqyPvvGDg/PYWWjRogd75KeOttjy6NG6Fp+WQYs/ogSmfIBUtyFuRzaw3W3rDBy5Nb8J4K8TE+goIFqoMX/T03GoU3rv7wfHUWJfKUQWCYN0YfeARL8mPwV9m64rN7dU5PmPgEYVHHKsiUrystdLTD2t1XEOxsirZViyAR9ee0sdbfQqgf2mVMjZ2PbOH04ADGGCmDn0Dyj1Esowa5W42jJYzA5XWTBA6tFu4T5zsVyYG1l0wQ7voBFUezUyJnrFh9QJwLC7BBluq9ULVcM7zwDkc4+X6f2LGmuH7WgXuESRgOLxqD2lVqo2i9tvB6uh9PXBRnRvNH98HgnhVxzzkUqxuUwsrnrtg+pROSJU2M9mO3izxvzm1Gw//IW2KqOji4faoot/uCg8LToNmDw8hP+5W6DELKrAM/8Q//k3lJFvedCMQrQt+6dSvMzMzgQWvB5RaJgZubG5InT/6dj4C8TCJAH/M3b2JF6L7O77Hn/hsM2HMaRw4rXvD00/mN4/CCOMT71VEUzZkWZ0kwf7K8AqlwX6B83n9Qrkwj+EYE4605E18g6v71L6mVnDChewVM2XYH9hfW4Z+95thUJg32X32Ah/cuoWjqfAjytoMbkemthePR+Jgd9tZJjpz58iNv4f/w0TcIbYm0K/1dAEuv2uH4tKZo2Ko+Bm64gHCqt1Ojcljy3AseJzpg5LYPOLdhNJ6Qy361jadcuY3l8djGHxs7N0DTcavhFeCEvHPOwPruOdQoUwJvaMbgSKM0uGJ8m4guBUaPaoo3DsYYNH0jwslh1TMTcmYcEYBiOaoLnwshYYGYN7MP2i07DIfTM/FXL/JoSKngPx3w7JUx7lzeggJrnsH6vTm5GgK2jWqM54Tbplk9UakXeS90e45ElbfQGVu07z1JXDsxhwZ3rhzB5RsnUKMUeXgMD8Lzl8/gT22b2b4qjH1dkP/fiij1z3+wIwHc8so8nLPwhv+1GYKclx0/jarrHqJaqUYiJsPTa0/hHBiBg2P+wcg9j8iHRFZUzZMfNyiOuaO9LQ1YQjGInGC9dA3Ds7O70HfkDCzfsgGZR2yUr0wcRSDeETp/eOLasrHf3R65bC2Ovl3xqFmxJXS1SwExRNzic+c2jsIJC8VB8OLuDXHAliT0rXXwRHgCJJe3S3phu7k6nx6A5jWI0ClFmGzC0LXn4XpjB0rsscC+VulIUkyLkk07oV+mDBT3QKnZ/MR81D9ow1eg5vTtCLy+Gn8N1zpNcruJkq37a5sYjMzlFMdEry9uxsw7jgi52gsjt37AeWrjMSItTpv6NcEF0ijcmFUEj6x8cWZAC1Rec4WkYXfknqt49Nsxow8eUcCw813S4rA52w44I1vyAvTXCv1nbESQseIxceELP9Sq0IQ8RV4jH/qt6bwTRi7dD7tzs5CtzWZRVukqdVAsZ0ZU6zQYmrqrtG0FrhtNxmMBkQv69JlI0YHuIFHptbRvi9Zth4l8q6poMK51NpTu0h+DylehUYMXsqXWoO2Sozi5ZgzeajE63bEuOl/zIEJfgE0PPtCVITh35izWd88LZ1Lh16Y2MqG3Zo1Wrt4IezQb7ec9EnWEvduLnpvY1bPYw4J/NHjiDfSpUx037l7Av63GIG2VKbp2yx9xCwFJ6NIoLm49kbI1vwWBbyX0zzXy6qbBqFPOEE9OKKrkzBUrQGNgiOdOfni0oLc4dlU7nd6leRuUK1mTnNbbIA3FU0hHIXU5kl7X/VZwOErku+UKVeOPmsnJ251K6Mfno+5+IvSAl8hdpgV2zG+D3H2ZeAOQz1CD5sOm0+9g1EpkAMPyY4kfr4g6M9asJbz3TT9rgetbhqDOv4lh6hGOPcM7QJPEECmSVUbB+eexs1sDDN+wD1tXL0DRqWfxfk1Put4QWTNlFOXctgqG/ctDMEhWCns2z0LXWdvgfGcdkqatjF27duOvbLVFQJ3bS+pi1tIZ6LP8OAJfKIQ/haYSalGwn7MUhtzbguIINIyUdK9vn4C7RJxM4OzXP1W6XBSoJzEcgm1F8Khydafh1UaO4pgcr/290bJYRVJwOCAlBfAxOnoMXRr9h7cklbue7EHHNJj7NAA2V+aiVlENjnwIQKCLKcpSP9bs3IncWWuIKH7/GSTDgX17sHxgHnRb9wLelk+RhNwtzz6mhBh+dHACKlBZptSusa2KIXm+0nC7Q3EEyhGuMsVJBBIuodt8xJ17xn+ENC8l9Dj5bsWrRv0sQg8PDYYruS6OoCAxnp6KpB7k74swMlOPCAuBjx8FnNEiE+LnBU9fknjpnJ8vWcKT8/2QoEAE0eQyuy8OCmHDrQgE0Dk18fEA7Xn+ePHmH6QYs/n7+dK1CvMH+3rCL5CV2RQnnNw+89GQQD9xnstw9VQs50MDAxFA9YRQG4NCw8S+t1ouGY6pbeYyuY0c5jssJAg+9JvrDgwOQUQ49ctH2ffxUfrH/bd8byHaShmoDa6khueogn6iDI6X7ivap6TQkGBxnPvr5xdA58PgS3XwIT8vb9HG8NAgBARzXyMQSIF7uG/cJqVepW2gen18/ITRGtfh6ukjfvN9UdustjFA20++nvvOgHCdfB84hVM/BU7cProvfqJOyhOgxDeXKe4hkCAJ3cnZDWG215Gx6NJYE7qjixv8fclPu5OL8OseXY3OfuA96APlSH9dKa65kwP5Zf+CdO/u7YlHt67j7vV9WHPVhMpUfMz/ik0Setx7seJbi34Woce3fsv2SgQSEgIJjtAdzZ9gRN8+yJUjOfJ02BxrAr2zYSQyFm6IjYPa4+zzD7rAKioBe1o/RPlihXDazBmtarSHsZ3r58umQcGA6iWRKXsKoWqbfPqFJPSE9NYkwL5IQk+AN1V26Y9DIJ4TOq2JfWMCSxs72JiZ4oHxOxgNyoNUZeqhapFUKD3xMGytLfD65Us8pzWz5tZ2goTN3r2FmaUNhUa1g8mrF3hpbAoPh5comjwL9t02ganJK7IeNYGtA0Vbs7PCvaev4On8FvUypkOSJMlgdO8DXCis6qvnz/DspTHsOLqagy3emryFPUniH9++hukHc3iQOtHi2lo0GbkFjraWePVaOf+zpfQfldB/pdOOP+6NiqcdloQeT2+cbLZEQA+BeE3oTo62aEdhPjfe+IgRg1uiT710+Gj/AqlqjseZBf3xdx8jLPk3mW59dtq8xUQI1ZGlcmPgljs4v2UknUskzo9asgCa7KVorSnFTteuZ67QYjUuz6uLPo1TYuaOhzA+vw1JOP65gzva1i0GA3Xdc/0FCPZ7h1yG+fDhw2Uk1x5v1Gq4KGv+JWOYnZgMTcqysPf6gmT/nSr5zxH6y+29ULgOW9tyMhPhT5MkSQKDxClx2doX/iYXRfsSGaTAyotsDQss7FUBhWddE79vjquCuc943kxJN+fSGlVNEqTJkgtPXH+dE46jO3d840vqh+2LN0hnF9+Imn52Seg/AJ68VCIQRxCI14TuSITeM20SbLpmjJe3DiNbknQ4tLAuSmfLgukDeiDn4D14d2MPStPaynU3H6IgO3744IApJf/C4F0PKW97ZEqeF01zZkD3sROgyVwYFq/uw9LOGTdX9kTGYpVg9/4xqqQxxKyjj2ByeSsSJU0Jazcn/ENxv4s07wn7NzdhSOW+cbVE7qT5YGZ8AplTaTC8f22UbdYfi2tqMP7EU9iZ3MDqzfvh+A1xzmMryX+O0NvVaIbadevAUzxsH9BrymqyDCJPcuQpitbFIFPZFnD2JWOcIC9MqmiIfY+9MLlXXbQsnRZOtCzpxsh/MfOBML0V6cqg4ph61wVuVsZE7Bnx5Pwe1K5ZDA1GLyJbGRtsvPRC5HO3eoSdjy2w7cgpdGtQEEfumlN88X8wZtV5cd7P6T05HSmFkauVZUHnd6zHjtm90HLcegRY3aGy02PipBFwCIg0GvI1PYcKZUth5pwlsKK2rRzUFf+ULo2DT2xhfXsfEhumQp8Bo4Qjk+kD2qJavea0WCcEG88/FHVc375E/DXZvwqPXz5HxyZN8W/5ijD3j8CGTbvFOTZEmr7hlK6/f9IPSeh/0t2WfU2oCMR/Qk+XFP/1Ww13X1vUZck4cXE4GO8Rkmf+IftwZ2lzFMibGWuPPkdFOnb+PRF6mbwo2XIq7Ny94ev9jiTq9DB9ehiajIXgpDWIu7WmFxnVVaS5b3uM/zsjZp8xhvnt3dAQoVu5uKJyVg3+G7kdNh8fIxmVax7giL+T5oGjhztcfUJQs0BSrH3mi5mVNBhz7AnsXt3AxCUb/3+EHvEGlQevgOnVtWiw7T09vx9Rlpa6TB8/FmNHjEHwGyNaUnMk8rn+uBvDtt7AhIHkYcr+JvJ3M8KDcZWiEPrVkSUx+Ogr3Dy1DZq/qtN0xxu8t3TE5ukdMOyKPfKlLS68bq3rVALvaGnNX6UqwN7NGtkNDHDBxAHt86bGcUtrlMmWD55kTby1dgZsfOiGbqUNcP6VDYxa5camG+/Rqdw/cHd3RrjW2hYez6FJXgOu/gFo808e3KYxxstHz2H50QS5U5ehZUK+qJE+C02POGBUtfxYfMUS9k+PQ9NkNwrkaEgtshDPw1NyYVksV1G8d3LF43fmMHl0GpqqM7F+YH1MexIE96erMGivMgD405Ik9D/tjsv+JkQE4j2h98qdDjXSZcMxSztUp492m0334EnLNjqVIELvsYfWg7YiyTs/ihf+iz7qKXGCCZ3cNJYpUAYrjT2wrh4NAui67CkzIlmuEjpCv762NxF6JThYvSYpXoOUqbIgR5rUSJqCCd0F5auQD+j25eFi9RJpkmqQIy2Vn4YGE7Q8xfHOalFmOtIU8N+xROjmx2ldbrLScPFy+7/Mod+ZXB45KtRFowbVkS49u640w6CZinMLkcz2oPq4DbpdrxtTMefoG4zr1Vwc65snOUZ1yI259yMl9GuT/kXFbhOwdPM+seRndtkiKFKnOTbMn4ZCa8kWYUEFDF6xFZkL16TVMx4oO3WvKGtMg7/F3xOdsuDYQ/K0lTwNOrZvj/b9BuOZgw+GNS4pztvvb4gNNxwxoFn5KO+a7ZV1SNdB8Uo2o3YJ3LJ4Q7iSW88Fa5Eze2XwIpqaGbOTGwygeqmcKN+1G9p37IQJ6y9jUqUCmNK/C3aRHUXiibuRb/AGON9YR89EISzdsgXJqpEXLr/7KFSsDUYYJIKZT8w+rhPiy6/fJ0noCf0Oy/79CQjEa0JnlbSdrQ0+WlgKkrS2tIIdzZHzb1sba1jb2pHXJhtw8BJ1s6dz9na2+GhuLvLZ2USes7Ky1pEt57GyVvb1r+ffonxLM3xkwzobC2RKkQhGD+3RrWNReLs5k7EdXatXp60dLXGj8iz1yo+tOj02+T5RudM62ywpNKRUV9Km9qRB8DRGy7YD8OTubVy9dh3k7RI1C6bGxANPYfn8nHDo8ZGYcVTPZspFERbITMc2sJsobbpMKveFTyLXBJfKlw43yOp/ZqeyqE2EDlLuF6JrlpJTi3A9Qh9QX0voHbPgiK0rahIZX/tgg7dXN+DwA0sM1RF6A6y/7ooeVbPilbUZfMU6ZEr+5kTgSWFktBqpqfxr984gVYGxsH93F2kSFRGE/q9hOpiQMeLKGiXRZNhuOFq9QK0Oa+B1byk0uRtzh9CM3FjOOmOCy+O6ocTKq3h2ZCE0lcgrF6WlHQogaYddf8I7H2MfJaH/sbdedjwBIRDvCT02hPdL89jZYOaQbugxcBhO3zb96dJ3bNoendAjwoNxw5jdYyop0N0Stl7uuHvpIi5evIgLFy/BNYCM2gK9cOrYYRw4eBw2nsp89QcKu6im54/uwNE3UmL1NHsIK+/I/UAXMxzYuxePTd7jrSvPy4ehZHlFwkdECB6bs1drUga8eiD+uphQeeR4OsjfEUcPHsTxU+fgFRKB968Ut5PBLu9g4xZEc/RvcPT4KfiHRIbcZKcar15boW75fLhLLjJNLp3GgRPncevOMxE8wvHjE5w8cUIE7nhw/TiOHjuO1zae1A5fPKbVD5y8aG7fzY/6GeaHs0cP4fS1V7jx0kyc2zy4LF5EjQiqw+FP+CEJ/U+4y7KPCR0BSejfaVmuT7TOrm5wdyPrdfsvO5uJDTl/T54fXbb2sx5yt+srMHXzpwE7frz8AFSpUAYdOjRB04nfagEfi9pDLNGgkeoDPBb5E2AWSegJ8KbKLv1xCEhC/wmE/j0k/DOviSuE/se9PQmow5LQE9DNlF35YxGQhC4J/Y99+GXHIxGQhC6fBolA/EdAErok9Pj/FMse/DACktB/GEJZwP8Bgd4b5+DfWd1RdlIHlBrdAiXHNUWJsU1QcmxzlBzdHKVGNRJbydHKVmp0Y5Qa2RClxzRFST43rhFKDGuK4mMo74jWKDetFypO7o3xezlUbTg8/QIx++hDMha+iYmHn2LC4eeYsvcVphx6hJFHnmHGoaeYtu8RptP++CNPMf3gM8rzAmOPvMKEI08w+chDTDr0RLdNpPwzDt2nfPfBv6cduIcHbFv0i9xzSkJPwITOUZLCQ0N10a1i875dOLAWHzzI8C3cHf5sOCfTH4GAJPQ/4jbH+06Wmd4dmu7/YNzu5Tj75CbOPrqG849vabfbOPfkNs4/vY0LT+/gwjPa6O8l3l7ex6UXD3D52V1ce34bF1/ew3Xj+yg7oTM0fSqh1fLxApuGiy9A0478bLTfDk3bjdC02UC/6W/79dB0pK31Zjq+lfZpa7uS8q6hjc6LbQvl53P0V7dR/o5UVmsqh2KLaNpsRPL2a+nbygFsf36ShJ4gCT0C63qXQJKU6ZE6c3lh+R3bNLRIYmSfcxojs2bBGx810GXMV4f7u+HAxrlYyAGev5BOze+hdaebCude2MDLnL3BKev/i/83gCJe0/gh2BuTO9QQx5KX+I+ORGDjqGbafAYYueksZQpAr1L5xbHUGWvAJVAJJXl8yQhdeU/tgxAR5IiJbasqx/4apGuZ58vtSEHHph15B1/bZ7prClbrBr8vdzW28MXbfJLQ4+2t+6MaXmlGbyLFYrCk2BnhFAo3LIJX3VCYW9r4n4ggq7fxOhlxlvJFkFTMv0UoXb6MPh/zThIBdy+D5iuVGO+ajky8dKwd+aroTATcYRMd20GEvAOZu2xA0m7kY6QjkzQReIedCum3N6KN8rdZBU0nvlYleP7L5dEAoSORfLu19Jd+0/JYayf3X3LfJKEnREKnOMi5smVESnJ4c+bLXPvJQxUeFg43u+d45R7pdvVzT57Hjbmo3F15ET6bLK8I4lz51B9Ti+SEpko/BDl+wANHonGHq8Jt7vh7/jjQn8jXsB286G3jFy/Q4x1SJ9Jg1wMHXNs0Hiky54DJlo5UVi64UWUN6boqq+7CfE9DOlYA/2vvOuBy+v7/E9lbkZmdjMrIyN57lZGZmZWZEKKsFDKzi2RlhIjMyEjLLC0UGk977/X+f859Itn55hf+53Bet+eOc+593/E+n3E+Hy+aysaOY2Xt7J6QlW+CLIo0l035qlnJjHkLqdxBhMFZP8QE+eNhCB0U7iRE+pt1I+K3vGB/S6P/O0LPRsh78Xe1Rhnx7+Dy/DUlVXJFcMyPc2+HvXoMF88fP+ghPi/w0t8fT108hNgFhVHSKeVyeFRebIbCaPNX2khLy8u58CvH//Ix9M49tJgKJYX6mOrw+98h1TVjiWib42mgJzIozzwoz3sKxRxJ8vNBMk25Tff1FGqq93OkvHyGZKoZXs+Q7f0MYa73aG5sPBL8fCnMs+QJmLiDcmxMUcWQncskhD6OETQjbJLGmcStSQQ94jj2WHWk2orWkbTNCHw8Seda1jjzOBii4Zslx4xh0jwbAHylsm2sLSL0Yho78Sbq99wvTuj/IqHnvp2xt5ZCTc8R6bGvUUGxOjacc4fdLgNoT+kHj5h0DFSkKHbr98LC8igyA6+had/xmKXVn5Kv1KJ4716oQES3cMd1rFJqivc2o3DtTTQGNhBBpe8KmlxujfGjemGBmSSRy4eyTLMNep//ENKGYsI8OSFE6HtBaoKbC0m6lhvxcd80d1JXUR8O4ekYIpBtMUocUwyyLYbTBz8H0+QqUUKbqqhO8fo7H/CHv0DoItx8E47ZdO6ikRbYNaijkGCnFA1eqlSrDVdxEtRrs4QzLOmOFKWwlQcLjTNZtiK2P42ASm1prL305uM5ZD6jF5HaPPP2lz9p/8SB/43Qc7BVcyB05g3BuCXXv4PHW+hQOOZexUoi4TvWnMgX9MwMNkNvpcrY8CQRJl0k2hxBM1O5Oq4H5e/C9egyzFlz6av9rhuvhE4jjIVtl+YoYsKybWhJeQjiPts7OyMOXdVnoF8fNXj9RDwC85HNMXHjLjSgQaesYhtcYaPMApSnl4xxyP0+5u2W5DP4r8V4tDxOB/9CK5lvMG7sLIwYNlSI51Cwko5xFej6Ow9CFbo3xi4UW/k3l/arpxGZKsPp2QPhG5GdlYWIztLIailCjooIqa1FyKYlq5mtaKksQpaSCElDFZFkb4f47rWR0LYy0nLDSnddPYGIuT2G75Co3EXjmLo8l9SZZD3MCkvNByP6mQjVxzEJnNaNpe8WfXuYWr7CDBuJVN/NCOXHM+n8g4qetfFpJZJnZE9VivYJjCRh4jcUTuj/MKEnPzRAV31J0Jbgywuw3toe43rqIMplGxzex+C87SWsVO+KRR6p8DXviVVGGzBQczIMNcsjKj0bic8tULNeW3QYNxUThyrB3jMS6f4XINdqOXqLFImtHTGEQql+ryS6H6UPcQV4kYrr9mIi9FrDhN3Tgx6iQuliUFtzkv3CIPogTDrmgSwvSRz+Y15vUUGacslv2Y5BHZqhFMVqz0wIwJBe7dGypZKwT72VDthJhN5w0GQgLQglpYth8SlHjKsnQrOFDkh6c1nIiLdkha5wDtqzpqNiKSk0bjdKiDmfLXZD1XLF0XrJod/wav1dTRaE0EM8HbFCdzGuPQ8RLjIlzAc1yotQmSXcMZY8Dwnvn2DxwoU45xHIst7AfucWPH7lj1uOT9CtRElEhfpg/8GzQrjecK+7iEvPC1gUH+SETpvuYqnWMLwgBcsWRuhDLGjPQOG+jzz+AmotmmHM1Klo3nk07llMR0edk7hvMgEK7buibf2qKCHfDhG+Z4X9S5QsAbmGQ+FnNxvrjzhgbs8+lLYnrzidtYD99XOYOGky1q2YgdGm9yiKIQUeIvMQKx5Ot0hvmwjTjevAFFcBpBHYP7on3qdGwmDWbGgOUMMjEtLDnt+E5QUn4RixzyNQDCU42t8TiNLV/qrgBxX2ygXrliyGocFuLDM4KZjDUiP9sX7zyVytRSasdm/ERjPmpEUZGJyvYvGiRTjjxrIh5uAN2Y2PHDwMU/NDSKVATCaGy+EVmQIflwtYf+01jhwwxOL1uyAmE21WcjSO77eivlbCTxyBdaYUshmp8KLw1688XMEoxefiEmzYuhHNZcvA2jcRdynT4R2fUKbSgt0l9zyQwlyg2KErRm+i68v2g1rz9lhqF4BHmxZDQa07xmkOR09NY6we2BkVypdDicp1sP7AsUJ/CdqtJpX7pNY4dsNWaDsjPROJ3aSRqSpCWjsRQEtW09vS7/YiZFCeiPhtOkjauhpp9Yshs6kIKe1laSAg0eY1mtMHooltPyF0ZgMnIhak7L0oTqp1L5eSuHW5jsRurkmq9TGMqGkpEDQj8AOYZPUMu6540DYmweeR99f+5oSe+1gcOnQI7MNTmHO4/4W2vjUPPZGIW5bCnvoz7U7AcTSuRKFcKU9L4M1NcHhLciuFVF09tBv6nHqLZOdtlHhGhuK028B4WEmI0+gzFEVx10XN2CcbskSuV33J7pPsResqYeIlUnFGXMZQnX35XtoAX0qaEvfJWD+WHnIm6Vvchlbt6ig1eCFyxO4SiaukOu65OOGJtx+W1CgGUcdFcDtPaWxpm/0LJ5SkpekVN9gaakNUURbJFNL2JeW1f3CRXiLaZuOXgPNT6YWUpShvno8gXVwKhnZPoD9KATUVxsPXYauw37aL97Buqxn27NmDmoRBu2Er8V7M4sHTOUgPwT1ypHF2l0ST+/9afpbQE9+5o1jNwXgTJEY/xXJwpVv9+qQ+9l+4T1nv9qM6feiJSVCthAj33okxupEs3hChd23XBApjVrJPMLqXLEVJkyT+Etdigfvr2sAn+ttOQlu6svvUH8YLRgvHbH74DmVKl0D1xhowO2QL+zW9IOqzF482doGoUV8EejoI5pUznvGY00MGjdppIyMj9Rtq/gT0kqmAUpWmYHatqjh2ej0669hhdF0yD4naC8Q/vrMSciKd0bhGGZjZOKBjwy5w203vVRwT5aMxtMMg4bE5P1aFzEbNhbDKF1e2QXBaDlSb9SQKBTpVolwDxCEmE0zxhjIcXjWfjX4niKRpndOONeg2oqeQdGiHciNoLDsFx73LUdv4HizNLBGdEg8tpRoIzoxFZQrpvHSDOYLiU7CpMfmbTDPAXB09DKnQjMwICThNmSfD3Y5Aoc8o7B0owugpM3DT+SVePzyO5k0r49gebZxzjoHJwglgT3wmGZMTnUh9LCcPS78kNCPVucwMc7qHL1C53rg8zNIC6d0pSZLpQToqFHLs3XSLwADSgslWLY/OQ2YJg+R9G9Zhz8krCHKkd7RW60J/ndobEKFPaQuj47skbaeRZ3p3aYr9TNI4EXoW1Wwi8hxG7i2LIfboPiSNIJ8cwiq9GZE81dSONej6CHj6X4mkc9Ek1a8QusTxrSIReoArtecjgsJkE5LASd0+4gjKTzJG07nriMAPQoo5zA3ciuLjPkjonNB/6sZzQpfEqf+8fjOwDGUpK0dq7BvsTQuxh6jpRAHngFumuPaWxudxLvTRksZdQV34Gks3HxG2b+wjnUvot9Go93hhnc2o4rjkTZ+qdJa5rDJiBXHDDsPmS475UHQndsOaW/kdPjwOr/6oMnULSof3ZVJd5apQ2bIL2ZeyU4NRpZwkd/2UbSQRkex2SCfXsU1UBjtuvxLWtSQVn0hUDuvOP8ntMgvTezWXSOxDNwi20exIL9TObV/X4mG+82suVxwG514h8C69fJ+cQ5tBZj/1DP6rO/0soUf5XIXUsOECdmN69MYZenYO9BTBymY7hs3VwRzF5vShzIRcWRGY/H5281w8z/XKVJ6mQ2syBQmdRRTeTlKuAw02H2zpgFcx39ZzC4Qu1Rya4yfj9H1vegwSUKZUcWynDIis2OnT4IAR+oYuKNNxEo0/Q1CMVOFWbuHQ6VsXil3IRPS9khkpXI/+5UfoW1OEvlvvC6GLq1TtIWgQ5ubmGkAwZfBjA8krJIVJlUdgInvaIjGse54ZqXPrQUK64qvrOkKcnoO2KhqCE1YPmZoChyDFVWhjorEplHc/xrq29bGS0gA6H9OHG72Sl9UaYs5xsvmGP4dUZ8uPZ31xl+5HHG/MVsdAB0lY5Xsrq2EdOcqcMp4H74+RkqMxskPv3GPjIDecDaSyKEPhM4GgY1NyYKI7B8xycWtBSRy8FYgrm+bhqKCQyEL1JaQdyPFGlcaT8g2C3t5jzl+nsEK1qXANi65KzoGV6aTKXnknGntoEDFrkAgW7m4QVWtR6K+LQOiT20Brs0RFjsxExHYriWxKxpVBqvVsUr2HU80iAo+zO4Ckfj2R0ZzInYgctA9IQo9rL0NKoxzEJCYQQbcglXu7Lwl9LJnicj3VHe1rUkxsEXzvV0bv5bPRc/lSuDsVQwttY1SeZoVVZ8jBlknmo4jYx5IDHZfQf8JoRfeOE3oBCb3QXyfe4L+KwM8SenqUL5GZPNx8InBw/iicI0LfSyrxqo0UcXLLXFRrqUZCeBhlsVslQHV22yKBiKJCPdG41yhKZuSFDqVKI8R5B0qRn0OFNmqoX08OrwVp9+tlQ2ci9F4SFbTkI/4VQu/9KaG/Fwj9oHMIVmp1QulqjdGul9G3Z3ukhxNBFcObmEyYje2MPqYuODO2ntBG7YZNId+2s6TfQLKXtptDfyQRNxRHYAIbqSSgI5GbWh9dGgPEQ7lRB7i+C4OxpgzCiNC7qNSD1pLJGCsnQgPFcTScySKnPFdsntyKEho9puOD0VxlBGy26+ARSeiTG8iC/EeRGe4BqU75Cf0xdRfntBrNqb8ZzImTNB87FSth7S1PrNcZn4/Q+7cjQieyUyWJX15zI1Ifk+mrlAyOzqsDa49omC0aCM0pakgn7++GzdSxnRIW7RXSGqRCsWkHNCBtRaNOnRDzIYUxbXl7n0irQnWsue4Lg0kdseiKhNBv6Cijn5IK+l0JhwWRuUiqN3Tn9yGzR/6siYXx7rQxmEgq8nZQW64pNJfNvNdjo5BDKa2zYmPo7zBkxMQgIykJCXpTka1AJM/U7ORzQ8AhplFFxJ+/KAyyfMRkwhnWhAYIn9rQP6jcScMwiknah1F71kYkvCQtQCi19Z7aCSIt0d7u5Pl+BOsuvEBwZCxKaJ+WOL0xT3ZmR2ee8ExyF+zo1A6bssYc59hvapfb0OkGcELnhF4YHwXexpcI/CyhIzMJhsanBcnV+9oZeJHtONbfCUsM7WhNMjZsI3VtTho2HrkpdOJD84TDSC1/0WQ9VhmswtIFM1CybAWkx/hh4dLtggR4ba8hopmJ5xvF5fhyrDohSfAj+YqnY8M6Izj7MeU24HvTkgJ3uCHIyRLrLcm2Ssl3jAwN8Tw4AQlvH0FviR4MNpwQzvmrhfZfY7SWaW8R9fgsLB5QNkbHExRY5BkNTiiboFlu2uFYT5y67SE04X7KEDG5dn/HM7theskLOWGPsHzFSupvCZYZmZAWIgevrh6E2bHLeH3vNAUoybNJx4d644gH2aqp3LWzwvyVxgIWDjs3I4SIOzspDIYnGOFLio/rTYQziJICsEhvM2nICOasDBhvWgf9pXrQW74JER8l9BScOWhFO2fBxHg9bj6W+AI8uXkONzwlJBz1kvwg1m2lmSBkp792ANN0NtMwhZVMbCZH2VSa+qlvtJkIMw+xxPBAbN++XTBD3L95Bg8+OLWTOn6F0X4B3zf2W7Ft+xZsZiauY2e/hfgvr29rSFNgJ5PfzEw2tZUKnb9k0hrdKnayVMnSgfRn7kggX5psJp1TZcukTvUpAVSwgHNWZg4e+ZBkPawRqfA7flPlLni7Dz+MBlPNsHJfT2w91BF9l80mLRU5bjJ7uvo+tFxKZM4kdMF+zqapEYGzwQCzw3+ws48jyT3X+70YTYkLjOBOcZzQv+HAx2O5//L3gR+Yi8BPE/p/RSwrDsrNm33Xy/2/dsGPL3wE9u3bBwUFhQLXVq1aFerJtF1DgWWmqaE8zaZJzyJ9Bw2amPo8MzeAViYNSJiuJ3VoF2SRmp05waWTdJ4kT2r4tz7IJrbPIJMQ/YedM02pVVf4uoQ+hiRtFlRmLHOQI+LWJDPhMHLYHW5NhP3BIc4KZRmBs0A0Y5iqnXnIs7+ZBzyt/xCAhjnTqe+h9MwsWA2T3g8ToeelpS5MgLiX+z/s5V6YDwpv699G4H9G6P82jPzqfjMCqkaTiYA7ELEqIjIxDpnZJJszkTszgxZUs6mS2iFLuSpymkghR0EKoJq8Vk/QLDAjiSDPU9rmXQ4niWDJ/+ZrEvqscyg7mwLHkJq8ru55FJt2FrI0k6LiotOQnnQIZeYdwKrTEg1KKbKji2gAUHGRLaosINLXJkLX2o96S8lEM40FpdmPo5SqOTklAzJTDqM4Se8BEb8ndgEndE7ov/kV5M3/DQhwQv8b7hI/x9YGNG+cEfrgRvALeouAqFCMWjmPyHUP5pnoIyYlDT1WT8S0rbpYdXQ3jl+1xcpDJmg8uz/2nDuGTvo0xXH+JKRSzPbV1hT1bTzNTNDqkF/lTlK0zQNv6B1xgfXDAIjj4uAdGA57l2BEJKTB+MIzeL2NwU4HD+y5+xrpFLGulu5lmnmQTAOKbGy+8gLPxfF4RbM87vpHwPy2D8LiEnDLJxyVpxxCcfXtCIj8PXP2/ypCP3nyJJ7QtCVvb29eP8GAfYznzp2LsLAwvH//nleOQYGfgcePH2Py5MmcMTgCfzQCbVaTDZ1NNVNvgLte7vAnNbqIggdddLoO2QmdEZMUD9FQBZy/6wDVhYMwjLz3tbevxuDVM2BkuQU9TGaT6lyePNzjMX23Pk1Za0dVDcO2fwgsw2K078UpJz+ssvOET1AUDM8+FWIJVCBp/crzdzQr4DncAqOx8+JzHCPiT8/IxOoLHthxwR3Tjz0kf4oXCIuiAFfbr+MFDQTWnXEhsk+D0Tny/Gcx3cdac5U7e8osLCzQpEkTNGvWjNdPMFBWVkaNGjXQunVrqKio8MoxKPAzcODAAU7ofzSV/fzJ3fGiYBP/SMlKS8KTsLwZEBJCp+iQIxvi7L3r8A9+BWmNlrB1v4Uao9vD8403EXojXH3ugTYUubCZjjq0dhti5Ka5WHt0J0ZspiBT/WohMiUZAzawOe2qEE1o/wWhWzs+xRtxFFKSk6C87go5D6ZDZsI+3PcJwspT7shITkBUciZJ6JKok1uuPEMqSeqWd/xg7vAYvlEpcHr5Fi4BEeRc6YprT97D8104yk2gYDUa5iShc5U7d4rjTnH/yGfqz7uM36Jyp1gFLL6+UL93ybQ9m3kof1j+D+DJIdsrcwrPoalfX8tkmU3bBUerz048h0KNshwB7LgCFZYYhNpjbbL6qfc4a4dtY0XA4bMiwTAvV8GP+p27y+ZHuxR8exElL8qIDcZKR8lsAFbaGk0Rpq2JxjbHrkunEBEhxpgNi7DLzhpTti/Dkp07MZck753nT8HU/ggO2J+GpcMB9F2rjWM3bWFudwSzNy1CWkYG2i+leApT2xKht81P6EKkOJpipk7ObxTPXYp5qGuQt/t4so9TzIxi48i5jYWAHbYTZpef4uR9XxjZPsatlyFw8hRTe+Q4N3QHSs6whBSbl84c61gSl3Fkax93HMVG7uZe7uxm8mlrfNpawb9E/IifQeB3EHrSMys06DIEFlp1sdBOEgTma+WhSU+Urj0KbpTKcoiF53dPN/zpGSi270UezhKG2aIzCC1atBBqHQo9+lMlPQxVKpfHJoo4N7V3D0gmdeWVxEBHyJSUFoKn9NHfn2/bhGFqUO3bAzWryyIp4+dI3ffkDjSlfAiVKOywWg91TJ4xE/fzxV4KkUTA27sEWmbnv7iEuQMbYCmFe23QuDFiv5MzKZvm57doNQ3a5hd/CMMXZ574BG2GmWOWigJsXT4Llk8hgibqGORr04WSmvSffvSH/Xx1h+QgOLqyULbfL8GezvAIeI/lTnkBbFoZklPceLKha7WBrtUOGhhJsqd9GBAx57jk1lUpSlwFiuFeHpktyyOpBYWefueHtVZsSiXrUzJ4ajKntxCkRjSl0xcS+h4Hd+xw9MeTN0HYcPkJwqLj0WeDPVJobmNEdAJWnnuBvY6vKeNbGsQxSXB7FUwe9OmIjE9C+ckWiE2lpDE0aIiMS6I0roG49DwItg98UVnLAlIjd9E8dC6hc0LnEvqPvgF8+y8i8DsIHaF3UFGxI85rl4b1k1CkhvjDYu9eHCbTmYNvCMw3meE1TaiO8LyOWsVF0Dt0RQgbe+O8DU6csoG6ahPYeOY5D90zGiMQX0mZOkgj9SYreoPqo1pfHezcuQOG+65+cvVpWK1vitBElpErBVsWzEcCkWFCaABsT1lieDNlLNdfDocXNC85NQFnD1lgn80lYZ94z3uQUeyNo2vHoOF4Q6HNuDfucHrshss21jCzOkkBcl7i8GXJvPKIt964TeFVUyijn+2xozhx3vHjeVy1OYb1cyahecPeGDVuBnrIVYGG4YfcARm4eOIoLI/bwe22LXlC07x3Kg4nLLDT/AB8KWkRK5N7NcC+6xdRrMzwbwbHyRQ/EqI+Th5eHZVGS875W8VyhBoGzzz9xeYNFC61y4DpmLuXxRT4tIRAU0M735owrytYqHfku/18bWN6jLeQwnioQgN0180LnPP5vrfJm71a02ZQpH2NnPIk9NZrp0CKnNhEU9pjjKmucBibS8+mrjFyz0pKRgZNVWOR4ViY12yqcU2lEE/BX9woLHU684TP1a7IUmAdKS1SuU/6UuV+kmzoi0+44C7FOtCxcsbbmBSsOP4IrgGxGLP3ASKjo2HrGgBfiqM/YpsDWiykuehDKOlKeDxG7HKkNNEheBsWTw51yTCzf0p2d29sukzhprWsKDnLQW5DZzfuexI6cwj7F+Ky/8o1FNU8dJZqtYg0cQX+kPADvo/AzxJ6hL8j1GrJo7qMCvZeoyArlLTEcMpwNKlbC8MWmuQP3pIairPnHJHgcxNvKVR7/4YiyMgqYeL0+bjj/QCjOzVDi55DkRUfCA2diTA4fB8Pj6xFgxatKC4/xfYX1ftCen5/h+YDV/yE0Ic2RJnyFTBpig55EX8iWSa6YNKIzug3ewUSvG9jgI42jpCX8krtgVBR7YhWTapTKt/lAijTGolQq3kbKMlXQs0RkjSabKJyHZlS2HsnCGGPL9K5SEG+em3IUjzzAVqHYFxZkv3tQXA27pgvRVX5sdjfTRYKKqqQlymNeno34LhYkfapgpGaE2Cw8YigZg+1X46WGrOpgxR0o0h59Zq1QTulRigu01BI5jJdsS5k6rVE5/aUqlOqODwj0uF21Y4GPnGwOntHkEYlJZUyDrZFAq3YMrEv6g1ZgtREd7xLzMBcE5JEKbL68F790aheHQzU3ULXE40WxUtB98JLin/+BHNWnULo/T2Qr18fbXuNRhANZF47UwCcsGfQNb9AkWdchbDRp3zYfOkQaGjMQnygE6rRNatO3AF3uw1QHqwPzS7tULNqKWw684Ai7nqhm3JrjBrdC8bHL6MTxdu/FpGJttMs4XNCFzMP5QXK6aTQFJttX8DRZABkldQpJBGwadlMkp3T0KJMcaw8T4ma3rhBuW5D3A3NSzXaZg3Z0CeRmnyqGrqvnCIhdHa7BFInk0V8AlJZdLjcyHDJROjpisWQnZwuTGkTQrjTfskUIKnEOGpnCqnvKVLciJ2SZ0E0luaSkwR9xvkdHngG4n1cGuYffkiDhizonXRDBNnNt199gWdB0RizywmnPcjTPjZZkLj1bNxpoJmDYdtuIjDoPcbuc6b+MkkVHw1X3yBcefIO5aYeRbFRh2naGk+f+k1C9318D7sPWMH77ddV0vlJkkZO74IQGhKCoOCQrw4CQmhbSMi322IE+r3tv0LK/+WYoiL03u03fpclYq/2wHzjw5jYVLlQ+TQmKuLbUb8Ktaf/P439LKEzRBJJ0maa37ZdNJHscRr9KF6+4QZrmE4ehGOfa2s/gXDqpGEwNVxMJFcSz8JTcNdIA/LdRiIj+hHKElFYOAUh2ue6QJQLLvmhX/PS2OYcm+8mvL+Vn9AvW+3FjVtX0LE0U5Er49MspmcmtUS7qYaI9TojEPK9AJo6ZEqEULkx4rKiUEaKQpNmvxH6K1WhI/UTSOdRX9IGffxrE1GZPxTjzObxqNhzHSpRZr6dm+ejXpehRIpRKFuMCD0oC877l6KywuyPZHvffBlkWurAZqYq9LZsRed6FdB4jEQijrlhjBZEjkgRCwlkts5tSKrr9UL64IcUPKyJTEmsWj8Bck1aYBYluLF0+9wg8AGOVHSXLoF40h7HiV9gBCXFuaSvguHbLmKInoScQn1d4RebBYN+rXGTeNnNZicUKYsd4m+j61iTj7ja9qVkJ265gU4ynlNKVyJ0esNuUgS/ljsZCYdCY+RsiO9ZUBKb/ihbXQNeJxaj1cU8VXjPXlMpKcx5TJlDKvGWEsl5y6whKFm6LF5RiNwXu9XRzegh5WyIQBCR4jPbDVAdborMqFcoR9oZdpUrZwwUjvNxPo8uc/fAcP0ipJKWZ8O9PAxUSUJnBMymrrVYoC65BjYXnRUWZCYmASkUxz2LCD2LYrcnssAylKQlJymNhkDE5kyzQ5gFJ1H8/tE0ZY2C1IjGfxLLfSLLdX4YJaYfg/rWG2iz3AYjze+h/gKaUz7yAPqZXMOMAzdQfMoJdCcV/MzDTtQG2ca1bTBr/020NLwI6Ymnob3/OhosPQ/tg7ehvtMJY8wdoW5+h9reS3b1Q3gX/nkC33yP+S//+KumrX1NQo8OuIPK9DJUl1dE2WoN8O4bJC0hTDHCPY/TC1waA/opY/sNvy8IPSTYC40rlMDdgOgvtkVEhMKoV22oD2+PmuNXITE6/I/QChQVoe/feuoTieFrz6AftOesQQzLYVGI5ai5xbfjchdiP19tKi1REmLyHys/T+iUkYvlmi9ZEfWHzkCW/zWU17WCXCkRlmsMxum8b/xXEQpwoHCYFAf9jE80Xh6YiHK1a6JWzYGYIVcOq6xd8fiokSRpTrGykKbldpf8H75gR3JUqiQvSEKUHYhmvkjU7DYzZeg4SaazD8VpRWvUUlKAcqvxGEvkePRJFLS71xDIXaqCHC07Cs+vmqgE/CjQCCP0SqKaEAskQYRevSJK91+G1w7kCFV7GORqyVDKUz3U6zSYApMkCOl9H1IGGpcjBvkJffdiyDSfm/tuhKMnJaqp22q+oM2KubFJuL7TLyNRjOFIavKOIzUxlM6JJWdRaSSHmvWVULdZI4wmMrJ2/TahD6Tsgq6BkhCi8ykhSbV1zjg3sQZqahrRmmChn/EnXmHb4Ha4HkNSabg3FA88RYbXSco4KJFuWXFc1AaGLJA8K0ToapNWC9qChDdP0MyCkuFQRrnxGjNy9/bGspOueGsxH+3s8m720ElawvaspBDIlpNGQHYaVCpVxlW6fS0mWsHLcjyaDjAhE3csylSpRnt6QqmvPjsC6vS9ZZhP7l5XSOnKDN3t5aWgZXwVYV5nsf5TQmeBZZianAhdeowqjt29BPtn92DncQfnXG7i/pP7yGhAKnfSugiVtEJQlMJVSjt7+b4jzrtchf2DG5ixjRJGjWktIXQt5hSnJ/RcRzs32htzimPJVpiDHIvDrknrx7BY7SwiHM1fF+K10zr2m0WSG0+kPoo5v7GQr3TcSKoUEU6oI1lkORbDndZNOI7iA00Qn8gl9K9K6L5P7+P4oYOwNDEkh5PeEL/3xmwDM7wJjcC7AD8cPn4J7+nvQC9nmG3ahB2HzmOvmTHsHz6G7ZH9MN1MDgpMGmdVHA5v22UQtdBFUqIY2w304REYirDwUJw+egxPfMiuMnUkunRTQf0aDaB/4SkioiJhtd0MG3bsw8u3kQgh6f3y6aMw2bgZD56/hpgGEtfPUIzpDVuwTH8DvMVhsLU4CLv7zxEm/hmNwo/3+Z8TenYS2lSRwpZbL3HWXB/OX4mR4HlUBzN7dYFsq7kfPxyF8Yf3c1/YnTqXL591YbT7oza87tti8hQtNOmpid/jzvKjM/i923+e0HPPI/EhqrUdyxLbC8Sx3+kdjswaiuN55s6vnrDDroVo3VwWPjSt58keSrRRoib8SGKaXK0CVh0hifL6Pkrw0gHnXe9Alto1c84j9Nu6ffNlyHsaHYJWJCV/yJo36ei9fH3eXEof7PIKRPvAIGrrsKsYu2cNRuMuPeB/n6XpbUOkm0qZNWWFLH0U1Zwk9OoSQqekHzO7UQpVGTWEx3gKfQzWpsAlD3ajltowJL+zFwYcIilGykSmtWlwk9v7zZ26kJGfJSH0uDuoTgOGofqSgUfmy0OoW6osNHY8wUaKYGZ6/TmtTUJ7asOZOPX4cIpcVqk+jrsGYjStO/LoW4SeA0M1MhuUIW0BlYk1RHBjGWgjHFBtIDMbvBfOq3/vPqSJEOE2OeGlRLyklMQKmD2RnMHqdP2I1X3dtlh2LxfnrDDIU0bDeTsvYFpzeTJvKKBnl7ZQm7iQ9s/Gkk7NUaF+CyhVLw+ty3k3u/84SSZHy4EVIKquJryfa9W7Y5iWLu4FRiHtKXmIs3tACWC8Nk1H3fo1MdyQfCYItakyFTFIey4UaPvc+zTyIBG6AdNY0PWEvjiLVXcoVXNuaWkwjjzFlamSdD2BlhRgRkRmF9HgBuR13gCyY1Wxfa46DOeOxwaatmY0bwyMlo5CmaGUIW5ELdqvnuSYkWQO0aQ2xrak2gIDt7Dro7wCfhHooHca7ZacJS/4M2irdwYd9c7S79NoQ+s7suVSW1p/mrafgiptb7/kAv2+iHa0fwf6zba3W3YBrZfZ0PoL9LctHXseHRfboD3Z2udYu3y8nsL+46+W0MPE7zFApjpatWK2KhHkKAe2y86h6N22PMYZXsUrdwda3wMh7+/SyFsElY6d0bR2eVQfvIZyEOuiWpOWaNWUEterLkRMTCTCaQrEMkrBt+zYHQTfNEO5lp1QQnUTohMi0YJeilPuz4V+ZFp2xLjJ03HsmjuWKLNMU83RqnFNlFXsimB/V8g3aARVFXqAZNvi9dMLKEHHtFDrjqnzDPDy/RuULiWNYesPITpcXCgS/v+c0OkpPLVoADooNcQUI/svn8mMGHToPgBrVi5F096zCkmazoJ+2zIYNIoco8q0zdfnevJ4ZfPw69SWhagxqTPZZyIpFB3pY3/N91Ml7K+/Prv16P7pbkSbbgtyP/6/3tafeGSBCf1PvAh+Tn8NAj83P+DLy8lMz0QGTfFLSU+jqWdkcSev9o+VvMqTaH1UUiKikxKo0pJSpMZQ5rWU9HRhqlrqh/3pb+G4D0tS22ewuYGkmZEU5h+UTX4PNHWR6VZYVjfhn2TYJvGtZ39QmFmmemHB4dl+dLwwOGRx5oVjWEmjb6DkaMGALyx/z636qwldTBLuqRWDBJK1drhA6QFl8TY4AnOV62Ci+W28fkyEXrovfG+YoYJSL8zpJIKJ9SEapTaHb2wixGIxxK9vki2rJXyj4xDqT5F8SPV11ysQ4vBoOOyYh5KjdiE6PhJKpUlld9sJpXprwWAJBSSgPm/7++dKB3UhTn6NuqTKdw6LIclbjIQ4X9L2lIb9fTtM1V6Esc0aoIzqIMTToIFFuvMPfF8oZM5MCV8Seg7uWhmha+eeaNq6Ba5smwxpIrfBBkeEp+jtrW2oU18DkU+sMMvyMdm1EjFdtQwNQJrhnl8sQl0sUZyckjqOXiSQ8SPrjejevTukSndCZHIoRvSgUa2UIpTLl8KRl0w8T4dup8aYf5RGnpli8sqlyEstpfAiJgPDVWvhaWIO5mv0E7BSN5FkYDq9oDcCY4U0CtDrT5KUlDSmm0qm25xaOQLFSldAEGWz0jU7isyA+xBJl0cYZeTyu7ARsir9v/o2pLrtgdYe5qGbhMElKuL8lm64SqrNjyX9PfrKVabzqIH9t3yRneKP6hXLoqyCKjxJgsnJSsfiXiT1VGoI+yekS83OhEafxQIGuxb2RomSZbDipDP9ikfv+nIwf8BkvwzoGudm4xI6ysL6UU1Rsoocdl32Q07iayjKV0OdZl0pG1YaRg3WxVHj+di05zia16kvYGLrGYMzGxZiqIYGtLRGwfuZHdZsl0h0Fxf3QWhCXiaytJAn6F61LJp2mIR3SRlYN42kLWrD3kuMaO+7mLB8pSCRnbp5Bz1qU97t9ZJUkT8qnNB/hBDfzhH48xH4qwk9TByECRVLQ0ROFWXKlEeJyjXxNojs3B0bYzQRx6Ix3ehjVwbvPJmXqkQ1p2WyjQi9JfxjiMDJpi5+dZXWK+EVzS30uLgdUqW6ITA0DCFU3c+YouS4Q0jNSkXrMiKceBmFcPKmv7tjlNDWhddhZPsie5iLN5LiXqIJqdbuh8YIRJ0Q50Vqo1JE8HEk+UdCo3tzVFIZiITIIExSbIxFe64gMuzH6vSfcZb7nNCzM1LRrEdf1K8rhxOeCTSICBd8QZb2UcEt0hffWd8D6/baItZJDwPXuOPR9hGkmtSkQWMIecaOJH5OoFFwFo50VcaW5yEYVF0BKmXLwYGmcOTEvME0zZHYstcGw+pUh0t0Bo2As3DFeheKddTDi/1LUGXUWHg+2ovNtzzQY8AE4S149+ololNzYKTRGVdJtWjeT4SnYjYYiIfeuFEwveyPI0Mr4gRh2ocGTxPn62Crxyv0WriG6QqhM1IeDxKSMV13P3atnAnfz98t2qcK3Qsmj2spVYVjGOWd3tEHx+9+orLMTkF6XJQwC3V4lTrE4KlISkknb1o7NOy2Eq+Okn2u2ACi5Gj0b8q0AJRMoWo3Ia1kXGQUBf1IR59yJfCGGN7aZBwqjZEE8OisvSLvbAJOoOei7cLvqIgI1KRreRIYgZfnV5IHtRUNlBrD8304klIzkBAWiOtXbVBCyLFNSAS5km16CqXhtIPGIsnUpgNdRfClgZFQMqJRiQZaQQlZgv/GyW0zsd+LnV0OerTujBdP7KBszNKWxkJ5kMRGurBJCUSkfjNx6Mfz5oT++QPFf3ME/j4E/mpCF4uDod+nMSrU6I6dWxeQvUdC6PodW6KMfAssnzcJTSqL4PLuHVToY+8UmYbY4NtExk3pIxkvIXT/K/S7GfwTs7BrWmMok10nLEyMqLhoTCdvyQ8DAUEL8II5yoVgYVeSUGs2wVNykutIEv0DnwAkUq5keXL2uSsmCZcIPTHWS7CdOYnjEEZSeWe1pmjReRriQ18JKnf19Za/XeXufXgquizNmxNrPLQ9bpDXyQ0DBVi4hCHrwWr0X+WKAEczbPMIpqc3AoN75nqO0q87i8lZhhn2GEU8tcKEPfZ4d3Ylak+xhsFEVTQtQVJ4XDw6yNcU9qmlxoJPpMHpzh2MIlxYuuQRI5gzTZyAYwuTe7DSHoxLZK7b2VeEs27BSHlMnrfd1uHqiglYNk2BEh68Edpa1Ki0YKtW0Vkt/A6+uwNmBh1g+zQBpoumgUxx+cqdrcOge18ijV85vgWmJiZYol4PmqvzB+ropkS2NnKe7NRZMe/4RA/Ub78QyZ4WWGH/hF0tejdtTctMVJNpDL/4XDUbTXVRKVsc/kyxEHofZfudEdpoNecTQqffF633YebIzhigfQilpKVgceESbGzt8NL/Dc0l7imo5B5YroLa7E1wfu6NGl2ZV3I0ujarTD2ziz2HMUv2CW0fJ5z8PhB6WihpnJp9NGHYbZ0KycxlYHTPAXD3uIQOlpKAHX1nSmJTb+shjegUTuj5nxb+iyPwbyLwVxM6I84IiuCTlhxPjmvRSKX4vGxdTEIikhKIsMMjkZqaCjFNQ0tIThGWzPEtlVTMYiJmQfrN/R0f4UWELIJDYKTgyMamrsUm0lQHOp7VtLQ0RIglx0TS1Ig0SgLAHOkSUlIF57aQEDGS0lIQxvoQjhcjmf0W+glBHGkE4uOjSfIXIyUlBXHR5EBXSJnevm5D94MKaRVGm7p9fHKNBrXDDRLoTCmAxIxl+lioXhsjtnrA9/JaOAQx+gzDwG55hH6XCH3Vg3jEv/WANKlxN19xRajDVhrMKOByRBIaUfuudNhLxwPoqr0QdeZISCjq1V3BdKFnaAS5NgNoTaxA6MbrN6FZnbK4QuptxzWdhXUvnrEBlQgjL7xD66olSY39DkkBD0kFXwJGhgtQZ/Q6oc2A6+T9XKq3QGab5k2GzyfvY06IE6RlOkpsWZ8U371quPLsU7/n3I3xj2n6TcO8PRPI76H1AsS6mOGIK3PAiUHPxhJC7yBflZIySDxgT4xlAzxZvKd+ru3QhHSp7jBcvRSKGqRFyC05XvswZf5a7Fo1Cb1m38LpEV0xZOpS7KUgGH21D5BnczchX7PnMVNUGrEYJkt0yIdDD2u1O6FOv2UwMzPFu3duqEXzk3du1UdpwvE9HTCsRV28peN2dmkB7dVbsXBQN+y+cA2llTRxxHwNGg1cgVj3U2h1UKK7aDZdQuib1aQRxQk9/4PBf3EE/lEE/npC/xmV9E/tQ2Qd9t0pb4WjHv+pcykg0X/LKe5rMaoL5Tn+SsPp8fFfEOqP+vpNfiE/6va3b2exutPI3v+hZNKAMJNigH9esjKFmbFfL1kZSCS1/NdKTgaZCXKbY1Gv4hLzklf86sVxlfuvIseP4wj8OQhwQi8gef4OQv6vbRaFl/uf8wjzMykMBDihFwaKvA2OQNEiwAmdE3rRPoG89z8CAU7of8Rt4CfBEfhPCHBC54T+nx4gfvC/gQAn9H/jPvKr+P+NACd0Tuj/v98AfvUCApzQ+YPAEfj7EfjrCN3Hxwfh4eG8foIBC5BTunTpv/9p5FdQZAhwQi8y6HnHHIFCQ+CvInRFRUUK89oKKioqvH6GQadOnaCkpARlZWVeOQYFfgZUVVUxezZL68nLn45ATqQ+cuL2Iid2G6/fwiDOHDkRLElNXmm72hbjzG/9PXXPbZxy+Tzixvefzr+K0P/0F42fH0eAI8AR+N0IZEfMk8QCz6IITbx+HQMWRz1sQb5b0W+zAyzv+P411fy6F3be9CrQ48QJvUBw8Z05AhwBjkDRIiAQehZFcMygsMZ/cs0Mkpzfh+Xn5/qj7f/l2rKivyD0nsb22O3g+dfULZeecUIv2leN984R4AhwBH4vAtnhROhpgZTjgFKvFkbN8iPSpdiLrC0WVDmFAgp/r128wQaj+T/cp7i0NGkSfCEvX5vapZDEn7aJABSjvASsPzk5lh+dzqEwruVDG6mviNAJp09Kp3V2ML745K+phmfdsYtSVBekcAm9IGjxfTkCHAGOQBEjIEjoKRTSOYnyNBRC7dtLFfUoH8PA/p2wa8tcSoB49/vtwhtLF475/j4Z9zFv9nCMGNodCQl0nom38+9P26dPHoCR6j0pFLbTl9v/63UluxCh6+S7U20MzmP5SZcC1zVErE8CIuEXEivUORb3Prahe8wZb8MTYHDa7Yt2F1k/FPZfbO1c4D7ZeS6yeojdnNCL+G3j3XMEOAIcgd+IQHYEEVX8Fcp5RMmBClITzsLORh9W+xZQnQ+H85RMKfEczlovg/0ZA6Fmp13AHXsjvHDfQSkYTgvtHz+4ED7uO2G1fwHOWeuhVo0quG1nQOvJRp1J6YppnxfOZnhwbT3OWC1BYgxlIYw9g+i3R+DksAG2x5fhKB17lPpE9iWhsr9PHdbFzYtGOE3HfNyecxl2p5YjJPCw0H9S6DGcPqIrnGeBrjX+MnLEkiyGH0rzZWcwm8i4oFXv+COhCc0dN7H/5kvEJadj3w0vLLB6ANNLT2F+zVNo86iTL/SJiK1pOW3/HWy9LEmdZHnbG3MsC97vtAN3sPs2l9B/46vEm+YIcAQ4AkWLgCChxxDhRZsXqAZ5bhASISGDjsVx4e/k4G3IiNyD9Og98Hku2T5yaCvJftmMVPdI/oY1ssLNIVu1HKUALgZz0zHCemlpUpvH74PevD4Yp9EW5cuXyu3DAjfOzUP5cqUoHbMlSf0WuHh2HipXKoPaNSvD+vB0MhtYkKaB0gSzStutLacL2xrWl5G0kXMYgU/XSv5OPlCga0XMIZLQ88/aaLDoJMbuulngOvMQaSyojDC7BvPrnohOSKU8C+mUYjobtm4Bwrbxu28JSzuPQIG8Wem5ngYvVPqS7f5X+tXYdh3mnNCL9mXjvXMEOAIcgd+JQHYEEVWECRBuWLAaYYgSJYpLjkneKBBlVuhqYbllTV+IA/RzydtMskzfBERSlkOWOtpcHa2VaqBihTJo17YONqzohUcOMySEjm3CPpojlJAWImkPyca4cWYiEXpJSr9I08eoz4tHxxKhlybSroBSlEK6ZAmyscP04zWwPmrKlf94bkg3QaDHQkl78esLeK0bSUKfme821NCxRn8T+wLXMbtuUDs5SEmXJFwasuWqQOiG59zRde1FYd3AzaQxodLJ8AKm7JekrFZdRVoFKp2NLhS4T3aevTZewh5H7wI9StyGXiC4+M4cAY4AR6BoEciOJFVy6CKqbPnzNchjkoQcY0n1nbBA+Ds7eLawvHRymLCUSOZLcol9OUnRi4W/9xh3JTInco6TEOyG5e3x6LIGpCitssmKdlil1x49O9VG2tuZwvajBwagjbIcEXoJImKyy9N5XjwyEJUqUhvxC0lVr4cOrWuimkwZQCzZbr2rN2pWL0t9zBfacL49Fl061JGcSwxpJQpwrQhdQIQ+I9+NqqR9BO1XXyhw7W9yVWinie4pKOmfhSrZ4hNS0qF34hFaLjsrbFMzlBC7ot5paGxnAwCgwcKTwlJujjXa/UK/rVfaYi8n9KJ92XjvHAGOAEfgdyKQQxJ6zvvxVEcVqEY+Hwr1ATWAMHJoC9fEkN5ySHutjo2rlHB4SxuE+Gtg1KCaRLjjMayvHHp3kUHfrrIY0keOSH4yBvWsjlPmalg8ozEMFzeHn1N/TFCvI7R3fFc7rFvSDOkBGtAcWhsXTnTFc+dBmDK6LhA8WjhPd/temKYpDwSNIql9LPp2kSUtgNbHa3A61x2zJ9YX2hs5sCa2bWyFiIBR0BhA5yQeU6BrzXk/lgh9ar7bUGKKBRoT4Ra0tlpli+svgiBPKnt2bKMlNrB59BrDibjrEmnf9AyGwtIzuOsTiloLTqCjkR3sn75DA10bjDW/jQd+YiiQ/b6g/dajAcQ+Tui/81XibXMEOAIcgaJFICdiFnLe9qHa/X9aEdKDJGppuF9WxbLZdf+nfRf8WnsRoU/Od6NEEw6gos6xv6aWJsl+/x2aTliAwlXuBQCL78oR4AhwBIoagewIUrMHKFNt8b+vb1sizE0ByS+b/e/7LtD1KhGhT8hP6Jrk4Dfj8N9Tp1niACf0on7deP8cAY4AR+D3IZATORc5r2WpyvD6TQwIH/G4/IQ+yhwirUN/T514kDvF/b7XiLfMEeAIcASKHoGc6PXI9iOHNn9ev4dBDnnof1oU9WxQb/7xv6bWnXcMgeEU4rcAhavcCwAW35UjwBHgCHAEOAJ/KgKc0P/UO8PPiyPAEeAIcAQ4AgVAgBN6AcDiu3IEOAIcAY4AR+BPRYAT+p96Z/h5cQQ4AhwBjgBHoAAIcEIvAFh8V44AR4AjwBHgCPypCHBC/1PvDD8vjgBHgCPAEeAIFAABTugFAIvvyhHgCHAEOAIcgT8VAU7of+qd4efFEeAIcAQ4AhyBAiDACb0AYPFdOQIcAY4AR4Aj8KciwAn9T70z/Lw4AhwBjgBHgCNQAAQ4oRcALL4rR4AjwBHgCHAE/lQEOKH/qXeGnxdHgCPAEeAIcAQKgAAn9AKAxXflCHAEOAIcAY7An4oAJ/Q/9c7w8+IIcAQ4AhwBjkABEPgjCD0jIwO8cgz4M8CfAf4M8GeAPwP/7RkoAP9/d1dRYTXE2+EIcAQ4AhwBjgBHoOgQ4IRedNjznjkCHAGOAEeAI1BoCHBCLzQoeUMcAY4AR4AjwBEoOgQ4oRcd9rxnjgBHgCPAEeAIFBoCnNALDUreEEeAI8AR4AhwBIoOAU7oRYc975kjwBHgCHAEOAKFhgAn9EKDkjfEEeAIcAQ4AhyBokOAE3rRYc975ghwBDgCHAGOQKEhwAm90KDkDXEEOAIcAY4AR6DoEOCEXnTY8545AhwBjgBHgCNQaAhwQi80KHlDHAGOAEeAI8ARKDoEOKEXHfa8Z44AR4AjwBHgCBQaApzQCw1K3hBHgCPAEeAIcASKDgFO6EWHPe+ZI8AR4AhwBDgChYbA/wHvb3IIJt6XQwAAAABJRU5ErkJggg==';
//    		$mdDialog.show({
//		      controller: ImageController,
//		      templateUrl: ctx +'/templates/views/payment/modal-image.html',
//		      parent: angular.element(document.body),
//		      targetEvent: ev,
//		      clickOutsideToClose:true,
//		      locals:{
//		    	  imageObj : imageObj
//		      }
//		    }).then(function(answer) {
//		    	console.log(answer);
//		    }, function() {
//		    	console.log('closed');
//		    });	
    	}
    	if(id == 2){
    		dataFactory.getQRCode($scope.invoice.id).then(function (response){
    			$log.log('response : ',response);
    			var imageObj = response;
    			$mdDialog.show({
    			      controller: ImageController,
    			      templateUrl: ctx +'/templates/views/payment/modal-image.html',
    			      parent: angular.element(document.body),
    			      targetEvent: ev,
    			      clickOutsideToClose:true,
    			      locals:{
    			    	  imageObj : imageObj
    			      }
    			    }).then(function(answer) {
    			    	console.log(answer);
    			    }, function() {
    			    	console.log('closed');
    			    });
    		}).catch(function(response) {
    			console.error('data error', response);
    		})
    		.finally(function() {
    			console.log("finally finished");
    		});
    	}
    	if(id == 3){
    		dataFactory.getBarcode($scope.invoice.id).then(function (response){
    			$log.log('response : ',response);
    			var imageObj = response;
    			$mdDialog.show({
    			      controller: ImageController,
    			      templateUrl: ctx +'/templates/views/payment/modal-image.html',
    			      parent: angular.element(document.body),
    			      targetEvent: ev,
    			      clickOutsideToClose:true,
    			      locals:{
    			    	  imageObj : imageObj
    			      }
    			    }).then(function(answer) {
    			    	console.log(answer);
    			    }, function() {
    			    	console.log('closed');
    			    });
    		}).catch(function(response) {
    			console.error('data error', response);
    		})
    		.finally(function() {
    			console.log("finally finished");
    		});
    		
    	}
    	
    }
    function ImageController($scope, $mdDialog,imageObj) {
        console.log('ImageController');
        $scope.imageObj = imageObj;
    	$scope.hide = function() {
          $mdDialog.hide();
        };

        $scope.cancel = function() {
          $mdDialog.cancel();
        };

        $scope.submit = function(bean) {
          $mdDialog.hide(bean);
        };
        
        
      }
    
    
 
});


app.controller('paymentFavoriteCtrl',function($scope,$rootScope,$log,dataFactory,$timeout,$mdDialog,$location){
	$log.log("paymentFavoriteCtrl");
	
	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.favoriteData = [];
	
	$scope.currentPage = 1;
	
	
	//~ Service
	dataFactory.getFavoriteData($scope.favoriteSelectedList).then(function(response){
		$log.log('getFavoriteData');
		$log.log(response)
		$scope.favoriteData = response;
		$scope.figureOut();
	});
	
	 $scope.saveFavorite = function(){
		 dataFactory.saveFavorite($scope.favoriteSelectedList).then(function(response){
			 $log.log(response);
		 });
	 }
	
	
	
	
	$scope.values = [10,15,20,25];
	
	$scope.figureOut = function(){
		$scope.totalItems = $scope.favoriteData.length;
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.favoriteData.slice(begin, end);
	}
	
	
	
	$scope.pageChanged = function() {
		$scope.figureOut();
	 };
	 
	 $scope.gotoConfirmtab = function(){
		 if($scope.favoriteData.length != 0){
			 //~ Fecth cataligId to array
			 var templistCatId = []; 
			 $scope.favoriteData.forEach(function(element){
				 templistCatId.push(element.catalogId);
			 })
			 $rootScope.invoiceSelectedSet = new Set(templistCatId)
			 console.log($rootScope.invoiceSelectedSet,$rootScope.invoiceItemSelectedList = angular.copy($scope.favoriteData));
			 $rootScope.tab = 1;
			 $location.path('paymentConfirm').replace();
			 $rootScope.headerStep = $scope.headerList[1];
		 }
		 
	 }
	 
	 

	
});

app.controller('paymentSuccessCtrl',function($scope, $rootScope,$location,$log,$window){
	$log.log('paymentSuccessCtrl');
	 $rootScope.tab = 3;
	 $scope.gotoMain = function(){
		 $window.location.href = '../paymentController/Payment';
	 }
	 
});