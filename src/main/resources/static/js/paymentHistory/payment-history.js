var app = angular.module("paymentHistoryApp", ['httpProvider','ui.bootstrap','ngRoute','ngResource','ngMaterial','ngSanitize','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	  var urlBase = ctx+'/paymentHistoryController';
	    var dataFactory = {};
			dataFactory.searchBilling = function (data) {
		        return  httpProvider().post(urlBase+'/searchBilling',data);
		    };	
			dataFactory.getdeptList = function () {
				return  httpProvider().get(urlBase+'/getdeptList');
			};
			dataFactory.getCatalogType = function () {
				return  httpProvider().get(urlBase+'/getMCatalogType');
			};
			dataFactory.getCatalogByCatType = function (id) {
				return  httpProvider().get(urlBase+'/getCatalogByCatType/'+id);
			};
	    return dataFactory;
});

app.controller("paymentHistoryCtrl",function($scope,$rootScope,$log,$location, $mdDialog,dataFactory,$http){
	$log.log('paymentHistoryCtrl');
	$scope.billingList = [];
	$scope.data = {};
	$scope.data.invoice={};
	$scope.data.billing={};
	
	$scope.searchBilling = function(){
		 $log.log("data tor : ",$scope.data);
//		 $scope.data.invoice.invoiceNo = {};
			dataFactory.searchBilling($scope.data).then(function(response){
				$scope.billingList = response;
				$scope.totalItems = $scope.billingList.length;
				$scope.figureOut();
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
			
	}
	$scope.searchBilling();

	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.totalItems = $scope.billingList.length;
	$scope.currentPage = 1;
	
	$scope.values = [10,15,20,25];
	
	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.billingList.slice(begin, end);
	}
	
	
	
	$scope.pageChanged = function() {
		$scope.figureOut();
	  };
	  
		$log.log('Master : Department List');
		 dataFactory.getdeptList().then(function (response) {
			$scope.deptList = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	  
		dataFactory.getCatalogType().then(function (response) {
			$rootScope.getCatalogTypeList = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
		
		$scope.getCatalogByCatType = function(id){
			console.log(id);
			if(eval(id)==null){
				id = 0;
			}
			dataFactory.getCatalogByCatType(id).then(function (response) {
				$rootScope.getCatalogList = response;
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
		}
		
		
		  $scope.reportInvoice = function(invoiceId){
		    
		    		
		    		$http.get(ctx+'/manageInvoice'+'/getInvoiceRpt/'+invoiceId,{responseType: 'arraybuffer'}).then(function (response) {
	    				console.log(response.data);
	    				 var file = new Blob([response.data], {type: 'application/pdf'});
	    				 var fileURL = URL.createObjectURL(file);
	    				 window.open(fileURL);
	    				 console.log("seccess");
	    			}, function (error) {
	    				$scope.status = 'error: ' + error.message;
	    			});
		    	
		  }
		

})