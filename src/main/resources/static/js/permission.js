$(document).ready(function() {
	$('input[type=checkbox][name=checkPermissionAll]').on('change', function() {
		if ($(this).attr('parentOf') == undefined) {
			checkPermissionTopic($(this).val(), $(this).prop('checked'));
		}
		checkPermissionDetail($(this).val(), $(this).prop('checked'));
		
		if ($(this).attr('child')) {
			checkPermissionMother($(this).attr('parentOf'));
		}
	});
	
	$('input[type=checkbox][name=permissionItems]').on('change', function() {
		checkPermissionMother($(this).attr('parentOf'));
	});
	
	$('input[type=checkbox][name=permissionItems][checked=checked]').each(function() {
		$(this).trigger('change');
	});
	
	$('input[type=checkbox][name=checkAll]').on('change', function() {
		$('#permissionGroupForm input[type=checkbox]').prop('checked', $(this).prop('checked'));
	});
	
	$('input[type=checkbox]').not('input[name=checkAll]').on('change', function() {
		checkAll();
	});
	
});

function checkAll() {
	var check = true;
	$('input[type=checkbox]').not('input[name=checkAll]').each(function() {
		if (!$(this).prop('checked')) {
			check = false;
			return false;
		}
	});
	$('input[type=checkbox][name=checkAll]').prop('checked', check);
}

function checkPermissionMother(id) {
	var check = true;
	var disable = true;
	$('input[type=checkbox][parentOf=' + id + ']').each(function() {
		if (!$(this).prop('checked')) {
			check = false;
			return false;
		}
	});
	
	$('input[type=checkbox][parentOf=' + id + ']').each(function() {
		if (!$(this).prop('disabled')) {
			disable = false;
			return false;
		}
	});
	
	$('input[type=checkbox][value=' + id + ']').prop('checked', check).prop('disabled', disable);
	checkPermissionAll($('input[type=checkbox][value=' + id + ']').attr('parentOf'));
}

function checkPermissionAll(id) {
	if (id != undefined) {
		var check = true;
		var disable = true;
		$('input[type=checkbox][parentOf=' + id + ']').each(function() {
			if (!$(this).prop('checked')) {
				check = false;
				return false;
			}
		});
		
		$('input[type=checkbox][parentOf=' + id + ']').each(function() {
			if (!$(this).prop('disabled')) {
				disable = false;
				return false;
			}
		});
		
		$('input[type=checkbox][value=' + id + ']').prop('checked', check).prop('disabled', disable);
	}
}

function checkPermissionTopic(id, check) {
	$('input[type=checkbox][name=checkPermissionAll][parentOf=' + id +']').each(function() {
		if(!$(this).prop('disabled')) {
			$(this).prop('checked', check);
			$(this).trigger('change');
		}
	});
}

function checkPermissionDetail(id, check) {
	$('input[type=checkbox][name=permissionItems][parentOf=' + id +']').each(function() {
		if(!$(this).prop('disabled')) {
			$(this).prop('checked', check);
		}
	});
}