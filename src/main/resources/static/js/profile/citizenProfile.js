var app = angular.module("profileCitizenApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider) {
	var urlBase = ctx+'/user';
//alert(urlBase);
	var dataFactory = {};

	dataFactory.saveUser = function (userBean) {
	    return  httpProvider().post(urlBase+'/profile/save',userBean);
	};
	
	dataFactory.getProfile = function () {
        return httpProvider().get(urlBase+'/profileData');
    };
	
	return dataFactory;
});

app.controller("profileCitizenCtrl", function ($log,$scope, dataFactory) {
	$scope.userBean = {};
	console.log($scope.userBean);
	$scope.saveUser = function () {alert('IN /user/profile/save');
		console.log($scope.userBean);
		dataFactory.saveUser($scope.userBean).then(function (response) {
			console.log(response);
			$scope.data4List = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	
	//$scope.getProfile = function () { 
		dataFactory.getProfile().then(function (response) {
			console.log(response);
			$scope.userBean = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	//};
});