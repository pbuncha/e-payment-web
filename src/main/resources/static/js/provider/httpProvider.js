

angular.module('httpProvider', [])
.factory('httpProvider',[ '$http', '$location', '$rootScope', function($http, $location, $rootScope) {
    return function() {
        var Resource = function(data) {
            angular.extend(this, data);
        };
        Resource.post = function(execURL,params) {
            return  $http.post(execURL, params)
            .then(function(response) {
                return response.data;
           });
        };


        Resource.get = function(execURL, params) {
            return  $http.get( execURL, {params: params} )
            .then(function(response) {
                return response.data;
            });
        };
        
        Resource.put = function(execURL, params) {
            return  $http.put(execURL, params)
            .then(function(response) {
                return response.data;
            });
        };
        
        Resource.delete = function(execURL, params) {
        	return  $http.delete( execURL, {params: params} )
            .then(function(response) {
                return response.data;
            });
        };



        return Resource;
    }
}]);




