var app = angular.module("receiptPrePrintApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider,$http) {
	var urlBase = ctx+'/receiptController';
	var dataFactory = {
			'urlBase':urlBase
	};

	dataFactory.getBillingRpt = function(data){
		return $http.post(urlBase+'/getBillRpt/',data,{responseType: 'arraybuffer'});
	};

	return dataFactory;
});

app.controller("receiptPrePrinCtrl", function ($log,$scope,dataFactory,$window, $sce, $http) {
	$log.log('receiptPrePrinCtrl');
	$scope.urlBase = dataFactory['urlBase'];

	if(billing != null && billing != ""){
		$scope.data = billing;
		$scope.itemList = $scope.data.invoice.items;
		$scope.data.isPreview = "0";
		$scope.data.isCopy = $scope.data.status;
		
		getPdfPriviewBillingRpt();
	}
	
	
	function getPdfPriviewBillingRpt(){
		$scope.data.isPreview = "1";
		dataFactory.getBillingRpt($scope.data).then(function (response) {
			$scope.file = "data:application/pdf;base64,"+arrayBufferToBase64(response.data);
		}, function (error) {

		});
	}
	
	
	$scope.trustSrc = function(src) {
		return $sce.trustAsResourceUrl(src);
	}
	function arrayBufferToBase64( buffer ) {
		var binary = '';
		var bytes = new Uint8Array( buffer );
		var len = bytes.byteLength;
		for (var i = 0; i < len; i++) {
			binary += String.fromCharCode( bytes[ i ] );
		}
		return window.btoa( binary );
	}

//	$scope.print = function(data){
//		data.isCopy = data.status;
//		data.printType = "P";
//		dataFactory.getBillingRpt(data).then(function (response) {
//			console.log(response.data);
//			var file = new Blob([response.data], {type: 'application/pdf'});
//			var fileURL = URL.createObjectURL(file);
//			window.open(fileURL);
//			console.log("seccess");
//		}, function (error) {
//			$scope.status = 'error: ' + error.message;
//		});
//	}

	$scope.cancel = function() {
		$window.close();
	};

	
	/**
	 * Print original Receipt
	 */
	$scope.printPdfOrg = function( data ){
		$http.post($scope.urlBase+"/print/original", data).then(function(res){
			var obj = angular.fromJson(res['data']);
			printJS(ctx+"/temporary/pdf/"+obj['fileName']);
			$scope.data.status = obj['status'];
			$scope.data.isCopy = obj['status'];
			getPdfPriviewBillingRpt();
		},function(error){
			$log.error(error['data']);
		});
	}
});