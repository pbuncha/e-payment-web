var app = angular.module("receiptSearchGovApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp']);

app.factory("dataFactory", function (httpProvider,$http,reportFactory) {
	var urlBase = ctx+'/receiptController';
	var dataFactory = {};
	
	dataFactory.getDefaultData = function () {
		return  httpProvider().get(urlBase+'/getDefaultData');
	};
	
	dataFactory.getdeptList = function () {
		return  httpProvider().get(urlBase+'/getdeptList');
	};
	dataFactory.searchBilling = function (data) {
        return  httpProvider().post(urlBase+'/searchBilling',data);
    };	
    dataFactory.getMCostCenterList = function () {
		return  httpProvider().get(urlBase+'/getMCostCenterList');
	};
	dataFactory.getdeptList = function () {
		return  httpProvider().get(urlBase+'/getdeptList');
	};
	dataFactory.getMCatalogType = function () {
		return  httpProvider().get(urlBase+'/getMCatalogType');
	};
	dataFactory.exportReport = function (criteria){
		reportFactory.post(urlBase + '/exportExcel', criteria);
	}
	
	
	dataFactory.getDisbursementUnitList = function (departmentId) {
		return httpProvider().get(urlBase+'/getDisbursementUnitList/'+departmentId);
	};
	dataFactory.getCostCenterList = function (disbursementUnitId) {
		return httpProvider().get(urlBase+'/getCostCenterList/'+disbursementUnitId);
	};

	
	return dataFactory;
});

app.factory('reportFactory',function($http){
	function strToArraybuffer(str, fileType){
		var binary_string =  window.atob(str);
		var bytes = new Uint8Array( binary_string.length );
		for (var i = 0; i < binary_string.length; i++){
			bytes[i] = binary_string.charCodeAt(i);
		}
		var file = new Blob([bytes.buffer], {type: fileType});
		var fileURL = URL.createObjectURL(file);
		window.open(fileURL);
	}

	return {
		get: function(url, params ){
			$http.get(url, params ).then(function(data) {
				strToArraybuffer(data['data']['file'], data['data']['fileType']);
			});
		},
		post: function(url, params ){
			$http.post(url, params ).then(function(data) {
				strToArraybuffer(data['data']['file'], data['data']['fileType']);
			});
		},

	}
});

app.controller("receiptSearchGovCtrl", function ($log,$scope,dataFactory, $http) {
	$log.log('receiptSearchGovCtrl');
	$scope.data = {invoice:{},billing:{}};
	$scope.selectItem = [];
	
	$scope.data.billStartDate = new Date();
	
	//getDefaultData
	$log.log('getDefaultData');
	dataFactory.getDefaultData().then(function (response) {
		console.log(response);
		if(response.departmentId!=null){
			$scope.data.departmentId = response.departmentId+"";
			$scope.getDisbursementUnitList(response.departmentId);
		}
	});
	 
	$log.log('Master : Department List');
	dataFactory.getdeptList().then(function (response) {
		console.log(response);
		$scope.deptList = response;
	});

//	$log.log('Master : getMCostCenterList');
//	dataFactory.getMCostCenterList().then(function (response) {
//		console.log(response);
//		$scope.costCenterList = response;
//	});
	$log.log('Master : getMCatalogList');
	dataFactory.getMCatalogType().then(function (response) {
		$scope.mCatalogList = response;
	});
	
	$scope.getDisbursementUnitList = function (departmentId) {
		console.log("departmentId : "+departmentId);
		if(departmentId!=null){
			dataFactory.getDisbursementUnitList(departmentId).then(function (response) {
				console.log(response);
				$scope.disbursementUnitList = response;
			}, function (error) {
				
			});
		}else{
			$scope.disbursementUnitList = [];
		}
		
	};
	
	$scope.getCostCenterList = function (disbursementUnitId) {
		console.log("disbursementUnitId : "+disbursementUnitId);
		if(disbursementUnitId!=null){
			dataFactory.getCostCenterList(disbursementUnitId).then(function (response) {
				console.log(response);
				$scope.costCenterList = response;
			}, function (error) {
				
			});
		}else{
			$scope.costCenterList = [];
		}
		
	};

	$scope.billingList = [];
	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.totalItems = $scope.billingList.length;
	$scope.currentPage = 1;
	
	$scope.values = [10,15,20,25];
	
	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.billingList.slice(begin, end);
	}
	
	$scope.pageChanged = function() {
		$scope.figureOut();
	};
	
	$scope.searchBilling = function(){
		if($scope.data.statusUse==""){
			$scope.data.statusUse = null;
		}
		$log.log("data tor : ",$scope.data);
		dataFactory.searchBilling($scope.data).then(function(response){
			console.log(response);
			$scope.billingList = response;
			$scope.totalItems = $scope.billingList.length;
			$scope.figureOut();
				
		}, function (error) {
			
		});
	}
	//$scope.searchBilling();
	//orderByField
	$scope.orderByField = '';
	$scope.reverseSort = false;
	$scope.orderFunction = function(field) {
		$scope.orderByField = field;
		$scope.reverseSort = !$scope.reverseSort;		
	}
	$scope.checkField = function(name) {
		if ($scope.orderByField == name) {
			return true;
		} else {
			return false;
		}
	}
	
	$scope.clear = function(){
		$scope.billingList =[];
		$scope.data = {invoice:{},billing:{}};
//		$scope.invoiceCheckList = [];
		$scope.sliceFavoriteList = [];
		$scope.perPages = 10;
		$scope.totalItems = $scope.billingList.length;
		$scope.currentPage = 1;
		$scope.selectItem = [];
		$scope.isSelectAll = false;
	};
	
	$scope.preview = function(id){
		window.open(ctx+'/receiptController/preview/'+id);

	}
	
	$scope.selectToDel = function( item ){
		var index = $scope.isDelete(item.id);
		if(index > -1 ){
			$scope.selectItem.splice(index,1);
		}else{
			$scope.selectItem.push(item);
		}
	}

	$scope.isDelete = function( id ){
		for( var i =0 ; i<$scope.selectItem.length; i++ ){
			if( $scope.selectItem[i].billingId == id ){
				return i;
			}
		}
		return -1;
	}
	
	$scope.selectAll = function(isSelectAll){
		console.log(isSelectAll);
		if( isSelectAll ){
			$scope.selectItem = angular.copy($scope.sliceFavoriteList);
		}else{
			$scope.selectItem = [];
		}

	}
	
	$scope.exportExcel = function(dataList){
		if(dataList.length == 0){
			alert("กรุณาเลือกรายการ");
		}else{
			dataFactory.exportReport(dataList);
			$scope.selectItem = [];
			$scope.isSelectAll = false;
		}
	}
	
	$scope.download = function(dataList){
		if(dataList.length == 0){
			//alert("กรุณาเลือกรายการ");
			return;
		}else{
			var lists = [];
			console.log(dataList);
			angular.forEach(dataList, function( item){
				lists.push(item.billingId);
			});
			return ctx+'/receiptController/downloadZip/'+lists.join();
//			$http.get(ctx+'/receiptController/downloadZip/'+lists.join() )
//			.then(function(response){
//				console.log(response['data']);
//			},function(error){
//				
//			});
			

//			$http.get(ctx+'/manageInvoice'+'/getMultiInvoiceRpt/'+ids.join(),{responseType: 'arraybuffer'})
//			.then(function (response) {
//				console.log(response.data);
//				var file = new Blob([response.data], {type: 'application/pdf'});
//				var fileURL = URL.createObjectURL(file);
//				window.open(fileURL);
//				console.log("seccess");
//			}, function (error) {
//				$scope.status = 'error: ' + error.message;
//			});
		}
	}
});
