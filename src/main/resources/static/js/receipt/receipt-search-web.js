var app = angular.module("receiptSearchWebApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp','BotDetectCaptcha']);

app.factory("dataFactory", function (httpProvider,$http) {
	var urlBase = ctx+'/receiptController';
	var dataFactory = {};

	dataFactory.searchBilling = function (data) {
        return  httpProvider().post(urlBase+'/searchBilling',data);
    };	
    
    dataFactory.preview = function (id){
		window.location.href = urlBase+'/preview/'+id;
	}
    
	return dataFactory;
});

app.config(function(captchaSettingsProvider) {

	  captchaSettingsProvider.setSettings({
	    captchaEndpoint: ctx+'/botdetectcaptcha'
	  });
	});

app.controller("receiptSearchWebCtrl", function ($log,$scope,dataFactory) {
	$log.log('receiptSearchWebCtrl');
	$scope.data = {invoice:{},billing:{}};
	
	$scope.billingList = [];
	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.totalItems = $scope.billingList.length;
	$scope.currentPage = 1;
		
	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.billingList.slice(begin, end);
	}
	

	$scope.pageChanged = function() {
		$scope.figureOut();
	};
	$scope.searchBilling = function(){
		$log.log("data tor : ",$scope.data);
		dataFactory.searchBilling($scope.data).then(function(response){
			console.log(response);
			$scope.billingList = response;
			$scope.totalItems = $scope.billingList.length;
			$scope.figureOut();
				
		}, function (error) {
			
		});
	}
	
	//orderByField
	$scope.orderByField = '';
	$scope.reverseSort = false;
	$scope.orderFunction = function(field) {
		$scope.orderByField = field;
		$scope.reverseSort = !$scope.reverseSort;		
	}
	$scope.checkField = function(name) {
		if ($scope.orderByField == name) {
			return true;
		} else {
			return false;
		}
	}
	
	$scope.preview = function(id){
		
		dataFactory.preview(id).then(function (response) {
			
		}, function (error) {
			
		});

	}
	
});