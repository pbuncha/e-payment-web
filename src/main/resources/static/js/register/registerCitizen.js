var app = angular.module("registerCitizenApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','ngMaterial','angucomplete','ngSanitize','dualmultiselect','BotDetectCaptcha']);


app.config(function(captchaSettingsProvider,$compileProvider) {

  captchaSettingsProvider.setSettings({
    captchaEndpoint: ctx+'/botdetectcaptcha'
  });
  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|data):/);
});

app.directive('fileModel', ['$parse', function ($parse) {
            return {
               restrict: 'A',
               link: function(scope, element, attrs) {
                  var model = $parse(attrs.fileModel);
                  var modelSetter = model.assign;

                  element.bind('change', function(){
                     scope.$apply(function(){
                        modelSetter(scope, element[0].files[0]);
                     });
                  });
               }
            };
         }]);

app.service('fileUpload', ['$http', function ($http) {
            this.uploadFileToUrl = function(file, uploadUrl){
               var fd = new FormData();
               fd.append('file', file);

               return $http.post(uploadUrl, fd, {
                  transformRequest: angular.identity,
                  headers: {'Content-Type': undefined}
               });

            }}]);

app.factory("dataFactory", function (httpProvider) {
	var urlBase = ctx+'/registerController';
	var urlBaseChangePassword = ctx+'/changePassword';
	
//alert(urlBase);
	var dataFactory = {};
	
	dataFactory.upload = function (file) {
        return  httpProvider().post(urlBase+'/upload',file);
    };

	dataFactory.saveUser = function (userBean,reCaptcha) {
	    return  httpProvider().post(urlBase+'/saveUser/'+reCaptcha,userBean);
	};
	
	dataFactory.inquiryByID = function () {
        return httpProvider().get(urlBase+'/register/inquiryByID');
    };
	
	dataFactory.getOrganizationType = function () {
        return httpProvider().get(urlBase+'/register/getOrganizationType');
    };
	
	dataFactory.getOrganizationSubType = function (id) {
        return httpProvider().get(urlBase+'/register/getOrganizationSubType/'+id);
    };
	
	dataFactory.getDepartment = function (id) {
        return httpProvider().get(urlBase+'/register/getDepartment/'+id);
    };

    dataFactory.getDepAddr = function (id) {
        return httpProvider().get(urlBase+'/register/getDepAddr/'+id);
    };

    dataFactory.getAllDepartment = function (id) {
        return httpProvider().get(urlBase+'/register/getDepartment');
    };
	
	dataFactory.getBusinessArea = function (id) {
        return httpProvider().get(urlBase+'/register/getBusinessArea/'+id);
    };
	
	dataFactory.getCostCenter = function (id1,id2) {
        return httpProvider().get(urlBase+'/register/getCostCenter/'+id1+'/'+id2);
    };
	
	dataFactory.getProvince = function () {
        return httpProvider().get(urlBase+'/register/getProvince/');
    };
	
	dataFactory.getAmphur = function (id) {
        return httpProvider().get(urlBase+'/register/getAmphur/'+id);
    };
	
	dataFactory.getTambon = function (id) {
        return httpProvider().get(urlBase+'/register/getTambon/'+id);
    };
    
	dataFactory.getUserCitizen = function (id) {
        return httpProvider().get(urlBase+'/register/getUserCitizen/'+id);
    };
	dataFactory.getPermissionGroupAll = function () {
        return httpProvider().get(urlBase+'/register/getPermissionGroupAll/');
    };
	dataFactory.getAddressByDept = function (id) {
        return httpProvider().get(urlBase+'/register/getAddressByDept/'+id);
    };
	dataFactory.getTitleList = function () {
		return  httpProvider().get(urlBase+'/getTitleList');
	};
	dataFactory.getAge = function (date) {
		return  httpProvider().post(urlBase+'/register/getAge',date);
	};
	dataFactory.changePassword = function (userBean,reCaptcha) {
		return  httpProvider().post(urlBaseChangePassword+'/changePasswordUser/'+reCaptcha,userBean);
	};
    
    
	
	return dataFactory;
});
var scopeHolder;
app.controller("registerCtrl", function ($log,$scope, dataFactory,$mdDialog,Captcha,fileUpload) {
	  $scope.today = function() {
		    $scope.dt = new Date();
		  };
		 // $scope.today();

		  $scope.clear = function() {
		    $scope.dt = null;
		  };

		  $scope.inlineOptions = {
		    customClass: getDayClass,
		    minDate: new Date(),
		    showWeeks: true
		  };

		  $scope.dateOptions = {
		  formatYear: 'yy',
		  minDate: new Date(2017, 0, 5),
		  showButtonBar: false,
		  showWeeks:false,
		    startingDay: 1
		  };

		  // Disable weekend selection
		  function disabled(data) {
		    var date = data.date,
		      mode = data.mode;
		    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
		  }

		  $scope.toggleMin = function() {
		    $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
		    $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
		  };

		  $scope.toggleMin();

		  $scope.open1 = function() {
		    $scope.popup1.opened = true;
		  };

		  $scope.open2 = function() {
		    $scope.popup2.opened = true;
		  };

		  $scope.setDate = function(year, month, day) {
		    $scope.dt = new Date(year, month, day);
		  };
		  
		 $scope.format = $scope.formatDate;
		 $scope.altInputFormats = ['M!/d!/yyyy'];

		  $scope.popup1 = {
		    opened: false
		  };

		  $scope.popup2 = {
		    opened: false
		  };

		  var tomorrow = new Date();
		  tomorrow.setDate(tomorrow.getDate() + 1);
		  var afterTomorrow = new Date();
		  afterTomorrow.setDate(tomorrow.getDate() + 1);
		  $scope.events = [
		    {
		      date: tomorrow,
		      status: 'full'
		    },
		    {
		      date: afterTomorrow,
		      status: 'partially'
		    }
		  ];

		  function getDayClass(data) {
		    var date = data.date,
		      mode = data.mode;
		  
		    if (mode === 'day') {
		      var dayToCheck = new Date(date).setHours(0,0,0,0);

		      for (var i = 0; i < $scope.events.length; i++) {
		        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

		        if (dayToCheck === currentDay) {
		          return $scope.events[i].status;
		        }
		      }
		    }

		    return '';
		  }
      	
	
		  
	
	
	$scope.userBean = {};
	$scope.gov = {};
	$scope.userBean.verifiedType = '2';
	scopeHolder = $scope;
    $scope.data = {};
    var chars = []
	$scope.isSubmit = false;
    $scope.changePassword = function () {
	    // create new BotDetect Captcha instance
        var captcha = new Captcha();

        // captcha id for validating captcha at server-side
        var grecaptcha = captcha.captchaId;

        // captcha code input value for validating captcha at server-side
        var captchaCode = $scope.userBean.captcha;

        var postData = {
          captchaId: grecaptcha,
          captchaCode: captchaCode
        };

		var b = (typeof $scope.userBean.password === 'undefined');

		if(b)
		{
			showAlert('กรุณาใส่รหัสผ่าน');
			return;
		}
    	 if($scope.userBean.password != $scope.userBean.cfpwd){
             showAlert('ยืนยันรหัสผ่านต้องเหมือนกับรหัสผ่าน');
             return;
         }
    	 dataFactory.changePassword($scope.userBean,grecaptcha).then(function (response) {
 			console.log(response);

			if(response == 'success'){
			    window.location = ctx;
			}else{
			    showAlert(response);
			}

 		}).catch(function(response) {
 			console.error('data error : ', response);
 			 showAlert(response);
 		})
 		.finally(function() {
 			console.log("finally finished");
 		});
    }
    
	$scope.getUserCitizen = function () {
		console.log($scope.userBean.citizenid);
		dataFactory.getUserCitizen($scope.userBean.citizenid).then(function (response) {
			console.log(response);
			if(response){
			    $scope.userBean = response;
			    $scope.getAmphur($scope.userBean.provinceId);
			    $scope.getTambon($scope.userBean.amphurId);
			    $scope.userBean.verifiedType = "2"
			    
			}else{
			    showAlert('ไม่พบข้อมูลของเลขบัตรประชาชนดังกล่าว');
			}

		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};

	$scope.onlyDigits = function(parent,childname,input){
	    if(input){
	        var name = input.replace(/[^0-9 ]/g, "");
            if(parent){
                parent[childname] = name;
            }else{
                $scope[childname] = nama;
            }
	    }


	}

	  
      $scope.$watch( 'userBean.birthDate', function(n,o){
    	
    		var file_input = document.querySelector("#birthDate");
    	  if(n != undefined){
    	
    			file_input.setCustomValidity("");
	  		dataFactory.getAge(n).then(function (response) {
	  			$scope.userBean.age = response;
	  			
			}).catch(function(response) {
				console.error('data error', response);
			})
			.finally(function() {
				console.log("finally finished");
			});
    	  }else{
    			file_input.setCustomValidity("กรุณากรอก วัน/เดือน/ปี เกิด");
    	  }
    })
    
    
    dataFactory.getPermissionGroupAll().then(function (response) {
    	$scope.permissionGroupList = response;
		$scope.permissionGroupOptions = {
			orderProperty: 'desc',
			items: $scope.permissionGroupList,
			selectedItems: []
		};	
    }).catch(function(response) {
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });


    $scope.checkTypeFile = function (file) {
    	   var typeFile = file.split('.');
    	   var type = ['pdf', 'docx', 'dox']
    	      if(!type.includes(typeFile[1])){
    	    	  showAlert("ประเภทของไฟล์ไม่ถูกต้อง");
    	    	  return true;
    	      }
    	
    }
    var checkFilesize = false;
	$scope.upload = function(){
        var file = $scope.data.ufile;
       if(file){
            var type = ['pdf', 'docx', 'dox']
           if(type.includes(file.name.split('.')[1])){
        	  
                var urlBase = ctx+'/registerController';
                var size = file.size;
                size = size / 1048576;
                if(size>2){
                	  showAlert('ขนาดเกิน 2 MB');
                	  return true;
                }
           
               fileUpload.uploadFileToUrl(file, urlBase+'/upload')
               .then(function(response) {
                   $scope.userBean.file = response.data.filename;
                   $scope.userBean.filePath = response.data.path;
                  
               }).catch(function(error){
                      showAlert(error)
                      $scope.data.uploadErr = error.data;
              }).finally(function() {
                    console.log("finally finished");
                });
           }else{
            showAlert('ประเภทของไฟล์ไม่ถูกต้อง');
            $scope.data.uploadErr = 'File type not support.';
           }
       }else{
        showAlert('กรุณาเลือกไฟล์');
       }

	}

	//alert($scope.userBean.verifiedType);
	$scope.saveUserCitizen = function () {
//	    alert('IN');
        if(!checkID($scope.userBean.citizenid) ){
            showAlert('เลขประจำตัวประชาชนไม่ถูกต้อง');
            return;
        }
        if(!$scope.userBean.birthDate){
            showAlert('กรุณาระบุวันเกิด');
            return;
        }

        if($scope.userBean.password != $scope.userBean.cfpwd){
            showAlert('ยืนยันรหัสผ่านต้องเหมือนกับรหัสผ่าน');
            return;
        }
		$scope.userBean.userType = 1;
		console.log($scope.userBean);
		doPost();


	};
	$scope.saveUserCorp = function () {
//	    alert('IN');
		$scope.userBean.userType = 2;
		if(!checkID($scope.userBean.company_auth_id) ){
            showAlert('เลขประจำตัวประชาชนผู้มีอำนาจลงนามไม่ถูกต้อง');
            return;
        }
        if(!$scope.userBean.company_auth_birth_date){
            showAlert('กรุณาระบุวันเกิด');
            return;
        }

        if(!$scope.userBean.file){
            showAlert('กรุณาอัพโหลดไฟล์เอกสาร');
            return;
        }

        if($scope.userBean.password != $scope.userBean.cfpwd){
            showAlert('ยืนยันรหัสผ่านต้องเหมือนกับรหัสผ่าน');
            return;
        }
		console.log($scope.userBean);
		if(!$scope.data.accept){
		    showAlert('กรุณายอมรับข้อตกลง');
		    return;
		}

		//return;
		doPost();
	};
	

	dataFactory.getTitleList().then(function (response) {
		$scope.titleList = response;
	});
	$scope.regexNumber = /^\d+$/;

	$scope.saveUserGoverment = function () {

		$scope.isSubmit = true;

		$scope.userBean.userType = 3;
		$scope.userBean.permissionGroup = [];

		if($scope.upload()){
			 return;
		}
		 if(!checkID($scope.userBean.citizenid) ){
	            showAlert('เลขประจำตัวประชาชนไม่ถูกต้อง');
	           
	        }
		angular.forEach($scope.permissionGroupOptions.selectedItems, function(value, key) {
			console.log("value : ",value.id);
			  $scope.userBean.permissionGroup.push(value.id);
//				$scope.selectedItems.push(value.id);
		});
		
		  if($scope.userBean.permissionGroup.length == 0){
	            showAlert('กรุณาเลือก สิทธิ์ในการเข้าใช้งานระบบ');
	            return;
	        }
		console.log("permissionGroup : ",$scope.userBean.permissionGroup);
		console.log("value : ",$scope.userBean.accept);
	
		if(!$scope.userBean.accept){
		    showAlert('กรุณายอมรับเงื่อนไขในการลงทะเบียน');
		    return;
		}
	
		doPost();
	};
	
	//console.log($scope.userBean);
	$scope.inquiryByID = function () {
		console.log($scope.userBean);
		dataFactory.inquiryByID($scope.userBean.citizenid).then(function (response) {
			console.log(response);
			$scope.userBean = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	
	function doPost(){
	    // create new BotDetect Captcha instance
        var captcha = new Captcha();

        // captcha id for validating captcha at server-side
        var grecaptcha = captcha.captchaId;

        // captcha code input value for validating captcha at server-side
        var captchaCode = $scope.userBean.captcha;

        var postData = {
          captchaId: grecaptcha,
          captchaCode: captchaCode
        };
		var b = (typeof $scope.userBean.password === 'undefined');

		if(b)
		{
			showAlert('กรุณาใส่รหัสผ่าน');
			return;
		}
		if($scope.userBean.password != $scope.userBean.cfpwd)
		{
			//alert('ยืนยันรหัสผ่านไม่ตรงกับรหัสผ่าน');
			showAlert('ยืนยันรหัสผ่านไม่ตรงกับรหัสผ่าน');
			return;
		}
		 if($scope.userBean.email == undefined || $scope.userBean.email==""){
             showAlert('email invalid format');
             return;
         }


		dataFactory.saveUser($scope.userBean,grecaptcha).then(function (response) {
			console.log(response);
			if(response == 'success'){
				 var successDialog;
				 successDialog = $mdDialog.alert({
				        title: 'ลงทะเบียนสำเร็จ',
				        textContent: 'ระบบได้ส่งลิงค์การยืนยันตัวตนไปยังอีเมลที่ท่านได้ลงทะเบียนไว้ กรุณายืนยันตัวตนของท่าน ก่อนเข้าใช้ระบบ',
				        ok: 'ตกลง'
				      });

				      $mdDialog
				        .show( successDialog )
				        .finally(function() {
				        	successDialog = undefined;
				        	  window.location = ctx;
				        });
			  
			}else{
			    showAlert(response);
			}

		}).catch(function(response) {
			console.log(response);
			showAlert(response)
		})
		.finally(function() {
			console.log("finally finished");
		});
	}
	
	function showAlert (textContent) {
    	$mdDialog.show(
    		      $mdDialog.alert()
    		        .clickOutsideToClose(true)
    		        .title('ข้อผิดพลาด')
    		        .textContent(textContent)
    		        .ok('OK')
    		    );
    }


    
    dataFactory.getAllDepartment().then(function (response) {
    	$scope.departmentList = response;
    }).catch(function(response) {
        console.error('data error', response);
    })
    .finally(function() {
        console.log("finally finished");
    });
    
   
	
	dataFactory.getOrganizationType().then(function (response) {
		$scope.data1List = response;
		$scope.userBean.orgType = {};
		$scope.userBean.orgType.id = "1";
		$scope.getOrganizationSubType($scope.userBean.orgType.id);
	}).catch(function(response) {
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally finished");
	});
	$scope.getBusinessArea = function(id){
		if(undefined == id){
			id = 0;
		}
	    dataFactory.getBusinessArea(id).then(function (response) {
    		$scope.dataBizArea = response;
    	}).catch(function(response) {
    		console.error('data error', response);
    	})
    	.finally(function() {
    		console.log("finally finished");
    	});
	    

	}

	
	$scope.getOrganizationSubType = function (id) {
		if(undefined == id){
			id = 0;
		}
		dataFactory.getOrganizationSubType(id).then(function (response) {
			$scope.dataOrgSub = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	
	$scope.getDepartment = function (id) {
		if(undefined == id){
			id = 0;
		}
		dataFactory.getDepartment(id).then(function (response) {
			$scope.dataDept = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});

		
	};
	$scope.deptid = '';
	$scope.setDeptId = function (id){
		$scope.deptid = id;
		if ($scope.bizareaid != '') $scope.getCostCenter();
	};
	$scope.bizareaid = '';
	$scope.setBizAreaId = function (id){
		$scope.bizareaid = id;
		if ($scope.deptid != '') $scope.getCostCenter();
	};
	
	$scope.getCostCenter = function (depId,bizId) {
		if(undefined == depId){
			depId = 0;
		}
		if(undefined == bizId){
			bizId = 0;
		}
		dataFactory.getCostCenter(depId,bizId).then(function (response) {
			$scope.dataCostCenter = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};	
	
	dataFactory.getProvince().then(function (response) {
		$scope.dataProvince = response;
	}).catch(function(response) {
		console.error('data error', response);
	})
	.finally(function() {
		console.log("finally finished");
	});
	

	$scope.getAmphur = function (id) {
		if(undefined==id){
			id = 0;
		}
		dataFactory.getAmphur(id).then(function (response) {
			$scope.dataAmphur = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	
	$scope.getTambon = function (id) {
		if(undefined==id){
			id = 0;
		}
		dataFactory.getTambon(id).then(function (response) {
			$scope.dataTambon = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	$scope.getAmphurGovAddre = function (id) {
		if(undefined==id){
			id = 0;
		}
		dataFactory.getAmphur(id).then(function (response) {
			$scope.dataAmphurGov = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};
	
	$scope.getTambonGovAddre = function (id) {
		if(undefined==id){
			id = 0;
		}
		dataFactory.getTambon(id).then(function (response) {
			$scope.dataTambonGov = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};


    function checkID(id)
    {
        if(id.length != 13) return false;
        for(i=0, sum=0; i < 12; i++)
        sum += parseFloat(id.charAt(i))*(13-i); 
        if((11-sum%11)%10!=parseFloat(id.charAt(12)))
        return false; return true;
    }
    
    

});

function checkEmail() {
	var email_input = document.querySelector("#email");
	
	var $email = $('form input[name="email'); //change form to id or containment selector
	var re = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;
	if ($email.val() == '' || !re.test($email.val()))
	{
		email_input.setCustomValidity("โปรดป้อนอีเมลที่ถูกต้อง");
	    return false;
	}else{
		email_input.setCustomValidity("");
	}


}

function checkMobile() {
	var mobile_input = document.querySelector("#mobile");
	
	var $email = $('form input[name="mobile'); //change form to id or containment selector
	var re = /^[0-9]{10}/;
	if ($email.val() == '' || !re.test($email.val()))
	{
		mobile_input.setCustomValidity("เบอร์โทรศัพท์ ไม่ถูกต้อง");
	    return false;
	}else{
		mobile_input.setCustomValidity("");
	}


}

function checkPostCode() {
	var postCode_input = document.querySelector("#postCode");
	
	var $postCode = $('form input[name="postCode'); //change form to id or containment selector
	var re = /^[0-9]{5}/;
	if ($postCode.val() == '' || !re.test($postCode.val()))
	{
		postCode_input.setCustomValidity("กรุณากรอก รหัสไปรษณีย์");
	    return false;
	}else{
		postCode_input.setCustomValidity("");
	}

}

function checkPatternNumber(field,patten,message) {
	
	var field_input = document.querySelector("#"+field);
	
	var $field_data = $('form input[name="'+field); //change form to id or containment selector
//	$field_data.val().replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');
	$( '#'+field ).val($field_data.val().replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1'))
	var re = patten;
	if ($field_data.val() == '' || !re.test($field_data.val())){
		field_input.setCustomValidity(message);
		  return false;

	}else{
		field_input.setCustomValidity("");
	}


}
function checkPassword() {
	var pwd_input = document.querySelector("#pwd");

	var $pwd = $('form input[name="pwd'); //change form to id or containment selector
	var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9]+).{10,}/;
	if ($pwd.val() == '' || !re.test($pwd.val())){
		
		pwd_input.setCustomValidity("รหัสผ่านไม่ถูกต้อง");
	    return false;
	}else{
		var fname1 = $('#fnameEN' ).val();
		var fname1Temp = fname1.substr(0, 3);
		var fname2Temp = fname1.substr(fname1.length-3, fname1.length);
		var pass1 = $pwd.val().toLowerCase().search(fname1Temp.toLowerCase());
		var pass2 = $pwd.val().toLowerCase().search(fname2Temp.toLowerCase());
		
		
		var lname1 = $('#lnameEN' ).val();
		var lname1Temp = lname1.substr(0, 3);
		var lname2Temp = lname1.substr(lname1.length-3, lname1.length);
		var pass3 = $pwd.val().toLowerCase().search(lname1Temp.toLowerCase());
		var pass4 = $pwd.val().toLowerCase().search(lname2Temp.toLowerCase());
		if((pass1== -1 && pass2 == -1 && pass3 == -1 && pass4 == -1) || (fname1 == '' || lname1 == '')  ){
			console.log("pass  : ");
			pwd_input.setCustomValidity("");
		}else{
			console.log("fali  : ");
			pwd_input.setCustomValidity("รหัสผ่านไม่ถูกต้อง");
		    return false;
		}
		
	}

}
function checkFile(chack) {
	var file_input = document.querySelector("#file");
	
	if(scopeHolder.checkTypeFile( $('#file' ).val())){
		  $('#file' ).val('');
		  scopeHolder.userBean.file="";
	}else{
		file_input.setCustomValidity("");
	}
		
}

function checkBirtday() {
	var file_input = document.querySelector("#birthDate");
	file_input.setCustomValidity("");
}


