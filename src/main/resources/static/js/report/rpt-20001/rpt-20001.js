(function() {
	
	var urlBase = ctx + '/reportController';
	
	
	
	angular.module("Rpt-20001", [ 'httpProvider', 'ui.bootstrap', 'ngSanitize',
			'datepickerApp' ]);
	

	angular.module("Rpt-20001")
	.config(function($sceDelegateProvider,$compileProvider) {

		  $sceDelegateProvider.resourceUrlWhitelist(['**']);
		  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|data):/);
		});

	angular.module("Rpt-20001")
	.factory("dataFactory", function(httpProvider, $reportFactory) {
		return {
			getMCatalogType : function() {
				return httpProvider().get(urlBase + '/getMCatalogType')
			},
			getDepartmentAll : function() {
				return httpProvider().get(urlBase + '/getDepartmentAll')
			},
			getMRevenueType : function(bean) {
				return httpProvider().get(urlBase + '/getMRevenueType');
			},
			exportReport: function(criterai){
				$reportFactory.post(urlBase + '/exportReport/rpt-20001', criterai);
			}
			
		}
	});
	
	angular.module("Rpt-20001")
	.factory('$reportFactory',function($http){
		function strToArraybuffer(str, fileType){
			var binary_string =  window.atob(str);
		    var bytes = new Uint8Array( binary_string.length );
		    for (var i = 0; i < binary_string.length; i++)        {
		        bytes[i] = binary_string.charCodeAt(i);
		    }
			 var file = new Blob([bytes.buffer], {type: fileType});
			 var fileURL = URL.createObjectURL(file);
			 window.open(fileURL);
		}
		
		return {
				get: function(url, params ){
					$http.get(url, params ).then(function(data) {
						strToArraybuffer(data['data']['file'], data['data']['fileType']);
					});
				},
				post: function(url, params ){
					$http.post(url, params ).then(function(data) {
						strToArraybuffer(data['data']['file'], data['data']['fileType']);
					});
				},
				
		}
	});
	
	angular.module("Rpt-20001")
	.controller("rpt-20001-main",
		function($scope, $log, $location, dataFactory, $uibModal, $http) {
		
			$scope['criteria'] = { 'reportType' : 'PDF', 'catalogStartDate': new Date() };
			$scope['exportPdf'] = function( criteria ){
				dataFactory.exportReport(criteria);
			}
			
			dataFactory.getMCatalogType().then(function(data){
				$scope.catalogTypeList = data;
			});
			dataFactory.getDepartmentAll().then(function(data){
				$scope.departmentList = data;
			});
			dataFactory.getMRevenueType().then(function(data){
				$scope.revenueTypeList = data;
			});

			$scope.popup_D1 = {};			
			$scope.popup_D1.opened = false;
			$scope.dateOptions_D1 = {
			    datepickerMode: "day",
			    minMode: "day",
			    showWeeks: false
			};
			$scope.open_popup_D1 = function() {
			    $scope.popup_D1.opened = true;
			}	

			$scope.popup_D2 = {};
			$scope.popup_D2.opened = false;
			$scope.dateOptions_D2 = {
			    datepickerMode: "day",
			    minMode: "day",
			    showWeeks: false
			};
			$scope.open_popup_D2 = function() {
			    $scope.popup_D2.opened = true;
			}	

	
	
	});
})(angular);