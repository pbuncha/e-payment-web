(function() {
	
	var urlBase = ctx + '/reportController';
	
	
	
	angular.module("Rpt-30001", [ 'httpProvider', 'ui.bootstrap', 'ngSanitize',
			'datepickerApp' ]);
	

	angular.module("Rpt-30001")
	.config(function($sceDelegateProvider,$compileProvider) {

		  $sceDelegateProvider.resourceUrlWhitelist(['**']);
		  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|data):/);
		});

	angular.module("Rpt-30001")
	.factory("dataFactory", function(httpProvider, $reportFactory) {
		return {
			getMCatalogType : function() {
				return httpProvider().get(urlBase + '/getMCatalogType')
			},
			getMinistryAll : function() {
				return httpProvider().get(urlBase + '/getMinistryAll')
			},
			getDepartmentAll : function() {
				return httpProvider().get(urlBase + '/getDepartmentAll')
			},
			getDepartmentByMinistryId : function(id) {
				return httpProvider().get(urlBase + '/getDepartmentByMinistryId/'+id)
			},
			getMRevenueType : function(bean) {
				return httpProvider().get(urlBase + '/getMRevenueType');
			},
			exportReport: function(criterai){
				$reportFactory.post(urlBase + '/exportReport/rpt-30001', criterai);
			}
			
		}
	});
	
	angular.module("Rpt-30001")
	.factory('$reportFactory',function($http){
		function strToArraybuffer(str, fileType){
			var binary_string =  window.atob(str);
		    var bytes = new Uint8Array( binary_string.length );
		    for (var i = 0; i < binary_string.length; i++)        {
		        bytes[i] = binary_string.charCodeAt(i);
		    }
			 var file = new Blob([bytes.buffer], {type: fileType});
			 var fileURL = URL.createObjectURL(file);
			 window.open(fileURL);
		}
		
		return {
				get: function(url, params ){
					$http.get(url, params ).then(function(data) {
						strToArraybuffer(data['data']['file'], data['data']['fileType']);
					});
				},
				post: function(url, params ){
					$http.post(url, params ).then(function(data) {
						strToArraybuffer(data['data']['file'], data['data']['fileType']);
					});
				},
				
		}
	});
	
	angular.module("Rpt-30001")
	.controller("rpt-30001-main",
		function($scope, $log, $location, dataFactory, $uibModal, $http) {
		
			$scope['criteria'] = { 'reportType' : 'PDF', 'catalogStartDate': new Date() };
			$scope['exportPdf'] = function( criteria ){
				dataFactory.exportReport(criteria);
			}
			
			dataFactory.getMCatalogType().then(function(data){
				$scope.catalogTypeList = data;
			});
			dataFactory.getMinistryAll().then(function(data){
				$scope.ministryList = data;
			});
//			dataFactory.getDepartmentAll().then(function(data){
//				$scope.departmentList = data;
//			});
			dataFactory.getMRevenueType().then(function(data){
				$scope.revenueTypeList = data;
			});

			$scope.getDepartmentByMinistryId = function(id){
				console.log(id);
				if(eval(id)==null){
					id = 0;
				}
				dataFactory.getDepartmentByMinistryId(id).then(function (response) {
					$scope.departmentList = response;
				});
			}
						
			$scope.isOpened = false;
			$scope.dateOptions = {
			    datepickerMode: "year",
			    minMode: "year",
			    formatYear: 'yyyy'
			};
			$scope.opened = function() {
			    $scope.isOpened = true;
			}	
		});
})(angular);