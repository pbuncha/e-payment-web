(function() {
	
	var urlBase = ctx + '/reportController';
	
	
	
	angular.module("Rpt-70002", [ 'httpProvider', 'ui.bootstrap', 'ngSanitize',
			'datepickerApp' ]);
	

	angular.module("Rpt-70002")
	.config(function($sceDelegateProvider,$compileProvider) {

		  $sceDelegateProvider.resourceUrlWhitelist(['**']);
		  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|data):/);
		});

	angular.module("Rpt-70002")
	.factory("dataFactory", function(httpProvider, $reportFactory) {
		return {
			getMCatalogType : function() {
				return httpProvider().get(urlBase + '/getMCatalogType')
			},
			getDepartmentAll : function() {
				return httpProvider().get(urlBase + '/getDepartmentAll')
			},
			getMRevenueType : function(bean) {
				return httpProvider().get(urlBase + '/getMRevenueType');
			},
			getCatalogList : function(bean) {
				return httpProvider().get(urlBase + '/getCatalogList');
			},
			getDisbursementUnit : function(bean) {
				return httpProvider().get(urlBase + '/getDisbursementUnit');
			},
			getCostCenter : function(bean) {
				return httpProvider().get(urlBase + '/getCostCenter');
			},
			getDisbursementUnitByDepartmentId : function(id) {
				return httpProvider().get(urlBase + '/getDisbursementUnitByDepartmentId/'+id);
			},
			getCostCenterByDisbursementUnitId : function(id) {
				return httpProvider().get(urlBase + '/getCostCenterByDisbursementUnitId/'+id);
			},
			exportReport: function(criterai){
				$reportFactory.post(urlBase + '/exportReport/rpt-70002', criterai);
			}
			
		}
	});
	
	angular.module("Rpt-70002")
	.factory('$reportFactory',function($http){
		function strToArraybuffer(str, fileType){
			var binary_string =  window.atob(str);
		    var bytes = new Uint8Array( binary_string.length );
		    for (var i = 0; i < binary_string.length; i++)        {
		        bytes[i] = binary_string.charCodeAt(i);
		    }
			 var file = new Blob([bytes.buffer], {type: fileType});
			 var fileURL = URL.createObjectURL(file);
			 window.open(fileURL);
		}
		
		return {
				get: function(url, params ){
					$http.get(url, params ).then(function(data) {
						strToArraybuffer(data['data']['file'], data['data']['fileType']);
					});
				},
				post: function(url, params ){
					$http.post(url, params ).then(function(data) {
						strToArraybuffer(data['data']['file'], data['data']['fileType']);
					});
				},
				
		}
	});
	
	angular.module("Rpt-70002")
	.controller("rpt-70002-main",
		function($scope, $log, $location, dataFactory, $uibModal, $http) {
		
			$scope['criteria'] = { 'reportType' : 'PDF' };
			
			$scope['exportPdf'] = function( criteria ){
				dataFactory.exportReport(criteria);
			}
			
			dataFactory.getMCatalogType().then(function(data){
				$scope.catalogTypeList = data;
			});
			dataFactory.getDepartmentAll().then(function(data){
				$scope.departmentList = data;
			});
			dataFactory.getMRevenueType().then(function(data){
				$scope.revenueTypeList = data;
			});
			dataFactory.getCatalogList().then(function(data){
				$scope.catalogList = data;
			});
//			dataFactory.getCostCenter().then(function(data){
//				$scope.costCenterList = data;
//			});
//			dataFactory.getDisbursementUnit().then(function(data){
//				$scope.disbursementUnitList = data;
//			});

			$scope.getDisbursementUnitByDepartmentId = function(id){
				console.log(id);
				if(eval(id)==null){
					id = 0;
				}
				dataFactory.getDisbursementUnitByDepartmentId(id).then(function (response) {
					$scope.disbursementUnitList = response;
				});
			}

			$scope.getCostCenterByDisbursementUnitId = function(id){
				console.log(id);
				if(eval(id)==null){
					id = 0;
				}
				dataFactory.getCostCenterByDisbursementUnitId(id).then(function (response) {
					$scope.costCenterList = response;
				});
			}			
			
			$scope.popup_D1 = {};			
			$scope.popup_D1.opened = false;
			$scope.dateOptions_D1 = {
			    datepickerMode: "day",
			    minMode: "day",
			    showWeeks: false
			};
			$scope.open_popup_D1 = function() {
			    $scope.popup_D1.opened = true;
			}	
			
		});
})(angular);