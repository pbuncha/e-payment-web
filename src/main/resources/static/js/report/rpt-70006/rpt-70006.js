(function() {
	
	var urlBase = ctx + '/reportController';
	
	
	
	angular.module("Rpt-70006", [ 'httpProvider', 'ui.bootstrap', 'ngSanitize',
			'datepickerApp' ]);
	

	angular.module("Rpt-70006")
	.config(function($sceDelegateProvider,$compileProvider) {

		  $sceDelegateProvider.resourceUrlWhitelist(['**']);
		  $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|data):/);
		});

	angular.module("Rpt-70006")
	.factory("dataFactory", function(httpProvider, $reportFactory) {
		return {
			getMCatalogType : function() {
				return httpProvider().get(urlBase + '/getMCatalogType')
			},
			getMinistryAll : function() {
				return httpProvider().get(urlBase + '/getMinistryAll')
			},
			getDepartmentAll : function() {
				return httpProvider().get(urlBase + '/getDepartmentAll')
			},
			getDepartmentByMinistryId : function(id) {
				return httpProvider().get(urlBase + '/getDepartmentByMinistryId/'+id)
			},
			getMRevenueType : function(bean) {
				return httpProvider().get(urlBase + '/getMRevenueType');
			},
			getCatalogList : function(bean) {
				return httpProvider().get(urlBase + '/getCatalogList');
			},
			getCostCenter : function(bean) {
				return httpProvider().get(urlBase + '/getCostCenter');
			},
			getDisbursementUnit : function(bean) {
				return httpProvider().get(urlBase + '/getDisbursementUnit');
			},
			exportReport: function(criterai){
				$reportFactory.post(urlBase + '/exportReport/rpt-70006', criterai);
			}
			
		}
	});
	
	angular.module("Rpt-70006")
	.factory('$reportFactory',function($http){
		function strToArraybuffer(str, fileType){
			var binary_string =  window.atob(str);
		    var bytes = new Uint8Array( binary_string.length );
		    for (var i = 0; i < binary_string.length; i++)        {
		        bytes[i] = binary_string.charCodeAt(i);
		    }
			 var file = new Blob([bytes.buffer], {type: fileType});
			 var fileURL = URL.createObjectURL(file);
			 window.open(fileURL);
		}
		
		return {
				get: function(url, params ){
					$http.get(url, params ).then(function(data) {
						strToArraybuffer(data['data']['file'], data['data']['fileType']);
					});
				},
				post: function(url, params ){
					$http.post(url, params ).then(function(data) {
						strToArraybuffer(data['data']['file'], data['data']['fileType']);
					});
				},
				
		}
	});
	
	angular.module("Rpt-70006")
	.controller("rpt-70006-main",
		function($scope, $log, $location, dataFactory, $uibModal, $http) {
		
			$scope['criteria'] = { 'reportType' : 'EXCEL' };
			
			$scope['exportPdf'] = function( criteria ){
				dataFactory.exportReport(criteria);
			}
			
			dataFactory.getMCatalogType().then(function(data){
				$scope.catalogTypeList = data;
			});
			dataFactory.getMinistryAll().then(function(data){
				$scope.ministryList = data;
			});
//			dataFactory.getDepartmentAll().then(function(data){
//				$scope.departmentList = data;
//			});
			dataFactory.getMRevenueType().then(function(data){
				$scope.revenueTypeList = data;
			});
			dataFactory.getCatalogList().then(function(data){
				$scope.catalogList = data;
			});
			dataFactory.getCostCenter().then(function(data){
				$scope.costCenterList = data;
			});
			dataFactory.getDisbursementUnit().then(function(data){
				$scope.disbursementUnitList = data;
			});

			$scope.getDepartmentByMinistryId = function(id){
				console.log(id);
				if(eval(id)==null){
					id = 0;
				}
				dataFactory.getDepartmentByMinistryId(id).then(function (response) {
					$scope.departmentList = response;
				});
			}
			
			$scope.popup_D1 = {};			
			$scope.popup_D1.opened = false;
			$scope.dateOptions_D1 = {
			    datepickerMode: "day",
			    minMode: "day",
			    showWeeks: false
			};
			$scope.open_popup_D1 = function() {
			    $scope.popup_D1.opened = true;
			}	

			$scope.popup_D2 = {};
			$scope.popup_D2.opened = false;
			$scope.dateOptions_D2 = {
			    datepickerMode: "day",
			    minMode: "day",
			    showWeeks: false
			};
			$scope.open_popup_D2 = function() {
			    $scope.popup_D2.opened = true;
			}	
			
			$scope.popup_M1 = {};
			$scope.popup_M1.opened = false;
			$scope.dateOptions_M1 = {
			    datepickerMode: "month",
			    minMode: "month"
			};
			$scope.open_popup_M1 = function() {
			    $scope.popup_M1.opened = true;
			}	

			$scope.popup_M2 = {};
			$scope.popup_M2.opened = false;
			$scope.dateOptions_M2 = {
			    datepickerMode: "month",
			    minMode: "month"
			};
			$scope.open_popup_M2 = function() {
			    $scope.popup_M2.opened = true;
			}	

			$scope.popup_Y1 = {};
			$scope.popup_Y1.opened = false;
			$scope.dateOptions_Y1 = {
			    datepickerMode: "year",
			    minMode: "year"
			};
			$scope.open_popup_Y1 = function() {
			    $scope.popup_Y1.opened = true;
			}	

			$scope.popup_Y2 = {};
			$scope.popup_Y2.opened = false;
			$scope.dateOptions_Y2 = {
			    datepickerMode: "year",
			    minMode: "year"
			};
			$scope.open_popup_Y2 = function() {
			    $scope.popup_Y2.opened = true;
			}	

			
	});
})(angular);