$(document).ready(function() {
		$(".js-example-placeholder-single").select2({
		  placeholder: "เลือก",
		  allowClear: true,
		  theme: "classic"
		});

		$(".js-example-placeholder-multiple").select2({
			placeholder: "เลือก"
		});

		
		
//		----------------- paging -----------------
		
		var form = $('#menuSearchForm');
		var menuSearch = 'menuSearch';
		var menuSearchResult = 'menuSearchResult';
		var dataList = $('#dataList');
		var isDocumentReady = true;
		
		init();
		
		function init() 
		{
			var params = WS.session.get(menuSearch);
			if(params) 
			{
				form.deserialize(params);
				search();
			} 
			else 
			{
				dataList.DataTable(PagingUtil.getStaticOption());
				isDocumentReady = false;
			}
		}
		
		function getColumns()
		{
			return [
			          {data: "functionName", className: "dt-body-center"},
			          {data: "status", className: "dt-body-center"},
			          {data: "userId", className: "dt-body-center"},
			          {data: "functionId", className: "dt-body-center"}
			       ];
		};

		function search()
		{
			var params = form.serialize();
			
			var dataTableOption = PagingUtil.getDefaultOption();
			
			$.extend(dataTableOption, {
				order: [[0, "asc"]],
				ajaxSource: ctx+"test/datatable?"+params,
				columns: getColumns(),
				createdRow: function (row, data, index) {
					/* getCreateRows(row, data, index); */
		        },
		        stateSave: true,
				stateSaveCallback: function(settings, data) {
					WS.session.setJSON(menuSearchResult, data);
					
					// change state to false
					isDocumentReady = false;
				},
				stateLoadCallback: function(settings) {
					if(isDocumentReady) {
						return WS.session.getJSON(menuSearchResult);
					}
					return false;
				}
			});
			
			WS.session.set(menuSearch, params);
			
			dataList.DataTable(dataTableOption);
		};

		$('#searchBtn').on('click', function() {
			search();
		});
		
		$("#upload-file-input").on("change", function() {
			upload('upload-file-form', 'message', this);
		});
		
	});