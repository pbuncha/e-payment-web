(function(){
	var isAjaxUploadSupported = (function(){
		if (window.FormData === undefined) return false;
		if (window.XMLHttpRequest === undefined) return false;
		var xhr = new XMLHttpRequest();
		return 'upload'in xhr && 'onprogress'in xhr;
	})();
	var uploadUrl = ctx+'views/common/upload/uploadTemporaryFilePart';
	
	$(document).on('click','.open-upload-dialog', function(e){
		var modal = $(this).closest('.upload-file-tab').find('.upload-file-dialog');
		modal.modal('show');
	});
	
	$(document).on('hidden.bs.modal','.upload-file-dialog', function(e){
		$(this).find('input').prop('disabled',true);
	});
	
	$(document).on('show.bs.modal','.upload-file-dialog', function(e){
		$(this).find('input').prop('disabled',false);
	});
	
	$(document).ready(function(){
		$('.upload-file-dialog').find('input').prop('disabled',true);
	});
	
	$(document).on('newElement','.file-upload',function(e){
		if (this != e.target) {
			return;
		}
		if ($(this).data('fileUpload') !== undefined) {
			return;
		} 
		$(this).data('fileUpload', true);
		var multipart_params = {};
		multipart_params[csrfParameterName]=csrfToken;
		$(".file-upload").plupload({
			runtimes : "html5,html4",
			url: uploadUrl,
			max_file_count: 20,
			chunk_size: "1024kb",
			filters: {
				max_file_size: "100mb",
				mime_types: [
					{title:"Image",extensions:"png,jpg"},
					{title:"Document", extensions:"pdf,doc,docx"},
//					{title:"Any File", extensions:"*"}
				]
			},
			rename: false,
			sortable: false,
			dragdrop: true,
			views: {
				list: true,
				active: 'list'
			},
			multipart_params : multipart_params
		});
	});
	
	$(document).on('uploaded',function(e,data){
		_fileIds.push($.trim(data.result.response));
	});	
	
	$(document).on('click','.plupload_button.plupload_start', function(e){
		$(this).closest('.file-upload').plupload('start');
	});
	
	function clearForm()
	{
		$('.docTitle').val('');
		$('.docNo').val('');
		$('.docDate').val('');
		$('.roomNo').val('');
		$('.cabinetNo').val('');
		$('.shelfNo').val('');
		$('.docComment').val('');
		$('.msg-empty-file').attr('class', 'row form-group hidden msg-empty-file');
	};
	
	$(document).on('complete',function(e){
		var queueLen = $('.plupload_filelist_content').children().length;
		if(queueLen>0)
		{
			$('.documentAttach').val(queueLen);
		}
	});
	
	$(document).on('click','.upload-ok', function(e){
		var documentNameValid = $('#docTitle').valid();
		var documentDateValid = $('#docDate').valid();
		var documentAttachValid = ($('.documentAttach').val()>0);
		
		if(documentNameValid && documentDateValid && documentAttachValid)
		{
			if($.trim($('.jsonStringOldFile').val()).length>0)
			{
				var commonFiles = JSON.parse($('.jsonStringOldFile').val());
				for(var i in commonFiles)
				{
					var commonFile = commonFiles[i];
					var _fileId = commonFile.fileId;
					var _fileName = commonFile.fileName;
					var fileData = (_fileId+','+_fileName);
					_fileIds.push(fileData);
				}
			}
			
			var tempData = saveDocTemp($('#docId').val(), $('#docTitle').val(), $('#docNo').val(), $('#docDate').val(), $('#roomNo').val(), $('#cabinetNo').val(), $('#shelfNo').val(), $('#docComment').val());
			$('#civilDocTemp').val(JSON.stringify(tempData));
			var civilSubmissionForm = $('#civilSubmissionForm');
			civilSubmissionForm.ajaxForm({
				success: function(responseText){
					$('.civil-common-doc-container').empty();
					$('.civil-common-doc-container').append(responseText);
					$('#civilDocTemp').val('');
					_fileIds = [];
					$('.upload-file-dialog').modal('hide');
				}
			});
			civilSubmissionForm.submit();
		}
		else
		{
//			$('#-error').remove();
			$('.msg-empty-file').attr('class', 'row form-group msg-empty-file');
		}
	});
	
	$('.upload-file-dialog').on('shown.bs.modal', function (e) {
		var uploader = $(".file-upload").plupload('getUploader').splice();
		$('.plupload_filelist_content', uploader).empty();
		clearForm();
    });
})();

/// modal upload


var _fileIds = [];
function saveDocTemp(_docId,_docTitle,_docNo,_docDate,_roomNo,_cabinetNo,_shelfNo,_docComment)
{
	var _tempFileIds = [];
	for(var i in _fileIds)
	{
		var fileData = _fileIds[i].split(',');
		_tempFileIds.push({fileId:fileData[0], fileName:fileData[1]});	
	}
	var docId = undefined;
	if(_docId!=undefined)
	{
		docId = _docId;
	}
	
	return [{docId:docId,docTitle:_docTitle,docNo:_docNo,docDate:_docDate,roomNo:_roomNo,cabinetNo:_cabinetNo,shelfNo:_shelfNo,docComment:_docComment,commonFiles:_tempFileIds}];
}
