$(document).on('configRule.validator',function(e){
	$(e.target).find('input[data-validator=confirmPassword]').addBack('input[data-validator=confirmPassword]').each(function(idx,ele){
		$(ele).rules('add',{
			equalTo: "#password",
			messages: {
				equalTo: "กรุณาระบุรหัสผ่านให้ตรงกัน"	
			}
		});
	});
	
	$(e.target).find('input[data-validator=confirmEmail]').addBack('input[data-validator=confirmEmail]').each(function(idx,ele){
		$(ele).rules('add',{
			equalTo: "#email",
			messages: {
				equalTo: "กรุณาระบอีเมลให้ตรงกัน"	
			}
		});
	});
});

$(document).on('configRule.validator',function(e){
	$(e.target).find('input[data-validator-password=checkPassword]').addBack('input[data-validator-password=checkPassword]').each(function(idx,ele){
		$(ele).rules('add',{
			remote: {
				url: ctx + 'admin/user/checkPassword',
				type: "POST",
				skipLoading: true,
				data: {
					password: function() {
						return $(ele).val();
					}
				}
			}
		});
	});
});