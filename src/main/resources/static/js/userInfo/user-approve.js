var app = angular.module("userApproveApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp','dualmultiselect','ngMaterial','filter.util' ]);

app.factory("dataFactory", function (httpProvider,reportFactory) {
	var urlBase = ctx+'/userInfoController';
	var dataFactory = {};

	dataFactory.getdeptList = function () {
		return  httpProvider().get(urlBase+'/getdeptList');
	};
	
	dataFactory.getMCostCenterList = function () {
		return  httpProvider().get(urlBase+'/getMCostCenterList');
	};
	
	dataFactory.getMBusinessAreaList = function () {
		return  httpProvider().get(urlBase+'/getMBusinessAreaList');
	};
	
	dataFactory.search = function (data) {
        return  httpProvider().post(urlBase+'/search',data);
    };	
    
    dataFactory.update = function (dataList) {
        return  httpProvider().post(urlBase+'/updateStatusUserPermission',dataList);
    };	
    
    dataFactory.getfile = function(path){
		reportFactory.post(urlBase + '/getfile', path);
	};

	return dataFactory;
});

app.factory('reportFactory',function($http){
	function strToArraybuffer(str, fileType,fileName){
		var a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";
		var binary_string =  window.atob(str);
		var bytes = new Uint8Array( binary_string.length );
		for (var i = 0; i < binary_string.length; i++)        {
			bytes[i] = binary_string.charCodeAt(i);
		}
		var file = new Blob([bytes.buffer], {type: fileType});
		var fileURL = URL.createObjectURL(file);

		console.log(fileURL);
//		window.open(fileURL,"_blank");
		a.href = fileURL;
		a.download = fileName;
		a.click();
		window.URL.revokeObjectURL(fileURL);
	}

	return {
		get: function(url, params ){
			$http.get(url, params ).then(function(data) {
				strToArraybuffer(data['data']['file'], data['data']['fileType']);
			});
		},
		post: function(url, params ){
			$http.post(url, params ).then(function(data) {				
				strToArraybuffer(data['data']['file'], data['data']['fileType'],data['data']['fileName']);
			});
		},

	}
});

app.controller("userApproveCtrl", function ($log,$scope,dataFactory,$uibModal,$document,$window,$mdDialog) {

	$log.log('Master : Department List');
	dataFactory.getdeptList().then(function (response) {
		console.log(response);
		$scope.deptList = response;
	});
	
	$log.log('Master : getMCostCenterList');
	dataFactory.getMCostCenterList().then(function (response) {
		console.log(response);
		$scope.costCenterList = response;
	});
	
	$log.log('Master : getMBusinessAreaList');
	dataFactory.getMBusinessAreaList().then(function (response) {
		console.log(response);
		$scope.businessAreaList = response;
	});
	
	$scope.data = {};
	$scope.data.approveComment = "กรุณาระบุเหตุผลหากไม่อนุมัติ";
	$scope.selectItem = [];
	$scope.userList = [];
	$scope.sliceFavoriteList = [];
	$scope.perPages = 10;
	$scope.totalItems = $scope.userList.length;
	$scope.currentPage = 1;
	
	$scope.values = [10,15,20,25];
	
	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
	    var end = begin + $scope.perPages;
	    $scope.from = begin;
	    $scope.to = end;
	    $scope.sliceFavoriteList = $scope.userList.slice(begin, end);
	}
	
	$scope.pageChanged = function(currentPage) {
		$scope.currentPage = currentPage;
		$scope.figureOut();
	};
	
	$scope.search = function(){
		if($scope.data.userType==""){
			$scope.data.userType = null;
		}
		if($scope.data.statusApprove==""){
			$scope.data.statusApprove = null;
		}
		if($scope.data.statusUse==""){
			$scope.data.statusUse = null;
		}
		if($scope.data.departmentId==""){
			$scope.data.departmentId = null;
		}
		if($scope.data.businessAreaId==""){
			$scope.data.businessAreaId = null;
		}
		if($scope.data.costCenterId==""){
			$scope.data.costCenterId = null;
		}
		$log.log("data tor : ",$scope.data);
		dataFactory.search($scope.data).then(function(response){
			console.log(response);
			$scope.userList = response;
			$scope.totalItems = $scope.userList.length;
			$scope.figureOut();
				
		}, function (error) {
			
		});
	}
	
	//orderByField
	$scope.orderByField = '';
	$scope.reverseSort = false;
	$scope.orderFunction = function(field) {
		$scope.orderByField = field;
		$scope.reverseSort = !$scope.reverseSort;		
	}
	$scope.checkField = function(name) {
		if ($scope.orderByField == name) {
			return true;
		} else {
			return false;
		}
	}
	
	$scope.clear = function(){
		$scope.userList =[];
		$scope.data = {};
		$scope.sliceFavoriteList = [];
		$scope.perPages = 10;
		$scope.totalItems = $scope.userList.length;
		$scope.currentPage = 1;
		$scope.selectItem = [];
		$scope.isSelectAll = false;
	};
	
	$scope.selectToDel = function( item ){
		var index = $scope.isDelete(item.userid);
		if(index > -1 ){
			$scope.selectItem.splice(index,1);
		}else{
			$scope.selectItem.push(item);
		}
	}

	$scope.isDelete = function( id ){
		for( var i =0 ; i<$scope.selectItem.length; i++ ){
			if( $scope.selectItem[i].userid == id ){
				return i;
			}
		}
		return -1;
	}
	
	$scope.selectAll = function(isSelectAll){
		console.log(isSelectAll);
		if( isSelectAll ){
			$scope.selectItem = angular.copy($scope.sliceFavoriteList);
		}else{
			$scope.selectItem = [];
		}

	}
	
	function showAlert (textContent) {
		$mdDialog.show(
				$mdDialog.alert()
				.clickOutsideToClose(true)
				.title('Error')
				.textContent(textContent)
				.ok('OK')
		);
	}
	
	$scope.print = function(path){
		dataFactory.getfile(path).catch(function(response) {
			console.error('data error', response);
			showAlert("ไม่สามารถเปิดไฟล์ได้.","ผิดพลาด");
		});
	}
	
	$scope.approve = {
			status: '1'
	};
	$scope.update = function(dataList){
		if(dataList.length == 0){
			showAlert("กรุณาเลือกรายการ");
		}else{
			
			for(var i in $scope.selectItem){
				$scope.selectItem[i].status = $scope.approve.status;
				
				if($scope.approve.status==0){
					//notApprove
					$scope.selectItem[i].approveComment = $scope.data.approveComment;
					if("กรุณาระบุเหตุผลหากไม่อนุมัติ"==$scope.data.approveComment){
						showAlert("กรุณาระบุเหตุผลหากไม่อนุมัติ");
					}else{
						console.log(dataList);
						
						dataFactory.update(dataList).then(function (response) {
							$scope.search();
							$scope.selectItem = [];
							$scope.isSelectAll = false;
							$scope.approve.status = "1";
							$scope.data.approveComment = "กรุณาระบุเหตุผลหากไม่อนุมัติ";
						}, function (error) {
							
						});
					}
						
					
				}else{
					//approves
					$scope.selectItem[i].approveComment = null;
					console.log(dataList);
					
					dataFactory.update(dataList).then(function (response) {
						$scope.search();
						$scope.selectItem = [];
						$scope.isSelectAll = false;
						$scope.approve.status = "1";
						$scope.data.approveComment = "กรุณาระบุเหตุผลหากไม่อนุมัติ";
					}, function (error) {
						
					});
				}
				
			}
			
			
			
			
		}
	}
	
});	
