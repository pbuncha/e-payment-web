var app = angular.module("userInfoApp", ['httpProvider','ngAnimate','ui.bootstrap','ngRoute','ngResource','angucomplete','ngAnimate','datepickerApp','dualmultiselect','ngMaterial','filter.util' ]);

app.factory("dataFactory", function (httpProvider,reportFactory) {
	var urlBase = ctx+'/userInfoController';
	var dataFactory = {};
	var urlBaseRegis = ctx+'/registerController';

	dataFactory.getUserCitizen = function (id) {
		return httpProvider().get(urlBaseRegis+'/register/getUserCitizen/'+id);
	};

	dataFactory.getAmphur = function (id) {
		return httpProvider().get(urlBaseRegis+'/register/getAmphur/'+id);
	};

	dataFactory.getTambon = function (id) {
		return httpProvider().get(urlBaseRegis+'/register/getTambon/'+id);
	};

	dataFactory.getTitleList = function () {
		return  httpProvider().get(urlBaseRegis+'/getTitleList');
	};

	dataFactory.getGroupList = function (id) {
		return  httpProvider().get(urlBase+'/searchGroup/'+id);
	};

	dataFactory.update = function (bean) {
		return httpProvider().post(urlBase+'/updateStatus',bean);
	};
	dataFactory.getfile = function(path){
		reportFactory.post(urlBase + '/getfile', path);
	};
	return dataFactory;
});
app.factory('reportFactory',function($http){
	function strToArraybuffer(str, fileType,fileName){
		var a = document.createElement("a");
		document.body.appendChild(a);
		a.style = "display: none";
		var binary_string =  window.atob(str);
		var bytes = new Uint8Array( binary_string.length );
		for (var i = 0; i < binary_string.length; i++)        {
			bytes[i] = binary_string.charCodeAt(i);
		}
		var file = new Blob([bytes.buffer], {type: fileType});
		var fileURL = URL.createObjectURL(file);

		console.log(fileURL);
//		window.open(fileURL,"_blank");
		a.href = fileURL;
		a.download = fileName;
		a.click();
		window.URL.revokeObjectURL(fileURL);
	}

	return {
		get: function(url, params ){
			$http.get(url, params ).then(function(data) {
				strToArraybuffer(data['data']['file'], data['data']['fileType']);
			});
		},
		post: function(url, params ){
			$http.post(url, params ).then(function(data) {				
				strToArraybuffer(data['data']['file'], data['data']['fileType'],data['data']['fileName']);
			});
		},

	}
});
app.controller("userInfoCtrl", function ($log,$scope,dataFactory,$uibModal,$document,$window,$mdDialog,$http) {
	$scope.userBean = {};
	$scope.gov = {};
	$scope.resultList = [];
	$scope.sliceData = [];
	$scope.perPages = 10;
	$scope.totalItems = $scope.resultList.length;
	$scope.currentPage = 1;
	$scope.values = [10,15,20,25];

	dataFactory.getTitleList().then(function (response) {
		$scope.titleList = response;
		$log.log(' Title List : ',$scope.titleList);
	});

	$scope.figureOut = function(){
		var begin = (($scope.currentPage - 1) * $scope.perPages);
		var end = begin + $scope.perPages;
		console.log(begin+" "+end);
		$scope.from = begin;
		$scope.to = end;
		$scope.sliceData = $scope.resultList.slice(begin, end);
	}

	$scope.pageChanged = function() {
		$scope.figureOut();

	};

	$scope.getUserCitizen = function () {
		console.log($scope.userBean.citizenid);
		dataFactory.getUserCitizen($scope.userBean.citizenid).then(function (response) {
			console.log(response);
			if(response){
				$scope.userBean = response;
				$scope.getAmphur($scope.userBean.provinceId);
				$scope.getTambon($scope.userBean.amphurId);

				dataFactory.getGroupList($scope.userBean.userid).then(function (data) {
					console.log(data);
					$scope.resultList = data;
					$scope.totalItems = $scope.resultList.length;
					$scope.figureOut();
				});
			}else{
				showAlert('ไม่พบข้อมูลของเลขบัตรประชาชนดังกล่าว');
			}

		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};

	$scope.getAmphur = function (id) {
		console.log(id);
//		$scope.userBean.provinceId = id;
		if(undefined==id){
			id = 0;
		}
		dataFactory.getAmphur(id).then(function (response) {
			console.log(response);
			$scope.dataAmphur = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};

	$scope.getTambon = function (id) {
		console.log(id);
		if(undefined==id){
			id = 0;
		}
		dataFactory.getTambon(id).then(function (response) {
			console.log(response);
			$scope.dataTambon = response;
		}).catch(function(response) {
			console.error('data error', response);
		})
		.finally(function() {
			console.log("finally finished");
		});
	};


	function showAlert (textContent,title) {
		$mdDialog.show(
				$mdDialog.alert()
				.clickOutsideToClose(true)
				.title(title)
				.textContent(textContent)
				.ok('OK')
		);
	};



	$scope.save = function(){
		console.log($scope.userBean);
		$scope.userBean.userPermissionGroup = $scope.groupSelect;
		dataFactory.update($scope.userBean).then(function (data) {
			showAlert("บันทึกสำเร็จ.","ผลการบันทึก");
		}).catch(function(response) {
			console.error('data error', response);
			showAlert("ไม่สามารถบันทึกได้.","ผิดพลาด");
		});
	};

	$scope.groupSelect = [];
	$scope.check = [];
	$scope.select = function(data){
		var index = -1;
		console.log(data);
		console.log($scope.groupSelect);
		$scope.groupSelect.some(function(obj,j) {
			return obj.permisId ===  data.permisId ? index = j : false;
		});
		console.log(index);
		if( index != -1 ){
			$scope.groupSelect.splice( index, 1 );
		}else{
			$scope.groupSelect.push(data);
		}
		
	console.log($scope.groupSelect);
	};
	
	$scope.print = function(path){
		dataFactory.getfile(path).catch(function(response) {
			console.error('data error', response);
			showAlert("ไม่สามารถเปิดไฟล์ได้.","ผิดพลาด");
		});
	}
	
	$scope.selectAll = function(isSelect){
		for(var i=0; i<$scope.resultList.length; i++){
			console.log(isSelect);
			console.log($scope.check);
			$scope.check[i] = isSelect;
		}
		if(isSelect){
			$scope.groupSelect = angular.copy($scope.sliceData);
		}else{
			$scope.groupSelect = [];
		}

	}
});	
