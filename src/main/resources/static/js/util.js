function calendar(id) {
	$('#' + id).datepicker({
		format : "dd/mm/yyyy",
		language : 'th',
		isBuddhist : true,
		clearBtn : true,
		todayHighlight : true,
		todayBtn : true

	})
	// Listen for the change even on the input
	.change(dateChanged).on('changeDate', dateChanged);

	function dateChanged(ev) {
		$(this).datepicker('hide');
	}
}
function modal() {
	// alert('modal');
	$('.ui.modal').modal('show');
}

// Get year generate
function getYear(id, defaultValue) {
	var minOffset = -1, maxOffset = 10; // Change to whatever you want
	var thisYear = (new Date()).getFullYear();

	thisYear = thisYear;
	var opt = new Option("optYear", id);
	for (var i = minOffset; i <= maxOffset; i++) {
		var year = thisYear - i;
		$('<option>', {
			value : year,
			text : year + 543
		}).appendTo($('#' + id));
	}
	$('#' + id).find('option[value="' + defaultValue + '"]').attr("selected",
			true);
}
// Get year month
function getFiscalMonth(id, year, defaultValue) {
	if (year != -1) {
		year = parseInt(year) + 543;
		$('#' + id).find('option').remove();
		$('#' + id)
				.append(
						'<option selected="selected" th:value="-1" value="-1">กรุณาเลือก</option>');

		var monthMap = [ {
			id : 10,
			name : "ตุลาคม " + (year - 1)
		}, {
			id : 11,
			name : "พฤศจิกายน " + (year - 1)
		}, {
			id : 12,
			name : "ธันวาคม " + (year - 1)
		}, {
			id : 1,
			name : "มกราคม " + (year)
		}, {
			id : 2,
			name : "กุมภาพันธ์ " + (year)
		}, {
			id : 3,
			name : "มีนาคม " + (year)
		}, {
			id : 4,
			name : "เมษายน " + (year)
		}, {
			id : 5,
			name : "พฤษภาคม " + (year)
		}, {
			id : 6,
			name : "มิถุนายน " + (year)
		}, {
			id : 7,
			name : "กรกฎาคม " + (year)
		}, {
			id : 8,
			name : "สิงหาคม " + (year)
		}, {
			id : 9,
			name : "กันยายน " + (year)
		} ];

		$.each(monthMap, function(index, data) {
			$('#' + id).append($('<option />').val(data.id).html(data.name));
		});
		$('#' + id).find('option[value="' + defaultValue + '"]').attr(
				"selected", true);
	} else {
		$('#' + id).find('option').remove();
		$('#' + id)
				.append(
						'<option selected="selected" th:value="-1" value="-1">กรุณาเลือก</option>');
	}
}
// New Order Date for compare
function newOrderDate(d) {
	var dd = d.substring(0, 2);
	var mm = d.substring(3, 5);
	var yyyy = d.substring(6, 10);
	return yyyy + ',' + mm + ',' + dd;
}

function dateToThaiString(d) {

	var dd = d.substring(8, 10);
	var mm = d.substring(5, 7);
	var yyyy = d.substring(0, 4);

	var monthMap = [ {
		id : "01",
		name : "มกราคม "
	}, {
		id : "02",
		name : "กุมภาพันธ์ "
	}, {
		id : "03",
		name : "มีนาคม "
	}, {
		id : "04",
		name : "เมษายน "
	}, {
		id : "05",
		name : "พฤษภาคม "
	}, {
		id : "06",
		name : "มิถุนายน "
	}, {
		id : "07",
		name : "กรกฎาคม "
	}, {
		id : "08",
		name : "สิงหาคม "
	}, {
		id : "09",
		name : "กันยายน "
	}, {
		id : "10",
		name : "ตุลาคม "
	}, {
		id : "11",
		name : "พฤศจิกายน "
	}, {
		id : "12",
		name : "ธันวาคม "
	} ];
	for (var i = 0; i < monthMap.length; i++) {
		if (mm == monthMap[i].id) {
			mm = monthMap[i].name;
		}
	}

	yyyy = parseInt(yyyy) + 543;

	return dd + ' ' + mm + '' + yyyy;
}

function monthToThaiString(m) {

	var monthMap = [ {
		id : "1",
		name : "มกราคม "
	}, {
		id : "2",
		name : "กุมภาพันธ์ "
	}, {
		id : "3",
		name : "มีนาคม "
	}, {
		id : "4",
		name : "เมษายน "
	}, {
		id : "5",
		name : "พฤษภาคม "
	}, {
		id : "6",
		name : "มิถุนายน "
	}, {
		id : "7",
		name : "กรกฎาคม "
	}, {
		id : "8",
		name : "สิงหาคม "
	}, {
		id : "9",
		name : "กันยายน "
	}, {
		id : "10",
		name : "ตุลาคม "
	}, {
		id : "11",
		name : "พฤศจิกายน "
	}, {
		id : "12",
		name : "ธันวาคม "
	} ];
	var mStr = '';
	for (var i = 0; i < monthMap.length; i++) {
		if (m == monthMap[i].id) {
			mStr = monthMap[i].name;
		}
	}

	return mStr;
}
function compareDate(start, end) {
	start = newOrderDate(start);
	end = newOrderDate(end);
	if (start != '' && end != '') {
		var startDate = new Date(start);
		var endDate = new Date(end);
		if (endDate < startDate) {
			$('.daterrors').css({
				display : "block"
			});
			return false;
		} else {
			return true;
		}
	}
	return false;
}
// form utility
$(document).ready(
		function() {
			$('[data-click-change-form-action]').on(
					'click',
					function() {
						$(this).closest('form').attr("action",
								$(this).attr("data-click-change-form-action"));
					});
		});

// dataTable utility
$(document).ready(function() {
	$.fn.extend({
		paging : function(option) {
			this.dataTable($.extend(PagingUtil.getDefaultOption(), option));
		}
	});
});

function paginateTable(tableId, numPerPage) {
	$('table' + tableId).each(
			function() {
				var currentPage = 0;
				if (undefined == numPerPage) {
					numPerPage = 10;
				}
				var $table = $(this);
				$table.bind('repaginate', function() {
					$table.find('tbody tr').hide().slice(
							currentPage * numPerPage,
							(currentPage + 1) * numPerPage).show();
				});
				$table.trigger('repaginate');
				var numRows = $table.find('tbody tr').length;
				var numPages = Math.ceil(numRows / numPerPage);
				var $pager = $('<div class="pager"></div>');

				for (var page = 0; page < numPages; page++) {
					$('<span class="page-number"></span>').text(page + 1).bind(
							'click',
							{
								newPage : page
							},
							function(event) {
								currentPage = event.data['newPage'];
								$table.trigger('repaginate');
								$(this).addClass('active').siblings()
										.removeClass('active');
							}).appendTo($pager).addClass('clickable');
				}

				$pager.insertAfter($table).find('span.page-number:first')
						.addClass('active');
			});
};

function showMessageBox(success, msg) {
	if (undefined === success) {
		$('#headerMsg').addClass('hidden');
		return;
	}

	var headerMsg = $('#headerMsg').attr('class', 'col-md-12');
	var p = headerMsg.children().attr(
			'class',
			success ? 'bg-success text-success panel-round'
					: 'bg-danger text-danger panel-round');
	$(p[0].children[0]).attr(
			'class',
			success ? 'hidden glyphicon glyphicon-exclamation-sign'
					: 'glyphicon glyphicon-exclamation-sign');
	$(p[0].children[1]).attr(
			'class',
			success ? 'glyphicon glyphicon-ok-circle'
					: 'hidden glyphicon glyphicon-ok-circle');
	$(p[0].children[2]).text(msg);

	$('body,html').animate({
		scrollTop : 0
	}, 400);
};

function checkDateFormat(date) {
	var datePattern = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
	return datePattern.test(date);
}

function detectBackButtonEvent() {

	$(document).unbind('keydown').bind(
			'keydown',
			function(event) {

				if (event.keyCode === 8) {
					var doPrevent = false;

					var d = event.srcElement || event.target;
					if ((d.tagName.toUpperCase() === 'INPUT' && (d.type
							.toUpperCase() === 'TEXT'
							|| d.type.toUpperCase() === 'PASSWORD'
							|| d.type.toUpperCase() === 'FILE'
							|| d.type.toUpperCase() === 'SEARCH'
							|| d.type.toUpperCase() === 'EMAIL'
							|| d.type.toUpperCase() === 'NUMBER' || d.type
							.toUpperCase() === 'DATE'))
							|| d.tagName.toUpperCase() === 'TEXTAREA') {
						doPrevent = d.readOnly || d.disabled;
					} else if (d.getAttribute('contenteditable') == true) {
						doPrevent = false;
					} else {
						doPrevent = true;
					}

					if (doPrevent) {
						event.preventDefault();

						// show dialog to confirm back to history
						$('#backModal').modal('show');
					}
				}

			});
}

function setNextFocus(curEleName, nextEleName, len) {
	if ($('#' + curEleName).val().length == len) {
		$('#' + nextEleName).focus();
	}
};

function upload(formSelectorName, msgSelectorName) {
	if (undefined != msgSelectorName && null != msgSelectorName) {
		var msgSelector = $('#' + msgSelectorName);
		msgSelector.text('');
	}

	// if(''==uploadObj.value)
	// {
	// return;
	// }

	var form = $('#' + formSelectorName);

	$.ajax({
		url : form.attr('action'),
		type : form.attr('method'),
		data : new FormData(form[0]),
		enctype : 'multipart/form-data',
		processData : false,
		contentType : false,
		cache : false,
		success : function(result) {
			if (undefined != msgSelectorName && null != msgSelectorName) {
				return result.message;
				// msgSelector.text(result.message);
			}
		}
	// ,
	// error: function (xhr) {
	// if(undefined!=msgSelectorName && null!=msgSelectorName)
	// {
	// if(xhr.status == 403 || xhr.status == 400)
	// {
	// msgSelector.text("พบข้อผิดพลาด");
	// }
	// else
	// {
	// msgSelector.text("แนบไฟล์ผิดพลาด เนื่องจากไฟล์มีขนาดใหญ่เกิน 10 MB");
	// }
	// }
	// }
	});
};

$(document).ready(function() {
	$("#collapse").click(function() {
		// $("#sidebar").animate().fadeOut();
		// $("#sidebar").animate({width:'toggle'},{complete:function(){$("#expand").show()}});
		// $("#sidebar").animate({
		// right: '+=200px',height: '0px'
		// });

		// $("#sidebar").animate().slideToggle();
		$("#slide-menu").animate().fadeOut({
			complete : function() {
				$("#content").animate({
					width : '100%'
				});
				$("#collapse").hide();
				$("#expand").show();
			}
		});
	});

	$("#expand").click(function(event) {
		// $("#sidebar").animate({width: '19%'}).fadeIn();
		// $("#content").animate({width: '80%'});
		// $("#sidebar").removeAttr("style");
		// $("#content").removeAttr("style");

		// $("#sidebar").animate().slideDown({
		$("#slide-menu").animate().fadeIn({
			start : function(promise) {
				$("#content").removeAttr("style");
				$("#collapse").show();
				$("#expand").hide();
			}
		});
	});
});

function getDateGregorianFromThaiDate(s) {
	var isBE = new Date().getFullYear() > 2500;
	var reg = /(\d{1,2})\/(\d{1,2})\/(\d{1,4})/
	var gs;
	if (null !== (gs = reg.exec(s))) {
		var d = gs[1];
		var m = gs[2];
		var y = gs[3] - (isBE ? 0 : 543);
		return new Date(y, m - 1, d);
	}
	return null;
};
function getThaiDateFromGregorianDate(date) {
	var isBE = new Date().getFullYear() > 2500;
	return pad(date.getDate(), 2) + '/' + pad(date.getMonth() + 1, 2) + '/'
			+ pad(date.getFullYear() + (isBE ? 0 : 543), 4);
};
function pad(n, width, z) {
	z = z || '0';
	n = n + '';
	return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
};

function getDateBetween(dateStartString, dateToString) {
	var startDate = getDateGregorianFromThaiDate(dateStartString);
	var toDate = getDateGregorianFromThaiDate(dateToString);
	var diff = new Date(toDate - startDate);
	return (diff / 1000 / 60 / 60 / 24) + 1;
}

function getDate(dateStartString, dateToString, isDay) {
	var receivedDate = getDateGregorianFromThaiDate(dateStartString);
	receivedDate = moment(receivedDate);

	var toDate = getDateGregorianFromThaiDate(dateToString);
	if (undefined == dateToString) {
		toDate = moment().toDate();
		toDate = getDateGregorianFromThaiDate(getThaiDateFromGregorianDate(toDate));
	}
	toDate = moment(toDate);

	var diffDuration = moment.duration(toDate.diff(receivedDate));
	if (undefined == isDay) {
		return diffDuration.years();
	}

	return diffDuration.days() + 1;
};

function calculateAge(datePickerSelector, elementShowAge) {
	var eleType = $('#' + elementShowAge).attr('type');

	$('#' + datePickerSelector).change(function(evt) {
		if ('' == $(evt.target).val()) {
			if (eleType != 'text') {
				$('#' + elementShowAge).text(0);
			} else {
				$('#' + elementShowAge).val(0);
			}
		}

		if (eleType != 'text') {
			$('#' + elementShowAge).text(getDate($(evt.target).val()));
		} else {
			$('#' + elementShowAge).val(getDate($(evt.target).val()));
		}
	});
};

function calculateAgeLabel(dateSelector, elementShowAge) {
	var ele = $('#' + dateSelector);
	$('#' + elementShowAge).text(getDate(ele.text().trim()));
};

function isNull(param) {
	if (null == param || "null" == param || "" == param
			|| (typeof (param) == "undefined")) {
		return true;
	}
	return false;
}

function initialSelect2(id, placeholderText) {
	if ($("#" + id).length != 0) {
		$("#" + id).select2(
				{
					placeholder : (placeholderText == undefined ? "โปรดระบุ"
							: placeholderText),
					allowClear : true,
					theme : "classic",
					width : "100%"
				});
	}
}

function initialSelect2Multiple(id) {
	if ($("#" + id).length != 0) {
		$("#" + id).select2();
	}
}

function provinceSelect2OnChange(provinceId, callback) {

	// var districtEle =
	// provinceEle.closest('.province-set').find('select.districtSlc');
	// var subDistrictEle =
	// provinceEle.closest('.province-set').find('select.subDistrictSlc');

	if (!isNull(provinceId)) {
		var url = ctx + 'province_filter/getDistrict/?provinceId=' + provinceId;
		$.ajax({
			skipLoading : true,
			url : encodeURI(url),
			success : function(result) {
				if ($('#districtSlc').length != 0) {
					$('#districtSlc').empty();
					$('#districtSlc').append(new Option('โปรดระบุ', -1));
					result.results.map(function(item) {
						var option1 = new Option(item.text, item.id, false);
						$('#districtSlc').append(option1);
					});
					$('#districtSlc').select2("val", "");
				}

				if ($('#subDistrictSlc').length != 0) {
					clearSelect2("subDistrictSlc");
				}
			}
		});
	} else {
		if ($('#districtSlc').length != 0) {
			clearSelect2("districtSlc");
		}
		if ($('#subDistrictSlc').length != 0) {
			clearSelect2("subDistrictSlc");
		}
	}
}

function districtSelect2OnChange(districtId) {

	if (!isNull(districtId)) {
		var url = ctx + 'province_filter/getSubDistrict/?districtId='
				+ districtId;
		$.ajax({
			skipLoading : true,
			url : encodeURI(url),
			success : function(result) {
				if ($('#subDistrictSlc').length != 0) {
					$('#subDistrictSlc').empty();
					$('#subDistrictSlc').append(new Option('โปรดระบุ', -1));
					result.results.map(function(item) {
						var option1 = new Option(item.text, item.id, false);
						$('#subDistrictSlc').append(option1);
					});
					$('#subDistrictSlc').select2("val", "");
				}
			}
		});
	} else {
		if ($('#subDistrictSlc').length != 0) {
			clearSelect2("subDistrictSlc");
		}
	}
}

function initDistrictSelect2(provinceId, districtId) {
	var url = ctx + 'province_filter/getDistrict/?provinceId=' + provinceId;
	$.ajax({
		skipLoading : true,
		url : encodeURI(url),
		success : function(result) {
			$('#districtSlc').empty();
			result.results.map(function(item) {
				var option1 = new Option(item.text, item.id, false);
				$('#districtSlc').append(option1);
			});
			$('#districtSlc').val(districtId);
			initialSelect2('districtSlc');
		}
	});
}

function initSubDistrictSelect2(districtId, subDistrictId) {
	var url = ctx + 'province_filter/getSubDistrict/?districtId=' + districtId;
	$.ajax({
		skipLoading : true,
		url : encodeURI(url),
		success : function(result) {
			$('#subDistrictSlc').empty();
			result.results.map(function(item) {
				var option1 = new Option(item.text, item.id, true);
				$('#subDistrictSlc').append(option1);
			});
			$('#subDistrictSlc').val(subDistrictId);
			initialSelect2('subDistrictSlc');
		}
	});
}

function provinceSelect2OnChangeWithId(provinceId, districtE, subDistrictE,
		callback) {

	var districtId = $(districtE).attr('id');
	var subDistrictId = $(subDistrictE).attr('id');
	if (!isNull(provinceId)) {
		var url = ctx + 'province_filter/getDistrict/?provinceId=' + provinceId;
		$.ajax({
			skipLoading : true,
			url : encodeURI(url),
			success : function(result) {
				if ($('#' + districtId).length != 0) {
					$('#' + districtId).empty();
					$('#' + districtId).append(new Option('โปรดระบุ', -1));
					result.results.map(function(item) {
						var option1 = new Option(item.text, item.id, false);
						$('#' + districtId).append(option1);
					});
					$('#' + districtId).select2("val", "");
				}

				if ($('#' + subDistrictId).length != 0) {
					clearSelect2(subDistrictId);
				}
			}
		});
	} else {
		if ($('#' + districtId).length != 0) {
			clearSelect2(districtId);
		}
		if ($('#' + subDistrictId).length != 0) {
			clearSelect2(subDistrictId);
		}
	}
}

function districtSelect2OnChangeWithId(districtId, subDistrictE) {

	var subDistrictId = $(subDistrictE).attr('id');
	if (!isNull(districtId)) {
		var url = ctx + 'province_filter/getSubDistrict/?districtId='
				+ districtId;
		$.ajax({
			skipLoading : true,
			url : encodeURI(url),
			success : function(result) {
				if ($('#' + subDistrictId).length != 0) {
					$('#' + subDistrictId).empty();
					$('#' + subDistrictId).append(new Option('โปรดระบุ', -1));
					result.results.map(function(item) {
						var option1 = new Option(item.text, item.id, false);
						$('#' + subDistrictId).append(option1);
					});
					$('#' + subDistrictId).select2("val", "");
				}
			}
		});
	} else {
		if ($('#' + subDistrictId).length != 0) {
			clearSelect2(subDistrictId);
		}
	}

}

function initDistrictSelect2WithId(provinceId, districtId, districtVal) {
	var url = ctx + 'province_filter/getDistrict/?provinceId=' + provinceId;
	$.ajax({
		skipLoading : true,
		url : encodeURI(url),
		success : function(result) {
			$('#' + districtId).empty();
			result.results.map(function(item) {
				var option1 = new Option(item.text, item.id, false);
				$('#' + districtId).append(option1);
			});
			$('#' + districtId).val(districtVal);
			initialSelect2(districtId);
		}
	});
}

function initSubDistrictSelect2WithId(districtId, subDistrictId, subDistrictVal) {
	var url = ctx + 'province_filter/getSubDistrict/?districtId=' + districtId;
	$.ajax({
		skipLoading : true,
		url : encodeURI(url),
		success : function(result) {
			$('#' + subDistrictId).empty();
			result.results.map(function(item) {
				var option1 = new Option(item.text, item.id, true);
				$('#' + subDistrictId).append(option1);
			});
			$('#' + subDistrictId).val(subDistrictVal);
			initialSelect2(subDistrictId);
		}
	});
}

function initOrgData(selectorId, limit, _placeholder) {
	$("#" + selectorId).select2Ajax({
		ajax : {
			url : ctx + 'organization/getOrganization'
		},
		placeholder : _placeholder
	});
};

function calculateDateByAddYear(inputDateValue, inputYearValue, resultDateId,
		resultHiddenId, dateAddValue, notMoreThanThisDate) {
	if (!isNull(inputDateValue) && inputDateValue.length > 0) {
		if (!isNull(inputYearValue) && -1 != inputYearValue) {
			var yearCount = parseInt(inputYearValue);
			var thaiDate = getDateGregorianFromThaiDate(inputDateValue);
			
			var addDate = 0
			if (undefined != dateAddValue) {
				addDate = dateAddValue;
			}
			var result = (moment(thaiDate).add(yearCount, 'year')).add(addDate,
					'day').toDate();
			if(undefined != notMoreThanThisDate && !isNull(notMoreThanThisDate) && notMoreThanThisDate.length > 0 && notMoreThanThisDate != '-'){
				var checkDate = getDateGregorianFromThaiDate(notMoreThanThisDate);
				if(result.getTime() > checkDate.getTime()) result = checkDate;
			}
			$("#" + resultDateId).text(getThaiDateFromGregorianDate(result));
			$("#" + resultHiddenId).val(getThaiDateFromGregorianDate(result));
		} else {
			$("#" + resultDateId).text('-');
			$("#" + resultHiddenId).val();
		}
	} else {
		$("#" + resultDateId).text('-');
		$("#" + resultHiddenId).val();
	}
}

// function clearSelect2(e){
// e.empty();
// e.trigger('change');
// }

function clearSelect2(id) {
	$('#' + id).empty();
	$('#' + id).append(new Option('โปรดระบุ', -1));
	$('#' + id).select2("val", "");
}

function clear_form_elements(id) {
	jQuery("#" + id).find(':input').each(function() {
		switch (this.type) {
		case 'password':
		case 'text':
		case 'textarea':
		case 'file':
		case 'select-one':
		case 'select-multiple':
			jQuery(this).val('');
			if (undefined != $(this).data('select2')) {
				$(this).val('-1');
				$(this).trigger('change');
			}
			break;
		case 'checkbox':
		case 'radio':
			this.checked = false;
		}
	});
}

function clear_form(e) {
	e.find(':input').each(function() {
		switch (this.type) {
		case 'password':
			jQuery(this).val('');
			break;
		case 'text':
			jQuery(this).val('');
			break;
		case 'textarea':
			jQuery(this).val('');
			break;
		case 'file':
			jQuery(this).val('');
			break;
		case 'select-one':
		case 'select-multiple':
			jQuery(this).val('-1');
			if (undefined != $(this).data('select2')) {
				$(this).val('-1');
				$(this).trigger('change');
			}
			break;
		case 'checkbox':
		case 'radio':
			this.checked = false;
		}
		;
	});
}

$.fn.digits = function() {
	return this
			.each(function() {
				$(this).text(
						$(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g,
								"$1,"));
			})
}

function calculateYearWith2Date(startDate, endDate) {
	var startDateTmp = getDateGregorianFromThaiDate(startDate);
	var startDateYear = startDateTmp.getFullYear();
	var startDateMonth = startDateTmp.getMonth();
	var startDateDay = startDateTmp.getDate();
	var endDateTmp = getDateGregorianFromThaiDate(endDate);
	var endDateYear = endDateTmp.getFullYear();
	var endDateMonth = endDateTmp.getMonth();
	var endDateDay = endDateTmp.getDate();
	var result = startDateYear - endDateYear;

	if (result > 0) {

		if (endDateMonth < startDateMonth)
			return (result);

		else if (endDateMonth > startDateMonth)
			return result -1;
		else if (endDateDay < startDateDay)
			return (result);
		else
			return result- 1;
	} else {
		return 0;
	}

	return result;
};

function initRelateURL() {
	var reqURI = $(location).attr('pathname');
	var reqURI = reqURI.split('/');
	var relatedId = "";
	for (i = 0; i < reqURI.length; i++) {
		if (isInt(reqURI[i])) {
			relatedId = reqURI[i];
			break;
		}
	}

	jQuery('.related-id').each(
			function() {
				if (relatedId != "") {
					if (undefined != $(this).attr('href')) {
						$(this).attr('href',
								$(this).attr('href').replace('?', relatedId));
					}
				} else {
					$(this).attr('href', '#');
				}
			});
};

function isInt(value) {
	var x;
	if (isNaN(value)) {
		return false;
	}
	x = parseFloat(value);
	return (x | 0) === x;
}

function initFlowchart(data) {
	var chart = flowchart.parse(data);
	chart.drawSVG('diagram');
	// chart.drawSVG('diagram', {
	// // 'x': 30,
	// // 'y': 50,
	// 'line-width': 3,
	// 'maxWidth': 3,//ensures the flowcharts fits within a certian width
	// 'line-length': 50,
	// 'text-margin': 10,
	// 'font-size': 14,
	// 'font': 'normal',
	// 'font-family': 'Helvetica',
	// 'font-weight': 'normal',
	// 'font-color': 'black',
	// 'line-color': 'black',
	// 'element-color': 'black',
	// 'fill': 'white',
	// 'yes-text': 'yes',
	// 'no-text': 'no',
	// 'arrow-end': 'block',
	// 'scale': 1,
	// 'symbols': {
	// 'start': {
	// 'font-color': 'red',
	// 'element-color': 'green',
	// 'fill': 'yellow'
	// },
	// 'end':{
	// 'class': 'end-element'
	// }
	// },
	// 'flowstate' : {
	// 'past' : { 'fill' : '#CCCCCC', 'font-size' : 12},
	// 'current' : {'fill' : 'yellow', 'font-color' : 'red', 'font-weight' :
	// 'bold'},
	// 'future' : { 'fill' : '#FFFF99'},
	// 'request' : { 'fill' : 'blue'},
	// 'invalid': {'fill' : '#444444'},
	// 'approved' : { 'fill' : '#58C4A3', 'font-size' : 12, 'yes-text' :
	// 'APPROVED', 'no-text' : 'n/a' },
	// 'rejected' : { 'fill' : '#C45879', 'font-size' : 12, 'yes-text' : 'n/a',
	// 'no-text' : 'REJECTED' }
	// }
	// });
};

$.fn.select2Ajax = function(option) {
	option = $.extend(true, {
		placeholder : (option.placeholder == undefined ? "โปรดระบุ"
				: option.placeholder),
		allowClear : true,
		theme : "classic",
		width : '100%',
		ajax : {
			skipLoading : true,
			url : 'select2',
			dataType : 'json',
			delay : 250,
			cache : true
		},
		minimumInputLength : 0
	}, option);
	this.select2(option);
};

$(document).on('newElement', '[data-select2-url]', function(e) {
	if (e.target !== this) {
		return;
	}
	var url = $(e.target).attr('data-select2-url');
	var select = $(e.target).find('select').addBack('select');
	var placeholder = $(e.target).attr('data-select2-placeholder');
	if (placeholder === undefined) {
		defaultOption = select.find('option[value=""]');
		if (defaultOption.length > 0)
			placeholder = defaultOption.text();
	}
	var allowClear = select.find('option[value=""]').length > 0;
	var ajax = null;
	if (url != '') {
		ajax = {
			url : url
		};
		allowClear = true;
	}
	select.select2Ajax({
		ajax : ajax,
		placeholder : placeholder,
		allowClear : allowClear
	});
	select.closest('form').on('reset', function(e) {
		setTimeout(function() {
			select.trigger('change');
		}, 0);
	});
});

function getNumberWithCommaFormat(val) {
	if (val == '.00') {
		return '0.00';
	}
	return val.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
};

function isWorkingDay(date) {
	return ((getDateGregorianFromThaiDate(date)).getDay() != 6 && (getDateGregorianFromThaiDate(date))
			.getDay() != 0);
};

function getAddressOrg(orgId, groupDiv, personType)
{
	if (personType != 4) {
		return;
	}

	$.ajax({
		url : encodeURI(ctx + 'organization/getOrganizationById?orgId='+ orgId),
		success : function(result) {
			$('#' + groupDiv).find('input:eq(0)').val(result.address);
			$('#' + groupDiv).find('input:eq(1)').val(result.zipCode);

			$('#provinceSlc').val(result.provinceId);
			initialSelect2('provinceSlc');
			initDistrictSelect2(result.provinceId, result.districtId);
			initSubDistrictSelect2(result.districtId,result.subDistrictId);
		}
	});
};

function getOrgDataByTypeId(orgId, groupDiv, personTypeId) {
	getAddressOrg(orgId, groupDiv, personTypeId);
};

function getOrgData(orgId, groupDiv, personTypeDiv) {
	var personType = $('#' + personTypeDiv + ' select').val();
	getAddressOrg(orgId, groupDiv, personType);
};

function showMessageConfirm(statusId){
	if(statusId == 'R'){
		$('#normalCase').hideForm();$('#actionButton').hide();
		$('#rejectCase').showForm();$('#rejectButton').show();
		$("#rejectCase textarea").attr("required",true);
	}else {
		$('#rejectCase').hideForm();$('#rejectButton').hide();
		$('#normalCase').showForm();$('#actionButton').show();
		$("#rejectCase textarea").removeAttr("required");
	}
}

function managePersonType(personTypeId){
	$("#AddressGroup").showForm();
	
	if(personTypeId == 1 || personTypeId == 2) {
		$("#personalType1and2Group , #deathGroup").showForm();
		$("#personalType3Group").hideForm();

		if(personTypeId == 1) {
			$("#modalOrganizeDiv,#spanOrgType1").showForm();
			$("#spanOrgType4").hideForm();
		}else{
			$("#modalOrganizeDiv").hideForm();
			clear_form_elements('modalOrganizeDiv');
		}
		
		manageDeathGroup();
		clear_form_elements('personalType3Group');
//		clear_form_elements('AddressGroup');

	}else if(personTypeId == 3){
		$("#personalType1and2Group , #deathGroup , #modalOrganizeDiv").hideForm();
		$("#personalType3Group").showForm();

		clear_form_elements('personalType1and2Group');
		clear_form_elements('modalOrganizeDiv');
//		clear_form_elements('AddressGroup');
		clear_form_elements('deathGroup');
	}else if(personTypeId == 4){
		$("#personalType1and2Group , #deathGroup , #personalType3Group , #spanOrgType1").hideForm();
		$("#modalOrganizeDiv , #spanOrgType4").showForm();

		clear_form_elements('personalType1and2Group');
		clear_form_elements('personalType3Group');
		clear_form_elements('deathGroup');
	}else{
		$("#personalType1and2Group , #deathGroup , #personalType3Group , #modalOrganizeDiv , #AddressGroup").hideForm();
		
		clear_form_elements('personalType1and2Group');
		clear_form_elements('personalType3Group');
		clear_form_elements('modalOrganizeDiv');
//		clear_form_elements('AddressGroup');
		clear_form_elements('deathGroup');
	}
}