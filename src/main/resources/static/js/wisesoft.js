var WS = {
	session: {
		set: function(key, data) {
			sessionStorage.setItem(key, data);
		},
		get: function(key) {
			return sessionStorage.getItem(key);
		},
		setJSON: function(key, data) {
			sessionStorage.setItem(key, JSON.stringify(data));
		},
		getJSON: function(key) {
			return JSON.parse(sessionStorage.getItem(key));
		},
		remove: function(key) {
			sessionStorage.removeItem(key);
		},
		clearAll: function() {
			var i = sessionStorage.length;
			while(i--) {
			  var key = sessionStorage.key(i);
			  sessionStorage.removeItem(key); 
			}
		}
	},
	page: {
		change: function(url) {
			window.location.href = url;
		},
		replace: function(url) {
			window.location.replace(url);
		},
		back: function() {
			window.history.back();
		}
	},
	format: {
		telephone: function(telno) {
			
			if(telno.length > 4) {
				
				var fsub = telno.length%4
				var telnoFormat = telno.substr(0,fsub) + "-";
				
				var idx = fsub+4;
				while(idx < telno.length) {
					telnoFormat += telno.substr(idx-4, idx-1) + "-";
					idx += 4;
				}
				
				telnoFormat += telno.substr(telno.length-4, telno.length-1);
				
				return telnoFormat;
			}
			
			return telno;
		}
	}
};

// WiseSoft jQuery Function
$.fn.serializeObject = function()
{
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};