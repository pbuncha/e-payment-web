package th.go.cgd.epayment;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import th.go.cgd.epayment.entity.UserCenter;
import th.go.cgd.epayment.service.UserService;
import th.go.cgd.epayment.util.CalendarHelper;

/**
 * @author torgan.p 15 ธ.ค. 2560 14:43:57
 *
 */
@SpringBootTest(classes = { EPaymentWebApplication.class })
@RunWith(SpringJUnit4ClassRunner.class)

public class DateTest {
	@Autowired
	private UserService userService;
    @Test
	public void getAge() throws Exception {
	UserCenter userCenter = userService.findOneUserCenter("1469909874812");
	Integer age = CalendarHelper.getAge(userCenter.getBirthDate());
	System.out.println("Date : "+userCenter.getBirthDate());
	System.out.println("Age : "+age);
	
	}

}
