package th.go.cgd.epayment;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javax.mail.MessagingException;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;

import th.go.cgd.epayment.controller.common.LoginController;
import th.go.cgd.epayment.model.AttachmentBean;
import th.go.cgd.epayment.model.EmailBean;
import th.go.cgd.epayment.service.interfaces.IEmailServiceImpl;

/**
 * @author torgan.p 14 ธ.ค. 2560 20:46:53
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class EmailTest {
    
    @Autowired
    private IEmailServiceImpl emailService;
	public static ResourceBundle bundle = ResourceBundle.getBundle("TemplateEmail");
	private static final Logger logger = Logger.getLogger(EmailTest.class);
	@Test
	public void sendMail() throws MessagingException, IOException {
	    
	    
		EmailBean mail = new EmailBean();

			mail.setTo("torgan_2net@hotmail.com,torgan.p@motiftech.com,dominoman10@gmail.com".split(","));
		
		String fullName = "torgan";
		String username = "1234";
		Object[] objArray = {fullName,username,"http://localhost:8080/e-payment-web"};
		String  content = emailService.getTemplateMail(objArray,"register.verify");
		mail.setSubject(bundle.getString("register.subject.verify"));
		mail.setText(content);
		emailService.sendMail(mail);
		logger.info("Send Mail Success : "+mail.getTo()[0]);
	    
//	    EmailBean emailBean = new EmailBean();
//	    emailBean.setTo("torgan_2net@hotmail.com,torgan.p@motiftech.com".split(","));
////	    emailBean.setTo("torgan.p@motiftech.com".split(","));
//	    emailBean.setText("xxxxxxxxx");
//	    
//	    List<AttachmentBean> attaList = new ArrayList<AttachmentBean>();
//	    File f = new File("C:/log.txt");
//	String fileBase64 = encodeFileToBase64Binary(f);
//
//	AttachmentBean attachmentBean = new AttachmentBean();
//	attachmentBean.setFilename(f.getName());
//	attachmentBean.setData(fileBase64.getBytes());
////	    text/html
//	attachmentBean.setMimeType("text/plan; charset=TIS-620");
//	attaList.add(attachmentBean);
//	    emailBean.setAttachments(attaList);
//	    emailServiceImpl.sendMail(emailBean);
//	    
	    
//	    Object[] objArray = {"111","222","333","444"};
//	    emailServiceImpl.getTemplateMail(objArray, "invoice");
	}
	private InputStreamSource convertBase64ToByte(String base64) {
		byte[] imgBytes = Base64.decodeBase64(base64);

		return new ByteArrayResource(imgBytes);
	} 
	
	private static String encodeFileToBase64Binary(File file) throws IOException {
//	    File file = new File(fileName);
	    byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
	    return new String(encoded, StandardCharsets.UTF_8);
	}
}
