package th.go.cgd.epayment;

import java.io.IOException;

import javax.mail.MessagingException;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author torgan.p 22 ม.ค. 2561 22:44:41
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
//@Ignore
public class EncodePassword {
//    	@Test
	public static void encode() throws MessagingException, IOException {
    	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    	
    	String hashedPassword = passwordEncoder.encode("1234");
    	System.out.println(hashedPassword);
//    	$2a$10$hPm/Olo/3MT1bFAzyRTPuOUzV/cD9rD7cSNWGzfbcoKgTL14a/Sg2
    	  if(BCrypt.checkpw("1234","$2a$10$8tFsGk99irGbNkP68CHuQOlcRuPVnk99mbZJ2PlH55g3f9BTBfveK")){
    		System.out.println(hashedPassword);
    		System.out.println(BCrypt.gensalt());
    		System.out.println(BCrypt.hashpw(hashedPassword, BCrypt.gensalt()));
    		
    		
    	  }
    
    	}
	    
    	
    	public static void main(String[] args) throws Exception {
    	encode();
    	}
}
