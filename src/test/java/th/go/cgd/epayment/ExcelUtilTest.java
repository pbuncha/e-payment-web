package th.go.cgd.epayment;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.itextpdf.text.DocumentException;
import th.go.cgd.epayment.report.service.ReportUtilService;

@RunWith(SpringJUnit4ClassRunner.class)
public class ExcelUtilTest {

	@Test
	public void generateExcelReport() {
		try {
			byte[] b = ReportUtilService.genarateCustomExcelReport(DATA, SHEET_NAME);
			FileOutputStream outputStream = new FileOutputStream(REPORT_EXCEL_NAME);
			outputStream.write(b);
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generatePdfReport() throws DocumentException, FileNotFoundException {
		try {
			byte[] b = ReportUtilService.genarateCustomPdfReport(DATA);

			FileOutputStream outputStream = new FileOutputStream(REPORT_PDF_NAME);
			outputStream.write(b);
			outputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static final Object[][] DATA = {
				{ "Datatype", "Type", "Size(in bytes)", "Size(in bytes)", "Size(in bytes)", "Size(in bytes)" },
				{ "int", "Primitive", 2, "Size(in bytes)", "Size(in bytes)", "Size(in bytes)" },
				{ "float", "Primitive", 4, "Size(in bytes)", "Size(in bytes)", "Size(in bytes)" },
				{ "double", "Primitive", 8, "Size(in bytes)", "Size(in bytes)", "Size(in bytes)" },
				{ "char", "Primitive", 1, "Size(in bytes)", "Size(in bytes)", "Size(in bytes)" },
				{ "String", "Non-Primitive", "No fixed size", "Size(in bytes)", "Size(in bytes)", "Size(in bytes)" }
			};

	private static final String SHEET_NAME = "sheet 1";
	private static final String REPORT_EXCEL_NAME = "C:\\Users\\wichuda.k\\Desktop\\MyFirstExcel.xlsx";
	private static final String REPORT_PDF_NAME = "C:\\Users\\wichuda.k\\Desktop\\MyFirstExcel.pdf";

}
