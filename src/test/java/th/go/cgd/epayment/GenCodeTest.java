package th.go.cgd.epayment;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import th.go.cgd.epayment.service.interfaces.IBarcodeService;
import th.go.cgd.epayment.service.interfaces.IGenerateCodeService;

/**
 * @author torgan.p 4 ม.ค. 2561 23:23:36
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class GenCodeTest {
    @Autowired
   private IGenerateCodeService generateCodeService;
    @Autowired
    IBarcodeService barcodeService;
    
    @Test
    public void genCodeTest(){
	generateCodeService.generateBillCode("P", 1);
    }
    @Test
    public void barcodeServiceTest(){
	
	System.out.println(barcodeService.getDataQRCode("33","44","55"));
    }
}
